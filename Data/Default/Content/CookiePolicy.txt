We and our business partners collect information about your use of our website through cookies.  Cookies are information files stored on your computer that help websites remember who you are and information about your visit.  Cookies can help to display the information on a website in a way that matches your interests.  Most major websites use cookies.  We use two types of cookie on our website.

The first type is a 'session-based' cookie.  This means that it is only saved on your hard disk for the time you are using our website.  We use this type of cookie to facilitate World Pay 3D secure transactions.  This cookie stores a World Pay ID which identifies the transaction.  This cookie is saved on your computer by World Pay.  You can find information about the cookie and how to disable it at <a href="http://www.worldpay.com/about_us/index.php?page=privacy&c=UK">http://www.worldpay.com/about_us/index.php?page=privacy&c=UK</a>. The World Pay payment service will not operate without this cookie. 

The second type of cookie we use is a 'persistent' cookie.  These stay on your hard disk for one year.  We use several of these for different purposes:

<b>Affiliate Tracking</b> - We and the other organisations we use keep information on the path you take to get to our website and on some of the pages you visit or use through our website, using cookies.  

<b>Language</b> - If you select a language in which to view our website, we will remember your preference using a cookie.

<b>Shopping Cart</b> - We use cookies to remember what you have put in your shopping cart.

<b>Google Analytics</b> - We use Google's analytics service to helps us understand the number of different people coming to our website, rather than the total number of times the site is used.  Without this cookie, if you visited the site once each week for three weeks we would count you as three separate users.  This cookie is saved on your computer by Google.  You can find information about the cookie and how to disable it at <a href="http://www.google.com/intl/en/privacy/privacy-policy.html">http://www.google.com/intl/en/privacy/privacy-policy.html</a>.

<b>Cookie Preference</b> - We use a cookie to make sure that your preferences in relation to cookies are respected.

We do not store any of your personal information in any of our cookies.  We only use cookies that are necessary to provide you with services you have requested or that analyze the effectiveness of our website and to help us make our website better for all our users.

If you do not want a website to create a cookie on your computer, most web browsers provide the option to disable them.  However, our website may not work properly if you reject our cookies.

If you want to know more about how cookies work and how to manage or delete them, visit the <a href="http://www.w3.org/Security/Faq/wwwsf2.html#CLT-Q10">World Wide Web Consortium's website</a> or visit the <a href="http://www.allaboutcookies.org/manage-cookies/">Interactive Advertising Bureau's website</a>.

We may update this policy from time to time so you may want to check it each time you visit our website.  We last changed this policy on 18 November 2011.

&copy; All rights reserved.  November, 2011.

