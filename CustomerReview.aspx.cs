﻿using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the Customer Review Page class.
    /// </summary>
    public partial class CustomerReview : CommonPageBase
    {
        #region Member Variables
        private int _productId;
        #endregion

        protected override void Page_PreInit(object sender, EventArgs e)
        {
            // Clear the current category session theme if some other category selected.
            Session["CurrentCategoryTheme"] = null;

            this.MasterPageFile = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/MasterPages/CustomerReview.master";

            // Get Product id from querystring  
            if (Request.Params["zpid"] != null)
            {
                this._productId = int.Parse(Request.Params["zpid"]);

                //Product = ZNodeProduct.Create(this._productId, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);
                Product = ZNodeProduct.GetProductDetails(this._productId, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);//Added Optimized SP call to avoid unneccessory data 2015-04-07
               
            }

            if (Product == null)
            {
                Response.Redirect("~/default.aspx");
            }

            // Add to http context so it can be shared with user controls
            HttpContext.Current.Items.Add("Product", Product);

            // Set SEO Properties
            ZNodeSEO seo = new ZNodeSEO();

            // Set SEO default values to the product 
            ZNodeMetaTags tags = new ZNodeMetaTags();
            seo.SEOTitle = tags.ProductTitle(Product);
            seo.SEODescription = tags.ProductDescription(Product);
            seo.SEOKeywords = tags.ProductKeywords(Product);
        }
    }
}
