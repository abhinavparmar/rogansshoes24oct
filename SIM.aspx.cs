﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Payment;
using ZNode.Libraries.Framework.Business;
using log4net;

namespace WebApp
{
    public partial class SIM : System.Web.UI.Page
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (SIM));

        private readonly PaymentSettingService _paymentSettingService;
        private readonly OrderService _orderService;

        public SIM()
        {
            _paymentSettingService = new PaymentSettingService();
            _orderService = new OrderService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.Form["x_MD5_Hash"]))
            {
                ProcessAuthorizeNetDirectPostRequest();
            }
            else
            {
                RedirectToRoot();
            }
        }

        private void ProcessAuthorizeNetDirectPostRequest()
        {
            _log.Debug("A request has been received to process an Authorize.NET direct post.");

            var directPostMethodSettings = _paymentSettingService.GetByGatewayTypeID((int) GatewayType.AUTHORIZE_DPM);

            if(directPostMethodSettings.Count != 1)
            {
                _log.Error("A payment gateway provider has not been setup for Authorize.NET Direct Post Method (DPM), or more than one provider is configured for DPM. (Only one provider can be configured.)");
                RedirectToRoot();
            }

            var directPostMethodSetting = directPostMethodSettings.First();
            var crypto = new ZNodeEncryption();

            var response = new AuthorizeNet.SIMResponse(Request.Form);
            var isValid = response.Validate("znode_hash", crypto.DecryptData(directPostMethodSetting.GatewayUsername));

            if (!isValid)
            {
                _log.Debug("Invalid");
                RedirectToRoot();
            }

            _log.Debug("Valid");

            _log.Debug(response.ToString().Replace("<li>", System.Environment.NewLine + "\t"));

            var fieldsSent = Request.Form.AllKeys.Select(key => string.Format("key: {0}, value: {1}", key, Request.Form[key]));

            _log.DebugFormat("The following request fields were submitted:{0}{1}", System.Environment.NewLine, string.Join(System.Environment.NewLine + "\t", fieldsSent));

            var order = _orderService.GetByOrderID(int.Parse(response.InvoiceNumber));

            _log.DebugFormat("Billing Street: {0}", order.BillingStreet);

            ReturnRedirect(Request.Url.GetLeftPart(UriPartial.Authority) + Response.ApplyAppPathModifier("/") +
                           string.Format("?m={0}&rc={1}", response.Message, response.ResponseCode));
        }

        private void RedirectToRoot()
        {
            ReturnRedirect("/");
        }

        private void ReturnRedirect(string url)
        {
            var redirecter = Redirecter(url);
            _log.DebugFormat("Getting ready to send the following response to the payment gateway: {0}", redirecter);
            Response.Write(redirecter);
            Response.End();
        }

        public static string Redirecter(string toUrl)
        {
            return string.Format("<html><head><script type='text/javascript' charset='utf-8'>	window.location='{0}';</script><noscript><meta http-equiv='refresh' content='1;url={0}'></noscript></head><body></body></html>", toUrl);
        }
    }
}