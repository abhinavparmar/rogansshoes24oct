﻿using SquishIt.Framework;
using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the OrderReceipt Page class.
    /// </summary>
    public partial class OrderReceipt : CommonPageBase
    {
        private bool isApplyStyle = true;

        protected override void Page_PreInit(object sender, EventArgs e)
        {
            if (HttpContext.Current.Session["cs"] != null && ZNodeConfigManager.SiteConfig.MobileTheme != null)
            {
                this.MasterPageFile = "~/themes/" + ZNodeConfigManager.SiteConfig.MobileTheme + "/MasterPages/OrderReceipt.master";
            }
            else
            {
                // Clear the current category session theme if some other category selected.
                Session["CurrentCategoryTheme"] = null;

                this.MasterPageFile = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/MasterPages/OrderReceipt.master";
            }
        }

        /// <summary>
        /// Set the Header Section
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected override void Page_Load(object sender, EventArgs e)
        {
            //Zeon Custom Code: Start
            // HTML

            #region[Commented Code]
            /*
            string cssInclude = string.Empty;

            if (HttpContext.Current.Session["cs"] != null && ZNodeConfigManager.SiteConfig.MobileTheme != null)
            {
                cssInclude = (Bundle.Css()
                    .Add("~/bootstrap/css/bootstrap.min.css")
                    .Add(ZNodeCatalogManager.GetCssPathByLocale(ZNodeConfigManager.SiteConfig.MobileTheme, ZNodeCatalogManager.CSS))
                    .Add(ZNodeCatalogManager.GetReceiptCssPathByLocale(ZNodeConfigManager.SiteConfig.MobileTheme, "Receipt"))
                    .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery.selectbox.css")
                    .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui-1.10.0.custom.min.css")
                    .Render("~/Squished/CSS/squished_" + ZNodeCatalogManager.Theme + "_#.css?v=" + ConfigurationManager.AppSettings["CSSVersion"].ToString()));
            }
            else
            {


                cssInclude = (Bundle.Css()
                   .Add("~/bootstrap/css/bootstrap.min.css")
                   .Add(ZNodeCatalogManager.GetCssPathByLocale(ZNodeCatalogManager.Theme, ZNodeCatalogManager.CSS))
                   .Add(ZNodeCatalogManager.GetReceiptCssPathByLocale(ZNodeCatalogManager.Theme, "Receipt"))
                   .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery.selectbox.css")
                   .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui-1.10.0.custom.min.css")
                   .Render("~/Squished/CSS/squished_" + ZNodeCatalogManager.Theme + "_#.css?v=" + ConfigurationManager.AppSettings["CSSVersion"].ToString()));

            }

            cssInclude = cssInclude + "<link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700|Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,900,900italic,700italic' rel='stylesheet' type='text/css'>";
            cssInclude = cssInclude + "<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><meta name='viewport' content='width=device-width, initial-scale=1'><meta name='viewport' content='minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no' />";

            // Add a reference for StyleSheet to the head section
            this.Page.Header.InnerHtml = cssInclude;
            */
            #endregion

            this.BindThemeCSSForSquishItOrWithout();

            // Add a reference for skype meta tag to the head section
            AddFaviconIconOnPage();
            HtmlMeta metaTag = new HtmlMeta();
            metaTag.Name = "SKYPE_TOOLBAR";
            metaTag.Content = "SKYPE_TOOLBAR_PARSER_COMPATIBLE";
            if (this.Page.Header != null) this.Page.Header.Controls.Add(metaTag);
            HtmlMeta metaTelTag = new HtmlMeta();
            metaTelTag.Name = "format-detection";
            metaTelTag.Content = "telephone=no";
            if (this.Page.Header != null) this.Page.Header.Controls.Add(metaTelTag);
            // Add a reference for skype meta tag to the head section
            BindRobotMetaTags();
        }

        #region[Zeon Custom Code for Render Css]

        /// <summary>
        /// Bind Theme CSS Fo rSquishIt Or Without using web.config flag
        /// </summary>
        private void BindThemeCSSForSquishItOrWithout()
        {
            bool isapplySquishIT = ConfigurationManager.AppSettings["ApplySquishIT"] != null ? bool.Parse(ConfigurationManager.AppSettings["ApplySquishIT"].ToString()) : false;

            if (isapplySquishIT)
            {
                this.ApplySquishITCss();
            }
            else
            {
                ApplyCssWithoutUsingSquishIT();
            }
        }

        /// <summary>
        /// Apply Css Without Using SquishIT
        /// </summary>
        private void ApplyCssWithoutUsingSquishIT()
        {
            string cssInclude = string.Empty;

            if (HttpContext.Current.Session["cs"] != null && ZNodeConfigManager.SiteConfig.MobileTheme != null)
            {
                IncludeCSS(this, "bootstrap/css/bootstrap.min.css");
                IncludeCSS(this, ZNodeCatalogManager.GetCssPathByLocale(ZNodeConfigManager.SiteConfig.MobileTheme, ZNodeCatalogManager.CSS).Replace("~/", string.Empty));
                IncludeCSS(this, ZNodeCatalogManager.GetReceiptCssPathByLocale(ZNodeConfigManager.SiteConfig.MobileTheme, "Receipt").Replace("~/", string.Empty));
                //Include Jquery CSS
                IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery.selectbox.css");
                IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui-1.10.0.custom.min.css");
                IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui.css");
                IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/Css/" + ZNodeCatalogManager.Theme + "_Client.css"); //PRFT Custom Code
            }
            else
            {
                IncludeCSS(this, "bootstrap/css/bootstrap.min.css");
                IncludeCSS(this, ZNodeCatalogManager.GetCssPathByLocale(ZNodeCatalogManager.Theme, ZNodeCatalogManager.CSS).Replace("~/", string.Empty));
                IncludeCSS(this, ZNodeCatalogManager.GetReceiptCssPathByLocale(ZNodeCatalogManager.Theme, "Receipt").Replace("~/", string.Empty));
                IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery.selectbox.css");
                IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui-1.10.0.custom.min.css");
                IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui.css");
                IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/Css/" + ZNodeCatalogManager.Theme + "_Client.css"); //PRFT Custom Code
                string fontURL = GetFontURL();
                IncludeCSS(this, fontURL); 

                //Include MetaTag
                this.Page.Header.Controls.Add(new LiteralControl("<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><meta name='viewport' content='width=device-width, initial-scale=1'><meta name='viewport' content='minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no' />"));
            }

        }

        /// <summary>
        /// Apply SquishIT Css
        /// </summary>
        private void ApplySquishITCss()
        {
            string userAgent = Request.UserAgent; //entire UA string
            string browser = Request.Browser.Type; //Browser name and Major Version #
            int browserVersion = Request.Browser.MajorVersion;
            string include = string.Empty;
            string cssInclude = string.Empty;

            if (userAgent.Contains("Trident") && userAgent.Contains("Trident/5.0"))
            {
                ApplyIE9CssPatch(include);
            }
            else
            {
                if (HttpContext.Current.Session["cs"] != null && ZNodeConfigManager.SiteConfig.MobileTheme != null)
                {
                    cssInclude = (Bundle.Css()
                        .Add("~/bootstrap/css/bootstrap.min.css")
                        .Add(ZNodeCatalogManager.GetCssPathByLocale(ZNodeConfigManager.SiteConfig.MobileTheme, ZNodeCatalogManager.CSS))
                        .Add(ZNodeCatalogManager.GetReceiptCssPathByLocale(ZNodeConfigManager.SiteConfig.MobileTheme, "Receipt"))
                        .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery.selectbox.css")
                        .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui-1.10.0.custom.min.css")
                        .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/Css/" + ZNodeCatalogManager.Theme + "_Client.css") //PRFT Custom Code
                        .Render("~/Squished/CSS/squished_" + ZNodeCatalogManager.Theme + "_#.css?v=" + ConfigurationManager.AppSettings["CSSVersion"].ToString()));
                }
                else
                {
                    cssInclude = (Bundle.Css()
                       .Add("~/bootstrap/css/bootstrap.min.css")
                       .Add(ZNodeCatalogManager.GetCssPathByLocale(ZNodeCatalogManager.Theme, ZNodeCatalogManager.CSS))
                       .Add(ZNodeCatalogManager.GetReceiptCssPathByLocale(ZNodeCatalogManager.Theme, "Receipt"))
                       .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery.selectbox.css")
                       .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui-1.10.0.custom.min.css")
                       .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/Css/" + ZNodeCatalogManager.Theme + "_Client.css") //PRFT Custom Code
                       .Render("~/Squished/CSS/squished_" + ZNodeCatalogManager.Theme + "_#.css?v=" + ConfigurationManager.AppSettings["CSSVersion"].ToString()));
                }
                cssInclude = cssInclude + "<link href='" + GetFontURL() + "' rel='stylesheet' type='text/css'>";
                cssInclude = cssInclude + "<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><meta name='viewport' content='width=device-width, initial-scale=1'><meta name='viewport' content='minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no' />";
                // Add a reference for StyleSheet to the head section
                this.Page.Header.InnerHtml = cssInclude;
            }
        }

        #endregion
    }
    //Zeon Custom Code: Start
}
