
DBCI Quick Reference
1.	Navigate to Database\ZnodeDBCI_Schema\ from the project root.
2.	Edit the file Kissprops.xml and put in the local database connection used for development.
3.	Set the project to run Debug DBCI build configuration. 
4.	Database should update after compile. 
5.	When adding a new script look at the prefix of the last file. Script file should be in the following format: 854-SampleUpdateProductSchema.sql
6.	If you have not installed the database navigate to the \[CoreVersion]\Database\Backup and restore the .bak file

More info about DBCI

There is a config file called Kissprops.xml located in the ZnodeDBCI_Schema directory. This has the connection string pointed to local and the database name. You will need to change this for your local development. Don�t check this in, because it will cause everyone else to have to change it back. Open the project in visual studio and select Debug DBCI configuration. After building the project it will run all the scripts and record the database version. Look in the output window to see the results. It will not run scripts more than once. The table is called db_schema. If a script fails the build will break. 

�	Each script must be unique and start with a number prefix
�	Right now we are in the 800s. Just ask if you are not sure or check the db_schema for the next value. 
�	All sprocs and db changes must be added to the scripts folder (try to do one script per db object)
�	Don�t write scripts that would remove customer data. Alter table is good, but not drop unless you have to.
�	Don�t overwrite a previous script. Instead add a new script to undo the changes
�	Checking in a bak file will no longer be necessary except at the creation of the branch
�	Run the Debug DBCI after every database change even net Tiers. Put the output of net tiers generated sql in the scripts folder and rename it with the appropriate prefix.
