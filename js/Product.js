﻿var Product = function (args) {
    this.Controls = args.Controls;
    var screenwidth;
};

Product.prototype = {
    Init: function () {
        $j(".CataLogImage").show();
        $j(".ProductVideo").hide();
        this.ManageStoreLocatorPhoneLinkByDevice();
        var index = 0;
        screenwidth = $j(window).width();
        var queryString = window.location.search.substring(1);
        var parms = queryString.split('&');

        for (var i = 0; i < parms.length; i++) {
            var pos = parms[i].indexOf('=');
            if (pos > 0) {
                var key = parms[i].substring(0, pos);
                var val = parms[i].substring(pos + 1);
                if (val.toLowerCase() == 'review') {
                    if (screenwidth > 768) {
                        index = $j('#Tab li a').index($j('a[href="#tabsCustomerReviews"]').get(0));
                    }
                    break;
                }
                else {
                    if (screenwidth > 768) {
                        index = $j('#Tab li a').index($j('a[href="#tabsFeatures"]').get(0));
                    }
                }
            }
        }

        if (screenwidth < 768) {
            $j(".tabStructure").remove();
            $j('.accordionStructure').click(function () {
                $j(this).next().toggle();
                $j(this).addClass("active");
                if ($j(this).next().css('display') == 'none') {
                    $j(this).removeClass("active");
                }
                return false;
            }).next().hide();
        }
        else {
            $j(".accordionStructure").hide();
            $j("#Tab").tabs({ active: index });
            $j(".ui-state-default").removeAttr('tabindex');
            $j(".ui-state-default a").removeAttr('tabindex');
        }
        //this.UpdateZoomImageUrl();
        this.ApplyLazyLoad();

        if (screenwidth < 1048) {
            $j("html, body").animate({ scrollTop: 0 }, "slow");
        }
    },
    OpenStoreAvailability: function () {
        $j("#" + this.Controls.inStoreAvailabilityListId).dialog({
            position: { my: "center", at: "center", of: window },
            resizable: false,
            modal: true,
            draggable: false,
            dialogClass: 'fixed-box',
            width: '510',
            height: '320'
        });
        $j("#" + this.Controls.inStoreAvailabilityListId).parent().addClass("CheckingStoreAvailabilty");
        //Remove href on desktop from Phone
        var that = this;
        setTimeout(function () {
            that.RemovePhoneLinkOnDesktop();
        }, 3500)

    },
    OpenImageZoomOverlay: function () {
        return false;
        /*
        var images;
        var largeImageSize = $j(this.Controls.hdnfLargeImagename).val();
        var smallImageSize = $j(this.Controls.hdnfSmallImagename).val();
        var altimages = document.getElementById('CatalogItemImage').src;
        if (largeImageSize != null && smallImageSize != null) {
            images = altimages.replace('/' + smallImageSize + '/', '/Original/');
        }
        else {
            images = altimages.replace('/385/', '/Original/');
        }

        $j("#" + this.Controls.divEnlargedImage).html("");
        var generateImgTag = "<img border='0' alt='MaximizedImage' src=" + images + " />";
        $j("#" + this.Controls.divEnlargedImage).append(generateImgTag);

        if (screenwidth > 767) {

            $j("#" + this.Controls.divEnlargedImage).dialog({
                position: { my: "left-287 top-100", at: "center", of: window },
                resizable: false,
                modal: true,
                width: 'auto'
            });
        }
        $j("#ui-dialog-title-dialog").hide();
        $j(".ui-dialog-titlebar").removeClass('ui-widget-header');
        $j("#" + this.Controls.divEnlargedImage).parent().addClass("ImageZoomPopup");
        */
    },
    UpdateMiniCart: function (cartItemCount, cartSubTotal) {
        if ($j.find('[id*="_lblCartItemCount"]') != null) {
            $j.find('[id*="_lblCartItemCount"]')[0].innerHTML = cartItemCount;
        }
        if ($j.find('[id*="_lblMobileCartItemCount"]') != null) {
            $j.find('[id*="_lblMobileCartItemCount"]')[0].innerHTML = cartItemCount;
        }
        $j.find('[id*="_lblCartSubTotal"]')[0].innerHTML = cartSubTotal;
        var miniCartFlyOut = new MiniCartFlyOut();
        miniCartFlyOut.ShowMiniCart();
        setTimeout(function () { miniCartFlyOut.HideMiniCart(); }, 2000);
    },
    OpenAffliatedUrlLink: function (url) {
        window.open(url, 'sharer', 'location=1,resizable=1,directories=1,menubar=1,scrollbars=1,toolbar=1,status=1,width=800,height=600');
        return true;
    },
    ShowVideoArena: function () {
        var src = $j("[id$='_ifrmYoutubeVideo']").attr('src');
        $j(".ProductVideo").dialog({
            position: { my: "center", at: "center", of: window },
            resizable: false,
            draggable: false,
            dialogClass: 'fixed-box',
            modal: true,
            width: 'auto',
            beforeClose: function (event, ui) {
                $j("[id$='_ifrmYoutubeVideo']").attr('src', src);
            },
        });
        $j(".ProductVideo").parent().addClass("ProductVideoPopup");
    },
    ShowSizeChartLink: function () {
        window.open($j(".szchartClass").attr("href"), "sizeChartWindow", "width=790,height=700,scrollbars=yes");
        return false;
    },
    ApplyZoom: function (largeImage) {
        //$j('#CatalogItemImage').addimagezoom({
        //    magnifiersize: [400, 400],
        //    magnifierpos: 'right',
        //    magvertcenter: true,
        //    cursorshade: true,
        //    largeimage: largeImage
        //});
    },
    UpdateZoomImageUrl: function () {
        var imagefilePath = $j("#CatalogItemImage").attr("src").replace('/385/', '/Original/');
        if ($j(window).width() > 768) {
            this.ApplyZoom(imagefilePath);
        }

    },
    UpdateAlternateImage: function (imageUrl) {
        $j("#CatalogItemImage").attr("src", imageUrl);
        this.UpdateZoomImageUrl();
    },
    ManageStoreLocatorPhoneLinkByDevice: function () {
        if ($j(window).width() >= 1024) {
            var contactControl = $j("[id$='_lblStorewiseInventory'] a");
            contactControl.removeAttr("href");
        }
    },
    ApplyLazyLoad: function () {
        $j(function () {
            $j("img.lazy").show().lazyload({ event: "scrollstop" });
        });
    },
    OpenTeamPricePopUp: function () {
        //alert("Called");
        $j("#divTeamPriceDescripion").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            dialogClass: 'fixed-box',
            title: 'Team Price?',
            width: 325
        });
        $j("#divTeamPriceDescripion").parent().addClass("GroupProuctPopup");
    },
    RemovePhoneLinkOnDesktop: function () {
        if ($j(window).width() >= 1024) {
            var contactControl = $j(".ProductDataTable #lblStorePhone");
            if (contactControl.length > 0) {
                for (var index = 0; index < contactControl.length; index++) {
                    var innerContent = contactControl[index].outerHTML;
                    var parentHTML = $j(contactControl[index]).parent().parent();
                    $j(parentHTML).html(innerContent);

                }
            }
        }
    }

};

document.cookie = 'znode' + escape('nothing');

function detect() {
    document.cookie = 'znodecookie';
    if (document.cookie.indexOf("znodecookie") < 0) {
        alert("you must enable cookies in your browser settings in order to add items to your cart.");
        return false;
    }
    else {
        return true;
    }
}


$j(document).ready(function () {
    // Configure/customize these variables.
    var showChar = 50; // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Show more >";
    var lesstext = "Show less";


    $j('.more').each(function () {
        var content = $j(this).html();

        if (content.length > showChar) {

            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);

            var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

            $j(this).html(html);
        }

    });

    $j(".morelink").click(function () {
        if ($j(this).hasClass("less")) {
            $j(this).removeClass("less");
            $j(this).html(moretext);
        } else {
            $j(this).addClass("less");
            $j(this).html(lesstext);
        }
        $j(this).parent().prev().toggle();
        $j(this).prev().toggle();
        return false;
    });

    InitializeZoom();


});


//var prm = Sys.WebForms.PageRequestManager.getInstance();
//prm.add_pageLoaded(CatProdListPageLoadedEventHandler);
//function CatProdListPageLoadedEventHandler() {
//    objProduct.UpdateZoomImageUrl();
//}


function InitializeZoom() {
    if ($j(window).width() > 768) {
        $j("#CatalogItemImage").elevateZoom({
            gallery: 'gallery_01',
            cursor: 'crosshair',
            galleryActiveClass: "active",
            zoomType: "inner",
            zoomWindowFadeIn: 500,
            zoomWindowFadeOut: 750
        });
    }
    else {
        $j(".ItemImg img").attr("onclick", "SetAltImage(this);");
    }
}

function SetAltImage(obj) {
    var imgurl = $j(obj).attr('src').replace('/55/', '/385/');
    $j("#CatalogItemImage").attr("src", imgurl);
}