﻿/******************************************************************
Add some common jQuery extensions
******************************************************************/
jQuery.extend(jQuery, {
    delegate: function (context, func, args) {
        // check to see if the there are any additional arguments and apply them if necessary
        if (args)
            return function () { return func.apply(context, args); }
        // if there is only one arguments, just invoke the method
        else
            return function () { return func.call(context); }
    }
});

jQuery.extend(jQuery.fn, {
    hasEvent: function (event) {
        var events = this.data("events");
        return (events && events[event]);
    }
});

jQuery.extend(jQuery.fn, {
    createElement: function (element, attributes) {
        var e = jQuery(document.createElement(element));

        jQuery.each(attributes, function (key, value) {
            jQuery(e).attr(key, value);
        });

        jQuery(this).append(e);
        return e;
    },
    hasAttr: function (attr) {
        return jQuery(this).attr(attr) !== undefined;
    },
    addOption: function (value, text, selected) {
        if (!jQuery(this).is("select"))
            return;

        var option = jQuery("<option></option>").val(value).html(text);

        if (selected)
            option.attr("selected", "selected");

        jQuery(this).append(option);
    },
    addText: function (text) {
        var currentText = jQuery(this).val();
        jQuery(this).val(currentText + text);
    },
    removeText: function (text) {
        var currentText = jQuery(this).val();
        jQuery(this).val(currentText.replace(text, ""));
    }
});

jQuery.extend(jQuery.fn, {
    computeViewport: function () {
        var viewport;

        if (typeof window.innerWidth != 'undefined')
            viewport = {
                width: window.innerWidth,
                height: window.innerHeight
            };
        else if (typeof document.documentElement != 'undefined'
		    && typeof document.documentElement.clientWidth != 'undefined'
		    && document.documentElement.clientWidth != 0)
            viewport = {
                width: document.documentElement.clientWidth,
                height: document.documentElement.clientHeight
            };
        else
            viewport = {
                width: document.body.clientWidth,
                height: document.body.clientHeight
            };

        var w = jQuery(window);
        viewport.top = w.scrollTop();
        viewport.left = w.scrollLeft();
        return viewport;
    },
    centerWithinViewport: function () {
        var element = jQuery(this);

        var width = element.width();
        var height = element.height();
        var viewport = jQuery.fn.computeViewport();

        if (width > viewport.width)
            width = viewport.width;
        if (height > viewport.height)
            height = viewport.height;
        element.css({
            position: 'absolute',
            left: (viewport.width - width) / 2 + viewport.left,
            top: (viewport.height - height) / 3 + viewport.top
        });
        return element;
    },
    positionWithinViewport: function (options) {
        var element = jQuery(this);

        var xPos = options.absoluteXPos;
        var yPos = options.absoluteYPos;
        var viewport = options.viewport;
        var speed = options.speed;
        var callback = options.callback;

        var elWidth = element.width();
        var elHeight = element.height();

        var withinViewport = true;

        if (viewport.length != 0) {
            var vOffset = viewport.offset();
            var vWidth = viewport.outerWidth();
            var vHeight = viewport.outerHeight();
            var vCoords = {
                top: vOffset.top,
                left: vOffset.left,
                bottom: vOffset.top + vHeight,
                right: vOffset.left + vWidth
            };

            var vBrowser = jQuery.fn.computeViewport();
            if ((vBrowser.top + vBrowser.height) < vCoords.bottom && vBrowser.height > (elHeight)) {
                vCoords.bottom = vBrowser.top + vBrowser.height;
            }
            //            // Redefine X-Coord if position we want is out of bounds
            //            if (xPos < vCoords.left || elWidth > vWidth) {
            //                xPos = vCoords.left;
            //                withinViewport = false;
            //            }
            //            else 
            if ((xPos + elWidth) > vCoords.right) {
                if ((vCoords.right - elWidth) > vCoords.left)
                    xPos = vCoords.right - elWidth;
                else
                    xPos = vCoords.left;
                withinViewport = false;
            }

            //            // Redefine Y-Coord if position we want is out of bounds
            //            if (yPos < vCoords.top || elHeight > vHeight) {
            //                yPos = vCoords.top;
            //                withinViewport = false;
            //            }
            //            else 
            if ((yPos + elHeight) > vCoords.bottom) {
                if ((vCoords.bottom - elHeight) > vCoords.top)
                    yPos = vCoords.bottom - elHeight;
                else
                    yPos = vCoords.top;
                withinViewport = false;
            }
        }

        element.css("left", xPos).css("top", yPos);

        if (speed != null)
            element.slideDown(speed, callback);
        else
            element.show();
        return (withinViewport);
    }
});
