﻿var Specials = function () {
}

Specials.prototype = {
    Init: function () {
        new SlideItMoo({
            overallContainer: 'SlideItMoo_outer',
            elementScrolled: 'SlideItMoo_inner',
            thumbsContainer: 'SlideItMoo_items',
            itemsVisible: 5,
            elemsSlide: 1,
            duration: 500,
            itemsSelector: '.SlideItMoo_element',
            showControls: 1
        });
    }
};