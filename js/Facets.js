﻿var Facets = function (args) {
};
Facets.prototype = {
    Init: function () {
        maketreefacets('FacetTreeMenu');
        this.SlideDownSelectedPanel();
    },
    SlideDownSelectedPanel: function () {
        var selectedTags = $j('.SelectedTagText');
        $j.each(selectedTags, function (key, value) {
            var selectedTagName = $j(value).html();
            $j('ul[data-facetname="' + selectedTagName + '"]').attr("style", "display:block");
        });
    },
    ShowFacets: function () {
        $j("#overlaypopup").addClass("PopupOverlay");
        $j(".MobilePopwarpper").fadeIn();
        $j(".PopUpTitle").attr("style", "display:block");
        $j("#popclose").attr("style", "display:block");
    },
    ClosePopup: function () {
        $j(".filter").attr("style", "display:Inline-block");
        $j("#overlaypopup").removeClass("PopupOverlay");
        $j(".PopUpTitle").attr("style", "display:none");
        $j("#popclose").attr("style", "display:none");
        $j(".MobilePopwarpper").fadeOut();
    }
};

$j(document).ready(function () {
    $j(".symbol-open").remove();

    // var isMobile =window. matchMedia("only screen and (max-width: 767px)");
    var allPanels = $j(".facetName").next("ul").attr('style', 'display:none;');

    //Changes for Show Facet on Popup in mobile view
    $j("#popclose").attr("style", "display:none");
    $j(".filter").attr("style", "display:none");
    $j(".PopUpTitle").attr("style", "display:none");

    if ($j(window).width() < 768) {
        //Section to write js for mobile device
        //Collapse all panel
        allPanels.slideUp();
        $j(".MobilePopwarpper").fadeOut();
        $j(".filter").attr("style", "display:Inline-block");

    }
    else {

        //On Desktop Slide Down first two panel. set the class active
        //$j($j(allPanels)[0]).prev('span').addClass('active');
        //$j($j(allPanels)[0]).attr('style', 'display:block;');

        //$j($j(allPanels)[1]).prev('span').addClass('active');
        //$j($j(allPanels)[1]).attr('style', 'display:block;');
        //debugger;

        //Toggle all facet navigation on desktop
        allPanels.slideDown();
        $j("#FacetTreeMenu span").addClass('active');
        $j("#FacetTreeMenu ul").attr("style", "display:block");

        if (typeof objFacets != 'undefined') {
            objFacets.SlideDownSelectedPanel();
        }
    }

    $j(".facetName").click(function () {
        if ($j(window).width() < 768) {
            //Section to write js for mobile device
            allPanels.slideUp();
            if ($j(this).next("ul").attr('style').indexOf('block') == -1) {
                $j(this).next("ul").slideDown();
                $j('.facetName').removeClass('active');
                $j(this).addClass('active');
                $j(this).next("ul").attr('style', "display:block");
            }
            else if ($j(this).next("ul").attr('style').indexOf('block') != -1) {
                $j(this).removeClass('active');
                $j(this).next("ul").attr('style', "display:none");
            }
        }
        else {
            if ($j(this).next("ul").attr('style').indexOf('block') == -1) {
                $j(this).next("ul").slideDown();
                $j(this).addClass('active');
                $j(this).next("ul").attr('style', "display:block");
            }
            else if ($j(this).next("ul").attr('style').indexOf('block') != -1) {
                $j(this).next("ul").slideUp();
                $j(this).removeClass('active');
                $j(this).next("ul").attr('style', "display:none");
            }
        }
    });

});

