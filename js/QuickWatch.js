﻿function ActiveTabChanged(sender, e) {
    PageLoadedEventHandler();
}

function detect() {
    document.cookie = 'ZNodeCookie';
    if (document.cookie.indexOf("ZNodeCookie") < 0) {
        alert("You must enable cookies in your browser settings in order to add items to your cart.");
        return false;
    }
    else {
        return true;
    }
}