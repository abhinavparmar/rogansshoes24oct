﻿var shoppingCartItems;
window.addEvents({
    'domready': function () {
        /* thumbnails example , div containers */
        shoppingCartItems = new SlideItMoo({
            overallContainer: 'NavigationCart_outer',
            elementScrolled: 'NavigationCart_inner',
            thumbsContainer: 'NavigationCart_Items',
            itemsVisible: 1,
            elemsSlide: 1,
            duration: 300,
            itemsSelector: '.NavigationCartItem',
            itemWidth: 130,
            showControls: 1
        });
    }
});