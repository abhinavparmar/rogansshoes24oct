﻿var CatalogImage = function ()
{
    var catalogImageItems;
}
CatalogImage.prototype = {
    Init: function () {
        catalogImageItems = new SlideItMoo({
            overallContainer: 'CatalogImage_outer',
            elementScrolled: 'CatalogImage_inner',
            thumbsContainer: 'CatalogImage_items',
            itemsVisible: 1,
            elemsSlide: 1,
            duration: 500,
            itemsSelector: '.CatalogImage_element',
            showControls: 1
        });
    }
};