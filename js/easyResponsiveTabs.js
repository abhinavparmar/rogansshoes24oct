// Easy Responsive Tabs Plugin
// Author: Samson.Onna <Email : samson3d@gmail.com>
(function (jQuery) {
    jQuery.fn.extend({
        easyResponsiveTabs: function (options) {
            //Set the default values, use comma to separate the settings, example:
            var defaults = {
                type: 'default', //default, vertical, accordion;
                width: 'auto',
                fit: true,
                closed: false,
                activate: function(){}
            }
            //Variables
            var options = jQuery.extend(defaults, options);            
            var opt = options, jtype = opt.type, jfit = opt.fit, jwidth = opt.width, vtabs = 'vertical', accord = 'accordion';
            var hash = window.location.hash;
            var historyApi = !!(window.history && history.replaceState);
            
            //Events
            jQuery(this).bind('tabactivate', function(e, currentTab) {
                if(typeof options.activate === 'function') {
                    options.activate.call(currentTab, e)
                }
            });

            //Main function
            this.each(function () {
                var jQueryrespTabs = jQuery(this);
                var jQueryrespTabsList = jQueryrespTabs.find('ul.resp-tabs-list');
                var respTabsId = jQueryrespTabs.attr('id');
                jQueryrespTabs.find('ul.resp-tabs-list li').addClass('resp-tab-item');
                jQueryrespTabs.css({
                    'display': 'block',
                    'width': jwidth
                });

                jQueryrespTabs.find('.resp-tabs-container > div').addClass('resp-tab-content');
                jtab_options();
                //Properties Function
                function jtab_options() {
                    if (jtype == vtabs) {
                        jQueryrespTabs.addClass('resp-vtabs');
                    }
                    if (jfit == true) {
                        jQueryrespTabs.css({ width: '100%', margin: '0px' });
                    }
                    if (jtype == accord) {
                        jQueryrespTabs.addClass('resp-easy-accordion');
                        jQueryrespTabs.find('.resp-tabs-list').css('display', 'none');
                    }
                }

                //Assigning the h2 markup to accordion title
                var jQuerytabItemh2;
                jQueryrespTabs.find('.resp-tab-content').before("<h2 class='resp-accordion' role='tab'><span class='resp-arrow'></span></h2>");

                var itemCount = 0;
                jQueryrespTabs.find('.resp-accordion').each(function () {
                    jQuerytabItemh2 = jQuery(this);
                    var jQuerytabItem = jQueryrespTabs.find('.resp-tab-item:eq(' + itemCount + ')');
                    var jQueryaccItem = jQueryrespTabs.find('.resp-accordion:eq(' + itemCount + ')');
                    jQueryaccItem.append(jQuerytabItem.html());
                    jQueryaccItem.data(jQuerytabItem.data());
                    jQuerytabItemh2.attr('aria-controls', 'tab_item-' + (itemCount));
                    itemCount++;
                });

                //Assigning the 'aria-controls' to Tab items
                var count = 0,
                    jQuerytabContent;
                jQueryrespTabs.find('.resp-tab-item').each(function () {
                    jQuerytabItem = jQuery(this);
                    jQuerytabItem.attr('aria-controls', 'tab_item-' + (count));
                    jQuerytabItem.attr('role', 'tab');

                    //Assigning the 'aria-labelledby' attr to tab-content
                    var tabcount = 0;
                    jQueryrespTabs.find('.resp-tab-content').each(function () {
                        jQuerytabContent = jQuery(this);
                        jQuerytabContent.attr('aria-labelledby', 'tab_item-' + (tabcount));
                        tabcount++;
                    });
                    count++;
                });
                
                // Show correct content area
                var tabNum = 0;
                if(hash!='') {
                    var matches = hash.match(new RegExp(respTabsId+"([0-9]+)"));
                    if (matches!==null && matches.length===2) {
                        tabNum = parseInt(matches[1],10)-1;
                        if (tabNum > count) {
                            tabNum = 0;
                        }
                    }
                }

                //Active correct tab
                jQuery(jQueryrespTabs.find('.resp-tab-item')[tabNum]).addClass('resp-tab-active');

                //keep closed if option = 'closed' or option is 'accordion' and the element is in accordion mode
                if(options.closed !== true && !(options.closed === 'accordion' && !jQueryrespTabsList.is(':visible')) && !(options.closed === 'tabs' && jQueryrespTabsList.is(':visible'))) {                  
                    jQuery(jQueryrespTabs.find('.resp-accordion')[tabNum]).addClass('resp-tab-active');
                    jQuery(jQueryrespTabs.find('.resp-tab-content')[tabNum]).addClass('resp-tab-content-active').attr('style', 'display:block');
                }
                //assign proper classes for when tabs mode is activated before making a selection in accordion mode
                else {
                    jQuery(jQueryrespTabs.find('.resp-tab-content')[tabNum]).addClass('resp-tab-content-active resp-accordion-closed')
                }

                //Tab Click action function
                jQueryrespTabs.find("[role=tab]").each(function () {
                   
                    var jQuerycurrentTab = jQuery(this);
                    jQuerycurrentTab.click(function () {
                        
                        var jQuerycurrentTab = jQuery(this);
                        var jQuerytabAria = jQuerycurrentTab.attr('aria-controls');

                        if (jQuerycurrentTab.hasClass('resp-accordion') && jQuerycurrentTab.hasClass('resp-tab-active')) {
                            jQueryrespTabs.find('.resp-tab-content-active').slideUp('', function () { jQuery(this).addClass('resp-accordion-closed'); });
                            jQuerycurrentTab.removeClass('resp-tab-active');
                            return false;
                        }
                        if (!jQuerycurrentTab.hasClass('resp-tab-active') && jQuerycurrentTab.hasClass('resp-accordion')) {
                            jQueryrespTabs.find('.resp-tab-active').removeClass('resp-tab-active');
                            jQueryrespTabs.find('.resp-tab-content-active').slideUp().removeClass('resp-tab-content-active resp-accordion-closed');
                            jQueryrespTabs.find("[aria-controls=" + jQuerytabAria + "]").addClass('resp-tab-active');

                            jQueryrespTabs.find('.resp-tab-content[aria-labelledby = ' + jQuerytabAria + ']').slideDown().addClass('resp-tab-content-active');
                        } else {
                            jQueryrespTabs.find('.resp-tab-active').removeClass('resp-tab-active');
                            jQueryrespTabs.find('.resp-tab-content-active').removeAttr('style').removeClass('resp-tab-content-active').removeClass('resp-accordion-closed');
                            jQueryrespTabs.find("[aria-controls=" + jQuerytabAria + "]").addClass('resp-tab-active');
                            jQueryrespTabs.find('.resp-tab-content[aria-labelledby = ' + jQuerytabAria + ']').addClass('resp-tab-content-active').attr('style', 'display:block');
                        }
                        //Trigger tab activation event
                        jQuerycurrentTab.trigger('tabactivate', jQuerycurrentTab);
                        
                        //Update Browser History
                        if(historyApi) {
                            var currentHash = window.location.hash;
                            var newHash = respTabsId+(parseInt(jQuerytabAria.substring(9),10)+1).toString();
                            if (currentHash!="") {
                                var re = new RegExp(respTabsId+"[0-9]+");
                                if (currentHash.match(re)!=null) {                                    
                                    newHash = currentHash.replace(re,newHash);
                                }
                                else {
                                    newHash = currentHash+"|"+newHash;
                                }
                            }
                            else {
                                newHash = '#'+newHash;
                            }
                            
                            history.replaceState(null,null,newHash);
                        }
                    });
                    
                });
                
                //Window resize function                   
                jQuery(window).resize(function () {
                    jQueryrespTabs.find('.resp-accordion-closed').removeAttr('style');
                });
            });
        }
    });
})(jQuery);

jQuery(document).ready(function () {
    jQuery('#horizontalTab').easyResponsiveTabs({
        type: 'default', //Types: default, vertical, accordion           
        width: 'auto', //auto or any width like 600px
        fit: true,   // 100% fit in a container
        closed: 'accordion', // Start closed if in accordion view
        activate: function (event) { // Callback function if tab is switched
            var jQuerytab = jQuery(this);
            var jQueryinfo = jQuery('#tabInfo');
            var jQueryname = jQuery('span', jQueryinfo);

            jQueryname.text(jQuerytab.text());

            jQueryinfo.show();
        }
    });

    jQuery('#verticalTab').easyResponsiveTabs({
        type: 'vertical',
        width: 'auto',
        fit: true
    });
});

