﻿var Payments = function (args) {
    this.Controls = args.Controls;
    this.Messages = args.Messages;
};
Payments.prototype = {
    Init: function () {
        this.LoadInitialCardSetting();
    },
    AddCreditCardPanel: function () {
        var divInd = 0;
        this.ManageCardTypeImage();
        var maxTenderInd = $j("#" + this.Controls.hdnMoreCreditCardCount).val();
        for (divInd = 1; divInd <= maxTenderInd; divInd++) {
            var isAdded = false;
            var cardDiv = $j("#divMoreCreditCard" + divInd);
            if (!cardDiv.is(":visible")) {
                cardDiv.show();
                var hiddenCntrl = cardDiv.find("input[type='hidden']");
                if (hiddenCntrl != "") {
                    hiddenCntrl.val("1");
                }
                if (divInd == 1 && !$j("#divMoreCreditCard" + (divInd + 1)).is(":visible")) {
                    var defaultControl = $j("#" + this.Controls.txtDefaultCCTotal);
                    $j("#" + this.Controls.txtDefaultCCTotal).show();
                    $j("#divDefaultCardTotal").show();
                    defaultControl.val($j("#Total").text().replace('$', ''));
                }
                isAdded = true;
            }
            if (isAdded) {
                if (divInd == $j("#" + this.Controls.hdnMoreCreditCardCount).val()) {
                    $j("#addMoreCardLnk").hide();
                }
                break;
            }
        }
        this.ManageOrderTotalWithCrditCardCount();
    },
    RemoveCreditCardPanel: function (currIndex) {
        $j("#addMoreCardLnk").show();
        if (currIndex <= $j("#" + this.Controls.hdnMoreCreditCardCount).val()) {
            var cardDiv = $j("#divMoreCreditCard" + currIndex);
            if (cardDiv.is(":visible")) {
                this.ResetValues(currIndex);
                cardDiv.hide()
                var hiddenCntrl = cardDiv.find("input[type='hidden']");
                if (hiddenCntrl != "") {
                    hiddenCntrl.val("0");
                }
            }
        }
        this.ManageOrderTotalWithCrditCardCount();
    },
    ManageOrderTotalWithCrditCardCount: function () {
        var creditCardPanelCnt = 0;
        var maxTenderInd = $j("#" + this.Controls.hdnMoreCreditCardCount).val();
        for (var divInd = 1; divInd <= maxTenderInd ; divInd++) {
            var cardDiv = $j("#divMoreCreditCard" + divInd);
            if (cardDiv.is(":visible")) {
                creditCardPanelCnt++;
            }
        }
        if (creditCardPanelCnt < 1) {
            $j("#" + this.Controls.txtDefaultCCTotal).hide();
            $j("#divDefaultCardTotal").hide();
            $j("#lblMultiCardErr").hide();
            $j("#lblCardAmtTotalErr").hide();
        }
        if (maxTenderInd.toString() == creditCardPanelCnt) {
            $j("#addMoreCardLnk").hide();
        }

    },
    ManageCreditCardAmount: function (currIndex, event) {
        var isVisble = 0;
        var totalEnableCardCnt = 0;
        var ordTotal = $j("#Total").text().replace('$', '');
        var allCardTotalInput = $j(".TenderTotal");
        var CurrControl = event.target.id;
        var cardTotal = 0;
        for (var ind = 0; ind <= allCardTotalInput.length - 1 ; ind++) {
            var cardValue = $j(allCardTotalInput)[ind].value;
            if ($j(allCardTotalInput[ind]).is(":visible")) {
                totalEnableCardCnt++;
            }
        }
        if (totalEnableCardCnt > 0 && totalEnableCardCnt <= 2) {
            $j("#lblCardAmtTotalErr").hide();
            var targetTotal = event.target.value;
            targetTotal = parseFloat(targetTotal.toString()).toFixed(2);
            for (var ind = 0; ind <= totalEnableCardCnt ; ind++) {
                var cardValue = $j(allCardTotalInput)[ind].value;
                if ($j(allCardTotalInput[ind]).is(":visible") && CurrControl.indexOf($j(allCardTotalInput)[ind].id) < 0) {
                    //if ((parseFloat(ordTotal).toFixed(2)) > (parseFloat(targetTotal).toFixed(2))) {
                    var regexp = /^\-1?$|^([0-9])*[.]?[0-9]{0,2}$/;
                    if (Math.round(ordTotal) > Math.round(targetTotal) && regexp.test(targetTotal)) {
                        $j(allCardTotalInput[ind]).val(parseFloat((ordTotal - targetTotal).toString()).toFixed(2))
                    }
                    break;
                }
            }
        }
        else {
            /*Show Manage Count Message if there are more than 1 Additional Card*/
            this.ValidateCardTotalAmount();

        }
    },
    LoadInitialCardSetting: function () {
        var extrCardCount = 0;
        if ($j("#hdnMoreCreditCardCount") != typeof ("undefined")) {
            for (var ind = 1; ind <= $j("#hdnMoreCreditCardCount").val() ; ind++) {
                var hiddenCntrl = $j("#divMoreCreditCard" + ind).find("input[type='hidden']");
                if (!$j("#divMoreCreditCard" + ind).is(":visible") && hiddenCntrl != "" && hiddenCntrl.val() == "0") {
                    $j("#divMoreCreditCard" + ind).hide();
                    hiddenCntrl.val("0");
                    this.ResetValues(ind);
                }
                else {
                    hiddenCntrl.val("1");
                    extrCardCount++;
                    $j("#divMoreCreditCard" + ind).show();
                }
            }
            if (extrCardCount > 0) {
                $j("#divDefaultCardTotal").show();
                $j("#addMoreCardLnk").show();
                $j("#txtDefaultCCTotal").show();
                if (extrCardCount.toString() === $j("#" + this.Controls.hdnMoreCreditCardCount).val()) {
                    $j("#addMoreCardLnk").hide();
                }
            }
            else {
                $j("#addMoreCardLnk").show();
                $j("#txtDefaultCCTotal").hide();
                $j("#divDefaultCardTotal").hide();
            }
            

        }
    },
    ValidateCardForExpiry: function (sender, args) {
        var currParentCntrl = $j("#" + sender.controltovalidate).parent();
        var currCCDiv = $j(currParentCntrl).attr("id");
        var lblMessage = "";
        var lblMessage2 = "";
        var currCardContrlIndex = 0;
        if (args.Value > 12) {
            currCardContrlIndex = currCCDiv.replace("divCCYear", "")
        }
        else {
            currCardContrlIndex = currCCDiv.replace("divCCMonth", "")
        }
        if (currCardContrlIndex <= $j("#" + this.Controls.hdnMoreCreditCardCount).val()) {
            var selYear = $j("#divCCYear" + currCardContrlIndex).find("select").val();
            var selMonth = $j("#divCCMonth" + currCardContrlIndex).find("select").val();
            lblMessage = $j("#lblCardExpiry" + currCardContrlIndex);
            lblMessage2 = lblMessage.find("#ltrExpiryMessage");
            if (selYear != "0" && selMonth != "0") {
                var currentTime = new Date();
                if (selYear == currentTime.getFullYear() && currentTime.getMonth() + 1 > selMonth) {
                    args.IsValid = false;
                    lblMessage.show();
                    lblMessage2.show()
                }
                else {
                    args.IsValid = true;
                    lblMessage.hide();
                    lblMessage2.hide();
                }
            }
            else {
                lblMessage.hide();
                lblMessage2.hide();
            }
        }
        else {
            args.IsValid = true;
        }
    },
    ValidateAllCardNumber: function (sender, args) {
        /*Validate Card Numbers*/
        if (this.IsMultipleCardPayment()) {
            var allCardInput = $j(".CreditCardNumber");
            $j("#lblMultiCardErr").hide();
            var cardNumList = "";
            var isInvalid = false;
            for (var ind = 0; ind <= allCardInput.length - 1 ; ind++) {
                if ($j(allCardInput[ind]).find("input").is(":visible") && $j(allCardInput[ind]).find("input").val() != "") {
                    var cardNum = $j(allCardInput[ind]).find("input").val();
                    if (cardNum != "") {
                        if (cardNumList.indexOf(cardNum) < 0) {
                            cardNumList = cardNumList + cardNum + ",";
                        }
                        else {
                            isInvalid = true;
                            break;
                        }
                    }
                }
            }
        }
        /*Validate Card Total*/
        var isInTotalInvalid = this.ValidateCardTotalAmount();
        if (isInvalid || isInTotalInvalid) {
            args.IsValid = false;
            if (!isInTotalInvalid) {
                $j("#lblMultiCardErr").show();
            }
        }
    },
    ResetValues: function (currDivIndex) {
        var cardDiv = $j("#divMoreCreditCard" + currDivIndex);
        //if (!cardDiv.is(":visible")) {
        cardDiv.find(".CCError").hide();
        var allTextInput = cardDiv.find("input[type = text]");
        /*Clear Card Num Text Box*/
        if (allTextInput != "") {
            for (var ind = 0; ind <= allTextInput.length - 1; ind++) {
                if ($j(allTextInput[ind]).hasClass("TenderTotal")) {
                    $j(allTextInput[ind]).val("0.00")
                }
                else {
                    allTextInput[ind].value = ""
                }
            }
        }
        ind = 0;
        var selControl = cardDiv.find("select");
        if (selControl != "") {
            for (var ind = 0; ind <= selControl.length - 1; ind++) {
                $j(selControl[ind]).val("0")
            }
        }

        //}
    },
    ValidateCardTotalAmount: function () {
        var isInvalid = false;
        if (this.IsMultipleCardPayment()) {
            var ordTotal = $j("#Total").text().replace('$', '');
            var allCardTotalInput = $j(".TenderTotal");
            var cardTotal = 0;
            for (var ind = 0; ind <= allCardTotalInput.length - 1 ; ind++) {
                var cardValue = $j(allCardTotalInput)[ind].value;
                if ($j(allCardTotalInput[ind]).is(":visible") && cardValue != 0.00 && cardValue > 0) {
                    var cardValue = $j(allCardTotalInput)[ind].value;
                    cardTotal = parseFloat(cardValue) + parseFloat(cardTotal);//parseFloat(cardValue.toString()).toFixed(2) + parseFloat(cardTotal.toString()).toFixed(2);
                    cardTotal = parseFloat(cardTotal).toFixed(2);
                }
            }
            if (parseFloat(cardTotal).toFixed(2) === parseFloat(ordTotal).toFixed(2)) {
                $j("#lblCardAmtTotalErr").text();
                $j("#lblCardAmtTotalErr").hide();
            }
            else {
                isInvalid = true;
                $j("#lblCardAmtTotalErr").text();
                $j("#lblCardAmtTotalErr").text(this.Messages.OrderTotalErrMesg);
                $j("#lblCardAmtTotalErr").show();

            }
        }

        return isInvalid;
    },
    IsMultipleCardPayment: function () {
        var isSel = false;
        for (divInd = 1; divInd <= $j("#" + this.Controls.hdnMoreCreditCardCount).val() ; divInd++) {
            var cardDiv = $j("#divMoreCreditCard" + divInd);
            if (cardDiv.is(":visible")) {
                isSel = true;
            }
        }
        return isSel;
    },
    ManageCardTypeImage: function () {
        var allCardImgDiv = $j(".CreditCardImg");
        if (allCardImgDiv.length > 1) {
            var cardListCnt = allCardImgDiv.length;
            if ($j(".CreditCardImg #imgAmex") != typeof ("undefined") && $j(".CreditCardImg #imgAmex").length < cardListCnt) {
                $j(".CreditCardImg #imgAmex").hide();
            }
            if ($j(".CreditCardImg #imgVisa") != typeof ("undefined") && $j(".CreditCardImg #imgVisa").length < cardListCnt) {
                $j(".CreditCardImg #imgVisa").hide();
            }
            if ($j(".CreditCardImg #imgMastercard") != typeof ("undefined") && $j(".CreditCardImg #imgMastercard").length < cardListCnt) {
                $j(".CreditCardImg #imgMastercard").hide();
            }
            if ($j(".CreditCardImg #imgDiscover") != typeof ("undefined") && $j(".CreditCardImg #imgDiscover").length < cardListCnt) {
                $j(".CreditCardImg #imgDiscover").hide();
            }
        }
    }

}