﻿var Account = function (args) {
    this.Controls = args.Controls;
    this.Messages = args.Messages;
}
Account.prototype = {
    Init: function () {
        this.ManagePageByDevice();
    },
    accountPrmPageLoadedEventHandler: function (productID, productName) {
        this.InitailaizeValue();
        $j("#" + this.Controls.lblProductName).text(productName);

        if (productID != undefined || productID != null || productID != "") {
            $j("#" + this.Controls.hdnProductID).val(productID);
        }
        else {
            $j("#" + this.Controls.hdnProductID).val("0");
        }
        $j("#emailDialog").dialog({
            modal: true,
            draggable: false,
            dialogClass: 'fixed-box',
            resizable: false
        });
        return false;
    },
    SetHiddenFiledValue: function () {
        if (Page_ClientValidate('SendWishListEmial')) {
            var emailId = $j("#" + this.Controls.txtEmailId).val();
            var friendName = $j("#" + this.Controls.txtFirstName).val();

            if (this.checkName(friendName) && this.checkRegexp(emailId)) {
                if (emailId != undefined && friendName != undefined || emailId != null && friendName != null) {
                    $j("#" + this.Controls.hdnName).val(friendName);
                    $j("#" + this.Controls.hdnEmailAddress).val(emailId);
                }
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    },
    checkName: function (friendName) {
        if (friendName != undefined && friendName != "") {
            return true;
        }
        $j("#" + this.Controls.lblErrors).text(this.Messages.NameRequiredErrorMessage);
        $j("#" + this.Controls.txtFirstName).focus();
        return false;
    },
    checkRegexp: function (emailID) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (regex.test(emailID)) {
            return true;
        }
        $j("#" + this.Controls.lblErrors).text(this.Messages.EmailValidationErrorMessage);
        $j("#" + this.Controls.txtEmailId).focus();
        return false;
    },
    InitailaizeValue: function () {
        $j("#" + this.Controls.txtEmailId).val("");
        $j("#" + this.Controls.txtFirstName).val("");
        $j("#" + this.Controls.hdnProductID).val("");
        $j("#" + this.Controls.hdnName).val("");
        $j("#" + this.Controls.hdnEmailAddress).val("");
        $j("#" + this.Controls.lblErrors).text("");
    },
    ManagePageByDevice: function () {
        if ($j(window).width() <= 767) {
            //Collapse all panel
            //alert("Mobile Device");
            maketreefacets('AccountSectionTree');
            this.SlidePageSection();
        }
    },
    SlidePageSection: function () {
        $j(".symbol-open").remove();
        var allPanels = $j(".AccountItem").next("ul").attr('style', 'display:none;');
        $j($j(allPanels)[0]).prev('span').addClass('active');
        $j($j(allPanels)[0]).attr('style', 'display:block;');
        $j(".AccountItem").click(function () {
            allPanels.slideUp();
            $j(".AccountItem").removeClass("active");
            if ($j(this).next("ul").attr('style').indexOf('block') == -1) {
                $j(this).next("ul").slideDown();
                $j(this).addClass('active');
            }
        });

    }
}



