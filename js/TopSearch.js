﻿var TopSearch = function (args) {
    this.Controls = args.Controls;
    this.Messages = args.Messages;
};

TopSearch.prototype = {
    Init: function () {
        $j("#" + this.Controls.SearchText).addClass('WaterMark').on("focus", $j.proxy(this.SearchFocus, this)).on("blur", $j.proxy(this.SearchBlur, this));
        this.SetToggalCategoryInMobileView();
        this.HideSiteWideBanner();
        $j("#SearchText").focus();
    },
    SearchFocus: function () {
        if ($j("#" + this.Controls.SearchText).val() == $j("#" + this.Controls.SearchText).attr('title')) {
            $j("#" + this.Controls.SearchText).val("").addClass('TextBoxPosition').removeClass("WaterMark");
        }
    },
    SearchBlur: function () {
        if ($j.trim($j("#" + this.Controls.SearchText).val()) == "") {
            var title = $j("#" + this.Controls.SearchText).attr('title');
            $j("#" + this.Controls.SearchText).val(title).removeClass("TextBoxPosition").addClass('WaterMark');
        }
    },
    SetToggalCategoryInMobileView: function () {
        if ($j(window).width() < 768) {
            $j(".SearchKeys").slideUp();
            $j(".Title").addClass("active");
        }
    },
    ToggalCategory: function () {
        if ($j(window).width() < 768) {
            if ($j(".SearchKeys").css("display") != "block") {
                $j(".SearchKeys").slideDown();
                $j('.Title').removeClass('active');
            }
            else if ($j(".SearchKeys").css("display") == "block") {
                $j(".SearchKeys").slideUp();
                $j(".Title").addClass('active');
            }
        }
        else {
            if ($j(".SearchKeys").css("display") != "block") {
                $j(".SearchKeys").slideDown();
                $j(".Title").removeClass('active');
            }
            else if ($j(".SearchKeys").css("display") == "block") {
                $j(".SearchKeys").slideUp();
                $j(".Title").addClass('active');
            }
        }
    },
    btnSearch_Click: function () {
        var searchText = $j("#" + this.Controls.SearchText).val();
        if (searchText == 'Search by SKU# or Keyword') {
            searchText = "";
        }
        var url = "/searchengine.aspx?category=" + this.Messages.CategoryName + "&text=" + encodeURIComponent(searchText);
        window.location = url;
        return false;
    },
    HideSiteWideBanner: function () {
        var bannerDivHTML = $j("#divSiteWideBanner").text();
        if (bannerDivHTML == null || bannerDivHTML == '' || bannerDivHTML.length <= 0) {
            $j("#divSiteWideBanner").hide();
        }
        else {
            $j("#divSiteWideBanner").show();
        }
    }
};


var SignupNewsLetter = function (args) {
    this.Controls = args.Controls;
};

SignupNewsLetter.prototype = {
    Init: function () {
        $j("#" + this.Controls.txtSignUpFirstName).on("focus", $j.proxy(this.FirstNameFocus, this)).on("blur", $j.proxy(this.FirstNameBlur, this));
        $j("#" + this.Controls.txtNewsLetter).on("focus", $j.proxy(this.NewsLetterFocus, this)).on("blur", $j.proxy(this.NewsLetterBlur, this));
    },
    FirstNameFocus: function () {
        this.OnFocus($j("#" + this.Controls.txtSignUpFirstName));
    },
    FirstNameBlur: function () {
        this.OnBlur($j("#" + this.Controls.txtSignUpFirstName))
    },
    NewsLetterFocus: function () {
        this.OnFocus($j("#" + this.Controls.txtNewsLetter));
    },
    NewsLetterBlur: function () {
        this.OnBlur($j("#" + this.Controls.txtNewsLetter));
    },
    ClearDefaultText: function () {
        if (email.val() == "Email") {
            email.val("");
        }
    },
    OnBlur: function (ctrl) {
        if ($j.trim(ctrl.val()) == "") {
            ctrl.val(ctrl.attr('title')).removeClass("TextBoxPosition").addClass('WaterMark');
        }
    },
    OnFocus: function (ctrl) {
        if (ctrl.val() == ctrl.attr('title')) {
            ctrl.val("").addClass('TextBoxPosition').removeClass("WaterMark");
        }
    },
    lnkNewsLetter_Click: function () {
        if (this.ValidateControl()) {
            window.location = "/emailsignup.aspx?firstname=" + $j("#" + this.Controls.txtSignUpFirstName).val() + "&email=" + $j("#" + this.Controls.txtNewsLetter).val();
            return false;
        }
        else {
            return false;
        }
    },
    ValidateControl: function () {
        var firstName = $j("#" + this.Controls.txtSignUpFirstName).val();
        if (firstName != '' && firstName != 'First Name') {
            var email = $j("#" + this.Controls.txtNewsLetter).val();
            if (email != '' && email != 'Email') {
                var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
                if (filter.test(email)) {
                    return true;
                }
                else {
                    $j('#email').html("Please use a valid email address").show();
                }
            }
            else {
                $j('#email').html("Email ID is required").show();
            }
        }
        else {
            $j('#FirstNameErr').html("Enter First Name").show();
        }
        return false;
    },
    HideErrorMessage: function (id) {
        $j("#" + id).hide();
    }
};

$j(document).ready(function () {

    var loginButtontext = $j(".LogButton").text();
    if (loginButtontext.toLowerCase() == "logout") {
        $j(".LogButton").addClass('LogOutButton');
    }

    var windowWidth = $j(window).width();
    if ($j(window).width() < 961) {

        //Toggle Shop Now On Page Load - Start
        $j(".sec1 h4").next("ul").slideToggle();
        $j(".sec1 h4").toggleClass('active');

        $j(".sec1 h2").next("ul").slideToggle();
        $j(".sec1 h2").toggleClass('active');
        //Toggle Shop Now On Page Load - End

        $j('.FooterMenu h4').click(function () {
            $j(this).next("ul").slideToggle();
            $j(this).toggleClass('active');
        });

        $j('.FooterMenu h2').click(function () {
            $j(this).next("ul").slideToggle();
            $j(this).toggleClass('active');
        });
    }

    if ($j(window).width() < 768 && $j(window).width() > 480) {
        $j('.SearchButton a').click(function () {
            $j(this).parent('.SearchButton').next(".SearchSection").toggleClass('active');
            $j(this).parent('.SearchButton').next(".SearchSection").slideToggle();
            if ($j(this).parent('.SearchButton').next(".SearchSection").hasClass('active')) {
                $j('#Container').css('margin-top', '74px');
                $j('#Container').css('transition', 'margin ease-out .5s');
                $j(".SearchButton a em").css('display', 'none')
                $j(".SearchButton a span.fa.fa-search").css({ 'font-size': '34px', 'color': '#b4111a' });
            } else {
                $j('#Container').css('margin-top', 'inherit');
                $j('#Container').css('transition', 'margin ease-out .5s');
                $j(".SearchButton a").css('text-indent', 'none');
                $j(".SearchButton a em").css('display', 'inline-block')
                $j(".SearchButton a span.fa.fa-search").css({ 'font-size': '28px', 'color': '#1b537b' });
            }
        });
    }

    if ($j(window).width() < 480) {
        $j('.SearchButton a').click(function () {
            $j(this).parent('.SearchButton').next(".SearchSection").toggleClass('active');
            $j(this).parent('.SearchButton').next(".SearchSection").slideToggle();
            if ($j(this).parent('.SearchButton').next(".SearchSection").hasClass('active')) {
                $j('#Container').css('margin-top', '74px');
                $j('#Container').css('transition', 'margin ease-out .5s');
                $j(".SearchButton a em").css('display', 'none')
                $j(".SearchButton a span.fa.fa-search").css({ 'font-size': '34px', 'color': '#b4111a' });
            } else {
                $j('#Container').css('margin-top', 'inherit');
                $j('#Container').css('transition', 'margin ease-out .5s');
                $j(".SearchButton a").css('text-indent', 'none');
                $j(".SearchButton a em").css('display', 'block')
                $j(".SearchButton a span.fa.fa-search").css({ 'font-size': '28px', 'color': '#1b537b' });
            }
        });
    }

    if ($j(window).width() >= 1024) {
        var contactControl = $j(".CustomerCarePhone");
        if (contactControl.length > 0) {
            for (var index = 0; index < contactControl.length; index++) {
                var innerContent = contactControl[index].innerHTML;
                contactControl[index].parentNode.innerHTML = innerContent;
            }
        }
    }

    //Set CSS on COMODO LINK FOR IE
    var isIEBrowser = IsBrowserIE();
    if (isIEBrowser === true) {
        $j(".ComodoLogo").addClass("IESpecific");
    }


});

function PopUpCaptchaWhatsThisWindow() {
    var popupWin = window.open('CaptchaHelp.html', 'EIN', 'scrollbars,resizable,width=650,height=450,left=50,top=50');
    popupWin.focus();

}

function IsBrowserIE() {
    var ua = window.navigator.userAgent;
    var isIE = false;
    var msie = ua.indexOf("MSIE ");
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        isIE = true;
    }
    return isIE;
}