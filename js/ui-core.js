﻿$j = jQuery.noConflict();

Website = {};
Website.UI = {};

/******************************************************************
Wire up all of our initializing events for the entire document
******************************************************************/
$j(document).ready(function () {
    Website.UI.DataViews.Initialize($j(this));
    Website.UI.Tabs.Initialize($j(this));
    Website.UI.CollapsiblePanels.Initialize($j(this));
});

/******************************************************************
DataViews (ListView / GridView)
******************************************************************/
Website.UI.DataViews = {};
Website.UI.DataViews.GridView = {};
Website.UI.DataViews.ListView = {};
Website.UI.DataViews.Initialize = function (container) {
    Website.UI.DataViews.GridView.Initialize(container);
    Website.UI.DataViews.ListView.Initialize(container);
};
Website.UI.DataViews.GridView.Initialize = function (container) {
    // Zebra-stripe the tables.
    $j("table.grid-view > tbody > tr:odd:not(.row-header, .row, .row-footer, .row-pager, .row-history)").addClass("row");
    $j("table.grid-view > tbody > tr:even:not(.row-header, .row, .row-footer, .row-pager, .row-history)").addClass("row row-alt");

    $j("table.dxgvTable > tbody > tr:odd:not(.row-header, .row, .row-footer, .row-pager, .row-history)").addClass("row");
    $j("table.dxgvTable > tbody > tr:even:not(.row-header, .row, .row-footer, .row-pager, .row-history)").addClass("row row-alt");
};
Website.UI.DataViews.ListView.Initialize = function (container) {

    var lists = $j(container).find(".list-view");

    // Zebra-stripe the list
    lists.find(".list-item:odd").addClass("list-item-alt");

};

/******************************************************************
Tabs
******************************************************************/
Website.UI.Tabs = {};
Website.UI.Tabs.Abort = function () {
    $j(".tabs").abort();
};
Website.UI.Tabs.Initialize = function (container) {

    $j(container).find(".tabs > ul").addClass("clearwrap");
    $j(container).find(".tabs > div").addClass("tab-body");

    if ($j(container).find(".tabs").html() != null) {
        $j(container).find(".tabs").tabs().css("visibility", "visible");
    }
    $j(container).find(".tabs .selected").click();

};

/******************************************************************
Collapsible panels
******************************************************************/
Website.UI.CollapsiblePanels = {};
Website.UI.CollapsiblePanels.Initialize = function (container) {

    // Wire up any collapsible panels with the appropriate class names
    $j(container).find("div.collapsible > h3, div.collapsible > h4, div.collapsible > h5, div.collapsible > h6").addClass("collapsible-header").prepend("<span class='arrow'>&nbsp;</span>").next().addClass("collapsible-content").hide();

    // When the user clicks on the header, show the panel
    $j(container).find("div.collapsible > h3, div.collapsible > h4, div.collapsible > h5, div.collapsible > h6").die("click").click(function () {
        $j(this).toggleClass("visible").next().slideToggle(100);
    });

    // Show any default panels
    $j(container).find("div.collapsible.selected > h3, div.collapsible.selected > h4, div.collapsible.selected > h5, div.collapsible.selected > h6").click();

    $j("div.collapsible").show();
};

