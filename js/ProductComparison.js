﻿var ProductComaparsion = function (args) {
    this.Controls = args.Controls;
    this.Messages = args.Messages;
};
ProductComaparsion.prototype = {
    Init: function () {
        $j("#" + this.Controls.lblErrors).text("");
        $j("#" + this.Controls.txtEmailFrom).val("");
        $j("#" + this.Controls.txtEmailTo).val("");
        this.ManagePageByDevice();
    },

    ManagePageByDevice: function () {
        //var isMobile = window.matchMedia("only screen and (max-width: 360px)");
        if ($j(window).width() <= 360) {
            dialogwidth = 280;
        }
        else {
            dialogwidth = 325;
        }
    },
    ProdCompPageLoadedEventHandler: function () {
        $j("#" + this.Controls.lblErrors).text("");
        $j("#" + this.Controls.txtEmailFrom).val("");
        $j("#" + this.Controls.txtEmailTo).val("");
        $j("#" + this.Controls.lblMessages).text("");
        $j("#divCompareEmailToFriend").dialog({
            modal: true,
            resizable: false,
            draggable: false,
            dialogClass: 'fixed-box',
            title: 'Send To Friend',
            width: dialogwidth
        });

        $j(".EmailFriendSection").parent().addClass("AddToCompareFailedPopUp")
        return false;
    },
    SendMailToFriend: function () {
        $j("#" + this.Controls.lblErrors).text("");

        var emailFrom = $j("#" + this.Controls.txtEmailFrom).val();
        var emailTo = $j("#" + this.Controls.txtEmailTo).val();

        if (emailFrom == "" || emailTo == "") {
            $j("#" + this.Controls.lblErrors).text(this.Messages.RequiredFieldsErrorMessage);
            return false;
        }

        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!regex.test(emailFrom)) {
            $j("#" + this.Controls.lblErrors).text(this.Messages.EmailValidationErrorMessage);
            $j("#" + this.Controls.txtEmailFrom).focus();
            return false;
        }

        if (!regex.test(emailTo)) {
            $j("#" + this.Controls.lblErrors).text(this.Messages.EmailValidationErrorMessage);
            $j("#" + this.Controls.txtEmailTo).focus();
            return false;
        }

        $j("#" + this.Controls.hdnEmailFrom).val($j("#" + this.Controls.txtEmailFrom).val());
        $j("#" + this.Controls.hdnEmailTo).val($j("#" + this.Controls.txtEmailTo).val());

        return true;
    }

};
