﻿var BestSellers = function (args) {
    var bestSellers;
}

BestSellers.prototype = {
    Init: function () {
        bestSellers = new SlideItMoo({
            overallContainer: 'BestSellers_outer',
            elementScrolled: 'BestSellers_inner',
            thumbsContainer: 'BestSellers_Items',
            itemsVisible: 2,
            elemsSlide: 1,
            duration: 300,
            itemsSelector: '.BestSellerItem',
            showControls: 1
        });
    }
};