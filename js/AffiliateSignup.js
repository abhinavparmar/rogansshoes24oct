﻿var AffiliateSignup = function (args) {
    this.Controls = args.Controls;
}

AffiliateSignup.prototype = {
    Init: function () { },
    EnableSignupButton: function () {
        var chkAccept = $j(this.Controls.chkAccept);
        $j(this.Controls.btnSignup).disabled = !(chkAccept.checked);
    }
};