﻿var _isset = 0;
window.onbeforeunload = closeIt;

function closeIt(event) {
    if (_isset == 0) {
        if (navigator.userAgent.indexOf("Firefox") != -1) {
            event.returnValue = "NOTE: All items will be lost unless you click on [ADD TO CART] first";
            event.preventDefault();
        }
        else {
            window.event.returnValue = "NOTE: All items will be lost unless you click on [ADD TO CART] first";
        }
    }
    else if (_isset == -1) {
        _isset = 0;
    }
}

function Reset(event) {
    _isset = -1;
}