﻿var isOkForAddToCart = false;

var AttributeHelper = function (args) {
    this.Controls = args.Controls;
    this.Messages = args.Messages;
};

AttributeHelper.prototype = {
    Init: function () {
        var table = $j('#table-data')[0];
        var c = 1010;
        $j(table).delegate('.tr_clone_add', 'click', function () {
            $j('.error').remove();
            var thisRow = $j(this).closest('tr')[0];
            $j(thisRow).clone().attr('id', 'id' + (++c)).insertAfter(thisRow).find('input:text').val('');
        });

        $j(table).delegate('.tr_clone_remove', 'click', function () {
            $j('.error').remove();
            $j(this).closest('tr').remove();
        });

        $j('.tr_clone_remove').hide();

        for (var i = 0; i < 2; i++) {
            this.CreateClone();
            $j('.tr_clone_remove').hide();
        }

        this.ResetAll();

    },
    GetSelectedData: function () {
        //debugger;
        $j('.error').remove();
        var skuLists = new Array();
        var table = $j("table tbody");
        var gridRows = $j("#table-data  tr");
        if (null != gridRows && gridRows.length > 0) {
            var gridRowIndex = 0;
            for (gridRowIndex = 1; gridRowIndex < gridRows.length; gridRowIndex++) {
                var txtQty = $j(gridRows[gridRowIndex]).find('input[id$="txtQty"]');
                if (null != txtQty && typeof txtQty.val() != 'undefined' && txtQty.val() != "") {


                    var attributeddl = $j(gridRows[gridRowIndex]).find("select");
                    var selectedAttributes = "";
                    var selectedAttributeDescription = "";
                    var selectedAttributeName = "";
                    if (attributeddl != null && attributeddl.length > 0) {
                        for (var i = 0; i < attributeddl.length; i++) {
                            var attributeddlID = attributeddl[i].id;

                            //get attribute name from drop down attribute
                            selectedAttributeName = $j(gridRows[gridRowIndex]).find("#" + attributeddlID).attr("name");
                            //alert($j(gridRows[gridRowIndex]).find("#" + attributeddlID).find('option:selected').val());
                            if ((attributeddl.length - 1) == i) {
                                //get selected attribute value
                                selectedAttributes = selectedAttributes + $j(gridRows[gridRowIndex]).find("#" + attributeddlID).find('option:selected').val();
                                //create attribute description using attibute name and text
                                selectedAttributeDescription = selectedAttributeDescription + selectedAttributeName + "-" + $j(gridRows[gridRowIndex]).find("#" + attributeddlID).find('option:selected').text();
                            }
                            else {
                                //get selected attribute value ',' at the end
                                selectedAttributes = selectedAttributes + $j(gridRows[gridRowIndex]).find("#" + attributeddlID).find('option:selected').val() + ",";
                                //create attribute description using attibute name and text and append ',' at the end
                                selectedAttributeDescription = selectedAttributeDescription + selectedAttributeName + "-" + $j(gridRows[gridRowIndex]).find("#" + attributeddlID).find('option:selected').text() + "<br/>";
                            }
                        }
                    }

                    var isValid = this.IsOkForAddToCart(selectedAttributes);
                    if (isValid) {
                        var comment = $j(gridRows[gridRowIndex]).find('input[id$="txtComment"]');

                        var objSkuDetail = new Object();
                        objSkuDetail.SelectedAttributes = selectedAttributes;
                        objSkuDetail.SelectedAttributeDescription = selectedAttributeDescription;
                        objSkuDetail.Quantity = parseInt(txtQty.val().toString());
                        objSkuDetail.ProductID = productID;
                        objSkuDetail.Comment = comment.val();
                        objSkuDetail.RowNumber = gridRowIndex;//Solve Message Issue
                        skuLists.push(objSkuDetail);
                    }

                }
            }
            if (skuLists != null && skuLists.length > 0) {
                var jsonString = JSON.stringify(skuLists);
                return jsonString;
            }
            else {
                return "";
            }
        }

    },
    AddToCart: function () {
        //debugger;
        var requesthandler = $j.proxy(this.HandleAddToCartSuccessResponse, this);
        var jsonString = this.GetSelectedData();
        $j('.error').remove();
        if (jsonString != "") {
            $j.ajax({
                type: "POST",
                url: "/zeonasynchelper.asmx/ProcessInstockItemsAddToCart",
                cache: true,
                //dataType: "json",
                data: { "attributelist": jsonString },
                success: requesthandler,
                error: function (xhr, options, error) {
                    //debugger;
                    alert(error);
                }
            });
        }
        else {
            //alert('Please select appropriate attributes');

            $j(".ErrorMessage").dialog({
                modal: true,
                draggable: false,
                resizable: false,
                dialogClass: 'fixed-box',
                title: 'Error Message',
                width: 325
            });
            $j(".ErrorMessage").parent().addClass("GroupProuctPopup");
        }
    },
    LoadNextLevelAttributeValues: function (object) {
        //var jsonString = GetSelectedAttributeValueOnChange();
        $j('.error').remove();
        var currentAttributeTypeID = $j(object).attr("data-typeid");
        var currentAttributeDisplayOrder = $j(object).attr("data-display-order");
        var selectedAttributeValue = "";
        var select = $j(object).closest('tr').find('select');
        var skuLists = new Array();
        var nextAttributeTypeID = 0;
        var nextAttributeId;
        if (select != null && select.length > 0) {
            for (var i = 0; i < select.length; i++) {
                var ddlID = select[i].id;
                var displayOrder = $j(object).closest('tr').find("#" + ddlID).attr("data-display-order");
                if (displayOrder == currentAttributeDisplayOrder || displayOrder < currentAttributeDisplayOrder) {
                    if (selectedAttributeValue == "") {
                        selectedAttributeValue = selectedAttributeValue + $j(object).closest('tr').find("#" + ddlID).find('option:selected').val();
                    }
                    else {
                        selectedAttributeValue = selectedAttributeValue + "," + $j(object).closest('tr').find("#" + ddlID).find('option:selected').val();
                    }
                }
                else if (displayOrder > currentAttributeDisplayOrder && nextAttributeTypeID == 0) {
                    nextAttributeTypeID = $j(object).closest('tr').find("#" + ddlID).attr("data-typeid");
                    nextAttributeId = $j(object).closest('tr').find("#" + ddlID).attr("id");
                }
                else {
                    $j(object).closest('tr').find("#" + ddlID).prop('selectedIndex', 0);
                }
            }


            var objSkuDetail = new Object();
            objSkuDetail.SelectedAttributes = selectedAttributeValue;
            objSkuDetail.ProductID = productID
            objSkuDetail.CurrentAttributeTypeId = nextAttributeTypeID;
            skuLists.push(objSkuDetail);

            var jsonString = JSON.stringify(skuLists);
            $j.ajax({
                type: "POST",
                url: "/zeonasynchelper.asmx/GetNextLevelAttribute",
                cache: true,
                datatype: "html",
                data: { "attributelist": jsonString },
                success: function (Result) {
                    var list = $j(Result).text();
                    if (list.length > 0) {
                        //alert(list);
                        var nextattributeDropDown = $j(object).closest('tr').find("#" + nextAttributeId);
                        nextattributeDropDown.empty();
                        var mySplitResult = list.split(",");
                        //alert(mySplitResult.length);
                        // if (mySplitResult.length > 1) {
                        nextattributeDropDown.append("<option id=\"opt\"  value=\"0\">-Select-</option>");
                        // }
                        for (i = 0; i < mySplitResult.length; i++) {
                            var attributeIdandValues = mySplitResult[i].split(':');

                            nextattributeDropDown.append($j("<option id=\"opt_" + attributeIdandValues[1] + "\" value=" + attributeIdandValues[0] + ">" + attributeIdandValues[1] + "</option>"));
                        }
                    }
                },
                error: function (xhr, options, error) {
                    //debugger;
                    alert(error);
                }
            });
        }
    },
    ValidateAttribute: function (object) {
        $j('.error').remove();
        //debugger;
        var isFirstAttrSelected = true;
        var selectedTr = $j(object).closest('tr');
        var $jtr = $j(object).closest('tr');
        var myRow = $jtr.index();
        //alert(myRow);
        var select = $j(object).closest('tr').find('select');
        var selectedAttributeValue = "";
        var skuLists = new Array();
        if (select != null && select.length > 0) {
            for (var i = 0; i < select.length; i++) {
                var ddlID = select[i].id;
                if ($j(object).closest('tr').find("#" + ddlID).find('option:selected').val() == "0") {
                    isFirstAttrSelected = false;
                    break;
                }
                if ((select.length - 1) == i) {
                    selectedAttributeValue = selectedAttributeValue + $j(object).closest('tr').find("#" + ddlID).find('option:selected').val();
                }
                else {
                    selectedAttributeValue = selectedAttributeValue + $j(object).closest('tr').find("#" + ddlID).find('option:selected').val() + ",";
                }
            }
            var objSkuDetail = new Object();
            objSkuDetail.SelectedAttributes = selectedAttributeValue;
            objSkuDetail.ProductID = productID;
            skuLists.push(objSkuDetail);

            var jsonString = JSON.stringify(skuLists);
            var message = "<tr id=\"emptytry\" class=\"alert-box error\"><td colspan=\"6\" style='color:red;'>" + this.Messages.SKUNotAvailableMessage + "</td></tr>";
            if (isFirstAttrSelected) {
                $j.ajax({
                    type: "POST",
                    url: "/zeonasynchelper.asmx/ValidateAttribute",
                    cache: true,
                    //dataType: "json",
                    data: { "attributelist": jsonString },
                    success: function (data) {
                        //debugger
                        data = $j(data).text();
                        if (parseInt(data, 10) == 0) {
                            var thisRow = $j(object).closest('tr');
                            $j(thisRow).after(message.toString());
                        }
                        else {
                            $j(thisRow).closest('tr').find(".error").remove();
                            //$j(thisRow).closest('tr').attr("sku", data);
                        }
                    },
                    error: function (xhr, options, error) {
                        //alert(error);
                    }
                });
            }
            else {
                var thisRow = $j(object).closest('tr');
                $j(thisRow).after(message.toString());
            }
        }
    },
    CreateClone: function () {
        //debugger
        $j('.error').remove();
        var c = 1010;
        var cloneRow = $j('.tr_clone')[0];
        var thisRow = $j('.tr_clone').last('tr')[0];
        $j(cloneRow).clone().attr('id', 'id' + (++c)).insertAfter(thisRow).find('[id$="txtQty"]').val('1');
        $j('.tr_clone').last('tr').find('.tr_clone_remove').show();
        $j('.tr_clone').last('tr').find('[id$="txtComment"]').val('');
        var ddlList = $j('.tr_clone').last('tr').find("select");
        //if (ddlList != null && ddlList.length > 0) {
        //    for (var i = 0; i < ddlList.length; i++) {
        //        if (ddlList[i].childElementCount == 1) {
        //            ddlList[i].append("<option id=\"opt\"  value=\"0\">-Select-</option>");
        //        }
        //    }
        //}
    },
    IsOkForAddToCart: function (selectedAttributeValue) {
        var isValid = true;
        var arraySelectedAttribute = selectedAttributeValue.split(',');
        for (var attributeloop = 0; attributeloop < arraySelectedAttribute.length; attributeloop++) {
            if (arraySelectedAttribute[attributeloop] != null && parseInt(arraySelectedAttribute[attributeloop], 10) == 0) {
                var value = parseInt(arraySelectedAttribute[attributeloop], 10);
                isValid = false;
                break;
            }
        }
        return isValid;
    },
    ResetAll: function () {
        $j('.error').remove();
        var skuLists = new Array();
        var table = $j("table tbody");
        var gridRows = $j("#table-data  tr");
        if (null != gridRows && gridRows.length > 0) {
            var gridRowIndex = 0;
            for (gridRowIndex = 1; gridRowIndex < gridRows.length; gridRowIndex++) {
                var attributeddl = $j(gridRows[gridRowIndex]).find("select");
                for (var i = 0; i < attributeddl.length; i++) {
                    var attributeddlID = attributeddl[i].id;
                    $j(gridRows[gridRowIndex]).find("#" + attributeddlID).prop('selectedIndex', 0);
                }
            }
        }
    },
    HandleAddToCartSuccessResponse: function (data) {
        //alert(data);
        $j('.error').remove();
        var isAllItemAddedTocart = "true";
        var result = $j(data).text();
        var gridRows = $j("#table-data  tr");
        var responseArray = result.split(',');
        if (result != '') {
            //Chech if Response Contains  False Value
            if (result.indexOf("false") >= 0) {
                isAllItemAddedTocart = "false";
            }
            if (null != gridRows && gridRows.length > 0) {
                var gridRowIndex = 0;
                for (gridRowIndex = 1; gridRowIndex < gridRows.length; gridRowIndex++) {
                    var resStr = responseArray[gridRowIndex - 1];
                    if (resStr != undefined && resStr != '') {
                        if (resStr.indexOf("|") > 0) {
                            var resMsg = responseArray[gridRowIndex - 1].split('|');
                            var rowNum = resMsg[0];
                            var statusMessage = resMsg[1];
                            if (statusMessage == 'false') {
                                var errMessage = "<tr id=\"emptytry\" class=\"alert-box error\"><td colspan=\"6\" style='color:red;'>" + this.Messages.ItemOutOfstockMessage + "</td></tr>";
                                $j(gridRows[rowNum]).after(errMessage.toString());
                            }
                            //else if (statusMessage == 'true') {
                            //    //$j(gridRows[gridRowIndex]).after("<tr id=\"emptytry\" class=\"alert-box error\"><td colspan=\"6\" style='color:green;'>Item added Successfully</td></tr>");
                            //    var successMessage = "<tr id=\"emptytry\" class=\"alert-box error\"><td colspan=\"6\" style='color:green;'>" + this.Messages.ItemInstockMessage + "</td></tr>";
                            //    $j(gridRows[rowNum]).after(successMessage.toString());
                            //}
                        }
                        else {
                            if (responseArray[gridRowIndex - 1] == 'false') {
                                var errMessage = "<tr id=\"emptytry\" class=\"alert-box error\"><td colspan=\"6\" style='color:red;'>" + this.Messages.ItemOutOfstockMessage + "</td></tr>";
                                $j(gridRows[gridRowIndex]).after(errMessage.toString());
                            }
                            //else if (responseArray[gridRowIndex - 1] == 'true') {
                            //    //$j(gridRows[gridRowIndex]).after("<tr id=\"emptytry\" class=\"alert-box error\"><td colspan=\"6\" style='color:green;'>Item added Successfully</td></tr>");
                            //    var successMessage = "<tr id=\"emptytry\" class=\"alert-box error\"><td colspan=\"6\" style='color:green;'>" + this.Messages.ItemInstockMessage + "</td></tr>";
                            //    $j(gridRows[gridRowIndex]).after(successMessage.toString());
                            //}
                        }
                    }

                }
                if (isAllItemAddedTocart == "true") {
                    //this.UpdateShoppingCartHeader();
                    if (window.location.pathname.toLowerCase().indexOf("zquickview.aspx") >= 0) {
                        window.parent.quickViewHelper.CloseQuickWatch();
                    }
                    else {
                        var hostUrl = location.protocol + "//" + location.host + "/shoppingcart.aspx";
                        location.href = hostUrl;
                    }
                }
                this.UpdateShoppingCartHeader();
            }
        }
    },
    UpdateShoppingCartHeader: function () {
        $j.ajax({
            type: "POST",
            url: "/zeonasynchelper.asmx/GetShoppingCartItemCountandSubTotal",
            cache: true,
            success: function (data) {
                data = $j(data).text();
                var shoppingCartDataArray = data.split(',');
                if (shoppingCartDataArray != null && shoppingCartDataArray.length > 0) {
                    for (var shoppingCartItem = 0; shoppingCartItem < shoppingCartDataArray.length; shoppingCartItem++) {
                        if (shoppingCartDataArray[shoppingCartItem] != null) {
                            var keyValuePair = shoppingCartDataArray[shoppingCartItem].split(':');
                            if (keyValuePair != null && keyValuePair.length == 2) {
                                if (keyValuePair[0] == "ItemCount") {
                                    $j("span[id$='lblCartItemCount']").html(keyValuePair[1]);
                                }
                                else if (keyValuePair[0] == "SubTotal") {
                                    $j("span[id$='lblCartSubTotal']").html("$" + keyValuePair[1]);
                                }
                            }
                        }
                    }
                }
            },
            error: function (xhr, options, error) {
                //alert(error);
            }
        });
    },
    OpenHelpPopUp: function () {
        //alert("Called");
        $j("#divNameDescripion").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            dialogClass: 'fixed-box',
            title: 'Why Name?',
            width: 325
        });
        $j("#divNameDescripion").parent().addClass("GroupProuctPopup");
    },
    OpenQuickTeamPricePopUp: function () {
        //alert("Called");
        $j("#divTeamPriceDescripion").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            dialogClass: 'fixed-box',
            title: 'Why Name?',
            width: 325
        });
        $j("#divTeamPriceDescripion").parent().addClass("GroupProuctPopup");
    }
};

var ProductPrice = function (args) {
    this.Controls = args.Controls;
};
ProductPrice.prototype = {
    Init: function () {
        $j("#divTeamPriceDescripion").parent().removeClass("GroupProuctPopup");
    },
    OpenQuickTeamPricePopUp: function () {
        //alert("Called");
        $j("#divTeamPriceDescripion").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            dialogClass: 'fixed-box',
            title: 'Team Price',
            width: 325
        });
        $j("#divTeamPriceDescripion").parent().addClass("GroupProuctPopup");
    }
};
$j(document).ready(function () {
    $j(".numeric").keyup(function () {
        if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
            this.value = this.value.replace(/[^0-9\.]/g, '');
        }
        if (this.value <= 0 || this.value > 99999) {
            this.value = '1';
        }
    });
});

