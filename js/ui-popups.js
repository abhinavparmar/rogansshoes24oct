﻿Website.UI.Popups = {};

$j(document).ready(function () {
    Website.UI.Popups.Initialize($j(this));
});

Website.UI.Popups.Initialize = function (container) {
    $j(".tooltip a").click(function () {
        Website.UI.Popups.Create($j(this), { "content": $j(this).next(".content").html() });
    });
}

Website.UI.Popups.Create = function (caller, options) {

    var createPopup = function (params) {
        var baseClass = params.baseClass;

        var popup = $j(document.body).createElement("div", { "class": baseClass, "style": "display:none;" });
        var arrow = $j(popup).createElement("div", { "class": baseClass + "-arrow" });
        var top = $j(popup).createElement("div", { "class": baseClass + "-top" });
        var topOut = $j(top).createElement("div", { "class": "wrap-out" });
        var topIn = $j(topOut).createElement("div", { "class": "wrap-in" }).append("&nbsp;");
        var content = $j(popup).createElement("div", { "class": baseClass + "-content" });
        var contentOut = $j(content).createElement("div", { "class": "wrap-out" });
        var contentIn = $j(contentOut).createElement("div", { "class": "wrap-in" });
        var header = $j(contentIn).createElement("div", { "class": baseClass + "-header" });
        var headerClose = $j(header).createElement("a", { "class": "close", "href": "javascript:void(0);" });
        var headerCloseText = $j(headerClose).createElement("span", {}).append("Close");
        var loading = $j(contentIn).createElement("div", { "class": baseClass + "-loading" });
        var body = $j(contentIn).createElement("div", { "class": baseClass + "-body" });
        var bottom = $j(popup).createElement("div", { "class": baseClass + "-bottom" });
        var bottomOut = $j(bottom).createElement("div", { "class": "wrap-out" });
        var bottomIn = $j(bottomOut).createElement("div", { "class": "wrap-in" }).append("&nbsp;");

        if (params.onclosebutton)
            headerClose.click(params.onclosebutton);
        return $j(popup);

    };

    var initialize = function () {
        if (options == null)
            options = {};

        var params = {
            baseClass: "popup",
            draggable: false
        };

        Website.UI.Popups.Close();

        var popup = createPopup(params);

        popup.find("a.close").click(function () {
            Website.UI.Popups.Close();
        });
        
        if (options.absoluteXPos == undefined)
            options.absoluteXPos = caller.offset().left + caller.width() + 20;

        if (options.absoluteYPos == undefined)
            options.absoluteYPos = caller.offset().top - 15;

        if (options.relativeXPos != undefined)
            options.absoluteXPos += options.relativeXPos;

        if (options.relativeYPos != undefined)
            options.absoluteYPos += options.relativeYPos;

        if (Website.UI.Popups.IsShown())
            Website.UI.Popups.Close();

        Website.UI.Popups.RepositionPopup(popup, $j(".viewport"), options.absoluteXPos, options.absoluteYPos);

        if (options.content) {
            popup.find(".popup-body").html(options.content);
            popup.find(".popup-loading").hide();
        }
    };

    initialize();

};

Website.UI.Popups.RepositionPopup = function (popup, viewport, absoluteXPos, absoluteYPos) {
    popup.positionWithinViewport({
        absoluteXPos: absoluteXPos,
        absoluteYPos: absoluteYPos,
        viewport: viewport
    });

    popup.find(".popup-arrow").css("top", absoluteYPos - popup.position().top + 10);
};

Website.UI.Popups.IsShown = function () {
    return ($j(".popup").css("display") == 'block');
};

Website.UI.Popups.Close = function () {
    $j(".popup").remove();
};
