﻿<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/login.master"  AutoEventWireup="true" CodeBehind="forgotpassword.aspx.cs" Title="Site Administration" Inherits="WebApp.SiteAdmin.forgotpassword" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %> 
<%@ Register TagPrefix ="ZNode" TagName="forgotpassword" Src="~/SiteAdmin/Controls/Default/ForgotPassword.ascx" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Login"> 
        <div>
            <znode:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="10" runat="server" />
        </div> 
        <div>
            <ZNode:forgotpassword runat="server"></ZNode:forgotpassword>
        </div>
       
    </div>
</asp:Content>
