<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/Login.master" AutoEventWireup="true" Inherits="SiteAdmin.Admin_Default" Title="Site Administration" Codebehind="Default.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register TagPrefix="znode" Namespace="WebApp.CustomClasses.WebControls" Assembly="WebApp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Login" >
        <h1>
            Merchant Login</h1>
        <p>
            Log in to setup and manage your store.</p>
        <div>
            <znode:spacer id="Spacer2" spacerheight="20" spacerwidth="10" runat="server" />
        </div>    
         <asp:Panel ID="LoginPanel" runat="server" DefaultButton="LoginButton">   
        <div class="AccessDenied"><asp:Label ID="lblaccess" runat="server" Visible="False" Text="Access Denied. Please contact site administrator.<br /><br />" CssClass="Error"></asp:Label></div>
        <div class="FormView">
            <div class="FieldStyle" style="width:100px;">
                <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName"><b>Username</b></asp:Label>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="UserName" runat="server" autocomplete="off" Width="140px"></asp:TextBox><br />
                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                    ErrorMessage="Username is required." ToolTip="Username is required." ValidationGroup="uxLogin"
                    Display="Dynamic" CssClass="Error">Username is required.</asp:RequiredFieldValidator>
            </div>            
            <div class="FieldStyle" style="width:100px;">
                <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password"><b>Password</b></asp:Label>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="Password" runat="server" autocomplete="off" TextMode="Password" Width="140px"></asp:TextBox><br />
                <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                    ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="uxLogin"
                    Display="Dynamic" CssClass="Error">Password is required.</asp:RequiredFieldValidator>
            </div>
            <div class="ClearBoth">
            </div>
            <div class="Error" >
                <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer4" spacerheight="5" spacerwidth="10" runat="server" />
            </div>
            <div style="padding-left: 105px;height:45px;">
                <div>
               <zn:LinkButton ID="LoginButton" runat="server" CommandName="Login" 
                    OnClick="LoginButton_Click" ButtonType="LoginButton" ValidationGroup="uxLogin"
                    ButtonPriority="Normal" Text="Login" />

                </div>
                <div>
                    <div>
                        <div>
                            <znode:spacer id="Spacer5" spacerheight="15" spacerwidth="10" runat="server" />
                        </div>
                        <div>
                            <a id="forgotPasswordLink" href='' class="forgotpassword" runat="server">Forgot Password?</a></div>
                    </div>
                </div>
               
            </div>
        </div>
        </asp:Panel>
    </div>
</asp:Content>
