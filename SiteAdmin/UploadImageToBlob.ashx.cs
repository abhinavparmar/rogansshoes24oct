﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using ZNode.Libraries.Framework.Business;

namespace WebApp.SiteAdmin
{
    /// <summary>
    /// Summary description for UploadImageToBlob
    /// </summary>
    public class UploadImageToBlob : IHttpHandler
    {
        HttpContext globalcontext;
        public void ProcessRequest(HttpContext context)
        {
            globalcontext = context;
            context.Response.ContentType = "text/plain";
            string filename = context.Request.Form["filename"];
            string fName = string.Empty;
            
            try
            {
                //FileInfo fi = new FileInfo(context.Server.MapPath(Path.Combine(OriginalPath, filename)));
                //byte[] orgImage = File.ReadAllBytes(fi.FullName);
                FileInfo fi = new FileInfo(filename);
                byte[] orgImage = File.ReadAllBytes(fi.FullName);
                string path = GetPath(filename);
                fName = fi.Name;

                if ((orgImage != null) && (!ZNodeStorageManager.Exists(Path.Combine(path, fName))))
                {
                    ZNodeStorageManager.WriteBinaryStorage(orgImage, Path.Combine(path, fName));
                }

                context.Response.Write(string.Format("{0}:success", fName));
                context.Response.End();
            }
            catch (Exception ex)
            {
                context.Response.Write(string.Format("{0}:failure<br />{1}", fName, ex.ToString()));
                context.Response.End();
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public string GetPath(string fullFileName)
        {
            if ((fullFileName.Contains("\\data\\default\\images")) || (fullFileName.Contains("/data/default/images")))
                return OriginalImagePath;
            else if ((fullFileName.Contains("\\data\\default\\config")) || (fullFileName.Contains("/data/default/config")))
                return OriginalConfigPath;
            else if ((fullFileName.Contains("\\data\\default\\content")) || (fullFileName.Contains("/data/default/content")))
                return OriginalContentPath;

            return string.Empty;
        }

        public string OriginalImagePath
        {
            get {
                if (globalcontext.Cache["ZNodeConfigManager.EnvironmentConfig.OriginalImagePath"] == null)
                {
                    while (ZNodeConfigManager.EnvironmentConfig == null)
                        ZNodeConfigManager.SetSiteConfig();

                    globalcontext.Cache.Insert("ZNodeConfigManager.EnvironmentConfig.OriginalImagePath", ZNodeConfigManager.EnvironmentConfig.OriginalImagePath);
                }

                return globalcontext.Cache["ZNodeConfigManager.EnvironmentConfig.OriginalImagePath"].ToString();
            }
        }

        public string OriginalConfigPath
        {
            get
            {
                if (globalcontext.Cache["ZNodeConfigManager.EnvironmentConfig.ConfigPath"] == null)
                {
                    while (ZNodeConfigManager.EnvironmentConfig == null)
                        ZNodeConfigManager.SetSiteConfig();

                    globalcontext.Cache.Insert("ZNodeConfigManager.EnvironmentConfig.ConfigPath", ZNodeConfigManager.EnvironmentConfig.ConfigPath);
                }

                return globalcontext.Cache["ZNodeConfigManager.EnvironmentConfig.ConfigPath"].ToString();
            }
        }

        public string OriginalContentPath
        {
            get
            {
                if (globalcontext.Cache["ZNodeConfigManager.EnvironmentConfig.ContentPath"] == null)
                {
                    while (ZNodeConfigManager.EnvironmentConfig == null)
                        ZNodeConfigManager.SetSiteConfig();

                    globalcontext.Cache.Insert("ZNodeConfigManager.EnvironmentConfig.ContentPath", ZNodeConfigManager.EnvironmentConfig.ContentPath);
                }

                return globalcontext.Cache["ZNodeConfigManager.EnvironmentConfig.ContentPath"].ToString();
            }
        }
    }
}