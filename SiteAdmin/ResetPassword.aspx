﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/SiteAdmin/Themes/Standard/login.master"  CodeBehind="ResetPassword.aspx.cs" Inherits="WebApp.SiteAdmin.ResetPassword" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register TagPrefix="znode" Namespace="WebApp.CustomClasses.WebControls" Assembly="WebApp" %>
<%@ Register TagPrefix ="ZNode" TagName="ResetPassword" Src="~/SiteAdmin/Controls/Default/ResetPassword.ascx" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Login" style="margin-left: 15px"> 
        <div>
            <znode:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="10" runat="server" />
        </div> 
        <div>
            <ZNode:ResetPassword runat="server"></ZNode:ResetPassword>
        </div>
       
    </div>
</asp:Content>
