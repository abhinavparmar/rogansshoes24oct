﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace SiteAdmin.Controls.Default
{
    /// <summary>
    /// Represents a Manufacturer Auto Complete user control class
    /// </summary>
    public partial class ManufacturerAutoComplete : System.Web.UI.UserControl
    {
        #region Properties

        private int DropdownItemCount = int.Parse(ConfigurationManager.AppSettings["DropdownMaxItemCount"]);

        /// <summary>
        /// Gets or sets the Text
        /// </summary>
        public string Text
        {
            get
            {
                return txtManufacturer.Text;
            }

            set
            {
                txtManufacturer.Text = value;
            }
        }

        /// <summary>
        /// Gets or sets the Width
        /// </summary>
        public string Width
        {
            get
            {
                if (ddlManufacturer.Visible)
                {
                    return ddlManufacturer.Width.ToString();
                }

                return txtManufacturer.Width.ToString();
            }

            set
            {
                if (ddlManufacturer.Visible)
                {
                    ddlManufacturer.Width = new Unit(value);
                }
                else
                {
                    txtManufacturer.Width = new Unit(value);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value
        /// </summary>
        public string Value
        {
            get
            {
                if (ddlManufacturer.Visible)
                {
                    return ddlManufacturer.SelectedValue;
                }

                return hdnManufacturerId.Value;
            }

            set
            {
                if (ddlManufacturer.Visible)
                {
                    ListItem item = ddlManufacturer.Items.FindByValue(value);
                    if (item != null)
                    {
                        ddlManufacturer.SelectedValue = value;
                    }
                    else
                    {
                        ddlManufacturer.SelectedIndex = 0;
                    }
                }
                else
                {
                    hdnManufacturerId.Value = value;
                }
            }
        }

        /// <summary>
        /// Gets a TList of Manufacturer List Table 
        /// </summary>
        private TList<Manufacturer> ManufacturerListTable
        {
            get
            {
                ManufacturerService manufacturerService = new ManufacturerService();
                ManufacturerQuery query = new ManufacturerQuery();
                query.Append(ManufacturerColumn.ActiveInd, "TRUE");

                TList<Manufacturer> manufacturer = manufacturerService.Find(query.GetParameters());
                foreach (Manufacturer _Manufacture in manufacturer)
                {
                    _Manufacture.Name = Server.HtmlDecode(_Manufacture.Name);
                }

                return manufacturer;
            }
        }

        #endregion

        /// <summary>
        /// Page Init event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            ddlManufacturer.Visible = false;

            if (this.ManufacturerListTable.Count < this.DropdownItemCount && !this.Page.IsPostBack)
            {
                ddlManufacturer.Visible = true;
                txtManufacturer.Visible = false;
                ddlManufacturer.DataSource = UserStoreAccess.CheckStoreAccess(this.ManufacturerListTable, true);
                ddlManufacturer.DataTextField = "Name";
                ddlManufacturer.DataValueField = "ManufacturerID";
                ddlManufacturer.DataBind();

                ListItem allItem = new ListItem("Not Applicable", string.Empty);
                ddlManufacturer.Items.Insert(0, allItem);
            }
        }
    }
}