<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="SiteAdmin.Controls.Default.ResetPassword" CodeBehind="ResetPassword.ascx.cs" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/spacer.ascx" TagName="spacer" TagPrefix="ZNode" %>
<%@ Register TagPrefix="znode" Namespace="WebApp.CustomClasses.WebControls" Assembly="WebApp" %>

<div class="Form">
    <div class="FormView">
        <div class="PageTitle">
            <h1>
                <asp:Label ID="Localize8" Text="Reset Password" runat="server" /></h1>
        </div>
        <div>
            <znode:spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="10" runat="server"></znode:spacer>
        </div>
        <!-- Reset password Panel -->
        <asp:Panel ID="pnlResetpassword" runat="server">
            <div class="HintText">
                <asp:Label ID="lblIntroMessage" runat="server"></asp:Label>
            </div>
            <div>
                <znode:spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></znode:spacer>
            </div>
            <div>
                <znode:spacer SpacerWidth="4" SpacerHeight="10" ID="Spacer3" runat="server" />
            </div>
            <div class="Error">
                <asp:Literal ID="PasswordFailureText" runat="server" EnableViewState="false"></asp:Literal>
            </div>
            <div>
                <znode:spacer SpacerWidth="4" SpacerHeight="10" ID="Spacer4" runat="server" />
            </div>
            <div class="Row" runat="server" id="tblRowUserId">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="UserID" runat="server" Text="New User Name"></asp:Label>
                </div>
                <div class="ValueStyle">
                    <div>
                        <asp:TextBox ID="UserName" autocomplete="off" runat="server" Width="155px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="UserName"
                            ErrorMessage="User name is required." ToolTip="User name is required." ValidationGroup="ChangePassword1"
                            CssClass="Error" Display="Dynamic" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="UserName"
                            CssClass="Error" ToolTip="User Name is required." ValidationExpression="[a-zA-Z0-9_-]{8,15}"
                            ValidationGroup="ChangePassword1" />
                    </div>
                    <div class="HintText">
                        <asp:Label ID="Localize4" Text="8-15 characters" runat="server" />
                    </div>
                </div>
            </div>
            <div class="Row" runat="server" id="pnlCurrentpassword">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="CurrentPasswordLabel" runat="server" Text="Current Password"></asp:Label>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password" Width="155px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword"
                        ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="ChangePassword1"
                        CssClass="Error" Display="Dynamic" />
                </div>
            </div>
            <div class="Row">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="NewPasswordLabel" runat="server" Text="New Password"></asp:Label>
                </div>
                <div class="ValueStyle">
                    <div>
                        <asp:TextBox ID="NewPassword" runat="server" TextMode="Password" Width="155px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword"
                            ErrorMessage="New Password is required." ToolTip="New Password is required."
                            ValidationGroup="ChangePassword1" CssClass="Error" Display="Dynamic" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="NewPassword"
                            CssClass="Error" Display="Dynamic" ErrorMessage="Password must be at least 8 alphanumeric characters with at least one number."
                            SetFocusOnError="True" ToolTip="Password must be at least 8 alphanumeric characters and contain at least one number."
                            ValidationExpression="(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,15})" ValidationGroup="ChangePassword1" />
                    </div>
                    <div class="HintText">
                        <asp:Label ID="Localize11" Text="8-15 characters, alpha-numeric only, include at least one number"
                            runat="server" />
                    </div>
                </div>
            </div>
            <div class="Row">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="ConfirmNewPasswordLabel" runat="server" Text="Confirm New Password"></asp:Label>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password" Width="155px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword"
                        ErrorMessage="Confirm Password is required." ToolTip="Confirm Password is required."
                        ValidationGroup="ChangePassword1" CssClass="Error" Display="Dynamic" />
                    <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
                        ControlToValidate="ConfirmNewPassword" CssClass="Error" Display="Dynamic" Text="The confirmation password must match your new password."
                        ValidationGroup="ChangePassword1"></asp:CompareValidator>
                </div>
            </div>
            <div>
                <znode:spacer SpacerWidth="4" SpacerHeight="10" ID="Spacer7" runat="server" />
            </div>
            <div class="Row" runat="server" id="tblRowEmail" visible="false">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="EmailLabel" runat="server" Text="E-mail"></asp:Label>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="Email" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="Email"
                        ValidationGroup="ChangePassword1" ErrorMessage="Email ID is required" ToolTip="Email ID is required"
                        CssClass="Error" Display="Dynamic" />
                    <asp:RegularExpressionValidator ID="ValidateEmail" runat="server" ControlToValidate="Email"
                        ValidationGroup="ChangePassword1" CssClass="Error" ErrorMessage="*Please use a valid email address."
                        ToolTip="*Please use a valid email address." ValidationExpression="[\w\.-]+(\+[\w-]*)?@([\w-]+\.)+[\w-]+"
                        Display="Dynamic"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="Row" id="tblRowSecurityQuestion" runat="server">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="lblSecretQuestion" runat="server" Text="Security Question"></asp:Label>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlSecretQuestions" runat="server">
                        <asp:ListItem Enabled="true" Selected="True" Text="What is the name of your favorite pet?" />
                        <asp:ListItem Enabled="true" Text="In what city were you born?" />
                        <asp:ListItem Enabled="true" Text="What high school did you attend?" />
                        <asp:ListItem Enabled="true" Text="What is your favorite movie?" />
                        <asp:ListItem Enabled="true" Text="What is your mother's maiden name?" />
                        <asp:ListItem Enabled="true" Text="What was the make of your first car?" />
                        <asp:ListItem Enabled="true" Text="What is your favorite color?" />
                    </asp:DropDownList>
                </div>
            </div>
            <div class="Row" id="tblRowPasswordAnswer" runat="server">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="AnswerLabel" runat="server" Text="Security Answer"></asp:Label>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="Answer" Width="155px" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Answer"
                        ErrorMessage="Security answer is required." ToolTip="Security answer is required."
                        ValidationGroup="ChangePassword1" CssClass="Error" Display="Dynamic" />
                </div>
            </div>
            <div class="Row">
                <div class="FieldStyle LeftContent">
                    <znode:spacer SpacerWidth="15" SpacerHeight="1" ID="Spacer6" runat="server" />
                </div>
                <div class="ValueStyle">
                    <div class="ImageButtons">

                        <znode:LinkButton ID="btnSubmit" runat="server" ButtonType="LoginButton" ValidationGroup="ChangePassword1" OnClick="ResetPasswordPushButton_Click"
                            ButtonPriority="Normal" Text="Submit" />

                        &nbsp; &nbsp;  
 
                                     <znode:LinkButton ID="btnClear" runat="server" OnClick="BtnClear_Click" ButtonType="LoginButton"
                                         ButtonPriority="Normal" Text="Cancel" />

                    </div>
                </div>
            </div>
            <div>
                <znode:spacer SpacerWidth="15" SpacerHeight="30" ID="Spacer5" runat="server" />
            </div>
        </asp:Panel>
        <!-- Success message section -->
        <asp:Panel ID="pnlSuccess" runat="server" Visible="false">
            <div class="SuccessText">
                <asp:Label ID="Localize19" Text="Your password has been changed!" runat="server"></asp:Label>
            </div>
            <div>
                <znode:spacer SpacerWidth="15" SpacerHeight="25" ID="uxSpacer" runat="server" />
            </div>
            <div class="ContinueButton">
                <asp:LinkButton ID="ContinuePushButton" OnClick="ContinuePushButton_Click" runat="server"
                    CausesValidation="False" CommandName="Continue" CssClass="Button" Text="Continue" />
            </div>
            <div>
                <znode:spacer SpacerWidth="15" SpacerHeight="30" ID="uxSpacerBottom" runat="server" />
            </div>
            <div>
                <img src="~/SiteAdmin/Themes/images/clear.gif" runat="server" alt="" width="1" height="100" />
            </div>

        </asp:Panel>
        <div>
            <znode:spacer ID="Spacer19" SpacerHeight="30" SpacerWidth="10" runat="server"></znode:spacer>
        </div>
    </div>

</div>
