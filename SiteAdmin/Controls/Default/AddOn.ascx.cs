﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Controls.Default
{
    /// <summary>
    /// Represents the Addon User control class
    /// </summary>
    public partial class AddOn : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private int ItemId;
        private string Mode = string.Empty;
        private string AddProductAddOnLink = "~/SiteAdmin/Secure/Vendors/VendorProducts/AddAddons.aspx?";
        #endregion

        #region Public Methods

        /// <summary>
        /// Gets the name of the Addon for this AddonId
        /// </summary>
        /// <param name="addOnId">The value of Addon Id</param>
        /// <returns>Returns the AddonName</returns>
        public string GetAddOnName(object addOnId)
        {
            ProductAddOnAdmin AdminAccess = new ProductAddOnAdmin();
            ZNode.Libraries.DataAccess.Entities.AddOn _addOn = AdminAccess.GetByAddOnId(int.Parse(addOnId.ToString()));

            if (_addOn != null)
            {
                return _addOn.Name;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the title of the Addon for this AddonId
        /// </summary>
        /// <param name="addOnId">The value of Addon Id</param>
        /// <returns>Returns the Addon Title</returns>
        public string GetAddOnTitle(object addOnId)
        {
            ProductAddOnAdmin AdminAccess = new ProductAddOnAdmin();
            ZNode.Libraries.DataAccess.Entities.AddOn _addOn = AdminAccess.GetByAddOnId(int.Parse(addOnId.ToString()));

            if (_addOn != null)
            {
                return _addOn.Title;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the AddonValues for this AddonId
        /// </summary>
        /// <param name="addOnId">The value of Addon Id</param>
        /// <returns>Returns the Addon Values</returns>
        public string GetAddOnValues(object addOnId)
        {
            ProductAddOnAdmin AddOnValueAdmin = new ProductAddOnAdmin();
            TList<AddOnValue> ValueList = AddOnValueAdmin.GetAddOnValuesByAddOnId(int.Parse(addOnId.ToString()));

            if (ValueList.Count > 0)
            {
                StringBuilder AddOnValueName = new StringBuilder();
                foreach (AddOnValue item in ValueList.ToArray())
                {
                    AddOnValueName.Append(item.Name + ", ");
                }

                AddOnValueName.Remove(AddOnValueName.ToString().Length - 2, 2);
                return AddOnValueName.ToString();
            }

            return string.Empty;
        }

        #endregion

        #region PageLoad

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get mode value from querystring        
            if (Request.Params["mode"] != null)
            {
                this.Mode = Request.Params["mode"];
            }

            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (this.ItemId > 0)
            {
                this.BindProductAddons();
            }
            else
            {
                throw new ApplicationException("Product requested could not be found.");
            }
        }
        #endregion

        #region Protected Methods and Events

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddNewAddOn_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddProductAddOnLink + "&itemid=" + this.ItemId);
        }

        /// <summary>
        /// Add Client side event to the Delete Button in the Grid.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridProductAddOns_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow || e.Row.RowType == DataControlRowType.Header)
            {
                UserStoreAccess uss = new UserStoreAccess();
                if (uss.UserType == WebApp.ZNodeUserType.Reviewer)
                {
                    e.Row.Cells[3].Visible = true;
                    e.Row.Cells[4].Visible = false;
                }
                else
                {
                    e.Row.Cells[3].Visible = false;
                    e.Row.Cells[4].Visible = true;
                }
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Retrieve the Button control from the Seventh column.
                LinkButton DeleteButton = (LinkButton)e.Row.Cells[2].FindControl("btnDelete");

                // Set the Button's CommandArgument property with the row's index.
                DeleteButton.CommandArgument = e.Row.RowIndex.ToString();

                // Add Client Side confirmation
                DeleteButton.OnClientClick = "return confirm('Are you sure you want to delete this item?');";
            }
        }

        /// <summary>
        /// Product Add On Row command event - occurs when delete button is fired.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridProductAddOns_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // property to an Integer.
                int index = Convert.ToInt32(e.CommandArgument);

                // Get the values from the appropriate cell in the GridView control.
                GridViewRow selectedRow = uxGridProductAddOns.Rows[index];

                TableCell Idcell = selectedRow.Cells[0];
                string Id = Idcell.Text;

                if (e.CommandName == "Remove")
                {
                    ProductAddOnAdmin AdminAccess = new ProductAddOnAdmin();
                    ProductAdmin prodAdmin = new ProductAdmin();
                    ZNode.Libraries.DataAccess.Entities.Product product = prodAdmin.GetByProductId(this.ItemId);

                    ProductAddOnService productaddonService = new ProductAddOnService();
                    ProductAddOn addon = new ProductAddOn();
                    addon = productaddonService.GetByProductAddOnID(int.Parse(Id));
                    productaddonService.DeepLoad(addon);
                    string addonname = addon.AddOnIDSource.Name;

                    string Associatename = "Delete Association between  Addon " + addonname + " and Product " + product.Name;

                    if (AdminAccess.DeleteProductAddOn(int.Parse(Id)))
                    {
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, Associatename, product.Name);
                    }

                    this.BindProductAddons();
                }
            }
        }

        /// <summary>
        /// Product AddOn grid Items Page Index Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridProductAddOns_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGridProductAddOns.PageIndex = e.NewPageIndex;
            this.BindProductAddons();
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindProductAddons()
        {
            ProductAddOnAdmin ProdAddonAdminAccess = new ProductAddOnAdmin();

            // Bind Associated Addons for this product
            uxGridProductAddOns.DataSource = ProdAddonAdminAccess.GetByProductId(this.ItemId);
            uxGridProductAddOns.DataBind();

            // Hide the Add button from Reviewer.
            UserStoreAccess uss = new UserStoreAccess();
            if (uss.UserType == WebApp.ZNodeUserType.Reviewer)
            {
                btnAddNewAddOn.Visible = false;
            }
        }
        #endregion
    }
}
