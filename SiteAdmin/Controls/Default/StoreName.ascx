<%@ Control Language="C#" AutoEventWireup="True" Inherits="SiteAdmin.Controls.Default.StoreName" CodeBehind="StoreName.ascx.cs" %>
<div class="FieldStyle">
    Select Store
</div>
<div class="ValueStyle">
    <asp:DropDownList ID="StoreList" runat="server" OnSelectedIndexChanged="Storelist_SelectedIndexChanged">
    </asp:DropDownList>
    <asp:CheckBoxList ID="StoreCheckList" runat="server" OnSelectedIndexChanged="Storelist_SelectedIndexChanged">
    </asp:CheckBoxList>
</div>
<asp:Panel ID="pnlLocale" runat="server">
    <div class="FieldStyle">
        Select Locale
    </div>
    <div class="ValueStyle">
        <asp:DropDownList ID="StoreLocale" runat="server">
        </asp:DropDownList>
    </div>
</asp:Panel>
