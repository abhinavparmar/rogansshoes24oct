<%@ Control Language="C#" AutoEventWireup="True" Inherits="SiteAdmin.Controls.Default.ShoppingCart"
    CodeBehind="ShoppingCart.ascx.cs" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/SalesDepartmentPhone.ascx" TagName="SalesDepartmentPhoneNo"
    TagPrefix="ZNode" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/CustomMessage.ascx" TagName="CustomMessage"
    TagPrefix="ZNode" %>
<div>
    <asp:GridView class="TableContainer" ID="uxCart" runat="server" AutoGenerateColumns="False"
        EmptyDataText="Shopping Cart Empty" GridLines="None" CellPadding="4" OnRowCommand="Cart_RowCommand"
        OnRowDataBound="UxCart_RowDataBound" CssClass="Grid">
        <Columns>
            <asp:TemplateField HeaderText="REMOVE ITEM" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <asp:ImageButton ID="ibRemoveLineItem" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "GUID").ToString() %>'
                        CommandName="remove" ImageUrl="~/SiteAdmin/Themes/images/RemoveLineItem.png"
                        ToolTip="Remove this item" CausesValidation="false" />
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle CssClass="RemoveItem" Wrap="True" />
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <a id="A1" href='<%# DataBinder.Eval(Container.DataItem, "Product.ViewProductLink").ToString()%>'
                        runat="server">
                        <img id="Img1" alt='<%# DataBinder.Eval(Container.DataItem, "Product.ImageAltTag")%>'
                            border='0' src='<%# DataBinder.Eval(Container.DataItem, "Product.ThumbnailImageFilePath")%>'
                            runat="server" /></a>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ITEM" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <div>
                        <a id="A2" href='<%# DataBinder.Eval(Container.DataItem, "Product.ViewProductLink").ToString()%>'
                            runat="server">
                            <%# DataBinder.Eval(Container.DataItem, "Product.Name").ToString()%></a>
                    </div>
                    <div class="Description">
                        Item#
                        <%# DataBinder.Eval(Container.DataItem, "Product.ProductNum").ToString()%><br>
                        <%# DataBinder.Eval(Container.DataItem, "Product.ShoppingCartDescription").ToString()%>
                    </div>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="QUANTITY" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <asp:DropDownList ID="uxQty" runat="server" AutoPostBack="true" EnableViewState="true"
                        OnSelectedIndexChanged="Quantity_SelectedIndexChanged" ValidationGroup="groupPayment"
                        CausesValidation="true" CssClass="Quantity">
                    </asp:DropDownList>
                    <asp:RangeValidator ValidationGroup="groupPayment" ID="RangeValidator1" MinimumValue="0"
                        ControlToValidate="uxQty" ErrorMessage="*OutofStock" MaximumValue='<%# CheckInventory(DataBinder.Eval(Container.DataItem, "GUID").ToString()) %>'
                        Enabled='<%# EnableStockValidator(DataBinder.Eval(Container.DataItem, "GUID").ToString()) %>'
                        SetFocusOnError="true" runat="server" Type="Integer" Display="Dynamic"></asp:RangeValidator>
                    <asp:HiddenField ID="GUID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "GUID").ToString() %>' />
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="PRICE" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <%# DataBinder.Eval(Container.DataItem, "UnitPrice","{0:c}") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix()%>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="TOTAL" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <%# DataBinder.Eval(Container.DataItem, "ExtendedPrice","{0:c}") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix()%>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Right" />
                <HeaderStyle HorizontalAlign="Right" />
            </asp:TemplateField>
        </Columns>
        <FooterStyle CssClass="Footer" />
        <RowStyle CssClass="Row" />
        <HeaderStyle CssClass="Header" />
        <AlternatingRowStyle CssClass="AlternatingRow" />
    </asp:GridView>
</div>
<div>
    <table cellpadding="0" cellspacing="0" class="TotalBoxFooter" border="0" width="100%">
        <tr>
            <td colspan="3" align="right">
                <asp:Label ID="PaymentErrorMsg" Text='We are unable to complete the purchase because no payment options have been set up.'
                    CssClass="Error" runat="server" Visible="false" />
                <div>
                    <asp:Label ID="lblErrorMessage" CssClass="Error" runat="server"></asp:Label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="CustomMessage" style="display: none">
                    <span>
                        <ZNode:CustomMessage ID="CustomMessage2" MessageKey="ShoppingCartFooterText" runat="server" />
                    </span>
                </div>
            </td>
            <td>
                <div class="Apply">
                    Enter Coupon Code&nbsp;<asp:TextBox runat="server" ID="ecoupon"></asp:TextBox>&nbsp;<asp:ImageButton
                        ID="ApplyCouponGo" CssClass="gobutton" runat="server" AlternateText="Apply" OnClick="Btnapply_click"
                        ValidationGroup="groupPayment" />
                </div>
                <div>
                    <asp:Label ID="lblPromoMessage" CssClass="Error" runat="server"></asp:Label>
                </div>
            </td>
            <td>
                <div class="TotalBox" align="right" style="margin-left: 10px;">
                    <table cellpadding="3" cellspacing="0" border="0px" width="100%">
                        <tr>
                            <td class="SubTotal">SUBTOTAL
                            </td>
                            <td class="TotalValue">
                                <asp:Label ID="SubTotal" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr runat="server" visible="true">
                            <td>DISCOUNT
                            </td>
                            <td>
                                <asp:Label ID="DiscountDisplay" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr id="tblRowTax" runat="server" visible="false">
                            <td>TAX
                                <asp:Label ID="TaxPct" runat="server" EnableViewState="false"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="Tax" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr id="tblRowShipping" runat="server" visible="false" class="SubTotal">
                            <td>SHIPPING
                            </td>
                            <td>
                                <asp:Label ID="Shipping" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr id="trGiftCardAmount" runat="server">
                            <td>GIFT CARD AMOUNT
                            </td>
                            <td>
                                <asp:Label ID="lblGiftCardAmount" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="bold">
                            <td class="Total">TOTAL
                            </td>
                            <td class="TotalValue">
                                <asp:Label ID="Total" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>
