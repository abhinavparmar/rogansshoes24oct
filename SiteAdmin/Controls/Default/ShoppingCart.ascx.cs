using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.Promotions;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Controls.Default
{
    /// <summary>
    /// Represents the Shopping Cart user control class
    /// </summary>
    public partial class ShoppingCart : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeShoppingCart _ShoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
        private DateTime currentdate = System.DateTime.Now.Date;
        private ZNodeCoupon _coupon = new ZNodeCoupon();
        private ZNodeOrderFulfillment _order = new ZNodeOrderFulfillment();
        private int RowNumber = 0;
        private bool _ShowTaxShipping = false;
        #endregion

        #region Public Events
        public event System.EventHandler CartItem_RemoveLinkClicked;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the Current shopping cart object
        /// </summary>
        public ZNodeShoppingCart ShoppingCartObject
        {
            get
            {
                return this._ShoppingCart;
            }

            set
            {
                this._ShoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to display/hide the shipping and tax fields
        /// </summary>
        public bool ShowTaxShipping
        {
            get
            {
                return this._ShowTaxShipping;
            }

            set
            {
                this._ShowTaxShipping = value;
            }
        }

        /// <summary>
        /// Gets the coupon code
        /// </summary>
        public string CouponCode
        {
            get
            {
                return ecoupon.Text.Trim();
            }
        }
        #endregion

        #region Public Method

        /// <summary>
        /// Returns boolean value to enable Validator to check against the Quantity textbox
        /// It checks for AllowBackOrder and TrackInventory settings for this product,and returns the value
        /// </summary>
        /// <param name="value">Represents a value</param>
        /// <returns>Returns true or false value to enable stock validator</returns>
        public bool EnableStockValidator(string value)
        {
            // Get Shopping Cart Item
            ZNodeShoppingCartItem item = this._ShoppingCart.GetItem(value);

            if (item != null)
            {
                // Allow Back Order
                if (item.Product.AllowBackOrder && item.Product.TrackInventoryInd)
                {
                    return false;
                }
                else if (item.Product.AllowBackOrder == false && item.Product.TrackInventoryInd == false)
                {
                    // Don't track inventory
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Returns Quantity on Hand (inventory stock value)
        /// </summary>
        /// <param name="value">Represents a value</param>
        /// <returns>Returns Quantity on hand value</returns>
        public int CheckInventory(string value)
        {
            // Get Shopping Cart Item
            ZNodeShoppingCartItem item = this._ShoppingCart.GetItem(value);

            if (item != null)
            {
                int? quantityAvailable = null;

                if (item.Product.ZNodeBundleProductCollection.Count == 0)
                {
                    quantityAvailable = item.Product.QuantityOnHand;
                }

                foreach (ZNode.Libraries.ECommerce.Entities.ZNodeProductBaseEntity _bundleProduct in item.Product.ZNodeBundleProductCollection)
                {
                    if (quantityAvailable == null)
                    {
                        quantityAvailable = _bundleProduct.QuantityOnHand;
                    }
                    else if (quantityAvailable > _bundleProduct.QuantityOnHand)
                    {
                        quantityAvailable = _bundleProduct.QuantityOnHand;
                    }
                }

                // Get Current Qunatity by Subtracting QunatityOrdered from Quantity On Hand(Inventory)
                int CurrentQuantity = quantityAvailable.GetValueOrDefault(0) - this._ShoppingCart.GetQuantityOrdered(item);

                if (CurrentQuantity <= 0)
                {
                    return 0;
                }
                else
                {
                    return CurrentQuantity;
                }
            }
            else
            {
                return 1;
            }
        }

        /// <summary>
        /// Bind control display based on properties set
        /// </summary>
        public void Bind()
        {
            this.RowNumber = 0;

            if (this._ShoppingCart != null)
            {
                this._ShoppingCart.Calculate();

                // bind cart
                uxCart.DataSource = this._ShoppingCart.ShoppingCartItems;
                uxCart.DataBind();

                // Set the value for the Dropdown list
                foreach (GridViewRow row in uxCart.Rows)
                {
                    HiddenField hdnGUID = row.FindControl("GUID") as HiddenField;
                    string GUID = hdnGUID.Value;

                    // Get Shopping cart item using GUID value
                    ZNodeShoppingCartItem item = this._ShoppingCart.GetItem(GUID);
                    DropDownList hdnQty = row.FindControl("uxQty") as DropDownList;

                    // If Min quantity is not set in admin, set it to 1
                    int minQty = item.Product.MinQty == 0 ? 1 : item.Product.MinQty;

                    // If Max quantity is not set in admin , set it to 10
                    int maxQty = item.Product.MaxQty == 0 ? 10 : item.Product.MaxQty;

                    ArrayList quantityList = new ArrayList();

                    for (int itemIndex = minQty; itemIndex <= maxQty; itemIndex++)
                    {
                        quantityList.Add(itemIndex);
                    }

                    // Bind Quantity drop down list
                    hdnQty.DataSource = quantityList;
                    hdnQty.DataBind();

                    hdnQty.SelectedValue = Convert.ToString(item.Quantity);
                }

                // Bind totals
                SubTotal.Text = this._ShoppingCart.SubTotal.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                DiscountDisplay.Text = "-" + this._ShoppingCart.Discount.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                lblGiftCardAmount.Text = "-" + this._ShoppingCart.GiftCardAmount.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                if (this._ShowTaxShipping)
                {
                    // Shows the tax and shipping fields
                    tblRowTax.Visible = true;
                    tblRowShipping.Visible = true;
                    Tax.Text = this._ShoppingCart.OrderLevelTaxes.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                    Shipping.Text = this._ShoppingCart.ShippingCost.ToString("C") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                    Total.Text = this._ShoppingCart.Total.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();

                    // Check Sale Tax rate
                    if (this._ShoppingCart.TaxRate > 0)
                    {
                        TaxPct.Text = "(" + this._ShoppingCart.TaxRate + "%)";
                    }
                    else
                    {
                        TaxPct.Text = string.Empty;
                    }
                }
                else
                {
                    // Hides the tax and shipping fields.
                    tblRowTax.Visible = false;
                    tblRowShipping.Visible = false;
                    Total.Text = (this._ShoppingCart.SubTotal - this._ShoppingCart.Discount).ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                }

                trGiftCardAmount.Visible = this._ShoppingCart.GiftCardAmount > 0 ? true : false;

                // Show messages
                lblErrorMessage.Text = this._ShoppingCart.ErrorMessage;
                lblPromoMessage.Text = this._ShoppingCart.PromoDescription;
            }
        }

        /// <summary>
        /// Event is called when Update Quantity 
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        public void UpdateQuantity(object sender, EventArgs e)
        {
            DropDownList ddl = (DropDownList)sender;

            // Get the rowId for the Dropdown list
            int rowId = Convert.ToInt32(ddl.Attributes["rowid"]);

            HiddenField hdnGUID = uxCart.Rows[rowId].Cells[0].FindControl("GUID") as HiddenField;

            // Set the Dropdown list value   
            int Qty = Convert.ToInt32(ddl.Text);
            string GUID = hdnGUID.Value;

            // Get Shopping cart item using GUID value
            ZNodeShoppingCartItem item = this._ShoppingCart.GetItem(GUID);

            // Update quantity in the shopping cart manager
            if (Qty == 0)
            {
                this._ShoppingCart.RemoveFromCart(GUID);
            }
            else
            {
                // If Product is set to Allow back order or Track inventory is not enable then Update the quantity                
                if (item.Product.AllowBackOrder)
                {
                    // Update Quantity for this product
                    this._ShoppingCart.UpdateItemQuantity(GUID, Qty);
                }
                else if (item.Product.AllowBackOrder == false && item.Product.TrackInventoryInd == false)
                {
                    // Update Quantity for this product
                    this._ShoppingCart.UpdateItemQuantity(GUID, Qty);
                }
                else
                {
                    // Check for available quantity (which included both Available + Quantity Ordered)
                    if (this.UpdateQuantity(item))
                    {
                        this._ShoppingCart.UpdateItemQuantity(GUID, Qty);
                    }
                }
            }
        }
        #endregion

        #region General Events

        /// <summary>
        /// Event is raised when Apply button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Btnapply_click(object sender, EventArgs e)
        {
            if (this._ShoppingCart != null)
            {
                this._ShoppingCart.AddCouponCode(this.CouponCode);
            }
        }

        /// <summary>
        /// DropDown Index change event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Quantity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._ShoppingCart != null)
            {
                this.UpdateQuantity(sender, e);
            }
        }

        #endregion

        #region Grid Event

        /// <summary>
        /// Grid command event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Cart_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (this._ShoppingCart != null)
            {
                // The remove link was clicked
                if (e.CommandName.Equals("remove"))
                {
                    string GUID = e.CommandArgument.ToString();

                    // Update in the shopping cart manager
                    this._ShoppingCart.RemoveFromCart(GUID);

                    if (this.CartItem_RemoveLinkClicked != null)
                    {
                        this.CartItem_RemoveLinkClicked(sender, e);
                    }
                }
            }
        }

        /// <summary>
        /// Grid Row Data Bound Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxCart_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (this._ShoppingCart != null)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Retrieve the dropdownlist control from the first column
                    DropDownList qty = e.Row.Cells[0].FindControl("uxQty") as DropDownList;
                    if (qty != null)
                    {
                        qty.Attributes.Add("rowid", this.RowNumber.ToString());
                    }

                    this.RowNumber++;
                }
            }
        }

        #endregion

        #region Page Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            ApplyCouponGo.ImageUrl = "~/SiteAdmin/Themes/images/Apply.gif";

            // Get the user account from session
            ZNodeUserAccount _userAccount = ZNodeUserAccount.CurrentAccount();

            ZNode.Libraries.DataAccess.Service.PaymentSettingService _pmtServ = new ZNode.Libraries.DataAccess.Service.PaymentSettingService();
            ZNode.Libraries.DataAccess.Entities.TList<ZNode.Libraries.DataAccess.Entities.PaymentSetting> _pmtSetting = _pmtServ.GetAll();

            PaymentErrorMsg.Visible = false;

            // Check whether this profile has payment options
            if (Request.QueryString["ErrorMsg"] != null)
            {
                if (Request.QueryString["ErrorMsg"] == "1")
                {
                    PaymentErrorMsg.Text = "We were unable to complete your request. Please contact the store administrator. Both an Anonymous and Default Registered profile must be created for this store.";
                }

                PaymentErrorMsg.Visible = true;
            }
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Check for Qunatity ordered against Quantity On Hand
        /// </summary>
        /// <param name="item">Shopping Cart Item</param>
        /// <returns>Returns true or false to update the quantity</returns>
        private bool UpdateQuantity(ZNodeShoppingCartItem item)
        {
            ZNodeShoppingCartItem _ShoppingCartItem = null;

            int QunatityOrdered = 0;

            // Loop through the grid rows
            foreach (GridViewRow row in uxCart.Rows)
            {
                DropDownList hdnQty = row.FindControl("uxQty") as DropDownList;
                int Qty = int.Parse(hdnQty.Text);

                // Find the hidden form field and get product GUID
                HiddenField hdnGUID = row.FindControl("GUID") as HiddenField;
                string GUID = hdnGUID.Value;

                // Get the item using GUID
                _ShoppingCartItem = this._ShoppingCart.GetItem(GUID);

                // If product is removed or update with 0,then this object is set to null.
                // (Above GetItem() method will return the value for this object)
                if (_ShoppingCartItem != null)
                {
                    // Match Product
                    if (item.Product.ProductID == _ShoppingCartItem.Product.ProductID)
                    {
                        // Check Product has attributes or not 
                        if (item.Product.SelectedSKU.SKUID > 0)
                        {
                            // If Product has attributes then check for SKUID
                            if (item.Product.SelectedSKU.SKUID == _ShoppingCartItem.Product.SelectedSKU.SKUID)
                            {
                                QunatityOrdered += Qty;
                            }
                        }
                        else
                        {
                            // Product has no attributes
                            // Increment QuantityOrdered value
                            QunatityOrdered += Qty;
                        }
                    }
                }
            }

            // Check Qunatity on hand is greater than Qunatity Ordered value
            if (item.Product.QuantityOnHand < QunatityOrdered)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion
    }
}