﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Controls.Default
{
    /// <summary>
    /// Represents the Tagging user control class
    /// </summary>
    public partial class Tagging : System.Web.UI.UserControl
    {
        #region Private Member Variables

        private int ItemId = 0;
        private int SkuId = 0;
        private string _ItemType = "ProductId";
        private string AssociateName = string.Empty;

        /// <summary>
        /// Gets or sets the Item Type
        /// </summary>
        public string ItemType
        {
            get
            {
                return this._ItemType;
            }

            set
            {
                this._ItemType = value;
            }
        }

        #endregion

        #region Protected Methods and Events

        /// <summary>
        /// Bind Tag Group List
        /// </summary>
        /// <param name="tagGroupList">The Tag Group instance</param>
        protected void BindTagGroup(TList<TagGroup> tagGroupList)
        {
            dlTags.DataSource = tagGroupList;
            dlTags.DataBind();

            if (tagGroupList.Count == 0)
            {
                lblError.Text = "There are no Facet Groups associated with this product's category";
                lblError.Visible = true;
                lblNoTags.Visible = true;
            }
            else
            {
                lblNoTags.Visible = false;
            }

            btnSubmitBottom.Visible = btnCancelBottom.Visible = tagGroupList.Count == 0 ? false : true;
        }

        /// <summary>
        /// Bind Tags Method
        /// </summary>
        /// <param name="TagGroupID">The value of TagGroupID</param>
        /// <returns>Returns the Tag TList</returns>
        protected TList<Tag> BindTags(object TagGroupID)
        {
            if (Convert.ToInt32(TagGroupID) > 0)
            {
                TagService ts = new TagService();

                return ts.GetByTagGroupID(Convert.ToInt32(TagGroupID));
            }

            return null;
        }

        /// <summary>
        /// Get Tag Names Method
        /// </summary>
        /// <param name="tagGroupId">The value of Tag Group Id</param>
        /// <returns>Returns the Tag Names</returns>
        protected string GetTagNames(object tagGroupId)
        {
            TagProductSKUService tpss = new TagProductSKUService();
            TList<TagProductSKU> tpsList;

            if (this._ItemType == "SkuId")
            {
                tpsList = tpss.GetBySKUID(this.SkuId);
            }
            else
            {
                tpsList = tpss.GetByProductID(this.ItemId);
            }

            TagService ts = new TagService();
            string tagNames = string.Empty;
            if (tpsList.Count > 0)
            {
                foreach (TagProductSKU tps in tpsList)
                {
                    Tag tag = ts.GetByTagID(tps.TagID.Value);
                    if (tag.TagGroupID == Convert.ToInt32(tagGroupId))
                    {
                        if (!string.IsNullOrEmpty(tagNames))
                        {
                            tagNames += ",";
                        }

                        tagNames += tag.TagName;
                    }
                }
            }

            return tagNames;
        }

        /// <summary>
        /// Get Tags Method
        /// </summary>
        /// <param name="tagGroupId">The value of Tag Group Id</param>
        /// <returns>Returns the Tag TList</returns>
        protected TList<Tag> GetTags(object tagGroupId)
        {
            TagProductSKUService tpss = new TagProductSKUService();
            TList<TagProductSKU> tpsList;
            if (this._ItemType == "SkuId")
            {
                tpsList = tpss.GetBySKUID(this.SkuId);
            }
            else
            {
                tpsList = tpss.GetByProductID(this.ItemId);
            }

            TagService ts = new TagService();
            TList<Tag> ttag = new TList<Tag>();
            if (tpsList.Count > 0)
            {
                foreach (TagProductSKU tps in tpsList)
                {
                    Tag tag = ts.GetByTagID(tps.TagID.Value);
                    if (tag.TagGroupID == Convert.ToInt32(tagGroupId))
                    {
                        ttag.Add(tag);
                    }
                }
            }

            return ttag;
        }

        /// <summary>
        /// Event is raised when Update button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Update_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            TagProductSKUService service = new TagProductSKUService();
            TList<TagProductSKU> tpsList;
            if (this._ItemType == "SkuId")
            {
                tpsList = service.GetBySKUID(this.SkuId);
            }
            else
            {
                tpsList = service.GetByProductID(this.ItemId);
            }

            for (int i = 0; i < dlTags.Items.Count; i++)
            {
                CheckBoxList chklstTags = (CheckBoxList)dlTags.Items[i].FindControl("chklstTags");

                foreach (ListItem li in chklstTags.Items)
                {
                    if (li.Selected == true)
                    {
                        if (tpsList.Find("TagID", Convert.ToInt32(li.Value)) == null)
                        {
                            TagProductSKU tps = new TagProductSKU();
                            tps.TagID = Convert.ToInt32(li.Value);

                            if (this._ItemType == "SkuId")
                            {
                                tps.SKUID = this.SkuId;
                            }
                            else
                            {
                                tps.ProductID = this.ItemId;
                            }

                            service.Save(tps);
                        }

                        sb.Append(li.Text + ",");
                    }
                    else
                    {
                        TagProductSKU tp;
                        if ((tp = tpsList.Find("TagID", Convert.ToInt32(li.Value))) != null)
                        {
                            service.Delete(tp);
                        }
                    }
                }
            }

            ProductAdmin prodAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product entity = prodAdmin.GetByProductId(this.ItemId);
            if (sb.Length > 0)
            {
                sb.Remove(sb.Length - 1, 1);
            }

            this.AssociateName = "Associated  Tag " + sb + " to Product " + entity.Name;
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, entity.Name);
            ProductReviewHistoryAdmin reviewAdmin = new ProductReviewHistoryAdmin();
            reviewAdmin.UpdateEditReviewHistory(this.ItemId, "Tags");

            tpsList.Dispose();
            service = null;
            this.BindGrid();
            this.pnlEditTags.Visible = false;
        }

        /// <summary>
        /// Event is raised when Cancel button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            this.pnlEditTags.Visible = false;
        }

        /// <summary>
        /// Edit Tagging Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnEditTagging_Click(object sender, EventArgs e)
        {
            pnlEditTags.Visible = true;
        }

        /// <summary>
        /// Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ProductAdmin prodAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product entity = prodAdmin.GetByProductId(this.ItemId);
        }

        /// <summary>
        /// Grid Data Bound Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_DataBound(object sender, EventArgs e)
        {
            bool visible = false;
            bool row = true;
            foreach (GridViewRow gvr in uxGrid.Rows)
            {
                gvr.Visible = (gvr.FindControl("dlTagNames") as DataList).Items.Count > 0;

                if (gvr.Visible)
                {
                    gvr.CssClass = row ? "RowStyle" : "AlternatingRowStyle";
                    row = row ? false : true;
                }

                visible = visible || gvr.Visible;
            }

            uxGrid.Visible = visible;

            lblEmptyGrid.Visible = !uxGrid.Visible;
        }

        /// <summary>
        /// Event is raised when Check List Tags Pre Render is called
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ChklstTags_PreRender(object sender, EventArgs e)
        {
            CheckBoxList chklstTags = (CheckBoxList)sender;

            TagProductSKUService tpss = new TagProductSKUService();
            TList<TagProductSKU> tpsList;

            tpsList = tpss.GetByProductID(this.ItemId);
            if (tpsList.Count > 0)
            {
                foreach (ListItem li in chklstTags.Items)
                {
                    li.Selected = tpsList.Find("TagID", Convert.ToInt32(li.Value)) != null;
                }
            }
        }
        #endregion

        #region Page Events

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }

            // Get ItemId from querystring        
            if (Request.Params["skuid"] != null)
            {
                this.SkuId = int.Parse(Request.Params["skuid"]);
            }

            if (!this.IsPostBack)
            {
                this.BindGrid();
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Get Associated Tag Groups Method
        /// </summary>
        /// <returns>Returns a Tag Group TList</returns>
        private TList<TagGroup> GetAssociatedTagGroups()
        {
            TagGroupService tgs = new TagGroupService();
            TList<TagGroup> tgg = new TList<TagGroup>();

            if (this.ItemId > 0)
            {
                ProductService ps = new ProductService();

                ZNode.Libraries.DataAccess.Entities.Product product = ps.DeepLoadByProductID(this.ItemId, true, ZNode.Libraries.DataAccess.Data.DeepLoadType.IncludeChildren, typeof(TList<ProductCategory>));

                if (product != null && product.ProductCategoryCollection != null)
                {
                    foreach (ProductCategory pc in product.ProductCategoryCollection)
                    {
                        foreach (TagGroup tg in tgs.GetByCategoryIDFromTagGroupCategory(pc.CategoryID))
                        {
                            if (tgg.IndexOf(tg) == -1)
                            {
                                tgg.Add(tg);
                            }
                        }
                    }
                }
            }

            return tgg;
        }

        /// <summary>
        /// Bind Grid Method
        /// </summary>
        private void BindGrid()
        {
            TList<TagGroup> tgg = this.GetAssociatedTagGroups();
            this.BindTagGroup(tgg);

            uxGrid.Visible = true;

            uxGrid.DataSource = tgg;

            uxGrid.DataBind();
        }

        /// <summary>
        /// Remove Tag Product SKU
        /// </summary>
        /// <param name="tag">The Tag Instance</param>
        private void RemoveTagProductSku(Tag tag)
        {
            TagProductSKUService service = new TagProductSKUService();
            TList<TagProductSKU> tps = service.GetByTagID(tag.TagID);

            foreach (TagProductSKU tp in tps)
            {
                if (tp.ProductID == this.ItemId)
                {
                    service.Delete(tp);
                }
            }

            this.BindGrid();
        }
        #endregion
    }
}