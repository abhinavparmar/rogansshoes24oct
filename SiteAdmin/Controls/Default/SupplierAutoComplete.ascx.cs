﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace SiteAdmin.Controls.Default
{
    /// <summary>
    /// Represents the Supplier Auto Complete user control class
    /// </summary>
    public partial class SupplierAutoComplete : System.Web.UI.UserControl
    {
        private int DropdownItemCount = int.Parse(ConfigurationManager.AppSettings["DropdownMaxItemCount"]);

        #region Private Events
        /// <summary>
        /// Occurs when the content of the auto complete text box changes between posts to the server.
        /// </summary>
        private event System.EventHandler onSelectedIndexChanged;
        #endregion

        #region Public Properties

        /// <summary>
        ///  Gets or sets the width of the auto complete control.
        /// </summary>
        public string Width
        {
            get
            {
                return txtSupplier.Width.ToString();
            }

            set
            {
                txtSupplier.Width = new Unit(value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether AutoPostBack true or false
        /// </summary>
        public bool AutoPostBack
        {
            get
            {
                if (ddlSupplier.Visible)
                {
                    return ddlSupplier.AutoPostBack;
                }

                return bool.Parse(hdnAutoPostBack.Value);
            }

            set
            {
                if (ddlSupplier.Visible)
                {
                    ddlSupplier.AutoPostBack = value;
                }

                hdnAutoPostBack.Value = value.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the text content of the hidden field control.
        /// </summary>
        public string Value
        {
            get
            {
                if (ddlSupplier.Visible)
                {
                    return ddlSupplier.SelectedValue;
                }

                return SupplierId.Value;
            }

            set
            {
                if (ddlSupplier.Visible)
                {
                    ListItem item = ddlSupplier.Items.FindByValue(value);
                    if (item != null)
                    {
                        ddlSupplier.SelectedValue = value;
                    }
                    else
                    {
                        ddlSupplier.SelectedIndex = 0;
                    }
                }

                SupplierId.Value = value;
            }
        }

        /// <summary>
        /// Gets or sets the text content of the auto complete control.
        /// </summary>
        public string Text
        {
            get
            {
                return txtSupplier.Text;
            }

            set
            {
                txtSupplier.Text = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the auto complete control is required.
        /// </summary>
        public bool IsRequired
        {
            get
            {
                return RequiredFieldValidator2.Visible;
            }

            set
            {
                if (ddlSupplier.Visible)
                {
                    RequiredFieldValidator2.ControlToValidate = "ddlSupplier";
                }
                else
                {
                    RequiredFieldValidator2.ControlToValidate = "txtSupplier";
                }

                RequiredFieldValidator2.Visible = value;
            }
        }

        /// <summary>
        /// Sets the value for select index changed handler
        /// </summary>
        public System.EventHandler OnSelectedIndexChanged
        {
            set
            {
                if (ddlSupplier.Visible)
                {
                    ddlSupplier.SelectedIndexChanged += value;
                }
                else
                {
                    this.onSelectedIndexChanged = value;
                }
            }
        }

        #endregion

        #region Private Properties
        /// <summary>
        /// Gets the SupplierList Table 
        /// </summary>
        private TList<Supplier> SupplierListTable
        {
            get
            {
                SupplierService SupplierService = new SupplierService();

                TList<Supplier> Suppliers = SupplierService.GetAll();
                Suppliers.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.Supplier sup) { return sup.ActiveInd == true; });
                Suppliers.Sort("DisplayOrder");
                return Suppliers;
            }
        }

        #endregion

        #region Page Events
        /// <summary>
        /// Page Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            ddlSupplier.Visible = false;

            if (this.SupplierListTable.Count < this.DropdownItemCount && !this.Page.IsPostBack)
            {
                ddlSupplier.Visible = true;
                ddlSupplier.AutoPostBack = bool.Parse(hdnAutoPostBack.Value);
                txtSupplier.Visible = false;
                autoCompleteExtender3.Enabled = false;
                ddlSupplier.DataSource = this.SupplierListTable;
                ddlSupplier.DataTextField = "Name";
                ddlSupplier.DataValueField = "SupplierID";
                ddlSupplier.DataBind();
                ListItem allItem = new ListItem("Not Applicable", string.Empty);
                ddlSupplier.Items.Insert(0, allItem);
                ddlSupplier.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder strBuilderScript = new StringBuilder();

            strBuilderScript.Append("<script language=\"JavaScript\" type=\"text/javascript\">\n");
            strBuilderScript.Append("<!--\n");
            strBuilderScript.Append("function documentOnKeyPress()\n");
            strBuilderScript.Append("{\n");
            strBuilderScript.Append(" var charCode = window.event.keyCode;\n");
            strBuilderScript.Append(" var elementType = window.event.srcElement.type;\n");
            strBuilderScript.Append("\n");
            strBuilderScript.Append(" if ( (charCode == 13) && (elementType == \"text\") )\n");
            strBuilderScript.Append("   {\n");
            strBuilderScript.Append("        // Cancel the keystroke completely\n");
            strBuilderScript.Append("        window.event.returnValue = false;\n");
            strBuilderScript.Append("        window.event.cancel = true;\n");
            strBuilderScript.Append("        // Or change it to a tab\n");
            strBuilderScript.Append("  //window.event.keyCode = 9;\n");
            strBuilderScript.Append("   }\n");
            strBuilderScript.Append("}\n");
            strBuilderScript.Append("document.onkeypress = documentOnKeyPress;\n");
            strBuilderScript.Append("// -->\n");
            strBuilderScript.Append("</script>\n");

            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "OnKeyPressScript", strBuilderScript.ToString());
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when the content of the auto complete text box changes
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AutoComplete_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.onSelectedIndexChanged != null && !ddlSupplier.Visible)
            {
                this.onSelectedIndexChanged(sender, e);
            }
        }
        #endregion
    }
}