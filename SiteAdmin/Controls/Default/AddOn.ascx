﻿<%@ Control Language="C#" AutoEventWireup="True" Inherits="SiteAdmin.Controls.Default.AddOn" CodeBehind="AddOn.ascx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<div class="ImageButtons" align="right">
    <asp:Button ID="btnAddNewAddOn" runat="server" CssClass="Size100" Text="Add Add-Ons"
        OnClick="BtnAddNewAddOn_Click" />
</div>

<div>
    <uc1:Spacer ID="Spacer13" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
</div>

<!-- Update Panel for grid paging that are used to avoid the postbacks -->
<asp:UpdatePanel ID="updPnlProductAddOnsGrid" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:GridView ID="uxGridProductAddOns" OnRowDataBound="UxGridProductAddOns_RowDataBound"
            runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" OnPageIndexChanging="UxGridProductAddOns_PageIndexChanging"
            CaptionAlign="Left" OnRowCommand="UxGridProductAddOns_RowCommand" Width="100%"
            EnableSortingAndPagingCallbacks="False" PageSize="15" AllowSorting="True" EmptyDataText="No Add-Ons associated with this Product.">
            <Columns>
                <asp:BoundField DataField="ProductAddOnId" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# GetAddOnName(Eval("AddonId")) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Title" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# GetAddOnTitle(Eval("AddonId")) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Values" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# GetAddOnValues(Eval("AddonId")) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton CommandName="Remove" CausesValidation="false" ID="btnDelete" runat="server" Text="Remove &raquo" CssClass="Button" />
                    </ItemTemplate>
                </asp:TemplateField>


            </Columns>
            <EmptyDataTemplate>
                No Add-Ons associated with this Product.
            </EmptyDataTemplate>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
    </ContentTemplate>
</asp:UpdatePanel>
