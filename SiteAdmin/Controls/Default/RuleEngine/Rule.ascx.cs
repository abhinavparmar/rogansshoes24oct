﻿using System;
using System.Reflection;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Promotions;
using ZNode.Libraries.ECommerce.Shipping;
using ZNode.Libraries.ECommerce.Suppliers;
using ZNode.Libraries.ECommerce.Taxes;
using ZNode.Libraries.ECommerce.Utilities;

namespace SiteAdmin.Controls.Default.RuleEngine
{
	/// <summary>
	/// Represents the SiteAdmin - SiteAdmin.Controls.Default.RuleEngine.Rule/AddRule class
	/// </summary>
	public partial class Rule : System.Web.UI.UserControl
	{
		private int _itemId = 0;
		private string _ruleType = "0";
		private int _activityEditTypeId = (int)ZNodeLogging.ErrorNum.EditObject;
		private int _activityCreateTypeId = (int)ZNodeLogging.ErrorNum.CreateObject;
		private RuleTypeAdmin _ruleTypeAdmin = new RuleTypeAdmin();

		protected string RedirectUrl
		{
			get
			{
				var redirectUrl = "~/SiteAdmin/Secure/Advanced/Accounts/RulesEngine/";

				switch (ddlRuleTypes.SelectedValue)
				{
					case "0":
						redirectUrl += "/Default.aspx?mode=promotion";
						break;
					case "1":
						redirectUrl += "/Default.aspx?mode=shipping";
						break;
					case "2":
						redirectUrl += "/Default.aspx?mode=tax";
						break;
					case "3":
						redirectUrl += "/Default.aspx?mode=supplier";
						break;
				}

				return redirectUrl;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (Request.Params["ItemID"] != null)
			{
				_itemId = int.Parse(Request.Params["ItemID"]);
			}

			if (Request.Params["ruletype"] != null)
			{
				_ruleType = Request.Params["ruletype"];

				ddlRuleTypes.SelectedValue = _ruleType;
				ddlRuleTypes.Enabled = false;
			}

			if (!Page.IsPostBack)
			{
				if (_ruleType == "0")
				{
					lblTitle.Text = "Configure .NET Promotion Class";
					pnlClassType.Visible = true;
				}

				if (_ruleType == "1")
				{
					lblTitle.Text = "Configure .NET Shipping Class";
				}

				if (_ruleType == "2")
				{
					lblTitle.Text = "Configure .NET Tax Class";
				}

				if (_ruleType == "3")
				{
					lblTitle.Text = "Configure .NET Supplier Class";
				}

				if (_itemId > 0)
				{
					Bind();
				}
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			var status = false;

			if (ddlRuleTypes.SelectedValue == "0")
			{
				// Promotions
				if (!CheckAssemblyClass(typeof(ZNodePromotionOption)))
				{
					return;
				}

				if (_ruleTypeAdmin.IsClassNameExist(_itemId, Server.HtmlEncode(txtRuleClassName.Text.Trim()), typeof(DiscountType)))
				{
					txtRuleClassName.Focus();
					lblErrorMsg.Text = string.Format("Promotion class name '{0}' already exists.", Server.HtmlEncode(txtRuleClassName.Text.Trim()));
					return;
				}

				var entity = new DiscountType();

				if (_itemId > 0)
				{
					entity = _ruleTypeAdmin.GetDiscountTypeById(_itemId);
				}

				entity.ClassType = ddlClassTypes.SelectedValue;
				entity.Name = Server.HtmlEncode(txtRuleName.Text.Trim());
				entity.ClassName = Server.HtmlEncode(txtRuleClassName.Text.Trim());
				entity.Description = Server.HtmlEncode(txtRuleDesc.Text.Trim());
				entity.ActiveInd = chkEnable.Checked;

				if (_itemId > 0)
				{
					status = _ruleTypeAdmin.UpdateDicountType(entity);
					LogActivity(_activityEditTypeId, "Edit of promotion rule");
				}
				else
				{
					status = _ruleTypeAdmin.AddDicountType(entity);
					LogActivity(_activityCreateTypeId, "Creation of promotion rule");
				}
			}
			else if (ddlRuleTypes.SelectedValue == "1")
			{
				// Shipping
				if (!CheckAssemblyClass(typeof(ZNodeShippingOption)))
				{
					return;
				}

				if (_ruleTypeAdmin.IsClassNameExist(_itemId, Server.HtmlEncode(txtRuleClassName.Text.Trim()), typeof(ShippingType)))
				{
					txtRuleClassName.Focus();
					lblErrorMsg.Text = string.Format("Shipping rule class name '{0}' already exists.", Server.HtmlEncode(txtRuleClassName.Text.Trim()));
					return;
				}

				var shipType = new ShippingType();

				if (_itemId > 0)
				{
					shipType = _ruleTypeAdmin.GetShippingTypeById(_itemId);
				}

				shipType.Name = Server.HtmlEncode(txtRuleName.Text.Trim());
				shipType.Description = Server.HtmlEncode(txtRuleDesc.Text.Trim());
				shipType.ClassName = Server.HtmlEncode(txtRuleClassName.Text.Trim());
				shipType.IsActive = chkEnable.Checked;

				if (_itemId > 0)
				{
					status = _ruleTypeAdmin.UpdateShippingType(shipType);
					LogActivity(_activityEditTypeId, "Edit of shipping rule");
				}
				else
				{
					status = _ruleTypeAdmin.AddShippingType(shipType);
					LogActivity(_activityCreateTypeId, "Creation of shipping rule");
				}
			}
			else if (ddlRuleTypes.SelectedValue == "2")
			{
				// Taxes
				if (!CheckAssemblyClass(typeof(ZNodeTaxOption)))
				{
					return;
				}

				if (_ruleTypeAdmin.IsClassNameExist(_itemId, Server.HtmlEncode(txtRuleClassName.Text.Trim()), typeof(TaxRuleType)))
				{
					txtRuleClassName.Focus();
					lblErrorMsg.Text = string.Format("Tax rule class name '{0}' already exists.", Server.HtmlEncode(txtRuleClassName.Text.Trim()));
					return;
				}

				var taxRuleType = new TaxRuleType();

				if (_itemId > 0)
				{
					taxRuleType = _ruleTypeAdmin.GetTaxRuleTypeById(_itemId);
				}

				taxRuleType.Name = Server.HtmlEncode(txtRuleName.Text.Trim());
				taxRuleType.Description = Server.HtmlEncode(txtRuleDesc.Text.Trim());
				taxRuleType.ClassName = Server.HtmlEncode(txtRuleClassName.Text.Trim());
				taxRuleType.ActiveInd = chkEnable.Checked;

				if (_itemId > 0)
				{
					status = _ruleTypeAdmin.UpdateTaxRuleType(taxRuleType);
					LogActivity(_activityEditTypeId, "Edit of tax rule");
				}
				else
				{
					status = _ruleTypeAdmin.AddTaxRuleType(taxRuleType);
					LogActivity(_activityCreateTypeId, "Creation of tax rule");
				}
			}
			else if (ddlRuleTypes.SelectedValue == "3")
			{
				// Supplier
				if (!CheckAssemblyClass(typeof(ZNodeSupplierOption)))
				{
					return;
				}

				if (_ruleTypeAdmin.IsClassNameExist(_itemId, Server.HtmlEncode(txtRuleClassName.Text.Trim()), typeof(SupplierType)))
				{
					txtRuleClassName.Focus();
					lblErrorMsg.Text = string.Format("Supplier rule class name '{0}' already exists.", Server.HtmlEncode(txtRuleClassName.Text.Trim()));
					return;
				}

				var supplierType = new SupplierType();

				if (_itemId > 0)
				{
					supplierType = _ruleTypeAdmin.GetSupplierTypeById(_itemId);
				}

				supplierType.Name = Server.HtmlEncode(txtRuleName.Text.Trim());
				supplierType.Description = Server.HtmlEncode(txtRuleDesc.Text.Trim());
				supplierType.ClassName = Server.HtmlEncode(txtRuleClassName.Text.Trim());
				supplierType.ActiveInd = chkEnable.Checked;

				if (_itemId > 0)
				{
					status = _ruleTypeAdmin.UpdateSupplierType(supplierType);
					LogActivity(_activityEditTypeId, "Edit of supplier rule");
				}
				else
				{
					status = _ruleTypeAdmin.AddSupplierType(supplierType);

					if (status)
					{
						var supplier = new Supplier();
						supplier.Name = Server.HtmlEncode(txtRuleDesc.Text.Trim()) + " Supplier";
						supplier.DisplayOrder = 500;
						supplier.ActiveInd = true;
						supplier.SupplierTypeID = supplierType.SupplierTypeID;
						supplier.EnableEmailNotification = true;

						var supplierAdmin = new SupplierAdmin();
						supplierAdmin.Insert(supplier);
					}

					// Log activity for supplier type and then supplier
					LogActivity(_activityCreateTypeId, "Creation of supplier rule");
					LogActivity(_activityCreateTypeId, "Creation of supplier");
				}
			}

			if (status)
			{
				Response.Redirect(RedirectUrl);
			}
			else
			{
				lblErrorMsg.Text = "Unable to process your request. Please try again.";
			}
		}

		protected void BtnCancel_Click(object sender, EventArgs e)
		{
			Response.Redirect(RedirectUrl);
		}

		private bool CheckAssemblyClass(Type AssemblyType)
		{
			var className = Server.HtmlEncode(txtRuleClassName.Text.Trim());

			lblErrorMsg.Text = "Could not instantiate the class '" + className + "'. Check that the class name you typed exactly matches your class.";

			var status = false;

			var assemblyName = AssemblyType.Assembly.GetName().Name;
			var promoRuleAssebmly = Assembly.Load(assemblyName);

			var obj = promoRuleAssebmly.CreateInstance(assemblyName + "." + className);
			if (obj != null)
			{
				status = true;
				lblErrorMsg.Text = string.Empty;
			}

			return status;
		}

		private void Bind()
		{
			var ruleName = string.Empty;

			switch (_ruleType)
			{
				case "0": // Promotions
					var promoType = _ruleTypeAdmin.GetDiscountTypeById(_itemId);
					txtRuleClassName.Text = Server.HtmlDecode(promoType.ClassName);
					txtRuleName.Text = Server.HtmlDecode(promoType.Name);
					ruleName = promoType.Name;
					txtRuleDesc.Text = Server.HtmlDecode(promoType.Description);
					chkEnable.Checked = promoType.ActiveInd;
					ddlClassTypes.SelectedValue = promoType.ClassType;
					break;
				case "1": // Shipping
					var shippingType = _ruleTypeAdmin.GetShippingTypeById(_itemId);
					txtRuleClassName.Text = Server.HtmlDecode(shippingType.ClassName);
					txtRuleName.Text = Server.HtmlDecode(shippingType.Name);
					ruleName = shippingType.Name;
					txtRuleDesc.Text = Server.HtmlDecode(shippingType.Description);
					chkEnable.Checked = shippingType.IsActive;
					break;
				case "2": // Tax
					var taxRuleType = _ruleTypeAdmin.GetTaxRuleTypeById(_itemId);
					txtRuleClassName.Text = Server.HtmlDecode(taxRuleType.ClassName);
					txtRuleName.Text = Server.HtmlDecode(taxRuleType.Name);
					ruleName = taxRuleType.Name;
					txtRuleDesc.Text = Server.HtmlDecode(taxRuleType.Description);
					chkEnable.Checked = taxRuleType.ActiveInd;
					break;
				case "3": // Supplier
					var supplierType = _ruleTypeAdmin.GetSupplierTypeById(_itemId);
					txtRuleClassName.Text = Server.HtmlDecode(supplierType.ClassName);
					txtRuleName.Text = Server.HtmlDecode(supplierType.Name);
					ruleName = supplierType.Name;
					txtRuleDesc.Text = Server.HtmlDecode(supplierType.Description);
					chkEnable.Checked = supplierType.ActiveInd;
					break;
			}

			lblTitle.Text += " - " + ruleName.Trim();
		}

		private void LogActivity(int activityTypeId, string message)
		{
			var data1 = UserStoreAccess.GetCurrentUserName();
			string data2 = null;
			string data3 = null;
			string status = null;
			string longData = null;
			var source = message + " - " + txtRuleName.Text.Trim();
			var target = txtRuleName.Text.Trim();
			ZNodeLogging.LogActivity(activityTypeId, data1, data2, data3, status, longData, source, target);
		}
	}
}