<%@ Control Language="C#" AutoEventWireup="True" Inherits="SiteAdmin.Controls.Default.HtmlTextBox" CodeBehind="HtmlTextBox.ascx.cs" %>

<script lang="ja" type="text/javascript">
    tinyMCE.init({
        //General options
        mode : "exact",
        elements: <%= "\"" + ZnodeEditBox.ClientID + "\"" %>,
        theme : "advanced",
        plugins : "safari,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
        // Theme options
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime,|,forecolor,backcolor",
        theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,media,advhr,|,print,|,ltr,rtl,|,fullscreen,|,spellchecker",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,
        spellchecker_rpc_url : "TinyMCE.ashx?module=SpellChecker",
        valid_elements : '*[*]',
        extended_valid_elements : "@[id|class|style|summary|title|itemscope|itemtype|itemprop|datetime|rel|content|colspan|rowspan|align|width|height|valign|data-*],div,span,dl,dt,dd,ul,ol,li,table,tr,td,meta,p",
        content_css :  "../../../../../js/tinymce/themes/advanced/skins/default/content.css"
    });
</script>
<asp:TextBox ID="ZnodeEditBox" runat="server" Rows="10" TextMode="MultiLine" Width="100%" Height="267px"></asp:TextBox>
