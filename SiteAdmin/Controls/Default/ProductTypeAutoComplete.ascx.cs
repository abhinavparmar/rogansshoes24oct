﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;

namespace SiteAdmin.Controls.Default
{
    /// <summary>
    /// Represents the Product Type Auto Complete user control class
    /// </summary>
    public partial class ProductTypeAutoComplete : System.Web.UI.UserControl
    {
        #region Private Variables

        private int DropdownItemCount = int.Parse(ConfigurationManager.AppSettings["DropdownMaxItemCount"]);
        private bool _IsEnabled = true;

        #endregion

        #region Private Events
        /// <summary>
        /// Occurs when the content of the auto complete text box changes between posts to the server.
        /// </summary>
        private event System.EventHandler onSelectedIndexChanged;
        #endregion

        #region Public Properties

        /// <summary>
        ///  Gets or sets the width of the auto complete control.
        /// </summary>
        public string Width
        {
            get
            {
                return txtProductType.Width.ToString();
            }

            set
            {
                txtProductType.Width = new Unit(value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether IsEnabled true or false
        /// </summary>
        public bool IsEnabled
        {
            get
            {
                return this._IsEnabled;
            }

            set
            {
                this._IsEnabled = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether Auto Post Back is true or false
        /// </summary>
        public bool AutoPostBack
        {
            get
            {
                if (ddlProductType.Visible)
                {
                    return ddlProductType.AutoPostBack;
                }

                return bool.Parse(hdnAutoPostBack.Value);
            }

            set
            {
                if (ddlProductType.Visible)
                {
                    ddlProductType.AutoPostBack = value;
                }

                hdnAutoPostBack.Value = value.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the text content of the hidden field control.
        /// </summary>
        public string Value
        {
            get
            {
                if (ddlProductType.Visible)
                {
                    return ddlProductType.SelectedValue;
                }

                return ProductTypeId.Value;
            }

            set
            {
                if (ddlProductType.Visible)
                {
                    ListItem item = ddlProductType.Items.FindByValue(value);
                    if (item != null)
                    {
                        ddlProductType.SelectedValue = value;
                    }
                    else
                    {
                        ddlProductType.SelectedIndex = 0;
                    }
                }

                ProductTypeId.Value = value;
            }
        }

        /// <summary>
        /// Gets or sets the text content of the auto complete control.
        /// </summary>
        public string Text
        {
            get
            {
                return txtProductType.Text;
            }

            set
            {
                txtProductType.Text = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the auto complete control is required.
        /// </summary>
        public bool IsRequired
        {
            get
            {
                return RequiredFieldValidator2.Visible;
            }

            set
            {
                if (ddlProductType.Visible)
                {
                    RequiredFieldValidator2.ControlToValidate = "ddlProductType";
                }
                else
                {
                    RequiredFieldValidator2.ControlToValidate = "txtProductType";
                }

                RequiredFieldValidator2.Visible = value;
            }
        }

        /// <summary>
        /// Sets the value for select index changed handler
        /// </summary>
        public System.EventHandler OnSelectedIndexChanged
        {
            set
            {
                if (ddlProductType.Visible)
                {
                    ddlProductType.SelectedIndexChanged += value;
                }
                else
                {
                    this.onSelectedIndexChanged = value;
                }
            }
        }
        #endregion

        #region Private Properties
        /// <summary>
        /// Gets a TList of Prodouct Type
        /// </summary>
        private TList<ProductType> ProductTypeListTable
        {
            get
            {
                ProductTypeService productTypeService = new ProductTypeService();

                TList<ProductType> productTypes = productTypeService.GetAll();
                foreach (ProductType pt in productTypes)
                {
                    pt.Name = Server.HtmlDecode(pt.Name);
                }

                return productTypes;
            }
        }

        #endregion

        #region Page Events

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            ddlProductType.Visible = false;

            if (this.ProductTypeListTable.Count < this.DropdownItemCount && !this.Page.IsPostBack)
            {
                ddlProductType.Visible = true;
                ddlProductType.AutoPostBack = bool.Parse(hdnAutoPostBack.Value);
                txtProductType.Visible = false;
                autoCompleteExtender3.Enabled = false;
                ddlProductType.DataSource = UserStoreAccess.CheckStoreAccess(this.ProductTypeListTable, true);
                ddlProductType.DataTextField = "Name";
                ddlProductType.DataValueField = "ProductTypeID";
                ddlProductType.DataBind();
            }

            if (ddlProductType.Visible && !this.IsRequired && !this.Page.IsPostBack)
            {
                ListItem allItem = new ListItem("All", string.Empty);
                ddlProductType.Items.Insert(0, allItem);
                ddlProductType.SelectedIndex = 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder stringBuilderScript = new StringBuilder();

            stringBuilderScript.Append("<script language=\"JavaScript\" type=\"text/javascript\">\n");
            stringBuilderScript.Append("<!--\n");
            stringBuilderScript.Append("function documentOnKeyPress()\n");
            stringBuilderScript.Append("{\n");
            stringBuilderScript.Append(" var charCode = window.event.keyCode;\n");
            stringBuilderScript.Append(" var elementType = window.event.srcElement.type;\n");
            stringBuilderScript.Append("\n");
            stringBuilderScript.Append(" if ( (charCode == 13) && (elementType == \"text\") )\n");
            stringBuilderScript.Append("   {\n");
            stringBuilderScript.Append("        // Cancel the keystroke completely\n");
            stringBuilderScript.Append("        window.event.returnValue = false;\n");
            stringBuilderScript.Append("        window.event.cancel = true;\n");
            stringBuilderScript.Append("        // Or change it to a tab\n");
            stringBuilderScript.Append("  //window.event.keyCode = 9;\n");
            stringBuilderScript.Append("   }\n");
            stringBuilderScript.Append("}\n");
            stringBuilderScript.Append("document.onkeypress = documentOnKeyPress;\n");
            stringBuilderScript.Append("// -->\n");
            stringBuilderScript.Append("</script>\n");

            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "OnKeyPressScript", stringBuilderScript.ToString());

            ddlProductType.Enabled = this.IsEnabled;
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when the content of the auto complete text box changes
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AutoComplete_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.onSelectedIndexChanged != null && !ddlProductType.Visible)
            {
                this.onSelectedIndexChanged(sender, e);
            }
        }
        #endregion
    }
}