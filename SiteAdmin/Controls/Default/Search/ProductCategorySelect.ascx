﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductCategorySelect.ascx.cs" Inherits="SiteAdmin.Controls.Default.Search.ProductCategorySelect" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/Search/ProductCategoryList.ascx" TagPrefix="uc1" TagName="ProductCategoryList" %>

<asp:Panel ID="CategoryPanel" runat="server" DefaultButton="btnSearch" > 
    <table border="0">
        <tr>
            <td>
                <asp:Label runat="server">Product Name</asp:Label>
            </td>
            <td>
                <asp:Label ID="Label1" runat="server">Product #</asp:Label>
            </td>
            <td>
                <asp:Label ID="Label2" runat="server">SKU</asp:Label>
            </td>
            <td>
                <asp:Label ID="Label3" runat="server">Catalog</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="txtProductName" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtProductNumber" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtSKU" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList ID="ddCatalog" runat="server" OnSelectedIndexChanged="ddCatalog_SelectedIndexChanged" AutoPostBack="True">
                    <Items>
                        <asp:ListItem Text="" Value="-1" Selected="True" />
                    </Items>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server">Brand</asp:Label>
            </td>
            <td>
                <asp:Label ID="Label5" runat="server">Product Type</asp:Label>
            </td>
            <td>
                <asp:Label ID="Label6" runat="server">Product Category</asp:Label>
            </td>
            <td>&#160;</td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList ID="ddBrand" runat="server">
                    <Items>
                        <asp:ListItem Text="" Value="-1" Selected="True" />
                    </Items>
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddProductType" runat="server">
                    <Items>
                        <asp:ListItem Text="" Value="-1" Selected="True" />
                    </Items>
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddProductCategory" runat="server" OnDataBound ="ddProductCategory_DataBound">
                    <Items>
                        <asp:ListItem Text="" Value="-1" Selected="True" />
                    </Items>
                </asp:DropDownList>
            </td>
            <td>&#160;</td>
        </tr>
    </table>
 </asp:Panel>
<p>
    &nbsp;
</p>
<div>
    <div class="ValueStyle">
        <asp:ImageButton ID="btnClear" runat="server" AlternateText="Clear" CausesValidation="False" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif" OnClick="BtnClear_Click" onmouseout="this.src='../../../../Themes/images/buttons/button_clear.gif';" onmouseover="this.src='../../../../Themes/images/buttons/button_clear_highlight.gif';" />
        <asp:ImageButton ID="btnSearch" runat="server" AlternateText="Search" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif" OnClick="BtnSearch_Click" onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';" />
        <div class="BoostSaveResults">
            <asp:Label ID="lblSaveResult" runat="server"></asp:Label>
        </div>
    </div>
</div>
<hr />
<h4 class="GridTitle"> 
    <asp:Label ID="lblTitle" runat="server" Visible="false" Text="Product List"></asp:Label> 
</h4> 
<div>
    <asp:Label ID="lblMsg" runat="server" Visible="false" Text="No Products exist in the database."></asp:Label>
</div>

<uc1:ProductCategoryList runat="server" ID="ucProductCategoryList" /><br />
<div>
    <div class="ValueStyle">
        <asp:ImageButton ID="btnSave" runat="server" AlternateText="Save" Visible="false" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_save.gif" OnClick="BtnSave_Click" onmouseout="this.src='../../../../Themes/Images/buttons/button_save.gif';" onmouseover="this.src='../../../../Themes/Images/buttons/button_save_highlight.gif';" />
        <asp:ImageButton ID="btnCancel" runat="server" AlternateText="Cancel" Visible="false" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif" OnClick="BtnCancel_Click" onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';" />
    </div>
</div>
