﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="FieldBoostSelect.ascx.cs" Inherits="SiteAdmin.Controls.Default.Search.FieldBoostSelect"%>
<%@ Register Src="~/SiteAdmin/Controls/Default/Search/FieldBoostList.ascx" TagPrefix="uc1" TagName="FieldBoostList" %>
<div class="BoostSaveResults">
            <asp:Label ID="lblSaveResult" runat="server"></asp:Label>
        </div>
<uc1:FieldBoostList runat="server" ID="ucFieldBoostList" />
<div>
    <div class="ValueStyle" style="height: 30px; vertical-align: bottom;">
        <br/>
        <asp:ImageButton ID="btnSave" runat="server" AlternateText="Save" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_save.gif" OnClick="BtnSave_Click" onmouseout="this.src='../../../../Themes/images/buttons/button_save.gif';" onmouseover="this.src='../../../../Themes/images/buttons/button_save_highlight.gif';" />
        <asp:ImageButton ID="btnCancel" runat="server" AlternateText="Cancel" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif" OnClick="BtnCancel_Click" onmouseout="this.src='../../../../Themes/images/buttons/button_cancel.gif';" onmouseover="this.src='../../../../Themes/images/buttons/button_cancel_highlight.gif';" />
    </div>
</div>
