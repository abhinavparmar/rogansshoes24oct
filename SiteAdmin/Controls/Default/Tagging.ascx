﻿<%@ Control Language="C#" AutoEventWireup="True" Inherits="SiteAdmin.Controls.Default.Tagging" CodeBehind="Tagging.ascx.cs" %>
<%@ Register TagPrefix="znode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<div>
    <znode:Spacer ID="Spacer13" SpacerHeight="10" SpacerWidth="3" runat="server"></znode:Spacer>
</div>
<!-- Product List -->
<asp:UpdatePanel runat="server" ID="upProductTags">
  <Triggers>
       <asp:PostBackTrigger ControlID="btnSubmitBottom" />
       <asp:PostBackTrigger ControlID="btnCancelBottom" />
    </Triggers>
    <ContentTemplate>
        <asp:Panel ID="pnlTagsList" runat="server">
            <div class="ImageButtons" align="left">
                <asp:Button ID="btnEditTagging" runat="server" CssClass="Size100" Text="Edit Facets"
                    OnClick="BtnEditTagging_Click" />
            </div>
            <div>
                <znode:Spacer ID="Spacer14" SpacerHeight="10" SpacerWidth="3" runat="server"></znode:Spacer>
            </div>
            <znode:Spacer ID="Spacer5" runat="server" spacerheight="10" />
            <h4 class="GridTitle">Associated Facets</h4>
            <asp:GridView ID="uxGrid" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                CaptionAlign="Left" CellPadding="4" CssClass="Grid" GridLines="None" Width="100%"
                OnRowCommand="UxGrid_RowCommand" OnDataBound="UxGrid_DataBound">
                <Columns>
                    <asp:BoundField DataField="TagGroupLabel" HeaderText="Tag Group Name" HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText="Tag Names" ItemStyle-Width="60%">
                        <ItemTemplate>
                            <asp:DataList runat="server" ID="dlTagNames" DataSource='<%# GetTags(Eval("TagGroupId")) %>'
                                RepeatDirection="Horizontal" RepeatLayout="Flow">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblTagNames" Text='<%# Eval("TagName") %>'></asp:Label>,
                                </ItemTemplate>
                            </asp:DataList>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="RowStyle" />
                <EditRowStyle CssClass="EditRowStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <EmptyDataTemplate>
                    No Facets associated with this
                    <%=ItemType.Replace("Id","") %>
                </EmptyDataTemplate>
            </asp:GridView>
            <asp:Label runat="server" ID="lblEmptyGrid" Visible="false">No Facets associated with this <%=ItemType.Replace("Id","") %></asp:Label>
        </asp:Panel>
        <div>
            <znode:Spacer ID="Spacer6" spacerheight="10" spacerwidth="3" runat="server"></znode:Spacer>
        </div>
        <asp:Panel runat="server" ID="pnlEditTags" Visible="false" CssClass="FormView">
            <div>
                <znode:Spacer ID="Spacer3" spacerheight="10" spacerwidth="3" runat="server"></znode:Spacer>
            </div>
            <h4 class="SubTitle">
                <asp:Label runat="server" ID="lblAddEdit"></asp:Label>Facet Products</h4>
            <p>
                Select all of the Facets that describe your product. If a Facet does not exactly match
                then pick the closest one or leave it unchecked. Facets allows customers to quickly
                find your product by selecting options in each category that narrow down what products
                are displayed.
            </p>
            <div>
                <asp:Label ID="lblError" runat="server" Text="Label" Visible="false" CssClass="Error"
                    ForeColor="red"></asp:Label>
            </div>
            <znode:Spacer ID="Spacer2" spacerheight="10" spacerwidth="3" runat="server"></znode:Spacer>
            <div class="FieldStyle">
                Facets :<br />
            </div>
            <div class="ValueStyle">
                <asp:DataList runat="server" ID="dlTags" RepeatDirection="Horizontal" RepeatLayout="Flow">
                    <ItemTemplate>
                        <div class="Bold">
                            <asp:Label runat="server" ID="lblTagGroup" Text='<%# Eval("TagGroupLabel") %>'></asp:Label>
                        </div>
                        <znode:Spacer ID="Spacer3" spacerheight="3" spacerwidth="3" runat="server"></znode:Spacer>
                        <asp:CheckBoxList ID="chklstTags" OnPreRender="ChklstTags_PreRender" RepeatLayout="Table"
                            DataTextField="TagName" DataValueField="TagID" DataSource='<%# BindTags(Eval("TagGroupId")) %>'
                            RepeatDirection="Horizontal" runat="server">
                        </asp:CheckBoxList>
                        <znode:Spacer ID="Spacer14" SpacerHeight="10" SpacerWidth="3" runat="server"></znode:Spacer>
                    </ItemTemplate>
                </asp:DataList>
                <asp:Label runat="server" ID="lblNoTags" Visible="false">No Facets found</asp:Label>
            </div>
            <znode:Spacer ID="Spacer1" runat="server" spacerheight="10" />
            <div class="ClearBoth">
                <br />
            </div>
            <div>
                <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" OnClick="Update_Click" CausesValidation="true" />
                <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="Cancel_Click" />
            </div>
            <znode:Spacer ID="Spacer4" runat="server" spacerheight="10" />
        </asp:Panel>
        <asp:UpdateProgress ID="uxTagUpdateProgress" runat="server" AssociatedUpdatePanelID="upProductTags"
    DisplayAfter="10">
    <ProgressTemplate>
        <div id="ajaxProgressBg">
        </div>
        <div id="ajaxProgress">
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>

