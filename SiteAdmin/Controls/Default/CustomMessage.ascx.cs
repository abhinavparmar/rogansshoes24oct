using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Controls.Default
{
    /// <summary>
    /// Represents the CustomMessage user control class
    /// </summary>
    public partial class CustomMessage : System.Web.UI.UserControl
    {
        /// <summary>
        /// Sets the Message Key
        /// </summary>
        public string MessageKey
        {
            set
            {
                string key = value;
                ZNode.Libraries.Admin.MessageConfigAdmin admin = new ZNode.Libraries.Admin.MessageConfigAdmin();
                string msg = admin.GetMessage(key, ZNodeConfigManager.SiteConfig.PortalID, 43);

                if (msg != null)
                {
                    lblMsg.Text = msg;
                }
            }
        }
    }
}