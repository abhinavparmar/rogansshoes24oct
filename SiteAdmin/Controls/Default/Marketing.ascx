﻿<%@ Control Language="C#" AutoEventWireup="True" Inherits="SiteAdmin.Controls.Default.Marketing" CodeBehind="Marketing.ascx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<div align="left" class="ImageButtons">
    <asp:Button ID="btnEditSEO" runat="server" OnClick="EditMarketing_Click" Text="Edit Information" />
</div>
<br />
<h4 class="SubTitle">Display</h4>
<div class="Display">
    <div>
        <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
    </div>
    <div class="FieldStyle">
        Display Weighting
    </div>
    <div class="ValueStyle">
        <asp:Label ID="lblDisplayorder" runat="server"></asp:Label>
    </div>
    <div class="FieldStyleA">
        Show on Home page
    </div>
    <div class="ValueStyleA">
        <img id="chkIsSpecialProduct" runat="server" alt="" src="" />
    </div>
    <div class="FieldStyle">
        Featured Item Icon
    </div>
    <div class="ValueStyle">
        <img id="FeaturedProduct" runat="server" alt="" src="" />
    </div>
    <div class="FieldStyleA">
        New Item Icon
    </div>
    <div class="ValueStyleA">
        <img id="chkIsNewItem" runat="server" alt="" src="" />
    </div>
    <div class="ClearBoth">
    </div>
    <br />
    <h4 class="SubTitle">SEO</h4>
    <uc1:Spacer ID="Spacer5" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    <div>
        The following option set the Search Engine Optimization (SEO) meta tags. These tags
        help search engines like Google understand and index the web site.
    </div>
    <uc1:Spacer ID="Spacer6" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    <div class="FieldStyle">
        Search Engine Title
    </div>
    <div class="ValueStyle">
        <asp:Label ID="lblSEOTitle" runat="server"></asp:Label>
    </div>
    <div class="FieldStyleA">
        Search Engine Keywords
    </div>
    <div class="ValueStyleA">
        <asp:Label ID="lblSEOKeywords" runat="server" CssClass="Price"></asp:Label>&nbsp;
    </div>
    <div class="FieldStyle">
        Search Engine Description
    </div>
    <div class="ValueStyle">
        <asp:Label ID="lblSEODescription" runat="server"></asp:Label>&nbsp;
    </div>
    <div class="FieldStyleA">
        SEO URL
    </div>
    <div class="ValueStyleA">
        <asp:Label ID="lblSEOURL" runat="server"></asp:Label>&nbsp;
    </div>
    <div class="ClearBoth">
        <br />
    </div>
</div>
<div align="left" class="ImageButtons" style="display: none;">
    <asp:Button ID="btnNotify" CssClass="Size210" runat="server" Text="Notify Marketing for Review"
        OnClick="BtnNotify_Click" />
</div>
