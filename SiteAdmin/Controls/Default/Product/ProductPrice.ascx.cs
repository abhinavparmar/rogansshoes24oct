using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Controls.Default.Product
{
    /// <summary>
    /// Represents the ProductPrice user control class
    /// </summary>
    public partial class ProductTitle : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProduct _Product;
        private int _Quantity = 1;
        private ZNodeProfile _Profile = new ZNodeProfile();
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the Znodeproduct object
        /// </summary>
        public ZNodeProduct Product
        {
            get
            {
                return this._Product;
            }

            set
            {
                this._Product = value;
            }
        }

        /// <summary>
        /// Gets or sets the quantity value
        /// </summary>
        public int Quantity
        {
            get
            {
                return this._Quantity;
            }

            set
            {
                this._Quantity = value;
            }
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind control display based on properties set
        /// </summary>
        public void Bind()
        {
            if (this._Product != null)
            {
                if (this._Product.CallForPricing)
                {
                    StringBuilder strBuilderPrice = new StringBuilder();
                    strBuilderPrice.Append("<span class=CallForPriceMsg>");
                    strBuilderPrice.Append(ZNodeCatalogManager.MessageConfig.GetMessage("ProductCallForPricing"));
                    strBuilderPrice.Append("</span>");

                    return;
                }

                ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
                item.Product = new ZNodeProductBase(this._Product);
                item.Quantity = this._Quantity;

                decimal price = item.ExtendedPrice;
                decimal retailprice = (this._Product.RetailPrice + this._Product.SelectedAddOnItems.TotalAddOnCost) * this._Quantity;

                if (this._Product.SalePrice.HasValue || (this._Product.WholesalePrice.HasValue && this._Profile.UseWholesalePricing))
                {
                    Price.Text = ZNodePricingFormatter.ConfigureProductPricing(retailprice, price);
                }
                else
                {
                    if (this._Product.IsMapPrice)
                    {
                        Price.Text = ZNodePricingFormatter.ConfigureProductPricing(price);
                    }
                    else
                    {
                        Price.Text = ZNodePricingFormatter.ConfigureProductPricingRed(price);
                    }
                }

                item.Product = null;
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this._Product = (ZNodeProduct)HttpContext.Current.Items["Product"];
            this.Bind();
        }
        #endregion
    }
}