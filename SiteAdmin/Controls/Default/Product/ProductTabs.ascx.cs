using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Controls.Default.Product
{
    /// <summary>
    /// Represents the ProductTabs user control class
    /// </summary>
    public partial class ProductTabs : System.Web.UI.UserControl
    {
        #region Private Variables
        private int ProductId;
        private ZNode.Libraries.DataAccess.Entities.Product product = new ZNode.Libraries.DataAccess.Entities.Product();
        private string Mode = string.Empty;
        #endregion

        #region Bind Method
        /// <summary>
        /// Bind Method
        /// </summary>
        public void Bind()
        {
            ProductAdditionalInformation.Text = Server.HtmlDecode(this.product.AdditionalInformation);
            ProductFeatureDesc.Text = Server.HtmlDecode(this.product.Description);

            int counter = 0;

            // This should be retrieved from the store settings in the future
            bool ShowTabsOnlyIfDataPresent = true;

            // Show tabs only if they contain any information
            if (ShowTabsOnlyIfDataPresent)
            {
                ProductTabstab.Tabs[0].Visible = true;
                ProductTabstab.Tabs[1].Visible = true;

                if (this.product.Description.Length == 0)
                {
                    ProductTabstab.Tabs[0].Visible = false;
                    ProductTabstab.Tabs[0].HeaderText = string.Empty;
                    counter++;
                }

                if (this.product.AdditionalInformation != null)
                {
                    if (this.product.AdditionalInformation.Length == 0 && ProductTabstab.Tabs.Count > 2)
                    {
                        ProductTabstab.Tabs[0].Visible = false;
                        counter++;
                    }
                }

                if (!this.Mode.Equals(string.Empty))
                {
                    if (counter == 0)
                    {
                        counter = 2;
                    }
                    else if (counter == 2)
                    {
                        counter = 1;
                    }

                    ProductTabstab.ActiveTabIndex = counter - 1;
                }
            }
        }

        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get product id from querystring  
            if (Request.Params["mode"] != null)
            {
                this.Mode = Request.Params["mode"];
            }

            if (Request.Params["zpid"] != null)
            {
                this.ProductId = int.Parse(Request.Params["zpid"]);
            }
            else if (Request.Params["itemid"] != null)
            {
                this.ProductId = int.Parse(Request.Params["itemid"]);
            }
            else if (Session["itemid"] != null)
            {
                this.ProductId = int.Parse(Session["itemid"].ToString());
            }
            else
            {
                return;
            }

            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            ProductAdmin pdtadmin = new ProductAdmin();
            this.product = pdtadmin.GetByProductId(this.ProductId);

            this.Bind();
        }

        #endregion
    }
}
