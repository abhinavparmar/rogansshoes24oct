﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Controls.Default
{
    /// <summary>
    /// Represents the MobileSettings user control class
    /// </summary>
    public partial class MobileSettingsList : System.Web.UI.UserControl
    {
        #region Private Variables
        private string EditPageLink = "~/SiteAdmin/Secure/Setup/Storefront/Stores/EditMobileSettings.aspx?itemid=";
        private int PortalId = 0;
        #endregion

        #region Protected Methods and Events

        protected string GetCategoryName(object categoryId)
        {
            string categoryName = string.Empty;
            if (categoryId != null)
            {
                CategoryAdmin categoryAdmin = new CategoryAdmin();
                Category category = categoryAdmin.GetByCategoryId(Convert.ToInt32(categoryId));
                if (category != null)
                {
                    categoryName = category.Name;
                }
            }

            return categoryName;
        }

        /// <summary>
        /// Gets the Image Path
        /// </summary>
        /// <param name="originalFileName">Original File Name</param>
        /// <returns>Returns the Image Path</returns>
        protected string GetImagePath(object originalFileName)
        {
            ZNodeImage znodeImage = new ZNodeImage();
            if (originalFileName == null)
            {
                return znodeImage.GetImageHttpPathThumbnail(string.Empty);
            }
            else
            {
                return znodeImage.GetImageHttpPathThumbnail(originalFileName.ToString());
            }
        }

        /// <summary>
        /// Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "Manage")
                {
                    string redirectLink = string.Format("{0}{1}&mode=mobilesettings", this.EditPageLink, e.CommandArgument);
                    Response.Redirect(redirectLink);
                }
            }
        }
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.PortalId = Convert.ToInt32(Request.QueryString["itemid"]);

            if (!this.IsPostBack)
            {
                this.BindStores();
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindStores()
        {
            PortalAdmin portalAdmin = new PortalAdmin();
            TList<Portal> portalList = new TList<Portal>();
            portalList.Add(portalAdmin.GetByPortalId(this.PortalId));
            uxGrid.DataSource = portalList;
            uxGrid.DataBind();
        }
        #endregion
    }
}
