﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Controls.Default
{
    /// <summary>
    /// Represents the Marketing user control class
    /// </summary>
    public partial class Marketing : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private int ItemId = 0;
        private string ManagePageLink = "~/SiteAdmin/Secure/Vendors/VendorProducts/EditMarketing.aspx?itemid=";
        #endregion

        #region page load
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId (Product Id) from QueryString        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (!this.IsPostBack)
            {
                // Hide the 'Notify Marketing for Review' from vendor.
                UserStoreAccess uss = new UserStoreAccess();
                btnNotify.Visible = uss.UserType == WebApp.ZNodeUserType.Reviewer ? true : false;

                this.Bind();
            }
        }
        #endregion

        #region Event Methods
        /// <summary>
        /// Event is raised when Edit Marketing button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void EditMarketing_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ManagePageLink + this.ItemId);
        }

        /// <summary>
        /// Event is raised when Notify button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnNotify_Click(object sender, EventArgs e)
        {
            this.SendMail();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Bind Method
        /// </summary>
        private void Bind()
        {
            // Create Instance for Product Admin and Product entity
            ZNode.Libraries.Admin.ProductAdmin prodAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product product = prodAdmin.GetByProductId(this.ItemId);

            // Display
            lblDisplayorder.Text = product.DisplayOrder.ToString();
            chkIsSpecialProduct.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(this.DisplayVisible(product.HomepageSpecial));
            if (product.FeaturedInd)
            {
                FeaturedProduct.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
            }
            else
            {
                FeaturedProduct.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(false);
            }

            chkIsNewItem.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(this.DisplayVisible(product.NewProductInd));

            // SEO
            lblSEODescription.Text = product.SEODescription;
            lblSEOKeywords.Text = product.SEOKeywords;
            lblSEOTitle.Text = product.SEOTitle;
            lblSEOURL.Text = product.SEOURL;
        }

        /// <summary>
        /// Validate for Null Values and return a Boolean Value
        /// </summary>
        /// <param name="fieldvalue">The Field Value</param>
        /// <returns>Returns true or false value</returns>
        private bool DisplayVisible(object fieldvalue)
        {
            if (fieldvalue == DBNull.Value)
            {
                return false;
            }
            else
            {
                if (Convert.ToInt32(fieldvalue) == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        /// <summary>
        /// Send the notification details to Marketing team about the product SEO details.
        /// </summary>
        private void SendMail()
        {
            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            string subject = "Product SEO Information";

            string body = string.Empty;
            string toAddress = ConfigurationManager.AppSettings["SEONotificationMail"];
            string templateFile = Path.Combine(HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath), "MarketingReviewEmailTemplate.htm");

            // Check whether the template file exist or not.
            if (!File.Exists(templateFile))
            {
                lblError.Text = "Unable to send email: Email Template MarketingReviewEmailTemplate.htm could not be found";
                return;
            }

            using (StreamReader rw = new StreamReader(templateFile))
            {
                body = rw.ReadToEnd();
                rw.Close();
            }

            ZNode.Libraries.DataAccess.Entities.Product product = this.GetProduct();

            Regex RegularExpressionName = new Regex("#Name#", RegexOptions.IgnoreCase);
            body = RegularExpressionName.Replace(body, product.Name);

            Regex RegularExpressionTitle = new Regex("#Title#", RegexOptions.IgnoreCase);
            body = RegularExpressionTitle.Replace(body, product.SEOTitle == null ? string.Empty : product.SEOTitle);

            Regex RegularExpressionKeyword = new Regex("#Keyword#", RegexOptions.IgnoreCase);
            body = RegularExpressionKeyword.Replace(body, product.SEOKeywords == null ? string.Empty : product.SEOKeywords);

            Regex RegularExpressionDescription = new Regex("#Description#", RegexOptions.IgnoreCase);
            body = RegularExpressionDescription.Replace(body, product.SEODescription == null ? string.Empty : product.SEODescription);

            Regex RegularExpressionUrl = new Regex("#Url#", RegexOptions.IgnoreCase);
            body = RegularExpressionUrl.Replace(body, product.SEOURL == null ? string.Empty : product.SEOURL);

            Regex RegularExpressionProductUrl = new Regex("#ProductUrl#", RegexOptions.IgnoreCase);
            body = RegularExpressionProductUrl.Replace(body, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath + "/Admin/Secure/product/view.aspx?itemid=" + product.ProductID.ToString() + "&mode=marketing");

            Regex RegularExpressionLogo = new Regex("#LOGO#", RegexOptions.IgnoreCase);
            body = RegularExpressionLogo.Replace(body, ZNodeConfigManager.SiteConfig.StoreName);

            Regex RegularExpressionCustPhone = new Regex("#CUSTOMERSERVICEPHONE#", RegexOptions.IgnoreCase);
            body = RegularExpressionCustPhone.Replace(body, ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber);

            Regex RegularExpressionCustEmail = new Regex("#CUSTOMERSERVICEMAIL#", RegexOptions.IgnoreCase);
            body = RegularExpressionCustEmail.Replace(body, ZNodeConfigManager.SiteConfig.CustomerServiceEmail);

            try
            {
                ZNodeEmail.SendEmail(toAddress, senderEmail, String.Empty, subject, body, true);
            }
            catch
            {
                string msg = string.Format("Could not send the product SEO information to \"{0}\". Check that the email address is correct.", toAddress);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(msg);
                lblError.Text = msg;
            }
        }

        /// <summary>
        /// Get the Product details
        /// </summary>
        /// <returns>Returns the Product Instance</returns>
        private ZNode.Libraries.DataAccess.Entities.Product GetProduct()
        {
            // Create Instance for Product Admin and Product entity
            ZNode.Libraries.Admin.ProductAdmin ProdAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product product = ProdAdmin.GetByProductId(this.ItemId);
            return product;
        }
        #endregion
    }
}