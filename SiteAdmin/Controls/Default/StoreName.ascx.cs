using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace SiteAdmin.Controls.Default
{
    /// <summary>
    /// Gets the Store Name user control class
    /// </summary>
    public partial class StoreName : System.Web.UI.UserControl
    {
        #region Private Variables
        private string _ControlType = string.Empty;
        private bool _ShowCheckBoxesVertically = false;
        private string _PreSelectValue = string.Empty;
        private string _LocalePreSelectValue = string.Empty;
        private List<string> _PreSelectedItems = new List<string>();
        private bool _EnableControl = true;
        private bool _IsVisibleLocaleDropdown = true;
        private bool _IsAutoPostBack = true;
        private int localeID = 0;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the ControlType. Whether the user control will be Drop Down or CheckboxList
        /// </summary>
        public string ControlType
        {
            get
            {
                return this._ControlType;
            }

            set
            {
                this._ControlType = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show CheckBoxes vertically
        /// </summary>
        public bool ShowCheckBoxesVertically
        {
            get
            {
                return this._ShowCheckBoxesVertically;
            }

            set
            {
                this._ShowCheckBoxesVertically = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to enable store dropdown
        /// </summary>
        public bool EnableControl
        {
            get
            {
                return this._EnableControl;
            }

            set
            {
                this._EnableControl = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to set visible locale dropdown
        /// </summary>
        public bool VisibleLocaleDropdown
        {
            get
            {
                return this._IsVisibleLocaleDropdown;
            }

            set
            {
                this._IsVisibleLocaleDropdown = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether AutoPostBack for dropdown
        /// </summary>
        public bool AutoPostBack
        {
            get
            {
                return this._IsAutoPostBack;
            }

            set
            {
                this._IsAutoPostBack = value;
            }
        }

        /// <summary>
        /// Gets Selected items of CheckboxList.
        /// </summary>
        public ListItemCollection SelectedItems
        {
            get
            {
                ListItemCollection col = new ListItemCollection();
                foreach (ListItem li in StoreCheckList.Items)
                {
                    if (li.Selected)
                    {
                        col.Add(li);
                    }
                }

                return col;
            }
        }

        /// <summary>
        /// Gets the selected  value of Drop Down List
        /// </summary>
        public int SelectedValue
        {
            get
            {
                if (StoreList.SelectedIndex != -1)
                {
                    return int.Parse(StoreList.SelectedValue);
                }

                return 0;
            }
        }

        /// <summary>
        /// Gets or sets the default value of Drop Down List
        /// </summary>
        public string PreSelectValue
        {
            get
            {
                return this._PreSelectValue;
            }

            set
            {
                this._PreSelectValue = value;

                StoreList.SelectedValue = this._PreSelectValue;
            }
        }

        /// <summary>
        /// Gets the selected  value of Drop Down List
        /// </summary>
        public int LocaleSelectedValue
        {
            get
            {
                if (StoreLocale.SelectedIndex != -1)
                {
                    return int.Parse(StoreLocale.SelectedValue);
                }

                return 43;
            }
        }

        /// <summary>
        /// Gets or sets the default value of Drop Down List
        /// </summary>
        public string LocalePreSelectValue
        {
            get
            {
                return this._LocalePreSelectValue;
            }

            set
            {
                this._LocalePreSelectValue = value;
                StoreLocale.SelectedValue = this._LocalePreSelectValue;
            }
        }

        /// <summary>
        /// Gets or sets the default selected value of CheckboxList
        /// </summary>
        public List<string> PreSelectedItems
        {
            get
            {
                return this._PreSelectedItems;
            }

            set
            {
                this._PreSelectedItems = value;
            }
        }

        #endregion

        #region Page Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // First retrieve order info by orderid to check if it valid for the current account
                ZNode.Libraries.DataAccess.Service.PortalService portalServ = new ZNode.Libraries.DataAccess.Service.PortalService();

                StoreList.Visible = false;
                StoreCheckList.Visible = false;
                TList<ZNode.Libraries.DataAccess.Entities.Portal> portals = portalServ.GetAll();

                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (profiles.StoreAccess != "AllStores")
                {
                    portals.Filter = string.Concat("PortalID IN [", profiles.StoreAccess, "]");
                }

                if (this.ControlType.ToLower() == "checkboxlist")
                {
                    // CheckboxList Type
                    StoreCheckList.Visible = true;
                    StoreCheckList.DataSource = portals;
                    StoreCheckList.DataTextField = "StoreName";
                    StoreCheckList.DataValueField = "PortalID";
                    StoreCheckList.AutoPostBack = this.AutoPostBack;
                    StoreCheckList.DataBind();

                    // Set Vertical Or Horizontal view
                    if (this.ShowCheckBoxesVertically)
                    {
                        StoreCheckList.RepeatDirection = RepeatDirection.Vertical;
                    }
                    else
                    {
                        StoreCheckList.RepeatDirection = RepeatDirection.Horizontal;
                    }

                    foreach (string s in this._PreSelectedItems)
                    {
                        foreach (ListItem li in StoreCheckList.Items)
                        {
                            if (s == li.Value)
                            {
                                li.Selected = true;
                            }
                        }
                    }
                }
                else
                {
                    // DropDownList Type
                    StoreList.Visible = true;
                    StoreList.DataSource = portals;
                    StoreList.DataTextField = "StoreName";
                    StoreList.DataValueField = "PortalID";
                    StoreList.DataBind();
                    StoreList.SelectedValue = this._PreSelectValue;
                    StoreList.Enabled = this._EnableControl;
                    StoreList.AutoPostBack = this.AutoPostBack;
                }



                if (StoreList.SelectedValue != "0")
                {
                    if (!this.VisibleLocaleDropdown)
                    {
                        pnlLocale.Visible = false;
                    }
                    //this.BindLocale();
                }
                
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Store List Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Storelist_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindLocale();
        }

        /// <summary>
        /// Bind Locale Method
        /// </summary>
        protected void BindLocale()
        {
            LocaleAdmin localeAdmin = new LocaleAdmin();
            StoreLocale.DataSource = localeAdmin.GetLocaleByPortalID(Convert.ToInt32(StoreList.SelectedValue));
            StoreLocale.DataTextField = "LocaleDescription";
            StoreLocale.DataValueField = "LocaleID";
            StoreLocale.DataBind();
            StoreLocale.Enabled = this._EnableControl;

            if (!this.VisibleLocaleDropdown)
            {
                pnlLocale.Visible = false;
            }
        }

        #endregion
    }
}