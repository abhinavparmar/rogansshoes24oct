﻿<%@ Control Language="C#" AutoEventWireup="True" Inherits="SiteAdmin.Controls.Default.CrosssSell" CodeBehind="CrosssSell.ascx.cs" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>

<ZNode:Spacer ID="Spacer5" runat="server" SpacerHeight="10" />

<div class="ImageButtons">
    <asp:Button ID="btnAddRelatedItems" runat="server" Text="Add Cross Sell Items"
        OnClick="BtnAddRelatedItems_Click" />
    <br />

</div>
<div>
    <ZNode:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
</div>

<asp:UpdatePanel ID="updPnlRealtedGrid" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:GridView ID="uxGrid" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
            runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
            CssClass="Grid" Width="100%" GridLines="None" OnRowCommand="UxGrid_RowCommand"
            AllowPaging="True" OnPageIndexChanging="UxGrid_PageIndexChanging" PageSize="10">
            <Columns>
                <asp:BoundField DataField="RelatedProductId" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText="Product Name" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="lblProductName" runat="server" Text='<%# GetProductName(Eval("RelatedProductId")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="Delete" Text="Remove &raquo" CommandArgument='<%# Eval("RelatedProductId") %>'
                            CommandName="RemoveItem" CssClass="Button" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                No related items found
            </EmptyDataTemplate>
            <RowStyle CssClass="RowStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <FooterStyle CssClass="FooterStyle" />
        </asp:GridView>
    </ContentTemplate>
</asp:UpdatePanel>
