﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Profile.ascx.cs" Inherits="SiteAdmin.Controls.Default.Accounts.Profile" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<div>
    <div class="LeftFloat" style="width: 40%; text-align: left">
        <h1 style="width:700px;">Associate Profile to: <asp:Label ID="lblName" runat="server" /></h1>
    </div>
    <div style="float: right" class="ImageButtons">
        <asp:Button ID="btnBack" runat="server" CssClass="Size100" OnClick="BtnBack_Click"
            Text="<< Back" />
    </div>
    <ZNode:DemoMode ID="DemoMode1" runat="server"></ZNode:DemoMode>
    <!-- Search profile panel -->
    <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server" Visible="false">
        <div class="SearchForm">
            <h4 class="SubTitle">Search Profile</h4>
            <div class="RowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle">Keyword</span><br />
                    <span class="ValueStyle">
                        <asp:TextBox ID="txtprofilename" runat="server"></asp:TextBox></span>
                </div>
            </div>
            <div class="ClearSearch">
                <asp:Button ID="btnSearch" runat="server" CssClass="Button" OnClick="BtnSearch_Click"
                    Text="Search" />
                <asp:Button ID="btnClear" CausesValidation="false" runat="server" OnClick="BtnClear_Click"
                    Text="Clear Search" CssClass="Button" />
            </div>
        </div>
    </asp:Panel>
    <!-- Profile List -->
    <asp:Panel ID="pnlProfileList" runat="server">
        <h4 class="GridTitle">Profile List</h4>
            <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" CaptionAlign="Left" AutoGenerateColumns="False"
                GridLines="None" AllowPaging="True" PageSize="10" OnPageIndexChanging="UxGrid_PageIndexChanging"
                EmptyDataText="No Products exist in the database." Width="100%" CellPadding="4">
                <Columns>
                    <asp:TemplateField HeaderText="Select" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkProfile" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ProfileId" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText="WholeSale Pricing" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img alt="" id="UseWholeSalePricingImg" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "UseWholeSalePricing").ToString()))%>'
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Show Price" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img alt="" id="ShowPricingImg" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ShowPricing").ToString()))%>'
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Weighting" HeaderText="Weighting" HeaderStyle-HorizontalAlign="Left" />
                </Columns>
                <EmptyDataTemplate>
                    No profiles found.
                </EmptyDataTemplate>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
        <div>
            <ZNode:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div>
            <asp:Label ID="lblError" runat="server" Text="Label" Visible="false" CssClass="Error"
                ForeColor="red"></asp:Label>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div>
            <asp:ImageButton ID="btnSubmitBottom" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="Submit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="Cancel_Click" />
        </div>
    </asp:Panel>
    <div>
        <ZNode:Spacer ID="Spacer3" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
</div>
