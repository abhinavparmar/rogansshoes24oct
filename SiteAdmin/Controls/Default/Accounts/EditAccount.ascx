﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditAccount.ascx.cs" Inherits="SiteAdmin.Controls.Default.Accounts.EditAccount" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>

<div class="FormView" align="center">
    <div>
        <div class="LeftFloat" style="width: auto; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
            <uc2:DemoMode ID="DemoMode1" runat="server" />
        </div>
        <div style="float: right">
            <asp:ImageButton ID="btnSubmitTop" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click"
                ValidationGroup="EditContact" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
        <div class="ClearBoth">
        </div>
        <div align="left">
            <div>
                <asp:Label ID="lblErrorMsg" runat="server" CssClass="Error" EnableViewState="false"></asp:Label>
            </div>
            <div>
                <uc1:Spacer ID="Spacer8" SpacerHeight="6" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
            <asp:Panel runat="server" ID="pnlSubTitle">
                <h4 class="SubTitle">General</h4>
            </asp:Panel>
            <!-- Update Panel for profiles drop down list to avoid the postbacks -->
            <asp:UpdatePanel ID="updPnlRealtedGrid" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlAccount">
                        <div class="FieldStyle">
                            Account Number
                            <asp:Label ID="lblaccntnumastric" runat="server" ForeColor="Red" Text="*"
                                Visible="False"></asp:Label>
                            <br />
                            <small>Your Internal account number(ERP). Leave blank if not applicable.</small>
                        </div>

                        <div class="ValueStyle">
                            <asp:TextBox ID="txtExternalAccNumber" runat="server" Width="150px"></asp:TextBox>

                        </div>
                    </asp:Panel>
                    <h4 class="SubTitle">Login Information</h4>
                    <%-- Zeon Custom Implementation:Replacing UserId with Email--%>
                    <div class="FieldStyle">
                        <asp:Literal ID="ltlStores" Text="Store Name" runat="server"></asp:Literal>
                    </div>
                    <div class="ValueStyle">
                        <asp:DropDownList ID="ddlPortal" runat="server"></asp:DropDownList>
                    </div>
                    <div class="FieldStyle">
                        Email Address<br />
                        <small>Email Address as User Name</small>
                        <asp:Label ID="lbluseridastric" runat="server" Text="*" Visible="false" ForeColor="Red"></asp:Label>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="UserID" runat="server" Columns="40" Width="150px" class="UserName"/>
                        <asp:RequiredFieldValidator ID="UseridRequired" runat="server"
                            ControlToValidate="UserID" ToolTip="User Id is required." ErrorMessage="User Id is required."
                            ValidationGroup="EditContact" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="Regularexpressionvalidator3" runat="server" ControlToValidate="UserID"
                            ErrorMessage="*Please use a valid email address." Display="Dynamic" ValidationExpression="[\w\.-]+(\+[\w-]*)?@([\w-]+\.)+[\w-]+"
                            ValidationGroup="EditContact" CssClass="Error"></asp:RegularExpressionValidator>
                    </div>
                    <asp:Label ID="lblUserIDErrorMsg" runat="server" CssClass="Error" EnableViewState="false"></asp:Label>

                    <div class="FieldStyle">
                        Password
                        <asp:Label ID="lblpwdastric" runat="server" Text="*" Visible="false" ForeColor="Red"></asp:Label>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="Password" ValidationGroup="PasswordField" runat="server" TextMode="Password" />
                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                            ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="EditContact"
                            Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>

                        <div>

                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="Password"
                                Display="Dynamic" ErrorMessage="New Password length should be minimum 8" SetFocusOnError="True"
                                ToolTip="New Password length should be minimum 8 and should contain at least 1 number."
                                ValidationExpression="(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,})" ValidationGroup="EditContact"
                                CssClass="Error">Password must be 8 or more characters and should contain only alphanumeric values with at least 1 numeric character.</asp:RegularExpressionValidator>
                        </div>
                        <asp:Label ID="lblPwdErrorMsg" runat="server" CssClass="Error" EnableViewState="false"></asp:Label>
                    </div>

                    <%-- <div>
                        <asp:Panel ID="pnlQuestionAnswer" runat="server" Visible="true">

                            <div class="FieldStyle">
                                Security Question
                            </div>
                            <div class="ValueStyle">
                                <asp:DropDownList ID="ddlSecretQuestions" runat="server">
                                    <asp:ListItem Enabled="true" Selected="True" Text="What is the name of your favorite pet?"></asp:ListItem>
                                    <asp:ListItem Enabled="true" Text="In what city were you born?"></asp:ListItem>
                                    <asp:ListItem Enabled="true" Text="What high school did you attend?"></asp:ListItem>
                                    <asp:ListItem Enabled="true" Text="What is your favorite movie?"></asp:ListItem>
                                    <asp:ListItem Enabled="true" Text="What is your mother's maiden name?"></asp:ListItem>
                                    <asp:ListItem Enabled="true" Text="What was the make of your first car?"></asp:ListItem>
                                    <asp:ListItem Enabled="true" Text="What is your favorite color?"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="FieldStyle">
                                Security Answer<asp:Label ID="lblAnsastric" runat="server" Text="*" Visible="false" ForeColor="Red"></asp:Label>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="Answer" runat="server" Text=""></asp:TextBox>
                                <asp:RequiredFieldValidator ID="AnswerRequired" runat="server" ControlToValidate="Answer"
                                    ErrorMessage="Security Answer is required." ToolTip="Security Answer is required." ValidationGroup="EditContact"
                                    CssClass="Error" Display="Dynamic">
                                </asp:RequiredFieldValidator>
                            </div>
                        </asp:Panel>
                    </div>--%>
                    <div class="FieldStyle">
                        Subscription Email 
                        <asp:Label ID="lblEMailastric" runat="server" ForeColor="Red" Text="*" Visible="False"></asp:Label>
                        <br />
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtEmail" runat="server" Width="131px" CssClass="Email"></asp:TextBox>
                        &nbsp;<asp:RequiredFieldValidator ID="EmailAddressRequired" runat="server" ControlToValidate="txtEmail" CssClass="Error" Display="Dynamic" ErrorMessage="Email Address is required." ToolTip="Email Address is required." ValidationGroup="EditContact"></asp:RequiredFieldValidator>
                        <div>
                            <asp:RegularExpressionValidator ID="Regularexpressionvalidator1" runat="server" ControlToValidate="txtEmail"
                                ErrorMessage="*Please use a valid email address." Display="Dynamic" ValidationExpression="[\w\.-]+(\+[\w-]*)?@([\w-]+\.)+[\w-]+"
                                ValidationGroup="EditContact" CssClass="Error"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <%--<h4 class="SubTitle">Contact Information</h4>--%>

                    <div class="FieldStyle">
                    </div>
                    <asp:Panel runat="server" ID="pnlCustomerInformation">
                        <div class="ValueStyle">
                            <asp:CheckBox ID="chkOptIn" runat="server" Text="Send periodic emails and special offers to user" />
                        </div>
                        <div class="FieldStyle">
                        </div>
                        <div class="ValueStyle">
                            <asp:CheckBox ID="chkWelcomeEmail" runat="server" Text="Send welcome email to user" />
                        </div>
                        <h4 class="SubTitle">Custom Information</h4>
                        <div>
                            <div class="FieldStyle">
                                Company Name 
                                <asp:Label ID="lblaccntnumastric0" runat="server" ForeColor="Red" Text="*"
                                    Visible="False"></asp:Label>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtCompanyName" runat="server" />&nbsp;&nbsp;<asp:RequiredFieldValidator
                                    ID="CmpnyNameRequired" runat="server" ControlToValidate="txtCompanyName"
                                    CssClass="Error" Display="Dynamic" Enabled="False"
                                    ErrorMessage="Company Name is required." ToolTip="Company Name is required."
                                    ValidationGroup="EditContact"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div>
                            <div class="FieldStyle">
                                Website
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtWebSite" runat="server" />
                            </div>
                        </div>
                        <div>
                            <div class="FieldStyle">
                                Source
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtSource" runat="server" />
                            </div>
                        </div>
                        <div class="FieldStyle">
                            Custom1
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtCustom1" runat="server" TextMode="MultiLine" Width="400" Height="100"
                                MaxLength="2"></asp:TextBox>
                        </div>
                        <%-- <div class="FieldStyle">
                            Custom2
                        </div>--%>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtCustom2" runat="server" TextMode="MultiLine" Width="400" Height="100"
                                MaxLength="2" Visible="false"></asp:TextBox>
                        </div>
                        <%-- <div class="FieldStyle">
                            Custom3
                        </div>--%>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtCustom3" runat="server" TextMode="MultiLine" Width="400" Height="100"
                                MaxLength="2" Visible="false"></asp:TextBox>
                        </div>
                        <uc1:Spacer ID="Spacer4" SpacerHeight="1" SpacerWidth="3" runat="server"></uc1:Spacer>
                        <div class="FieldStyle">
                            Description
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Width="400"
                                Height="200" MaxLength="2" />
                        </div>
                    </asp:Panel>
                    <uc1:Spacer ID="Spacer2" SpacerHeight="1" SpacerWidth="3" runat="server"></uc1:Spacer>
                </ContentTemplate>
            </asp:UpdatePanel>
            <uc1:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
            <uc1:Spacer ID="Spacer1" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
            <div class="ClearBoth">
                <br />
                <asp:ImageButton ID="btnSubmitBottom" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true"
                    ValidationGroup="EditContact" />
                <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $j('.UserName').keyup(function () {
        $j('.Email').val(this.value);
    });
</script>