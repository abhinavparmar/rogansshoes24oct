﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountUnlock.ascx.cs" Inherits="SiteAdmin.Controls.Default.Accounts.AccountUnlock" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>

<h5>Please Confirm<ZNode:DemoMode ID="DemoMode1" runat="server" />
</h5>
<p>
    Please confirm that you want to <strong>enable</strong> the account  <asp:Label ID="lblAccountName" runat="server" Text=""></asp:Label>.
</p>
<asp:Label ID="lblMsg" runat="server" Width="450px" CssClass="Error"></asp:Label><br />
<br />
<div class="ImageButtons">
    <asp:Button ID="btnUnlock" CausesValidation="False" Text="Enable Account" CssClass="Size150"
        runat="server" OnClick="BtnUnlockUSer_Click" />
    <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
        runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
</div>
<br />
<br />
<br />

