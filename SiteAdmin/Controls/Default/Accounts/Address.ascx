﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Address.ascx.cs" Inherits="SiteAdmin.Controls.Default.Accounts.Address" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>

<div class="FormView" align="center">
    <div>
        <div class="LeftFloat" style="width: 30%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
            <uc2:DemoMode ID="DemoMode1" runat="server" />
        </div>
        <div style="float: right">
            <asp:ImageButton ID="btnSubmitTop" ValidationGroup="EditContact"
                ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif" runat="server" AlternateText="Submit"
                OnClick="BtnSubmit_Click" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
        <div class="ClearBoth">
        </div>
        <div align="left">
            <div>
                <asp:Label ID="lblErrorMsg" runat="server" CssClass="Error" EnableViewState="false"></asp:Label>
            </div>
            <div>
                <uc1:Spacer ID="Spacer8" SpacerHeight="2" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
            <div class="ClearBoth">
                <div id="tblAddress" runat="server" class="LeftFloat">
                    <div class="FieldStyle">
                        Address Name<br />
                        <small>Example: "Home Address".</small>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtName" runat="server" Width="180" Columns="30" MaxLength="100" />
                        <asp:RequiredFieldValidator ID="rfvAddressName" runat="server" ControlToValidate="txtName"
                            ValidationGroup="EditContact" ErrorMessage="Address Name required." CssClass="Error"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="FieldStyle">
                        First Name
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtFirstName" runat="server" Width="180" Columns="30" MaxLength="100" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFirstName"
                            ValidationGroup="EditContact" ErrorMessage="First Name required." CssClass="Error"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="FieldStyle">
                        Last Name
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtLastName" runat="server" Width="180" Columns="30" MaxLength="100"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtLastName"
                            ValidationGroup="EditContact" ErrorMessage="Last Name required." CssClass="Error"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="FieldStyle">
                        Company Name
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtCompanyName" runat="server" Width="180" Columns="30" MaxLength="100"></asp:TextBox>
                    </div>
                    <div class="FieldStyle">
                        Street 1
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtStreet1" runat="server" Width="180" Columns="30" MaxLength="100"></asp:TextBox>
                    </div>
                    <div class="FieldStyle">
                        Street 2
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtStreet2" runat="server" Width="180" Columns="30" MaxLength="100"></asp:TextBox>
                    </div>
                    <div class="FieldStyle">
                        City
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtCity" runat="server" Width="180" Columns="30" MaxLength="100"></asp:TextBox>
                    </div>
                    <div class="FieldStyle">
                        Country
                    </div>
                    <div class="ValueStyle">
                        <asp:DropDownList ID="ddlCountryCode" Width="185" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCountryCode_OnSelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <div class="FieldStyle">
                        State/Province/Region
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtState" runat="server" Width="180" Columns="10"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" ErrorMessage="<%$ Resources:CommonCaption, StateRequired %>" SetFocusOnError="true"
                            ControlToValidate="txtState" runat="server" Display="Dynamic" CssClass="Error" ValidationGroup="EditContact"></asp:RequiredFieldValidator>
                        <asp:DropDownList ID="ddlState" runat="server" Width="180" Visible="false"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="reqBillingStateCode" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, SelectStateRequired %>" ControlToValidate="ddlState" InitialValue="<%$ Resources:CommonCaption, DefaultSelectedState %>" CssClass="Error" ValidationGroup="EditContact"></asp:RequiredFieldValidator>
                    </div>
                    <div class="FieldStyle">
                        Postal Code
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtPostalCode" runat="server" Width="180" Columns="30" MaxLength="20"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="rgxBillingPostalCodeValidation" runat="server" ControlToValidate="txtPostalCode" Display="Dynamic"
                            CssClass="Error" SetFocusOnError="true" ErrorMessage="Please enter valid zip code" ValidationExpression="^\d{5}(-\d{4})?$"
                            ValidationGroup="EditContact"></asp:RegularExpressionValidator>
                    </div>
                    <div class="FieldStyle">
                    </div>
                    <div class="ValueStyle">
                        <asp:CheckBox ID="chkIsPOBox" runat="server" ToolTip="P.O. Box" Text="Please check here if this is a P.O. Box" />
                        <asp:HiddenField ID="hdnAddressExtnID" runat="server" />
                    </div>
                    <div class="FieldStyle">
                        Phone Number
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtPhoneNumber" runat="server" Width="180" Columns="30" MaxLength="100"></asp:TextBox>
                    </div>
                    <div class="FieldStyle">
                    </div>
                    <div class="ValueStyle">
                        <asp:CheckBox ID="chkUseAsDefaultBillingAddress" Text="This address is my billing address"
                            TextAlign="Right" runat="server" Checked="false" />
                    </div>
                    <div class="FieldStyle">
                    </div>
                    <div class="ValueStyle">
                        <asp:CheckBox ID="chkUseAsDefaultShippingAddress" Text="This address is my shipping address"
                            TextAlign="Right" runat="server" Checked="true" />
                    </div>
                </div>
            </div>
        </div>
        <div class="ClearBoth">
        </div>
    </div>
    <br />
    <uc1:Spacer ID="Spacer2" SpacerHeight="1" SpacerWidth="3" runat="server"></uc1:Spacer>
    <div class="ClearBoth" style="text-align: left">
        <br />
        <asp:ImageButton ID="btnSubmitBottom" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
            runat="server" AlternateText="Submit" ValidationGroup="EditContact" OnClick="BtnSubmit_Click"
            CausesValidation="true" />
        <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
            runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
    </div>
</div>
