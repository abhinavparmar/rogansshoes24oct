﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="AffiliatePayment.ascx.cs" Inherits="SiteAdmin.Controls.Default.Accounts.AffiliatePayment" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>

<ZNode:DemoMode ID="DemoMode1" runat="server" />
<div class="FormView">
    <div class="LeftFloat" style="width: 70%; text-align: left">
        <h1>
            <asp:Label ID="lblTitle" Text="Add Payment" runat="server"></asp:Label></h1>
    </div>
    <div style="text-align: right">
        <asp:ImageButton ID="btnSubmitTop" runat="server" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
            OnClick="BtnSubmit_Click" AlternateText="Submit"/>
        <asp:ImageButton ID="btnCancelTop" runat="server" CausesValidation="false" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
            OnClick="BtnCancel_Click" AlternateText="Cancel"/>
    </div>
    <div class="ClearBoth">
        <ZNode:Spacer ID="Spacer1" SpacerHeight="15" SpacerWidth="3" runat="server" />
    </div>
    <div class="FieldStyle">
        Amount Owed
    </div>
    <div class="ValueStyle">
        <asp:Label ID="lblAmountOwed" runat="server" Text="" />
    </div>
    <div class="FieldStyle">
        Payment Description<br />
        <small>Enter a short meaningful description for this payment.</small>
    </div>
    <div class="ValueStyle">
        <asp:TextBox ID="txtDescription" runat="server" Columns="30" Rows="5" TextMode="MultiLine" />
    </div>
    <div class="FieldStyle">
        Payment Date<span class="Asterix">*</span>
    </div>
    <div class="ValueStyle">
        <asp:TextBox ID="txtDate" Text='' runat="server" />
        &nbsp;<asp:Image ID="imgbtnStartDt" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/SiteAdmin/Themes/images/SmallCalendar.gif" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter received date"
            ControlToValidate="txtDate" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtDate"
            CssClass="Error" Display="Dynamic" ErrorMessage="Enter Valid Date in MM/DD/YYYY format"
            ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"></asp:RegularExpressionValidator>
        <ajaxToolKit:CalendarExtender ID="CalendarExtender1" Enabled="true" PopupButtonID="imgbtnStartDt"
            runat="server" TargetControlID="txtDate">
        </ajaxToolKit:CalendarExtender>
    </div>
    <div class="FieldStyle">
        Amount<span class="Asterix">*</span>
    </div>
    <div class="ValueStyle">
        <asp:TextBox ID="txtAmount" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Enter a amount."
            ControlToValidate="txtAmount" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtAmount"
            Type="Currency" Operator="DataTypeCheck" ErrorMessage="You must enter a valid amount."
            CssClass="Error" Display="Dynamic" />
        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtAmount"
            ErrorMessage="You must enter a amount." MaximumValue="99999999" Type="Currency"
            MinimumValue="0" Display="Dynamic" CssClass="Error"></asp:RangeValidator>
    </div>
    <div class="ClearBoth">
    </div>
    <div>
        <asp:Label ID="ErrorMessage" runat="server" CssClass="Error"></asp:Label>
    </div>
    <ZNode:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="3" runat="server" />
    <div>
        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
            OnClick="BtnSubmit_Click" AlternateText="Submit"/>
        <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="false" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
            OnClick="BtnCancel_Click" AlternateText="Cancel"/>
    </div>
</div>

