﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Controls.Default.Accounts
{
    public partial class AddNote : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private int AccountId;
        private string _RoleName = string.Empty;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets a value for RoleName
        /// </summary>
        public string RoleName
        {
            get
            {
                return this._RoleName;
            }

            set
            {
                this._RoleName = value;
            }
        }
        #endregion

        #region General Events

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            btnSubmitTop.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_submit_highlight.gif") + "'");
            btnSubmitTop.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_submit.gif") + "'");
            btnCancelTop.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_cancel_highlight.gif") + "'");
            btnCancelTop.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_cancel.gif") + "'");

            btnSubmitBottom.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_submit_highlight.gif") + "'");
            btnSubmitBottom.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_submit.gif") + "'");
            btnCancelBottom.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_cancel_highlight.gif") + "'");
            btnCancelBottom.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_cancel.gif") + "'");

            if (Request.Params["itemid"] != null)
            {
                this.AccountId = int.Parse(Request.Params["itemid"].ToString());
            }
            if (Request.Params["pagefrom"] != null)
            {
                this.RoleName = Request.Params["pagefrom"].ToString();
            }
        }

        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            // Validate the controls
            bool isValid = Validate();

            if (isValid)
            {
                NoteAdmin _Noteadmin = new NoteAdmin();
                Note _NoteAccess = new Note();

                _NoteAccess.CaseID = null;
                _NoteAccess.AccountID = this.AccountId;
                _NoteAccess.CreateDte = System.DateTime.Now;
                _NoteAccess.CreateUser = HttpContext.Current.User.Identity.Name;
                _NoteAccess.NoteTitle = Server.HtmlEncode(txtNoteTitle.Text.Trim());
                _NoteAccess.NoteBody = ctrlHtmlText.Html;

                bool Check = _Noteadmin.Insert(_NoteAccess);

                if (Check)
                {
                    AccountAdmin _accountAdmin = new AccountAdmin();
                    Account _account = new Account();
                    _account = _accountAdmin.GetByAccountID(this.AccountId);

                    string UserName = string.Empty;
                    if (_account.UserID != null)
                    {
                        MembershipUser _user = Membership.GetUser(_account.UserID);
                        UserName = _user.UserName;
                    }

                    string AssociateName = "Creation of Note - " + txtNoteTitle.Text.Trim();

                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, UserName);

                    // Redirect to main page
                    if (RoleName.ToLower() == "admin")
                    {
                        Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/StoreAdministrators/View.aspx?itemid=" + this.AccountId + "&Mode=notes&pagefrom=admin");
                    }
                    else if (RoleName.ToLower() == "franchise")
                    {
                        Response.Redirect("~/SiteAdmin/Secure/Vendors/FranchiseAdministrators/View.aspx?itemid=" + this.AccountId + "&Mode=notes&pagefrom=franchise");
                    }
                    else if (RoleName.ToLower() == "vendor")
                    {
                        Response.Redirect("~/SiteAdmin/Secure/Vendors/VendorAccounts/View.aspx?itemid=" + this.AccountId + "&Mode=notes&pagefrom=vendor");
                    }
                    else
                    {
                        Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/View.aspx?itemid=" + this.AccountId + "&Mode=notes&pagefrom=customer");
                    }
                }
                else
                {
                    // Display error message
                    lblError.Text = "An error occurred while updating. Please try again.";
                    lblError.Visible = true;
                }
            }
        }
        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (RoleName.ToLower() == "admin")
            {
                Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/StoreAdministrators/View.aspx?itemid=" + this.AccountId + "&Mode=notes&pagefrom=admin");
            }
            else if (RoleName.ToLower() == "franchise")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/FranchiseAdministrators/View.aspx?itemid=" + this.AccountId + "&Mode=notes&pagefrom=franchise");
            }
            else if (RoleName.ToLower() == "vendor")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/VendorAccounts/View.aspx?itemid=" + this.AccountId + "&Mode=notes&pagefrom=vendor");
            }
            else
            {
                Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/View.aspx?itemid=" + this.AccountId + "&Mode=notes&pagefrom=customer");
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Validate the controls
        /// </summary>
        /// <returns></returns>
        private bool Validate()
        {
            bool isValid = true;
            if (string.IsNullOrEmpty(txtNoteTitle.Text))
            {
                //Display error message for Note Title
                lblErrorTitle.Visible = true;
                lblErrorTitle.Text = "* Enter Note Title";
                isValid = false;
            }
            else
            {
                lblErrorTitle.Visible = false;
            }
            
            if (string.IsNullOrEmpty(ctrlHtmlText.Html))
            {
                // Display error message for body note
                lblError.Visible = true;
                lblError.Text = "* Enter Body Note";
                isValid = false;
            }
            else
            {
                lblError.Visible = false;
            }

            return isValid;
        }
        #endregion
    }
}