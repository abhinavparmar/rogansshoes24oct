﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Controls.Default.Accounts
{
    public partial class AccountDelete : System.Web.UI.UserControl
    {
        #region Procted Member Variables
        private int ItemId = 0;
        private AccountAdmin accountAdmin = new AccountAdmin();
        private OrderAdmin orderAdmin = new OrderAdmin();
        private string pageFrom = string.Empty;
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            btnDelete.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_delete_highlight.gif") + "'");
            btnDelete.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_delete.gif") + "'");
            btnCancel.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_cancel_highlight.gif") + "'");
            btnCancel.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_cancel.gif") + "'");

            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }

            if (Request.Params["pagefrom"] != null)
            {
                this.pageFrom = Request.Params["pagefrom"].ToString();
            }

            if (!Page.IsPostBack)
            {
                Account _account = this.accountAdmin.GetByAccountID(this.ItemId);

                if (_account.UserID != null)
                {
                    MembershipUser _user = Membership.GetUser(_account.UserID.Value);

                    string roleList = string.Empty;

                    // Get roles for this User account
                    string[] roles = Roles.GetRolesForUser(_user.UserName);

                    foreach (string Role in roles)
                    {
                        roleList += Role + "<br>";
                    }

                    string rolename = roleList;

                    // Hide the Delete button if a NonAdmin user has entered this page
                    if (!Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "ADMIN"))
                    {
                        if (Roles.IsUserInRole(_user.UserName, "ADMIN"))
                        {
                            btnDelete.Visible = false;
                        }
                        else if (Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "CUSTOMER SERVICE REP"))
                        {
                            if (rolename == Convert.ToString("USER<br>") || rolename == Convert.ToString(string.Empty))
                            {
                                btnDelete.Visible = true;
                            }
                            else
                            {
                                btnDelete.Visible = false;
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Events
        /// <summary>
        /// Delete button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            CaseAdmin caseAdmin = new CaseAdmin();
            NoteAdmin noteAdmin = new NoteAdmin();
            AccountProfileAdmin accountProfile = new AccountProfileAdmin();

            Account _account = this.accountAdmin.GetByAccountID(this.ItemId);
            string genericErrorMessage = "Unable to delete this account. Please remove any child relationship with this account.";

            Guid UserKey = new Guid();
            TransactionManager tranManager = ConnectionScope.CreateTransaction();
            try
            {
                if (_account != null)
                {
                    if (_account.UserID.HasValue)
                    {
                        // Get UserId for this account
                        UserKey = _account.UserID.Value;
                    }

                    int ordercount = this.orderAdmin.GetTotalOrderItemsByAccountId(this.ItemId);
                    if (ordercount <= 0)
                    {
                        // Delete the Profiles for this AccountId
                        TList<AccountProfile> tAP = accountProfile.DeepLoadProfilesByAccountID(this.ItemId);
                        foreach (AccountProfile ap in tAP)
                        {
                            accountProfile.Delete(ap.AccountProfileID);
                        }

                        // Delete the note for this AccountId.
                        noteAdmin.DeleteByAccountId(this.ItemId);

                        // Delete the case for this AccountId.
                        caseAdmin.DeleteByAccountId(this.ItemId);

                        // Delete accounts all address 
                        bool isSuccess = false;
                        TList<ZNode.Libraries.DataAccess.Entities.Address> addressList = this.accountAdmin.GetAllAddressByAccountID(_account.AccountID);
                        foreach (ZNode.Libraries.DataAccess.Entities.Address address in addressList)
                        {
                            isSuccess = this.accountAdmin.DeleteAddress(address.AddressID);

                            // Address delete failed.
                            if (!isSuccess)
                            {
                                if (tranManager.IsOpen)
                                {
                                    tranManager.Rollback();
                                }
                            }
                        }

                        isSuccess = this.accountAdmin.Delete(_account);
                        if (isSuccess)
                        {
                            // Check 
                            // Get user by Unique GUID - User Id
                            MembershipUser user = Membership.GetUser(UserKey);
                            if (user != null)
                            {
                                string AssociateName = "Delete Account - " + user.UserName;
                                string AccountName = user.UserName;

                                // Delete online account
                                isSuccess = Membership.DeleteUser(user.UserName);

                                if (!isSuccess)
                                {
                                    if (tranManager.IsOpen)
                                    {
                                        tranManager.Rollback();
                                    }

                                    lblMsg.Text = genericErrorMessage;
                                }
                                else
                                {
                                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, AccountName);
                                }
                            }

                            if (tranManager.IsOpen)
                            {
                                tranManager.Commit();
                            }

                            // Redirect to list page
                            if (pageFrom.ToLower() == "admin")
                            {
                                Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/StoreAdministrators/Default.aspx");
                            }
                            else if (pageFrom.ToLower() == "franchise")
                            {
                                Response.Redirect("~/SiteAdmin/Secure/Vendors/FranchiseAdministrators/Default.aspx");
                            }
                            else if (pageFrom.ToLower() == "vendor")
                            {
                                Response.Redirect("~/SiteAdmin/Secure/Vendors/VendorAccounts/Default.aspx");
                            }
                            else
                            {
                                Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/Default.aspx");
                            }
                        }
                        else
                        {
                            // Delete account failed.
                            if (tranManager.IsOpen)
                            {
                                tranManager.Rollback();
                            }

                            lblMsg.Text = genericErrorMessage;
                        }
                    }
                    else
                    {
                        lblMsg.Text = genericErrorMessage;
                    }
                }
            }
            catch (Exception ex)
            {
                if (tranManager.IsOpen)
                {
                    tranManager.Rollback();
                }

                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);

                lblMsg.Text = genericErrorMessage;
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            // Redirect to list page
            if (pageFrom.ToLower() == "admin")
            {
                Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/StoreAdministrators/Default.aspx");
            }
            else if (pageFrom.ToLower() == "franchise")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/FranchiseAdministrators/Default.aspx");
            }
            else if (pageFrom.ToLower() == "vendor")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/VendorAccounts/Default.aspx");
            }
            else
            {
                Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/Default.aspx");
            }
        }
        #endregion
    }
}