﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="View.ascx.cs" Inherits="SiteAdmin.Controls.Default.Accounts.View" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<div>
    <div>
        <div class="LeftFloat" style="width: 30%; text-align: left">
            <h1 style="width: 700px;">
                    <asp:Label ID="lblCustomerDetails" runat="server" Text="Label"></asp:Label></h1>
        </div>
        <div style="float: right" class="ImageButtons">
            <asp:Button ID="CustomerList" runat="server" Text="< Back" CssClass="Size100"
                OnClick="CustomerList_Click" />
        </div>
        <div class="ClearBoth">
        </div>
        <div align="left">
            <ajaxToolKit:TabContainer ID="tabCustomerDetails" runat="server">
                <ajaxToolKit:TabPanel ID="pnlGeneral" runat="server">
                    <HeaderTemplate>
                        General
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div>
                            <ZNode:Spacer ID="Spacer7" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>
                        <div class="ImageButtons">
                            <asp:Button ID="EditCustomer" runat="server" CssClass="Size100" Text="Edit Account"
                                OnClick="CustomerEdit_Click" />
                        </div>
                        <br />
                        <h4 class="SubTitle">Contact Information</h4>
                        <div class="ViewForm200">
                            <div class="FieldStyleA">
                                Name (Billing)
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblName" runat="server" Text="Label"></asp:Label>&nbsp;
                            </div>

                            <div class="FieldStyle">
                                Company
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblCompanyName" runat="server" Text="Label"></asp:Label>&nbsp;
                            </div>

                            <div class="FieldStyleA">
                                Phone (Billing)
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblPhone" runat="server" Text="Label"></asp:Label>&nbsp;
                            </div>

                            <div class="FieldStyle">
                                Email
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblEmail" runat="server" Text="Label"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyleA" nowrap="nowrap">
                                Email Opt In
                            </div>
                            <div valign="top" class="ValueStyleA">
                            <img id="EmailOptin" runat="server" />
                                  &nbsp;&nbsp;</div>

                            <h4 class="SubTitle">Account Information</h4>
                            <div class="FieldStyleA">
                                Account ID
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblAccountID" runat="server" Text="Label"></asp:Label>&nbsp;
                            </div>
                            
                            <div class="FieldStyle">
                                Account Number
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblExternalAccNumber" runat="server" Text="Label" Width="98px"></asp:Label>&nbsp;
                            </div>

                            <div class="FieldStyleA">
                                Login Name
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblLoginName" runat="server" Text="Label"></asp:Label>&nbsp;
                            </div>
                            
                            <ZNode:Spacer ID="Spacer11" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            
                            <h4 class="SubTitle">Addresses</h4>
                            <div>
                                <div class="ImageButtons">
                                    <asp:Button ID="btnAddNewAddress" runat="server" CssClass="Size200"
                                        Text="Add New Address" OnClick="BtnAddNewAddress_Click" />
                                </div>
                                <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>

                                <asp:GridView ID="gvAddress" ShowFooter="True" CaptionAlign="Left" runat="server"
                                    CellPadding="4" AutoGenerateColumns="False" CssClass="Grid" Width="100%" GridLines="None"
                                    AllowPaging="True" EmptyDataText="No addresses were found." PageSize="5" OnPageIndexChanging="GvAddress_PageIndexChanging"
                                    OnRowCommand="GvAddress_RowCommand">
                                    <Columns>
                                        <asp:BoundField DataField="Name" HeaderText="Name">
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Default Shipping">
                                            <ItemTemplate>
                                                <img id="Img11" runat="server" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "IsDefaultShipping").ToString()))%>' />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Default Billing">
                                            <ItemTemplate>
                                                <img alt="" id="Img12" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "IsDefaultBilling").ToString()))%>'
                                                    runat="server" />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddress" runat="server" Text='<%# GetAddress(DataBinder.Eval(Container.DataItem, "AddressID").ToString())%>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="Edit" runat="server" CommandArgument='<%# Eval("AddressID") %>'
                                                    CommandName="Edit" CssClass="actionlink" Text="Edit »" />
                                                <asp:LinkButton ID="Delete" runat="server" CommandArgument='<%# Eval("AddressID") %>'
                                                    CommandName="Remove" CssClass="actionlink" Text="Remove »" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="RowStyle" />
                                    <HeaderStyle CssClass="HeaderStyle" />
                                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                    <FooterStyle CssClass="FooterStyle" />
                                </asp:GridView>

                            </div>
                            <div class="Error">
                            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label></div>
                            <div class="ClearBoth"></div>
                            <h4 class="SubTitle">Additional Information</h4>
                            <div>
                            
                                <div class="FieldStyleA">
                                    Website
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblWebSite" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyle">
                                    Description
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblDescription" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyleA">
                                    Source
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblSource" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>
                            
                                <div class="FieldStyle">
                                    Create Date
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblCreatedDate" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyleA">
                                    Create User
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblCreatedUser" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyle">
                                    Update Date
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblUpdatedDate" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyleA">
                                    Update User
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblUpdatedUser" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>
                            
                                <div class="FieldStyle">
                                    Custom1
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblCustom1" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyleA">
                                    Custom2
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblCustom2" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyle">
                                    Custom3
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblCustom3" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>
                            
                            </div>
                        </div>
                        
                        <div class="ClearBoth">
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="pnlSiteAdminGeneral" runat="server">
                    <HeaderTemplate>
                        General
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div>
                            <ZNode:Spacer ID="Spacer124" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>
                        <div class="ImageButtons">
                            <asp:Button ID="Button1" runat="server" CssClass="Size100" Text="Edit Account"
                                OnClick="CustomerEdit_Click" />
                        </div>
                        <br />
                        <h4 class="SubTitle">General Information</h4>
                        <div class="ViewForm200">
                            <div class="FieldStyleA">
                                Login Name
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblSiteLoginName" runat="server" Text=""></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyle">
                                Account Number
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblSiteAccountNumber" runat="server" Text="" Width="98px"></asp:Label>&nbsp;
                            </div>
                            <ZNode:Spacer ID="Spacer13" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            <h4 class="SubTitle">Contact Information</h4>
                            <div class="FieldStyleA">
                                Email
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblSiteEmail" runat="server" Text=""></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyle" nowrap="nowrap">
                                Email Opt In
                            </div>
                            <div valign="top" class="ValueStyle">
                                <img id="SiteEmailOptin" runat="server" />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                            <ZNode:Spacer ID="Spacer14" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            <div>
                                <ZNode:Spacer ID="Spacer18" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <h4 class="SubTitle">Additional Information</h4>
                            <div>
                                <div class="FieldStyleA">
                                    Create Date
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblSiteCreateDate" runat="server" Text=""></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyle">
                                    Create User
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblSiteCreateUser" runat="server" Text=""></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyleA">
                                    Update Date
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblSiteUpdateDate" runat="server" Text=""></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyle">
                                    Update User
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblSiteUpdateUser" runat="server" Text=""></asp:Label>&nbsp;
                                </div>
                            </div>
                        </div>
                        <asp:Label ID="Label19" runat="server" CssClass="Error"></asp:Label>
                        <div class="ClearBoth">
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="tabpnlOrders" runat="server" Enabled="false">
                    <HeaderTemplate>
                        Orders
                    </HeaderTemplate>
                    <ContentTemplate>
                        <h4 class="SubTitle">Order List</h4>
                        <asp:GridView ID="uxGrid" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            CaptionAlign="Left" CellPadding="4" CssClass="Grid" EmptyDataText="No Orders were found for this account."
                            GridLines="None" Width="100%" OnRowDataBound="UxGrid_RowDataBound" OnPageIndexChanging="UxGrid_PageIndexChanging">
                            <Columns>
                                <asp:TemplateField HeaderText="Order ID" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hlinkOrder" runat="server" Text='<%# Eval("orderid") %>'></asp:HyperLink>
                                        <asp:HiddenField ID="HdnId" runat="server" Value='<%# Eval("orderid") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:BoundField DataField="Orderdate" DataFormatString="{0:MM/dd/yyyy hh:mm}" HeaderText="Order Date"
                                    HtmlEncode="False" SortExpression="OrderDate" HeaderStyle-HorizontalAlign="Left" />
                                <asp:TemplateField HeaderText="Order Amount" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" SortExpression="total" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="TotalAmount" Text='<%# FormatPrice(Eval("total")) %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Order Status" SortExpression="OrderStateID" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="OrderStatus" Text='<%# DisplayOrderStatus(Eval("OrderStateID")) %>'
                                            runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="RowStyle" />
                            <EditRowStyle CssClass="EditRowStyle" />
                            <HeaderStyle CssClass="HeaderStyle" />
                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                        </asp:GridView>
                        <div>
                            <ZNode:Spacer ID="Spacer15" SpacerHeight="1" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="tabpnlNotes" runat="server">
                    <HeaderTemplate>
                        Notes
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div>
                            <ZNode:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>
                        <div class="ButtonStyle">
                            <zn:LinkButton ID="AddNewNote" runat="server"
                                ButtonType="Button" OnClick="AddNewNote_Click" Text="Add Note"
                                ButtonPriority="Primary" />
                        </div>
                        <div>
                            <ZNode:Spacer ID="Spacer4" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>
                        <h4 class="SubTitle">Note List</h4>
                        <asp:Repeater ID="CustomerNotes" runat="server">
                            <ItemTemplate>
                                <asp:Label ID="Label1" CssClass="ValueStyle" runat="Server" Text='<%# FormatCustomerNote(Eval("NoteTitle"),Eval("CreateUser"),Eval("CreateDte")) %>' />
                                <ZNode:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                                <asp:Label ID="Label2" CssClass="ValueStyle" runat="Server" Text='<%# Eval("NoteBody") %>' />
                                <ZNode:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </ItemTemplate>
                        </asp:Repeater>
                        <div>
                            <ZNode:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="pnlProfile" runat="server">
                    <HeaderTemplate>
                        Profiles
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div>
                            <ZNode:Spacer ID="Spacer5" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>
                        <div class="ButtonStyle">
                            <zn:LinkButton ID="btnAddProfile" runat="server"
                                ButtonType="Button" OnClick="AddProfile_Click" Text="Associate Profile"
                                ButtonPriority="Primary" />
                        </div>
                        <div>
                            <ZNode:Spacer ID="Spacer16" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>
                        <div>
                            <div class="Error">
                                <asp:Label ID="lblProfileError" runat="server" Text="" />
                            </div>
                            <div>
                                <!--Update Panel for grid paging that are used to avoid the postbacks-->
                                <asp:UpdatePanel ID="updProfileGrid" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:GridView ID="uxProfileGrid" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
                                            runat="server" CellPadding="4" AutoGenerateColumns="False" CssClass="Grid" Width="100%"
                                            GridLines="None" OnRowCommand="UxProfileGrid_RowCommand" AllowPaging="True" OnPageIndexChanging="UxProfileGrid_PageIndexChanging"
                                            OnRowDeleting="UxProfileGrid_RowDeleting" PageSize="5">
                                            <Columns>
                                                <asp:BoundField DataField="ProfileID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                                                <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <%# Eval("ProfileIDSource.Name")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Use WholeSale Pricing" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <img alt="" id="UseWholeSalePricingImg" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ProfileIDSource.UseWholeSalePricing").ToString()))%>'
                                                            runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Show Pricing" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <img alt="" id="ShowPricingImg" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ProfileIDSource.ShowPricing").ToString()))%>'
                                                            runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="Delete" Text="Remove &raquo" CommandArgument='<%# Eval("AccountProfileID") %>'
                                                            CommandName="Remove" CssClass="actionlink" runat="server" />
                                                        <asp:HiddenField ID="hdnProfileName" runat="server" Value='<%# Eval("ProfileIDSource.Name")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                No profiles found.
                                            </EmptyDataTemplate>
                                            <RowStyle CssClass="RowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                            <FooterStyle CssClass="FooterStyle" />
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="tabpnlAffiliate" runat="server">
                    <HeaderTemplate>
                        Affiliate 
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div>
                            <ZNode:Spacer ID="Spacer12" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>
                        <div class="ImageButtons">
                            <asp:Button ID="TrackingEdit" runat="server" Text="Edit Tracking" OnClick="TrackingEdit_Click" />
                        </div>
                        <br />
                        <h4 class="SubTitle">Tracking Information</h4>
                        <div class="ViewForm200">
                            <div class="FieldStyle">
                                Tracking Link
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblAffiliateLinkError" runat="server" CssClass="Error"></asp:Label>
                                <asp:HyperLink ID="hlAffiliateLink" runat="server" Target="_blank"></asp:HyperLink>
                            </div>
                            <div class="FieldStyleA">
                                Commission Type
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblReferralType" runat="server" Text=""></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyle">
                                Commission
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblReferralCommission" runat="server" Text="" Width="98px"></asp:Label>
                            </div>
                            <div class="FieldStyleA">
                                TaxID
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblTaxId" runat="server" Text="Label"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyle">
                                Partner Approval Status
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblReferralStatus" runat="server" Text="Label"></asp:Label>
                            </div>
                            <div class="FieldStyleA">
                                Amount Owed
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblAmountOwed" runat="server"></asp:Label>&nbsp;
                            </div>
                        </div>
                        <br />
                        <h4 class="SubTitle">Referral Commission</h4>
                        <asp:GridView ID="uxReferralCommission" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            CaptionAlign="Left" CellPadding="4" CssClass="Grid" PageSize="10" OnPageIndexChanging="UxReferralCommission_PageIndexChanging"
                            EmptyDataText="No referral commissions were found for this account." GridLines="None" 
                            Width="100%">
                            <Columns>
                                <asp:HyperLinkField DataNavigateUrlFields="OrderId" DataNavigateUrlFormatString="~/SiteAdmin/Secure/Orders/OrderManagement/ViewOrders/View.aspx?itemid={0}"
                                    DataTextField="OrderId" HeaderText="Order ID" SortExpression="OrderId" HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="ReferralCommission" HeaderText="Commission" DataFormatString="{0:0.00}"
                                    HeaderStyle-HorizontalAlign="Right" />
                                <asp:TemplateField HeaderText="Commission Type" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="ReferralCommissionTypeID" Text='<%# GetCommissionTypeById(Eval("ReferralCommissionTypeID")) %>'
                                            runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ReferralAccountID" Visible="false" HeaderText="Referral Account"
                                    HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="TransactionID" HeaderText="Transaction ID" HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="Description" HeaderText="Description" HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="CommissionAmount" ItemStyle-HorizontalAlign="Right" HeaderText="Commission Amount"
                                    DataFormatString="{0:c}" />
                            </Columns>
                            <RowStyle CssClass="RowStyle" />
                            <EditRowStyle CssClass="EditRowStyle" />
                            <HeaderStyle CssClass="HeaderStyle" />
                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                        </asp:GridView>
                        <asp:Panel ID="pnlAffiliatePayment" runat="server">
                            <div class="ButtonStyle">
                                <zn:LinkButton ID="btnAddPayment" runat="server"
                                    ButtonType="Button" OnClick="AddAffiliatePayment_Click" Text="Add Payment"
                                    ButtonPriority="Primary" />
                            </div>
                            <h4 class="GridTitle">Affiliate Payment Information</h4>
                            <asp:GridView ID="uxPaymentList" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CaptionAlign="Left" CellPadding="4" CssClass="Grid" EmptyDataText="No payments were found for this account."
                                ForeColor="#333333" GridLines="None" Width="100%" OnPageIndexChanging="UxPaymentList_PageIndexChanging">
                                <Columns>
                                    <asp:BoundField HeaderText="Payment Description" DataField="Description" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField HeaderText="Payment Date" DataField="ReceivedDate" DataFormatString="{0:d}"
                                        HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField HeaderText="Amount" DataField="Amount" DataFormatString="{0:c}" HeaderStyle-HorizontalAlign="Left" />
                                </Columns>
                                <RowStyle CssClass="RowStyle" />
                                <EditRowStyle CssClass="EditRowStyle" />
                                <HeaderStyle CssClass="HeaderStyle" />
                                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                            </asp:GridView>
                        </asp:Panel>
                        <div class="ClearBoth">
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="permissionPanel" runat="server" Enabled="false">
                    <HeaderTemplate>
                        Permissions
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div>
                            <ZNode:Spacer ID="Spacer9" runat="server" SpacerHeight="10" SpacerWidth="3" />
                        </div>
                        <div class="ImageButtons">
                            <asp:Button ID="ermissionEdit" runat="server" CssClass="Size175" OnClick="PermissionEdit_Click" Text="Edit Roles And Permissions" />
                        </div>
                        <br />
                        <h4 class="SubTitle">Access To Stores</h4>
                        <div class="ViewForm200">
                            <div class="FieldStyle">
                                Stores
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblstores" runat="server"></asp:Label>
                            </div>
                            <h4 class="SubTitle">User Roles</h4>
                            <div class="FieldStyle">
                                Role
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblRoles" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="ClearBoth">
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
            </ajaxToolKit:TabContainer>
        </div>
    </div>
</div>
