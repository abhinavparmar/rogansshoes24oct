﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountDisable.ascx.cs" Inherits="SiteAdmin.Controls.Default.Accounts.AccountDisable" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>
<h5>Please Confirm<ZNode:DemoMode ID="DemoMode1" runat="server" />
</h5>
<p>
    Please confirm that you want to <strong>disable</strong> the account  <asp:Label ID="lblAccountName" runat="server" Text=""></asp:Label>.
</p>
<asp:Label ID="lblMsg" runat="server" Width="450px" CssClass="Error"></asp:Label><br />
<br />
<div class="ImageButtons">
    <asp:Button CssClass="Size175" ID="btnDelete" CausesValidation="False" Text="Disable Account"
        runat="server" OnClick="BtnDisable_Click" />
    <asp:ImageButton ID="btnCancelTop" CausesValidation="False" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
        runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
</div>
<br />
<br />
<br />

