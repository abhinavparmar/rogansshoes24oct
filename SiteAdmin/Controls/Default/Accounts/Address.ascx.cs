﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Controls.Default.Accounts
{
    public partial class Address : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private int AddressId;
        private int AccountId;
        private int Mode;
        private string _RoleName = string.Empty;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets a value for RoleName
        /// </summary>
        public string RoleName
        {
            get
            {
                return this._RoleName;
            }

            set
            {
                this._RoleName = value;
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            btnSubmitTop.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_submit_highlight.gif") + "'");
            btnSubmitTop.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_submit.gif") + "'");
            btnCancelTop.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_cancel_highlight.gif") + "'");
            btnCancelTop.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_cancel.gif") + "'");

            btnSubmitBottom.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_submit_highlight.gif") + "'");
            btnSubmitBottom.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_submit.gif") + "'");
            btnCancelBottom.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_cancel_highlight.gif") + "'");
            btnCancelBottom.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_cancel.gif") + "'");

            if (HttpContext.Current.Request.Params["itemid"] == null)
            {
                this.AddressId = 0;
            }
            else
            {
                this.AddressId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (HttpContext.Current.Request.Params["accountid"] == null)
            {
                this.AccountId = 0;
            }
            else
            {
                this.AccountId = int.Parse(Request.Params["accountid"].ToString());
            }

            if (HttpContext.Current.Request.Params["mode"] == null)
            {
                this.Mode = 0;
            }
            else
            {
                this.Mode = int.Parse(Request.Params["mode"].ToString());
            }

            if (HttpContext.Current.Request["pagefrom"] != null)
            {
                this.RoleName = Request.Params["pagefrom"].ToString();
            }

            if (!Page.IsPostBack)
            {
                this.BindCountry();

                if (this.AddressId > 0)
                {
                    this.BindData();

                    lblTitle.Text = "Edit Address";
                }
                else
                {
                    //Zeon Custom Code:Start
                    if (ConfigurationManager.AppSettings["AddressCustomCountry"].ToString().ToUpper().Contains(ddlCountryCode.SelectedValue))
                    {
                        BindStateByCountryCode(ddlCountryCode.SelectedValue);
                    }
                    //Zeon Custom Code:Ends
                    lblTitle.Text = "Add Address";
                }

            }
            //Zeon Custom Code:Start
            SetStateControl(ddlState.Visible);
            //Zeon Custom Code:Ends
        }


        /// <summary>
        /// Page_PreRender
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Zeon Custom Code:Start
            this.EnablePostalCodeValidation(ddlCountryCode);
            //Zeon Custom Code:Ends
        }
        #endregion

        #region Event Methods
        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, ImageClickEventArgs e)
        {
            this.RediretPage();
        }

        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, ImageClickEventArgs e)
        {
            this.SubmitPage();
        }

        /// <summary>
        /// Handeles selected indexchange event of ddlCountryCode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlCountryCode_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            BindStateByCountryCode(ddlCountryCode.SelectedValue);
            //Zeon Custom Code:Start
            this.EnablePostalCodeValidation(ddlCountryCode);
            //Zeon Custom Code:Ends
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Bind Account Details
        /// </summary>
        private void BindData()
        {
            ZNode.Libraries.DataAccess.Service.AddressService addressService = new ZNode.Libraries.DataAccess.Service.AddressService();
            ZNode.Libraries.DataAccess.Entities.Address address = addressService.GetByAddressID(this.AddressId);

            if (address != null)
            {
                // Set field values
                txtName.Text = Server.HtmlDecode(address.Name);
                txtFirstName.Text =Server.HtmlDecode(address.FirstName);
                txtLastName.Text = Server.HtmlDecode(address.LastName);
                txtCompanyName.Text = Server.HtmlDecode(address.CompanyName);
                txtStreet1.Text = Server.HtmlDecode(address.Street);
                txtStreet2.Text = Server.HtmlDecode(address.Street1);
                txtCity.Text = address.City;
                //txtState.Text = address.StateCode;
                txtPostalCode.Text = address.PostalCode;
                ddlCountryCode.SelectedIndex = ddlCountryCode.Items.IndexOf(ddlCountryCode.Items.FindByValue(address.CountryCode));
                //Zeon Custom Code:Starts
                if (ConfigurationManager.AppSettings["AddressCustomCountry"].ToString().ToUpper().Contains(address.CountryCode))
                {
                    ListItem item = new ListItem();
                    item.Value = address.StateCode;
                    BindStateByCountryCode(ddlCountryCode.SelectedValue);
                    if (ddlState.Items.FindByValue(address.StateCode.ToString()) != null)
                    {
                        ddlState.SelectedValue = address.StateCode;
                    }
                }
                else
                {
                    txtState.Text = address.StateCode;
                }
                BindAddressExtensionDetails(address.AddressID, address.StateCode);
                //Zeon Custom Code:Ends
                txtPhoneNumber.Text = address.PhoneNumber;
                chkUseAsDefaultBillingAddress.Checked = address.IsDefaultBilling;
                chkUseAsDefaultShippingAddress.Checked = address.IsDefaultShipping;
            }
        }

        /// <summary>
        /// Binds country drop-down list
        /// </summary>
        private void BindCountry()
        {
            // PortalCountry
            PortalCountryHelper portalCountryHelper = new PortalCountryHelper();
            DataSet countryDs = portalCountryHelper.GetCountriesByPortalID(ZNodeConfigManager.SiteConfig.PortalID);

            DataView View = new DataView(countryDs.Tables[0]);
            View.RowFilter = "ActiveInd = 1";

            // Drop Down List
            ddlCountryCode.DataSource = View;
            ddlCountryCode.DataTextField = "Name";
            ddlCountryCode.DataValueField = "Code";
            ddlCountryCode.DataBind();
            ddlCountryCode.SelectedIndex = ddlCountryCode.Items.IndexOf(ddlCountryCode.Items.FindByValue("US"));
        }

        /// <summary>
        /// Submit the page data.
        /// </summary>
        private void SubmitPage()
        {
            ZNode.Libraries.DataAccess.Service.AddressService addressService = new ZNode.Libraries.DataAccess.Service.AddressService();
            ZNode.Libraries.DataAccess.Entities.Address address = new ZNode.Libraries.DataAccess.Entities.Address();
            if (this.AddressId > 0)
            {
                address = addressService.GetByAddressID(this.AddressId);
            }

            if (!this.HasDefaultAddress(this.AccountId, true))
            {
                lblErrorMsg.Text = "At least one address should be default billing address.";
                return;
            }

            if (!this.HasDefaultAddress(this.AccountId, false))
            {
                lblErrorMsg.Text = "At least one address should be default shipping address.";
                return;
            }

            address.AccountID = this.AccountId;
            address.Name =Server.HtmlEncode(txtName.Text);
            address.FirstName = Server.HtmlEncode(txtFirstName.Text);
            address.LastName = Server.HtmlEncode(txtLastName.Text);
            address.CompanyName = Server.HtmlEncode(txtCompanyName.Text);
            address.Street = Server.HtmlEncode(txtStreet1.Text);
            address.Street1 =Server.HtmlEncode(txtStreet2.Text);
            address.City = Server.HtmlEncode(txtCity.Text);
            //address.StateCode = txtState.Text.ToUpper();
            address.PostalCode = txtPostalCode.Text;
            address.CountryCode = ddlCountryCode.SelectedValue;
            address.PhoneNumber = Server.HtmlEncode(txtPhoneNumber.Text);
            //Zeon Custom Code:Starts
            if (ddlState.Visible && ConfigurationManager.AppSettings["AddressCustomCountry"].ToString().ToUpper().Contains(address.CountryCode))
            {
                address.StateCode = ddlState.SelectedValue.IndexOf("AE") >= 0 ? "AE" : ddlState.SelectedValue;
            }
            else
            {
                address.StateCode = txtState.Text.ToUpper();
            }
            //Zeon Custom Code:Ends

            TList<ZNode.Libraries.DataAccess.Entities.Address> addressList = addressService.GetByAccountID(this.AccountId);
            if (chkUseAsDefaultBillingAddress.Checked)
            {
                // Clear all previous default billing address flag.
                foreach (ZNode.Libraries.DataAccess.Entities.Address currentItem in addressList)
                {
                    currentItem.IsDefaultBilling = false;
                }

                addressService.Update(addressList);
            }

            if (chkUseAsDefaultShippingAddress.Checked)
            {
                // Clear all previous default billing address flag.
                foreach (ZNode.Libraries.DataAccess.Entities.Address currentItem in addressList)
                {
                    currentItem.IsDefaultShipping = false;
                }

                addressService.Update(addressList);
            }

            address.IsDefaultBilling = chkUseAsDefaultBillingAddress.Checked;
            address.IsDefaultShipping = chkUseAsDefaultShippingAddress.Checked;

            bool isSuccess = false;
            if (this.AddressId > 0)
            {
                isSuccess = addressService.Update(address);
            }
            else
            {
                isSuccess = addressService.Insert(address);
            }

            if (isSuccess)
            {
                //Zeon Custom Code:Start
                SaveAddressExtnData(address.AddressID);
                //Zeon Custom Code:Ends
                this.RediretPage();
            }
        }

        /// <summary>
        /// Check whether the account has default billing or shipping address.
        /// </summary>
        /// <param name="accountId">Account Id</param>
        /// <param name="isBillingAddress">True to check billing address, false to check shipping address.</param>
        /// <returns>Returns true if account has default address or false.</returns>
        private bool HasDefaultAddress(int accountId, bool isBillingAddress)
        {
            bool hasDefault = true;

            AccountAdmin accountAdmin = new AccountAdmin();
            ZNode.Libraries.DataAccess.Entities.Address addresses = null;
            if (isBillingAddress)
            {
                addresses = accountAdmin.GetDefaultBillingAddress(accountId);
                hasDefault = this.ValidateDefaultAddress(addresses, chkUseAsDefaultBillingAddress);
            }
            else
            {
                addresses = accountAdmin.GetDefaultShippingAddress(accountId);
                hasDefault = this.ValidateDefaultAddress(addresses, chkUseAsDefaultShippingAddress);
            }

            return hasDefault;
        }

        /// <summary>
        /// Validate account had default address.
        /// </summary>
        /// <param name="address">Address object</param>
        /// <param name="cb">Check box control</param>
        /// <returns>Returns true if account has default address.</returns>
        private bool ValidateDefaultAddress(ZNode.Libraries.DataAccess.Entities.Address address, CheckBox cb)
        {
            if (address == null && !cb.Checked)
            {
                return false;
            }
            else
            {
                if (!cb.Checked && address != null && address.AddressID == this.AddressId)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Redirect to the account view page.
        /// </summary>
        private void RediretPage()
        {
            if (this.AccountId > 0)
            {
                if (RoleName.ToLower() == "admin")
                {
                    Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/StoreAdministrators/View.aspx?itemid=" + this.AccountId + "&mode=" + this.Mode + "&pagefrom=admin");
                }
                else if (RoleName.ToLower() == "franchise")
                {
                    Response.Redirect("~/SiteAdmin/Secure/Vendors/FranchiseAdministrators/View.aspx?itemid=" + this.AccountId + "&mode=" + this.Mode + "&pagefrom=franchise");
                }
                else if (RoleName.ToLower() == "vendor")
                {
                    Response.Redirect("~/SiteAdmin/Secure/Vendors/VendorAccounts/View.aspx?itemid=" + this.AccountId + "&mode=" + this.Mode + "&pagefrom=vendor");
                }
                else
                {
                    Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/View.aspx?itemid=" + this.AccountId + "&mode=" + this.Mode + "&pagefrom=customer");
                }

            }
            else
            {
                if (RoleName.ToLower() == "admin")
                {
                    Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/StoreAdministrators/Default.aspx");
                }
                else if (RoleName.ToLower() == "franchise")
                {
                    Response.Redirect("~/SiteAdmin/Secure/Vendors/FranchiseAdministrators/Default.aspx");
                }
                else if (RoleName.ToLower() == "vendor")
                {
                    Response.Redirect("~/SiteAdmin/Secure/Vendors/VendorAccounts/Default.aspx");
                }
                else
                {
                    Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/Default.aspx");
                }
            }
        }
        #endregion

        #region Zeon Custom Code
        /// <summary>
        /// Bind State as per countryCode
        /// </summary>
        private void BindStateByCountryCode(string countryCode)
        {
            bool isStateList = false;
            if (ConfigurationManager.AppSettings["AddressCustomCountry"] != null && ConfigurationManager.AppSettings["AddressCustomCountry"].ToString().ToUpper().Contains(countryCode))
            {
                //Zeon Custom Code:Starts
                //StateService stateService = new StateService();
                //string filterQuery = "CountryCode='" + countryCode + "'";
                //TList<State> stateList = stateService.Find(filterQuery);
                ZNode.Libraries.DataAccess.Custom.ZStateHelper zStateHelper = new ZNode.Libraries.DataAccess.Custom.ZStateHelper();
                DataTable dtStatesList = zStateHelper.GetStateListByCountryCode(countryCode);
                //Zeon Custom Code:Ends
                if (dtStatesList != null && dtStatesList.Rows.Count > 0)
                {
                    ddlState.DataSource = dtStatesList;
                    ddlState.DataTextField = "Name";
                    ddlState.DataValueField = "Code";
                    ddlState.DataBind();
                    ddlState.Items.Insert(0, new ListItem(Resources.CommonCaption.DefaultSelectedState.ToString(), Resources.CommonCaption.DefaultSelectedState.ToString()));
                    ddlState.SelectedIndex = 0;
                    isStateList = true;
                }
            }
            SetStateControl(isStateList);
        }

        /// <summary>
        /// Set visibility of either textbox or dropdown for state
        /// </summary>
        /// <param name="isListEnabled">bool</param>
        private void SetStateControl(bool isListEnabled)
        {
            ddlState.Visible = isListEnabled;
            txtState.Visible = !isListEnabled;
        }

        /// <summary>
        /// Bind Address Extn Details
        /// </summary>
        /// <param name="addressID"></param>
        private void BindAddressExtensionDetails(int addressID,string stateCode)
        {
            AddressExtnService addrssExtnSer = new AddressExtnService();
            TList<AddressExtn> addressList = addrssExtnSer.GetByAddressID(addressID);
            if (addressList != null && addressList.Count > 0)
            {
                chkIsPOBox.Checked = addressList[0].IsPOBox != null ? bool.Parse(addressList[0].IsPOBox.ToString()) : false;
                hdnAddressExtnID.Value = addressList[0].AddressExtnID.ToString();
                if (ddlState.Visible && stateCode.IndexOf("AE") >= 0) { ddlState.SelectedValue = ddlState.Items.FindByText(addressList[0].Custom1.ToString()).Value; }
            }
            else
            {
                chkIsPOBox.Checked = false;
            }
        }

        /// <summary>
        /// save Address Extn Data
        /// </summary>
        /// <param name="addressID"></param>
        private void SaveAddressExtnData(int addressID)
        {
            int addressExtnId = 0;
            AddressExtn addressExtn = new AddressExtn();
            AddressExtnService addrssExtnSer = new AddressExtnService();
            if (!string.IsNullOrEmpty(hdnAddressExtnID.Value)) { int.TryParse(hdnAddressExtnID.Value, out addressExtnId); }
            addressExtn.IsPOBox = chkIsPOBox.Checked;
            addressExtn.AddressID = addressID;
            try
            {
                addressExtn.Custom1 = ddlState.SelectedValue.IndexOf("AE") >= 0 ? ddlState.SelectedItem.Text : null;
                if (addressExtnId > 0)
                {
                    addressExtn.AddressExtnID = addressExtnId;
                    addrssExtnSer.Update(addressExtn);
                }
                else
                {
                    addrssExtnSer.Save(addressExtn);
                }
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in SaveAddressExtnData in!!" + this.Request.Url + addressID + ex.ToString());
            }
        }

        /// <summary>
        /// Set Postal Code Validation
        /// </summary>
        /// <param name="ddlCountryList"></param>
        private void EnablePostalCodeValidation(DropDownList ddlCountryList)
        {
            //Changes done on date  : 30/Jan/2015 : Purpose :Apply regx validation for US Postal Code
            if (ddlCountryList.SelectedValue.Equals("US", StringComparison.OrdinalIgnoreCase))
            {
                rgxBillingPostalCodeValidation.Visible = true;
            }
            else
            {
                rgxBillingPostalCodeValidation.Visible = false;
            }
        }

        #endregion
    }
}