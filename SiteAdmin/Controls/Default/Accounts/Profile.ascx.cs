﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Controls.Default.Accounts
{
    public partial class Profile : System.Web.UI.UserControl
    {

        #region Private Member Variables
        private int ItemId = 0;
        private string _RoleName = string.Empty;
        #endregion


        #region Public Properties
        /// <summary>
        /// Gets or sets a value for RoleName
        /// </summary>
        public string RoleName
        {
            get
            {
                return this._RoleName;
            }

            set
            {
                this._RoleName = value;
            }
        }
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            btnSearch.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_search_highlight.gif") + "'");
            btnSearch.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_search.gif") + "'");
            btnClear.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_clear_highlight.gif") + "'");
            btnClear.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_clear.gif") + "'");

            btnSubmitBottom.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_submit_highlight.gif") + "'");
            btnSubmitBottom.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_submit.gif") + "'");
            btnCancelBottom.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_cancel_highlight.gif") + "'");
            btnCancelBottom.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_cancel.gif") + "'");

            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }
            else
            {
                this.ItemId = 0;
            }

            if (Request.Params["pagefrom"] != null)
            {
                this.RoleName = Request.Params["pagefrom"].ToString();
            }
            if (!Page.IsPostBack)
            {
                this.BindGrid();
                this.BindData();
            }
        }
        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Submit_Click(object sender, EventArgs e)
        {
            this.RememberOldValues();
            AccountProfileAdmin accountProfileAdmin = new AccountProfileAdmin();
            Dictionary<int, string> profileIdList = (Dictionary<int, string>)Session["CHECKEDITEMS"];
            if (profileIdList != null)
            {
                StringBuilder sb = new StringBuilder();
                StringBuilder profileName = new StringBuilder();
                foreach (KeyValuePair<int, string> pair in profileIdList)
                {
                    // Get ProfileId
                    int profileId = pair.Key;

                    if (!accountProfileAdmin.ProfileExists(this.ItemId, profileId))
                    {
                        AccountProfile accountProfile = new AccountProfile();
                        accountProfile.ProfileID = profileId;
                        accountProfile.AccountID = this.ItemId;
                        accountProfileAdmin.Insert(accountProfile);
                        profileName.Append(pair.Value + ",");
                    }
                    else
                    {
                        sb.Append(pair.Value + ",");
                        lblError.Visible = true;
                    }
                }

                Session.Remove("CHECKEDITEMS");

                if (sb.ToString().Length > 0)
                {
                    sb.Remove(sb.ToString().Length - 1, 1);

                    // Display Error message
                    lblError.Text = "The following profile(s) are already associated with this account.<br/>" + sb.ToString();

                    foreach (GridViewRow row in uxGrid.Rows)
                    {
                        CheckBox check = (CheckBox)row.Cells[0].FindControl("chkProduct") as CheckBox;

                        if (check != null)
                        {
                            check.Checked = false;
                        }
                    }
                }
                else
                {
                    // Log Activity
                    profileName.Remove(profileName.Length - 1, 1);
                    AccountAdmin _accountAdmin = new AccountAdmin();
                    Account _account = new Account();
                    _account = _accountAdmin.GetByAccountID(this.ItemId);
                    if (_account.UserID != null)
                    {
                        MembershipUser _user = Membership.GetUser(_account.UserID);
                        string UserName = _user.UserName;
                        string AssociateName = "Associated Profile " + profileName + " to Account " + UserName;
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, UserName);
                    }

                    if (RoleName.ToLower() == "admin")
                    {
                        Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/StoreAdministrators/View.aspx?itemid=" + this.ItemId + "&Mode=profiles&pagefrom=admin");
                    }
                    else if (RoleName.ToLower() == "franchise")
                    {
                        Response.Redirect("~/SiteAdmin/Secure/Vendors/FranchiseAdministrators/View.aspx?itemid=" + this.ItemId + "&Mode=profiles&pagefrom=franchise");
                    }
                    else if (RoleName.ToLower() == "vendor")
                    {
                        Response.Redirect("~/SiteAdmin/Secure/Vendors/VendorAccounts/View.aspx?itemid=" + this.ItemId + "&Mode=profiles&pagefrom=vendor");
                    }
                    else
                    {
                        Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/View.aspx?itemid=" + this.ItemId + "&Mode=profiles&pagefrom=customer");
                    }
                }
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            if (RoleName.ToLower() == "admin")
            {
                Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/StoreAdministrators/View.aspx?itemid=" + this.ItemId + "&Mode=profiles&pagefrom=admin");
            }
            else if (RoleName.ToLower() == "franchise")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/FranchiseAdministrators/View.aspx?itemid=" + this.ItemId + "&Mode=profiles&pagefrom=franchise");
            }
            else if (RoleName.ToLower() == "vendor")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/VendorAccounts/View.aspx?itemid=" + this.ItemId + "&Mode=profiles&pagefrom=vendor");
            }
            else
            {
                Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/View.aspx?itemid=" + this.ItemId + "&Mode=profiles&pagefrom=customer");
            }
        }

        /// <summary>
        /// Search Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchData();
        }

        /// <summary>
        /// Clear Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {

            if (RoleName.ToLower() == "admin")
            {
                Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/StoreAdministrators/AddProfile.aspx?itemid=" + this.ItemId + "&pagefrom=admin");
            }
            else if (RoleName.ToLower() == "franchise")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/FranchiseAdministrators/AddProfile.aspx?itemid=" + this.ItemId + "&pagefrom=franchise");
            }
            else if (RoleName.ToLower() == "vendor")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/VendorAccounts/AddProfile.aspx?itemid=" + this.ItemId + "&pagefrom=vendor");
            }
            else
            {
                Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/AddProfile.aspx?itemid=" + this.ItemId + "&pagefrom=customer");
            }
        }

        /// <summary>
        /// Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.RememberOldValues();
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGrid();
        }

        /// <summary>
        /// Back Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            if (RoleName.ToLower() == "admin")
            {
                Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/StoreAdministrators/View.aspx?itemid=" + this.ItemId + "&Mode=profiles&pagefrom=admin");
            }
            else if (RoleName.ToLower() == "franchise")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/FranchiseAdministrators/View.aspx?itemid=" + this.ItemId + "&Mode=profiles&pagefrom=franchise");
            }
            else if (RoleName.ToLower() == "vendor")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/VendorAccounts/View.aspx?itemid=" + this.ItemId + "&Mode=profiles&pagefrom=vendor");
            }
            else
            {
                Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/View.aspx?itemid=" + this.ItemId + "&Mode=profiles&pagefrom=customer");
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Bind Grid method
        /// </summary>
        private void BindGrid()
        {
            ProfileAdmin profileAdmin = new ProfileAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Profile> profiles = profileAdmin.GetAll();
            uxGrid.DataSource = profiles;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Bind Search Data Method
        /// </summary>
        private void BindSearchData()
        {
            ProfileService profileService = new ProfileService();
            ProfileQuery query = new ProfileQuery();
            query.Append(ProfileColumn.Name, Server.HtmlEncode(txtprofilename.Text.Trim()));
            TList<ZNode.Libraries.DataAccess.Entities.Profile> profiles = profileService.Find(query);
            uxGrid.DataSource = profiles;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Remember Old Values Method
        /// </summary>
        private void RememberOldValues()
        {
            Dictionary<int, string> profiletIdList = new Dictionary<int, string>();

            // Loop through the grid values
            foreach (GridViewRow row in uxGrid.Rows)
            {
                CheckBox check = (CheckBox)row.Cells[0].FindControl("chkProfile") as CheckBox;

                // Check in the Session
                if (Session["CHECKEDITEMS"] != null)
                {
                    profiletIdList = (Dictionary<int, string>)Session["CHECKEDITEMS"];
                }

                int id = int.Parse(row.Cells[1].Text);

                if (check.Checked)
                {
                    if (!profiletIdList.ContainsKey(id))
                    {
                        profiletIdList.Add(id, row.Cells[2].Text);
                    }
                }
                else
                {
                    profiletIdList.Remove(id);
                }
            }

            if (profiletIdList.Count > 0)
            {
                Session["CHECKEDITEMS"] = profiletIdList;
            }
        }

        private void BindData()
        {
            AccountAdmin accountAdmin = new AccountAdmin();
            ZNode.Libraries.DataAccess.Entities.Account account = accountAdmin.GetByAccountID(this.ItemId);

            if (account != null)
            {
                ZNode.Libraries.DataAccess.Entities.Address defaultBillingAddress = accountAdmin.GetDefaultBillingAddress(this.ItemId);

                if (defaultBillingAddress == null)
                {
                    defaultBillingAddress = new ZNode.Libraries.DataAccess.Entities.Address();
                }
                lblName.Text = defaultBillingAddress.FirstName + " " + defaultBillingAddress.LastName;
            }
        }

        #endregion
    }
}