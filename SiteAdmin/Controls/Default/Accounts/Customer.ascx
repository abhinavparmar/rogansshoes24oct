﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Customer.ascx.cs" Inherits="SiteAdmin.Controls.Default.Accounts.Customer" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>

<div align="center">
    <div>
        <div class="ClearBoth">
        </div>
        <div align="left">
            <h4 class="SubTitle">
                <asp:Label ID="lblHeading" runat="server"></asp:Label>
            </h4>
            <asp:Panel ID="SearchCustomers" DefaultButton="btnSearch" runat="Server">
                <div class="SearchForm">
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle">
                                <asp:Label ID="lbStoreName" Text="Store Name" runat="server"></asp:Label></span><br />
                            <span class="ValueStyle">
                                <asp:DropDownList ID="ddlPortal" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlPortal_SelectedIndexChanged">
                                </asp:DropDownList>
                            </span>
                        </div>
                    </div>
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle">First Name</span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtFName" runat="server"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">Last Name</span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtLName" runat="server"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">Company Name</span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtComapnyNM" runat="server"></asp:TextBox></span>
                        </div>
                    </div>
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle">Login Name</span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtLoginName" runat="server"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">Account Number</span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtExternalaccountno" runat="server"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle"><asp:Label ID="lblContactID" runat="server"></asp:Label></span>
                            <br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtContactID" runat="server"></asp:TextBox></span>
                        </div>
                    </div>
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle">Phone Number</span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtPhoneNumber" runat="server"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">Email ID</span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtEmailID" runat="server"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">Select a Profile</span><br />
                            <span class="ValueStyle">
                                <asp:DropDownList ID="lstProfile" runat="server">
                                </asp:DropDownList>
                            </span>
                        </div>
                    </div>
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle">Start Date<small> MM/DD/YYYY</small></span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtStartDate" Text="" runat="server"></asp:TextBox>
                                <asp:Image ID="imgbtnStartDt" runat="server" Style="cursor: pointer; vertical-align: top"
                                    ImageUrl="~/SiteAdmin/Themes/images/SmallCalendar.gif"></asp:Image>
                                <ajaxToolKit:CalendarExtender ID="CalendarExtender1" Enabled="true" PopupButtonID="imgbtnStartDt"
                                    runat="server" TargetControlID="txtStartDate">
                                </ajaxToolKit:CalendarExtender>
                                <br />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtStartDate"
                                    CssClass="Error" Display="Dynamic" ErrorMessage="* Enter Valid Date in MM/DD/YYYY format"
                                    ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"></asp:RegularExpressionValidator>
                            </span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">End Date<small> MM/DD/YYYY</small></span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtEndDate" Text='' runat="server" />&nbsp;<asp:ImageButton ID="ImgbtnEndDt"
                                    runat="server" ImageAlign="AbsMiddle" ImageUrl="~/SiteAdmin/Themes/images/SmallCalendar.gif" />
                                <ajaxToolKit:CalendarExtender ID="CalendarExtender2" Enabled="true" PopupButtonID="ImgbtnEndDt"
                                    runat="server" TargetControlID="txtEndDate">
                                </ajaxToolKit:CalendarExtender>

                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEndDate"
                                    CssClass="Error" Display="Dynamic" ErrorMessage="* Enter Valid Date in MM/DD/YYYY format"
                                    ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"></asp:RegularExpressionValidator><br />
                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtStartDate"
                                    ControlToValidate="txtEndDate" CssClass="Error" Display="Dynamic" ErrorMessage=" End Date must be greater than Begin date <br/>"
                                    Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>
                            </span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">
                                <asp:Label ID="lblAffiliateStatus" runat="server" Text="Affiliate Approval Status"></asp:Label></span><br />
                            <span class="ValueStyle">
                                <asp:DropDownList ID="lstReferralStatus" runat="server">
                                </asp:DropDownList>
                            </span>
                        </div>
                    </div>
                    <div class="ClearBoth">
                        <asp:ImageButton ID="btnClearSearch" CausesValidation="false" runat="server" OnClick="BtnClearSearch_Click"
                            ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif" AlternateText="Clear" />
                        <asp:ImageButton ID="btnSearch" runat="server" OnClick="BtnSearch_Click" 
                            ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif" AlternateText="Search"/>
                    </div>
                </div>
            </asp:Panel>
            <div align="right" class="ImageButtons">
                <asp:Button ID="download" runat="server" Text="Download to Excel" OnClick="Download_Click" />
            </div>
            <br />

            <h4 class="GridTitle">
                <asp:Label ID="lblGridTitle" runat="server" Text=""></asp:Label></h4>
        </div>
    </div>
    <div>
        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" Width="100%" ForeColor="#333333"
            CellPadding="4" DataKeyNames="accountid" EmptyDataText="No contacts exist in the database."
            OnRowCommand="UxGrid_RowCommand" OnPageIndexChanging="UxGrid_PageIndexChanging"
            PageSize="50" AllowPaging="True" GridLines="None" AutoGenerateColumns="False"
            CaptionAlign="Left" OnRowDataBound="uxGrid_RowDataBound">
            <Columns>
                <asp:TemplateField HeaderText="Account ID" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:HyperLink runat="server" ID="editaccount" NavigateUrl='<%# GetNavigateURL(DataBinder.Eval(Container.DataItem,"AccountId")) %>' Text='<%# Eval("AccountId") %>'></asp:HyperLink>
                        <%--<asp:Label runat="server" ID="editaccountid"  Visible='<%# !HideEditButton(DataBinder.Eval(Container.DataItem,"UserId")) %>'  Text='<%# Eval("AccountId") %>'></asp:Label> --%>
                    </ItemTemplate>
                </asp:TemplateField>


                <asp:TemplateField HeaderText="Full Name" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="lblFullName" runat="Server" Text='<%# ConcatName(Eval("BillingFirstName"),Eval("BillingLastName")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="BillingPhoneNumber" HeaderText="Phone Number" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText="Email ID" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <a href='<%# "mailto:" + Eval("BillingEmailID") %>'>
                            <%# DataBinder.Eval(Container.DataItem,"BillingEmailID") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ACTIONS" HeaderStyle-HorizontalAlign="Left">
                    <ItemStyle Wrap="false" />
                    <ItemTemplate>
                        <asp:LinkButton ID="ViewCustomer" Text="MANAGE &raquo" CommandName="View" CssClass="LinkButton"
                            CommandArgument='<%# DataBinder.Eval(Container.DataItem,"AccountId") %>'
                            runat="server" />&nbsp;&nbsp;    
                        <asp:LinkButton ID="UnlockCustomer" Text='<%# GetActiveInd(DataBinder.Eval(Container.DataItem,"UserId")) + "&raquo;&nbsp;&nbsp;" %>' CommandName='<%# GetActiveInd(DataBinder.Eval(Container.DataItem,"UserId")) %>' CssClass="LinkButton"
                            CommandArgument='<%# DataBinder.Eval(Container.DataItem,"AccountId") %>'
                            runat="server" />


                        <asp:LinkButton ID="DeleteCustomer" Text="DELETE &raquo" CommandName="Delete" CssClass="LinkButton"
                            CommandArgument='<%# DataBinder.Eval(Container.DataItem,"AccountId") %>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
    </div>
    <asp:Label ID="lblError" runat="server" Text="" CssClass="Error"></asp:Label>
</div>

