﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Permission.ascx.cs" Inherits="SiteAdmin.Controls.Default.Accounts.Permission" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/StoreName.ascx" TagName="StoreName" TagPrefix="ZNode" %>

<h1>Edit Permissions: <asp:Label ID="lblPermissionName" runat="server" Text=""></asp:Label></h1>
<div>
    <asp:Label CssClass="Error" ID="lblPortalAcountErrorMessage" runat="server" EnableViewState="false"
        Visible="false"></asp:Label>
</div>
<h4 class="SubTitle">
    <asp:Label ID="lblSelectStores" runat="server" Text=""></asp:Label></h4>
<div style="margin-left: 7px; margin-top: 5px;">
    <asp:CheckBox ID="chkAllStores" runat="server" Text="All Stores" AutoPostBack="true"
        OnCheckedChanged="ChkAllStores_checkedChanged" />
</div>
<br />
<asp:Panel ID="pnlSpecificStore" runat="server" Visible="false">
    <div>
        <asp:CheckBoxList ID="StoreCheckList" runat="server" RepeatDirection="Horizontal"
            RepeatColumns="4" CellPadding="5" />
    </div>
</asp:Panel>
<h4 class="SubTitle">Select Roles</h4>
<div>
    <asp:CheckBoxList ID="RolesCheckboxList" runat="server" Enabled="false" RepeatDirection="Horizontal"
        RepeatColumns="3" CellPadding="5">
    </asp:CheckBoxList>
</div>
<div>
    <ZNode:Spacer ID="Spacer1" runat="server" SpacerHeight="15" SpacerWidth="3" />
</div>
<div>
    <asp:ImageButton ID="btnAddSelectedStore" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
        runat="server" AlternateText="Submit" ValidationGroup="grpAssociateAddOn" OnClick="BtnAddSelectedPortals_Click" CausesValidation="true" />
    <asp:ImageButton ID="btnBottomCancel" CausesValidation="False" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
        runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
    <div>
        <ZNode:Spacer ID="Spacer9" SpacerHeight="15" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
</div>
<asp:Label CssClass="Error" ID="lblError" runat="server"></asp:Label>