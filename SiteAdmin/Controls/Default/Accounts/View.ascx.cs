﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using WebApp;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Controls.Default.Accounts
{
    public partial class View : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private int AccountId;
        private string Mode = string.Empty;
        private string _PageFrom = string.Empty;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets a value for RoleName
        /// </summary>
        public string PageFrom
        {
            get
            {
                return this._PageFrom;
            }

            set
            {
                this._PageFrom = value;
            }
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Bind the user account list.
        /// </summary>
        /// <param name="addressId"> The value of AddressId</param>
        /// <returns>Returns the Address</returns>
        public string GetAddress(string addressId)
        {
            string addressText = string.Empty;

            AddressService addressService = new AddressService();
            ZNode.Libraries.DataAccess.Entities.Address address = addressService.GetByAddressID(Convert.ToInt32(addressId));
            if (address != null)
            {
                addressText = ZCommonHelper.GetAddressString(address); //Zeon Custom Code
            }

            return addressText;
        }

        /// <summary>
        /// Gets the name of the Addon for this AddonId
        /// </summary>
        /// <param name="portalId">The value of PortalId</param>
        /// <returns>Returns the Store Name</returns>
        public string GetStoreName(object portalId)
        {
            PortalAdmin AdminAccess = new PortalAdmin();
            Portal _portal = AdminAccess.GetByPortalId(int.Parse(portalId.ToString()));

            if (_portal != null)
            {
                return _portal.StoreName;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the Store Title for this Portal Id
        /// </summary>
        /// <param name="portalId">The value of Portal Id</param>
        /// <returns>Returns the Store Title</returns>
        public string GetStoreTitle(object portalId)
        {
            PortalAdmin AdminAccess = new PortalAdmin();
            Portal _portal = AdminAccess.GetByPortalId(int.Parse(portalId.ToString()));

            if (_portal != null)
            {
                return _portal.CompanyName;
            }

            return string.Empty;
        }
        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Bind Data Method
        /// </summary>
        protected void BindData()
        {
            AccountAdmin accountAdmin = new AccountAdmin();
            AccountTypeAdmin accountTypeAdmin = new AccountTypeAdmin();
            ProfileAdmin profileAdmin = new ProfileAdmin();
            CustomerAdmin customerAdmin = new CustomerAdmin();

            ZNode.Libraries.DataAccess.Entities.Account account = accountAdmin.GetByAccountID(this.AccountId);
            
            if (account != null)
            {
                ZNode.Libraries.DataAccess.Entities.Address defaultBillingAddress = accountAdmin.GetDefaultBillingAddress(this.AccountId);

                if (defaultBillingAddress == null)
                {
                    defaultBillingAddress = new ZNode.Libraries.DataAccess.Entities.Address();
                }

                // General Information
                lblName.Text = defaultBillingAddress.FirstName + " " + defaultBillingAddress.LastName;
                lblPhone.Text = defaultBillingAddress.PhoneNumber;
                lblAccountID.Text = account.AccountID.ToString();
                lblExternalAccNumber.Text = account.ExternalAccountNo;
                lblDescription.Text = account.Description;
                //Zeon Custom Code:Starts
                //lblLoginName.Text = customerAdmin.GetByUserID(int.Parse(this.AccountId.ToString()));//old Code
                string loginName = string.Empty;
                loginName = customerAdmin.GetByUserID(int.Parse(this.AccountId.ToString()));
                if (loginName.IndexOf('_') > 0) { loginName = loginName.Split('_')[0]; }
                lblLoginName.Text = loginName;
                //Zeon Custom Code:Ends
                lblCompanyName.Text = account.CompanyName;


                if (this.PageFrom.ToLower() == "admin")
                {
                    //Site Admin's General Information Details.
                    lblCustomerDetails.Text = "Store Administrator: " + account.AccountID.ToString() + " - " + defaultBillingAddress.FirstName + " " + defaultBillingAddress.LastName;
                    lblSiteLoginName.Text = customerAdmin.GetByUserID(int.Parse(this.AccountId.ToString())); //Znode Old Code
                    lblSiteAccountNumber.Text = account.AccountID.ToString();
                    lblSiteEmail.Text = account.Email;
                    if (account.EmailOptIn)
                    {
                        SiteEmailOptin.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
                    }
                    else
                    {
                        SiteEmailOptin.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(false);
                    }
                    lblSiteCreateDate.Text = account.CreateDte.ToShortDateString();
                    lblSiteCreateUser.Text = account.CreateUser;
                    lblSiteUpdateUser.Text = account.UpdateUser;
                    if (account.UpdateDte != null)
                    {
                        lblSiteUpdateDate.Text = account.UpdateDte.Value.ToShortDateString();
                    }
                }
                else if (this.PageFrom.ToLower() == "franchise")
                {
                    lblCustomerDetails.Text = "Franchise Admin: " + account.AccountID.ToString() + " - " + defaultBillingAddress.FirstName + " " + defaultBillingAddress.LastName;
                }
                else if (this.PageFrom.ToLower() == "vendor")
                {
                    lblCustomerDetails.Text = "Vendor Account: " + account.AccountID.ToString() + " - " + defaultBillingAddress.FirstName + " " + defaultBillingAddress.LastName;
                }
                else
                {
                    lblCustomerDetails.Text = "Customer: " + account.AccountID.ToString() + " - " + defaultBillingAddress.FirstName + " " + defaultBillingAddress.LastName;
                }
                lblWebSite.Text = account.Website;
                lblSource.Text = account.Source;
                lblCreatedDate.Text = account.CreateDte.ToShortDateString();
                lblCreatedUser.Text = account.CreateUser;

                // Get Referral Type Name for a Account
                if (account.ReferralCommissionTypeID != null)
                {
                    lblReferralType.Text = this.GetCommissionTypeById(account.ReferralCommissionTypeID.ToString());
                }
                else
                {
                    lblReferralType.Text = string.Empty;
                }

                if (account.ReferralStatus == "A")
                {
                    string affiliateLink = "http://" + UserStoreAccess.DomainName + "/default.aspx?affiliate_id=" + account.AccountID;
                    hlAffiliateLink.Text = affiliateLink;
                    hlAffiliateLink.NavigateUrl = affiliateLink;
                }
                else
                {
                    hlAffiliateLink.Text = "NA";
                }

                if (account.ReferralCommission != null)
                {
                    if (account.ReferralCommissionTypeID == 1)
                    {
                        lblReferralCommission.Text = account.ReferralCommission.Value.ToString("N");
                    }
                    else
                    {
                        lblReferralCommission.Text = account.ReferralCommission.Value.ToString("c");
                    }
                }
                else
                {
                    lblReferralCommission.Text = string.Empty;
                }

                lblTaxId.Text = account.TaxID;
                lblReferralStatus.Text = "Inactive";
                if (account.ReferralStatus != null)
                {
                    // Getting the Status Description 
                    Array values = Enum.GetValues(typeof(ZNodeApprovalStatus.ApprovalStatus));
                    Array names = Enum.GetNames(typeof(ZNodeApprovalStatus.ApprovalStatus));
                    for (int i = 0; i < names.Length; i++)
                    {
                        if (names.GetValue(i).ToString() == account.ReferralStatus)
                        {
                            lblReferralStatus.Text = ZNodeApprovalStatus.GetEnumValue(values.GetValue(i));
                            break;
                        }
                    }

                    this.BindPayments(accountAdmin);
                }
                else
                {
                    pnlAffiliatePayment.Visible = false;
                }

                if (account.UpdateDte != null)
                {
                    lblUpdatedDate.Text = account.UpdateDte.Value.ToShortDateString();
                }

                // Email Opt-In
                if (account.EmailOptIn)
                {
                    EmailOptin.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
                }
                else
                {
                    EmailOptin.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(false);
                }

                lblEmail.Text = account.Email;
                lblUpdatedUser.Text = account.UpdateUser;
                lblCustom1.Text = account.Custom1;
                lblCustom2.Text = account.Custom2;
                lblCustom3.Text = account.Custom3;

                // To get Amount owed 
                AccountHelper helperAccess = new AccountHelper();
                DataSet myDataSet = helperAccess.GetCommisionAmount(ZNodeConfigManager.SiteConfig.PortalID, account.AccountID.ToString());

                lblAmountOwed.Text = "$" + myDataSet.Tables[0].Rows[0]["CommissionOwed"].ToString();

                // Orders Grid
                this.BindGrid();

                this.BindReferralCommission();

                // Retrieves the Role for User using Userid
                if (account.UserID != null)
                {
                    Guid UserKey = new Guid();
                    UserKey = account.UserID.Value;
                    MembershipUser _user = Membership.GetUser(UserKey);
                    string roleList = string.Empty;

                    // Get Profile Store Access using userName.
                    WebApp.ProfileCommon newProfile = ProfileCommon.Create(_user.UserName, true);

                    if (newProfile.StoreAccess == "AllStores" || newProfile.StoreAccess == string.Empty)
                    {
                        lblstores.Text = newProfile.StoreAccess;
                    }
                    else
                    {
                        PortalAdmin portalAdmin = new PortalAdmin();
                        lblstores.Text = portalAdmin.GetStoreNameByPortalIDs(newProfile.StoreAccess);
                    }

                    // Get roles for this User account
                    string[] roles = Roles.GetRolesForUser(_user.UserName);

                    foreach (string Role in roles)
                    {
                        roleList += Role + "<br>";
                    }

                    lblRoles.Text = roleList;

                    string rolename = roleList;

                    // If the user is Admin role, then show permissions tab
                    if (Roles.IsUserInRole("ADMIN"))
                    {
                        permissionPanel.Enabled = true;
                        tabpnlOrders.Enabled = true;
                    }

                    // Hide the Edit button if a NonAdmin user has entered this page
                    if (!Roles.IsUserInRole("ADMIN"))
                    {
                        if (Roles.IsUserInRole(_user.UserName, "ADMIN"))
                        {
                            EditCustomer.Visible = false;
                        }
                        else if (Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "CUSTOMER SERVICE REP"))
                        {
                            if (rolename == Convert.ToString("USER<br>") || rolename == Convert.ToString(string.Empty))
                            {
                                EditCustomer.Visible = true;
                            }
                            else
                            {
                                EditCustomer.Visible = false;
                            }
                        }
                    }
                }

                // Bind address list
                this.BindAddresses();
            }
        }

        /// <summary>
        /// Get the commission type by Id
        /// </summary>
        /// <param name="commissionTypeId">Commission Type ID</param>
        /// <returns>Returns the commission type name.</returns>
        protected string GetCommissionTypeById(object commissionTypeId)
        {
            string commissionType = string.Empty;
            if (commissionTypeId.ToString() != string.Empty)
            {
                ReferralCommissionAdmin referralCommissionAdmin = new ReferralCommissionAdmin();
                ReferralCommissionType referralType = referralCommissionAdmin.GetByReferralID(Convert.ToInt32(commissionTypeId));
                commissionType = referralType.Name;
            }

            return commissionType;
        }

        /// <summary>
        /// Bind Order Grid
        /// </summary>
        protected void BindGrid()
        {
            OrderAdmin _OrderAdmin = new OrderAdmin();
            TList<Order> _Orders = _OrderAdmin.GetByAccountID(this.AccountId);

            uxGrid.DataSource = _Orders;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Bind payments Grid
        /// </summary>
        protected void BindPayments()
        {
            AccountAdmin accountAdmin = new AccountAdmin();
            this.BindPayments(accountAdmin);
        }

        /// <summary>
        /// Bind payments Grid
        /// </summary>    
        /// <param name="AccountAdmin">The AccountAdmin instanceS</param>
        protected void BindPayments(AccountAdmin AccountAdmin)
        {
            uxPaymentList.DataSource = AccountAdmin.GetPaymentsByAccountId(this.AccountId);
            uxPaymentList.DataBind();

            pnlAffiliatePayment.Visible = true;
        }

        /// <summary>
        /// //Bind Repeater
        /// </summary>
        protected void BindNotes()
        {
            NoteAdmin _NoteAdmin = new NoteAdmin();
            TList<Note> noteList = _NoteAdmin.GetByAccountID(this.AccountId);
            noteList.Sort("NoteID Desc");
            CustomerNotes.DataSource = noteList;
            CustomerNotes.DataBind();
        }

        /// <summary>
        /// CustomerList Button  Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void CustomerList_Click(object sender, EventArgs e)
        {
            if (PageFrom.ToLower() == "admin")
            {
                Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/StoreAdministrators/Default.aspx");
            }
            else if (PageFrom.ToLower() == "franchise")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/FranchiseAdministrators/Default.aspx");
            }
            else if (PageFrom.ToLower() == "vendor")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/VendorAccounts/Default.aspx");
            }
            else
            {
                Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/Default.aspx");
            }
        }

        /// <summary>
        /// Edit Customer Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void CustomerEdit_Click(object sender, EventArgs e)
        {
            if (PageFrom.ToLower() == "admin")
            {
                Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/StoreAdministrators/Edit.aspx?mode=0&itemid=" + this.AccountId + "&pagefrom=admin");
            }
            else if (PageFrom.ToLower() == "franchise")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/FranchiseAdministrators/Edit.aspx?mode=0&itemid=" + this.AccountId + "&pagefrom=franchise");
            }
            else if (PageFrom.ToLower() == "vendor")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/VendorAccounts/Edit.aspx?mode=0&itemid=" + this.AccountId + "&pagefrom=vendor");
            }
            else
            {
                Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/EditCustomer.aspx?mode=0&itemid=" + this.AccountId + "&pagefrom=customer");
            }
        }

        /// <summary>
        /// Edit Tracking Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void TrackingEdit_Click(object sender, EventArgs e)
        {
            if (PageFrom.ToLower() == "admin")
            {
                Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/StoreAdministrators/AddAffiliateSettings.aspx?mode=1&itemid=" + this.AccountId + "&pagefrom=admin");
            }
            else if (PageFrom.ToLower() == "franchise")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/FranchiseAdministrators/AddAffiliateSettings.aspx?mode=1&itemid=" + this.AccountId + "&pagefrom=franchise");
            }
            else if (PageFrom.ToLower() == "vendor")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/VendorAccounts/AddAffiliateSettings.aspx?mode=1&itemid=" + this.AccountId + "&pagefrom=vendor");
            }
            else
            {
                Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/AddAffiliateSettings.aspx?mode=1&itemid=" + this.AccountId + "&pagefrom=customer");
            }
        }

        /// <summary>
        /// Edit Permission Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void PermissionEdit_Click(object sender, EventArgs e)
        {
            if (PageFrom.ToLower() == "admin")
            {
                Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/StoreAdministrators/AddPermission.aspx?itemid=" + this.AccountId + "&pagefrom=admin");
            }
            else if (PageFrom.ToLower() == "franchise")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/FranchiseAdministrators/AddPermission.aspx?itemid=" + this.AccountId + "&pagefrom=franchise");
            }
            else if (PageFrom.ToLower() == "vendor")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/VendorAccounts/AddPermission.aspx?itemid=" + this.AccountId + "&pagefrom=vendor");
            }
            else
            {
                Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/AddPermission.aspx?itemid=" + this.AccountId + "&pagefrom=customer");
            }
        }

        /// <summary>
        /// Add Note Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddNewNote_Click(object sender, EventArgs e)
        {
            if (PageFrom.ToLower() == "admin")
            {
                Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/StoreAdministrators/AddNote.aspx?itemid=" + this.AccountId + "&pagefrom=admin");
            }
            else if (PageFrom.ToLower() == "franchise")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/FranchiseAdministrators/AddNote.aspx?itemid=" + this.AccountId + "&pagefrom=franchise");
            }
            else if (PageFrom.ToLower() == "vendor")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/VendorAccounts/AddNote.aspx?itemid=" + this.AccountId + "&pagefrom=vendor");
            }
            else
            {
                Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/AddNote.aspx?itemid=" + this.AccountId + "&pagefrom=customer");
            }
        }

        /// <summary>
        /// Add Payment button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddAffiliatePayment_Click(object sender, EventArgs e)
        {
            if (PageFrom.ToLower() == "admin")
            {
                Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/StoreAdministrators/AddAffiliatePayment.aspx?accId=" + this.AccountId + "&pagefrom=admin");
            }
            else if (PageFrom.ToLower() == "franchise")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/FranchiseAdministrators/AddAffiliatePayment.aspx?accId=" + this.AccountId + "&pagefrom=franchise");
            }
            else if (PageFrom.ToLower() == "vendor")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/VendorAccounts/AddAffiliatePayment.aspx?accId=" + this.AccountId + "&pagefrom=vendor");
            }
            else
            {
                Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/AddAffiliatePayment.aspx?accId=" + this.AccountId + "&pagefrom=customer");
            }
        }

        /// <summary>
        /// Add Profile Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddProfile_Click(object sender, EventArgs e)
        {
            if (PageFrom.ToLower() == "admin")
            {
                Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/StoreAdministrators/AddProfile.aspx?itemid=" + this.AccountId + "&pagefrom=admin");
            }
            else if (PageFrom.ToLower() == "franchise")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/FranchiseAdministrators/AddProfile.aspx?itemid=" + this.AccountId + "&pagefrom=franchise");
            }
            else if (PageFrom.ToLower() == "vendor")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/VendorAccounts/AddProfile.aspx?itemid=" + this.AccountId + "&pagefrom=vendor");
            }
            else
            {
                Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/AddProfile.aspx?itemid=" + this.AccountId + "&pagefrom=customer");
            }
        }

        /// <summary>
        /// Add New Addres button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddNewAddress_Click(object sender, EventArgs e)
        {
            if (PageFrom.ToLower() == "admin")
            {
                Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/StoreAdministrators/EditAddress.aspx?accountid=" + this.AccountId + "&pagefrom=admin");
            }
            else if (PageFrom.ToLower() == "franchise")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/FranchiseAdministrators/EditAddress.aspx?accountid=" + this.AccountId + "&pagefrom=franchise");
            }
            else if (PageFrom.ToLower() == "vendor")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/VendorAccounts/EditAddress.aspx?accountid=" + this.AccountId + "&pagefrom=vendor");
            }
            else
            {
                Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/EditAddress.aspx?accountid=" + this.AccountId + "&pagefrom=customer");
            }
        }

        /// <summary>
        /// Profile Items Page Index Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxProfileGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxProfileGrid.PageIndex = e.NewPageIndex;
            this.BindAccountProfile();
        }

        /// <summary>
        /// Referral commission Items Page Index Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxReferralCommission_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxReferralCommission.PageIndex = e.NewPageIndex;
            this.BindReferralCommission();
        }

        /// <summary>
        /// Profile Grid Row Deleting Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxProfileGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindAccountProfile();
        }

        /// <summary>
        /// Profile Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxProfileGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "Remove")
                {
                    AccountProfileAdmin accountProfileAdmin = new AccountProfileAdmin();

                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                    HiddenField hdnProfile = (HiddenField)row.FindControl("hdnProfileName");
                    string ProfileName = hdnProfile.Value;

                    AccountAdmin _accountAdmin = new AccountAdmin();
                    Account _account = new Account();
                    _account = _accountAdmin.GetByAccountID(this.AccountId);
                    MembershipUser _user = Membership.GetUser(_account.UserID);
                    string UserName = _user.UserName;

                    string AssociateName = "Deleted the Association between '" + ProfileName + "' Profile and '" + UserName + "' Account ";

                    bool Status = accountProfileAdmin.Delete(int.Parse(e.CommandArgument.ToString()));

                    if (Status)
                    {
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, UserName);

                        this.BindAccountProfile();
                    }
                }
            }
        }

        /// <summary>
        /// Format the Price with two decimal
        /// </summary>
        /// <param name="fieldvalue">The value of fieldvalue</param>
        /// <returns>Returns the formatted price</returns>
        protected string FormatPrice(object fieldvalue)
        {
            if (fieldvalue == DBNull.Value)
            {
                return string.Empty;
            }
            else
            {
                if (fieldvalue.ToString().Length == 0)
                {
                    return string.Empty;
                }
                else
                {
                    return "$" + fieldvalue.ToString().Substring(0, fieldvalue.ToString().Length - 2);
                }
            }
        }

        /// <summary>
        /// Format the Price with two decimal
        /// </summary>
        /// <param name="fieldvalue">The value of fieldvalue</param>
        /// <returns>Returns the formatted price</returns>
        protected string GetPageFrom()
        {
            return "~/SiteAdmin/Secure/Orders/OrderManagement/ViewOrders/View.aspx?itemid={0}&from=account&pagefrom=" + PageFrom.ToString();
        }

        /// <summary>
        /// Display the Order State name for a Order state
        /// </summary>
        /// <param name="fieldValue">The value of FieldValue</param>
        /// <returns>Returns the Display order status</returns>
        protected string DisplayOrderStatus(object fieldValue)
        {
            ZNode.Libraries.Admin.OrderAdmin _OrderStateAdmin = new OrderAdmin();
            OrderState _OrderState = _OrderStateAdmin.GetByOrderStateID(int.Parse(fieldValue.ToString()));
            return _OrderState.OrderStateName.ToString();
        }

        /// <summary>
        ///  Format Customer Note
        /// </summary>
        /// <param name="field1">The value of field1</param>
        /// <param name="field2">The value of field2</param>
        /// <param name="field3">The value of field3</param>
        /// <returns>Returns the Formatted Customer Note</returns>
        protected string FormatCustomerNote(object field1, object field2, object field3)
        {
            return "<b>" + field1.ToString() + "</b>  [created by " + field2.ToString() + " on " + Convert.ToDateTime(field3).ToShortDateString() + "]";
        }

        /// <summary>
        /// Grid page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGrid();
        }

        /// <summary>
        /// Grid Row DataBound Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnField1 = e.Row.Cells[0].FindControl("HdnId") as HiddenField;
                HyperLink hlname1 = e.Row.Cells[0].FindControl("hlinkOrder") as HyperLink;
                hlname1.NavigateUrl = string.Format("~/SiteAdmin/Secure/Orders/OrderManagement/ViewOrders/View.aspx?itemid={0}&from=account&pagefrom={1}", hdnField1.Value, PageFrom.ToLower().ToString());
            }
        }

        /// <summary>
        /// Payment List Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxPaymentList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxPaymentList.PageIndex = e.NewPageIndex;
            this.BindPayments();
        }

        /// <summary>
        /// Address Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GvAddress_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvAddress.PageIndex = e.NewPageIndex;
            this.BindAddresses();
        }

        /// <summary>
        /// Address Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GvAddress_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int addressId = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "Edit")
                {
                    Response.Redirect("editaddress.aspx?mode=0&itemid=" + addressId + "&accountid=" + this.AccountId +"&pagefrom=" + this.PageFrom);
                }

                if (e.CommandName == "Remove")
                {
                    this.DeleteAddress(addressId);
                }
            }
        }
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.AccountId = int.Parse(Request.Params["itemid"].ToString());
            }
            else
            {
                this.AccountId = 0;
            }

            if (Request.Params["Mode"] != null)
            {
                this.Mode = Request.Params["Mode"].ToString();
            }

            if (Request.Params["pagefrom"] != null)
            {
                this.PageFrom = Request.Params["pagefrom"].ToString();
            }

            if (!IsPostBack)
            {
                this.BindData();
                this.BindNotes();
                this.BindAccountProfile();
                this.ResetTabs();
                this.RoleValidation();
            }
        }
        #endregion

        #region Private Methods

        private void ClearBillingDetails()
        {
            lblName.Text = string.Empty;
            lblPhone.Text = string.Empty;
        }

        private void RoleValidation()
        {
            if (this.PageFrom.ToLower() == "admin")
            {
                tabCustomerDetails.Tabs[0].Enabled = false;
                tabCustomerDetails.Tabs[2].Enabled = false;
                tabCustomerDetails.Tabs[3].Enabled = false;
                tabCustomerDetails.Tabs[4].Enabled = false;
                tabCustomerDetails.Tabs[5].Enabled = false;
            }
            else if (this.PageFrom.ToLower() == "franchise")
            {
                tabCustomerDetails.Tabs[1].Enabled = false;
                tabCustomerDetails.Tabs[3].Enabled = false;
                tabCustomerDetails.Tabs[4].Enabled = false;
                tabCustomerDetails.Tabs[5].Enabled = false;
            }
            else if (this.PageFrom.ToLower() == "vendor")
            {
                tabCustomerDetails.Tabs[1].Enabled = false;
                tabCustomerDetails.Tabs[3].Enabled = false;
                tabCustomerDetails.Tabs[4].Enabled = false;
                tabCustomerDetails.Tabs[5].Enabled = false;
            }
            else
            {
                tabCustomerDetails.Tabs[1].Enabled = false;
                tabCustomerDetails.Tabs[6].Enabled = false;
               if(tabCustomerDetails.ActiveTab.TabIndex >0 )
                tabCustomerDetails.ActiveTabIndex = tabCustomerDetails.ActiveTab.TabIndex - 1;
            }
        }

        /// <summary>
        /// Bind the account referral commision grid 
        /// </summary>    
        private void BindReferralCommission()
        {
            ReferralCommissionAdmin referralCommissionAdmin = new ReferralCommissionAdmin();
            TList<ReferralCommission> referralCommissionList = new TList<ReferralCommission>();

            // Gets the referral commission details
            if (this.AccountId != 0)
            {
                referralCommissionList = referralCommissionAdmin.GetReferralCommissionByAccountID(this.AccountId);
            }

            uxReferralCommission.DataSource = referralCommissionList;
            uxReferralCommission.DataBind();
        }

        /// <summary>
        /// Bind Account Profile Method
        /// </summary>
        private void BindAccountProfile()
        {
            uxProfileGrid.DataSource = null;
            uxProfileGrid.DataBind();
            AccountProfileAdmin accountProfileAdmin = new AccountProfileAdmin();
            uxProfileGrid.DataSource = accountProfileAdmin.DeepLoadProfilesByAccountID(this.AccountId);
            uxProfileGrid.DataBind();
        }

        /// <summary>
        /// Bind the user account list.
        /// </summary>
        private void BindAddresses()
        {
            AddressService addressService = new AddressService();
            TList<ZNode.Libraries.DataAccess.Entities.Address> addressList = addressService.GetByAccountID(this.AccountId);

            addressList.Sort("Name ASC");
            gvAddress.DataSource = addressList;
            gvAddress.DataBind();
        }

        /// <summary>
        /// Reset Tabs Method
        /// </summary>
        private void ResetTabs()
        {
            if (this.PageFrom.ToLower() == "admin")
            {
                tabCustomerDetails.ActiveTabIndex = 1;
            }
            else
            {
                tabCustomerDetails.ActiveTabIndex = 0;
            }

            if (this.Mode.Equals("profiles"))
            {
                tabCustomerDetails.ActiveTabIndex = 4;
            }
            else if (this.Mode.Equals("affiliate"))
            {
                tabCustomerDetails.ActiveTabIndex = 5;
            }
            else if (this.Mode.Equals("notes"))
            {
                AccountAdmin accountAdmin = new AccountAdmin();
                ZNode.Libraries.DataAccess.Entities.Account account = accountAdmin.GetByAccountID(this.AccountId);
                if (account.UserID != null)
                {
                    tabCustomerDetails.ActiveTabIndex = 3;
                }
                else
                {
                    tabCustomerDetails.ActiveTabIndex = 2;
                }
            }
            else if (this.Mode.Equals("permissions"))
            {
                tabCustomerDetails.ActiveTabIndex = 6;
            }
            else if (this.Mode.Equals("order"))
            {
                tabCustomerDetails.ActiveTab = tabpnlOrders;
            }
            //else
            //{
            //    tabCustomerDetails.ActiveTabIndex = 0;
            //}
        }

        /// <summary>
        /// Delete the selected address.
        /// </summary>
        /// <param name="addressId">Address Id</param>
        private void DeleteAddress(int addressId)
        {
            AddressService addressService = new AddressService();
            AccountAdmin accountAdmin = new AccountAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Address> addressList = addressService.GetByAccountID(this.AccountId);
            int addressCount = addressList.Count;

            // If account has more than one address then validate for default address.
            if (addressCount > 1)
            {
                // Validate default shipping address.
                addressList.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.Address a) { return a.IsDefaultBilling == true && a.AddressID == addressId; });
                if (addressList.Count == 1)
                {
                    lblMsg.Text = "Default billing address could not be deleted. Set some other address as default billing address and then try again.";
                    return;
                }
                addressList.RemoveFilter();

                // Validate default shipping address.
                addressList.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.Address a) { return a.IsDefaultShipping == true && a.AddressID == addressId; });
                if (addressList.Count == 1)
                {
                    lblMsg.Text = "Default shipping address could not be deleted. Set some other address as default shipping address and then try again.";
                    return;
                }
                if (addressList.Count == 0)
                {
                    lblMsg.Text = string.Empty;
                }
            }
            addressService.Delete(addressId);

            this.BindAddresses();
            this.ClearBillingDetails();
        }

        #endregion
    }
}