﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AffiliateSettings.ascx.cs" Inherits="SiteAdmin.Controls.Default.Accounts.AffiliateSettings" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>

<div class="FormView">
    <div class="LeftFloat" style="width: 70%; text-align: left">
        <h1>
            <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
        <uc2:DemoMode ID="DemoMode1" runat="server" />
    </div>
    <div style="text-align: right; padding-right: 10px;">
        <asp:ImageButton ID="btnSubmitTop" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
            runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" ValidationGroup="EditContact" CausesValidation="true" />
        <asp:ImageButton ID="btnCancelTop" CausesValidation="False" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
            runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
    </div>
    <div align="left" class="ClearBoth">
        <asp:Label ID="lblErrorMsg" runat="server" CssClass="Error" EnableViewState="false"></asp:Label>
    </div>
    <div>
        <uc1:Spacer ID="Spacer8" SpacerHeight="2" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <h4 class="SubTitle">Tracking Information</h4>
            <div class="HintStyle">
                Use this section to set up tracking codes for this account. These tracking codes
                    will attribute purchases made by other customers that have been referred by this
                    account.<br />
                <br />
            </div>
            <div class="FieldStyle">
                <asp:Label runat="server" ID="lblAffiliateLink">Tracking Link (URL)</asp:Label><br />
                <small>Customers who visit your site using this link will automatically be logged as being
                    referred by this account when they purchase.</small>
            </div>
            <div class="ValueStyle">
                <asp:Label ID="lblAffiliateLinkError" runat="server" CssClass="Error"></asp:Label>
                <asp:HyperLink ID="hlAffiliateLink" runat="server" Target="_blank"></asp:HyperLink>
            </div>
            <div class="FieldStyle">
                Commission Type<br />
                <small>Enter how much you will pay this account for referred sales. Leave blank if not
                        applicable.</small>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="lstReferral" runat="server" OnSelectedIndexChanged="DiscountType_SelectedIndexChanged"
                    AutoPostBack="True">
                </asp:DropDownList>
                <asp:TextBox ID="Discount" runat="server" Columns="25"></asp:TextBox><br />
                <asp:RegularExpressionValidator ValidationExpression="^\d*\.?\d*$" ID="discrevAmountValidator" runat="server"
                    ControlToValidate="Discount" ValidationGroup="EditContact" CssClass="Error"
                    Enabled="true" Display="Dynamic" ErrorMessage="Enter valid numbers only">
                </asp:RegularExpressionValidator>
                <br />
                <asp:RangeValidator ID="discAmountValidator" runat="server" ControlToValidate="Discount"
                    ValidationGroup="EditContact" CssClass="Error" Enabled="false" Display="Dynamic"
                    MaximumValue="9999999" MinimumValue="0" CultureInvariantValues="true" Type="Currency"></asp:RangeValidator>
                <asp:RangeValidator ID="discPercentageValidator" Enabled="false" runat="server" ControlToValidate="Discount"
                    ValidationGroup="EditContact" CssClass="Error" Display="Dynamic" MaximumValue="100"
                    CultureInvariantValues="true" MinimumValue="0" SetFocusOnError="True" Type="Double"></asp:RangeValidator>
            </div>
            <div class="FieldStyle">
                Tax ID
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtTaxId" runat="server" Width="179px" MaxLength="9"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                Partner Approval Status<br />
                <small>Set the Partner Approval Status to ACTIVE to start attributing visitors to this
                        account.</small>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="lstReferralStatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="LstReferralStatus_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="FieldStyle" runat="server" id="amountOwed">
                Amount Owed
            </div>
            <div class="ValueStyle">
                <asp:Label ID="lblAmountOwed" runat="server" Text="" />
            </div>
            <uc1:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="ClearBoth">
        <br />
    </div>
    <div>
        <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='~/SiteAdmin/Themes/Images/buttons/button_submit_highlight.gif';"
            onmouseout="this.src='~/SiteAdmin/Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
            runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" ValidationGroup="EditContact" />
        <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='~/SiteAdmin/Themes/Images/buttons/button_cancel_highlight.gif';"
            onmouseout="this.src='~/SiteAdmin/Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
            runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
    </div>
</div>
