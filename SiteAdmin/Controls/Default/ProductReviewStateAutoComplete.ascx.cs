﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace SiteAdmin.Controls.Default
{
    /// <summary>
    /// Represents the Product Review State Auto Complete  user control class
    /// </summary>
    public partial class ProductReviewStateAutoComplete : System.Web.UI.UserControl
    {
        private int DropdownItemCount = int.Parse(ConfigurationManager.AppSettings["DropdownMaxItemCount"]);

        /// <summary>
        /// Occurs when the content of the auto complete text box changes between posts to the server.
        /// </summary>
        private event System.EventHandler onSelectedIndexChanged;

        #region Public Properties

        /// <summary>
        ///  Gets or sets the width of the auto complete control.
        /// </summary>
        public string Width
        {
            get
            {
                return txtProductReviewState.Width.ToString();
            }

            set
            {
                txtProductReviewState.Width = new Unit(value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether Auto Postback is true or false
        /// </summary>
        public bool AutoPostBack
        {
            get
            {
                if (ddlProductReviewState.Visible)
                {
                    return ddlProductReviewState.AutoPostBack;
                }

                return bool.Parse(hdnAutoPostBack.Value);
            }

            set
            {
                if (ddlProductReviewState.Visible)
                {
                    ddlProductReviewState.AutoPostBack = value;
                }

                hdnAutoPostBack.Value = value.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the text content of the hidden field control.
        /// </summary>
        public string Value
        {
            get
            {
                if (ddlProductReviewState.Visible)
                {
                    return ddlProductReviewState.SelectedValue;
                }

                return ProductReviewStateId.Value;
            }

            set
            {
                if (ddlProductReviewState.Visible)
                {
                    ListItem item = ddlProductReviewState.Items.FindByValue(value);
                    if (item != null)
                    {
                        ddlProductReviewState.SelectedValue = value;
                    }
                    else
                    {
                        ddlProductReviewState.SelectedValue = string.Empty;
                    }
                }

                ProductReviewStateId.Value = value;
            }
        }

        /// <summary>
        /// Gets or sets the text content of the auto complete control.
        /// </summary>
        public string Text
        {
            get
            {
                return txtProductReviewState.Text;
            }

            set
            {
                txtProductReviewState.Text = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the auto complete control is required.
        /// </summary>
        public bool IsRequired
        {
            get
            {
                return RequiredFieldValidator2.Visible;
            }

            set
            {
                if (ddlProductReviewState.Visible)
                {
                    RequiredFieldValidator2.ControlToValidate = "ddlProductReviewState";
                }
                else
                {
                    RequiredFieldValidator2.ControlToValidate = "txtProductReviewState";
                }

                RequiredFieldValidator2.Visible = value;
            }
        }

        /// <summary>
        /// Sets the value for select index changed handler
        /// </summary>
        public System.EventHandler OnSelectedIndexChanged
        {
            set
            {
                if (ddlProductReviewState.Visible)
                {
                    ddlProductReviewState.SelectedIndexChanged += value;
                }
                else
                {
                    this.onSelectedIndexChanged = value;
                }
            }
        }

        #endregion

        #region Private Properties
        /// <summary>
        /// Gets a TList of Product Review State
        /// </summary>
        private TList<ProductReviewState> ProductReviewStateListTable
        {
            get
            {
                ProductReviewStateService ProductReviewStateService = new ProductReviewStateService();

                TList<ProductReviewState> ProductReviewStates = ProductReviewStateService.GetAll();

                return ProductReviewStates;
            }
        }
        #endregion

        #region Page Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            ddlProductReviewState.Visible = false;

            if (this.ProductReviewStateListTable.Count < this.DropdownItemCount && !this.Page.IsPostBack)
            {
                ddlProductReviewState.Visible = true;
                ddlProductReviewState.AutoPostBack = bool.Parse(hdnAutoPostBack.Value);
                txtProductReviewState.Visible = false;
                autoCompleteExtender3.Enabled = false;
                ddlProductReviewState.DataSource = this.ProductReviewStateListTable;
                ddlProductReviewState.DataTextField = "ReviewStateName";
                ddlProductReviewState.DataValueField = "ReviewStateID";
                ddlProductReviewState.DataBind();

                ListItem allItem = new ListItem("All", string.Empty);
                ddlProductReviewState.Items.Insert(0, allItem);
                ddlProductReviewState.SelectedIndex = ddlProductReviewState.Items.IndexOf(ddlProductReviewState.Items.FindByValue(string.Empty));
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder stringBuilderScript = new StringBuilder();

            stringBuilderScript.Append("<script language=\"JavaScript\" type=\"text/javascript\">\n");
            stringBuilderScript.Append("<!--\n");
            stringBuilderScript.Append("function documentOnKeyPress()\n");
            stringBuilderScript.Append("{\n");
            stringBuilderScript.Append(" var charCode = window.event.keyCode;\n");
            stringBuilderScript.Append(" var elementType = window.event.srcElement.type;\n");
            stringBuilderScript.Append("\n");
            stringBuilderScript.Append(" if ( (charCode == 13) && (elementType == \"text\") )\n");
            stringBuilderScript.Append("   {\n");
            stringBuilderScript.Append("        // Cancel the keystroke completely\n");
            stringBuilderScript.Append("        window.event.returnValue = false;\n");
            stringBuilderScript.Append("        window.event.cancel = true;\n");
            stringBuilderScript.Append("        // Or change it to a tab\n");
            stringBuilderScript.Append("  //window.event.keyCode = 9;\n");
            stringBuilderScript.Append("   }\n");
            stringBuilderScript.Append("}\n");
            stringBuilderScript.Append("document.onkeypress = documentOnKeyPress;\n");
            stringBuilderScript.Append("// -->\n");
            stringBuilderScript.Append("</script>\n");

            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "OnKeyPressScript", stringBuilderScript.ToString());
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when the content of the auto complete text box changes
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AutoComplete_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.onSelectedIndexChanged != null && !ddlProductReviewState.Visible)
            {
                this.onSelectedIndexChanged(sender, e);
            }
        }
        #endregion
    }
}