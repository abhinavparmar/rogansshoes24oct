﻿<%@ Control Language="C#" AutoEventWireup="True"
    Inherits="SiteAdmin.Controls.Default.AlternateImage" CodeBehind="AlternateImage.ascx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<div>
    <uc1:Spacer ID="Spacer13" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
</div>
<div class="ImageButtons">
    <asp:Button ID="btnAddImage" runat="server" CssClass="Size175" Text="Add New Alternate Image" OnClick="BtnAddImage_click" />
    <asp:Button ID="btnAccept" runat="server" Text="Accept Checked"
        OnClick="BtnAccept_Click" />
    <asp:Button ID="btnReject" runat="server" Text="Reject Checked"
        OnClick="BtnReject_Click" />
    <br />
    <uc1:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
    <asp:Label ID="lblCaption" runat="server" Text="" />
</div>
<div>
    <uc1:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
</div>
<div>
    <asp:GridView ID="GridThumb" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
        runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
        CssClass="Grid" Width="100%" GridLines="None" PageSize="10"
        OnRowCommand="GridThumb_RowCommand" OnRowDataBound="GridThumb_RowDataBound">
        <Columns>
            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:CheckBox ID="chkSelect" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <img id="imgStatus" src='<%# GetStatusImageIcon(DataBinder.Eval(Container.DataItem, "ReviewStateID"))%>' runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ProductImageId" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
            <asp:TemplateField HeaderText="Image" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <img id="SwapImage" alt="" src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>'
                        runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-HorizontalAlign="Left" />
            <asp:TemplateField HeaderText="Product Page" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <img id="Img1" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                        runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Category Page" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <img id="Img2" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ShowOnCategoryPage").ToString()))%>'
                        runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="DisplayOrder" HeaderText="Display Order" HeaderStyle-HorizontalAlign="Left" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton CssClass="Button" ID="EditProductView" Text="Edit &raquo" CommandArgument='<%# Eval("productimageid") %>'
                        CommandName="Edit" runat="Server" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:LinkButton ID="Delete" Text="Delete &raquo" CommandArgument='<%# Eval("productimageid") %>'
                        CommandName="RemoveItem" CssClass="Button" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            No images found.
        </EmptyDataTemplate>
        <RowStyle CssClass="RowStyle" />
        <HeaderStyle CssClass="HeaderStyle" />
        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
        <FooterStyle CssClass="FooterStyle" />
    </asp:GridView>
</div>
