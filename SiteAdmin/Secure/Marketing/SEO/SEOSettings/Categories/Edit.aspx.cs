using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Marketing.SEO.SEOSettings.Categories
{
    /// <summary>
    /// Represents the SiteAdmin.Secure.Marketing.SEO.SEOSettings.Categories.Edit class
    /// </summary>
    public partial class Edit : System.Web.UI.Page
    {
        #region Protected Variables
        private int ItemId;
        private string ManagePageLink = "~/SiteAdmin/Secure/Marketing/SEO/SEOSettings/Default.aspx?mode=category";
        #endregion

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (!Page.IsPostBack)
            {
                // If edit func then bind the data fields
                if (this.ItemId > 0)
                {
                    lblTitle.Text = "Edit Category SEO Settings - ";
                    this.BindEditData();
                }
            }
        }

        #region Bind Data
        /// <summary>
        /// Bind data to the fields on the edit screen
        /// </summary>
        protected void BindEditData()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            Category category = categoryAdmin.GetByCategoryId(this.ItemId);

            if (category != null)
            {
                // Checking the user profile for roles and permission and then loading the data based on roles
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (!UserStoreAccess.CheckUserRoleInCategory(profiles, this.ItemId))
                {
                    Response.Redirect(this.ManagePageLink);
                }
                
                lblTitle.Text += category.Title;
                txtTitle.Text = Server.HtmlDecode(category.Title);
                txtshortdescription.Text = Server.HtmlDecode(category.ShortDescription);
                ctrlHtmlText.Html = Server.HtmlDecode(category.Description);
                txtSEOMetaDescription.Text = Server.HtmlDecode(category.SEODescription);
                txtSEOMetaKeywords.Text = Server.HtmlDecode(category.SEOKeywords);
                txtSEOTitle.Text = Server.HtmlDecode(category.SEOTitle);
                txtSEOURL.Text = category.SEOURL;
            }
            else
            {
                throw new ApplicationException("Category Requested could not be found.");
            }
        }

        #endregion

        #region General Events
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            UrlRedirectAdmin urlRedirectAdmin = new UrlRedirectAdmin();
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            Category category = new Category();
            string mappedSEOUrl = string.Empty;

            if (this.ItemId > 0)
            {
                category = categoryAdmin.GetByCategoryId(this.ItemId);

                if (category.SEOURL != null)
                {
                    mappedSEOUrl = category.SEOURL;
                }
            }

            category.CategoryID = this.ItemId;
            category.ShortDescription = Server.HtmlEncode(txtshortdescription.Text);
            category.Description = ctrlHtmlText.Html;
            category.Title = Server.HtmlEncode(txtTitle.Text);
            category.SEOTitle = Server.HtmlEncode(txtSEOTitle.Text);
            category.SEOKeywords = Server.HtmlEncode(txtSEOMetaKeywords.Text);
            category.SEODescription = Server.HtmlEncode(txtSEOMetaDescription.Text);
            category.SEOURL = null;
            if (txtSEOURL.Text.Trim().Length > 0)
            {
                category.SEOURL = txtSEOURL.Text.Trim().Replace(" ", "-");
            }

            if (string.Compare(category.SEOURL, mappedSEOUrl, true) != 0)
            {
                if (urlRedirectAdmin.SeoUrlExists(category.SEOURL, category.CategoryID))
                {
                    lblError.Text = "The SEO Page Name you entered is already in use on another page. Please select another name for your SEO Page Name";
                    return;
                }
            }

            bool retval = false;

            if (this.ItemId > 0)
            {
                retval = categoryAdmin.Update(category);

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit Category SEO Settings - " + category.Title, category.Title);
            }

            if (retval)
            {
                if (chkAddURLRedirect.Checked)
                {
                    urlRedirectAdmin.AddUrlRedirectTable(SEOUrlType.Category, mappedSEOUrl, category.SEOURL, category.CategoryID.ToString());
                }

                urlRedirectAdmin.UpdateUrlRedirect(mappedSEOUrl);

                Response.Redirect(this.ManagePageLink);
            }
            else
            {
                if (this.ItemId > 0)
                {
                    lblError.Text = "Could not update the product category. Please try again.";
                }

                return;
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ManagePageLink);
        }

        #endregion
    }
}