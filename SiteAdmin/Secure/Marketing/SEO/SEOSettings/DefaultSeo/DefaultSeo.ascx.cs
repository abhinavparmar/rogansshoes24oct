﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Marketing.SEO.SEOSettings.DefaultSeo
{
    public partial class DefaultSeo : System.Web.UI.UserControl
    {
        #region Events

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindData();
                this.BindStores();
            }
        }

        /// <summary>
        /// Store List Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Storelist_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindData();
        }
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            Portal portal = storeAdmin.GetByPortalId(Convert.ToInt32(StoreList.SelectedValue));

            // SEO Settings
            portal.SeoDefaultProductTitle = txtSEOProductTitle.Text.Trim();
            portal.SeoDefaultProductDescription = txtSEOProductDescription.Text.Trim();
            portal.SeoDefaultProductKeyword = txtSEOProductKeyword.Text.Trim();

            portal.SeoDefaultCategoryTitle = txtSEOCategoryTitle.Text.Trim();
            portal.SeoDefaultCategoryDescription = txtSEOCategoryDescription.Text.Trim();
            portal.SeoDefaultCategoryKeyword = txtSEOCategoryKeyword.Text.Trim();

            portal.SeoDefaultContentTitle = txtSEOContentTitle.Text.Trim();
            portal.SeoDefaultContentDescription = txtSEOContentDescription.Text.Trim();
            portal.SeoDefaultContentKeyword = txtSEOContentKeyword.Text.Trim();

            bool ret = storeAdmin.Update(portal);

            // Remove the siteconfig from session
            ZNodeConfigManager.SiteConfig = null;

            if (!ret)
            {
                lblMsg.Text = "An error ocurred while updating the SEO settings. Please try again.";

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.StoreSettingsChangeFailed, HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit Default SEO Settings - " + portal.StoreName, portal.StoreName);

                Response.Redirect("~/SiteAdmin/Secure/Marketing/SEO/SEOSettings/Default.aspx?mode=default");

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.StoreSettingsChangeSuccess, HttpContext.Current.User.Identity.Name);
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Marketing/Default.aspx");
        }

        #endregion

        #region Bind Method
        /// <summary>
        /// Bind Store 
        /// </summary>
        private void BindStores()
        {
            ZNode.Libraries.DataAccess.Service.PortalService portalServ = new ZNode.Libraries.DataAccess.Service.PortalService();

            TList<ZNode.Libraries.DataAccess.Entities.Portal> portals = portalServ.GetAll();

            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (profiles.StoreAccess != "AllStores")
            {
                portals.Filter = string.Concat("PortalID IN [", profiles.StoreAccess, "]");
            }
            StoreList.Visible = true;
            StoreList.DataSource = portals;
            StoreList.DataTextField = "StoreName";
            StoreList.DataValueField = "PortalID";
            StoreList.DataBind();
        }

        private void ClearData()
        {
            txtSEOProductTitle.Text = string.Empty;
            txtSEOProductDescription.Text = string.Empty;
            txtSEOProductKeyword.Text = string.Empty;
            txtSEOCategoryTitle.Text = string.Empty;
            txtSEOCategoryDescription.Text = string.Empty;
            txtSEOCategoryKeyword.Text = string.Empty;
            txtSEOContentTitle.Text = string.Empty;
            txtSEOContentDescription.Text = string.Empty;
            txtSEOContentKeyword.Text = string.Empty;
        }
        /// <summary>
        /// Bind data to form fields
        /// </summary>
        private void BindData()
        {
            //this.ClearData();

            int portalid = ZNodeConfigManager.SiteConfig.PortalID;
            if (StoreList.SelectedValue != "")
                portalid = Convert.ToInt32(StoreList.SelectedValue);
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            Portal portal = storeAdmin.GetByPortalId(portalid);

            // Set SEO Details
            if (!string.IsNullOrEmpty(portal.SeoDefaultProductTitle))
            {
                txtSEOProductTitle.Text = portal.SeoDefaultProductTitle.ToString();
            }
            else
            {
                txtSEOProductTitle.Text = "<NAME>";
            }
            if (!string.IsNullOrEmpty(portal.SeoDefaultProductDescription))
            {
                txtSEOProductDescription.Text = portal.SeoDefaultProductDescription.ToString();
            }
            else
            {
                txtSEOProductDescription.Text = "<NAME>";
            }

            if (portal.SeoDefaultProductKeyword != null)
            {
                txtSEOProductKeyword.Text = portal.SeoDefaultProductKeyword.ToString();
            }

            if (!string.IsNullOrEmpty(portal.SeoDefaultCategoryTitle))
            {
                txtSEOCategoryTitle.Text = portal.SeoDefaultCategoryTitle.ToString();
            }
            else
            {
                txtSEOCategoryTitle.Text = "<NAME>";
            }
            if (!string.IsNullOrEmpty(portal.SeoDefaultCategoryDescription))
            {
                txtSEOCategoryDescription.Text = portal.SeoDefaultCategoryDescription.ToString();
            }
            else
            {
                txtSEOCategoryDescription.Text = "<NAME>";
            }
            if (portal.SeoDefaultCategoryKeyword != null)
            {
                txtSEOCategoryKeyword.Text = portal.SeoDefaultCategoryKeyword.ToString();
            }

            if (!string.IsNullOrEmpty(portal.SeoDefaultContentTitle))
            {
                txtSEOContentTitle.Text = portal.SeoDefaultContentTitle.ToString();
            }
            else
            {
                txtSEOContentTitle.Text = "<NAME>";
            }
            if (!string.IsNullOrEmpty(portal.SeoDefaultContentDescription))
            {
                txtSEOContentDescription.Text = portal.SeoDefaultContentDescription.ToString();
            }
            else
            {
                txtSEOContentDescription.Text = "<NAME>";
            }

            if (portal.SeoDefaultContentKeyword != null)
            {
                txtSEOContentKeyword.Text = portal.SeoDefaultContentKeyword.ToString();
            }
        }
        #endregion
    }
}