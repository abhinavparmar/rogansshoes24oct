﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="DefaultSeo.ascx.cs" Inherits="SiteAdmin.Secure.Marketing.SEO.SEOSettings.DefaultSeo.DefaultSeo" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/StoreName.ascx" TagName="StoreName" TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="ManufacturerAutoComplete" Src="~/SiteAdmin/Controls/Default/ManufacturerAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="CategoryAutoComplete" Src="~/SiteAdmin/Controls/Default/CategoryAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ProductTypeAutoComplete" Src="~/SiteAdmin/Controls/Default/ProductTypeAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>

<asp:UpdatePanel ID="updatepanelProductList" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        
        <%--    <div class="LeftFloat" style="width: 70%;">
            </div>
            <div class="LeftFloat" align="right" style="width: 30%">
                <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
                <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
            </div>--%>
        
        <div>
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>
        <div class="FormView">
            <h4 class="SubTitle">Store Settings</h4>
            <div style="margin: 10px 0px 15px 0px;">
                <div class="FieldStyle">
                    Select Store
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="StoreList" AutoPostBack="true" runat="server" OnSelectedIndexChanged="Storelist_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
           <h4 class="SubTitle">Default Product Settings

              <div class="tooltip">
                  <a href="javascript:void(0);" class="learn-more"><span>
                      <asp:Localize ID="Localize1" runat="server"></asp:Localize></span></a>
                  <div class="content">
                      <h6>Help</h6>
                      <p>
                          You can add the following substitution variables in your default meta tag text that will be substituted with the appropriate values at runtime when the page is displayed.
                      </p>
                      <p>
                          &lt;NAME&gt; - Will substitute the Product Name.<br />
                          &lt;PRODUCT_NUM&gt; - Will substitute the Product Number (only for Product Pages).<br />
                          &lt;SKU&gt; - Will substitute the Product SKU (only for Product Pages).<br />
                          &lt;BRAND&gt; - Will substitute the Product Brand (only for Product Pages).
                      </p>
                  </div>
              </div>

            </h4>
            <div class="FieldStyle">
                SEO Product Title
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOProductTitle" runat="server"  Width="152px"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                SEO Product Description
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOProductDescription" runat="server"   Width="450px" TextMode="MultiLine"
                    Height="150px"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                SEO Product Keyword
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOProductKeyword" runat="server" Width="152px"></asp:TextBox>
            </div>
            <h4 class="SubTitle">Default Category Settings

             <div class="tooltip">
                 <a href="javascript:void(0);" class="learn-more"><span>
                     <asp:Localize ID="Localize3" runat="server"></asp:Localize></span></a>
                 <div class="content">
                     <h6>Help</h6>
                     <p>
                         You can add the following substitution variables in your default meta tag text that will be substituted with the appropriate values at runtime when the page is displayed.
                     </p>
                     <p>
                         &lt;NAME&gt; - Will substitute the Category Name.
                     </p>
                 </div>
             </div>

            </h4>
            <div class="FieldStyle">
                SEO Category Title
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOCategoryTitle" runat="server" Width="152px"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                SEO Category Description
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOCategoryDescription" runat="server" Width="450px" TextMode="MultiLine"
                    Height="150px"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                SEO Category Keyword
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOCategoryKeyword" runat="server" Width="152px"></asp:TextBox>
            </div>
            <h4 class="SubTitle">Default Content Page Settings
            <div class="tooltip">
                <a href="javascript:void(0);" class="learn-more"><span>
                    <asp:Localize ID="Localize4" runat="server"></asp:Localize></span></a>
                <div class="content">
                    <h6>Help</h6>
                    <p>
                        You can add the following substitution variables in your default meta tag text that will be substituted with the appropriate values at runtime when the page is displayed.
                    </p>
                    <p>
                        &lt;NAME&gt; - Will substitute the Content Page title.
                    </p>
                </div>
            </div>
            </h4>
            <div class="FieldStyle">
                SEO Content Title
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOContentTitle"  runat="server" Width="152px"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                SEO Content Description
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOContentDescription" runat="server" Width="450px" TextMode="MultiLine"
                    Height="150px"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                SEO Content Keyword
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOContentKeyword" runat="server" Width="152px"></asp:TextBox>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer1" SpacerHeight="15" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div class="ClearBoth" align="left">
                <br />
            </div>
            <div>
                <asp:ImageButton ID="btnSubmit" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
                <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
