﻿<%@ Page Language="C#" AutoEventWireup="True" ValidateRequest="false" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" CodeBehind="Default.aspx.cs" Inherits="SiteAdmin.Secure.Marketing.SEO.SEOSettings.Default" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ManufacturerAutoComplete" Src="~/SiteAdmin/Controls/Default/ManufacturerAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="CategoryAutoComplete" Src="~/SiteAdmin/Controls/Default/CategoryAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ProductTypeAutoComplete" Src="~/SiteAdmin/Controls/Default/ProductTypeAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="DefaultSeo" Src="~/SiteAdmin/Secure/Marketing/SEO/SEOSettings/DefaultSeo/DefaultSeo.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ProductSeo" Src="~/SiteAdmin/Secure/Marketing/SEO/SEOSettings/Products/ProductSeo.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="CategorySeo" Src="~/SiteAdmin/Secure/Marketing/SEO/SEOSettings/Categories/CategorySeo.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ContentSeo" Src="~/SiteAdmin/Secure/Marketing/SEO/SEOSettings/Content/ContentSeo.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="Redirect301" Src="~/SiteAdmin/Secure/Marketing/SEO/SEOSettings/URLRedirect/UrlRedirect.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <script type="text/javascript">
        function ActiveTabChanged(sender, e) {
            var Tab = document.getElementById('<%=tabSeoSettings.ClientID%>');
            var activeTabIndex = document.getElementById('<%=hdnActiveTabIndex.ClientID%>');
            activeTabIndex.value = sender.get_activeTabIndex();
        }
    </script>
    <asp:HiddenField ID="hdnActiveTabIndex" runat="server" Value="0" />
    <div>
          <h1>Manage SEO  </h1>
    </div>
     <div class="ClearBoth">
                <p>
                    Optimize your store for search engines so visitors can find your store.
                </p>
            </div>
    <div>
        <ZNode:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>

    <div style="clear: both;">
        <ajaxToolKit:TabContainer ID="tabSeoSettings" runat="server" ActiveTabIndex="0" OnClientActiveTabChanged="ActiveTabChanged">
              <ajaxToolKit:TabPanel ID="pnlDefaultSeo" runat="server">
                <HeaderTemplate>
                     Default Settings
                </HeaderTemplate>
                <ContentTemplate>
                    <ZNode:DefaultSeo ID="DefaultSeo" runat="server"></ZNode:DefaultSeo>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="pnlProductSeo" runat="server">
                <HeaderTemplate>
                    Products
                </HeaderTemplate>
                <ContentTemplate>
                   <ZNode:ProductSeo ID="ProductSeo" runat="server"></ZNode:ProductSeo>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="pnlCategorySeo" runat="server">
                <HeaderTemplate>
                   Categories
                </HeaderTemplate>
                <ContentTemplate>
                   <ZNode:CategorySeo ID="CategorySeo" runat="server"></ZNode:CategorySeo>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="pnlContentSeo" runat="server">
                <HeaderTemplate>
                     Content Pages
                </HeaderTemplate>
                <ContentTemplate>
                   <ZNode:ContentSeo ID="ContentSeo" runat="server" />
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
             <ajaxToolKit:TabPanel ID="pnl301Redirect" runat="server">
                <HeaderTemplate>
                     301 URL Redirect
                </HeaderTemplate>
                <ContentTemplate>
                   <ZNode:Redirect301 ID="Redirect301" runat="server" />
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
        </ajaxToolKit:TabContainer>
    </div>
</asp:Content>