﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="URLRedirect.ascx.cs" Inherits="WebApp.SiteAdmin.Secure.Marketing.SEO.SEOSettings.URLRedirect.URLRedirect" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>

<asp:UpdatePanel ID="updatepanelProductList" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div>
            <div class="LeftFloat" style="width: 50%">
                <h1>301 URL Redirect</h1>
            </div>
            <div class="ButtonStyle">
                <zn:LinkButton ID="btnAddUrlRedirect" runat="server" CausesValidation="False"
                    ButtonType="Button" OnClick="BtnAddUrlRedirect_Click" Text="Add 301 Redirect"
                    ButtonPriority="Primary" />
            </div>
        </div>
        <div class="ClearBoth" align="left">
            <br />
        </div>
        <div class="SearchForm">
            <h4 class="SubTitle">Search URL Redirects</h4>
            <asp:Panel ID="pnlSearch" DefaultButton="btnSearch" runat="server">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">From URL</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtOldUrl" runat="server"></asp:TextBox></span>
                        <div>
                            <asp:RegularExpressionValidator ID="regProductName" runat="server" ControlToValidate="txtOldUrl"
                                ErrorMessage="Enter valid URL" Display="Dynamic"
                                CssClass="Error" ValidationExpression="[0-9A-Za-z\s\',.:&%#$@/_-]+"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">To URL</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtNewUrl" runat="server"></asp:TextBox></span>
                        <div>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtNewUrl"
                                ErrorMessage="Enter valid URL" Display="Dynamic"
                                CssClass="Error" ValidationExpression="[0-9A-Za-z\s\',.:&%#$@/_-]+"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">URL Status</span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlURLStatus" runat="server">
                                <asp:ListItem Text="All" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Enabled" Value="true"></asp:ListItem>
                                <asp:ListItem Text="Disabled" Value="false"></asp:ListItem>
                            </asp:DropDownList>
                        </span>
                    </div>
                </div>
                <div class="ClearBoth">
                </div>
                <div>
                    <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_clear_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                        runat="server" AlternateText="Clear" OnClick="BtnClearSearch_Click" />
                    <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                        runat="server" AlternateText="Search" OnClick="BtnSearch_Click" />
                </div>
            </asp:Panel>
            <div class="ClearBoth">
            </div>
        </div>
        <div class="ClearBoth">
            <br />
        </div>
        <h4 class="GridTitle">URL Redirect List</h4>
        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
            CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" PageSize="25"
            AllowSorting="True" EmptyDataText="No URL redirects exist in the database." OnRowDeleting="UxGrid_RowDeleting"
            OnRowDataBound="UxGrid_RowDataBound">
            <Columns>
                <asp:BoundField DataField="UrlRedirectID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText="From URL" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "OldUrl").ToString().Replace("~/",string.Empty) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="To URL" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "NewUrl").ToString().Replace("~/",string.Empty) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Enabled" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img alt="" id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "IsActive").ToString()))%>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:ButtonField CommandName="Edit" Text="Edit &raquo" ButtonType="Link">
                    <ControlStyle CssClass="Button" />
                </asp:ButtonField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="btnDelete" CssClass="Button" runat="server" Text="Delete &raquo"
                            CommandName="Delete" CommandArgument="Delete" />

                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
