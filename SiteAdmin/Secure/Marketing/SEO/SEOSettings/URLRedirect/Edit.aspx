<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" Inherits="SiteAdmin.Secure.Marketing.SEO.SEOSettings.URLRedirect.Edit" CodeBehind="Edit.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="LeftFloat" style="width: 70%">
        <h1>
            <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
        <uc2:DemoMode ID="DemoMode1" runat="server" />
    </div>
    <div class="LeftFloat" align="right" style="width: 30%">
        <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../../Themes/Images/buttons/button_submit_highlight.gif';"
            onmouseout="this.src='../../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
            runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
        <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
            onmouseout="this.src='../../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
            runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
    </div>
    <div class="ClearBoth" align="left">
    </div>
    <div class="FormView">
        <div>
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>
        <div>
            <uc1:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="FieldStyle">
            URL to Redirect From<span class="Asterix">*</span><br />
            <small style="display: none">Be sure to include the .aspx file extension(ex. apple.aspx or product.aspx?zpid=302).</small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtOldUrl" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
            <asp:RequiredFieldValidator ID="OldSeourlFieldValidator" CssClass="Error" ControlToValidate="txtOldUrl"
                runat="server" ErrorMessage="Enter From Seo URL"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            URL to Redirect To<span class="Asterix">*</span><br />
            <small style="display: none">Be sure to include the .aspx file extension(ex. apple.aspx or category.aspx?zcid=81).</small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtNewUrl" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
            <asp:RequiredFieldValidator ID="NewSeourlFieldValidator" CssClass="Error" ControlToValidate="txtNewUrl"
                runat="server" ErrorMessage="Enter To Seo URL"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            Enable this Redirection
        </div>
        <div class="ValueStyle">
            <asp:CheckBox ID='chkIsActive' Checked="true" runat="server" Text="" /></div>
        <div class="ClearBoth" align="left">
            <br />
        </div>
        <div>
            <asp:ImageButton ID="btnSubmit" onmouseover="this.src='../../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>
