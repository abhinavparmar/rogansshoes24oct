using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Marketing.SEO.SEOSettings.URLRedirect
{
    /// <summary>
    /// Represents the SiteAdmin.Secure.Marketing.SEO.URLRedirect.Edit class
    /// </summary>
    public partial class Edit : System.Web.UI.Page
    {
        #region Private Variables
        private int ItemId;
        private string ListPageLink = "~/SiteAdmin/Secure/Marketing/SEO/SEOSettings/Default.aspx?mode=urlredirect";
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the fields 
        /// </summary>
        protected void BindData()
        {
            UrlRedirectAdmin urlRedirectAdmin = new UrlRedirectAdmin();
            UrlRedirect urlRedirectEntity = null;

            if (this.ItemId > 0)
            {
                urlRedirectEntity = urlRedirectAdmin.GetById(this.ItemId);

                if (urlRedirectEntity != null)
                {
                    txtNewUrl.Text = this.FormattedUrl(urlRedirectEntity.NewUrl);
                    txtOldUrl.Text = this.FormattedUrl(urlRedirectEntity.OldUrl);
                    chkIsActive.Checked = urlRedirectEntity.IsActive;
                }
            }
        }
        #endregion

        #region Events

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (!Page.IsPostBack)
            {
                // If edit func then bind the data fields
                if (this.ItemId > 0)
                {
                    lblTitle.Text = "Edit URL 301 Redirect";

                    // Bind data to the fields on the page
                    this.BindData();
                }
                else
                {
                    lblTitle.Text = "Add URL 301 Redirect";
                }
            }
        }

        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            UrlRedirectAdmin urlRedirectAdmin = new UrlRedirectAdmin();
            UrlRedirect urlRedirectEntity = new UrlRedirect();

            bool status = false;
            string oldSeoURL = string.Empty;
            bool activeInd = false;
            bool urlRedirectExists = false;

            if (this.ItemId > 0)
            {
                urlRedirectEntity = urlRedirectAdmin.GetById(this.ItemId);
            }

            oldSeoURL = urlRedirectEntity.OldUrl;
            activeInd = urlRedirectEntity.IsActive;                     

            urlRedirectEntity.IsActive = chkIsActive.Checked;
            urlRedirectEntity.OldUrl = this.MakeUrl(txtOldUrl.Text.Trim());
            urlRedirectEntity.NewUrl = this.MakeUrl(txtNewUrl.Text.Trim());

            if (urlRedirectEntity.OldUrl.ToLower() == urlRedirectEntity.NewUrl.ToLower())
            {
                lblMsg.Text = "Please enter the valid URL.";
                return;
            }

            UrlRedirect _urlRedirect = new UrlRedirect();
            TList<UrlRedirect> listUrl = new TList<UrlRedirect>();
            listUrl = urlRedirectAdmin.GetAll();
            if (listUrl.Count > 0)
            {
                foreach (UrlRedirect urlRedirect in listUrl)
                {
                    if (urlRedirectEntity.OldUrl.ToLower() == urlRedirect.NewUrl.ToLower())
                    {
                        urlRedirectExists = true;
                    }
                }
            }

            if (urlRedirectExists)
            {
                lblMsg.Text = "URL Redirect loop is not allowed";
                return;
            }

            urlRedirectExists = urlRedirectAdmin.Exists(urlRedirectEntity.OldUrl, urlRedirectEntity.NewUrl, urlRedirectEntity.UrlRedirectID);

            if (urlRedirectExists)
            {
                lblMsg.Text = "From URL already exists. Unable to update SEO Friendly URL.";
                return;
            }

            if (!urlRedirectAdmin.SeoUrlExists(urlRedirectEntity.NewUrl))
            {
                lblMsg.Text = "Please enter the valid URL in the To URL.";
                return;
            }

            urlRedirectEntity.IsActive = chkIsActive.Checked;
            urlRedirectEntity.OldUrl = this.MakeUrl(txtOldUrl.Text.Trim());
            urlRedirectEntity.NewUrl = this.MakeUrl(txtNewUrl.Text.Trim());

            if (this.ItemId > 0)
            {
                status = urlRedirectAdmin.Update(urlRedirectEntity);
             
                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit URL 301 Redirect ", txtNewUrl.Text);
            }
            else
            {
                status = urlRedirectAdmin.Add(urlRedirectEntity);
                
                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Added URL 301 Redirect ", txtNewUrl.Text);
            }

            if (status)
            {
                Response.Redirect(this.ListPageLink);
            }
            else
            {
                lblMsg.Text = "Unable to process your request.";
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ListPageLink);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Formatted Url Method
        /// </summary>
        /// <param name="url">The value of url</param>
        /// <returns>Returns the formatted url</returns>
        private string FormattedUrl(string url)
        {
            if (url.Length > 0)
            {
                url = url.Replace("~/", string.Empty);
            }

            return url;
        }

        /// <summary>
        /// Make Url Method
        /// </summary>
        /// <param name="seoUrl">The value of seoUrl</param>
        /// <returns>Returns the url</returns>
        private string MakeUrl(string seoUrl)
        {
            seoUrl = "~/" + seoUrl;

            return seoUrl;
        }
        #endregion
    }
}