using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Marketing.SEO.SEOSettings.Content
{
    /// <summary>
    /// Reprsents the SiteAdmin.Secure.Marketing.SEO.SEOSettings.Content.Edit.Edit class
    /// </summary>
    public partial class Edit : System.Web.UI.Page
    {
        #region Protected Variables
        private int ItemId;
        private string ManageLink = "~/SiteAdmin/Secure/Marketing/SEO/SEOSettings/Default.aspx?mode=content";
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (!Page.IsPostBack)
            {
                // If edit func then bind the data fields
                if (this.ItemId > 0)
                {
                    lblTitle.Text = "Edit Page - ";
                    
                    // Bind data to the fields on the page
                    this.BindData();
                }
            }
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the fields 
        /// </summary>
        protected void BindData()
        {
            ContentPageAdmin pageAdmin = new ContentPageAdmin();
            ContentPage contentPage = new ContentPage();

            if (this.ItemId > 0)
            {
                contentPage = pageAdmin.GetPageByID(this.ItemId);

                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (profiles.StoreAccess != "AllStores")
                {
                    string[] stores = profiles.StoreAccess.Split(',');
                    Array Stores = (Array)stores;
                    int found = Array.IndexOf(Stores, contentPage.PortalID.ToString());
                    if (found == -1)
                    {
                        Response.Redirect("~/SiteAdmin/Secure/Marketing/SEO/SEOSettings/Default.aspx", true);
                    }
                }

                // Set fields 
                lblTitle.Text += contentPage.Title;
                txtTitle.Text = Server.HtmlDecode(contentPage.Title);
                txtSEOMetaDescription.Text = Server.HtmlDecode(contentPage.SEOMetaDescription);
                txtSEOMetaKeywords.Text = Server.HtmlDecode(contentPage.SEOMetaKeywords);
                txtSEOTitle.Text = Server.HtmlDecode(contentPage.SEOTitle);
                txtSEOUrl.Text = contentPage.SEOURL;

                // Get content
                ctrlHtmlText.Html = pageAdmin.GetPageHTMLByName(contentPage.Name, contentPage.PortalID, contentPage.LocaleId.ToString());
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            UrlRedirectAdmin urlRedirectAdmin = new UrlRedirectAdmin();
            ContentPageAdmin pageAdmin = new ContentPageAdmin();
            ContentPage contentPage = new ContentPage();
            string mappedSEOUrl = string.Empty;

            bool allowDelete = true;

            // If edit mode then retrieve data first
            if (this.ItemId > 0)
            {
                contentPage = pageAdmin.GetPageByID(this.ItemId);
                
                // Override this setting
                allowDelete = contentPage.AllowDelete;

                if (contentPage.SEOURL != null)
                {
                    mappedSEOUrl = contentPage.SEOURL;
                }
            }

            // Set values
            contentPage.ActiveInd = true;
            contentPage.Title = Server.HtmlEncode(txtTitle.Text);
            contentPage.SEOMetaDescription = Server.HtmlEncode(txtSEOMetaDescription.Text);
            contentPage.SEOMetaKeywords = Server.HtmlEncode(txtSEOMetaKeywords.Text);
            contentPage.SEOTitle = Server.HtmlEncode(txtSEOTitle.Text);
            contentPage.SEOURL = null;
            if (txtSEOUrl.Text.Trim().Length > 0)
            {
                contentPage.SEOURL = txtSEOUrl.Text.Trim().Replace(" ", "-");
            }

            if (string.Compare(contentPage.SEOURL, mappedSEOUrl, true) != 0)
            {
                if (urlRedirectAdmin.SeoUrlExists(contentPage.SEOURL, contentPage.ContentPageID))
                {
                    lblMsg.Text = "The SEO Page Name you entered is already in use on another page. Please select another name for your SEO Page Name";
                    return;
                }
            }
            bool retval = false;

            if (this.ItemId > 0)
            {
                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit Page - " + contentPage.Title, contentPage.Title);

                // Update code here
                retval = pageAdmin.UpdatePage(contentPage, ctrlHtmlText.Html, ctrlHtmlText.Html, contentPage.PortalID, contentPage.LocaleId.ToString(), HttpContext.Current.User.Identity.Name, mappedSEOUrl, chkAddURLRedirect.Checked);
            }

            if (retval)
            {
                // Redirect to main page
                Response.Redirect(this.ManageLink);
            }
            else
            {
                if (contentPage.SEOURL != null)
                {
                    // Display error message
                    lblMsg.Text = "Failed to update page. Please check with SEO Url settings and try again.";
                }
                else
                {
                    // Display error message
                    lblMsg.Text = "Failed to update page. Please try again.";
                }
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ManageLink);
        }
        #endregion
    }
}