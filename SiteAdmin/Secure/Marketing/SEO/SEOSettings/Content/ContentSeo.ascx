﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContentSeo.ascx.cs" Inherits="WebApp.SiteAdmin.Secure.Marketing.SEO.SEOSettings.Content.ContentSeo" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>

<asp:UpdatePanel ID="updatepanelProductList" runat="server">
    <ContentTemplate>
    <div class="FormView">
        <p>
            Update Content Page level SEO settings such as title, description, meta tags and page URL.
        </p>
        <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
        <h4 class="GridTitle">Page List</h4>
        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
            CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" PageSize="25"
            AllowSorting="True" EmptyDataText="No content pages exist in the database.">
            <Columns>
                <asp:BoundField DataField="ContentPageID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="Name" HeaderText="Page Name" HeaderStyle-HorizontalAlign="Left"
                    ItemStyle-Width="40%" />
                <asp:TemplateField HeaderText="Store Name" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# GetStoreName(DataBinder.Eval(Container.DataItem,"PortalID").ToString()) %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:ButtonField CommandName="Edit" Text="Manage SEO &raquo" ButtonType="Link">
                    <ControlStyle CssClass="actionlink" />
                </asp:ButtonField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        </div>
    </div>
    </ContentTemplate>
</asp:UpdatePanel>
