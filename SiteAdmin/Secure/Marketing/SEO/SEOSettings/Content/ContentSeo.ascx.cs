﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.Framework.Business;
namespace WebApp.SiteAdmin.Secure.Marketing.SEO.SEOSettings.Content
{
    public partial class ContentSeo : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private string EditLink = "~/SiteAdmin/Secure/Marketing/SEO/SEOSettings/Content/Edit.aspx?itemid=";
        #endregion

        #region Public Methods
        /// <summary>
        /// Returns the path to open the html content pages
        /// </summary>
        /// <param name="pageName">The value of pagename</param>
        /// <param name="seoURL">The value of seoURL</param>
        /// <returns>Returns the pageURL</returns>
        public string GetPageURL(string pageName, object seoURL)
        {
            // If page name is Home, then it must be open with default page.
            if (pageName.Equals("Home"))
            {
                return "~/";
            }

            string seoUrl = string.Empty;

            if (seoURL != null)
            {
                if (seoURL.ToString().Length > 0)
                {
                    seoUrl = seoURL.ToString();
                }
            }

            // Otherwise the content pages should be open with content.aspx page more Specific to Content pages
            return ZNodeSEOUrl.MakeURL(pageName, SEOUrlType.ContentPage, seoUrl);
        }

        /// <summary>
        /// Get Store Name by PortalId
        /// </summary>
        /// <param name="portalID">The value of portalID</param>
        /// <returns>Returns the Store Name</returns>
        public string GetStoreName(string portalID)
        {
            PortalAdmin portalAdmin = new PortalAdmin();

            return portalAdmin.GetStoreNameByPortalID(portalID);
        }

        #endregion

        #region Page Load Events

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindGridData();
            }
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGridData();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // property to an Integer.
                int index = Convert.ToInt32(e.CommandArgument);

                // Get the values from the appropriate
                // cell in the GridView control.
                GridViewRow selectedRow = uxGrid.Rows[index];

                TableCell Idcell = selectedRow.Cells[0];
                string Id = Idcell.Text;

                if (e.CommandName == "Edit")
                {
                    Response.Redirect(this.EditLink + Id);
                }
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindGridData()
        {
            ContentPageAdmin pageAdmin = new ContentPageAdmin();
            TList<ContentPage> pages = pageAdmin.GetAllPages();
            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (profiles.StoreAccess != "AllStores")
            {
                pages.Filter = string.Concat("PortalID IN [", profiles.StoreAccess, "]");
            }

            pages.Sort("Name");
            uxGrid.DataSource = pages;
            uxGrid.DataBind();
        }
        #endregion
    }
}