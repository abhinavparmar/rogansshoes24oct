<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" ValidateRequest="false"
    AutoEventWireup="True" Inherits="SiteAdmin.Secure.Marketing.SEO.SEOSettings.Content.Edit" CodeBehind="Edit.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="LeftFloat" style="width: 70%">
        <h1>
            <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
        <uc2:DemoMode ID="DemoMode1" runat="server" />
    </div>
    <div class="LeftFloat" align="right" style="width: 30%">
        <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../../Themes/Images/buttons/button_submit_highlight.gif';"
            onmouseout="this.src='../../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
            runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
        <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
            onmouseout="this.src='../../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
            runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
    </div>
    <div class="ClearBoth" align="left">
    </div>
    <div class="FormView">
        <div>
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>
        <div>
            <uc1:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="SEOProductPageAdd">
            <h4 class="SubTitle">General Settings</h4>
            <div class="FieldStyle">
                Page Title
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtTitle" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
            </div>
            <h4 class="SubTitle">SEO Settings</h4>
            <div class="FieldStyle">
                SEO Title 
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOTitle" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
            </div>
            <div class="FieldStyle">
               SEO Keywords
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOMetaKeywords" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                SEO Description
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOMetaDescription" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                SEO Page Name
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOUrl" runat="server" MaxLength="100" Columns="50"></asp:TextBox>
                 <div class="tooltip">
                          <a href="javascript:void(0);" class="learn-more"><span>
                              <asp:Localize ID="Localize2" runat="server"></asp:Localize></span></a>
                          <div class="content">
                              <h6>Help</h6>
                              <p>
                                 Specify a page name with alpha-numeric characters only. Spaces should be replaced with "-"
                              </p>
                          </div>
                      </div>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtSEOUrl"
                    CssClass="Error" Display="Dynamic" ErrorMessage="Enter valid SEO Page Name"
                    SetFocusOnError="True" ValidationExpression="([A-Za-z0-9-_]+)"></asp:RegularExpressionValidator>
            </div>
            <div class="FieldStyle" style="display: none">Add 301 redirect on URL changes.</div>
            <div class="ValueStyle" style="display: none">
                <asp:CheckBox ID="chkAddURLRedirect" runat="server" Text='' />
            </div>
            <div class="ClearBoth" align="left">
                <br />
            </div>
            <h4 class="SubTitle">Page Content</h4>
            <div class="ValueStyle">
                <uc1:HtmlTextBox ID="ctrlHtmlText" runat="server"></uc1:HtmlTextBox>
            </div>
            <div class="ClearBoth" align="left">
                <br />
            </div>
        </div>
        <div>
            <asp:ImageButton ID="btnSubmit" onmouseover="this.src='../../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>
