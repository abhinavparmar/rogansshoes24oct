<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" ValidateRequest="false" Inherits="SiteAdmin.Secure.Marketing.SEO.SEOSettings.Products.Edit" CodeBehind="Edit.aspx.cs" %>

<%@ Register TagPrefix="uc2" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="ZNode" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="LeftFloat" style="width: 70%">
        <h1>
            <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
    </div>
    <div class="LeftFloat" align="right" style="width: 30%">
        <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../../Themes/Images/buttons/button_submit_highlight.gif';"
            onmouseout="this.src='../../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
            runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
        <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
            onmouseout="this.src='../../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
            runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
    </div>
    <div class="ClearBoth" align="left">
    </div>
    <div class="FormView">
        <asp:Label ID="lblError" CssClass="Error" runat="server"></asp:Label>
        <div>
            <div class="SEOProductPageAdd">
                <h4 class="SubTitle">General Information</h4>
                <div class="FieldStyle">
                    Product Name<span class="Asterix">*</span>
                </div>
                <div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtProductName"
                        ErrorMessage="Enter Product Name" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtProductName" runat="server" Width="152px"></asp:TextBox>
                </div>
                <h4 class="SubTitle">SEO Settings</h4>
                <div class="FieldStyle">
                    SEO Title
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtSEOTitle" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
                </div>
                <div class="FieldStyle">
                   SEO Keywords
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtSEOMetaKeywords" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
                </div>
                <div class="FieldStyle">
                   SEO Description
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtSEOMetaDescription" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
                </div>
                <div class="FieldStyle">
                    SEO Page Name
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtSEOUrl" runat="server" MaxLength="100" Columns="50"></asp:TextBox> 
                    <div class="tooltip">
                          <a href="javascript:void(0);" class="learn-more"><span>
                              <asp:Localize ID="Localize2" runat="server"></asp:Localize></span></a>
                          <div class="content">
                              <h6>Help</h6>
                              <p>
                                 Specify a page name with alpha-numeric characters only. Spaces should be replaced with "-"
                              </p>
                          </div>
                      </div>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtSEOUrl"
                        CssClass="Error" Display="Dynamic" ErrorMessage="Enter a valid SEO Page Name"
                        SetFocusOnError="True" ValidationExpression="([A-Za-z0-9-_]+)"></asp:RegularExpressionValidator>
                </div>
                <div class="FieldStyle">301 Redirect</div>
                <div class="ValueStyle">
                    <asp:CheckBox ID="chkAddURLRedirect" runat="server" Text='Enable 301 redirects for URL changes' />
                </div>
                <div class="ClearBoth" align="left">
                    <br />
                </div>
                <h4 class="SubTitle">Description</h4>
                <div class="FieldStyle">
                    Short Description
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtshortdescription" runat="server" Width="500px" TextMode="MultiLine"
                        Height="75px" MaxLength="100"></asp:TextBox>
                </div>
                <div class="FieldStyle">
                    Long Description
                </div>
                <div class="ValueStyle" style="width: 70%">
                    <ZNode:HtmlTextBox ID="ctrlHtmlDescription" runat="server"></ZNode:HtmlTextBox>
                </div>
                <div class="FieldStyle">
                    Product Features
                </div>
                <div class="ValueStyle" style="width: 70%">
                    <ZNode:HtmlTextBox ID="ctrlHtmlPrdFeatures" runat="server"></ZNode:HtmlTextBox>
                </div>
                <div class="FieldStyle">
                    Product Specifications
                </div>
                <div class="ValueStyle" style="width: 70%">
                    <ZNode:HtmlTextBox ID="ctrlHtmlPrdSpec" runat="server"></ZNode:HtmlTextBox>
                </div>
                <div class="FieldStyle">
                    Additional Information
                </div>
                <div class="ValueStyle" style="width: 70%">
                    <ZNode:HtmlTextBox ID="ctrlHtmlProdInfo" runat="server"></ZNode:HtmlTextBox>
                </div>
            </div>
            <div>
                <uc2:Spacer ID="Spacer8" SpacerHeight="20" SpacerWidth="3" runat="server"></uc2:Spacer>
            </div>
        </div>
        <div class="ClearBoth" align="left">
            <br />
        </div>
        <div>
            <asp:ImageButton ID="btnSubmit" onmouseover="this.src='../../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>
