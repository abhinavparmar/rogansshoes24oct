﻿<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="SiteAdmin.Secure.Marketing.SEO.GoogleSiteMap.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <div class="LeftFloat" style="width: 70%">
        <h1>Google Site Map</h1>
    </div>
    <div class="LeftFloat" align="right" style="width: 30%">
        <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
            onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
            runat="server" AlternateText="Submit" ValidationGroup="sitemap" OnClick="BtnSubmit_Click" />
        <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
            onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
            runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
    </div>
    <div class="ClearBoth" align="left">
    </div>
    <div class="YellowBox" runat="server" id="dvMsg" visible="false">
        <div>
            <asp:Label ID="lblmsg" Font-Bold="true" runat="server"></asp:Label><br />
        </div>
        <div>
            <asp:PlaceHolder runat="server" ID="plfileNames"></asp:PlaceHolder>
        </div>
    </div>
    <div class="FormView">
        <div>
            Create a XML site map or Product Feed for Google.
        </div>
        <br />
        <div class="FieldStyle">
            Frequency
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlFrequency" runat="server">
                <asp:ListItem Text="Daily" Value="Daily"></asp:ListItem>
                <asp:ListItem Text="Always" Value="Always"></asp:ListItem>
                <asp:ListItem Text="Hourly" Value="Hourly"></asp:ListItem>
                <asp:ListItem Text="Weekly" Value="Weekly"></asp:ListItem>
                <asp:ListItem Text="Monthly" Value="Monthly"></asp:ListItem>
                <asp:ListItem Text="Yearly" Value="Yearly"></asp:ListItem>
                <asp:ListItem Text="Never" Value="Never"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <asp:UpdatePanel ID="updLasMod" runat="server">
            <ContentTemplate>
                <div class="FieldStyle">
                    Last modification
                </div>
                <div class="ValueStyle">
                    <asp:RadioButtonList ID="rdbLastModified" runat="server" RepeatDirection="Vertical"
                        AutoPostBack="true" OnSelectedIndexChanged="RdbLastModified_SelectedIndexChanged">
                        <asp:ListItem Text="None" Value="None" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Use the database update date" Value="DB"></asp:ListItem>
                        <asp:ListItem Text="Use date / time of this update" Value="CUSTOM"></asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:TextBox ID="txtLastModDateTime" runat="server" Visible="false"></asp:TextBox>
                    <asp:CustomValidator runat="server" ID="valDateRange" Display="Dynamic" ControlToValidate="txtLastModDateTime"
                        OnServerValidate="valDateRange_ServerValidate" ErrorMessage="Enter valid date"
                        SetFocusOnError="True" ValidationGroup="sitemap" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="FieldStyle">
            Priority
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlPrioroty" runat="server">
                <asp:ListItem Text="Select Priority" Value="0"></asp:ListItem>
                <asp:ListItem Text="1.0" Value="1.0"></asp:ListItem>
                <asp:ListItem Text="1.1" Value="1.1"></asp:ListItem>
                <asp:ListItem Text="1.2" Value="1.2"></asp:ListItem>
                <asp:ListItem Text="1.3" Value="1.3"></asp:ListItem>
                <asp:ListItem Text="1.4" Value="1.4"></asp:ListItem>
                <asp:ListItem Text="1.5" Value="1.5"></asp:ListItem>
                <asp:ListItem Text="1.6" Value="1.6"></asp:ListItem>
                <asp:ListItem Text="1.7" Value="1.7"></asp:ListItem>
                <asp:ListItem Text="1.8" Value="1.8"></asp:ListItem>
                <asp:ListItem Text="1.9" Value="1.9"></asp:ListItem>
                <asp:ListItem Text="2.0" Value="2.0"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <asp:UpdatePanel ID="updType" runat="server">
            <ContentTemplate>
                <div class="FieldStyle">
                    Type of XML Site Map
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlType" AutoPostBack="true" runat="server" OnSelectedIndexChanged="DdlType_SelectedIndexChanged">
                        <asp:ListItem Text="XML Site Map" Value="GoogleXML"></asp:ListItem>
                        <asp:ListItem Text="Google Product Feed" Value="GoogleProduct"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter a Type"
                        ToolTip="Please enter a Type" ControlToValidate="ddlType" ValidationGroup="sitemap"
                        CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div visible="false" runat="server" id="dvSiteMap">
                    <div class="FieldStyle">
                        XML Site Map
                    </div>
                    <div class="ValueStyle">
                        <asp:RadioButtonList ID="chkXMLSiteMapType" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="Category" Value="Category"></asp:ListItem>
                            <asp:ListItem Text="Content Pages" Value="Content"></asp:ListItem>
                            <%--<asp:ListItem Text="All" Value="All"></asp:ListItem>--%>
                        </asp:RadioButtonList>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="FieldStyle">
            XML File Name
            <br />
            <small>Example :<br />
                MySiteMap (Do not specify file extension)</small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtXMLFileName" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter a File Name"
                ToolTip="Please enter a File Name" ControlToValidate="txtXMLFileName" ValidationGroup="sitemap"
                CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtXMLFileName"
                CssClass="Error" Display="Dynamic" ErrorMessage="Enter valid XML Site Map file name"
                ValidationGroup="sitemap" SetFocusOnError="True" ValidationExpression="([A-Za-z0-9-_]+)"></asp:RegularExpressionValidator>
        </div>
        <div class="FieldStyle">
            Select Stores
        </div>
        <div class="ValueStyle">
            <div style="margin-left: 7px; margin-top: 5px;">
                <asp:CheckBox ID="chkAllStores" runat="server" Text="All Stores" AutoPostBack="true"
                    OnCheckedChanged="ChkAllStores_checkedChanged" />
            </div>
        </div>
        <div class="FieldStyle">
        </div>
        <div class="ValueStyle">
            <asp:CheckBoxList ID="StoreCheckList" runat="server" RepeatDirection="Horizontal" RepeatColumns="3"
                CellPadding="5" />
        </div>
        <div class="ClearBoth" align="left">
            <br />
        </div>
        <div>
            <asp:ImageButton ID="btnSubmit" onmouseover="this.src=''../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src=''../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" ValidationGroup="sitemap"
                CausesValidation="true" />
            <asp:ImageButton ID="ImageButton1" CausesValidation="False" onmouseover="this.src=''../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src=''../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>
