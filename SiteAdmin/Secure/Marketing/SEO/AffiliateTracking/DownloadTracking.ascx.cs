using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace SiteAdmin.Secure.Marketing.SEO.AffiliateTracking
{
    /// <summary>
    /// Represents the SiteAdmin - Secure.Marketing.SEO.AffiliateTracking.DownloadTracking user control class
    /// </summary>
    public partial class DownloadTracking : System.Web.UI.UserControl
    {
        #region Private member Variables
        private string DefaultPageLink = "~/SiteAdmin/Secure/Marketing/Default.aspx";
        private DataDownloadAdmin adminAccess = new DataDownloadAdmin();
        private DataManagerAdmin manager = new DataManagerAdmin();
        #endregion

        #region Helper Methods
        /// <summary>
        ///  Returns string for a Given Dataset Values
        /// </summary>
        /// <param name="strFileName">The value of strFileName</param>
        /// <param name="gridViewControl">The value of gridViewControl</param>
        public void ExportDataToExcel(string strFileName, GridView gridViewControl)
        {
            Response.Clear();
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=" + strFileName);
            
            // Set as Excel as the primary format
            Response.AddHeader("Content-Type", "application/Excel");
            Response.ContentType = "application/Excel";
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gridViewControl.RenderControl(htw);
            Response.Write(sw.ToString());

            gridViewControl.Dispose();

            Response.End();
        }

        /// <summary>
        ///  Returns string for a Given Dataset Values
        /// </summary>
        /// <param name="fileName">The value of fileName</param>
        /// <param name="Data">The value of Data</param>
        public void ExportDataToCSV(string fileName, byte[] Data)
        {
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Response.AddHeader("Content-Type", "application/Excel");
            
            // Set as text as the primary format
            Response.ContentType = "text/csv";
            Response.ContentType = "application/vnd.xls";
            Response.AddHeader("Pragma", "public");

            Response.BinaryWrite(Data);
            Response.End();
        }

        #endregion

        #region General Events

        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = this.BindTrackingData();

                if (ds.Tables[0].Rows.Count != 0)
                {
                    // Save as Excel Sheet
                    if (ddlFileSaveType.SelectedValue == ".xls")
                    {
                        // Temp Grid control
                        GridView gridView = new GridView();
                        gridView.DataSource = ds;
                        gridView.DataBind();
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Imported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Download Tracking Data", "Tracking Data");
                        this.ExportDataToExcel("DownloadedTrackingData.xls", gridView);
                    }
                    else
                    {
                        // Save as CSV Format
                        // Set Formatted Data from dataset object
                        string strData = this.adminAccess.Export(ds, true);

                        byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(strData);
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Imported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Download Tracking Data", "Tracking Data");
                        this.ExportDataToCSV("DownloadedTrackingData.csv", data);
                    }
                }
                else
                {
                    ltrlError.Text = "No Tracking Data Found";
                    return;
                }
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
                ltrlError.Text = "Failed to process your request. Please try again.";
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.DefaultPageLink);
        }
        
        /// <summary>
        /// Bind the Tracking Data
        /// </summary>
        /// <returns>Returns the DataSet</returns>
        protected DataSet BindTrackingData()
        {
            string startdate = txtStartDate.Text.Trim();
            string enddate = txtEndDate.Text.Trim();

            DataSet ds = this.manager.GetDownloadTrackingData(startdate, enddate);

            return ds;
        }

        #endregion
    }
}