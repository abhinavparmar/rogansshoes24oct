<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master"  AutoEventWireup="True" Inherits="SiteAdmin.Secure.Marketing.Promotions.CustomerReviews.Delete" Codebehind="Delete.aspx.cs" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">    
    <ZNode:DemoMode ID="DemoMode1" runat="server" />
    <h5>Please Confirm</h5>
    <p>Please confirm if you want to delete the product review titled "<b><%=ProductReviewTitle%></b>". This change cannot be undone.
    </p>
    <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label><br /><br />
     <asp:ImageButton ID="btnDelete" onmouseover="this.src='../../../../Themes/Images/buttons/button_delete_highlight.gif';"
        onmouseout="this.src='../../../../Themes/Images/buttons/button_delete.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_delete.gif"
        runat="server" AlternateText="Delete" OnClick="BtnDelete_Click" CausesValidation="true" />
    <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
        onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
        runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
    <div><ZNode:spacer id="Spacer1" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:spacer></div>
</asp:Content>

