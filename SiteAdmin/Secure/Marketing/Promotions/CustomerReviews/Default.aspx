<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" ValidateRequest="false" Inherits="SiteAdmin.Secure.Marketing.Promotions.CustomerReviews.Default" Codebehind="Default.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="ProductAutoComplete" Src="~/SiteAdmin/Controls/Default/ProductAutoComplete.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                Customer Reviews</h1>
            <p>
                Approve or disable reviews submitted by customers.
            </p>
        </div>
        <div class="ClearBoth">
            <h4 class="SubTitle">
                Search Product Reviews
            </h4>
            <asp:Panel ID="pnlReviewSearch" DefaultButton="btnSearch" runat="server">
                <div class="SearchForm">
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle">Review Title</span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="ReviewTitle" runat="server" ValidationGroup="grpSearch"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">Nick Name</span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="Name" runat="server" ValidationGroup="grpSearch"></asp:TextBox></span>
                        </div>
                    </div>
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle">Product Name</span><br />
                            <span class="ValueStyle">
                                <ZNode:ProductAutoComplete ID="ddlProductNames" runat="server" />
                            </span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">Status</span><br />
                            <span class="ValueStyle">
                                <asp:DropDownList ID="ListReviewStatus" runat="server">
                                    <asp:ListItem Selected="True" Text="All" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Active" Value="A"></asp:ListItem>
                                    <asp:ListItem Text="Inactive" Value="I"></asp:ListItem>
                                    <asp:ListItem Text="New" Value="N"></asp:ListItem>
                                </asp:DropDownList>
                            </span>
                        </div>
                    </div>
                    <div class="ClearBoth">
                        <asp:ImageButton ID="btnClear" onmouseover="this.src='../../../../Themes/Images/buttons/button_clear_highlight.gif';"
                                onmouseout="this.src='../../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                                runat="server" CausesValidation="false" OnClick="BtnClearSearch_Click" AlternateText="Clear"/>
                        <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                            onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                            runat="server" OnClick="BtnSearch_Click" AlternateText="Search"/>
                    </div>
                </div>
            </asp:Panel>
        </div>
        <br />
        <h4 class="GridTitle">
            Product Reviews List
        </h4>
        <div>
            <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
                CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
                CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" PageSize="15"
                AllowSorting="True" EmptyDataText="No reviews available to display.">
                <Columns>
                    <asp:BoundField DataField="ReviewId" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText="Product Name" HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="Subject" HtmlEncode="false" HeaderText="Headline" HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText="Comments" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <span class="FieldStyle">Title:</span><span class="ValueStyle"><%# DataBinder.Eval(Container.DataItem,"Subject")%></span>
                            <div>
                                <uc1:spacer id="Spacer1" spacerheight="5" spacerwidth="3" runat="server"></uc1:spacer>
                            </div>                                                      
                            <div style="overflow:hidden"><%# DataBinder.Eval(Container.DataItem, "Comments").ToString()%></div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="CreateUser" HtmlEncode="false" HeaderText="User Name" HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="UserLocation" HtmlEncode="false" HeaderText="User Location" HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="Rating" HtmlEncode="false" HeaderText="Rating" HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "CreateDate","{0:MM/dd/yyyy}")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="CreateUser" HtmlEncode="false" HeaderText="User" HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText="Status" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "Status").ToString()%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ACTIONS" HeaderStyle-HorizontalAlign="Left">
                        <ItemStyle Wrap="true" Width="200px" />
                        <ItemTemplate>
                                <asp:LinkButton ID="btnEditReview" CommandName="Edit" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"ReviewId")%>'
                                    Text="Edit &raquo" runat="server" CssClass="LinkButton" />&nbsp;&nbsp;
                                <asp:LinkButton ID="btnChangeStatus" CommandName="Status" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"ReviewId")%>'
                                    Text="Change Status &raquo" runat="server" CssClass="LinkButton" />&nbsp;&nbsp;
                                <asp:LinkButton ID="btnDelete" CommandName="Delete" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"ReviewId")%>'
                                    Text="Delete &raquo" runat="server" CssClass="LinkButton"  />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
        </div>
    </div>
</asp:Content>
