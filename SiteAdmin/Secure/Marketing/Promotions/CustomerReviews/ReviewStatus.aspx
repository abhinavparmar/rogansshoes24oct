<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master"  ValidateRequest="false" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Marketing.Promotions.CustomerReviews.ReviewStatus" Codebehind="ReviewStatus.aspx.cs" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">    
    <div class="Form">
          <h1>Review Title - &nbsp;<asp:Label ID="lblReviewHeader" runat="server" /></h1>
          <ZNode:DemoMode ID="DemoMode1" runat="server" />        
          <div class="HintStyle">Select "Active" to post the review on the site. "New" or "Inactive" reviews will not be shown on the public site.</div><br />
          <div class="FieldStyle">
            <asp:DropDownList ID="ListReviewStatus" runat="server">            
                <asp:ListItem Text="Active" Value="A"></asp:ListItem>
                <asp:ListItem Text="Inactive" Value="I"></asp:ListItem>
                <asp:ListItem Selected="True" Text="New" Value="N"></asp:ListItem>
            </asp:DropDownList>
          </div>
          <div><ZNode:spacer id="Spacer1" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:spacer></div>
          <div class="ValueField">
               <asp:ImageButton ID="btnSubmit" runat="server" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                   OnClick="UpdateReviewStatus_Click" onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';"
                   onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';" AlternateText="Submit"/>
                        <asp:ImageButton ID="btnCancel" runat="server" CausesValidation="false" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                            OnClick="CancelStatus_Click" onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';"
                            onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';" AlternateText="Cancel"/>
          </div>
         <div><ZNode:spacer id="LongSpace" SpacerHeight="200" SpacerWidth="3" runat="server"></ZNode:spacer></div>
    </div>
</asp:Content>

