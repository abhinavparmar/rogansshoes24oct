using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Marketing.Promotions.CustomerReviews
{
    /// <summary>
    /// Represents the Site Admin - SiteAdmin.Secure.Marketing.Promotion.CustomerReviews.EditReview class
    /// </summary>
    public partial class EditReview : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int itemId;
        private string listPageLink = "~/SiteAdmin/Secure/Marketing/Promotions/CustomerReviews/Default.aspx";
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.itemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.itemId = 0;
            }

            if (!Page.IsPostBack)
            {
                this.BindEditData();
            }
        }

        #endregion

        #region General Events
        /// <summary>
        /// Update Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnUpdate_Click(object sender, EventArgs e)
        {
            ReviewAdmin reviewAdmin = new ReviewAdmin();
            Review review = reviewAdmin.GetByReviewID(this.itemId);

            if (review != null)
            {
                review.Subject = Server.HtmlEncode(Headline.Text.Trim());
                review.Pros = Server.HtmlEncode(Pros.Text.Trim());
                review.Cons = Server.HtmlEncode(Cons.Text.Trim());
                review.Comments = Server.HtmlEncode(Comments.Text.Trim());
                review.CreateUser = Server.HtmlEncode(txtCreateUser.Text);
                review.UserLocation = Server.HtmlEncode(txtUserLocation.Text);
                review.Rating = Convert.ToInt32(ddlRating.SelectedValue);
                if (ListReviewStatus.SelectedValue == "I")
                {
                    review.Status = "I";
                }
                else if (ListReviewStatus.SelectedValue == "A")
                {
                    review.Status = "A";
                }
                else
                {
                    review.Status = "N";
                }

                bool isUpdated = reviewAdmin.Update(review);

                if (isUpdated)
                {
                    string associateName = "Edit of Customer Review - " + review.Subject;
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, review.Subject);
                    Response.Redirect(this.listPageLink);
                }
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.listPageLink);
        }
        #endregion

        #region Bind Method
        /// <summary>
        /// Bind product review edit data.
        /// </summary>
        private void BindEditData()
        {
            ReviewAdmin reviewAdmin = new ReviewAdmin();
            Review review = reviewAdmin.DeepLoadByReviewID(this.itemId);

            if (review != null && review.ProductIDSource != null)
            {
                lblReviewHeader.Text = review.ProductIDSource.Name;
                lblProductName.Text = Server.HtmlDecode(review.ProductIDSource.Name);
                Headline.Text = Server.HtmlDecode(review.Subject);
                Pros.Text = Server.HtmlDecode(review.Pros);
                Cons.Text = Server.HtmlDecode(review.Cons);
                Comments.Text = Server.HtmlDecode(review.Comments);
                txtCreateUser.Text = Server.HtmlDecode(review.CreateUser);
                txtUserLocation.Text = Server.HtmlDecode(review.UserLocation);
                ddlRating.SelectedIndex = ddlRating.Items.IndexOf(ddlRating.Items.FindByValue(review.Rating.ToString()));
                ListReviewStatus.SelectedValue = review.Status;
                lblCreateDate.Text = review.CreateDate.ToString();
                txtUserEmail.Text = GetUserEmail();
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        #endregion

        #region Zeon Custom Code

        /// <summary>
        ///  Get User Review Email
        /// </summary>
        /// <returns>string</returns>
        private string GetUserEmail()
        {
            string email = string.Empty;
            if (this.itemId > 0)
            {
                ReviewExtnService reviewExtnSer = new ReviewExtnService();
                TList<ReviewExtn> reviewExtn = reviewExtnSer.GetByReviewID(this.itemId);
                if (reviewExtn != null && reviewExtn.Count > 0)
                {
                    email = !string.IsNullOrEmpty(reviewExtn[0].Email) ? reviewExtn[0].Email : string.Empty;
                }
            }
            return email;
        }

        #endregion
    }


}