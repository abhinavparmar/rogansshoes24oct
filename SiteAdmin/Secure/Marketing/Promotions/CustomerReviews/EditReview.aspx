<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True"
    ValidateRequest="false" Inherits="SiteAdmin.Secure.Marketing.Promotions.CustomerReviews.EditReview"
    CodeBehind="EditReview.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode"
    TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <ZNode:DemoMode ID="DemoMode1" runat="server"></ZNode:DemoMode>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>Edit Review -
                <asp:Label ID="lblReviewHeader" runat="server"></asp:Label></h1>
        </div>
        <div style="text-align: right">
            <asp:ImageButton ID="btnSubmitTop" runat="server" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                OnClick="BtnUpdate_Click" onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';"
                onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';" AlternateText="Submit" />
            <asp:ImageButton ID="btnCancelTop" runat="server" CausesValidation="false" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                OnClick="BtnCancel_Click" onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';"
                onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';" AlternateText="Cancel" />
        </div>
        <div class="FormView">
            <div>
                <ZNode:Spacer ID="Spacer1" SpacerHeight="25" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div class="FieldStyle">
                Review Status
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ListReviewStatus" runat="server">
                    <asp:ListItem Text="Active" Value="A"></asp:ListItem>
                    <asp:ListItem Text="Inactive" Value="I"></asp:ListItem>
                    <asp:ListItem Selected="True" Text="New" Value="N"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="FieldStyle">
                Product Name
            </div>
            <div class="ValueStyle">
                <div>
                    <asp:Label ID="lblProductName" runat="server"></asp:Label>
                </div>
            </div>
            <div class="FieldStyle">
                Headline<span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID='Headline' runat='server' MaxLength="100" Columns="50"></asp:TextBox>
                <div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Headline"
                        Display="Dynamic" ErrorMessage="* Enter a Headline" CssClass="Error" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div id='divPros' runat="server" visible="false">
                <div class="FieldStyle">
                    Pros
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID='Pros' runat='server' MaxLength="100" Columns="50"></asp:TextBox>
                </div>
            </div>
            <div id='divCons' runat="server" visible="false">
                <div class="FieldStyle">
                    Cons
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID='Cons' runat='server' MaxLength="100" Columns="50"></asp:TextBox>
                </div>
            </div>
            <div class="FieldStyle">
                Comments<span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID='Comments' runat='server' TextMode="MultiLine" Rows="20" Columns="50"></asp:TextBox>
                <div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Comments"
                        Display="Dynamic" ErrorMessage="* Enter a Comment" CssClass="Error" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="FieldStyle">
                Created User<span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID='txtCreateUser' runat='server' MaxLength="100" Columns="50"></asp:TextBox>
                <div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtCreateUser"
                        Display="Dynamic" ErrorMessage="* Enter a Create User Name." CssClass="Error"
                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div>
                <div class="FieldStyle">
                    User Location<span class="Asterix">*</span>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID='txtUserLocation' runat='server' MaxLength="100" Columns="50"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtUserLocation"
                            Display="Dynamic" ErrorMessage="* Enter a User Location." CssClass="Error" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="FieldStyle">
                    Rating<span class="Asterix">*</span>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlRating" runat="server">
                        <asp:ListItem Value="5" Text="5 - Excellent, Perfect"></asp:ListItem>
                        <asp:ListItem Value="4" Text="4 - That's Good Stuff"></asp:ListItem>
                        <asp:ListItem Value="3" Text="3 - Average, Ordinary"></asp:ListItem>
                        <asp:ListItem Value="2" Text="2 - Needs that Special Something"></asp:ListItem>
                        <asp:ListItem Value="1" Text="1 - Not Good"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="FieldStyle">
                    Created User Email
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID='txtUserEmail' runat='server' MaxLength="100" Columns="50" Enabled="false"></asp:TextBox>
                </div>
                <div class="FieldStyle">
                    Create Date
                </div>
                <div class="ValueStyle">
                    <asp:Label ID="lblCreateDate" runat="server"></asp:Label>
                    <div>
                    </div>
                </div>
                <div class="ClearBoth">
                    <ZNode:Spacer ID="Spacer2" SpacerHeight="25" SpacerWidth="3" runat="server"></ZNode:Spacer>
                </div>
                <div class="ClearBoth">
                    <asp:ImageButton ID="btnSubmit" runat="server" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                        OnClick="BtnUpdate_Click" onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';"
                        onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';" AlternateText="Submit" />
                    <asp:ImageButton ID="btnCancel" runat="server" CausesValidation="false" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                        OnClick="BtnCancel_Click" onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';"
                        onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';" AlternateText="Cancel" />
                </div>
                <div>
                    <ZNode:Spacer ID="LongSpace" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
                </div>
            </div>
        </div>
</asp:Content>
