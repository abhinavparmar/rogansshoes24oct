using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Marketing.Promotions.PromotionsAndCoupons
{
    /// <summary>
    /// Represents the SiteAdmin - SiteAdmin.Secure.Marketing.Promotion.PromotionsAndCoupons.Add class
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId = 0;
        #endregion


        #region Bind Methods
        /// <summary>
        /// Bind Edit mode fields
        /// </summary>
        public void BindEditData()
        {
            ZNode.Libraries.Admin.PromotionAdmin couponAdmin = new ZNode.Libraries.Admin.PromotionAdmin();
            ZNode.Libraries.DataAccess.Entities.Promotion promotion = couponAdmin.DeepLoadByPromotionId(this.ItemId);

            if (promotion != null)
            {
                // General Section
                PromotionName.Text = Server.HtmlDecode(promotion.Name);
                Description.Text = Server.HtmlDecode(promotion.Description);
                StartDate.Text = promotion.StartDate.ToShortDateString();
                EndDate.Text = promotion.EndDate.ToShortDateString();
                DisplayOrder.Text = promotion.DisplayOrder.ToString();
                if (!string.IsNullOrEmpty(promotion.PromoCode))
                {
                    PromoCode.Text = promotion.PromoCode.ToString();
                }

                // Discount             
                Discount.Text = promotion.Discount.ToString();
                DiscountType.SelectedValue = promotion.DiscountTypeIDSource.ClassName.ToString();
                this.ToggleDiscountValidator();
                ProductAdmin prodAdmin = new ProductAdmin();

                if (!string.IsNullOrEmpty(promotion.DiscountTypeIDSource.ClassName))
                {
                    if (promotion.ProfileID.HasValue)
                    {
                        ddlProfileTypes.SelectedValue = promotion.ProfileID.Value.ToString();
                    }

                    if (promotion.ProductID.HasValue)
                    {
                        txtReqProductId.Text = promotion.ProductID.Value.ToString();
                        txtRequiredProduct.Text = promotion.ProductIDSource.Name;
                    }

                    if (promotion.PromotionProductID.HasValue)
                    {
                        txtPromProductId.Text = promotion.PromotionProductID.Value.ToString();


                        txtPromoProduct.Text = Server.HtmlEncode(prodAdmin.GetProductName(promotion.PromotionProductID.Value));

                        ddlQuantity.SelectedValue = promotion.PromotionProductQty.GetValueOrDefault(1).ToString();
                    }

                    txtPromProductId.Text = promotion.PromotionProductID.GetValueOrDefault(0).ToString();
                    ddlQuantity.SelectedValue = promotion.PromotionProductQty.GetValueOrDefault(0).ToString();
                    ddlMinimumQty.SelectedValue = promotion.QuantityMinimum.GetValueOrDefault(1).ToString();

                    // Coupon Info
                    chkCouponInd.Checked = promotion.CouponInd;
                    chkCouponUrl.Checked = promotion.EnableCouponUrl.GetValueOrDefault();
                    CouponCode.Text = promotion.CouponCode;

                    if (chkCouponInd.Checked)
                    {
                        pnlCouponInfo.Visible = true;
                        pnlCouponInfo.Attributes.Clear();
                    }

                    if (chkCouponUrl.Checked)
                    {
                        pnlCouponurl.Visible = true;

                        if (CouponCode.Text.Length > 0)
                        {
                            lblcouponName.Text = "Add the following parameter to any store URL to automatically apply this coupon: <b>?coupon=" + CouponCode.Text + "</b>";
                        }
                        else
                        {
                            lblcouponName.Text = "Add the following parameter to any store URL to automatically apply this coupon: <b>?coupon=&lt;COUPON CODE&gt;</b>";
                        }
                    }

                    txtPromotionMessage.Text = Server.HtmlDecode(promotion.PromotionMessage);

                    if (promotion.DiscountTypeIDSource.ClassName.Equals("ZNodeCallForPricingPromotion", StringComparison.OrdinalIgnoreCase))
                    {
                        txtCallforMessaging.Text = Server.HtmlDecode(promotion.PromotionMessage);
                        txtCallforPriceProduct.Text = Server.HtmlEncode(prodAdmin.GetProductName(promotion.ProductID.GetValueOrDefault(0)));
                        txtCallforPriceProductId.Text = promotion.ProductID.GetValueOrDefault(0).ToString();
                        BindSKUs(promotion.ProductID.GetValueOrDefault(0));
                        ddlCallforPriceSKus.SelectedValue = promotion.PromotionProductID.GetValueOrDefault(0).ToString();
                    }

                    if (promotion.CouponQuantityAvailable.HasValue)
                    {
                        Quantity.Text = promotion.CouponQuantityAvailable.Value.ToString();
                    }

                    if (promotion.OrderMinimum.HasValue)
                    {
                        OrderMinimum.Text = promotion.OrderMinimum.Value.ToString("N2");
                    }

                    //Zeon Custom Code:: Start
                    if (promotion.PromotionExtnCollection != null && promotion.PromotionExtnCollection.Count > 0)
                    {
                        if (ddlPortals.Items.FindByValue(promotion.PromotionExtnCollection[0].PortalID.GetValueOrDefault(0).ToString()) != null)
                            ddlPortals.SelectedValue = promotion.PromotionExtnCollection[0].PortalID.GetValueOrDefault(0).ToString();

                        if (ddlCatalogs.Items.FindByValue(promotion.PromotionExtnCollection[0].CatalogID.GetValueOrDefault(0).ToString()) != null)
                            ddlCatalogs.SelectedValue = promotion.PromotionExtnCollection[0].CatalogID.GetValueOrDefault(0).ToString();

                        if (ddlCategories.Items.FindByValue(promotion.PromotionExtnCollection[0].CategoryID.GetValueOrDefault(0).ToString()) != null)
                            ddlCategories.SelectedValue = promotion.PromotionExtnCollection[0].CategoryID.GetValueOrDefault(0).ToString();

                        if (ddlBrands.Items.FindByValue(promotion.PromotionExtnCollection[0].ManufacturerID.GetValueOrDefault(0).ToString()) != null)
                            ddlBrands.SelectedValue = promotion.PromotionExtnCollection[0].ManufacturerID.GetValueOrDefault(0).ToString();
                    }


                    //Zeon Custom Code:: End


                    // Set page Title
                    lblTitle.Text += promotion.Name;
                }
            }
        }

        /// <summary>
        /// Bind List controls 
        /// </summary>
        public void BindData()
        {
            // Bind DiscountTypes
            ZNode.Libraries.Admin.PromotionAdmin Discountname = new ZNode.Libraries.Admin.PromotionAdmin();
            TList<DiscountType> dicountTypeList = Discountname.GetAllDiscountTypes();
            DiscountType.DataSource = dicountTypeList.FindAll(DiscountTypeColumn.ActiveInd, true);
            DiscountType.DataTextField = "Name";
            DiscountType.DataValueField = "ClassName";
            DiscountType.DataBind();

            ProfileAdmin profileAdmin = new ProfileAdmin();
            ddlProfileTypes.DataSource = profileAdmin.GetAll();
            ddlProfileTypes.DataTextField = "Name";
            ddlProfileTypes.DataValueField = "ProfileID";
            ddlProfileTypes.DataBind();

            ListItem li = new ListItem("All Profiles", "0");
            ddlProfileTypes.Items.Insert(0, li);
            ddlProfileTypes.SelectedValue = "0";

            this.BindStores();
            this.BindCatalogs();
            this.BindCategories();
            this.BindBrands();

            this.ToggleDiscountValidator();
        }

        /// <summary>
        /// Bind quantity drop down list
        /// </summary>
        public void BindQuantityList()
        {
            for (int i = 1; i < 15; i++)
            {
                ddlQuantity.Items.Add(i.ToString());
                ddlMinimumQty.Items.Add(i.ToString());
            }
        }
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get promotionid from query string
            if (Request.Params["ItemID"] != null)
            {
                this.ItemId = int.Parse(Request.Params["ItemID"].ToString());
            }

            if (!Page.IsPostBack)
            {
                this.BindQuantityList();
                this.BindData();

                discAmountValidator.Text = "Enter discount amount between " + 0.01.ToString("N") + "- 9999999";
                discPercentageValidator.Text = "Enter percentage discount between " + 0.01.ToString("N") + "- 100";

                if (this.ItemId > 0)
                {
                    // Edit Mode
                    lblTitle.Text = "Edit Promotion � ";
                    this.BindEditData();
                }
                else
                {
                    StartDate.Text = System.DateTime.Now.ToShortDateString();
                    EndDate.Text = System.DateTime.Now.AddDays(30).ToShortDateString();
                    lblTitle.Text = "Add a New Promotion";
                }
            }

            btnShowPopup.Attributes.Add("onclick", "var newwindow = window.open('searchproduct.aspx?sourceId=" + txtReqProductId.ClientID + "&source=" + txtRequiredProduct.ClientID + "','RequiredProduct','left=300, top=100, height=720, width= 650, status=n o, resizable= no, scrollbars= yes, toolbar= no,location= 0, menubar= no'); newwindow.focus(); return false;");

            btnCallforPriceShowPopup.Attributes.Add("onclick", "var newwindow = window.open('searchproduct.aspx?sourceId=" + txtCallforPriceProductId.ClientID + "&source=" + txtCallforPriceProduct.ClientID + "&refreshid=" + BtnSKU.ClientID + "','RequiredProduct','left=300, top=100, height=720, width= 650, status=n o, resizable= no, scrollbars= yes, toolbar= no,location= 0, menubar= no'); newwindow.focus(); return false;");

            btnSelectpromotionalProduct.Attributes.Add("onclick", "var newwindow = window.open('searchproduct.aspx?sourceId=" + txtPromProductId.ClientID + "&source=" + txtPromoProduct.ClientID + "','PromotionalProduct','left=300, top=100, height=720, width= 650, status=n o, resizable= no, scrollbars= yes, toolbar= no,location= 0, menubar= no');  return false;");
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ProductAdmin prodAdmin = new ProductAdmin();
            PromotionAdmin promotionAdmin = new ZNode.Libraries.Admin.PromotionAdmin();
            ZNode.Libraries.DataAccess.Entities.Promotion promotionEnity = null;
            PromotionExtn promotionExtEntity = null;

            if (this.ItemId > 0)
            {
                promotionEnity = promotionAdmin.GetByPromotionId(this.ItemId);

                //Zeon Customization Set Promotion Extension Details
                promotionExtEntity = promotionEnity.PromotionExtnCollection != null && promotionEnity.PromotionExtnCollection.Count > 0
                    ? promotionEnity.PromotionExtnCollection[0] : new PromotionExtn() { PromotionID = promotionEnity.PromotionID };

                promotionExtEntity = promotionAdmin.GetPromotionExtnByPromotionId(this.ItemId);
                if (promotionExtEntity == null)
                {
                    promotionExtEntity = new PromotionExtn()
                    {
                        PromotionID = promotionEnity.PromotionID
                    };
                }
                promotionExtEntity.PortalID = ddlPortals.SelectedValue == "0" ? (int?)null : int.Parse(ddlPortals.SelectedValue);

            }
            else
            {
                promotionEnity = new ZNode.Libraries.DataAccess.Entities.Promotion();
                promotionExtEntity = new PromotionExtn();
                promotionExtEntity.PortalID = ddlPortals.SelectedValue == "0" ? (int?)null : int.Parse(ddlPortals.SelectedValue);
            }

            // Set properties
            promotionEnity.ProductID = null;
            promotionEnity.SKUID = null;
            promotionEnity.AddOnValueID = null;
            promotionEnity.AccountID = null;

            // General Info section properties
            promotionEnity.Name = Server.HtmlEncode(PromotionName.Text.Trim());
            promotionEnity.Description = Server.HtmlEncode(Description.Text.Trim());
            promotionEnity.StartDate = Convert.ToDateTime(StartDate.Text.Trim());
            promotionEnity.EndDate = Convert.ToDateTime(EndDate.Text.Trim());
            promotionEnity.DisplayOrder = int.Parse(DisplayOrder.Text.Trim());
            promotionEnity.PromoCode = PromoCode.Text.Trim();




            // DiscountType
            if (DiscountType.SelectedIndex != -1)
            {
                promotionEnity.DiscountTypeID = promotionAdmin.GetDiscountTypeId(DiscountType.SelectedItem.Text, DiscountType.SelectedValue);
            }

            decimal decDiscountAmt = 0;
            decimal.TryParse(Discount.Text, out decDiscountAmt);
            decimal orderMin = 0;
            decimal.TryParse(OrderMinimum.Text, out orderMin);
            promotionEnity.OrderMinimum = orderMin;

            string className = DiscountType.SelectedValue;

            // Product Level Promotions
            if (className.Equals("ZNodePromotionPercentOffProduct", StringComparison.OrdinalIgnoreCase)
                        || className.Equals("ZNodePromotionAmountOffProduct", StringComparison.OrdinalIgnoreCase)
                        || className.Equals("ZNodePricingAmountOffProduct", StringComparison.OrdinalIgnoreCase)
                        || className.Equals("ZNodePricingPercentOffProduct", StringComparison.OrdinalIgnoreCase))
            {
                promotionEnity.QuantityMinimum = int.Parse(ddlMinimumQty.SelectedValue);

                if (txtReqProductId.Text != "0")
                {
                    promotionEnity.ProductID = int.Parse(txtReqProductId.Text.Trim());
                }
                else
                {
                    promotionEnity.ProductID = prodAdmin.GetProductIdByName(txtRequiredProduct.Text.Trim());
                }

            }
            else if (className.Equals("ZNodePromotionPercentXifYPurchased", StringComparison.OrdinalIgnoreCase)
                        || className.Equals("ZNodePromotionAmountOffXifYPurchased", StringComparison.OrdinalIgnoreCase))
            {
                promotionEnity.QuantityMinimum = int.Parse(ddlMinimumQty.SelectedValue);
                promotionEnity.PromotionProductQty = int.Parse(ddlQuantity.SelectedValue);

                if (txtReqProductId.Text != "0")
                {
                    promotionEnity.ProductID = int.Parse(txtReqProductId.Text.Trim());
                }
                else
                {
                    promotionEnity.ProductID = prodAdmin.GetProductIdByName(txtRequiredProduct.Text.Trim());
                }

                if (txtPromProductId.Text != "0")
                {
                    promotionEnity.PromotionProductID = int.Parse(txtPromProductId.Text.Trim());
                }
                else
                {
                    promotionEnity.PromotionProductID = prodAdmin.GetProductIdByName(txtPromoProduct.Text.Trim());
                }

            }
            else if (className.Equals("ZNodeCallForPricingPromotion", StringComparison.OrdinalIgnoreCase))
            {
                promotionEnity.ProductID = Convert.ToInt32(txtCallforPriceProductId.Text);
                promotionEnity.PromotionProductID = Convert.ToInt32(ddlCallforPriceSKus.SelectedValue);
                decDiscountAmt = 0;
            }
            else if (className.Equals("ZeonPromotionAmountOffCatalog", StringComparison.OrdinalIgnoreCase)
                || className.Equals("ZeonPromotionPercentOffCatalog", StringComparison.OrdinalIgnoreCase)) // Zeon Custom Code : Add Catalog ID and Portal ID for Catalog Promotion
            {

                promotionExtEntity.CatalogID = ddlCatalogs.SelectedValue == "0" ? (int?)null : int.Parse(ddlCatalogs.SelectedValue);
                promotionEnity.ProfileID = int.Parse(ddlProfileTypes.SelectedValue);
            }
            else if (className.Equals("ZeonPromotionAmountOffCategory", StringComparison.OrdinalIgnoreCase)
                || className.Equals("ZeonPromotionPercentOffCategory", StringComparison.OrdinalIgnoreCase)) // Zeon Custom Code : Add Category ID and Portal ID for category Promotion
            {
                promotionExtEntity.CategoryID = ddlCategories.SelectedValue == "0" ? (int?)null : int.Parse(ddlCategories.SelectedValue);
                promotionEnity.ProfileID = int.Parse(ddlProfileTypes.SelectedValue);
            }
            else if (className.Equals("ZeonPromotionAmountOffBrand", StringComparison.OrdinalIgnoreCase)
                || className.Equals("ZeonPromotionPercentOffBrand", StringComparison.OrdinalIgnoreCase)) // Zeon Custom Code : Add Category ID and Portal ID for category Promotion
            {
                promotionExtEntity.ManufacturerID = ddlBrands.SelectedValue == "0" ? (int?)null : int.Parse(ddlBrands.SelectedValue);
                promotionEnity.ProfileID = int.Parse(ddlProfileTypes.SelectedValue);
            }
            else
            {
            }

            // Set Discount field
            promotionEnity.Discount = decDiscountAmt;

            if (ddlProfileTypes.SelectedValue.ToString() == "0")
            {
                promotionEnity.ProfileID = null;
            }
            else
            {
                promotionEnity.ProfileID = int.Parse(ddlProfileTypes.SelectedValue);
            }

            // Coupon Info section           

            if (chkCouponInd.Checked && pnlCoupon.Visible)
            {
                promotionEnity.CouponInd = chkCouponInd.Checked;
                promotionEnity.EnableCouponUrl = chkCouponUrl.Checked;
                promotionEnity.CouponCode = CouponCode.Text;
                promotionEnity.CouponQuantityAvailable = int.Parse(Quantity.Text);
                promotionEnity.PromotionMessage = Server.HtmlEncode(txtPromotionMessage.Text);
            }
            else
            {
                promotionEnity.CouponInd = false;
                promotionEnity.EnableCouponUrl = false;
                promotionEnity.CouponCode = string.Empty;
                promotionEnity.PromotionMessage = string.Empty;
                promotionEnity.CouponQuantityAvailable = 0;
            }

            if (pnlAcushnetCallForPricingProduct.Visible)
            {
                promotionEnity.PromotionMessage = Server.HtmlEncode(txtCallforMessaging.Text);
            }

            if (promotionAdmin.IsPromotionOverlapped(promotionEnity))
            {
                lblError.Text = "Duplicate promotion already exists.";
                DiscountType.SelectedIndex = -1;
                RequiredFieldValidator8.Visible = false;
                return;
            }

            bool check = false;

            if (this.ItemId > 0)
            {
                check = promotionAdmin.UpdatePromotion(promotionEnity);
                if (check)
                {
                    //PRFT :Custom Code Update Custom Fields to  be null
                    if (!className.Equals("ZeonPromotionAmountOffCatalog") && !className.Equals("ZeonPromotionPercentOffCatalog"))
                    {
                        promotionExtEntity.CatalogID = null;
                    }
                    if (!className.Equals("ZeonPromotionAmountOffCategory") && !className.Equals("ZeonPromotionPercentOffCategory"))
                    {
                        promotionExtEntity.CategoryID = null;
                    }
                    if (!className.Equals("ZeonPromotionAmountOffBrand") && !className.Equals("ZeonPromotionPercentOffBrand"))
                    {
                        promotionExtEntity.ManufacturerID = null;
                    }
                    //PRFT :Custom Code Update Custom Fields to  be null
                    if (promotionExtEntity.PromotionExtnID > 0)
                    {
                        promotionAdmin.UpdatePromotionExtn(promotionExtEntity);
                    }
                    else
                    {
                        promotionAdmin.AddPromotionExtn(promotionExtEntity);
                    }

                }
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit of Promotion - " + PromotionName.Text.Trim(), PromotionName.Text.Trim());
            }
            else
            {
                check = promotionAdmin.AddPromotion(promotionEnity);
                if (check)
                {
                    promotionExtEntity.PromotionID = promotionEnity.PromotionID;
                    promotionAdmin.AddPromotionExtn(promotionExtEntity);
                }
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Creation of Promotion - " + PromotionName.Text.Trim(), PromotionName.Text.Trim());
            }

            if (check)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                lblError.Text = "An error occurred while updating. Please try again.";
            }
        }

        /// <summary>
        /// Cancel Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        /// <summary>
        /// Discount Type select index change event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DiscountType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ToggleDiscountValidator();
        }

        protected void BtnSKU_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCallforPriceProductId.Text))
            {
                int productId = 0;
                int.TryParse(txtCallforPriceProductId.Text, out productId);

                BindSKUs(productId);
            }
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Bind Product Method
        /// </summary>
        /// <param name="txtProdId">The value of ProductId</param>
        private void BindProduct(string txtProdId)
        {
            int prodId = 0;
            int.TryParse(txtProdId, out prodId);

            if (prodId > 0)
            {
                ProductAdmin productAdmin = new ProductAdmin();
                Product prod = productAdmin.GetByProductId(prodId);

                if (prod != null)
                {
                    txtRequiredProduct.Text = prod.Name;
                }
            }
        }

        private void BindSKUs(int productId)
        {
            if (productId > 0)
            {
                SKUAdmin skuAdmin = new SKUAdmin();
                TList<SKU> skus = skuAdmin.GetByProductID(productId);

                ddlCallforPriceSKus.DataSource = skus;
                ddlCallforPriceSKus.DataTextField = "SKU";
                ddlCallforPriceSKus.DataValueField = "SKUID";
                ddlCallforPriceSKus.DataBind();
            }
        }

        /// <summary>
        /// Enable/Disable Percentage/Amount validator on Discount field
        /// </summary>
        private void ToggleDiscountValidator()
        {

            string className = DiscountType.SelectedValue;
            chkCouponInd.Checked = false;
            pnlProducts.Visible = false;
            discAmountValidator.Enabled = false;
            discPercentageValidator.Enabled = false;
            pnlPromotionalProduct.Visible = false;
            lblDiscAmtMessage.Visible = false;
            pnlOrderMin.Visible = true;
            pnlCoupon.Visible = true;
            prodQuantity.Visible = true;
            pnlDiscountAmount.Visible = true;
            pnlAcushnetCallForPricingProduct.Visible = false;
            pnlPortal.Visible = true;
            pnlRequiredCatalog.Visible = false;
            pnlRequiredCategory.Visible = false;
            pnlRequiredBrand.Visible = false;

            if (className.Equals("ZNodePromotionPercentOffOrder", StringComparison.OrdinalIgnoreCase))
            {
                // Percentage Discount on Order
                discPercentageValidator.Enabled = true;
                discAmountValidator.Enabled = false;
            }
            else if (className.Equals("ZNodePromotionPercentOffProduct", StringComparison.OrdinalIgnoreCase))
            {
                // Product - Percentage Discount 
                pnlProducts.Visible = true;
                discAmountValidator.Enabled = false;
                discPercentageValidator.Enabled = true;

            }
            else if (className.Equals("ZNodePromotionPercentOffShipping", StringComparison.OrdinalIgnoreCase))
            {
                // Percentage Discount on Shipping
                discPercentageValidator.Enabled = true;
                discAmountValidator.Enabled = false;

            }
            else if (className.Equals("ZNodePromotionPercentXifYPurchased", StringComparison.OrdinalIgnoreCase))
            {
                // Percentage off x if y purchased
                lblDiscAmtMessage.Visible = true;
                pnlProducts.Visible = true;
                pnlPromotionalProduct.Visible = true;
                pnlOrderMin.Visible = false;


                discAmountValidator.Enabled = false;
                discPercentageValidator.Enabled = true;
            }
            else if (className.Equals("ZNodePromotionAmountOffOrder", StringComparison.OrdinalIgnoreCase))
            {
                // Amount off Order
                discPercentageValidator.Enabled = false;
                discAmountValidator.Enabled = true;

            }
            else if (className.Equals("ZNodePromotionAmountOffProduct", StringComparison.OrdinalIgnoreCase))
            {
                // Product - Amount Discount
                pnlProducts.Visible = true;
                discPercentageValidator.Enabled = false;
                discAmountValidator.Enabled = true;

            }
            else if (className.Equals("ZNodePromotionAmountOffShipping", StringComparison.OrdinalIgnoreCase))
            {
                // Amount on Shipping
                discPercentageValidator.Enabled = false;
                discAmountValidator.Enabled = true;

            }
            else if (className.Equals("ZNodePromotionAmountOffXifYPurchased", StringComparison.OrdinalIgnoreCase))
            {
                // Amount off (buy 1 Get 1 free)
                lblDiscAmtMessage.Visible = true;
                pnlProducts.Visible = true;
                pnlPromotionalProduct.Visible = true;
                pnlOrderMin.Visible = false;


                discAmountValidator.Enabled = true;
                discPercentageValidator.Enabled = false;
            }
            else if (className.Equals("ZNodePricingAmountOffProduct", StringComparison.OrdinalIgnoreCase))
            {
                // Amount discount on pricing amount
                pnlProducts.Visible = true;
                discAmountValidator.Enabled = true;
                pnlOrderMin.Visible = false;
                discPercentageValidator.Enabled = false;
                pnlCoupon.Visible = false;
                prodQuantity.Visible = false;

            }
            else if (className.Equals("ZNodePricingPercentOffProduct", StringComparison.OrdinalIgnoreCase))
            {
                // Percent of disocunt on Product pricing 
                pnlProducts.Visible = true;
                discAmountValidator.Enabled = false;
                pnlOrderMin.Visible = false;
                pnlCoupon.Visible = false;
                discPercentageValidator.Enabled = true;
                prodQuantity.Visible = false;

            }
            else if (className.Equals("ZNodePromotionProduct", StringComparison.OrdinalIgnoreCase))
            {
                // To overriding the product properties. For this make sure you did code for this in ZNodePromotionProduct class.
                pnlProducts.Visible = false;
                discAmountValidator.Enabled = false;
                pnlOrderMin.Visible = false;
                pnlCoupon.Visible = false;
                discPercentageValidator.Enabled = false;
                prodQuantity.Visible = false;

            }
            else if (className.Equals("ZNodeCallForPricingPromotion", StringComparison.OrdinalIgnoreCase))
            {
                pnlProducts.Visible = false;
                discAmountValidator.Enabled = false;
                pnlOrderMin.Visible = false;
                pnlCoupon.Visible = false;
                chkCouponInd.Checked = false;
                discPercentageValidator.Enabled = false;
                prodQuantity.Visible = false;
                pnlDiscountAmount.Visible = false;
                pnlAcushnetCallForPricingProduct.Visible = true;

            }
            else if (className.Equals("ZeonPromotionAmountOffCatalog", StringComparison.OrdinalIgnoreCase)
                || className.Equals("ZeonPromotionPercentOffCatalog", StringComparison.OrdinalIgnoreCase))
            {
                pnlPortal.Visible = true;
                pnlRequiredCatalog.Visible = true;
                pnlCoupon.Visible = false;
                pnlOrderMin.Visible = false;
                discAmountValidator.Enabled = true;
                discPercentageValidator.Enabled = false;


                if (className.Equals("ZeonPromotionPercentOffCatalog", StringComparison.OrdinalIgnoreCase))
                {
                    discAmountValidator.Enabled = false;
                    discPercentageValidator.Enabled = true;
                }
            }
            else if (className.Equals("ZeonPromotionAmountOffCategory", StringComparison.OrdinalIgnoreCase)
                || className.Equals("ZeonPromotionPercentOffCategory", StringComparison.OrdinalIgnoreCase))
            {
                pnlPortal.Visible = true;
                pnlCoupon.Visible = false;
                pnlOrderMin.Visible = false;
                pnlRequiredCategory.Visible = true;
                discAmountValidator.Enabled = true;
                discPercentageValidator.Enabled = false;


                if (className.Equals("ZeonPromotionPercentOffCategory", StringComparison.OrdinalIgnoreCase))
                {
                    discAmountValidator.Enabled = false;
                    discPercentageValidator.Enabled = true;
                }
            }
            else if (className.Equals("ZeonPromotionAmountOffBrand", StringComparison.OrdinalIgnoreCase)
            || className.Equals("ZeonPromotionPercentOffBrand", StringComparison.OrdinalIgnoreCase))
            {
                pnlPortal.Visible = true;
                pnlCoupon.Visible = false;
                pnlOrderMin.Visible = false;
                pnlRequiredBrand.Visible = true;
                discAmountValidator.Enabled = true;
                discPercentageValidator.Enabled = false;


                if (className.Equals("ZeonPromotionPercentOffBrand", StringComparison.OrdinalIgnoreCase))
                {
                    discAmountValidator.Enabled = false;
                    discPercentageValidator.Enabled = true;
                }
            }
            else
            {
                pnlOrderMin.Visible = true;
                lblDiscAmtMessage.Visible = true;
                pnlProducts.Visible = true;
                discAmountValidator.Enabled = false;
                discPercentageValidator.Enabled = false;
                pnlPromotionalProduct.Visible = true;
            }
        }
        #endregion

        #region[Zeon Custom Code]

        /// <summary>
        /// Bind Categories
        /// </summary>
        private void BindCategories()
        {
            var categoryAdmin = new CategoryAdmin();
            var categoryHelper = new CategoryHelper();
            var catalogAdmin = new CatalogAdmin();
            int catalogId = 0;

            if (ddlPortals.SelectedValue == "0")
            {
                var categories = categoryAdmin.GetAllCategories();
                categories.Sort("Name");
                ddlCategories.DataSource = categories;
            }
            else if (ddlPortals.SelectedValue != "0" || ItemId != 0)
            {
                catalogId = catalogAdmin.GetCatalogIDByPortalID(Convert.ToInt32(ddlPortals.SelectedValue));
                var categories = categoryHelper.GetCategoryByCatalogID(catalogId);
                ddlCategories.DataSource = categories;
            }
            ddlCategories.DataTextField = "Name";
            ddlCategories.DataValueField = "CategoryID";
            ddlCategories.DataBind();

            // Set "All Categories" to top of the list and decode item text
            ddlCategories.Items.Insert(0, new ListItem("Select Category", "0"));
            ddlCategories.SelectedIndex = 0;
            DecodeListItems(ddlCategories);
        }

        /// <summary>
        /// Bind Stores
        /// </summary>
        private void BindStores()
        {
            var portalAdmin = new PortalAdmin();
            var portals = portalAdmin.GetAllPortals();

            ddlPortals.DataSource = portals;
            ddlPortals.DataTextField = "StoreName";
            ddlPortals.DataValueField = "PortalID";
            ddlPortals.DataBind();

            // Add "All Stores" to top of the list and decode item text
            ddlPortals.Items.Insert(0, new ListItem("All Store", "0"));
            ddlPortals.SelectedValue = "0";

            DecodeListItems(ddlPortals);
        }

        /// <summary>
        /// Bind Catalogs
        /// </summary>
        private void BindCatalogs()
        {
            var catalogAdmin = new CatalogAdmin();
            var catalogHelper = new CatalogHelper();

            if (ddlPortals.SelectedValue != "0" || ItemId != 0)
            {
                var ds = catalogHelper.GetCatalogsByPortalId(Convert.ToInt32(ddlPortals.SelectedValue));
                var dr = ds.Tables[0].NewRow();
                ddlCatalogs.DataSource = ds.Tables[0];
            }
            else
            {
                var catalogs = catalogAdmin.GetAllCatalogs();
                catalogs.Sort("Name");
                ddlCatalogs.DataSource = catalogs;
            }

            ddlCatalogs.DataTextField = "Name";
            ddlCatalogs.DataValueField = "CatalogID";
            ddlCatalogs.DataBind();

            // Set "All Catalogs" to top of the list and decode item text 
            if (ddlPortals.SelectedValue == "0")
            {
                ddlCatalogs.Items.Insert(0, new ListItem("All Catalog", "0"));
            }

            ddlCatalogs.SelectedIndex = 0;
            DecodeListItems(ddlCatalogs);
        }

        /// <summary>
        /// Decode List Items
        /// </summary>
        /// <param name="dropDownList"></param>
        protected void DecodeListItems(DropDownList dropDownList)
        {
            foreach (ListItem item in dropDownList.Items)
            {
                item.Text = Server.HtmlDecode(item.Text);
            }
        }

        /// <summary>
        /// Bind Profiles
        /// </summary>
        private void BindProfiles()
        {
            var profileAdmin = new ProfileAdmin();

            if (ddlPortals.SelectedValue == "0")
            {
                var profiles = profileAdmin.GetAll();
                ddlProfileTypes.DataSource = profiles;
            }
            else if (ddlPortals.SelectedValue != "0" || ItemId != 0)
            {
                var profiles = profileAdmin.GetAssociatedProfilesByPortalID(Convert.ToInt32(ddlPortals.SelectedValue));
                ddlProfileTypes.DataSource = UserStoreAccess.CheckProfileAccess(profiles.Tables[0]);
            }

            ddlProfileTypes.DataTextField = "Name";
            ddlProfileTypes.DataValueField = "ProfileID";
            ddlProfileTypes.DataBind();

            // Add "All Profiles" to top of the list and decode item text  
            ddlProfileTypes.Items.Insert(0, new ListItem("All Profiles", "0"));
            DecodeListItems(ddlProfileTypes);
        }

        protected void ddlPortals_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProfiles();
            BindCatalogs();
            BindCategories();
        }

        /// <summary>
        /// Bind Brands
        /// </summary>
        private void BindBrands()
        {
            var mfgAdmin = new ManufacturerAdmin();
            var brands = mfgAdmin.GetAll();
            brands.Sort("Name");

            ddlBrands.DataSource = brands;
            ddlBrands.DataTextField = "Name";
            ddlBrands.DataValueField = "ManufacturerID";
            ddlBrands.DataBind();

            // Set "All Brands" to top of the list and decode item text
            ddlBrands.Items.Insert(0, new ListItem("All Brands", "0"));
            ddlBrands.SelectedIndex = 0;

            DecodeListItems(ddlBrands);
        }

        #endregion
    }
}