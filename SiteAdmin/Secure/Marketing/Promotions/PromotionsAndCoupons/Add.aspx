<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True"
    Inherits="SiteAdmin.Secure.Marketing.Promotions.PromotionsAndCoupons.Add" ValidateRequest="false"
    CodeBehind="Add.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode"
    TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <script type="text/javascript">
        var hiddenTextValue; //alias to the hidden field: hideValue
        var couponUrlText = "Add the following parameter to any store URL to automatically apply this coupon: <b>?coupon=";
        var cc = "&lt;COUPON CODE&gt;";

        function AutoCompleteSelected(source, eventArgs) {
            hiddenTextValue = $get("<%=txtReqProductId.ClientID %>");
            hiddenTextValue.value = eventArgs.get_value();
        }

        function AutoCompleteShowing(source, eventArgs) {
            hiddenTextValue = $get("<%=txtReqProductId.ClientID %>");
            hiddenTextValue.value = 0;
        }

        function AutoComplete_PromoProductsSelected(source, eventArgs) {
            hiddenTextValue = $get("<%=txtPromProductId.ClientID %>");
            hiddenTextValue.value = eventArgs.get_value();
        }

        function AutoComplete_PromoProductsShowing(source, eventArgs) {
            hiddenTextValue = $get("<%=txtPromProductId.ClientID %>");
            hiddenTextValue.value = 0;
        }

        function SetupCouponSection() {
            var chkCouponInd = document.getElementById("<%=chkCouponInd.ClientID %>");
            var pnlCouponInfo = document.getElementById("<%=pnlCouponInfo.ClientID %>");

            var regCouponCode = document.getElementById("<%=regCouponCode.ClientID %>");
            var rfvCouponCode = document.getElementById("<%=rfvCouponCode.ClientID %>");
            var rfvQuantity = document.getElementById("<%=rfvQuantity.ClientID %>");
            var rvQuantityRange = document.getElementById("<%=rvQuantityRange.ClientID %>");
            var state = false;
            if (chkCouponInd) {
                state = chkCouponInd.checked;

                ValidatorEnable(regCouponCode, state);
                ValidatorEnable(rfvCouponCode, state);
                ValidatorEnable(rfvQuantity, state);
                ValidatorEnable(rvQuantityRange, state);

                if (state == true) {
                    pnlCouponInfo.style.display = '';
                }
                else {
                    pnlCouponInfo.style.display = 'none';
                }
            }
        }

        function UpdateCouponUrl() {
            var chkCouponUrl = document.getElementById("<%=chkCouponUrl.ClientID %>");
            var txtCouponCode = document.getElementById("<%=CouponCode.ClientID %>");
            var lblCouponName = document.getElementById("<%=lblcouponName.ClientID %>");

            if (chkCouponUrl.checked) {

                if (txtCouponCode.value.length > 0) {
                    cc = txtCouponCode.value;
                }

                lblCouponName.innerHTML = couponUrlText + cc + "</br>";
            }
        }

        // Enable or disable the coupon code regular expression validator
        function ToggleCouponUrl() {
            var chkCouponUrl = document.getElementById("<%=chkCouponUrl.ClientID %>");
            var pnlCouponurl = document.getElementById("<%=pnlCouponurl.ClientID %>");
            var txtCouponCode = document.getElementById("<%=CouponCode.ClientID %>");
            var lblCouponName = document.getElementById("<%=lblcouponName.ClientID %>");

            if (chkCouponUrl.checked == true) {
                // Show the coupon url panel
                pnlCouponurl.style.display = '';

                if (txtCouponCode.value.length > 0) {
                    cc = txtCouponCode.value;
                }

                lblCouponName.innerHTML = couponUrlText + cc + "</br>";
            }
            else {
                // Hide the coupon url panel
                pnlCouponurl.style.display = 'none';
            }
        }

    </script>
    <div class="FormView">
        <div class="LeftFloat" style="width: auto; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
                <uc2:DemoMode ID="DemoMode1" runat="server" />
            </h1>
        </div>
        <div style="text-align: right">
            <asp:ImageButton ID="ImageButton1" OnClientClick="SetupCouponSection()" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
        <div class="ClearBoth">
        </div>
        <div>
            <uc1:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="Error">
            <asp:Label ID="lblError" runat="server" Visible="true"></asp:Label>
        </div>
        <h4 class="SubTitle">General Information</h4>
        <div class="FieldStyle">
            Promotion Name<span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="PromotionName" runat="server" Columns="25">
            </asp:TextBox><asp:RequiredFieldValidator CssClass="Error" ID="RequiredFieldValidator7"
                runat="server" ControlToValidate="PromotionName" Display="Dynamic" ErrorMessage="* Enter Promotion Name."
                SetFocusOnError="True"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            Promotion Description
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="Description" runat="server" Columns="25">
            </asp:TextBox>
        </div>
        <div class="FieldStyle">
            Begin date (MM/DD/YYYY)<span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="StartDate" Text="01/01/2007" runat="server" />
            <asp:ImageButton CausesValidation="false" ID="imgbtnStartDt" runat="server" ImageAlign="AbsMiddle"
                ImageUrl="~/SiteAdmin/Themes/images/SmallCalendar.gif" />
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="StartDate"
                CssClass="Error" Display="Dynamic" ErrorMessage="* Enter Valid Start Date" ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))">
            </asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator CssClass="Error" ID="RequiredFieldValidator4" runat="server"
                ControlToValidate="StartDate" Display="Dynamic" ErrorMessage="* Enter a valid start Date"
                SetFocusOnError="True">
            </asp:RequiredFieldValidator>
        </div>
        <ajaxToolKit:CalendarExtender ID="CalendarExtender1" Enabled="true" PopupButtonID="imgbtnStartDt"
            runat="server" TargetControlID="StartDate">
        </ajaxToolKit:CalendarExtender>
        <div class="FieldStyle">
            End date (MM/DD/YYYY)<span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="EndDate" Text="01/01/2007" runat="server" />
            <asp:ImageButton CausesValidation="false" ID="ImgbtnEndDt" runat="server" ImageAlign="AbsMiddle"
                ImageUrl="~/SiteAdmin/Themes/images/SmallCalendar.gif" />
            <div>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="EndDate"
                    CssClass="Error" Display="Dynamic" ErrorMessage="* Enter Valid Date in MM/DD/YYYY format"
                    ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))">
                </asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator CssClass="Error" ID="RequiredFieldValidator5" runat="server"
                    ControlToValidate="EndDate" Display="Dynamic" ErrorMessage="* Enter a valid Exp Date"
                    SetFocusOnError="True">
                </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareEndDate" runat="server" ControlToCompare="StartDate"
                    ControlToValidate="EndDate" Display="Dynamic" CssClass="Error" ErrorMessage="The Expiration date must be later than the Start Date"
                    Operator="GreaterThanEqual" Type="Date">
                </asp:CompareValidator>
            </div>
        </div>
        <ajaxToolKit:CalendarExtender ID="CalendarExtender2" Enabled="true" PopupButtonID="ImgbtnEndDt"
            runat="server" TargetControlID="EndDate">
        </ajaxToolKit:CalendarExtender>
        <asp:Panel ID="pnlPromoCode" runat="server">
            <div class="FieldStyle">
                Promotion Code<span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="PromoCode" runat="server">
                </asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator12" CssClass="Error" runat="server"
                    Display="Dynamic" ErrorMessage="*Enter Promo Code" ControlToValidate="PromoCode">
                </asp:RequiredFieldValidator>
            </div>
        </asp:Panel>
        <div class="FieldStyle">
            Display Order<span class="Asterix">*</span><br />
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="DisplayOrder" runat="server" MaxLength="9" Columns="9">
            </asp:TextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator11" CssClass="Error" runat="server"
                Display="Dynamic" ErrorMessage="*Enter Display Order" ControlToValidate="DisplayOrder">
            </asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator4" CssClass="Error" runat="server" ControlToValidate="DisplayOrder"
                Display="Dynamic" ErrorMessage="Enter whole number." MaximumValue="999999999"
                MinimumValue="1" Type="Integer">
            </asp:RangeValidator>
        </div>
        <h4 class="SubTitle">Discount</h4>
        <div class="FieldStyle">
            Discount Type
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="DiscountType" runat="server" OnSelectedIndexChanged="DiscountType_SelectedIndexChanged"
                AutoPostBack="True">
            </asp:DropDownList>
            <asp:HiddenField ID="hdnIsCustomPromotion" runat="server"  value="false"/>
        </div>
        <asp:UpdatePanel ID="updPnlProducts" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:TextBox CssClass="HiddenFieldStyle" Text="0" ID="txtPromProductId" runat="server"></asp:TextBox>
                <asp:TextBox CssClass="HiddenFieldStyle" Text="0" ID="txtReqProductId" runat="server"></asp:TextBox>
                <asp:Panel ID="pnlProducts" runat="server">
                    <div id="prodQuantity" runat="server">
                        <div class="FieldStyle">
                            Select the Minimum quantity<br />
                            <small>Minimum quantity that should be ordered for this promotion to be applicable.</small>
                        </div>
                        <div class="ValueStyle">
                            <asp:DropDownList Width="50" ID="ddlMinimumQty" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="pnlselectproduct" runat="server">
                        <div class="FieldStyle">
                            Product<br />
                            <small>Select the product to which promotion will be applied.</small>
                        </div>
                        <div class="ValueStyle">
                            <span class="LeftFloat">
                                <asp:TextBox ReadOnly="true" ID="txtRequiredProduct" AutoPostBack="true" AutoCompleteType="None"
                                    autocomplete="off" runat="server"></asp:TextBox></span>
                            <span>
                                <asp:Image CssClass="SearchIcon" ImageUrl="~/SiteAdmin/Themes/images/enlarge.gif"
                                    ToolTip="Search Product" AlternateText="Search" runat="server" ImageAlign="AbsMiddle"
                                    ID="btnShowPopup" /></span>
                        </div>
                        <ajaxToolKit:AutoCompleteExtender ID="AutoCompleteExtender3" runat="server" TargetControlID="txtRequiredProduct"
                            ServicePath="ZNodeCatalogServices.asmx" ServiceMethod="GetCompletionListWithContextAndValues"
                            UseContextKey="true" MinimumPrefixLength="2" EnableCaching="false" CompletionSetCount="100"
                            CompletionInterval="1" DelimiterCharacters=";, :" BehaviorID="autoCompleteBehavior3"
                            OnClientPopulating="AutoCompleteShowing" OnClientItemSelected="AutoCompleteSelected" />
                        <div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Select Product"
                                Display="Dynamic" ControlToValidate="txtRequiredProduct" CssClass="Error"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlPromotionalProduct" runat="server" Visible="false">
                    <div class="FieldStyle">
                        Select the quantity of Products
                        <br />
                        that will receive this Promotion
                    </div>
                    <div class="ValueStyle">
                        <asp:DropDownList Width="50" ID="ddlQuantity" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div>
                        <div class="FieldStyle">
                            Select the product
                            <br />
                            you would like to discount
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtPromoProduct" ReadOnly="true" AutoPostBack="true" AutoCompleteType="None"
                                autocomplete="off" runat="server"></asp:TextBox>
                            <asp:Image CssClass="SearchIcon" ImageUrl="~/SiteAdmin/Themes/images/enlarge.gif"
                                ToolTip="Search Product" AlternateText="Search" runat="server" ImageAlign="AbsMiddle"
                                ID="btnSelectpromotionalProduct" />
                        </div>
                        <ajaxToolKit:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtPromoProduct"
                            ServicePath="ZNodeCatalogServices.asmx" ServiceMethod="GetCompletionListWithContextAndValues"
                            UseContextKey="true" MinimumPrefixLength="2" EnableCaching="false" CompletionSetCount="100"
                            CompletionInterval="1" DelimiterCharacters=";, :" BehaviorID="autoCompleteBehavior4"
                            OnClientShowing="AutoComplete_PromoProductsShowing" OnClientItemSelected="AutoComplete_PromoProductsSelected" />
                        <div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Select promotional product."
                                Display="Dynamic" ControlToValidate="txtPromoProduct" CssClass="Error"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Panel ID="pnlDiscountAmount" runat="server">
            <div class="FieldStyle">
                Discount Amount<span class="Asterix">*</span><br />
                <small>
                    <asp:Label ID="lblDiscAmtMessage" runat="server" Visible="false">Enter the amount discounted from the Promotional Product.</asp:Label></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="Discount" runat="server" MaxLength="7" Columns="25">
                </asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Discount"
                    Display="Dynamic" ErrorMessage="* Enter Discount" SetFocusOnError="True" CssClass="Error">
                </asp:RequiredFieldValidator>
                <asp:RangeValidator ID="discAmountValidator" runat="server" ControlToValidate="Discount"
                    CssClass="Error" Enabled="false" Display="Dynamic" MaximumValue="9999999" MinimumValue="0.01"
                    CultureInvariantValues="true" Type="Currency">
                </asp:RangeValidator>
                <asp:RangeValidator ID="discPercentageValidator" Enabled="true" runat="server" ControlToValidate="Discount"
                    CssClass="Error" Display="Dynamic" MaximumValue="100" CultureInvariantValues="true"
                    MinimumValue="0.01" SetFocusOnError="True" Type="Double">
                </asp:RangeValidator>
            </div>
        </asp:Panel>
        <%--Zeon Custom HTML - Start--%>
        <asp:Panel ID="pnlPortal" runat="server">
            <div class="FieldStyle">
                <asp:Label ID="lblStoreName" runat="server" Text="Store"></asp:Label>
                <span class="Asterix">*</span><br />
                <small>Store to which this promotion should be applied.</small>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlPortals" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPortals_SelectedIndexChanged" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlPortals"
                    Display="Dynamic" ErrorMessage="* Select Store" SetFocusOnError="True" CssClass="Error" InitialValue="0">
                </asp:RequiredFieldValidator>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlRequiredCatalog" runat="server">
            <div class="FieldStyle">
                <asp:Label ID="lblCatalog" runat="server" Text="Required Catalog"></asp:Label>
                <span class="Asterix">*</span><br />
                <small>The catalog to which this promotion should be applied</small>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlCatalogs" runat="server" />
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlRequiredCategory" runat="server">
            <div class="FieldStyle">
                <asp:Label ID="lblRequiredCategory" runat="server" Text="Required Category"></asp:Label>
                <span class="Asterix">*</span><br />
                <small>
                    <asp:Literal ID="ltRequredCategoryDesc" runat="server" Text="The category to which this promotion should be applied."></asp:Literal></small>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlCategories" runat="server" />
                <asp:RequiredFieldValidator ID="rfvddlCategories" runat="server" ControlToValidate="ddlCategories"
                    Display="Dynamic" ErrorMessage="* Select Category" SetFocusOnError="True" CssClass="Error" InitialValue="0">
                </asp:RequiredFieldValidator>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlRequiredBrand" runat="server">
            <div class="FieldStyle">
                <asp:Label ID="lblRequiredBrand" runat="server" Text="Required Brand"></asp:Label>
                <span class="Asterix">*</span><br />
                <small>
                    <asp:Label ID="lblRequiredBrandHintText" runat="server" Text="The brand or manufacturer to which this promotion should be applied."></asp:Label>
                </small>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlBrands" runat="server" />
            </div>
        </asp:Panel>
        <%--Zeon Custom HTML - end--%>
        <div class="FieldStyle">
            Profile <span class="Asterix">*</span><br />
            <small>Select a profile to which this promotion should be applied.</small>
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlProfileTypes" runat="server">
            </asp:DropDownList>
        </div>
        <asp:Panel ID="pnlOrderMin" runat="server">
            <div class="FieldStyle">
                Minimum Order Amount<span class="Asterix">*</span><br />
                <small>Enter the minimum amount of the order that a customer should place before being eligible for this promotion.</small>
            </div>
            <div class="ValueStyle">
                <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;<asp:TextBox ID="OrderMinimum" runat="server" MaxLength="7" Columns="25"
                    Width="170px">0</asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="OrderMinimum"
                    Display="Dynamic" ErrorMessage="* Order Minimum is required" SetFocusOnError="True"
                    CssClass="Error">
                </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="OrderMinimum"
                    Display="Dynamic" ErrorMessage="* Enter a valid Order Minimum Amount" SetFocusOnError="true"
                    ValidationExpression="(^N/A$)|(^[-]?(\d+)(\.\d{0,3})?$)|(^[-]?(\d{1,3},(\d{3},)*\d{3}(\.\d{1,3})?|\d{1,3}(\.\d{1,3})?)$)"
                    CssClass="Error">
                </asp:RegularExpressionValidator>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlCoupon" runat="server" Visible="true">
            <h4 class="SubTitle">Coupon Information</h4>
            <div class="FieldStyle">
                <asp:CheckBox ID="chkCouponInd" onclick="SetupCouponSection()" runat="server" Text="Requires Coupon" />
            </div>

            <div class="ClearBoth">
                <br />
            </div>
            <asp:Panel ID="pnlCouponInfo" runat="server" Style="display: none;">
                <div class="FieldStyle">
                    Enter Coupon Code<span class="Asterix">*</span>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="CouponCode" onkeyup="UpdateCouponUrl()" runat="server" Columns="25">
                    </asp:TextBox>
                    <asp:RegularExpressionValidator ID="regCouponCode" Enabled="false" runat="server"
                        ControlToValidate="CouponCode" CssClass="Error" Display="Dynamic" ErrorMessage="Enter a valid Coupon Code"
                        SetFocusOnError="True" ValidationExpression="([A-Za-z0-9-_]+)">
                    </asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfvCouponCode" runat="server" ControlToValidate="CouponCode"
                        Display="Dynamic" ErrorMessage="* Enter a Coupon Code" SetFocusOnError="True"
                        CssClass="Error">
                    </asp:RequiredFieldValidator>
                </div>
                <div class="FieldStyle">
                    Promotion Message<br />
                    <small>Enter a message to display when a coupon is applied in the shopping cart.</small>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtPromotionMessage" runat="server" Columns="25">
                    </asp:TextBox>
                </div>
                <div class="FieldStyle">
                    Enter Available Quantity<span class="Asterix">*</span>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="Quantity" runat="server" MaxLength="4" Columns="25" Width="173px">99</asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvQuantity" runat="server" ControlToValidate="Quantity"
                        Display="Dynamic" ErrorMessage="* Enter Quantity" SetFocusOnError="True" CssClass="Error">
                    </asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvQuantityRange" runat="server" ControlToValidate="Quantity"
                        Display="Dynamic" ErrorMessage="* Enter a valid quantity. Enter a number between 0-9999"
                        MaximumValue="9999" MinimumValue="0" Type="Integer" CssClass="Error">
                    </asp:RangeValidator>
                </div>
                <div class="FieldStyle">
                </div>
                <div class="ValueStyleText">
                    <asp:CheckBox ID="chkCouponUrl" onclick="ToggleCouponUrl()" runat="server" Text="Enable Coupon URL" />
                </div>
                <asp:Panel ID="pnlCouponurl" runat="server">
                    <div class="FieldStyle">
                    </div>
                    <div class="ValueStyle">
                        <asp:Label ID="lblcouponName" runat="server"></asp:Label>
                    </div>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlAcushnetCallForPricingProduct" Visible="false">
            <div class="FieldStyle">
                Select the Product<br />
                <small>this product will show Call for message entered below.</small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtCallforPriceProduct" AutoPostBack="true"
                    runat="server">
                </asp:TextBox>
                <asp:Image CssClass="SearchIcon" ImageUrl="~/SiteAdmin/Themes/images/enlarge.gif"
                    ToolTip="Search Product" AlternateText="Search" runat="server" ImageAlign="AbsMiddle"
                    ID="btnCallforPriceShowPopup" />
            </div>
            <asp:TextBox CssClass="HiddenFieldStyle" Text="0" ID="txtCallforPriceProductId" runat="server">
            </asp:TextBox>
            <ajaxToolKit:AutoCompleteExtender ID="CallForPriceAutoCompleteExtender" runat="server"
                TargetControlID="txtCallforPriceProduct" ServicePath="ZNodeCatalogServices.asmx"
                ServiceMethod="GetCompletionListWithContextAndValues" UseContextKey="true" MinimumPrefixLength="2"
                EnableCaching="false" CompletionSetCount="100" CompletionInterval="1" DelimiterCharacters=";, :"
                BehaviorID="autoCompleteBehavior3" OnClientPopulating="AutoCompleteShowing" OnClientItemSelected="AutoCompleteSelected" />
            <div>
                <asp:RequiredFieldValidator ID="CallForPriceRequiredFieldValidator" runat="server"
                    ErrorMessage="Select Product" Display="Dynamic" ControlToValidate="txtCallforPriceProduct"
                    CssClass="Error">
                </asp:RequiredFieldValidator>
            </div>
            <div class="FieldStyle">
                Product SKUs<br />
                <small>select SKU for a call for price message to customer.</small>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList runat="server" ID="ddlCallforPriceSKus">
                </asp:DropDownList>
            </div>
            <div class="FieldStyle">
                Call for Price Message<br />
                <small>Enter a call for price message to customer.</small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtCallforMessaging" runat="server" Columns="25">
                </asp:TextBox>
            </div>
            <asp:Button ID="BtnSKU" runat="server" OnClick="BtnSKU_Click" CausesValidation="False" Style="display: none;" />
        </asp:Panel>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>

        <div class="ClearBoth">
            <br />
        </div>
        <div>
            <asp:ImageButton ID="btnSubmitBottom" OnClientClick="SetupCouponSection()" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
    <!--During Update Process -->
    <asp:UpdateProgress ID="UpdateProgressMozilla" runat="server" DisplayAfter="0" DynamicLayout="true">
        <ProgressTemplate>
            <asp:Panel ID="Panel1" CssClass="overlay" runat="server">
                <asp:Panel ID="Panel2" CssClass="loader" runat="server">
                    Loading...<img id="Img1" align="absmiddle" src="~/SiteAdmin/Themes/images/loading.gif"
                        runat="server" />
                </asp:Panel>
            </asp:Panel>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
