using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace SiteAdmin.Secure.Marketing.Promotions.PromotionsAndCoupons

{
    /// <summary>
    /// Represents the SiteAdmin - SiteAdmin.Secure.Marketing.Promotion.PromotionsAndCoupons.Default class
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Protected Member Variables
        private static bool IsSearchEnabled = false;
        private string CatalogImagePath = string.Empty;
        private string ListPage = "~/SiteAdmin/Secure/Marketing/Promotions/PromotionsAndCoupons/Default.aspx";
        private string AddLink = "~/SiteAdmin/Secure/Marketing/Promotions/PromotionsAndCoupons/Add.aspx";
        private string DeleteLink = "~/SiteAdmin/Secure/Marketing/Promotions/PromotionsAndCoupons/Delete.aspx?ItemID=";
       
        #endregion

        #region Page load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                IsSearchEnabled = false;
                this.BindDiscountTypeList();
                this.BindGridData();
            }
        }
        #endregion

        #region General Events

        /// <summary>
        /// Add Promtion Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddCoupon_Click(object sender, EventArgs e)
        {
            // Redirect to Add promotion Page    
            Response.Redirect(this.AddLink);
        }

        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            IsSearchEnabled = true;
            ViewState.Remove("PromotionsList");
            this.SearchPromotions();
        }

        /// <summary>
        /// Clear Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClearSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ListPage);
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            if (IsSearchEnabled)
            {
                this.SearchPromotions();
            }
            else
            {
                this.BindGridData();
            }
        }

        /// <summary>
        /// Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                int index = Convert.ToInt32(e.CommandArgument);

                // Get the values from the appropriate
                // cell in the GridView control.
                GridViewRow selectedRow = uxGrid.Rows[index];

                TableCell Idcell = selectedRow.Cells[0];
                string Id = Idcell.Text;

                if (e.CommandName == "Edit")
                {
                    this.AddLink = this.AddLink + "?ItemID=" + Id;
                    Response.Redirect(this.AddLink);
                }
                else if (e.CommandName == "Delete")
                {
                    Response.Redirect(this.DeleteLink + Id);
                }
            }
        }

        #endregion

        #region Helper Methods
        /// <summary>
        /// Discount Type Name
        /// </summary>
        /// <param name="discountTypeID">Discount Type ID</param>
        /// <returns>Returns the Discount Type Name</returns>
        protected string DiscountTypeName(object discountTypeID)
        {
            if (discountTypeID != null)
            {
                int discTypeID = int.Parse(discountTypeID.ToString());

                PromotionAdmin promotionAdmin = new PromotionAdmin();
                DiscountType entity = promotionAdmin.GetByDiscountTypeID(discTypeID);

                if (entity != null)
                {
                    return entity.Name;
                }
            }
            
            return string.Empty;
        }

        protected string GetProfileName(object ProfileId)
        {
            if (ProfileId != null)
            {
                if (!String.IsNullOrEmpty(ProfileId.ToString()))
                {
                    ProfileAdmin profileAdmin = new ProfileAdmin();
                    string ProfileName = profileAdmin.GetProfileNameByProfileID(int.Parse(ProfileId.ToString()));
                    return ProfileName;
                }
            }

            return "All Profiles";
        }
        #endregion

        #region Bind Methods

        /// <summary>
        /// Bind Grid Data
        /// </summary>
        private void BindGridData()
        {
            ZNode.Libraries.Admin.PromotionAdmin promotions = new ZNode.Libraries.Admin.PromotionAdmin();

            TList<ZNode.Libraries.DataAccess.Entities.Promotion> promolist = promotions.GetAllPromotions();
            DataSet ds = promolist.ToDataSet(false);
            DataView dataView = UserStoreAccess.CheckProfileAccess(ds.Tables[0], true).DefaultView;
            uxGrid.DataSource = dataView;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Bind Discount type dropdownlist
        /// </summary>
        private void BindDiscountTypeList()
        {
            PromotionAdmin AdminAccess = new PromotionAdmin();
            ddlDiscountTypes.DataSource = AdminAccess.GetAllDiscountTypes();
            ddlDiscountTypes.DataTextField = "Name";
            ddlDiscountTypes.DataValueField = "DiscountTypeId";
            ddlDiscountTypes.DataBind();

            ListItem li = new ListItem("All", "0");
            ddlDiscountTypes.Items.Insert(0, li);
        }

        /// <summary>
        /// Search Promotion
        /// </summary>
        private void SearchPromotions()
        {
            DataView dv = new DataView();

            if (ViewState["PromotionsList"] != null)
            {
                dv = new DataView(ViewState["PromotionsList"] as DataTable);
            }
            else
            {
                ZNode.Libraries.Admin.PromotionAdmin couponbind = new ZNode.Libraries.Admin.PromotionAdmin();
                DataSet ds = couponbind.GetPromotions();
                string filterQuery = string.Empty;

                dv = new DataView(ds.Tables[0]);
                dv.Sort = "displayorder asc";
              
                if (ddlDiscountTypes.SelectedValue != "0")
                {
                    filterQuery = "DiscountTypeId = " + ddlDiscountTypes.SelectedValue + " and  ";
                }
                
                if (txtStartDate.Text.Trim().Length > 0 && txtEndDate.Text.Trim().Length == 0)
                {
                    filterQuery = "StartDate >= '" + txtStartDate.Text.Trim() + " 00:00" + "' and  ";
                }
                else if (txtEndDate.Text.Trim().Length > 0 && txtStartDate.Text.Trim().Length == 0)
                {
                    filterQuery = "EndDate <='" + txtEndDate.Text.Trim() + " 23:59" + "' and ";
                }
                else if (txtStartDate.Text.Trim().Length > 0 && txtEndDate.Text.Trim().Length > 0)
                {
                    filterQuery = "StartDate >= '" + txtStartDate.Text.Trim() + " 00:00" + "' and  EndDate <='" + txtEndDate.Text.Trim() + " 23:59" + "' and ";
                }

                if (txtName.Text.Trim().Length > 0)
                {
                    filterQuery += "Name like '%" + Server.HtmlEncode(txtName.Text.Trim()) + "%' and ";
                }

                if (txtAmount.Text.Trim().Length > 0)
                {
                    filterQuery += "Discount >= " + txtAmount.Text.Trim() + " and ";
                }

                if (CouponCode.Text.Trim().Length > 0)
                {
                    filterQuery += "CouponCode like '%" + CouponCode.Text.Trim() + "%' and ";
                }

                // If filter query has conditition, if any
                if (filterQuery.Length > 0)
                {
                    // Apply filter
                    dv.RowFilter = filterQuery + "description like '%'";
                }

                ViewState.Add("PromotionsList", dv.ToTable());

                uxGrid.DataSource = dv;
                uxGrid.DataBind();
            }
        }
        #endregion
    }
}