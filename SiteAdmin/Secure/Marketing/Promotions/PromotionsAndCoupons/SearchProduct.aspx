<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/popup.master"  ValidateRequest="false" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Marketing.Promotions.PromotionsAndCoupons.SearchProduct" Codebehind="SearchProduct.aspx.cs" %>
<%@ Register Src="ProductSearch.ascx" TagName="ProductSearch" TagPrefix="ZNode" %>


<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" Runat="Server">
	<ZNode:ProductSearch ID="uxProductSearch" runat="server" />
</asp:Content>
