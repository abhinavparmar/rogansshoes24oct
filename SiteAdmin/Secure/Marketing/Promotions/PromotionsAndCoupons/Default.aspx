<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True"
    Inherits="SiteAdmin.Secure.Marketing.Promotions.PromotionsAndCoupons.Default" ValidateRequest="false"
    CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>Promotions and Coupons</h1>
        </div>
        <div class="ButtonStyle" style="text-transform: none;">
            <zn:LinkButton ID="btnAddCoupon" runat="server"
                ButtonType="Button" OnClick="BtnAddCoupon_Click" Text="Add a New Promotion"
                ButtonPriority="Primary" />
        </div>
        <div class="ClearBoth" align="left">
            <p>
                Manage promotional campaigns and discount coupons.
            </p>
            <br />
        </div>
        <h4 class="SubTitle">Search Promotions</h4>
        <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">Name</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtName" runat="server" ValidationGroup="grpSearch"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">Amount</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtAmount" runat="server" MaxLength="7" ValidationGroup="grpSearch"></asp:TextBox>
                            <br />
                            <asp:RangeValidator ID="discAmountValidator" runat="server" ControlToValidate="txtAmount"
                                CssClass="Error" Display="Dynamic" MaximumValue="9999999" MinimumValue="0.01"
                                ErrorMessage="Enter valid Amount" CultureInvariantValues="true" ValidationGroup="grpSearch"
                                Type="Currency"></asp:RangeValidator><br />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtAmount"
                                ValidationGroup="grpSearch" Display="Dynamic" ErrorMessage="Enter only numeric value."
                                SetFocusOnError="true" ValidationExpression="(^N/A$)|(^[-]?(\d+)(\.\d{0,3})?$)|(^[-]?(\d{1,3},(\d{3},)*\d{3}(\.\d{1,3})?|\d{1,3}(\.\d{1,3})?)$)"
                                CssClass="Error"></asp:RegularExpressionValidator>
                        </span>
                    </div>
                </div>
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">Coupon Code</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="CouponCode" runat="server" ValidationGroup="grpSearch"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">Discount Type</span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlDiscountTypes" runat="server" />
                        </span>
                    </div>
                </div>
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">Begin Date</span>
                        <div>
                        </div>
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtStartDate" runat="server" ValidationGroup="grpSearch"></asp:TextBox>
                            <asp:ImageButton ID="imgbtnStartDt" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/SiteAdmin/Themes/images/SmallCalendar.gif" />
                            <ajaxToolKit:CalendarExtender ID="CalendarExtender1" Enabled="true" PopupButtonID="imgbtnStartDt"
                                runat="server" TargetControlID="txtStartDate">
                            </ajaxToolKit:CalendarExtender>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtStartDate"
                                CssClass="Error" Display="Dynamic" ErrorMessage="* Enter Valid Start Date in MM/DD/YYYY format"
                                ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"
                                ValidationGroup="grpSearch"></asp:RegularExpressionValidator>
                        </span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">End Date</span>
                        <div>
                        </div>
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtEndDate" runat="server" ValidationGroup="grpSearch"></asp:TextBox>
                            <asp:ImageButton ID="ImgbtnEndDt" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/SiteAdmin/Themes/images/SmallCalendar.gif" />
                            <ajaxToolKit:CalendarExtender ID="CalendarExtender2" Enabled="true" PopupButtonID="ImgbtnEndDt"
                                runat="server" TargetControlID="txtEndDate">
                            </ajaxToolKit:CalendarExtender>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEndDate"
                                CssClass="Error" Display="Dynamic" ErrorMessage="* Enter Valid End Date in MM/DD/YYYY format"
                                ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"
                                ValidationGroup="grpSearch"></asp:RegularExpressionValidator>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtStartDate"
                                ControlToValidate="txtEndDate" CssClass="Error" ErrorMessage="End date should be greater than Begin date"
                                ValidationGroup="grpSearch" SetFocusOnError="True" Display="Dynamic" Operator="GreaterThanEqual"
                                Type="Date"></asp:CompareValidator>
                        </span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <asp:ImageButton ID="btnClearSearch" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_clear_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                        runat="server" AlternateText="Clear" OnClick="BtnClearSearch_Click" />
                    <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                        runat="server" AlternateText="Search" OnClick="BtnSearch_Click" ValidationGroup="grpSearch" />
                </div>
            </div>
        </asp:Panel>
        <br />
        <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
        <h4 class="GridTitle">Promotions List
        </h4>
        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
            CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" PageSize="25"
            AllowSorting="True" EmptyDataText="No promotions available to display.">
            <Columns>
                <asp:BoundField DataField="PromotionID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <a href='add.aspx?ItemID=<%# DataBinder.Eval(Container.DataItem, "PromotionID").ToString()%>'>
                            <%# DataBinder.Eval(Container.DataItem, "Name").ToString()%></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Profile" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <div>
                            <%# GetProfileName((DataBinder.Eval(Container.DataItem, "ProfileID")))%>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Amount" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <div align="left">
                            <%# (DataBinder.Eval(Container.DataItem, "Discount"))%>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Discount Type Name" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# DiscountTypeName(DataBinder.Eval(Container.DataItem, "DiscountTypeID"))%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Display Order" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "DisplayOrder")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="StartDate" DataFormatString="{0:MM/dd/yyyy}" HeaderText="Start date"
                    HtmlEncode="False" HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="EndDate" DataFormatString="{0:MM/dd/yyyy}" HeaderText="End Date"
                    HtmlEncode="False" HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="CouponCode" HeaderText="Coupon Code" HeaderStyle-HorizontalAlign="Left" />
                <asp:ButtonField CommandName="Edit" Text="Edit &raquo" ButtonType="Link">
                    <ControlStyle CssClass="actionlink" />
                </asp:ButtonField>
                <asp:ButtonField CommandName="Delete" Text="Delete &raquo" ButtonType="Link">
                    <ControlStyle CssClass="actionlink" />
                </asp:ButtonField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        </div>
    </div>
</asp:Content>
