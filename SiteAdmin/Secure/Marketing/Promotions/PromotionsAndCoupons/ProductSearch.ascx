<%@ Control Language="C#" AutoEventWireup="True"
    Inherits="SiteAdmin.Secure.Marketing.Promotions.PromotionsAndCoupons.ProductSearch" Codebehind="ProductSearch.ascx.cs" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<div class="Form">
    <asp:UpdatePanel ID="UpdatePnlSearch" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <h1>
                Search Products</h1>
            <asp:Panel ID="pnlSearch" DefaultButton="btnSearch" runat="server">
                <div>
                    <ZNode:Spacer ID="Spacer2" runat="server" SpacerWidth="5" SpacerHeight="10" />
                </div>
                <div class="FormView">
               
                    <div>
                        <div>
                            <div class="FieldStyle">
                                Keyword</div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtKeyword" runat="server"></asp:TextBox></div>
                        </div>
                       <div>
                            <div class="FieldStyle">
                                Brand</div>
                            <div class="ValueStyle">
                                <asp:DropDownList ID="dmanufacturer" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div>
                       
                        <div>
                            <div class="FieldStyle">
                                Product Category</div>
                            <div class="ValueStyle">
                                <asp:DropDownList ID="dproductcategory" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="ClearBoth"></div>
                    <div>
                        
                            <div class="ValueStyle">
                               <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_clear_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                        runat="server" AlternateText="Clear" OnClick="BtnClear_Click" />
                                <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                        runat="server" AlternateText="Search" OnClick="BtnSearch_Click" />
                            </div>
                        
                     </div>
                
                </div>
            <div class="ClearBoth"></div>
            </asp:Panel>
            <h4>
                Product List</h4>
            <div class="HintStyle">
                Click on the "Name" link in the first column to select a product.</div>
            <div>
                <ZNode:Spacer ID="Spacer1" runat="server" SpacerWidth="5" SpacerHeight="5" />
            </div>
            <asp:GridView ID="uxGrid" CellPadding="0" CssClass="Grid" runat="server" AutoGenerateColumns="False"
                AllowPaging="True" OnPageIndexChanging="UxGrid_PageIndexChanging" Width="100%" GridLines="None"
                EmptyDataText="No Products exist in the database.">
                <Columns>
                    <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <a href="javascript:Close('<%# Eval("ProductID") %>','<%# GetName(Eval("Name")) %>')">
                                <%# DataBinder.Eval(Container.DataItem,"Name") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Product Number" DataField="ProductNum" HeaderStyle-HorizontalAlign="Left" />
                     <asp:TemplateField HeaderText="Image" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img alt="" id="img1" src='<%# (new ZNode.Libraries.ECommerce.Utilities.ZNodeImage()).GetImageHttpPathThumbnail(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>'
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Is Active" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img alt="" id="imgActive" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EditRowStyle CssClass="EditRowStyle" />
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <SelectedRowStyle CssClass="SelectedRowStyle" />
                <PagerStyle CssClass="PagerStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div>
        <ZNode:Spacer ID="uxSpacer" runat="server" SpacerWidth="5" SpacerHeight="10" />
    </div>
    <div align="right">
          <asp:ImageButton ID="btnClose" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
              OnClientClick="javascript:window.close();" onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Submit" />
     </div>
</div>
