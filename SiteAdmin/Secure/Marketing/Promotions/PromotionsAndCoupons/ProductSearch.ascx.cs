using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Marketing.Promotions.PromotionsAndCoupons

{
    /// <summary>
    /// Represents the Site Admin - SiteAdmin.Secure.Marketing.Promotion.PromotionsAndCoupons.ProductSearch  class
    /// </summary>
    public partial class ProductSearch : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private string _Name = string.Empty;
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind data to the drop down list
        /// </summary>
        public void BindDropDownData()
        {
            // Add the manufacturers to the drop-down list
            ManufacturerAdmin manuadmin = new ManufacturerAdmin();
            dmanufacturer.DataSource = manuadmin.GetAll();
            dmanufacturer.DataTextField = "Name";
            dmanufacturer.DataValueField = "ManufacturerID";
            dmanufacturer.DataBind();
            ListItem item = new ListItem("ALL", "0");
            dmanufacturer.Items.Insert(0, item);

            // Making ALL as the default value of the drop-down list box
            dmanufacturer.SelectedIndex = 0;

            // Add Product categories to the drop-down list
            CategoryAdmin categoryadmin = new CategoryAdmin();
            dproductcategory.DataSource = categoryadmin.GetAllCategories();
            dproductcategory.DataTextField = "Name";
            dproductcategory.DataValueField = "Categoryid";
            dproductcategory.DataBind();
            ListItem item2 = new ListItem("ALL", "0");
            dproductcategory.Items.Insert(0, item2);
            dproductcategory.SelectedIndex = 0;
        }
        #endregion

        #region Page Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindDropDownData();
                this.Bind();
            }

            string script = @"<script>function Close(var1, var2){";

            if (Request.Params["sourceId"] != null && Request.Params["source"] != null)
            {
                script += "window.opener.document.getElementById('" + Request.Params["sourceId"].ToString() + "').value = var1;";
                script += "window.opener.document.getElementById('" + Request.Params["source"].ToString() + "').value = var2.replace(\":\",\"'\");";
                if (Request.Params["refreshid"] != null)
                {
                    script += "window.opener.document.getElementById('" + Request.Params["refreshid"].ToString() + "').click();";
                }
            }

            script += "window.close();}</script>";

            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "ClosePopup", script);
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.Bind();
        }
        #endregion

        #region General Events
        /// <summary>
        /// Bind Method
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.Bind();
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            txtKeyword.Text = string.Empty;
            dproductcategory.SelectedIndex = 0;
            dmanufacturer.SelectedIndex = 0;
            this.Bind();
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Get Name Method
        /// </summary>
        /// <param name="name">The value of name</param>
        /// <returns>Returns the Name</returns>
        protected string GetName(object name)
        {
            string val = string.Empty;

            if (name != null)
            {
                val = name.ToString().Replace("'", ":");
            }

            return val;
        }
        
        /// <summary>
        /// Search a Product by name,productnumber,sku,manufacturer,category,producttype
        /// </summary>
        private void Bind()
        {
            ProductAdmin prodadmin = new ProductAdmin();
            DataSet ds = prodadmin.SearchProductsByKeyword(Server.HtmlEncode(txtKeyword.Text.Trim()), dmanufacturer.SelectedValue, string.Empty, dproductcategory.SelectedValue);
            DataView dv = new DataView(ds.Tables[0]);
            dv.Sort = "DisplayOrder";
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }
        #endregion
    }
}