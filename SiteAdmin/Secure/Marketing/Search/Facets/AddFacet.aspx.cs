using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;

namespace SiteAdmin.Secure.Marketing.Search.Facets
{
    /// <summary>
    /// Represents the SiteAdmin - SiteAdmin.Secure.Marketing.Search.AddFacet class
    /// </summary>
    public partial class AddFacet : System.Web.UI.Page
    {
        #region Protected Variables
        private int ItemId = 0;
        private int TagId = 0;
        private string ViewLink = "~/SiteAdmin/Secure/Marketing/Search/Facets/addfacetgroup.aspx?mode=at&itemid=";
        private TagGroup tg;

        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);

                if (this.ItemId > 0)
                {
                    ZNode.Libraries.DataAccess.Service.TagGroupService tgs = new ZNode.Libraries.DataAccess.Service.TagGroupService();
                    this.tg = tgs.GetByTagGroupID(this.ItemId);

                    CatalogAdmin catalogAdmin = new CatalogAdmin();
                    ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                    if (profiles.StoreAccess != "AllStores")
                    {
                        string[] stores = catalogAdmin.GetCatalogIDsByPortalIDs(profiles.StoreAccess).Split(',');
                        int found = Array.IndexOf(stores, this.tg.CatalogID.ToString());
                        if (found == -1)
                        {
                            Response.Redirect("Default.aspx", true);
                        }
                    }

                    if (this.tg.ControlTypeID == (int)ZNode.Libraries.ECommerce.Tagging.ZNodeTagControlType.ICONS)
                    {
                        pnlIconImage.Visible = true;
                    }
                    else
                    {
                        pnlIconImage.Visible = false;
                    }
                }
            }

            // Get AttributeId from QueryString
            if (Request.Params["FacetId"] != null)
            {
                this.TagId = int.Parse(Request.Params["FacetId"]);
            }

            if (!Page.IsPostBack)
            {
                // Check for Edit Mode
                if (this.TagId > 0)
                {
                    // Bind Data into fields
                    this.BindData();
                    tblShowImage.Visible = true;
                    pnlShowImage.Visible = true;
                    tblUploadImage.Visible = false;
                    lblTitle.Text = "Edit Facet";
                }
                else
                {
                    tblShowImage.Visible = false;
                    pnlShowImage.Visible = false;
                    tblUploadImage.Visible = true;
                    lblTitle.Text = "Add Facet";
                }
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            // Declarations
            string fileName = string.Empty;
            TagAdmin _AdminAccess = new TagAdmin();
            Tag _Tag = new Tag();

            // Check for Edit Mode
            if (this.TagId > 0)
            {
                _Tag = _AdminAccess.GetByTagID(this.TagId);
            }
            else
            {
                TList<Tag> tagList = _AdminAccess.GetTagsByTagGroupID(this.ItemId);
                Tag tag = tagList.Find("TagName", Server.HtmlEncode(tagName.Text.Trim()));
                if (tag != null)
                {
                    lblError.Text = "This Facet Name already exists, Please try with different facet name.";
                    return;
                }
            }

            // Set Values
            _Tag.TagName = Server.HtmlEncode(tagName.Text.Trim());
            _Tag.TagDisplayOrder = int.Parse(DisplayOrder.Text.Trim());
            _Tag.TagGroupID = this.ItemId;

            // Image Process
            if ((this.TagId == 0) || (RadioNewImage.Checked == true))
            {
                if (UploadIconImage.PostedFile != null)
                {
                    if (UploadIconImage.PostedFile.FileName != string.Empty)
                    {
                        // Check for Product Image
                        fileName =  System.IO.Path.GetFileNameWithoutExtension(UploadIconImage.PostedFile.FileName) + "_" + DateTime.Now.ToString("ddMMyyhhmmss") + System.IO.Path.GetExtension(UploadIconImage.PostedFile.FileName);

                        // Check for Product Image
                        if (fileName != string.Empty)
                        {
                            _Tag.IconPath = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + fileName;
                        }
                    }
                }
            }
            else
            {
                _Tag.IconPath = _Tag.IconPath;
            }

            // Upload File if this is a new tag or the New Image option was selected for an existing product
            if (RadioNewImage.Checked || this.TagId == 0)
            {
                if (fileName != string.Empty)
                {
                    byte[] imageData = new byte[UploadIconImage.PostedFile.InputStream.Length];
                    UploadIconImage.PostedFile.InputStream.Read(imageData, 0, (int)UploadIconImage.PostedFile.InputStream.Length);
                    ZNodeStorageManager.WriteBinaryStorage(imageData, ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + fileName);
                    UploadIconImage.Dispose();
                }
            }

            bool status = false;

            if (this.TagId > 0)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit of Tag - " + tagName.Text, tagName.Text);

                // Update Product Attribute
                status = _AdminAccess.UpdateTag(_Tag);
            }
            else
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Creation of Tag - " + tagName.Text, tagName.Text);

                status = _AdminAccess.AddTag(_Tag);
            }

            if (status)
            {
                // Redirect to main page
                Response.Redirect(this.ViewLink + this.ItemId);
            }
            else
            {
                // Display error message
                lblError.Text = "An error occurred while updating. Please try again.";
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            // Redirect to main page
            Response.Redirect(this.ViewLink + this.ItemId);
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioCurrentImage_CheckedChanged(object sender, EventArgs e)
        {
            tblUploadImage.Visible = false;
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioNewImage_CheckedChanged(object sender, EventArgs e)
        {
            tblUploadImage.Visible = true;
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind Edit Attribute Datas
        /// </summary>
        private void BindData()
        {
            // Declarations
            TagAdmin _AdminAccess = new TagAdmin();
            Tag _Tag = _AdminAccess.GetByTagID(this.TagId);

            // Check Tag for null
            if (_Tag != null)
            {
                tagName.Text = Server.HtmlDecode(_Tag.TagName);
                DisplayOrder.Text = _Tag.TagDisplayOrder.ToString();
                ZNodeImage znodeImage = new ZNodeImage();
                IconImage.ImageUrl = znodeImage.GetImageHttpPathSmall(_Tag.IconPath);
            }
        }
        #endregion
    }
}