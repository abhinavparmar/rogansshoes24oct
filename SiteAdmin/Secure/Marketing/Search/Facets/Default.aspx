<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Marketing.Search.Facets.Default" CodeBehind="Default.aspx.cs" Title="Manage Facets - Default" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <div>
            <div class="LeftFloat" style="width: 50%">
                <h1>Facet Groups</h1>
            </div>
            <div class="ButtonStyle">
                <zn:LinkButton ID="btnAddTagGroup" runat="server"
                    ButtonType="Button" OnClick="BtnAdd_Click" CausesValidation="False" Text="Add Facet Group"
                    ButtonPriority="Primary" />
            </div>
        </div>
        <p class="ClearBoth">
            Facets enable customers to quickly filter product search results based on familiar criteria.
        </p>
        <div class="ClearBoth" align="left">
            <br />
        </div>

        <h4 class="SubTitle">Search Facet Groups</h4>
        <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">Facet Group Name</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtTagGroupName" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">Catalog</span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlCatalog" runat="server"></asp:DropDownList></span>
                    </div>
                </div>
                <div class="ClearBoth">
                </div>
                <div>
                    <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_clear_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                        runat="server" AlternateText="Clear" OnClick="BtnClear_Click" />
                    <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                        runat="server" AlternateText="Search" OnClick="BtnSearch_Click" />
                </div>
            </div>
        </asp:Panel>
        <br />
        <h4 class="GridTitle">Facet Group List</h4>
        <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" OnSorting="UxGrid_Sorting" OnPageIndexChanging="UxGrid_PageIndexChanging"
            CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" EnableSortingAndPagingCallbacks="False"
            PageSize="25" EmptyDataText="No Facet Groups exist in the database.">
            <Columns>
                <asp:BoundField DataField="TagGroupID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField SortExpression="TagGroupLabel" DataField="TagGroupLabel" HeaderText="Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="60%" />
                <asp:BoundField SortExpression="DisplayOrder" DataField="DisplayOrder" HeaderText="Display Order" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="btnEdit" CssClass="actionlink" runat="server" CommandName="Manage" Text="Manage &raquo"
                            ButtonType="Button" CommandArgument='<%# Eval("TagGroupID") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="btndelete" CssClass="actionlink" runat="server" CommandName="Delete"
                            Text="Delete &raquo" CommandArgument='<%# Eval("TagGroupID") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:LinkButton ID="btnView" CssClass="actionlink" runat="server" CommandName="Tags" Text="Tags &raquo"
                            CommandArgument='<%# Eval("TagGroupID") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        </div>
    </div>
</asp:Content>
