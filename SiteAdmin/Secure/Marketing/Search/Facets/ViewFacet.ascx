﻿<%@ Control Language="C#" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Marketing.Search.Facets.ViewFacet" CodeBehind="ViewFacet.ascx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<div class="FormView">
    <div>
        <asp:Label ID="lblTagGroupType" Visible="false" runat="server"></asp:Label>
        <div class="ButtonStyle">
            <zn:LinkButton ID="Addtag" runat="server"
                ButtonType="Button" OnClick="AddTag_Click" Text="Add Facet"
                ButtonPriority="Primary" />
        </div>
    </div>
    <div class="ClearBoth" align="left">
        <br />
    </div>
    <asp:Label ID="FailureText" runat="Server" EnableViewState="false" CssClass="Error" />
    <h4 class="GridTitle">Facet List</h4>
    <asp:GridView ID="uxGrid" runat="server" AllowPaging="True" AutoGenerateColumns="False"
        CaptionAlign="Left" CellPadding="4" CssClass="Grid" EmptyDataText="No Facets exist in the database."
        GridLines="None" Width="100%" OnPageIndexChanging="UxGrid_PageIndexChanging"
        OnRowCommand="UxGrid_RowCommand" OnRowDeleting="UxGrid_RowDeleting">
        <Columns>
            <asp:BoundField DataField="TagID" HeaderText="ID"
                HeaderStyle-HorizontalAlign="Left">
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="TagName" HeaderText="Facet Name"
                HeaderStyle-HorizontalAlign="Left">
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="TagDisplayOrder" HeaderText="Display Order"
                HeaderStyle-HorizontalAlign="Left">
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
            </asp:BoundField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="EditTag" CommandName="Edit" CommandArgument='<%# Eval("TagID") %>'
                        runat="server" CssClass="actionlink" Text="Edit &raquo" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="DeleteTag" CommandName="Delete" CommandArgument='<%# Eval("TagID") %>'
                        runat="server" CssClass="actionlink" Text="Delete &raquo"
                        OnClientClick="return confirm('Are you sure you want to delete this Facet?')" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle CssClass="RowStyle" />
        <EditRowStyle CssClass="EditRowStyle" />
        <HeaderStyle CssClass="HeaderStyle" />
        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
    </asp:GridView>
    <uc1:Spacer ID="Spacer5" SpacerHeight="1" SpacerWidth="3" runat="server"></uc1:Spacer>
</div>
