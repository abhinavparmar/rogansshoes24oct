<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" ValidateRequest="false" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Marketing.Search.Facets.AddFacet" CodeBehind="AddFacet.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
            <ZNode:DemoMode ID="DemoMode1" runat="server" />
        </div>
        <div class="LeftFloat" align="right" style="width: 30%">
            <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
        <div class="ClearBoth" align="left">
        </div>
        <asp:Label ID="lblError" runat="server" CssClass="Error" />
        <div class="FieldStyle">
            Facet Value <span class="Asterix">*</span><br />
            <small>Use only characters a-z and 0-9. Do not use a special
                or other language characters in your facet name.</small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="tagName" runat="server" MaxLength="50" Columns="30"></asp:TextBox>&nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tagName"
                Display="Dynamic" ErrorMessage="* Enter a Facet name" SetFocusOnError="True"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
                    ID="RegularExpressionValidator2" runat="server" ControlToValidate="tagName"
                    CssClass="Error" Display="Dynamic" ErrorMessage="Do not use a special
                or other language characters in your Facet name."
                    SetFocusOnError="True"
                    ValidationExpression="([A-Za-z0-9-_ <>]+)"></asp:RegularExpressionValidator>
        </div>
        <div class="FieldStyle">
            Display Order<span class="Asterix">*</span><br />
            <small>Enter a number. Items with a lower number are displayed first on the page.</small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="DisplayOrder" runat="server" MaxLength="9" Columns="5"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Display="Dynamic"
                ErrorMessage="* Enter a Display Order" ControlToValidate="DisplayOrder"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="DisplayOrder"
                Display="Dynamic" ErrorMessage="Enter a whole number." MaximumValue="999999999"
                MinimumValue="1" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="ClearBoth" align="left">
        </div>
        <asp:Panel runat="server" ID="pnlIconImage">
            <h4 class="SubTitle">Icon Image</h4>
            <small>Upload a suitable icon image for your facet. Only JPG, GIF and PNG images are supported.
                The file size should be less than 1.5 Meg. Your image will automatically be scaled
                so it displays correctly in the catalog.</small>
            <div>
                <div class="LeftFloat" id="tblShowImage" runat="server" visible="false" style="width: 150px; border-right: solid 1px #cccccc;">
                    <asp:Image ID="IconImage" runat="server" />
                </div>
                <div class="LeftFloat" style="margin-left: -1px; margin-top: 5px;">
                    <asp:Panel runat="server" ID="pnlShowImage">
                        <div>
                            Select an Option
                        </div>
                        <div>
                            <asp:RadioButton ID="RadioCurrentImage" Text="Keep Current Image" runat="server"
                                GroupName="TagIconImage" AutoPostBack="True" OnCheckedChanged="RadioCurrentImage_CheckedChanged"
                                Checked="True" /><br />
                            <asp:RadioButton ID="RadioNewImage" Text="Upload New Image" runat="server"
                                GroupName="TagIconImage" AutoPostBack="True" OnCheckedChanged="RadioNewImage_CheckedChanged" />
                        </div>
                        <br />
                    </asp:Panel>
                    <div id="tblUploadImage" runat="server" visible="false">
                        <div class="FieldStyle">
                            Select an Image:
                        </div>
                        <div class="ValueStyle">
                            <asp:FileUpload ID="UploadIconImage" runat="server" BorderStyle="Inset" EnableViewState="true" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="UploadIconImage"
                                CssClass="Error" Display="dynamic" ValidationExpression=".*(\.[Jj][Pp][Gg]|\.[Gg][Ii][Ff]|\.[Jj][Pp][Ee][Gg]|\.[Pp][Nn][Gg])"
                                ErrorMessage="Please select a valid JPEG, JPG, PNG or GIF image"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div>
                        <asp:Label ID="lblImageError" runat="server" CssClass="Error" ForeColor="Red"
                            Text="" Visible="False"></asp:Label>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <div class="ClearBoth">
            <br />
            <br />
        </div>
        <div>
            <asp:ImageButton ID="btnSubmit" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>
