<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" ValidateRequest="false" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Marketing.Search.Facets.AddFacetGroup" CodeBehind="AddFacetGroup.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="viewtag" Src="~/SiteAdmin/Secure/Marketing/Search/Facets/viewfacet.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <script type="Text/JavaScript">
        function ListBox(sender, args) {
            var ctlListbox = document.getElementById(sender.controltovalidate);
            args.IsValid = ctlListbox.options.length > 0;
        }
    </script>
    <div class="FormView">
        <h1>
            <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
        <div>
            <ZNode:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <ajaxToolKit:TabContainer ID="tabTagGroupSettings" runat="server">
            <ajaxToolKit:TabPanel ID="pnlSettings" runat="server">
                <HeaderTemplate>
                    Facet Group Information
                </HeaderTemplate>
                <ContentTemplate>
                    <h4 class="SubTitle">General Settings</h4>
                    <div class="FieldStyle">
                        Facet Group Name<span class="Asterix">*</span><br />
                        <small>Use only characters a-z and 0-9. Do not use a special or other language characters in your facet name.</small>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtTagGroupName" runat="server" Width="152px"></asp:TextBox>&nbsp;
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTagGroupName"
                                    ErrorMessage="* Enter Facet Group Name" CssClass="Error" Display="Dynamic"
                                    ValidationGroup="grpTitle"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtTagGroupName"
                            CssClass="Error" Display="Dynamic" ErrorMessage="Enter a valid name. Do not use a special or other language characters in your facet name"
                            SetFocusOnError="True" ValidationExpression="([A-Za-z0-9-_ ]+)" ValidationGroup="grpTitle"></asp:RegularExpressionValidator>
                    </div>
                    <div class="FieldStyle">
                        Display Order<span class="Asterix">*</span><br />
                        <small style="display:none;">Enter a number. This determines the order in which the product will be displayed.
                                    A product with the lower display order will be displayed first.</small>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtDisplayOrder" runat="server" MaxLength="9" Columns="5">500</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ValidationGroup="grpTitle" ControlToValidate="txtDisplayOrder"
                            CssClass="Error" Display="Dynamic" ErrorMessage="* Enter a Display Order"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="txtDisplayOrder"
                            CssClass="Error" Display="Dynamic" ErrorMessage="* Enter a whole number." MaximumValue="999999999" ValidationGroup="grpTitle"
                            MinimumValue="1" Type="Integer"></asp:RangeValidator>
                    </div>
                    <div class="ValueStyle">
                        <asp:DropDownList ID="ddlTagControlType" runat="server" Visible="false">
                        </asp:DropDownList>
                    </div>
                    <asp:Panel ID="pnlCatalog" runat="server">
                        <div class="FieldStyle">Select Catalog</div>
                        <div class="ValueStyle">
                            <asp:DropDownList ID="ddlCatalog" runat="server"
                                AutoPostBack="True" OnSelectedIndexChanged="DdlCatalog_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                        <br />
                    </asp:Panel>
                    <asp:UpdatePanel ID="UpdatePnlProductDetail" runat="server">
                        <ContentTemplate>
                            <h4 class="SubTitle">Associate Facet Groups With Categories</h4>
                            <div style="display: block;">
                                <div>
                                    <div class="LeftFloat" style="width: 200px;">
                                        <span class="FieldStyle">Unassociated Categories</span><br />
                                        <span>
                                            <asp:ListBox ID="lstboxCategories" runat="server" SelectionMode="Multiple" Width="150px"
                                                Height="150px"></asp:ListBox>

                                        </span>
                                    </div>
                                    <div class="LeftFloat ImageButtons" style="width: 150px; padding-top: 50px;">
                                        <span>
                                            <asp:Button CssClass="Size100" ID="btnAddlist" runat="server" Text="Add &raquo;"
                                                OnClick="BtnAddlist_Onclick" Width="100px" /><br />
                                            <br />
                                            <asp:Button CssClass="Size100" ID="btnRemovelist" runat="server" Text="&laquo; Remove"
                                                OnClick="BtnRemovelist_Onclick" Width="100px" /></span>
                                    </div>
                                    <div class="LeftFloat" style="width: 170px">
                                        <span class="FieldStyle">Associated Categories</span><br />
                                        <span>
                                            <asp:ListBox ID="lstboxTagCategories" runat="server" SelectionMode="Multiple" Width="150px"
                                                Height="150px"></asp:ListBox>
                                            <br />
                                        </span>
                                        <asp:CustomValidator ID="CustomValidator1" runat="server"
                                            ControlToValidate="lstboxTagCategories" CssClass="Error"
                                            ErrorMessage="*Please associate at least one category"
                                            OnServerValidate="CustomValidator1_ServerValidate" SetFocusOnError="True"
                                            ValidateEmptyText="True" ValidationGroup="grpTitle"
                                            ClientValidationFunction="ListBox"></asp:CustomValidator>
                                    </div>
                                    <div class="LeftFloat" style="width: 50px; padding-top: 50px;" align="left">
                                        <asp:ImageButton ImageUrl="~/SiteAdmin/Themes/images/403-up.gif" ID="btnUp" runat="server"
                                            OnClick="BtnUp_Onclick" />
                                        <br />
                                        <br />
                                        <asp:ImageButton ImageUrl="~/SiteAdmin/Themes/images/402-down.gif" ID="btndown" runat="server"
                                            OnClick="Btndown_Onclick" />
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="ClearBoth">
                    </div>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="pnlDepartment" runat="server" Visible="false">
                <HeaderTemplate>
                    Associated Facets
                </HeaderTemplate>
                <ContentTemplate>
                    <ZNode:viewtag runat="server"></ZNode:viewtag>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
        </ajaxToolKit:TabContainer>
        <br />
        <div>
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>
        <br />
        <div>
            <asp:ImageButton ID="btnSubmit" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" ValidationGroup="grpTitle" />
            <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>

</asp:Content>
