﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Marketing.Search.Facets
{
    /// <summary>
    /// Represents the SiteAdmin - SiteAdmin.Secure.Marketing.Search.Facets class
    /// </summary>
    /// 

    public partial class ViewFacet : System.Web.UI.UserControl
    {
        #region Private Member Variables
        private int ItemId = 0;
        private string AddTagValueLink = "~/SiteAdmin/Secure/Marketing/Search/Facets/addfacet.aspx?itemid=";
        private string ListLink = "~/SiteAdmin/Secure/Marketing/Search/Facets/list.aspx";
        private string EditLink = "~/SiteAdmin/Secure/Marketing/Search/Facets/addfacet.aspx?itemid=";

        #endregion

        #region Page Load
        /// <summary>
        /// page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }

            if (!Page.IsPostBack)
            {
                this.BindGrid();
                this.BindData();
            }
        }

        #endregion

        #region General Events

        /// <summary>
        /// List Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void TagGroupTypeList_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ListLink);
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddTag_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddTagValueLink + this.ItemId);
        }

        /// <summary>
        /// Edit Attribute Type Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void EditAttributeType_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.EditLink + this.ItemId);
        }

        #endregion

        #region Grid Events
        /// <summary>
        /// Grid Page Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGrid();
        }

        /// <summary>
        /// Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "page")
            {
            }
            else
            {
                // Get the Value from the command argument
                string Id = e.CommandArgument.ToString();
                if (e.CommandName == "Edit")
                {
                    // Redirect to Attribute Edit page
                    Response.Redirect(this.AddTagValueLink + this.ItemId + "&FacetId=" + Id);
                }

                if (e.CommandName == "Delete")
                {
                    TagAdmin adminAccess = new TagAdmin();
                    Tag tag = adminAccess.GetByTagID(int.Parse(Id));
                    string TagName = tag.TagName;

                    if (adminAccess.DeleteTag(tag))
                    {
                        // Nothing todo here
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Delete Tag - " + TagName, TagName);
                    }
                    else
                    {
                        FailureText.Text = "* Delete action could not be completed because the Facet Value is in use.";
                    }
                }
            }
        }

        /// <summary>
        /// Grid Row Deleting Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindGrid();
        }

        #endregion

        #region Bind Data
        /// <summary>
        /// Bind Data Method
        /// </summary>
        private void BindData()
        {
            TagAdmin adminAccess = new TagAdmin();
            TagGroup tagGroup = adminAccess.GetByTagGroupID(this.ItemId);

            if (tagGroup != null)
            {
                lblTagGroupType.Text = tagGroup.TagGroupLabel;
            }
        }

        #endregion

        #region Bind Grid

        /// <summary>
        /// Bind Grid method
        /// </summary>
        private void BindGrid()
        {
            TagAdmin adminAccess = new TagAdmin();
            TList<Tag> tagList = new TList<Tag>();
            tagList = adminAccess.GetTagsByTagGroupID(this.ItemId);
            foreach (Tag tag in tagList)
            {
                tag.TagName = Server.HtmlDecode(tag.TagName);
            }

            uxGrid.DataSource = tagList;
            uxGrid.DataBind();
        }

        #endregion
    }
}