using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Marketing.Search.Facets
{
    /// <summary>
    /// Represents the SiteAdmin - SiteAdmin.Secure.Marketing.Search.Facets.Delete class
    /// </summary>
    public partial class Delete : System.Web.UI.Page
    {
        #region Protected Variables

        private int ItemId;
        private string RedirectLink = "~/SiteAdmin/Secure/Marketing/Search/Facets/Default.aspx";
        private string _TagGroupName = string.Empty;

        #endregion

        /// <summary>
        /// Gets or sets the Product Review Title
        /// </summary>
        public string TagGroupName
        {
            get
            {
                return this._TagGroupName;
            }

            set
            {
                this._TagGroupName = value;
            }
        }

        #region Page Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (!Page.IsPostBack)
            {
                this.BindData();
            }
        }

        #endregion

        #region General Events

        /// <summary>
        /// Delete Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            TagAdmin _tagAdmin = new TagAdmin();
            TagGroup _tagGroup = _tagAdmin.GetByTagGroupID(this.ItemId);

            bool Check = false;
            string TagName = _tagGroup.TagGroupLabel;
            Check = _tagAdmin.DeleteTagGroup(_tagGroup);

            if (Check)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Delete TagGroup - " + TagName, TagName);

                Response.Redirect(this.RedirectLink);
            }
            else
            {
                this.TagGroupName = _tagGroup.TagGroupLabel;
                lblErrorMsg.Text = "* Delete action could not be completed because the Tag Group Type is in use.";
                lblErrorMsg.Visible = true;
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.RedirectLink);
        }

        #endregion

        #region Bind Data

        /// <summary>
        /// Bind Data Method
        /// </summary>
        private void BindData()
        {
            TagAdmin _tagAdmin = new TagAdmin();
            TagGroup _tagGroup = _tagAdmin.GetByTagGroupID(this.ItemId);

            if (_tagGroup != null)
            {
                CatalogAdmin catalogAdmin = new CatalogAdmin();
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (profiles.StoreAccess != "AllStores")
                {
                    string[] stores = catalogAdmin.GetCatalogIDsByPortalIDs(profiles.StoreAccess).Split(',');
                    int found = Array.IndexOf(stores, _tagGroup.CatalogID.ToString());
                    if (found == -1)
                    {
                        Response.Redirect("Default.aspx", true);
                    }
                }

                this.TagGroupName = _tagGroup.TagGroupLabel;
            }
        }

        #endregion
    }
}