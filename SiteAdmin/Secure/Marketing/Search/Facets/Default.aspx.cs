using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Marketing.Search.Facets
{
    /// <summary>
    /// Represents the SiteAdmin.Secure.Marketing.Search.Facets.Default class
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Protected Member Variables
        private string AddLink = "~/SiteAdmin/Secure/Marketing/Search/Facets/AddFacetGroup.aspx";
        private string DeleteLink = "~/SiteAdmin/Secure/Marketing/Search/Facets/Delete.aspx?itemid=";
        private string ViewLink = "~/SiteAdmin/Secure/Marketing/Search/Facets/View.aspx?itemid=";
        private string EditLink = "~/SiteAdmin/Secure/Marketing/Search/Facets/AddFacetGroup.aspx?itemid=";
        #endregion

        #region Protected properties
        private string GridViewSortDirection
        {
            get 
            { 
                return ViewState["SortDirection"] as string ?? "ASC"; 
            }

            set 
            { 
                ViewState["SortDirection"] = value; 
            }
        }
        #endregion

        #region General Events

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindCatalogData();
                this.BindSearchData();
            }
        }
        
        /// <summary>
        /// Add Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddLink);
        }

        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchData();            
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandArgument.ToString() == "page")
            { 
            }
            else
            {
                string Id = e.CommandArgument.ToString();

                if (e.CommandName == "Manage")
                {
                    Response.Redirect(this.EditLink + Id);
                }
                else if (e.CommandName == "Tags")
                {
                    Response.Redirect(this.ViewLink + Id);
                }
                else if (e.CommandName == "Delete")
                {
                    Response.Redirect(this.DeleteLink + Id);
                }
            }
        }

        /// <summary>
        /// Grid Sorting Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            TagAdmin tagAccess = new TagAdmin();
            DataSet ds = tagAccess.GetTagGroupBySearchData(Server.HtmlEncode(txtTagGroupName.Text.Trim()), int.Parse(ddlCatalog.SelectedValue));
            uxGrid.DataSource = this.SortDataTable(ds, e.SortExpression, true);
            uxGrid.DataBind();

            if (this.GetSortDirection() == "DESC")
            {
                e.SortDirection = SortDirection.Descending;
            }
            else
            {
                e.SortDirection = SortDirection.Ascending;
            }
        }

        /// <summary>
        /// Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindSearchData();
        }

        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind Search Data
        /// </summary>
        protected void BindSearchData()
        {
            TagAdmin tagAccess = new TagAdmin();
            DataSet ds = tagAccess.GetTagGroupBySearchData(Server.HtmlEncode(txtTagGroupName.Text.Trim()), int.Parse(ddlCatalog.SelectedValue));
            DataView dv = new DataView(ds.Tables[0]);
            dv.Sort = "DisplayOrder";
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Bind Store Catalogs
        /// </summary>
        protected void BindCatalogData()
        {
            CatalogAdmin catalogAdmin = new CatalogAdmin();
            TList<Catalog> catalogList = catalogAdmin.GetAllCatalogs();
            DataSet ds = catalogList.ToDataSet(false);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                {
                    ds.Tables[0].Rows[index]["Name"] = Server.HtmlDecode(ds.Tables[0].Rows[index]["Name"].ToString());
                }
            }
            
            DataView dataView = ds.Tables[0].DefaultView;
            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (string.Compare(profiles.StoreAccess, "AllStores") != 0)
            {
                dataView.RowFilter = string.Concat("CatalogID IN (", catalogAdmin.GetCatalogIDsByPortalIDs(profiles.StoreAccess), ")");
            }
            
            ddlCatalog.DataSource = dataView;
            ddlCatalog.DataTextField = "Name";
            ddlCatalog.DataValueField = "CatalogID";
            ddlCatalog.DataBind();

            if (string.Compare(profiles.StoreAccess, "AllStores") == 0)
            {
                ListItem li = new ListItem("ALL", "0");
                ddlCatalog.Items.Insert(0, li);
            }
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Sorting Data
        /// </summary>
        /// <param name="dataSet">The value of dataset to be sorted</param>
        /// <param name="gridViewSortExpression">Grid View Sort Expression</param>
        /// <param name="isPageIndexChanging">Is page Index Changing value</param>
        /// <returns>Returns the DataView</returns>
        protected DataView SortDataTable(DataSet dataSet, string gridViewSortExpression, bool isPageIndexChanging)
        {
            if (dataSet != null)
            {
                DataView dataView = new DataView(dataSet.Tables[0]);

                if (gridViewSortExpression.Length > 0)
                {
                    if (isPageIndexChanging)
                    {
                        dataView.Sort = string.Format("{0} {1}", gridViewSortExpression, this.GridViewSortDirection);
                    }
                    else
                    {
                        dataView.Sort = string.Format("{0} {1}", gridViewSortExpression, this.GetSortDirection());
                    }
                }
                
                return dataView;
            }
            else
            {
                return new DataView();
            }
        }

        /// <summary>
        /// Get Sort Direction Method
        /// </summary>
        /// <returns>Returns the Sort Direction</returns>
        private string GetSortDirection()
        {
            switch (this.GridViewSortDirection)
            {
                case "ASC":
                    this.GridViewSortDirection = "DESC";
                    break;

                case "DESC":
                    this.GridViewSortDirection = "ASC";
                    break;
            }
            
            return this.GridViewSortDirection;
        }
        #endregion
    }
}