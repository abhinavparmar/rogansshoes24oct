<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Marketing.Other.StoreLocator.Default" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>Store Locator
                 <div class="tooltip">
                     <a href="javascript:void(0);" class="learn-more"><span>
                         <asp:Localize ID="Localize2" runat="server"></asp:Localize></span></a>
                     <div class="content">
                         <h6>Help</h6>
                         <p>
                             To enable the Store Locator search function you need to purchase and upload
                ZIP code data. See our documentation for instructions.
                         </p>
                     </div>
                 </div>

            </h1>
        </div>
        <div class="ButtonStyle">
            <zn:LinkButton ID="btnAddStore" runat="server" CausesValidation="False"
                ButtonType="Button" OnClick="BtnAddStore_Click" Text="Add New Store Location"
                ButtonPriority="Primary" />
        </div>
        <div class="ClearBoth">
            <p>
                Manage locations of your physical stores that can then be searched via a store locator.
            </p>
        </div>
        <h4 class="SubTitle">Search Store</h4>
        <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">Store Name</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtstorename" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">Zip Code</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtzipcode" runat="server"></asp:TextBox></span>
                    </div>
                </div>
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">City</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtcity" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">State</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtstate" runat="server"></asp:TextBox></span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_clear_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                        runat="server" AlternateText="Clear" OnClick="BtnClear_Click" />
                    <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                        runat="server" AlternateText="Search" OnClick="BtnSearch_Click" />
                </div>
            </div>
        </asp:Panel>
        <br />
        <h4 class="GridTitle">Store List</h4>
        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
            CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" EnableSortingAndPagingCallbacks="False"
            PageSize="25" AllowSorting="True" EmptyDataText="No store exist in the database.">
            <Columns>
                <asp:BoundField DataField="StoreID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText="Store Name" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <a href='Add.aspx?itemid=<%# DataBinder.Eval(Container.DataItem, "StoreId").ToString()%>'>
                            <%# Eval("Name") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="City" HtmlEncode="false" HeaderText="City" HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="State" HtmlEncode="false" HeaderText="State" HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="Zip" HeaderText="Zip Code" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText="Is Active" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ACTIONS" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <div class="LeftFloat" style="width: 40%; text-align: left">
                            <asp:LinkButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("StoreId") %>'
                                CommandName="Manage" Text="MANAGE &raquo" CssClass="LinkButton" />
                        </div>
                        <div class="LeftFloat" style="width: 50%; text-align: left">
                            <asp:LinkButton ID="btnDelete" runat="server" CommandArgument='<%# Eval("StoreId") %>'
                                CommandName="Delete" Text="DELETE &raquo " CssClass="LinkButton" />
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        </div>
    </div>
</asp:Content>
