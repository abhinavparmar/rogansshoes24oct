using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;

namespace SiteAdmin.Secure.Marketing.Other.StoreLocator
{
    /// <summary>
    /// Represents the SiteAdmin.Secure.Marketing.Other.StoreLocator.Default class.
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Private Variables
        private static bool isSearchEnabled = false;
        private string addPageLink = "~/SiteAdmin/Secure/Marketing/Other/StoreLocator/Add.aspx";
        private string deletePageLink = "~/SiteAdmin/Secure/Marketing/Other/StoreLocator/Delete.aspx";        
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindGridData();
            }
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            // Check if search is Enabled
            if (isSearchEnabled)
            {
                uxGrid.PageIndex = e.NewPageIndex;
                this.BindSearchStore(); 
            }
            else
            {
                uxGrid.PageIndex = e.NewPageIndex;
                this.BindGridData();
            }
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // property to an Integer.
                string Id = e.CommandArgument.ToString();

                if (e.CommandName == "Manage")
                {
                    this.addPageLink = this.addPageLink + "?ItemID=" + Id;
                    Response.Redirect(this.addPageLink);
                }
                else if (e.CommandName == "Delete")
                {
                    Response.Redirect(this.deletePageLink + "?ItemID=" + Id);
                }
            }
        }

        #endregion       

        #region General Events

        /// <summary>
        /// Add New Store Detail
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddStore_Click(object sender, System.EventArgs e)
        {
            Response.Redirect(this.addPageLink);
        }

        /// <summary>
        /// Search button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchStore();
            isSearchEnabled = true;
        }

        /// <summary>
        /// Clear button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            txtcity.Text = string.Empty;
            txtstate.Text = string.Empty;
            txtstorename.Text = string.Empty;
            txtzipcode.Text = string.Empty;
            this.BindGridData();
        }

        #endregion

        #region Bind

        /// <summary>
        /// Search a Store by Store Name, ZipCode , State and City
        /// </summary>
        private void BindSearchStore()
        {
            StoreLocatorAdmin storeLocatorAdmin = new StoreLocatorAdmin();
            uxGrid.DataSource = storeLocatorAdmin.SearchStore(Server.HtmlEncode(txtstorename.Text.Trim()), txtzipcode.Text.Trim(), txtcity.Text.Trim(), txtstate.Text.Trim());
            uxGrid.DataBind();
        }

        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindGridData()
        {
            StoreLocatorAdmin storeLocatorAdmin = new StoreLocatorAdmin();
            uxGrid.DataSource = UserStoreAccess.CheckStoreAccess(storeLocatorAdmin.GetAllStore());
            uxGrid.DataBind();
        }

        #endregion
    }
}