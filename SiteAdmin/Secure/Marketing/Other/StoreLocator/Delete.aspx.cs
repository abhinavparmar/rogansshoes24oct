using System;
using System.Web.UI;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace SiteAdmin.Secure.Marketing.Other.StoreLocator
{
    /// <summary>
    /// Represents the SiteAdmin.Secure.Marketing.Other.StoreLocator.Delete class.
    /// </summary>
    public partial class Delete : System.Web.UI.Page
    {
        #region Protected Variables
        private int itemId;
        private string redirectLink = "~/SiteAdmin/Secure/Marketing/Other/StoreLocator/Default.aspx";
        private string _StoreName = string.Empty;        
        #endregion

        /// <summary>
        /// Gets or sets the store name.
        /// </summary>
        public string StoreName
        {
            get { return this._StoreName; }
            set { this._StoreName = value; }
        }

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemID from querystring        
            if (Request.Params["ItemID"] != null)
            {
                this.itemId = int.Parse(Request.Params["ItemID"]);
            }
            else
            {
                this.itemId = 0;
            }

            if (!Page.IsPostBack)
            {
                this.BindData();
            }
        }
        #endregion        

        #region General Events

        /// <summary>
        /// Delete button clcick event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            ZNode.Libraries.Admin.StoreLocatorAdmin StoreAdmin = new StoreLocatorAdmin();

            bool isDeleted = false;

            Store store = StoreAdmin.GetByStoreID(this.itemId);

            string associateName = "Delete Store Location '" + store.Name + "' Store Locator";
            string storeLocatorName = store.Name;

            isDeleted = StoreAdmin.DeleteStore(this.itemId);

            if (isDeleted)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, storeLocatorName);

                Response.Redirect(this.redirectLink);
            }
            else
            {
                lblErrorMsg.Text = "Delete action could not be completed.";
                lblErrorMsg.Visible = true;
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.redirectLink);
        }
        #endregion

        #region Bind Data

        /// <summary>
        /// Bind the edit data
        /// </summary>
        private void BindData()
        {
            ZNode.Libraries.Admin.StoreLocatorAdmin storeLocatorAdmin = new StoreLocatorAdmin();
            ZNode.Libraries.DataAccess.Entities.Store store = storeLocatorAdmin.GetByStoreID(this.itemId);

            if (store != null)
            {
                this.StoreName = store.Name;
            }
        }

        #endregion
    }
}