﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Reporting.WebForms;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_Reports_InventoryReport class
    /// </summary>
    public partial class Admin_Secure_Reports_InventoryReport : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private string _ReportTitle;
        private DataTable subReportDataSource = new DataTable();
        private bool _StoreSelectorVisible = false;
        private bool _IntervalSelector = false;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the IsPostBack property
        /// </summary>
        public string IsPostback
        {
            get 
            { 
                return hdnIsPostBack.Value; 
            }

            set 
            { 
                hdnIsPostBack.Value = value; 
            }
        }
       
        /// <summary>
        /// Gets or sets a value indicating whether the store selector visibility
        /// </summary>
        public bool StoreSelectorVisible
        {
            get
            {
                return this._StoreSelectorVisible;
            }
            
            set
            {
                this._StoreSelectorVisible = value;
            }
        }
     
        /// <summary>
        /// Gets or sets a value indicating whether the interval selector visibility
        /// </summary>
        public bool IntervalSelector
        {
            get
            {
                if (hdnInventorySelector.Value.Trim().Length > 0)
                {
                    return Convert.ToBoolean(hdnInventorySelector.Value);
                }
                else
                {
                    return this._IntervalSelector;
                }
            }
            
            set
            {
                this._IntervalSelector = value;
                hdnInventorySelector.Value = value.ToString();
                pnlCustom.Visible = this._IntervalSelector;
            }
        }

        /// <summary>
        /// Gets or sets the Mode
        /// </summary>
        public ZnodeReport Mode
        {
            get
            {
                string mode = hdnMode.Value;
                ZnodeReport report = ZnodeReport.ServiceRequest;
                if (Enum.IsDefined(typeof(ZnodeReport), mode))
                {
                    report = (ZnodeReport)Enum.Parse(typeof(ZnodeReport), hdnMode.Value, true);
                }
                
                return report;
            }
            
            set 
            { 
                hdnMode.Value = value.ToString(); 
            }
        }

        /// <summary>
        /// Gets or sets the ReportTitle
        /// </summary>
        public string ReportTitle
        {
            get 
            { 
                return this._ReportTitle; 
            }

            set
            {
                this._ReportTitle = value;
                lblTitle.Text = value;
            }
        }

        #endregion

        #region Page Load Event

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Headers["User-Agent"].Contains("WebKit") || Request.Headers["User-Agent"].Contains("Firefox"))
            {
                objReportViewer.InteractivityPostBackMode = InteractivityPostBackMode.AlwaysSynchronous;
            }

            if (!Page.IsPostBack && this.Mode == ZnodeReport.ReOrder)
            {
                this.objReportViewer.Visible = false;
                this.BtnOrderFilter_Click(sender, e);
            }
        }

        /// <summary>
        /// Page Pre Render Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!Convert.ToBoolean(this.IsPostback))
            {
                // Set interval selector visiblity based on parameter.
                pnlCustom.Visible = this.IntervalSelector;
                this.objReportViewer.Visible = false;
                hdnIsPostBack.Value = "true";

                pnlStorelist.Visible = true;
                ddlPortal.SelectedIndex = -1;
                ListOrderStatus.SelectedIndex = -1;
                this.BindStores();
                if (this.Mode == ZnodeReport.ReOrder)
                {
                    pnlCustom.Visible = this.IntervalSelector;
                    ZnodeReport selectedReport = this.GetZnodeReportName();

                    string portalId = ddlPortal.SelectedValue == string.Empty ? "0" : ddlPortal.SelectedValue;
                    string reportPath = string.Empty;
                    string reportSourceName = string.Empty;

                    DataSet reportDataSet = null;
                    ReportAdmin reportAdmin = new ReportAdmin();

                    switch (this.Mode)
                    {

                        case ZnodeReport.ReOrder:
                            reportPath = "SiteAdmin/Secure/Reports/ReOrder.rdlc";
                            reportSourceName = "ZNodeReorderDataSet_Reorder";
                            break;

                    }

                    reportDataSet = reportAdmin.ReportList(selectedReport, DateTime.Today, DateTime.Today, string.Empty, portalId, string.Empty);


                    if (reportDataSet.Tables.Count == 0 || reportDataSet.Tables[0].Rows.Count == 0)
                    {
                        lblErrorMsg.Text = "No records found";
                        objReportViewer.Visible = false;
                        return;
                    }
                    else
                    {
                        this.BindReport(reportPath, reportSourceName, reportDataSet);
                    }

                    string RptName = reportPath.ToString();
                    RptName = RptName.Remove(0, 21);
                    RptName = RptName.Remove(RptName.Length - 5, 5);

                    // Log Activity
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.ActivityLogReport, UserStoreAccess.GetCurrentUserName(), null, null, null, null, RptName + " - Report", RptName);
       
                }
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Report Viewer - Sub Report processing event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            if (this.Mode == ZnodeReport.ServiceRequest)
            {
                e.DataSources.Add(new ReportDataSource("ZNodeCaseRequestDataSet_CaseRequestDetails", this.subReportDataSource));
            }
            
            if (this.Mode == ZnodeReport.Picklist)
            {
                e.DataSources.Add(new ReportDataSource("ZNodePickListDataSet_PickListItem", this.subReportDataSource));
            }
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            ddlPortal.SelectedIndex = -1;
            ListOrderStatus.SelectedIndex = -1;
            this.objReportViewer.Visible = false;
        }

        /// <summary>
        /// Order Filter Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnOrderFilter_Click(object sender, EventArgs e)
        {
            pnlCustom.Visible = this.IntervalSelector;
            ZnodeReport selectedReport = this.GetZnodeReportName();

            string period = ListOrderStatus.SelectedItem.Text;
            string portalId = ddlPortal.SelectedValue == string.Empty ? "0" : ddlPortal.SelectedValue;
            string reportPath = string.Empty;
            string reportSourceName = string.Empty;

            DataSet reportDataSet = null;
            ReportAdmin reportAdmin = new ReportAdmin();

            switch (this.Mode)
            {
                case ZnodeReport.FrequentCustomer:
                    reportPath = "SiteAdmin/Secure/Reports/FrequentCustomer.rdlc";
                    reportSourceName = "ZNodeFrequentCustomerDataSet_FrequentCustomer";
                    break;
                case ZnodeReport.TopSpendingCustomer:
                    reportPath = "SiteAdmin/Secure/Reports/VolumeCustomer.rdlc";
                    reportSourceName = "ZNodeTopSpendingCustomerDataSet_TopSpendingCustomer";
                    break;
                case ZnodeReport.TopEarningProduct:
                    reportPath = "SiteAdmin/Secure/Reports/PopularProduct.rdlc";
                    reportSourceName = "ZnodeTopEarningProductDataSet_TopEarningProduct";
                    break;
                case ZnodeReport.BestSeller:
                    reportPath = "SiteAdmin/Secure/Reports/BestSellers.rdlc";
                    reportSourceName = "ZNodeBestSellerDataSet_BestSeller";
                    break;
                case ZnodeReport.CouponUsage:
                    reportPath = "SiteAdmin/Secure/Reports/CouponUsage.rdlc";
                    reportSourceName = "ZNodeCoupon_ZNodeCoupon";
                    break;
                case ZnodeReport.AffiliateOrder:
                    reportPath = "SiteAdmin/Secure/Reports/Affiliate.rdlc";
                    reportSourceName = "ZNodeAffiliate_ZNodeAffiliate";
                    break;
                case ZnodeReport.ServiceRequest:
                    reportPath = "SiteAdmin/Secure/Reports/Feedback.rdlc";
                    reportSourceName = "ZNodeCaseRequestDataSet_CaseRequest";
                    break;
                case ZnodeReport.ReOrder:
                    reportPath = "SiteAdmin/Secure/Reports/ReOrder.rdlc";
                    reportSourceName = "ZNodeReorderDataSet_Reorder";
                    break;
                case ZnodeReport.EmailOptInCustomer:
                    reportPath = "SiteAdmin/Secure/Reports/EmailOptIn.rdlc";
                    reportSourceName = "ZNodeEmailOptInCustomerDataSet_EmailOptInCustomer";
                    break;
                case ZnodeReport.Picklist:
                    reportPath = "SiteAdmin/Secure/Reports/Inventory.rdlc";
                    reportSourceName = "ZNodePickListDataSet_PickList";
                    break;
            }

            reportDataSet = reportAdmin.ReportList(selectedReport, DateTime.Today, DateTime.Today, string.Empty, portalId, string.Empty);

            // Description decoded for ServiceRequest Report
            if (this.Mode == ZnodeReport.ServiceRequest)
            {
                if (reportDataSet.Tables[1].Rows.Count > 0)
                {
                    for (int index = 0; index < reportDataSet.Tables[1].Rows.Count; index++)
                    {
                        reportDataSet.Tables[1].Rows[index]["Description"] = Server.HtmlDecode(reportDataSet.Tables[1].Rows[index]["Description"].ToString());
                    }
                }
            }
            
            if (reportDataSet.Tables.Count == 0 || reportDataSet.Tables[0].Rows.Count == 0)
            {
                lblErrorMsg.Text = "No records found";
                objReportViewer.Visible = false;
                return;
            }
            else
            {
                this.BindReport(reportPath, reportSourceName, reportDataSet);
            }
            
            string RptName = reportPath.ToString();
            RptName = RptName.Remove(0, 21);
            RptName = RptName.Remove(RptName.Length - 5, 5);

            // Log Activity
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.ActivityLogReport, UserStoreAccess.GetCurrentUserName(), null, null, null, null, RptName + " - Report", RptName);
        }

        #endregion

        #region Helper Methods
        /// <summary>
        /// Bind Stores Method
        /// </summary>
        private void BindStores()
        {
            // Load StoresList
            PortalAdmin portalAdmin = new PortalAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Portal> portals = portalAdmin.GetAllPortals();

            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (profiles.StoreAccess != "AllStores")
            {
                portals.Filter = string.Concat("PortalID IN [", profiles.StoreAccess, "]");
            }
            else
            {
                Portal po = new Portal();
                po.StoreName = "ALL STORES";
                portals.Insert(0, po);
            }

            // Portal Drop Down List
            ddlPortal.DataSource = portals;
            ddlPortal.DataTextField = "StoreName";
            ddlPortal.DataValueField = "PortalID";
            ddlPortal.DataBind();

            foreach (ListItem listItem in ddlPortal.Items)
            {
                listItem.Text = Server.HtmlDecode(listItem.Text);
            }

            string[] period = (string[])Enum.GetNames(typeof(ZnodeReportInterval));
            ListOrderStatus.DataSource = period;
        }

        /// <summary>
        /// Bind Report Method
        /// </summary>
        /// <param name="reportPath">The value of reportPath</param>
        /// <param name="reportSourceName">The value of reportSourceName </param>
        /// <param name="ds">The value of DataSet</param>
        private void BindReport(string reportPath, string reportSourceName, DataSet ds)
        {
            this.objReportViewer.Visible = true;
            this.objReportViewer.Reset();
            this.objReportViewer.LocalReport.DataSources.Clear();
            this.objReportViewer.PageCountMode = PageCountMode.Actual; 
            this.objReportViewer.LocalReport.ReportPath = reportPath;
            ReportParameter param1 = new ReportParameter("CurrentLanguage", System.Globalization.CultureInfo.CurrentCulture.Name);
            objReportViewer.LocalReport.SetParameters(new ReportParameter[] { param1 });
            
            if (this.Mode == ZnodeReport.Picklist || this.Mode == ZnodeReport.ServiceRequest)
            {
                this.subReportDataSource = ds.Tables[1];
                this.objReportViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(this.LocalReport_SubreportProcessing);
            }

            this.objReportViewer.LocalReport.DataSources.Add(new ReportDataSource(reportSourceName, ds.Tables[0]));
            this.objReportViewer.LocalReport.Refresh();
        }

        /// <summary>
        /// Get the selected ZnodeReport enumeration object
        /// </summary>
        /// <returns>Returns the selected ZnodeReport</returns>
        private ZnodeReport GetZnodeReportName()
        {
            // Selected report
            ZnodeReport report = ZnodeReport.None;

            // Selected interval.
            string selectedinterval = ListOrderStatus.SelectedItem.Text;
            ZnodeReportInterval interval = ZnodeReportInterval.None;
            
            // Convert selected string to ZnodeReportinterval enumeration
            if (Enum.IsDefined(typeof(ZnodeReportInterval), selectedinterval))
            {
                interval = (ZnodeReportInterval)Enum.Parse(typeof(ZnodeReportInterval), selectedinterval, true);
            }
            
            switch (this.Mode)
            {
                case ZnodeReport.FrequentCustomer:
                    if (interval == ZnodeReportInterval.Day)
                    {
                        report = ZnodeReport.FrequentCustomerByDay;
                    }
                    else if (interval == ZnodeReportInterval.Week)
                    {
                        report = ZnodeReport.FrequentCustomerByWeek;
                    }
                    else if (interval == ZnodeReportInterval.Month)
                    {
                        report = ZnodeReport.FrequentCustomerByMonth;
                    }
                    else if (interval == ZnodeReportInterval.Quarter)
                    {
                        report = ZnodeReport.FrequentCustomerByQuarter;
                    }
                    else if (interval == ZnodeReportInterval.Year)
                    {
                        report = ZnodeReport.FrequentCustomerByYear;
                    }
                    else
                    {
                        report = ZnodeReport.FrequentCustomer;
                    }
                    
                    break;

                case ZnodeReport.TopSpendingCustomer:
                    if (interval == ZnodeReportInterval.Day)
                    {
                        report = ZnodeReport.TopSpendingCustomerByDay;
                    }
                    else if (interval == ZnodeReportInterval.Week)
                    {
                        report = ZnodeReport.TopSpendingCustomerByWeek;
                    }
                    else if (interval == ZnodeReportInterval.Month)
                    {
                        report = ZnodeReport.TopSpendingCustomerByMonth;
                    }
                    else if (interval == ZnodeReportInterval.Quarter)
                    {
                        report = ZnodeReport.TopSpendingCustomerByQuarter;
                    }
                    else if (interval == ZnodeReportInterval.Year)
                    {
                        report = ZnodeReport.TopSpendingCustomerByYear;
                    }
                    else
                    {
                        report = ZnodeReport.TopSpendingCustomer;
                    }
                    
                    break;

                case ZnodeReport.TopEarningProduct:
                    if (interval == ZnodeReportInterval.Day)
                    {
                        report = ZnodeReport.TopEarningProductByDay;
                    }
                    else if (interval == ZnodeReportInterval.Week)
                    {
                        report = ZnodeReport.TopEarningProductByWeek;
                    }
                    else if (interval == ZnodeReportInterval.Month)
                    {
                        report = ZnodeReport.TopEarningProductByMonth;
                    }
                    else if (interval == ZnodeReportInterval.Quarter)
                    {
                        report = ZnodeReport.TopEarningProductByQuarter;
                    }
                    else if (interval == ZnodeReportInterval.Year)
                    {
                        report = ZnodeReport.TopEarningProductByYear;
                    }
                    else
                    {
                        report = ZnodeReport.TopEarningProduct;
                    }
                    
                    break;

                case ZnodeReport.BestSeller:
                    if (interval == ZnodeReportInterval.Day)
                    {
                        report = ZnodeReport.BestSellerByDay;
                    }
                    else if (interval == ZnodeReportInterval.Week)
                    {
                        report = ZnodeReport.BestSellerByWeek;
                    }
                    else if (interval == ZnodeReportInterval.Month)
                    {
                        report = ZnodeReport.BestSellerByMonth;
                    }
                    else if (interval == ZnodeReportInterval.Quarter)
                    {
                        report = ZnodeReport.BestSellerByQuarter;
                    }
                    else if (interval == ZnodeReportInterval.Year)
                    {
                        report = ZnodeReport.BestSellerByYear;
                    }
                    else
                    {
                        report = ZnodeReport.BestSeller;
                    }
                    
                    break;

                case ZnodeReport.CouponUsage:

                    if (interval == ZnodeReportInterval.Day)
                    {
                        report = ZnodeReport.CouponUsageByDay;
                    }
                    else if (interval == ZnodeReportInterval.Week)
                    {
                        report = ZnodeReport.CouponUsageByWeek;
                    }
                    else if (interval == ZnodeReportInterval.Month)
                    {
                        report = ZnodeReport.CouponUsageByMonth;
                    }
                    else if (interval == ZnodeReportInterval.Quarter)
                    {
                        report = ZnodeReport.CouponUsageByQuarter;
                    }
                    else if (interval == ZnodeReportInterval.Year)
                    {
                        report = ZnodeReport.CouponUsageByYear;
                    }
                    else
                    {
                        report = ZnodeReport.CouponUsage;
                    }
                    
                    break;

                case ZnodeReport.AffiliateOrder:

                    if (interval == ZnodeReportInterval.Day)
                    {
                        report = ZnodeReport.AffiliateOrderByDay;
                    }
                    else if (interval == ZnodeReportInterval.Week)
                    {
                        report = ZnodeReport.AffiliateOrderByWeek;
                    }
                    else if (interval == ZnodeReportInterval.Month)
                    {
                        report = ZnodeReport.AffiliateOrderByMonth;
                    }
                    else if (interval == ZnodeReportInterval.Quarter)
                    {
                        report = ZnodeReport.AffiliateOrderByQuarter;
                    }
                    else if (interval == ZnodeReportInterval.Year)
                    {
                        report = ZnodeReport.AffiliateOrderByYear;
                    }
                    else
                    {
                        report = ZnodeReport.AffiliateOrder;
                    }
                    
                    break;

                case ZnodeReport.EmailOptInCustomer:
                    report = ZnodeReport.EmailOptInCustomer;
                    break;

                case ZnodeReport.Picklist:
                    report = ZnodeReport.Picklist;
                    break;

                case ZnodeReport.ServiceRequest:
                    report = ZnodeReport.ServiceRequest;
                    break;

                case ZnodeReport.ReOrder:
                    report = ZnodeReport.ReOrder;
                    break;
            }
            
            // Return the selected report name
            return report;
        }

        #endregion
    }
}