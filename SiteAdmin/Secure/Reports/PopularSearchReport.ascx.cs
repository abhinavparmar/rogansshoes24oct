﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Reporting.WebForms;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Reports
{
    public partial class PopularSearchReport : System.Web.UI.UserControl
    {
        #region Private Variables

        private static string PageType = string.Empty;
        private string ReportId = string.Empty;
        private string _ReportTitle;
        #endregion

        #region Field Definition

        /// <summary>
        /// Gets or sets the IsPostBack property
        /// </summary>
        public string IsPostback
        {
            get
            {
                return hdnIsPostBack.Value;
            }

            set
            {
                hdnIsPostBack.Value = value;
            }
        }

        /// <summary>
        /// Gets or sets the Mode Property
        /// </summary>
        public ZnodeReport Mode
        {
            get
            {
                string mode = hdnMode.Value;
                ZnodeReport report = ZnodeReport.PopularSearch;
                if (Enum.IsDefined(typeof(ZnodeReport), mode))
                {
                    report = (ZnodeReport)Enum.Parse(typeof(ZnodeReport), hdnMode.Value, true);
                }

                return report;
            }

            set
            {
                hdnMode.Value = value.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the report title
        /// </summary>
        public string ReportTitle
        {
            get
            {
                return this._ReportTitle;
            }

            set
            {
                this._ReportTitle = value;
            }
        }
        #endregion


        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Headers["User-Agent"].Contains("WebKit") || Request.Headers["User-Agent"].Contains("Firefox"))
            {
                objReportViewer.InteractivityPostBackMode = InteractivityPostBackMode.AlwaysSynchronous;
            }
        }

        /// <summary>
        /// Page Pre Render Method
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!Convert.ToBoolean(hdnIsPostBack.Value))
            {
                this.objReportViewer.Visible = false;
                this.IsPostback = "true";
                ListOrderStatus.SelectedIndex = -1; 
            }
        }
        #endregion

        #region Events
       
        /// <summary>
        /// Back Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Reports/default.aspx");
        }

        /// <summary>
        /// Order Filter Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnOrderFilter_Click(object sender, EventArgs e)
        {
            this.ShowReport();
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            //if (this.Mode == ZnodeReport.Orders)
            //{
            //    pnlprofile.Visible = false;
            //}

            //if (this.Mode == ZnodeReport.Accounts)
            //{
            //    pnlprofile.Visible = false;
            //}

            //txtEndDate.Text = string.Empty;
            //txtStartDate.Text = string.Empty;
            //ddlOrderStatus.SelectedIndex = 0;
            //ddlPortal.SelectedIndex = 0;
            // Selected interval.

            ListOrderStatus.SelectedIndex = -1; 
            this.objReportViewer.Visible = false;

        }
        #endregion

        

        /// <summary>
        /// Show Report Method
        /// </summary>
        private void ShowReport()
        {
            objReportViewer.Visible = true;
            DataSet reportDataSet = null;
            ZnodeReport report = this.GetZnodeReport();

            // Get Filetered Orders in DataSet
            ReportAdmin reportAdmin = new ReportAdmin();
            reportDataSet = reportAdmin.ReportList(report, DateTime.Today, DateTime.Today, string.Empty, string.Empty, string.Empty);

            if (reportDataSet.Tables[0].Rows.Count == 0)
            {
                lblErrorMsg.Text = "No records found";
                objReportViewer.Visible = false;
                return;
            }
            else
            {
                if (Request.Headers["User-Agent"].Contains("WebKit") || Request.Headers["User-Agent"].Contains("Firefox"))
                {
                    objReportViewer.InteractivityPostBackMode = InteractivityPostBackMode.AlwaysSynchronous;
                }

                this.objReportViewer.LocalReport.DataSources.Clear();
                this.objReportViewer.LocalReport.ReportPath = "SiteAdmin/Secure/Reports/PopularSearch.rdlc";

                ReportParameter param1 = new ReportParameter("CurrentLanguage", System.Globalization.CultureInfo.CurrentCulture.Name);
                objReportViewer.LocalReport.SetParameters(new ReportParameter[] { param1 });
                objReportViewer.LocalReport.DataSources.Add(new ReportDataSource("ZnodeSEOMost_Search_ZnodeSEOSearch", reportDataSet.Tables[0]));
                this.objReportViewer.PageCountMode = PageCountMode.Actual; 
                objReportViewer.LocalReport.Refresh();
            }
        }

        #region Private Methods
        /// <summary>
        /// Get Znode Report Method
        /// </summary>
        /// <returns>Returns the ZNodeReport</returns>
        private ZnodeReport GetZnodeReport()
        {
            ZnodeReport report = ZnodeReport.None;

            // Selected interval.
            string selectedInterval = ListOrderStatus.SelectedItem.Text;
            ZnodeReportInterval interval = ZnodeReportInterval.None;

            // Convert selected string to ZnodeReportInterval enumeration
            if (Enum.IsDefined(typeof(ZnodeReportInterval), selectedInterval))
            {
                interval = (ZnodeReportInterval)Enum.Parse(typeof(ZnodeReportInterval), selectedInterval, true);
            }

            if (interval == ZnodeReportInterval.None)
            {
                return ZnodeReport.PopularSearch;
            }
            else
            {
                if (interval == ZnodeReportInterval.Day)
                {
                    report = ZnodeReport.PopularSearchByDay;
                }

                if (interval == ZnodeReportInterval.Week)
                {
                    report = ZnodeReport.PopularSearchByWeek;
                }

                if (interval == ZnodeReportInterval.Month)
                {
                    report = ZnodeReport.PopularSearchByMonth;
                }

                if (interval == ZnodeReportInterval.Quarter)
                {
                    report = ZnodeReport.PopularSearchByQuarter;
                }

                if (interval == ZnodeReportInterval.Year)
                {
                    report = ZnodeReport.PopularSearchByYear;
                }
            }

            return report;
        }
        #endregion
    }
}