﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PopularSearchReport.ascx.cs" Inherits="SiteAdmin.Secure.Reports.PopularSearchReport" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>

<div class="ReportLayout">
       <h4 class="SubTitle">
       Store Report
    </h4>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div id="pnlCustom" runat="server" class="SearchForm">
            <div class="RowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle">Get searches for the last</span><br />
                    <span class="ValueStyle">
                        <asp:DropDownList ID="ListOrderStatus" runat="server">
                            <asp:ListItem Value="0" Text="Select"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Day"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Week"></asp:ListItem>
                            <asp:ListItem Value="3" Text="Month"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Quarter"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Year"></asp:ListItem>
                        </asp:DropDownList>
                    </span>
                </div>
            </div>
            <div align="left" class="ClearBoth">
                <br />
            </div>
            <div align="left">
                <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../Themes/Images/buttons/button_clear_highlight.gif';"
                    onmouseout="this.src='../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                    runat="server" AlternateText="Clear" OnClick="BtnClear_Click" />
                <asp:ImageButton ID="btnOrderFilter" onmouseover="this.src='../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" OnClick="BtnOrderFilter_Click" />
            </div>
        </div>
        <div align="right">
            <asp:Button CausesValidation="false" ID="btnBack" CssClass="Button" runat="server"
                Text="<< Previous Page" OnClick="BtnBack_Click" Visible="false" />
        </div>
        <div>
            <uc1:Spacer ID="Spacer3" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div>
            <asp:Label ID="lblErrorMsg" CssClass="Error" runat="server" EnableViewState="false"></asp:Label>
        </div>
        <rsweb:ReportViewer ShowBackButton="False" ID="objReportViewer" runat="server" Width="100%" AsyncRendering="false"
            Font-Names="Verdana" Font-Size="8pt" ShowExportControls="true" ShowPageNavigationControls="true"
            ShowCredentialPrompts="false" ShowDocumentMapButton="false" ShowFindControls="false"
            ShowPrintButton="true" ShowRefreshButton="false" ShowZoomControl="false">
        </rsweb:ReportViewer>
        <div>
            <uc1:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <asp:HiddenField runat="server" ID="hdnIsPostBack" Value="false" />
        <asp:HiddenField runat="server" ID="hdnMode" Value="23" />

    </div>