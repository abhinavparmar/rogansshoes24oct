﻿<%@ Control Language="C#" AutoEventWireup="True" Inherits="SiteAdmin.Admin_Secure_Reports_InventoryReport"
    CodeBehind="InventoryReport.ascx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<div class="ReportLayout">
    <h4 class="SubTitle">
        <asp:Label ID="lblTitle" runat="server"></asp:Label>
    </h4>
    <div>
        <uc1:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <div>
        <div class="SearchForm">
            <div class="RowStyle">
                <asp:PlaceHolder ID="pnlStorelist" runat="server" Visible="false">
                    <div class="ItemStyle" style="margin-right: 35px;">
                        <span class="FieldStyle">Select Store</span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlPortal" runat="server">
                            </asp:DropDownList>
                        </span>
                    </div>
                </asp:PlaceHolder>
                <div class="ItemStyle" style="margin-right: 35px; width: 150px;">
                    <asp:PlaceHolder ID="pnlCustom" runat="server" Visible="false"><span class="FieldStyle">
                        Get report for the last</span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ListOrderStatus" runat="server">
                                <asp:ListItem Value="0" Text="Select"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Day"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Week"></asp:ListItem>
                                <asp:ListItem Value="3" Text="Month"></asp:ListItem>
                                <asp:ListItem Value="4" Text="Quarter"></asp:ListItem>
                                <asp:ListItem Value="5" Text="Year"></asp:ListItem>
                            </asp:DropDownList>
                        </span></asp:PlaceHolder>
                    &nbsp;
                </div>
                <div class="ItemStyle" style="width: 200px; text-align: right; padding-top: 15px;">
                    <asp:PlaceHolder ID="pnlSearch" runat="server" Visible="true">
                        <div class="Buttons">
                            <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../Themes/Images/buttons/button_clear_highlight.gif';"
                                onmouseout="this.src='../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                                runat="server" AlternateText="Clear" OnClick="BtnClear_Click" />
                            <asp:ImageButton ID="btnOrderFilter" onmouseover="this.src='../../Themes/Images/buttons/button_submit_highlight.gif';"
                                onmouseout="this.src='../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                                runat="server" AlternateText="Submit" OnClick="BtnOrderFilter_Click" CausesValidation="true" />
                        </div>
                    </asp:PlaceHolder>
                </div>
            </div>
        </div>
    </div>
    <div align="left" class="ClearBoth">
        <br />
    </div>
    <div class="ClearBoth">
        <uc1:Spacer ID="Spacer3" spacerheight="15" spacerwidth="3" runat="server"></uc1:Spacer>
    </div>
    <div>
        <asp:Label ID="lblErrorMsg" CssClass="Error" runat="server" EnableViewState="false"></asp:Label>
    </div>
    <div>
        <rsweb:ReportViewer ShowBackButton="False" ID="objReportViewer" runat="server" Width="100%" AsyncRendering="false"
            Font-Names="Verdana" Font-Size="8pt" ShowExportControls="true" ShowPageNavigationControls="true"
            ShowCredentialPrompts="false" ShowDocumentMapButton="false" ShowFindControls="false"
            ShowPrintButton="true" ShowRefreshButton="false" ShowZoomControl="false">
            <LocalReport DisplayName="Report">
            </LocalReport>
        </rsweb:ReportViewer>
    </div>
    <div>
        <uc1:Spacer ID="Spacer1" spacerheight="10" spacerwidth="3" runat="server"></uc1:Spacer>
    </div>
    <asp:HiddenField runat="server" ID="hdnIsPostBack" Value="false" />
    <asp:HiddenField runat="server" ID="hdnInventorySelector" Value="false" />
    <asp:HiddenField runat="server" ID="hdnMode" Value="13" />
</div>
