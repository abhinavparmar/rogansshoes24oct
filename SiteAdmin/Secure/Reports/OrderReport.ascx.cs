﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Reporting.WebForms;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_Reports_OrderReport class
    /// </summary>
    public partial class Admin_Secure_Reports_OrderReport : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private DataTable subReportDataSource = new DataTable();
        private string _ReportTitle;
        private bool _OrderStatusSelector = false;
        #endregion

        #region Field Definition

        /// <summary>
        /// Gets or sets the IsPostBack property
        /// </summary>
        public string IsPostback
        {
            get 
            { 
                return hdnIsPostBack.Value; 
            }

            set 
            { 
                hdnIsPostBack.Value = value; 
            }
        }

        /// <summary>
        /// Gets or sets the Mode Property
        /// </summary>
        public ZnodeReport Mode
        {
            get
            {
                string mode = hdnMode.Value;
                ZnodeReport report = ZnodeReport.ServiceRequest;
                if (Enum.IsDefined(typeof(ZnodeReport), mode))
                {
                    report = (ZnodeReport)Enum.Parse(typeof(ZnodeReport), hdnMode.Value, true);
                }
                
                return report;
            }
            
            set 
            { 
                hdnMode.Value = value.ToString(); 
            }
        }

        /// <summary>
        /// Gets or sets the report title
        /// </summary>
        public string ReportTitle
        {
            get 
            { 
                return this._ReportTitle; 
            }

            set
            {
                this._ReportTitle = value;
                lblTitle.Text = value;
            }
        }

       /// <summary>
       /// Gets or sets a value indicating whether OrderStatusSelector is selected or not
       /// </summary>
        public bool OrderStatusSelector
        {
            get 
            { 
                return this._OrderStatusSelector; 
            }
            
            set
            {
                this._OrderStatusSelector = value;
                pnlOrderStatus.Visible = this._OrderStatusSelector;
            }
        }
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Headers["User-Agent"].Contains("WebKit") || Request.Headers["User-Agent"].Contains("Firefox"))
            {
                objReportViewer.InteractivityPostBackMode = InteractivityPostBackMode.AlwaysSynchronous;
            }
        }
        
        /// <summary>
        /// Page Pre Render Method
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!Convert.ToBoolean(hdnIsPostBack.Value))
            {
                this.objReportViewer.Visible = false;
                this.IsPostback = "true";
                this.BindOrderState();

                this.BindStores();
                if (this.Mode == ZnodeReport.Accounts)
                {
                    txtStartDate.Text = string.Empty;
                    txtEndDate.Text = string.Empty;
                }
                if (this.Mode == ZnodeReport.Orders)
                {
                    txtStartDate.Text = DateTime.Now.AddDays(-30).ToString("MM/dd/yyyy");
                    txtEndDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                   // ShowReport();
                }
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Report Viewer - Sub Report processing event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("ZNodeOrderLineItems_ZNodeOrderLineItem", this.subReportDataSource));
        }

        /// <summary>
        /// Back Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Reports/default.aspx");
        }

        /// <summary>
        /// Order Filter Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnOrderFilter_Click(object sender, EventArgs e)
        {
            this.ShowReport();
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            if (this.Mode == ZnodeReport.Orders)
            {
                pnlprofile.Visible = false;
            }
            
            if (this.Mode == ZnodeReport.Accounts)
            {
                pnlprofile.Visible = false;
            }

            txtEndDate.Text = string.Empty;
            txtStartDate.Text = string.Empty;
            ddlOrderStatus.SelectedIndex = 0;
            ddlPortal.SelectedIndex = 0;
            this.objReportViewer.Visible = false;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Bind Stores Method
        /// </summary>
        private void BindStores()
        {
            // Load StoresList
            PortalAdmin portalAdmin = new PortalAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Portal> portals = portalAdmin.GetAllPortals();

            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (profiles.StoreAccess != "AllStores")
            {
                portals.Filter = string.Concat("PortalID IN [", profiles.StoreAccess, "]");
            }
            else
            {
                Portal po = new Portal();
                po.StoreName = "ALL STORES";
                portals.Insert(0, po);
            }

            // Portal Drop Down List
            ddlPortal.DataSource = portals;
            ddlPortal.DataTextField = "StoreName";
            ddlPortal.DataValueField = "PortalID";
            ddlPortal.DataBind();

            foreach (ListItem listItem in ddlPortal.Items)
            {
                listItem.Text = Server.HtmlDecode(listItem.Text);
            }
          
        }

        /// <summary>
        /// Bind Order State Method
        /// </summary>
        private void BindOrderState()
        {
            // Load Order State Item 
            OrderAdmin _OrderAdminAccess = new OrderAdmin();
            ddlOrderStatus.DataSource = _OrderAdminAccess.GetAllOrderStates();
            ddlOrderStatus.Items.Clear();
            ddlOrderStatus.DataTextField = "OrderStateName";
            ddlOrderStatus.DataValueField = "OrderStateId";
            ddlOrderStatus.DataBind();
            ListItem item1 = new ListItem("ALL", "0");
            ddlOrderStatus.Items.Insert(0, item1);
            ddlOrderStatus.SelectedIndex = 0;
        }

        /// <summary>
        /// Show Report Method
        /// </summary>
        private void ShowReport()
        {
            // Get Filetered Orders in DataSet
            ReportAdmin reportAdmin = new ReportAdmin();

            string orderStatus = ddlOrderStatus.SelectedValue;
            string portalId = ddlPortal.SelectedValue;

            DateTime startDate = DateTime.MinValue;
            DateTime endDate = DateTime.MinValue;
            if (!string.IsNullOrEmpty(txtStartDate.Text))
            {
                startDate = DateTime.Parse(txtStartDate.Text.Trim());
            }

            if (!string.IsNullOrEmpty(txtEndDate.Text))
            {
                endDate = DateTime.Parse(txtEndDate.Text.Trim());
            }

            DataSet reportDataSet = null;
            objReportViewer.Reset();
            objReportViewer.LocalReport.DataSources.Clear();
            this.objReportViewer.PageCountMode = PageCountMode.Actual; 
            if (this.Mode == ZnodeReport.Orders)
            {
                // Get order header DataSet
                reportDataSet = reportAdmin.ReportList(ZnodeReport.Orders, startDate, endDate, string.Empty, portalId, orderStatus);

                // Get order line item DataSet
                this.subReportDataSource = reportDataSet.Tables[1];
                this.objReportViewer.LocalReport.ReportPath = "SiteAdmin/Secure/Reports/Orders.rdlc";
                objReportViewer.LocalReport.DataSources.Add(new ReportDataSource("ZNodeOrderDataSet_ZNodeOrder", reportDataSet.Tables[0]));
                this.objReportViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(this.LocalReport_SubreportProcessing);
                            
            }
            else if (this.Mode == ZnodeReport.Accounts)
            {
                // Custom1 parameter used here to send store name
                reportDataSet = reportAdmin.ReportList(ZnodeReport.Accounts, startDate, endDate, string.Empty, portalId, string.Empty);
                this.objReportViewer.LocalReport.ReportPath = "SiteAdmin/Secure/Reports/Accounts.rdlc";
                objReportViewer.LocalReport.DataSources.Add(new ReportDataSource("ZNodeAccountDataSet_ZNodeAccount", reportDataSet.Tables[0]));
            }

            if (reportDataSet.Tables[0].Rows.Count == 0)
            {
                lblErrorMsg.Text = "No records found";
                objReportViewer.Visible = false;
                return;
            }
            else
            {
                this.objReportViewer.Visible = true;
                ReportParameter param1 = new ReportParameter("CurrentLanguage", System.Globalization.CultureInfo.CurrentCulture.Name);
                objReportViewer.LocalReport.SetParameters(new ReportParameter[] { param1 }); 
                objReportViewer.LocalReport.Refresh();
            }
        }
        #endregion
    }
}