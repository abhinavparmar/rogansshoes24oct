﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RecurringBillingReport.ascx.cs" Inherits="SiteAdmin.Admin_Secure_Reports_RecurringBillingReport" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/Profilelist.ascx" TagName="ProfileList"
    TagPrefix="ZNode" %>
<div class="ReportLayout">
    <h4 class="SubTitle">
        <asp:Label ID="lblTitle" runat="server"></asp:Label>
    </h4>
    <div>
        <uc1:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <asp:Panel ID="pnlCustom" runat="server">
        <div class="SearchForm">
            <div class="RowStyle">
                <div class="ItemStyle" style="width: 140px">
                    <span class="FieldStyle">Begin Date </span>
                    <br />
                    <span class="ValueStyle">
                        <asp:TextBox ID="txtStartDate" CssClass="Size100" Text='' runat="server" />&nbsp;<asp:ImageButton
                            ID="imgbtnStartDt" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/SiteAdmin/Themes/images/SmallCalendar.gif" />
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="<br />Enter Begin date"
            ControlToValidate="txtStartDate" ValidationGroup="grpReports" CssClass="Error"
            Text="" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtStartDate"
            CssClass="Error" Display="Dynamic" ErrorMessage="Enter Valid Date in <br />MM/DD/YYYY format"
            Text="" ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"
            ValidationGroup="grpReports"></asp:RegularExpressionValidator>
                    </span>
                </div>
                <div class="ItemStyle" style="width: 140px">
                    <span class="FieldStyle">End Date</span><br />
                    <span class="ValueStyle">
                        <asp:TextBox ID="txtEndDate" Text='' CssClass="Size100" runat="server" />&nbsp;<asp:ImageButton
                            ID="ImgbtnEndDt" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/SiteAdmin/Themes/images/SmallCalendar.gif" /><br />
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEndDate"
            Text="" ErrorMessage="Enter End date" ValidationGroup="grpReports" CssClass="Error"
            Display="Dynamic"></asp:RequiredFieldValidator>
    
        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEndDate"
            CssClass="Error" Display="Dynamic" ErrorMessage="Enter Valid Date in<br /> MM/DD/YYYY format"
            Text="" ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"
            ValidationGroup="grpReports"></asp:RegularExpressionValidator>
                    </span>
                </div>
                <div class="ItemStyle" style="width: 130px; position: relative;">
                    <span class="FieldStyle">Store Name</span><br />
                    <span class="ValueStyle">
                        <asp:DropDownList ID="ddlPortal" Width="150px" runat="server">
                        </asp:DropDownList>
                    </span>
                </div>
                <div class="ItemStyle" style="padding-left: 50px; width: 100px">
                    <asp:PlaceHolder ID="pnlOrderStatus" runat="server"><span class="FieldStyle">Order State</span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlOrderStatus" runat="server" />
                        </span></asp:PlaceHolder>
                </div>
                <div class="ItemStyle" visible="false">
                    <span class="FieldStyle">
                        <asp:Label ID="lblProfileName" runat="server" Visible="false"></asp:Label>
                    </span><span class="ValueStyle">
                        <asp:PlaceHolder ID="pnlprofile" runat="server" Visible="false"><span class="ValueStyle">
                            <ZNode:ProfileList ID="ProfileList" runat="server"></ZNode:ProfileList>
                        </span></asp:PlaceHolder>
                    </span>
                </div>
            </div>
        </div>
        <div class="ClearBoth">
        </div>
        <div class="Buttons">
            <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../Themes/Images/buttons/button_clear_highlight.gif';"
                onmouseout="this.src='../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                runat="server" AlternateText="Clear" OnClick="BtnClear_Click" />
            <asp:ImageButton ID="btnOrderFilter" onmouseover="this.src='../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnOrderFilter_Click" CausesValidation="true"
                ValidationGroup="grpReports" />
        </div>
    </asp:Panel>
    <div class="ClearBoth">
        <ajaxToolKit:CalendarExtender ID="CalendarExtender1" Enabled="true" PopupButtonID="imgbtnStartDt"
            PopupPosition="TopLeft" runat="server" TargetControlID="txtStartDate">
        </ajaxToolKit:CalendarExtender>
       <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtStartDate"
            Text="" ControlToValidate="txtEndDate" CssClass="Error" Display="Dynamic" ErrorMessage="End Date must be greater than Begin date"
            Operator="GreaterThanEqual" Type="Date" ValidationGroup="grpReports"></asp:CompareValidator>
      
        <ajaxToolKit:CalendarExtender ID="CalendarExtender2" Enabled="true" PopupButtonID="ImgbtnEndDt"
            PopupPosition="TopLeft" runat="server" TargetControlID="txtEndDate">
        </ajaxToolKit:CalendarExtender>
       </div>
    <div>
        <uc1:Spacer ID="Spacer3" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <div>
        <asp:Label ID="lblErrorMsg" CssClass="Error" runat="server" EnableViewState="false"></asp:Label>
    </div>
    <div>
        <rsweb:ReportViewer ShowBackButton="False" ID="objReportViewer" runat="server" Width="100%" AsyncRendering="false"
            Font-Names="Verdana" Font-Size="8pt" ShowExportControls="true" ShowPageNavigationControls="true"
            ShowCredentialPrompts="false" ShowDocumentMapButton="false" ShowFindControls="false"
            ShowPrintButton="true" ShowRefreshButton="false" ShowZoomControl="false">
        </rsweb:ReportViewer>
    </div>
    <div>
        <uc1:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <asp:HiddenField runat="server" ID="hdnIsPostBack" Value="false" />
    <asp:HiddenField runat="server" ID="hdnMode" Value="12" />
</div>
