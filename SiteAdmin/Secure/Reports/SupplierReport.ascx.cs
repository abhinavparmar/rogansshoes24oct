﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Reporting.WebForms;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_Reports_CSupplierReport class
    /// </summary>
    public partial class Admin_Secure_Reports_CSupplierReport : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private DataTable subReportDataSource = new DataTable();
        private string Year = string.Empty;
        private string _ReportTitle;

        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the IsPostBack
        /// </summary>
        public string IsPostback
        {
            get 
            { 
                return hdnIsPostBack.Value; 
            }

            set 
            { 
                hdnIsPostBack.Value = value; 
            }
        }

        /// <summary>
        /// Gets or sets the ReportTitle
        /// </summary>
        public string ReportTitle
        {
            get 
            { 
                return this._ReportTitle; 
            }

            set
            {
                this._ReportTitle = value;
                lblTitle.Text = value;
            }
        }

        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Headers["User-Agent"].Contains("WebKit") || Request.Headers["User-Agent"].Contains("Firefox"))
            {
                objReportViewer.InteractivityPostBackMode = InteractivityPostBackMode.AlwaysSynchronous;
            }
        }

        /// <summary>
        /// Page Pre Render Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!Convert.ToBoolean(hdnIsPostBack.Value))
            {
                hdnIsPostBack.Value = "true";
                this.BindStores();
                this.objReportViewer.Visible = false;
            }
        }
        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Report Viewer - Sub Report processing event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("ZNodeSupplier_OrderLineItem", this.subReportDataSource));
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            hdnIsPostBack.Value = "false";
            txtEndDate.Text = string.Empty;
            txtStartDate.Text = string.Empty;
            ddlPortal.SelectedIndex = -1;
            ddlSupplier.Value = "0";
            this.objReportViewer.Visible = false;

        }

        /// <summary>
        /// Get Order details click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnOrderFilter_Click(object sender, EventArgs e)
        {
            this.ShowReport();
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Bind Report Method
        /// </summary>
        private void ShowReport()
        {
            objReportViewer.Visible = true;
            lblTitle.Text = "Supplier List";
            
            // Get Filetered Orders in DataSet
            ReportAdmin reportAdmin = new ReportAdmin();

            string supplierId = ddlSupplier.Value;
            string portalId = ddlPortal.SelectedValue;
            DataSet reportDataSet = null;
            DateTime startDate = DateTime.MinValue;
            DateTime endDate = DateTime.MinValue;

            if (!string.IsNullOrEmpty(txtStartDate.Text))
            {
                startDate = DateTime.Parse(txtStartDate.Text.Trim());
            }

            if (!string.IsNullOrEmpty(txtEndDate.Text))
            {
                endDate = DateTime.Parse(txtEndDate.Text.Trim());
            }

            reportDataSet = reportAdmin.ReportList(ZnodeReport.SupplierList, startDate, endDate, supplierId, portalId, string.Empty);

            if (reportDataSet.Tables[0].Rows.Count == 0)
            {
                lblErrorMsg.Text = "No records found";
                objReportViewer.Visible = false;
                return;
            }
            else
            {
                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.ActivityLogReport, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Supplier - Report", "Supplier");

                this.subReportDataSource = reportDataSet.Tables[1];
                objReportViewer.Visible = true;
                objReportViewer.LocalReport.DataSources.Clear();
                this.objReportViewer.LocalReport.ReportPath = "SiteAdmin/Secure/Reports/SupplierReport.rdlc";
                ReportParameter param1 = new ReportParameter("CurrentLanguage", System.Globalization.CultureInfo.CurrentCulture.Name);
                objReportViewer.LocalReport.SetParameters(new ReportParameter[] { param1 });
                objReportViewer.LocalReport.DataSources.Add(new ReportDataSource("ZNodeSupplier_Order", reportDataSet.Tables[0]));
                this.objReportViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(this.LocalReport_SubreportProcessing);
                this.objReportViewer.PageCountMode = PageCountMode.Actual; 
                objReportViewer.LocalReport.Refresh();
            }
        }

        /// <summary>
        /// Bind store names
        /// </summary>
        private void BindStores()
        {
            // Load StoresList
            PortalAdmin portalAdmin = new PortalAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Portal> portals = portalAdmin.GetAllPortals();

            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (profiles.StoreAccess != "AllStores")
            {
                portals.Filter = string.Concat("PortalID IN [", profiles.StoreAccess, "]");
            }
            else
            {
                Portal po = new Portal();
                po.StoreName = "ALL STORES";
                portals.Insert(0, po);
            }

            // Portal Drop Down List
            ddlPortal.DataSource = portals;
            ddlPortal.DataTextField = "StoreName";
            ddlPortal.DataValueField = "PortalID";
            ddlPortal.DataBind();

            foreach (ListItem listItem in ddlPortal.Items)
            {
                listItem.Text = Server.HtmlDecode(listItem.Text);
            }
        }

        #endregion
    }
}