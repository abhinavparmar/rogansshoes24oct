﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master"
    AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SiteAdmin.Secure.Help.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <div class="LeftMargin">
        <h1>
            Help</h1>
        <hr />
        <div class="ImageAlign">
            <h1>
                Merchants</h1>
            <br />
            <div class="Shortcuts">
                <a href="http://www.znode.com/downloads/7.0/Znode_multifront_merchant_guide_7.0.pdf" target="_blank">Quick Start Guide - Merchants</a>
            </div>
        </div>       
    </div>
</asp:Content>
