using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Orders.SupplierManagement.Suppliers
{
    /// <summary>
    /// Represents the Site Admin - Secure.Orders.SupplierManagement.Suppliers.Add class
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId = 0;
        #endregion

        #region Bind Data

        /// <summary>
        /// Bind Edit Datas
        /// </summary>
        public void BindEditDatas()
        {
            SupplierAdmin supplierAdmin = new SupplierAdmin();
            ZNode.Libraries.DataAccess.Entities.Supplier supplier = supplierAdmin.GetBySupplierID(this.ItemId);

            if (supplier != null)
            {
                Name.Text = supplier.Name;
                Description.Text = supplier.Description;
                ContactFirstName.Text = supplier.ContactFirstName;
                ContactLastName.Text = supplier.ContactLastName;
                ContactPhone.Text = supplier.ContactPhone;
                EmailId.Text = supplier.ContactEmail;
                NotifyEmail.Text = supplier.NotificationEmailID;
                NotificationTemplate.Text = supplier.EmailNotificationTemplate;
                ChkEmailNotify.Checked = supplier.EnableEmailNotification;
                DisplayOrder.Text = supplier.DisplayOrder.ToString();
                CheckActiveInd.Checked = supplier.ActiveInd;
                txtCustom1.Text = supplier.Custom1;
                txtCustom2.Text = supplier.Custom2;
                txtCustom3.Text = supplier.Custom3;
                txtCustom4.Text = supplier.Custom4;
                txtCustom5.Text = supplier.Custom5;
                txtExternalSupplierNo.Text = supplier.ExternalSupplierNo;
                ddlSupplierTypes.SelectedValue = supplier.SupplierTypeID.GetValueOrDefault().ToString();
            }
        }

        /// <summary>
        /// Bind supplier types
        /// </summary>
        public void Bind()
        {
            SupplierAdmin supplierAdmin = new SupplierAdmin();
            TList<SupplierType> supplierRuleTypeList = supplierAdmin.GetSupplierTypes();
            ddlSupplierTypes.DataSource = supplierRuleTypeList.FindAll(SupplierTypeColumn.ActiveInd, true);
            ddlSupplierTypes.DataTextField = "Name";
            ddlSupplierTypes.DataValueField = "SupplierTypeID";
            ddlSupplierTypes.DataBind();
        }

        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }
            
            if (!Page.IsPostBack)
            {
                this.Bind();

                if (this.ItemId > 0)
                {
                    this.BindEditDatas();
                    lblTitle.Text = "Edit Supplier";
                }
                else
                {
                    lblTitle.Text = "Add New Supplier";
                }
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Concate firstname and lastname 
        /// </summary>
        /// <param name="FirstName">The value of First Name</param>
        /// <param name="LastName">The value of Last Name</param>
        /// <returns>Returns the concatenated value of firstname and lastname</returns>
        protected string ConcatName(object FirstName, object LastName)
        {
            string fullName = string.Concat(FirstName, " ", LastName);
            return fullName;
        }

        /// <summary>
        /// Add Account Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddAccount_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Orders/SupplierManagement/Suppliers/Addaccount.aspx?supplier=" + this.ItemId);
        }
        
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            SupplierAdmin supplierAdmin = new SupplierAdmin();
            Supplier supplier = new Supplier();

            if (this.ItemId > 0)
            {
                supplier = supplierAdmin.GetBySupplierID(this.ItemId);
            }

            supplier.Name = Name.Text.Trim();
            supplier.Description = Description.Text.Trim();
            supplier.ContactFirstName = ContactFirstName.Text.Trim();
            supplier.ContactLastName = ContactLastName.Text.Trim();
            supplier.ContactPhone = ContactPhone.Text;
            supplier.ContactEmail = EmailId.Text;
            supplier.NotificationEmailID = NotifyEmail.Text;
            supplier.EmailNotificationTemplate = NotificationTemplate.Text;
            supplier.EnableEmailNotification = ChkEmailNotify.Checked;
            supplier.DisplayOrder = int.Parse(DisplayOrder.Text.Trim());
            supplier.ActiveInd = CheckActiveInd.Checked;
            supplier.Custom1 = txtCustom1.Text.Trim();
            supplier.Custom2 = txtCustom2.Text.Trim();
            supplier.Custom3 = txtCustom3.Text.Trim();
            supplier.Custom4 = txtCustom4.Text.Trim();
            supplier.Custom5 = txtCustom5.Text.Trim();
            supplier.ExternalSupplierNo = txtExternalSupplierNo.Text.Trim();
            if (ddlSupplierTypes.SelectedIndex != -1)
            {
                supplier.SupplierTypeID = int.Parse(ddlSupplierTypes.SelectedValue);
            }

            bool check = false;

            if (this.ItemId > 0)
            {
                check = supplierAdmin.Update(supplier);
                
                // Log Activity
                string AssociateName = "Edit of Supplier - " + Name.Text;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, Name.Text);
            }
            else
            {
                check = supplierAdmin.Insert(supplier);
                
                // Log Activity
                string AssociateName = "Creation of Supplier - " + Name.Text;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, Name.Text);
            }

            if (check)
            {
                System.Web.HttpContext.Current.Session["SupplierList"] = null;
                
                // Redirect to main page
                Response.Redirect("Default.aspx");
            }
            else
            {
                // Display error message
                lblError.Text = "An error occurred while updating. Please try again.";
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
        #endregion
    }
}