<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Orders.SupplierManagement.Suppliers.Add"
    ValidateRequest="false" CodeBehind="Add.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
                <uc2:DemoMode ID="DemoMode1" runat="server"></uc2:DemoMode>
            </h1>
        </div>
        <div style="text-align: right">
            <asp:ImageButton ID="btnSubmitTop" runat="server" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                OnClick="BtnSubmit_Click" onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';"
                onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';" AlternateText="Submit"/>
            <asp:ImageButton ID="btnCancelTop" runat="server" CausesValidation="false" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                OnClick="BtnCancel_Click" onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';"
                onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';" AlternateText="Cancel"/>
        </div>
        <div class="ClearBoth">
             <uc1:Spacer ID="Spacer5" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        </div>

        <div class="FormView">
            <div class="FieldStyle">
                Supplier Name<span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID='Name' runat='server' MaxLength="50" Columns="50"></asp:TextBox><asp:RequiredFieldValidator
                    ID="RequiredFieldValidator1" CssClass="Error" runat="server" ControlToValidate="Name" Display="Dynamic"
                    ErrorMessage="* Enter Supplier Name" SetFocusOnError="True"></asp:RequiredFieldValidator>
            </div>
            <div class="FieldStyle">
                Supplier Code
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID='txtExternalSupplierNo' runat='server' MaxLength="100" Columns="50"></asp:TextBox>
            </div>

            <div class="FieldStyle">
                Description
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="Description" runat="server" Rows="10" MaxLength="4000" TextMode="MultiLine"
                    Columns="50"></asp:TextBox>
            </div>
            <div class="ClearBoth">
            </div>
            <h4 class="SubTitle">Supplier Contact</h4>
            <div class="FieldStyle">
                Contact First Name
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="ContactFirstName" runat="server"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                Contact Last Name
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="ContactLastName" runat="server"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                Contact Phone
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="ContactPhone" runat="server"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                Contact Email
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID='EmailId' runat='server' MaxLength="50" Columns="50"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="EmailId"
                    CssClass="Error" Display="Dynamic" ErrorMessage="Enter Valid EmailId" SetFocusOnError="True"
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            </div>
            <div class="ClearBoth">
            </div>
            <h4 class="SubTitle">Supplier Notification</h4>
            <div class="FieldStyle">
                Supplier Notification Method
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlSupplierTypes" runat="server">
                </asp:DropDownList>
            </div>
            <div class="FieldStyle">
                Notification Email<br />
                <small>Send order notifications to this email. Separate multiple emails with commas.
                </small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID='NotifyEmail' runat='server' Columns="50" TextMode="MultiLine"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="NotifyEmail"
                    CssClass="Error" Display="Dynamic" ErrorMessage="Enter Valid EmailId" SetFocusOnError="True"
                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*"></asp:RegularExpressionValidator>
            </div>
            <div class="FieldStyle">
                Email Notification Template<br />
                <small>Enter the XSL email template to use when sending an order to this supplier. An
                    example template can be found at Data/Default/Config/Receipt.xsl</small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="NotificationTemplate" runat="server" Rows="15" Columns="50" TextMode="MultiLine"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                Display Order<span class="Asterix">*</span><br />

            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="DisplayOrder" runat="server" MaxLength="9" Columns="5">500</asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="DisplayOrder"
                    CssClass="Error" Display="Dynamic" ErrorMessage="Enter Display Order"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator3" runat="server" CssClass="Error" ControlToValidate="DisplayOrder"
                    Display="Dynamic" ErrorMessage="Enter Whole number." MaximumValue="999999999"
                    MinimumValue="1" Type="Integer"></asp:RangeValidator>
            </div>
            <div>
                <uc1:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
                <asp:CheckBox ID="ChkEmailNotify" runat="server" Text="Enable Email Notification?"
                    CssClass="FieldStyle" Checked="true" Visible="false" />
            </div>
            <div class="FieldStyle">
            </div>
            <div class="ValueStyleText">
                <asp:CheckBox ID="CheckActiveInd" runat="server" Text="Enable this Supplier" CssClass="FieldStyle"
                    Checked="true" />
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </div>
            <div class="ClearBoth">
                <uc1:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
            </div>
            <h4 class="SubTitle">Custom Information</h4>
            <div class="FieldStyle">
                Custom1
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtCustom1" runat="server" TextMode="MultiLine" Width="400" Height="100"
                    MaxLength="2"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                Custom2
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtCustom2" runat="server" TextMode="MultiLine" Width="400" Height="100"
                    MaxLength="2"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                Custom3
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtCustom3" runat="server" TextMode="MultiLine" Width="400" Height="100"
                    MaxLength="2"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                Custom4
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtCustom4" runat="server" Height="100" MaxLength="2" TextMode="MultiLine"
                    Width="400"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                Custom5
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtCustom5" runat="server" Height="100" MaxLength="2" TextMode="MultiLine"
                    Width="400"></asp:TextBox>
            </div>
        </div>
        <div class="ClearBoth">
        </div>
        <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>

        <div class="ClearBoth">
            <asp:ImageButton ID="btnSubmit" runat="server" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                OnClick="BtnSubmit_Click" onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';"
                onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';" AlternateText="Submit"/>
            <asp:ImageButton ID="btnCancel" runat="server" CausesValidation="false" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                OnClick="BtnCancel_Click" onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';"
                onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';" AlternateText="Cancel"/>
        </div>
    </div>
</asp:Content>
