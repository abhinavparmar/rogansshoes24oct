using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Orders.SupplierManagement.Suppliers
{
    /// <summary>
    /// Represents the SiteAdmin - Secure.Orders.SupplierManagement.Suppliers.Default class
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Protected Member Variables
        private string CatalogImagePath = string.Empty;
        private string AddLink = "~/SiteAdmin/Secure/Orders/SupplierManagement/Suppliers/Add.aspx";
        private int ItemId = 0;
        private DataSet MyDataSet = null;
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["ItemID"] != null)
            {
                this.ItemId = int.Parse(Request.Params["ItemID"].ToString());
            }
            
            if (!Page.IsPostBack)
            {
                this.BindGridData();
            }
            
            lblError.Visible = false;
        }
        #endregion

        #region General Events

        /// <summary>
        /// Represents the Add Supplier Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddSupplier_Click(object sender, EventArgs e)
        {
            // Redirect to Add Supplier Page
            Response.Redirect(this.AddLink);
        }

        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.MyDataSet = this.BindSearchData();
            DataView dv = new DataView(this.MyDataSet.Tables[0]);
            dv.Sort = "DisplayOrder Asc";
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClearSearch_Click(object sender, EventArgs e)
        {
            txtName.Text = string.Empty;
            ddlSupplierStatus.SelectedValue = string.Empty;
            this.BindGridData();
        }
        #endregion

        #region Grid Events

        /// <summary>
        /// Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGridData();
        }

        /// <summary>
        /// Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // Property to an Integer.
                int Id = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName == "Edit")
                {
                    this.AddLink = this.AddLink + "?itemid=" + Id.ToString();
                    Response.Redirect(this.AddLink);
                }
                else if (e.CommandName == "Delete")
                {
                    ZNode.Libraries.Admin.SupplierAdmin adminaccess = new ZNode.Libraries.Admin.SupplierAdmin();
                    bool Check = false;

                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                    HiddenField hdnSupplier = (HiddenField)row.FindControl("hdnSupplierName");
                    string SupplierName = hdnSupplier.Value;

                    string AssociateName = "Delete Supplier -  " + SupplierName;

                    Check = adminaccess.Delete(int.Parse(Id.ToString()));

                    if (Check)
                    {
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, SupplierName);

                        this.BindGridData();
                    }
                    else
                    {
                        lblError.Visible = true;
                        lblError.Text = "Delete action could not be completed because the supplier is in use.";
                    }
                }
            }
        }
        
        /// <summary>
        /// Grid Row Deleting Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindGridData();
        }

        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindGridData()
        {
            ZNode.Libraries.Admin.SupplierAdmin supplierAdmin = new SupplierAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Supplier> supplier = supplierAdmin.GetAll();

            if (supplier != null)
            {
                supplier.Sort("Name Asc");
            }

            if (supplier.Count > 0)
            {
                foreach (Supplier supplierList in supplier)
                {
                    supplierList.Name = Server.HtmlEncode(supplierList.Name);
                }
            }

            uxGrid.DataSource = supplier;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Binds Search Data
        /// </summary>
        /// <returns>Returns the DataSet</returns>
        private DataSet BindSearchData()
        {
            SupplierAdmin AdminAccess = new SupplierAdmin();
            DataSet ds = AdminAccess.SearchSupplier(txtName.Text, ddlSupplierStatus.SelectedValue);
            return ds;
        }

        #endregion
    }
}