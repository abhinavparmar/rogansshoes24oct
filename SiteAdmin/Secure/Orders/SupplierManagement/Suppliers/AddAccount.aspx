<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Orders.SupplierManagement.Suppliers.AddAccount"
    ValidateRequest="false" CodeBehind="AddAccount.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/SupplierAutoComplete.ascx" TagName="SupplierAutoComplete"
    TagPrefix="ZNode" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView" align="center">
        <div>
            <div class="LeftFloat" style="width: 30%; text-align: left">
                <h1>
                    <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
                <uc2:DemoMode ID="DemoMode1" runat="server" />
            </div>
            <div style="float: right">
                <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
                <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
            </div>
            <div class="ClearBoth">
            </div>
            <div align="left">
                <div>
                    <asp:Label ID="lblErrorMsg" runat="server" CssClass="Error" EnableViewState="false"></asp:Label>
                </div>
                <div>
                    <uc1:Spacer ID="Spacer8" SpacerHeight="2" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <h4 class="SubTitle">Login Information</h4>
                <div class="FieldStyle">
                    User Id
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="UserID" runat="server" Columns="40" Width="150px" />
                </div>
                <div class="FieldStyle">
                    Password
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="Password" ValidationGroup="PasswordField" runat="server" />
                    <div>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="Password"
                            Display="Dynamic" ErrorMessage="New Password length should be minimum 8" SetFocusOnError="True"
                            ToolTip="New Password length should be minimum 8 and should contain at least 1 number."
                            ValidationExpression="(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,})" ValidationGroup="EditContact"
                            CssClass="Error">Password must be 8 or more characters and should contain only alphanumeric values with at least 1 numeric character.</asp:RegularExpressionValidator>
                    </div>
                    <asp:Label ID="lblPwdErrorMsg" runat="server" CssClass="Error" EnableViewState="false"></asp:Label>
                </div>
                <div class="FieldStyle">
                    <asp:Label ID="lblSecretQuestion" runat="server" AssociatedControlID="ddlSecretQuestions">Security Question</asp:Label>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlSecretQuestions" runat="server">
                        <asp:ListItem Enabled="true" Selected="True" Text="What is the name of your favorite pet?"></asp:ListItem>
                        <asp:ListItem Enabled="true" Text="In what city were you born?"></asp:ListItem>
                        <asp:ListItem Enabled="true" Text="What high school did you attend?"></asp:ListItem>
                        <asp:ListItem Enabled="true" Text="What is your favorite movie?"></asp:ListItem>
                        <asp:ListItem Enabled="true" Text="What is your mother's maiden name?"></asp:ListItem>
                        <asp:ListItem Enabled="true" Text="What was the make of your first car?"></asp:ListItem>
                        <asp:ListItem Enabled="true" Text="What is your favorite color?"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="FieldStyle">
                    <asp:Label ID="AnswerLabel" runat="server" AssociatedControlID="Answer">Security Answer</asp:Label>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="Answer" runat="server"></asp:TextBox>
                    <asp:Label ID="lblAnswerErrorMsg" runat="server" CssClass="Error" EnableViewState="false"></asp:Label>
                </div>
                <div class="ClearBoth">
                    <div id="tblBillingAddr" runat="server" class="LeftFloat">
                        <h4 class="SubTitle">Billing Address</h4>
                        <div class="FieldStyle">
                            First Name
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingFirstName" runat="server" Width="130" Columns="30" MaxLength="100" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtBillingFirstName"
                                ValidationGroup="EditContact" ErrorMessage="First Name required." CssClass="Error"
                                Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <div class="FieldStyle">
                            Last Name
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingLastName" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtBillingLastName"
                                ValidationGroup="EditContact" ErrorMessage="Last Name required." CssClass="Error"
                                Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="Error" DisplayMode="List"
                                ShowMessageBox="True" ShowSummary="False" ValidationGroup="EditContact" />
                        </div>
                        <div class="FieldStyle">
                            Company Name
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingCompanyName" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox>
                        </div>
                        <div class="FieldStyle">
                            Phone Number
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingPhoneNumber" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox>
                        </div>
                        <div class="FieldStyle">
                            Email Address
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingEmail" runat="server" Width="131px"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="regemailID" runat="server" ControlToValidate="txtBillingEmail"
                                ErrorMessage="*Please use a valid email address." Display="Dynamic" ValidationExpression="[\w\.-]+(\+[\w-]*)?@([\w-]+\.)+[\w-]+"
                                ValidationGroup="EditContact" CssClass="Error"></asp:RegularExpressionValidator>
                        </div>
                        <div class="FieldStyle">
                            Street 1
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingStreet1" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox>
                        </div>
                        <div class="FieldStyle">
                            Street 2
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingStreet2" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox>
                        </div>
                        <div class="FieldStyle">
                            City
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingCity" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox>
                        </div>
                        <div class="FieldStyle">
                            State
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingState" runat="server" Width="30" Columns="10" MaxLength="2"></asp:TextBox>
                        </div>
                        <div class="FieldStyle">
                            Postal Code
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingPostalCode" runat="server" Width="130" Columns="30" MaxLength="20"></asp:TextBox>
                        </div>
                        <div class="FieldStyle">
                            Country
                        </div>
                        <div class="ValueStyle">
                            <asp:DropDownList ID="lstBillingCountryCode" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <uc1:Spacer ID="Spacer1" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
                <div class="ClearBoth">
                    <br />
                    <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                        runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
                    <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                        runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
