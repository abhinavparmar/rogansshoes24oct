<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Orders.OrderManagement.ViewOrders.OrderStatus" CodeBehind="OrderStatus.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <h1>Update Order Status</h1>
        <div style="margin-bottom: 20px;">
            <p>
                Update your order status after shipment, cancellation or returns. Customer may get notifications on status changes.
            </p>
        </div>
        <div style="margin-bottom: 20px;">
            <div class="BottomPixel">
                <b>Order ID: </b>
                <asp:Label ID="lblOrderID" runat="server" />
            </div>
             
            <div class="BottomPixel">
                <b>Customer Name: </b>
                <asp:Label ID="lblCustomerName" runat="server" />
            </div>
           
            <div class="BottomPixel">
                <b>Order Total: </b>
                <asp:Label ID="lblTotal" runat="server" />
            </div>
            <div>
                <b>Payment Status:</b>
                <asp:Label ID="lblPaymentStatus" runat="server" />
            </div>
        </div>
        <div class="FieldStyle">
             <b>Order Status: &nbsp;&nbsp;&nbsp;&nbsp;</b>
            <asp:DropDownList ID="ListOrderStatus" runat="server" AutoPostBack="false" OnSelectedIndexChanged="OrderStatus_SelectedIndexChanged" />
        </div>
        <br />
        <p>
            <small>Note: Changing the status will not Void or Refund credit card transactions. Please
            use the "Refund" feature for this.</small>
        </p>
        <br />
        <asp:Panel ID="pnlTrack" runat="server">
           <%-- <div class="FieldStyle">
                <asp:Label ID="TrackTitle" runat="server"></asp:Label>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="TrackingNumber" runat="server"></asp:TextBox>
            </div>--%>
            <div>
                <asp:Label ID="trackmessage" runat="server"></asp:Label><br />
                <br />
            </div>
        </asp:Panel>
        <div class="ImageButtons">
            <%-- <asp:Button ID="EmailStatus" runat="Server" CssClass="Size100" Text="Email Status"
                    OnClick="EmailStatus_Click" />--%>
            <asp:Button ID="UpdateOrderStatus" runat="Server" CssClass="Size100" Text="Update"
                OnClick="UpdateOrderStatus_Click" />
            <%-- <asp:Button ID="Cancel" runat="Server" CssClass="Size100" Text="Cancel" OnClick="CancelStatus_Click" />--%>
            <asp:ImageButton ID="btnCancelOrder" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="CancelStatus_Click" />
        </div>
        <uc1:Spacer ID="LongSpace" SpacerHeight="20" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
</asp:Content>
