using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Orders.OrderManagement.ViewOrders
{
    /// <summary>
    /// Represents the SiteAdmin.Secure.Orders.Sales.Orders - Default class
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Private Variables
        private static bool SearchEnabled = false;
        private string ViewOrderLink = "~/SiteAdmin/Secure/Orders/OrderManagement/ViewOrders/View.aspx?itemid=";
        private string ChangeStatusLink = "~/SiteAdmin/Secure/Orders/OrderManagement/ViewOrders/OrderStatus.aspx?itemid=";
        private string RefundOrderLink = "~/SiteAdmin/Secure/Orders/OrderManagement/ViewOrders/RefundPayment.aspx?itemid=";
        private string CaptureLink = "~/SiteAdmin/Secure/Orders/OrderManagement/ViewOrders/CapturePayment.aspx?itemid=";
        private string RMALink = "~/SiteAdmin/Secure/Orders/ReturnsManagement/RMAManager/RMARequests.aspx?orderid=";
        private DataSet MyDataSet = null;
        private string tabDelimeter = ",";
        private string PortalIds = string.Empty;
        private string FirstName = null;
        private string LastName = null;
        private string CompanyName = null;
        private string AccountNumber = null;
        private DateTime? StartDate = null;
        private DateTime? EndDate = null;
        private int? OrderStateID = null;
        private int? PortalID = null;
        private int? OrderId = null;

        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the SortField property is tracked in ViewState
        /// </summary>
        public string SortField
        {
            get
            {
                object o = ViewState["SortField"];
                if (o == null)
                {
                    return String.Empty;
                }

                return (string)o;
            }

            set
            {
                if (value == this.SortField)
                {
                    // Same as current sort file, toggle sort direction
                    this.SortAscending = !this.SortAscending;
                }

                ViewState["SortField"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether SortAscending property is tracked in ViewState
        /// </summary>
        public bool SortAscending
        {
            get
            {
                object o = ViewState["SortAscending"];
                if (o == null)
                {
                    return true;
                }

                return (bool)o;
            }

            set
            {
                ViewState["SortAscending"] = value;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Includes Javascript file and css file into this page
        /// </summary>
        public void RegisterClientScript()
        {
            // Include the Client Side Script from the resource file
            // The Resource File is named �Calender.js�
            // Located inside the Calendar directory
            HtmlGenericControl Include = new HtmlGenericControl("script");
            Include.Attributes.Add("type", "text/javascript");
            Include.Attributes.Add("src", "Calendar/Calendar.js");

            // The Resource File is named �Calender.css�
            // Located inside the Calendar directory
            HtmlGenericControl Include1 = new HtmlGenericControl("link");
            Include1.Attributes.Add("type", "text/css");
            Include1.Attributes.Add("rel", "stylesheet");
            Include1.Attributes.Add("href", "Calendar/Calendar.css");

            // Add a script reference for Javascript to the head section
            this.Page.Header.Controls.Add(Include);
            this.Page.Header.Controls.Add(Include1);
        }

        /// <summary>
        /// Get Highest OrderId Method
        /// </summary>
        public void GetHighestOrderId()
        {
            OrderAdmin _OrderAdmin = new OrderAdmin();
            int OrderId = _OrderAdmin.GetHighestOrderId(UserStoreAccess.GetAvailablePortals);
            OrderNumber.Text = OrderId.ToString();
        }

        private void GetData()
        {
            int shortOrderId;
            if (txtorderid.Text.Length > 0 && Int32.TryParse(txtorderid.Text.Trim(), out shortOrderId))
            {
                this.OrderId = shortOrderId;
            }

            if (txtfirstname.Text.Length > 0)
            {
                this.FirstName = txtfirstname.Text.Trim();
            }

            if (txtlastname.Text.Length > 0)
            {
                this.LastName = txtlastname.Text.Trim();
            }

            if (txtcompanyname.Text.Length > 0)
            {
                this.CompanyName = txtcompanyname.Text.Trim();
            }

            if (txtaccountnumber.Text.Length > 0)
            {
                this.AccountNumber = txtaccountnumber.Text.Trim();
            }

            if (txtlastname.Text.Length > 0)
            {
                this.LastName = txtlastname.Text.Trim();
            }

            if (txtStartDate.Text.Length > 0)
            {
                this.StartDate = DateTime.Parse(txtStartDate.Text.Trim());
            }

            if (txtEndDate.Text.Length > 0)
            {
                this.EndDate = DateTime.Parse(txtEndDate.Text.Trim());
            }

            if (ListOrderStatus.SelectedValue != "0")
            {
                this.OrderStateID = Convert.ToInt32(ListOrderStatus.SelectedValue);
            }

            if (ddlPortal.SelectedValue != "0")
            {
                this.PortalID = Convert.ToInt32(ddlPortal.SelectedValue);
            }
        }
        /// <summary>
        /// Bind Search Data Method
        /// </summary>
        public void BindSearchData()
        {
            this.GetData();
            if (this.PortalIds.Length > 0)
            {
                OrderAdmin _OrderAdmin = new OrderAdmin();
                this.MyDataSet = _OrderAdmin.FindOrders(this.OrderId, this.FirstName, this.LastName, this.CompanyName, this.AccountNumber, this.StartDate, this.EndDate, this.OrderStateID, this.PortalID, this.PortalIds);
                DataView dv = new DataView(this.MyDataSet.Tables[0]);
                dv.Sort = "OrderID Desc";
                uxGrid.DataSource = dv;
                uxGrid.DataBind();
            }
            else
            {
                lblmsg.Text = "You do not have permission to view this order. Please contact your administrator for permission";
                ButOrderLineItems.Enabled = false;
                ButDownload.Enabled = false;
            }
        }

        /// <summary>
        /// Bind Data Method
        /// </summary>
        public void BindData()
        {
            ZNode.Libraries.Admin.OrderAdmin OrderAdmin = new ZNode.Libraries.Admin.OrderAdmin();

            // Add New Item
            ListItem Li = new ListItem();
            Li.Text = "All";
            Li.Value = "0";

            // Load Order State Item 
            ListOrderStatus.DataSource = OrderAdmin.GetAllOrderStates();
            ListOrderStatus.DataTextField = "orderstatename";
            ListOrderStatus.DataValueField = "Orderstateid";
            ListOrderStatus.DataBind();
            ListOrderStatus.Items.Insert(0, Li);
            ListOrderStatus.Items[0].Selected = true;


            var lastItemIndex = ListOrderStatus.Items.Count - 1;

            // Find and remove the PENDING APPROVAL item
            var listItemPending = ListOrderStatus.Items[lastItemIndex];
            ListOrderStatus.Items.RemoveAt(lastItemIndex);

            ListOrderStatus.Items.Insert(1, listItemPending);

        }

        /// <summary>
        /// Contact first name and last name
        /// </summary>
        /// <param name="firstname">The value of firstname</param>
        /// <param name="lastName">The value of lastname</param>
        /// <returns>Returns the Name</returns>
        public string ReturnName(object firstname, object lastName)
        {
            return firstname.ToString() + " " + lastName.ToString();
        }

        /// <summary>
        /// Format the Price with two decimal
        /// </summary>
        /// <param name="fieldvalue">The value of fieldvalue</param>
        /// <returns>Returns the formatted price</returns>
        public string FormatPrice(object fieldvalue)
        {
            if (fieldvalue == DBNull.Value)
            {
                return string.Empty;
            }
            else
            {
                return "$" + fieldvalue.ToString().Substring(0, fieldvalue.ToString().Length - 2);
            }
        }

        /// <summary>
        /// Display the Payment type for the Order
        /// </summary>
        /// <param name="value">Represents the value</param>
        /// <returns>Returns the Payment Type</returns>
        protected string DisplayPaymentType(object value)
        {
            if (value != null)
            {
                if (value.ToString().Length > 0)
                {
                    ZNode.Libraries.Admin.OrderAdmin _OrderAdmin = new ZNode.Libraries.Admin.OrderAdmin();
                    PaymentType _type = _OrderAdmin.GetByPaymentTypeId(int.Parse(value.ToString()));
                    return _type.Name.ToString();
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Retrieves description of payment status
        /// </summary>
        /// <param name="value">Represents a value</param>
        /// <returns>Returns the Payment Status</returns>
        protected string DisplayPaymentStatus(object value)
        {
            if (value != null)
            {
                if (value.ToString().Length > 0)
                {
                    PaymentStatusService serv = new PaymentStatusService();
                    PaymentStatus ps = serv.GetByPaymentStatusID(int.Parse(value.ToString()));

                    return ps.PaymentStatusName.ToString();
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Enable or disable refund button
        /// </summary>
        /// <param name="value">Represents a value</param>
        /// <param name="value1">Represents a value1</param>
        /// <returns>Returns enable or disable refund button</returns>
        protected bool EnableRefund(object value, object value1,object orderstatus )
        {
            if (orderstatus.ToString().ToUpper() == "CANCELLED" || orderstatus.ToString().ToUpper() == "RETURNED")
                return false;
            bool flag = false;
            if (value != null)
            {
                if (value.ToString().Length > 0)
                {
                    if (int.Parse(value.ToString()) == 0 && (int.Parse(value1.ToString()) != 3))
                    {
                        flag = true;
                    }
                    else
                    {
                        flag = false;
                    }
                }
            }

            return flag;
        }
        /// <summary>
        /// Enable or disable RMA button
        /// </summary>
        /// <param name="value">Represents a value</param>       
        /// <returns>Returns enable or disable RMA button</returns>
        public bool EnableRMA(object value, object orderstatus, object date, object PaymentSettingId)
        {
            bool flag = false;
            if (orderstatus.ToString().ToUpper() == "CANCELLED" || orderstatus.ToString().ToUpper() == "RETURNED")
                return false;

            //Commented the code as per the task 14404 - Summary of changes
            //StoreSettingsAdmin settingsAdmin = new StoreSettingsAdmin();
            //PaymentSetting paymentSetting = settingsAdmin.GetPaymentSettingByID(Convert.ToInt32(PaymentSettingId));

            //if (paymentSetting != null)
            //{
            //    if (paymentSetting.GatewayTypeID == 1)
            //       flag= true;
            //    else
            //        return false;
            //}


            RMAConfigurationAdmin rmaConfigAdmin = new RMAConfigurationAdmin();
            TList<RMAConfiguration> rmaConfigs = rmaConfigAdmin.GetAllRMAConfiguration();

            int rmaPeriod = 0;
            if (rmaConfigs.Count > 0)
            {
                if(rmaConfigs[0].MaxDays.HasValue)
                rmaPeriod = rmaConfigs[0].MaxDays.Value;

                DateTime orderdate = Convert.ToDateTime(date.ToString()).AddDays(rmaPeriod);


                if (orderdate.Date< DateTime.Now.Date)
                    return false;
            }
           
            if (value != null)
            {
              int orderid=int.Parse(value.ToString());
              RMARequestAdmin rmaRequestAdmin = new RMARequestAdmin();
              flag = rmaRequestAdmin.GetOrderRMAFlag(Convert.ToInt32(orderid));
                              
            }

            return flag;
        }
        /// <summary>
        /// Enable or Disable Capture Button
        /// </summary>
        /// <param name="paymentTypeId">Payment Type Id</param>
        /// <param name="paymentStatusId">Payment status Id.</param>
        /// <returns>Returns true if Capture enable </returns>
        protected bool EnableCapture(object paymentTypeId, object paymentStatusId)
        {
            if (paymentStatusId != null)
            {
                if (paymentStatusId.ToString().Length > 0)
                {
                    if (int.Parse(paymentStatusId.ToString()) == 0 && Convert.ToString(paymentTypeId) == "0")
                    {
                        // Show Capture button only for credit card payment type and status is authorized.
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Get Store Name by PortalId
        /// </summary>
        /// <param name="portalID">The value of portalID</param>
        /// <returns>Returns the Store Name</returns>
        protected string GetStoreName(string portalID)
        {
            PortalAdmin portalAdmin = new PortalAdmin();

            return portalAdmin.GetStoreNameByPortalID(portalID);
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            ltrlError.Text = string.Empty;

            // Get Portals
            this.PortalIds = UserStoreAccess.GetAvailablePortals;

            if (!Page.IsPostBack)
            {
                lblError.Text = string.Empty;
                SearchEnabled = false;
                this.BindPortal();
                this.BindData();
                this.BindSearchData();
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Search Button Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            uxGrid.PageIndex = 0;
            this.BindSearchData();
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClearSearch_Click(object sender, EventArgs e)
        {
            SearchEnabled = false;
            txtStartDate.Text = string.Empty;
            txtEndDate.Text = string.Empty;
            txtorderid.Text = string.Empty;
            txtfirstname.Text = string.Empty;
            txtlastname.Text = string.Empty;
            txtcompanyname.Text = string.Empty;
            txtaccountnumber.Text = string.Empty;
            ltrlError.Text = string.Empty;
            ddlPortal.SelectedIndex = 0;
            this.BindData();
            uxGrid.PageIndex = 0;
            this.BindSearchData();
        }

        /// <summary>
        /// Download Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ButDownload_Click(object sender, EventArgs e)
        {
            lblError.Text = string.Empty;
            DataDownloadAdmin csv = new DataDownloadAdmin();
            OrderAdmin _OrderAdmin = new OrderAdmin();

            string Orderid = Convert.ToString(OrderNumber.Text);
            DataSet Orders = _OrderAdmin.GetOrdersByOrderId(Orderid);

            if (Orders.Tables[0].Rows.Count > 0)
            {
                // Set Formatted Data from DataSet           
                string strData = csv.Export(Orders, true, this.tabDelimeter);

                byte[] data = ASCIIEncoding.ASCII.GetBytes(strData);

                Response.Clear();

                // Set as Excel as the primary format
                Response.AddHeader("Content-Type", "application/Excel");

                Response.AddHeader("Content-Disposition", "attachment;filename=Order.csv");
                Response.ContentType = "application/vnd.xls";
                Response.BinaryWrite(data);

                Response.End();
            }
            else
            {
                ltrlError.Text = "* No Orders to download";
                return;
            }
        }

        /// <summary>
        /// Order ListItem Buttton Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ButOrderLineItemsDownload_Click(object sender, EventArgs e)
        {
            DataDownloadAdmin csv = new DataDownloadAdmin();
            OrderAdmin _OrderAdmin = new OrderAdmin();
            DataSet OrderLineItems = new DataSet();
            DataTable dt = new DataTable();

            string Orderid = Convert.ToString(OrderNumber.Text);
            dt = _OrderAdmin.GetOrderLineItemsByOrderId(Orderid).Tables[0];
            dt.Columns.Remove("PortalId");

            foreach (DataRow dr in dt.Rows)
            {
                dr["description"] = this.StripHTML(dr["description"].ToString());
            }

            OrderLineItems.Tables.Add(dt.Copy());

            if (OrderLineItems.Tables[0].Rows.Count > 0)
            {
                string strData = csv.Export(OrderLineItems, true, this.tabDelimeter);

                byte[] data = ASCIIEncoding.ASCII.GetBytes(strData);

                Response.Clear();
                Response.ClearHeaders();
                Response.ClearContent();

                // Set as Excel as the primary format
                Response.AddHeader("Content-Disposition", "attachment;filename=OrderLineItems.csv");
                Response.AddHeader("Content-Type", "application/Excel");
                Response.ContentType = "text/csv";
                Response.ContentType = "application/vnd.xls";
                Response.AddHeader("Pragma", "public");
                Response.BinaryWrite(data);
                Response.End();

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Imported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Download OrderLineItems", "Order Line Items");
            }
            else
            {
                ltrlError.Text = "* No Orders to download";
                return;
            }
        }
        #endregion

        #region Grid Events

        /// <summary>
        /// Method to sort the grid in Ascending and Descending Order
        /// </summary>   
        protected void SortGrid()
        {
            DateTime? StartDate = null;
            DateTime? EndDate = null;

            if (txtStartDate.Text.Length > 0)
            {
                StartDate = DateTime.Parse(txtStartDate.Text.Trim());
            }

            if (txtEndDate.Text.Length > 0)
            {
                EndDate = DateTime.Parse(txtEndDate.Text.Trim());
            }

            if (this.PortalIds.Length > 0)
            {
                OrderAdmin _OrderAdmin = new OrderAdmin();
                DataSet ds;

                if (Roles.IsUserInRole("ADMIN"))
                {
                    ds = _OrderAdmin.FindOrders(this.OrderId, this.FirstName, this.LastName, this.CompanyName, this.AccountNumber, this.StartDate, this.EndDate, this.OrderStateID, this.PortalID, this.PortalIds);
                }
                else
                {
                    ds = _OrderAdmin.FindOrders(this.OrderId, this.FirstName, this.LastName, this.CompanyName, this.AccountNumber, this.StartDate, this.EndDate, this.OrderStateID, this.PortalID, this.PortalIds);
                }

                uxGrid.DataSource = ds;

                DataSet dataSet = uxGrid.DataSource as DataSet;

                DataView dataView = new DataView(dataSet.Tables[0]);

                // Apply sort filter and direction
                dataView.Sort = this.SortField;

                // If sortDirection is not Ascending
                if (!this.SortAscending)
                {
                    dataView.Sort += " DESC";
                }

                uxGrid.DataSource = null;
                uxGrid.DataSource = dataView;
                uxGrid.DataBind();
            }
            else
            {
                lblmsg.Text = "You do not have permission to view this order. Please contact your administrator for permission";
                ButOrderLineItems.Enabled = false;
                ButDownload.Enabled = false;
            }
        }

        /// <summary>
        /// Grid Paging Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindSearchData();
        }

        /// <summary>
        ///  Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "ViewOrder")
                {
                    Response.Redirect(this.ViewOrderLink + e.CommandArgument.ToString());
                }

                if (e.CommandName == "RefundOrder")
                {
                    Response.Redirect(this.RefundOrderLink + e.CommandArgument.ToString());
                }
                else if (e.CommandName == "Status")
                {
                    Response.Redirect(this.ChangeStatusLink + e.CommandArgument.ToString());
                }
                else if (e.CommandName == "Capture")
                {
                    Response.Redirect(this.CaptureLink + e.CommandArgument.ToString());
                }
                else if (e.CommandName == "RMA")
                {
                    Response.Redirect(this.RMALink + e.CommandArgument.ToString());
                }
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Get Order Line Items Method
        /// </summary>
        /// <returns>Returns the Order Line Items Data Set</returns>
        private DataSet GetOrderLineItems()
        {
            OrderAdmin _OrderAdmin = new OrderAdmin();

            string stdate = String.Empty;
            string enddate = String.Empty;

            // Check for Search is enabled or not
            if (SearchEnabled)
            {
                stdate = txtStartDate.Text.Trim();
                enddate = txtEndDate.Text.Trim();
            }

            DataSet MyDataSet = _OrderAdmin.GetOrderLineItems(txtorderid.Text.Trim(), txtfirstname.Text.Trim(), txtlastname.Text.Trim(), txtcompanyname.Text.Trim(), txtaccountnumber.Text.Trim(), stdate, enddate, int.Parse(ListOrderStatus.SelectedValue.ToString()), ZNodeConfigManager.SiteConfig.PortalID);

            return MyDataSet;
        }

        /// <summary>
        /// Bind Portals
        /// </summary>
        private void BindPortal()
        {
            PortalAdmin portalAdmin = new PortalAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Portal> portals = portalAdmin.GetAllPortals();

            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (string.Compare(profiles.StoreAccess, "AllStores") != 0)
            {
                if (profiles.StoreAccess != string.Empty)
                {
                    portals.Filter = string.Concat("PortalID IN [", profiles.StoreAccess, "]");
                }
            }
            else
            {
                Portal po = new Portal();
                po.StoreName = "ALL STORES";
                portals.Insert(0, po);
            }

            // Portal Drop Down List
            ddlPortal.DataSource = portals;
            ddlPortal.DataTextField = "StoreName";
            ddlPortal.DataValueField = "PortalID";
            ddlPortal.DataBind();
        }

        /// <summary>
        /// Strip HTML method
        /// </summary>
        /// <param name="description">The value of description</param>
        /// <returns>Returns the HTMl string</returns>
        private string StripHTML(string description)
        {
            return System.Text.RegularExpressions.Regex.Replace(description, "<[^>]*>", " ");
        }
        #endregion

        protected void uxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            foreach (GridViewRow gvC in uxGrid.Rows)
            {
                Label OrderStatus = (Label)gvC.FindControl("OrderStatus");
                if (OrderStatus.Text == "PENDING APPROVAL")
                {
                    OrderStatus.Text = "PENDING";
                    OrderStatus.ForeColor = System.Drawing.Color.Red;
                }
            }
        }
    }
}