<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True"
    Inherits="SiteAdmin.Secure.Orders.OrderManagement.ViewOrders.View" ValidateRequest="false" CodeBehind="View.aspx.cs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div align="center">
        <div>
            <div class="LeftFloat" style="width: 30%; text-align: left">
                <h1>Order ID:
                    <asp:Label ID="lblOrderHeader" runat="server" Text="Label" /></h1>
            </div>
            <div style="float: right" class="ImageButtons">
                <asp:Button ID="List" runat="server" Text="< Back to Order List" OnClick="List_Click" />
                <asp:Button ID="Refund" runat="server" Text="Void or Refund" OnClick="Refund_Click" />
            </div>
            <div class="ClearBoth">
            </div>
            <div align="left">
                <div>
                    <div class="LeftFloat" style="width: 600px">
                        <!-- Order Info -->
                        <h4 class="SubTitle">Order Information</h4>
                        <div class="ViewForm100">
                            <div class="FieldStyle">
                                Order Status
                            </div>
                            <div class="ValueStyle">
                                <div class="LeftFloat" >
                                    <asp:Label ID="lblOrderStatus" Font-Bold="true" runat="server" />
                                </div>
                                <div class="RightFloatButton ImageButtons" >
                                     <asp:Button ID="ChangeStatus" runat="server" Text="Update Order Status" OnClick="ChangeStatus_Click" />
                                </div>
                            </div>
                            <div class="FieldStyleA">
                                Payment Status
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblPaymentStatus" Font-Bold="true" runat="server" />&nbsp;
                            </div>
                            <div class="FieldStyle">
                                Order Date
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblOrderDate" runat="server" />
                            </div>
                            <div class="FieldStyleA">
                                Order Amount
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblOrderAmount" runat="server" />&nbsp;
                            </div>
                            <div class="FieldStyle">
                                Shipping Amount
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblShipAmount" runat="server" />
                            </div>
                            <div class="FieldStyleA">
                                Tax Amount
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblTaxAmount" runat="server" />&nbsp;
                            </div>
                            <div class="FieldStyle">
                                Discount Amount
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblDiscountAmt" runat="server" />
                            </div>
                            <div class="FieldStyleA">
                                Gift Card Amount
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblGiftCardAmount" runat="server" />&nbsp;
                            </div>
                            <div class="FieldStyle">
                                Payment Method
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblPaymentType" runat="server" />
                            </div>
                            <div class="FieldStyleA">
                                Transaction ID
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblTransactionId" runat="server" />&nbsp;
                            </div>
                            <div class="FieldStyle">
                                Purchase Order
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblPurchaseOrder" runat="server" />
                            </div>
                            <div class="FieldStyleA">
                                Shipping Method
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblShippingMethod" runat="server" />&nbsp;
                            </div>
                            <div class="FieldStyle">
                                Tracking Number
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblTrackingNumber" runat="server" />
                            </div>
                            <div class="FieldStyleA">
                                Promotion Codes
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblCouponCode" runat="server" />&nbsp;
                            </div>
                        </div>
                    </div>
                    <div class="LeftFloat" style="width: 100%;">
                        <!-- Address -->
                        <h4 class="SubTitle">Customer Information</h4>
                        <div>
                            <div class="LeftFloat" style="text-align: left; width: 400px; word-wrap: break-word;">
                                <div class="FieldStyle" style="text-align: left">
                                    <b>Billing Address</b>
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblBillingAddress" runat="server" Text="Label"></asp:Label>
                                </div>
                            </div>
                            <div style="float: left; width: 400px; word-wrap: break-word;">
                                <div class="FieldStyle" style="text-align: left">
                                    <b>Shipping Address</b>
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblShippingAddress" runat="server" Text="Label"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ClearBoth">
                </div>
                <br />
                <asp:Panel ID="ShippingErrorPanel" runat="server">
                    <h4>
                        <asp:Label ID="ErrorHeader" Text="Shipping Errors" runat="server"></asp:Label></h4>
                    <div align="justify">
                        <asp:Label ID="ShippingErrors" runat="server" CssClass="Error"></asp:Label>
                    </div>
                </asp:Panel>
                <h4 class="GridTitle">Order Items - Ship Together</h4>
                <asp:GridView ID="uxGrid" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
                    runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
                    CssClass="Grid" Width="100%" GridLines="None" EmptyDataText="No orderline items found"
                    AllowPaging="True" OnPageIndexChanging="UxGrid_PageIndexChanging" OnRowCommand="UxGrid_RowCommand"
                    PageSize="10">
                    <Columns>
                        <asp:BoundField DataField="OrderLineItemID" HeaderText="Line Item ID" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText="Product Name" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="ProductNum" HeaderText="Product Code" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "Description") %>
                               <asp:Literal ID="ltrNote" runat="server" Text='<%# GetLineItemNotes(DataBinder.Eval(Container.DataItem,"OrderLineItemID").ToString(),DataBinder.Eval(Container.DataItem, "ParentOrderLineItemId")) %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="SKU" HeaderText="SKU" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="Quantity" HeaderText="Quantity" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText="Price" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "price","{0:c}").ToString()%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ShipDate" HeaderText="Ship Date" DataFormatString="{0:d}"
                            HtmlEncode="False" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="TrackingNumber" HeaderText="Tracking Number" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="TransactionNumber" HeaderText="Transaction Number" HeaderStyle-HorizontalAlign="Left" />
                    </Columns>
                    <EmptyDataTemplate>
                        No orderline items found
                    </EmptyDataTemplate>
                    <RowStyle CssClass="RowStyle" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    <FooterStyle CssClass="FooterStyle" />
                    <PagerStyle CssClass="PagerStyle" />
                </asp:GridView>
                <asp:Panel ID="ShippingPanel" runat="server">
                    <asp:Panel ID="DemensionPanel" runat="server">
                        <br />
                        <div style="width: 50%">
                            We have roughly estimated your package size and weight based on information in the
                            Product database. If this estimate is incorrect please update it before creating
                            your shipment.
                        </div>
                        <br />
                        <asp:RegularExpressionValidator ID="EstimatedWeightValidator" ControlToValidate="EstimatedWeight"
                            ErrorMessage="Please enter a Decimal Value for Weight" runat="server" ValidationExpression="^(?!0*\.?0*$)([0-9]+(\.)?[0-9]?[0-9]?)$"
                            ValidationGroup="OrderEstimate"></asp:RegularExpressionValidator><br />
                        <asp:RegularExpressionValidator ID="EstimatedHeightValidator" ControlToValidate="EstimatedHeight"
                            ErrorMessage="Please enter a whole number for Height" runat="server" ValidationExpression="^(?!0*\.?0*$)([0-9]+?[0-9]?[0-9]?)$"
                            ValidationGroup="OrderEstimate"></asp:RegularExpressionValidator><br />
                        <asp:RegularExpressionValidator ID="EstimatedLengthValidator" ControlToValidate="EstimatedLength"
                            ErrorMessage="Please enter a whole number for Length" runat="server" ValidationExpression="^(?!0*\.?0*$)([0-9]+?[0-9]?[0-9]?)$"
                            ValidationGroup="OrderEstimate"></asp:RegularExpressionValidator><br />
                        <asp:RegularExpressionValidator ID="EstimatedWidthValidator" ControlToValidate="EstimatedWidth"
                            ErrorMessage="Please enter a whole number for Width" runat="server" ValidationExpression="^(?!0*\.?0*$)([0-9]+?[0-9]?[0-9]?)$"
                            ValidationGroup="OrderEstimate"></asp:RegularExpressionValidator><br />
                        <asp:RequiredFieldValidator ID="RequiredEstimatedWidth" ControlToValidate="EstimatedWidth"
                            ErrorMessage="Width must be specified to create a shipment" runat="server" ValidationGroup="RequiredEstimate"></asp:RequiredFieldValidator><br />
                        <asp:RequiredFieldValidator ID="RequiredEstimatedLength" ControlToValidate="EstimatedLength"
                            ErrorMessage="Length must be specified to create a shipment" runat="server" ValidationGroup="RequiredEstimate"></asp:RequiredFieldValidator><br />
                        <asp:RequiredFieldValidator ID="RequiredEstimatedHeight" ControlToValidate="EstimatedHeight"
                            ErrorMessage="Height must be specified to create a shipment" runat="server" ValidationGroup="RequiredEstimate"></asp:RequiredFieldValidator><br />
                        <asp:RequiredFieldValidator ID="RequiredEstimatedWeight" ControlToValidate="EstimatedWeight"
                            ErrorMessage="Weight must be specified to create a shipment" runat="server" ValidationGroup="RequiredEstimate"></asp:RequiredFieldValidator><br />
                        <div>
                            <div class="LeftFloat" width="25%" valign="top">
                                <div class="FieldStyle">
                                    Weight
                                </div>
                                <div class="ValueStyle">
                                    <asp:TextBox ID="EstimatedWeight" runat="server" Width="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="LeftFloat" width="25%" valign="top">
                                <div class="FieldStyle">
                                    Height
                                </div>
                                <div class="ValueStyle">
                                    <asp:TextBox ID="EstimatedHeight" runat="server" Width="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="LeftFloat" width="25%" valign="top">
                                <div class="FieldStyle">
                                    Length
                                </div>
                                <div class="ValueStyle">
                                    <asp:TextBox ID="EstimatedLength" runat="server" Width="50"></asp:TextBox>
                                </div>
                                <div class="LeftFloat" width="25%" valign="top">
                                    <div class="FieldStyle">
                                        Width
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:TextBox ID="EstimatedWidth" runat="server" Width="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                    </asp:Panel>
                    <br />
                    <br />
                </asp:Panel>
                <h4 class="GridTitle">Order Items - Ship Separately</h4>
                <asp:GridView ID="uxGrid2" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
                    runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
                    CssClass="Grid" Width="100%" GridLines="None" EmptyDataText="No items found."
                    AllowPaging="True" OnPageIndexChanging="UxGrid_PageIndexChanging" OnRowCommand="UxGrid_RowCommand"
                    PageSize="10">
                    <Columns>
                        <asp:BoundField DataField="OrderLineItemID" HeaderText="Line Item ID" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="Name" HeaderText="Product Name" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="ProductNum" HeaderText="Product Code" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "Description") %>
                                <asp:Literal ID="ltrNote" runat="server" Text='<%#GetLineItemNotes(DataBinder.Eval(Container.DataItem,"OrderLineItemID").ToString(),DataBinder.Eval(Container.DataItem, "ParentOrderLineItemId"))%>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="SKU" HeaderText="SKU" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="Quantity" HeaderText="Quantity" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText="Price" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "price","{0:c}").ToString()%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ShipDate" HeaderText="Ship Date" DataFormatString="{0:d}"
                            HtmlEncode="False" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText="Shipping Cost" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%#DataBinder.Eval(Container.DataItem, "ShippingCost", "{0:c}").ToString()%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="TrackingNumber" HeaderText="Tracking Number" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Button runat="server" CausesValidation="true" ID="CancelShipping" Text='<%# ButtonText(Eval("TrackingNumber"),Eval("OrderLineItemID")) %>'
                                    CssClass="Button" CommandArgument='<%# Eval("OrderLineItemID") %>' CommandName='<%# ShippingCommand(Eval("TrackingNumber"),Eval("OrderLineItemID")) %>'
                                    Visible="false" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Button runat="server" CausesValidation="false" ID="Label" Text="Print Label"
                                    CssClass="Button" CommandArgument='<%# Eval("TrackingNumber") %>' CommandName="Label"
                                    Visible='<%# ShowLabelButton(Eval("TrackingNumber")) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No items found.
                    </EmptyDataTemplate>
                    <RowStyle CssClass="RowStyle" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    <FooterStyle CssClass="FooterStyle" />
                    <PagerStyle CssClass="PagerStyle" />
                </asp:GridView>
                <asp:HiddenField runat="server" ID="EstimatedLineItemID" />
                <asp:Panel ID="LineItemDemensions" runat="server">
                    <br />
                    <div>
                        <div class="LeftFloat">
                            To Estimate shipping for a ship seperate order item click "Estimate Dimensions".
                            If this estimate is incorrect please update it before creating your shipment.
                        </div>
                    </div>
                    <br />
                    <asp:RequiredFieldValidator ID="LineItemWidthRequired" ControlToValidate="LineItemWidth"
                        ErrorMessage="Width must be specified to create a shipment" runat="server" ValidationGroup="RequiredLineItems"></asp:RequiredFieldValidator><br />
                    <asp:RequiredFieldValidator ID="LineItemLengthRequired" ControlToValidate="LineItemLength"
                        ErrorMessage="Length must be specified to create a shipment" runat="server" ValidationGroup="RequiredLineItems"></asp:RequiredFieldValidator><br />
                    <asp:RequiredFieldValidator ID="LineItemHeightRequired" ControlToValidate="LineItemHeight"
                        ErrorMessage="Height must be specified to create a shipment" runat="server" ValidationGroup="RequiredLineItems"></asp:RequiredFieldValidator><br />
                    <asp:RequiredFieldValidator ID="LineItemWeightRequired" ControlToValidate="LineItemWeight"
                        ErrorMessage="Weight must be specified to create a shipment" runat="server" ValidationGroup="RequiredLineItems"></asp:RequiredFieldValidator><br />
                    <asp:RegularExpressionValidator Display="static" ID="LineItemWeightValidator" ControlToValidate="LineItemWeight"
                        ErrorMessage="Please enter a decimal for Weight" runat="server" ValidationExpression="^(?!0*\.?0*$)([0-9]+(\.)?[0-9]?[0-9]?)$"
                        ValidationGroup="1OrderEstimate"></asp:RegularExpressionValidator><br />
                    <asp:RegularExpressionValidator ID="LineItemHeightValidator" ControlToValidate="LineItemHeight"
                        ErrorMessage="Please enter a whole number for Height" runat="server" ValidationExpression="^(?!0*\.?0*$)([0-9]+?[0-9]?[0-9]?)$"
                        ValidationGroup="1OrderEstimate"></asp:RegularExpressionValidator><br />
                    <asp:RegularExpressionValidator ID="LineItemLengthValidator" ControlToValidate="LineItemLength"
                        ErrorMessage="Please enter a whole number for Length" runat="server" ValidationExpression="^(?!0*\.?0*$)([0-9]+?[0-9]?[0-9]?)$"
                        ValidationGroup="1OrderEstimate"></asp:RegularExpressionValidator><br />
                    <asp:RegularExpressionValidator ID="LineItemWidthValidator" ControlToValidate="LineItemWidth"
                        ErrorMessage="Please enter a whole number for Width" runat="server" ValidationExpression="^(?!0*\.?0*$)([0-9]+?[0-9]?[0-9]?)$"
                        ValidationGroup="1OrderEstimate"></asp:RegularExpressionValidator><br />
                    <div>
                        <div class="LeftFloat" width="25%" valign="top">
                            <div class="FieldStyle">
                                Weight
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="LineItemWeight" runat="server" Width="50"></asp:TextBox>
                            </div>
                        </div>
                        <div class="LeftFloat" width="25%" valign="top">
                            <div class="FieldStyle">
                                Height
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="LineItemHeight" runat="server" Width="50"></asp:TextBox>
                            </div>
                        </div>
                        <div class="LeftFloat" width="25%" valign="top">
                            <div class="FieldStyle">
                                Length
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="LineItemLength" runat="server" Width="50"></asp:TextBox>
                            </div>
                        </div>
                        <div class="LeftFloat" width="25%" valign="top">
                            <div class="FieldStyle">
                                v 
                                Width
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="LineItemWidth" runat="server" Width="50"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <h4 class="SubTitle">Additional Instructions</h4>
                <div align="justify">
                    <asp:Label ID="lblAdditionalInstructions" runat="server"></asp:Label>
                </div>
               <%-- Perficient Custom Code for Showing Multiple Credit Card Payment Data--%>
                 <h4 class="SubTitle">Additional Card Payment Details</h4>
                  <asp:Literal ID="ltrAdditionalCardDetails" runat="server"></asp:Literal>
               <%-- Perficient Custom Code for Showing Multiple Credit Card Payment Data--%>
                <asp:Panel ID="pnlrma" runat="server" Visible="false">
                    <h4 class="SubTitle">Return Merchandise Authorization Request</h4>
                    <div>
                        <asp:GridView ID="uxRMAGrid" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            CaptionAlign="Left" CellPadding="4" CssClass="Grid" EmptyDataText="No RMA Request exist in the database."
                            EnableSortingAndPagingCallbacks="False" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
                            OnRowCommand="UxGrid_RowCommand" PageSize="25" Width="100%">
                            <Columns>
                                <asp:HyperLinkField DataNavigateUrlFields="RMARequestID,OrderID" DataNavigateUrlFormatString='../../ReturnsManagement/RMAManager/viewRMARequest.aspx?itemid={0}&orderid={1}&page=order'
                                    DataTextField="RMARequestID" HeaderText="RMA ID" ItemStyle-Width="120" SortExpression="RMARequestID"
                                    HeaderStyle-HorizontalAlign="Left" />

                                <asp:BoundField DataField="RequestDate" HeaderText="Request Date" SortExpression="OrderDate"
                                    ItemStyle-Width="160" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False" HeaderStyle-HorizontalAlign="Left" />

                                <asp:TemplateField SortExpression="OrderStateID" HeaderText="Status" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="RequestStatus" Text='<%# Eval("RequestStatus") %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                            <FooterStyle CssClass="FooterStyle" />
                            <RowStyle CssClass="RowStyle" />
                            <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
                            <HeaderStyle CssClass="HeaderStyle" />
                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                        </asp:GridView>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
