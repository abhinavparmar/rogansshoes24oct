<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master"
    AutoEventWireup="True" Inherits="SiteAdmin.Secure.Orders.OrderManagement.ViewOrders.Confirmstatus"
    CodeBehind="Confirmstatus.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <h5>Please Confirm<uc1:DemoMode ID="DemoMode1" runat="server"></uc1:DemoMode>
    </h5>
    <p>
        Please confirm if you want to change the status of the <b>Order No
            <%=OrderId%></b> to <b>
                <%=OrderStatus%></b>. This change cannot be undone.
    </p>
    <asp:Label CssClass="Error" ID="lblErrorMessage" runat="server" Text=''></asp:Label>
    <div class="ImageButtons">
        <asp:Button CssClass="Size100" ID="btnConfirm" CausesValidation="False" Text="Confirm"
            runat="server" OnClick="BtnConfirm_Click" />
        <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
            onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
            runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
    </div>
    <div>
        <br />
        <asp:Label ID="errormessage" runat="server"></asp:Label>
    </div>
    <br />
    <br />
</asp:Content>
