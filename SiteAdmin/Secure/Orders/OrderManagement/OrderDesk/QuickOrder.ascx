<%@ Control Language="C#" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Orders.OrderManagement.OrderDesk.QuickOrder" CodeBehind="QuickOrder.ascx.cs" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/ProductAttributeGrid.ascx" TagName="ProductAttributeGrid"
    TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="StoreName" Src="~/SiteAdmin/Controls/Default/StoreName.ascx" %>
<div>
    <div class="LeftFloat" style="width: 30%; text-align: left">
        <h1>Search Products</h1>
    </div>
    <div style="float: right">
        <asp:LinkButton ID="btnClose" CausesValidation="false" runat="server" OnClick="BtnClose_Click">Close X</asp:LinkButton>
    </div>
    <div class="ClearBoth">
    </div>
    <asp:HiddenField ID="PageIndex" Value="1" runat="server" />
    <asp:HiddenField ID="TotalPages" Value="0" runat="server" />
    <!-- Product Search -->
    <asp:Panel ID="pnlSearchParams" runat="server" DefaultButton="btnSearch">
        <div class="SearchForm">
            <div class="RowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle" style="width: 200px">Partial Product Name</span><br />
                    <span class="ValueStyle" style="width: 200px">
                        <asp:TextBox ID="txtProductName" runat="server"></asp:TextBox></span>
                </div>
                <div class="ItemStyle">
                    <span class="FieldStyle" style="width: 150px">Partial Item#</span><br />
                    <span class="ValueStyle" style="width: 150px">
                        <asp:TextBox ID="txtProductNum" runat="server" Width="80px"></asp:TextBox></span>
                </div>
                <div class="ItemStyle">
                    <span class="FieldStyle" style="width: 150px">Partial SKU</span><br />
                    <span class="ValueStyle" style="width: 150px">
                        <asp:TextBox ID="txtProductSku" runat="server" Width="80px"></asp:TextBox></span>
                </div>
                <div class="ItemStyle">
                    <span class="FieldStyle" style="width: 150px">Partial Brand</span><br />
                    <span class="ValueStyle" style="width: 150px">
                        <asp:TextBox ID="txtBrand" runat="server" Width="100px"></asp:TextBox></span>
                </div>
                <div class="ItemStyle">
                    <span class="FieldStyle" style="width: 100px">Partial Category</span><br />
                    <span class="ValueStyle" style="width: 100px">
                        <asp:TextBox ID="txtCategory" runat="server" Width="100px"></asp:TextBox></span>
                </div>
            </div>
            <div class="RowStyle">
                <div>
                    <ZNode:Spacer ID="Spacer6" SpacerHeight="10" SpacerWidth="1" runat="server"></ZNode:Spacer>
                </div>
            </div>
            <div class="RowStyle">
                <div class="ButtonStyle">
                    <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                        runat="server" OnClick="Search_Click" AlternateText="Search"/>
                </div>
            </div>
            <div class="RowStyle">
                <div>
                    <ZNode:Spacer ID="Spacer5" SpacerHeight="10" SpacerWidth="1" runat="server"></ZNode:Spacer>
                </div>
            </div>
            <div class="RowStyle">
                <div>
                    <asp:Label CssClass="Error" ID="lblSearhError" runat="server" EnableViewState="false" />
                </div>
            </div>
        </div>
    </asp:Panel>
    <!-- Product List -->
    <asp:Panel ID="pnlProductList" runat="server" Visible="false">
        <div>
            <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="1" runat="server"></ZNode:Spacer>
        </div>
        <div class="HintText">
            Click on the "Select" link in the first column to select a product.
        </div>
        <asp:GridView ID="uxGrid" PageSize="10" CellSpacing="0" CellPadding="6" CssClass="SearchGrid"
            runat="server" AutoGenerateColumns="False" GridLines="None" OnSelectedIndexChanged="UxGrid_SelectedIndexChanged">
            <Columns>
                <asp:CommandField CausesValidation="false" ShowSelectButton="true" ValidationGroup="grpCartItems"
                    ButtonType="Link" SelectText="SELECT" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText="Id" Visible="false" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="ui_CustomerIdSelect" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"ProductId") %>'></asp:Label></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Name" HtmlEncode="false" DataField="Name" HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField HeaderText="Product Number" DataField="ProductNum" HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField HeaderText="Short Description" HtmlEncode="false" DataField="ShortDescription" HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField HeaderText="Retail Price" DataField="RetailPrice" DataFormatString="{0:c}"
                    HeaderStyle-HorizontalAlign="Left" />
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerStyle CssClass="PagerStyle" />
        </asp:GridView>
        <div class="PagingField">
            <asp:LinkButton ID="FirstPageLink" Text="&laquo; First" OnClick="MoveToFirstPage"
                runat="server"></asp:LinkButton>
            <asp:LinkButton ID="PreviousPageLink" Text="&laquo; Prev" runat="server" OnClick="PrevRecord"></asp:LinkButton>
            &nbsp;| Page
            <%= PageIndex.Value + " of " + TotalPages.Value %>
            |&nbsp;
            <asp:LinkButton ID="NextPageLink" Text="Next &raquo;" OnClick="NextRecord" runat="server"></asp:LinkButton>
            <asp:LinkButton ID="LastPageLink" Text="Last &raquo;" OnClick="MoveToLastPage" runat="server"></asp:LinkButton>
        </div>
    </asp:Panel>
    <!-- Product Detail section -->
    <asp:Panel ID="pnlProductDetails" runat="server" Visible="false">
        <div id="Table1" runat="server">
            <div class="Grid">
                <div class="HeaderStyle">
                    <span style="width: 200px; display: inline-block;">Qty </span><span style="width: 200px; display: inline-block;">Options </span>
                    <span style="width: 200px; display: inline-block;">Unit Price </span><span style="width: 200px; display: inline-block;">Total Price                                                          
                    </span>
                </div>
                <div class="RowStyle" style="border-bottom: solid 1px #5d7b9d;" valign="top">
                    <span style="width: 200px; display: inline-block; margin: 0px; padding: 5px 0 10px 5px;">
                        <asp:DropDownList ID="Qty" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Qty_SelectedIndexChanged" CausesValidation="true" ValidationGroup="grpCartItems">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ValidationGroup="grpCartItems" ID="req1" ControlToValidate="Qty"
                            CssClass="Error" ErrorMessage="*required" runat="server" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ValidationGroup="grpCartItems" ValidationExpression="^\d+$"
                            ID="val1" CssClass="Error" ControlToValidate="Qty" ErrorMessage="*invalid" runat="server"
                            Display="Dynamic" SetFocusOnError="true"></asp:RegularExpressionValidator>
                        <asp:RangeValidator ValidationGroup="grpCartItems" ID="QuantityRangeValidator" CssClass="Error"
                            MinimumValue="0" ControlToValidate="Qty" ErrorMessage="*Out of Stock" MaximumValue="1"
                            Display="Dynamic" SetFocusOnError="true" runat="server" Type="Integer"></asp:RangeValidator>
                    </span><span style="width: 200px; margin: 0px; display: inline-block; padding: 5px 0 0 5px;">
                        <asp:PlaceHolder ID='ControlPlaceHolder' runat="server"></asp:PlaceHolder>
                    </span><span style="width: 200px; margin: 0px; display: inline-block; padding: 5px 0 10px 5px;">
                        <asp:Label ID="lblUnitPrice" runat="server" Text='&nbsp;'></asp:Label>
                    </span><span style="width: 200px; margin: 0px; display: inline-block; padding: 5px 0 10px 5px;">
                        <asp:Label ID="lblTotalPrice" runat="server" Text='&nbsp;'></asp:Label>
                    </span>
                </div>
            </div>
            <div align="right">
                <asp:Label ID="uxStatus" CssClass="Error" EnableViewState="false" runat="server"
                    Text=''></asp:Label>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="5" runat="server"></ZNode:Spacer>
            </div>
            <div>
                <div id="tablerow" class="OuterBorder" style="border: solid 5px #eff3f6; display: table;"
                    runat="server" visible="false">
                    <div class="LeftFloat" style="width: 25%; padding: 5px; border-right: solid 5px #eff3f6; display: inline-block;">
                        <asp:Image ID="CatalogItemImage" AlternateText="NA" runat="server" />
                    </div>
                    <div class="LeftFloat" style="width: 70%; padding: 5px;">
                        <asp:Label ID="ProductDescription" Text='' runat="server"></asp:Label>
                        <span>
                            <ZNode:ProductAttributeGrid ID="uxProductAttributeGrid" CssClassName="SizeGrid" runat="server" />
                        </span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <div align="right">
                        <div class="ImageButtons">
                            <asp:ImageButton ID="SubmitButton" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" ValidationGroup="grpCartItems" />
                            <asp:Button ID="btnAddProduct" runat="server" Text="Add Another Product" OnClick="BtnSubmitAnother_Click"
                                ValidationGroup="grpCartItems" />
                            <asp:ImageButton ID="CancelButton" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</div>
