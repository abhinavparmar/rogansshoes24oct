using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Orders.OrderManagement.OrderDesk
{
    /// <summary>
    /// Represents the SiteAdmin.Secure.Orders.Sales.OrderDesk - Customersearch user control class
    /// </summary>
    public partial class CustomerSearch : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeAddress _billingAddress = new ZNodeAddress();
        private ZNodeAddress _shippingAddress = new ZNodeAddress();
        private string _profileName = string.Empty;
        private string _loginName = string.Empty;
        private string PortalIds = string.Empty;
        private int OrderId = 0;
        private int CurrentPortalID = 0;
        #endregion

        #region Public Events
        
        public event System.EventHandler SelectedIndexChanged;
        
        public event System.EventHandler SearchButtonClick;
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the Account Object from/to page viewstate
        /// </summary>
        public Account UserAccount
        {
            get
            {
                if (ViewState["AccountObject"] != null)
                {
                    // The account info with 'AccountObject' is retrieved from the viewstate and
                    // It is converted to a Entity Account object
                    return (Account)ViewState["AccountObject"];
                }

                return new Account();
            }
            
            set
            {
                // Customer account object is placed in the viewstate and assigned a key, AccountObject.            
                ViewState["AccountObject"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the billing address
        /// </summary>
        public int BillingAddressID
        {
            get
            {
                if (ViewState["BillingAddressID"] != null)
                {
                    return (int)ViewState["BillingAddressID"];
                }

                return 0;
            }
            
            set
            {
                ViewState["BillingAddressID"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the shipping address
        /// </summary>
        public int ShippingAddressID
        {
            get
            {
                if (ViewState["ShippingAddressID"] != null)
                {
                    return (int)ViewState["ShippingAddressID"];
                }

                return 0;
            }
            
            set
            {
                ViewState["ShippingAddressID"] = value;
            }
        }

        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind Search Data Method
        /// </summary>
        public void BindSearchData()
        {
            //linkNewCustomerSearch.Visible = false; //Zeon Custom code
            ui_FoundUsers.DataBind();

            if (txtOrderID.Text.Trim().Length > 0)
            {
                this.OrderId = int.Parse(txtOrderID.Text.Trim());
            }

            CustomerAdmin customerAdmin = new CustomerAdmin();
            DataSet ds = customerAdmin.CustomerSearch(Server.HtmlEncode(FirstName.Text.Trim()), Server.HtmlEncode(LastName.Text.Trim()), Server.HtmlEncode(CompanyName.Text.Trim()), ZipCode.Text.Trim(), txtUserID.Text.Trim(), this.OrderId, this.CurrentPortalID, this.PortalIds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                // Bind account list grid
                ui_FoundUsers.DataSource = ds;
                ui_FoundUsers.DataBind();
            }
            else
            {
                lblSearhError.Text = "No users found.";
                //linkNewCustomerSearch.Visible = true; //Zeon Custom code
            }
        }

        /// <summary>
        /// Clear UI Method
        /// </summary>
        public void ClearUI()
        {
            ui_FoundUsers.DataBind();

            FirstName.Text = string.Empty;
            LastName.Text = string.Empty;
            ZipCode.Text = string.Empty;
            CompanyName.Text = string.Empty;
            txtOrderID.Text = string.Empty;
            txtUserID.Text = string.Empty;
            UpdatePnlSearch.Update();
            updatePnlFoundUsers.Update();
        }

        #endregion

        #region Page Load Event

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //linkNewCustomerSearch.Visible = false;//Zeon Custom code

            // Get Portals 
            this.PortalIds = UserStoreAccess.GetAvailablePortals;

            // Set session value for current store portal
            this.CurrentPortalID = ZNodeConfigManager.SiteConfig.PortalID;
        }
        #endregion

        #region General Events
        /// <summary>
        /// Event is raised when the "Search" Button is clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Ui_Search_Click(object sender, EventArgs e)
        {
            this.BindSearchData();
            updatePnlFoundUsers.Update();
        }

        /// <summary>
        /// Occurs when one of the pager buttons is clicked,
        /// but before the "FoundUsers" GridView control handles the paging operation.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Ui_FoundUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Retrieve Account Id
            Label userLogin = ui_FoundUsers.Rows[ui_FoundUsers.SelectedIndex].FindControl("ui_CustomerIdSelect") as Label;

            AccountService accountService = new AccountService();
            Account _account = accountService.GetByAccountID(int.Parse(userLogin.Text));

            if (_account != null)
            {
                this.UserAccount = _account;

                // Set Page session object
                // Get inserted Address Id
                AccountAdmin accountAdmin = new AccountAdmin();
                Address address = accountAdmin.GetDefaultBillingAddress(this.UserAccount.AccountID);
                if (address != null)
                {
                    this.BillingAddressID = address.AddressID;
                }

                address = accountAdmin.GetDefaultShippingAddress(this.UserAccount.AccountID);
                if (address != null)
                {
                    this.ShippingAddressID = address.AddressID;
                }

                // Set the user account object in session to be used for gift card processing.
                HttpContext.Current.Session["AliasUserAccount"] = this.UserAccount;

                ZNode.Libraries.DataAccess.Custom.AccountHelper accountHelper = new ZNode.Libraries.DataAccess.Custom.AccountHelper();
                int _ProfileID = accountHelper.GetCustomerProfile(_account.AccountID, this.CurrentPortalID);

                if (_ProfileID != 0)
                {
                    ZNode.Libraries.Admin.ProfileAdmin profileAdmin = new ZNode.Libraries.Admin.ProfileAdmin();

                    HttpContext.Current.Session["ProfileCache"] = profileAdmin.GetByProfileID(_ProfileID);
                }
            }

            if (this.SelectedIndexChanged != null)
            {
                // Triggers parent control event
                this.SelectedIndexChanged(sender, e);
            }
        }

        /// <summary>
        /// Occurs when one of the pager buttons is clicked, 
        /// but before the GridView control handles the paging operation.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Ui_FoundUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ui_FoundUsers.PageIndex = e.NewPageIndex;
            this.BindSearchData();
        }

        /// <summary>
        /// Event is raised when the "Cancel" Button is clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnCancel_Click(object sender, ImageClickEventArgs e)
        {
            if (this.SearchButtonClick != null)
            {
                this.SearchButtonClick(sender, e);
            }
        }
      
        /// <summary>
        /// Returns login name for a Provider access key(UserID)
        /// </summary>
        /// <param name="userId">The value of userId</param>
        /// <returns>Returns the Login name</returns>
        protected string GetLoginName(object userId) 
        {
            if (userId != null)
            {
                if (!string.IsNullOrEmpty(userId.ToString()))
                {
                    MembershipUser user = Membership.GetUser(userId);

                    if (user != null)
                    {
                        return user.UserName;
                    }
                }
            }

            return string.Empty;
        }
        
        #endregion

        //protected void btnCancel_Click(object sender, ImageClickEventArgs e)
        //{

        //}

        #region Zeon Custom Methods

        /// <summary>
        /// Bind User Name:Replace portalid 
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        protected string BindUserName(object username)
        {
            if (username != null)
            {
                return username.ToString().Replace("_" + this.CurrentPortalID, "");
            }
            return string.Empty;
        }

        /// <summary>
        /// Handles the RowCommand event of the ui_FoundUsers control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCommandEventArgs"/> instance containing the event data.</param>
        protected void ui_FoundUsers_RowCommand(Object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Browse")
            {
                // Retrieve Account Id
                int index = 0;
                if (e.CommandArgument != null)
                {
                    int.TryParse(e.CommandArgument.ToString(), out index);
                }

                Label userLogin = ui_FoundUsers.Rows[index].FindControl("ui_CustomerIdSelect") as Label;

                AccountService accountService = new AccountService();
                Account selectedUserAccount = accountService.GetByAccountID(int.Parse(userLogin.Text));
                if (selectedUserAccount != null)
                {                    
                    this.UserAccount = selectedUserAccount;

                    ZNodeUserAccount userAccountFromAdmin = new ZNodeUserAccount(selectedUserAccount);

                    // Set Page session object
                    // Get inserted Address Id
                    AccountAdmin accountAdmin = new AccountAdmin();
                    Address address = accountAdmin.GetDefaultBillingAddress(this.UserAccount.AccountID);
                    if (address != null)
                    {
                        this.BillingAddressID = address.AddressID;
                        userAccountFromAdmin.BillingAddress = address;
                    }

                    address = accountAdmin.GetDefaultShippingAddress(this.UserAccount.AccountID);
                    if (address != null)
                    {
                        this.ShippingAddressID = address.AddressID;
                        userAccountFromAdmin.ShippingAddress = address;
                    }

                    // Set the user account object in session to be used for gift card processing.
                    HttpContext.Current.Session["AliasUserAccount"] = this.UserAccount;

                    ZNode.Libraries.DataAccess.Custom.AccountHelper accountHelper = new ZNode.Libraries.DataAccess.Custom.AccountHelper();
                    int _ProfileID = accountHelper.GetCustomerProfile(selectedUserAccount.AccountID, this.CurrentPortalID);

                    if (_ProfileID != 0)
                    {
                        ZNode.Libraries.Admin.ProfileAdmin profileAdmin = new ZNode.Libraries.Admin.ProfileAdmin();

                        HttpContext.Current.Session["ProfileCache"] = profileAdmin.GetByProfileID(_ProfileID);
                    }
                                                          
                    Session["BuyerUser"] = userAccountFromAdmin;

                    RedirectToSiteUrl(GetRedirectUrl(), Resources.CommonCaption.AdminCreateOrderTarget, string.Empty);

                }
                if (this.SelectedIndexChanged != null)
                {
                    // Triggers parent control event
                    this.SelectedIndexChanged(sender, e);
                }
            }
        }

        /// <summary>
        /// Grmerate url to redirect
        /// </summary>
        /// <returns>url</returns>
        private string GetRedirectUrl()
        {
            string redirectUrl = string.Empty;
            string domainName = string.Empty;
            string protocol = (ZNodeConfigManager.SiteConfig.UseSSL ? "https://" : "http://");
            redirectUrl ="~/default.aspx";

            //DomainService domainService = new DomainService();
            //TList<Domain> portalList = domainService.GetByPortalID(CurrentPortalID);
            //if (portalList != null)
            //{               
            //    domainName = portalList[0].DomainName;
            //    string protocol = (ZNodeConfigManager.SiteConfig.UseSSL ? "https://" : "http://");
            //    redirectUrl = protocol + domainName + "/default.aspx";
            //}

            return redirectUrl;
        }

        /// <summary>
        /// Redirecting to site
        /// </summary>
        /// <param name="url">url for site</param>
        /// <param name="target"></param>
        /// <param name="windowFeatures"></param>
        private void RedirectToSiteUrl(string url, string target, string windowFeatures)
        {
            HttpContext context = HttpContext.Current;
            if ((String.IsNullOrEmpty(target) || target.Equals(Resources.CommonCaption.AdminCreateOrderTarget, StringComparison.OrdinalIgnoreCase)) && String.IsNullOrEmpty(windowFeatures))
            {
                context.Response.Redirect(url);
            }
            else
            {
                Page page = (Page)context.Handler;
                if (page == null)
                {
                    throw new InvalidOperationException("Cannot redirect to new window outside Page context.");
                }
                url = page.ResolveClientUrl(url);
                string script;
                if (!String.IsNullOrEmpty(windowFeatures))
                {
                    script = @"window.open(""{0}"", ""{1}"", ""{2}"");";
                }
                else
                {
                    script = @"window.open(""{0}"", ""{1}"");";
                }
                script = String.Format(script, url, target, windowFeatures);
                ScriptManager.RegisterStartupScript(page, typeof(Page), "Redirect", script, true);
            }
        }
        
        #endregion
    }
}