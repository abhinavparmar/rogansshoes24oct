﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Orders.OrderManagement.OrderDesk
{
    /// <summary>
    /// Represents the SiteAdmin.Secure.Orders.Sales.OrderDesk - OrderDesk Receipt class
    /// </summary>
    public partial class OrderDeskReceipt : System.Web.UI.Page
    {
        #region Events
        /// <summary>
        /// Fires when New Customer link is triggered
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LinkNewOrder_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Orders/OrderManagement/OrderDesk/Default.aspx");
        }

        #endregion
    }
}