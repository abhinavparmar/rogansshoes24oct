<%@ Control Language="C#" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Orders.OrderManagement.OrderDesk.OrderPayment"
    CodeBehind="OrderPayment.ascx.cs" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/spacer.ascx" TagName="spacer" TagPrefix="uc1" %>
<%@ Register TagPrefix="ZNode" Assembly="ZNode.Libraries.ECommerce.Catalog" Namespace="ZNode.Libraries.ECommerce.Catalog" %>
<div>
    <div>
        <h4 class="SubTitle">Shipping</h4>
    </div>
    <asp:Panel ID="pnlShipping" runat="server" Visible="true">
        <div class="FormView Size100">
            <div class="FieldStyle">
                Shipping Option
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="lstShipping" runat="server" AutoPostBack="True" OnSelectedIndexChanged="LstShipping_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="ClearBoth">
                <asp:Literal ID="uxErrorMsg" EnableViewState="false" runat="server"></asp:Literal>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlPayment" runat="server" Visible="true">
        <div>
            <h4 class="SubTitle">Payment</h4>
        </div>
        <div class="FormView Size100">
            <div class="FieldStyle">
                Payment Option
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="lstPaymentType" runat="server" OnSelectedIndexChanged="LstPaymentType_SelectedIndexChanged"
                    AutoPostBack="true">
                </asp:DropDownList>
            </div>
        </div>
        <!-- COD payment section -->
        <asp:Panel ID="pnlCOD" runat="server" Visible="false">
            You have selected the COD option for payment. You would need to pay the entire amount
            on delivery.
        </asp:Panel>
        <!-- purchase order payment section -->
        <asp:Panel ID="pnlPurchaseOrder" runat="server" Visible="false">
            <div class="FormView Size100">
                <div class="FieldStyle">
                    P.O. Number
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtPONumber" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" ErrorMessage="Enter Purchase Order Number"
                            ControlToValidate="txtPONumber" runat="server" Display="Dynamic" CssClass="Error"
                            ValidationGroup="groupPayment"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <div class="ClearBoth">
        </div>
        <!-- Credit card payment section-->
        <asp:Panel ID="pnlCreditCard" runat="server">
            <div class="FormView Size100">
                <div class="FieldStyle">
                    Card Number
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtCreditCardNumber" runat="server" Width="130" Columns="30" MaxLength="20"
                        autoComplete="off"></asp:TextBox>&nbsp;&nbsp;<img id="imgVisa" src="~/SiteAdmin/Themes/images/card_visa.gif"
                            runat="server" align="absmiddle" /><img id="imgMastercard" src="~/SiteAdmin/Themes/images/card_mastercard.gif"
                                runat="server" align="absmiddle" /><img id="imgAmex" src="~/SiteAdmin/Themes/images/card_amex.gif"
                                    align="absmiddle" runat="server" /><img id="imgDiscover" src="~/SiteAdmin/Themes/images/card_discover.gif"
                                        align="absmiddle" runat="server" /><br />
                    <div>
                        <asp:RequiredFieldValidator ID="req1" ErrorMessage="Enter Credit Card Number" ControlToValidate="txtCreditCardNumber"
                            runat="server" Display="Dynamic" CssClass="Error" ValidationGroup="groupPayment"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtCreditCardNumber"
                            Display="Dynamic" ErrorMessage="* Enter Valid Credit Card Number" ValidationExpression="^[2|3|4|5|6]([0-9]{15}$|[0-9]{12}$|[0-9]{13}$|[0-9]{14}$)"
                            ValidationGroup="groupPayment"></asp:RegularExpressionValidator>
                    </div>
                    <br />
                    <ZNode:ZNodeCreditCardValidator ID="MyValidator" ControlToValidate="txtCreditCardNumber"
                        ErrorMessage="Please enter a Valid Credit Card Number" Display="Dynamic" runat="server"
                        ValidateCardType="True" ValidationGroup="groupPayment" />
                </div>
            </div>
            <div class="ClearForm">
            </div>
            <div class="FormView Size100">
                <div class="FieldStyle">
                    Expiration Date
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="lstMonth" runat="server">
                        <asp:ListItem Value="">-- Month --</asp:ListItem>
                        <asp:ListItem Value="01">Jan</asp:ListItem>
                        <asp:ListItem Value="02">Feb</asp:ListItem>
                        <asp:ListItem Value="03">Mar</asp:ListItem>
                        <asp:ListItem Value="04">Apr</asp:ListItem>
                        <asp:ListItem Value="05">May</asp:ListItem>
                        <asp:ListItem Value="06">Jun</asp:ListItem>
                        <asp:ListItem Value="07">Jul</asp:ListItem>
                        <asp:ListItem Value="08">Aug</asp:ListItem>
                        <asp:ListItem Value="09">Sep</asp:ListItem>
                        <asp:ListItem Value="10">Oct</asp:ListItem>
                        <asp:ListItem Value="11">Nov</asp:ListItem>
                        <asp:ListItem Value="12">Dec</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                    <asp:DropDownList ID="lstYear" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="FormView Size100">
                <div class="FieldStyle">
                    Security Code
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtCVV" runat="server" Width="30" autoComplete="off" Columns="30"
                        MaxLength="4"></asp:TextBox>&nbsp;&nbsp;<a href="javascript:popupWin=window.open('../../../../../controls/default/cvv/cvv.htm','EIN','scrollbars,resizable,width=515,height=300,left=50,top=50');popupWin.focus();"
                            runat="server">help</a><div>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" ErrorMessage="Enter Credit Card Security Code"
                                    ControlToValidate="txtCVV" runat="server" Display="Dynamic" CssClass="Error"
                                    ValidationGroup="groupPayment"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <div class="ClearForm">
        </div>
    </asp:Panel>
    <asp:Panel ID="Panel1" runat="server">
        <div class="FormView Size100">
            <!-- Gift Card Section -->
            <asp:Panel ID="pnlGiftCardNumber" runat="server">
                <div>
                    <div class="FieldStyle LeftContent">
                        Gift Card Number
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtGiftCardNumber" runat="server" EnableViewState="true" Width="130"
                            Columns="30" MaxLength="100" ValidationGroup="GiftCard"></asp:TextBox>&nbsp;
                        <asp:ImageButton ID="btnApplyGiftCard" ImageAlign="Top" ImageUrl="~/SiteAdmin/Themes/images/Apply.gif"
                            runat="server" AlternateText="Apply" CssClass="gobutton" ValidationGroup="GiftCard"
                            OnClick="BtnApplyGiftCard_Click"></asp:ImageButton>
                    </div>
                </div>
                <div class="ClearBoth">
                </div>
                <div>
                    <asp:Label ID="lblGiftCardMessage" runat="server" CssClass="Error" Text=""></asp:Label>
                </div>
            </asp:Panel>
        </div>
    </asp:Panel>
    <div class="ClearBoth">
        <uc1:spacer ID="Spacer9" SpacerHeight="15" SpacerWidth="5" runat="server" />
        <div class="HintStyle">
            Please enter any comments or special instructions for your order
        </div>
        <div>
            <uc1:spacer ID="Spacer15" SpacerHeight="15" SpacerWidth="5" runat="server" />
            <asp:TextBox Columns="45" Rows="3" ID="txtAdditionalInstructions" runat="server"
                TextMode="MultiLine"></asp:TextBox>
        </div>
    </div>
</div>
