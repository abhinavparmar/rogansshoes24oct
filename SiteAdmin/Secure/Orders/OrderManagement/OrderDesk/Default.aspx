<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Orders.OrderManagement.OrderDesk.Default"
    ValidateRequest="false" CodeBehind="Default.aspx.cs" %>

<%@ Register Src="Confirm.ascx" TagName="Confirm" TagPrefix="ZNode" %>
<%@ Register Src="OrderPayment.ascx" TagName="Payment" TagPrefix="ZNode" %>
<%@ Register Src="QuickOrder.ascx" TagName="QuickOrder" TagPrefix="ZNode" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/ShoppingCart.ascx" TagName="ShoppingCart"
    TagPrefix="ZNode" %>
<%@ Register Src="CreateUser.ascx" TagName="CreateUser" TagPrefix="ZNode" %>
<%@ Register Src="CustomerSearch.ascx" TagName="CustomerSearch" TagPrefix="ZNode" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/StoreName.ascx" TagName="StoreName" TagPrefix="ZNode" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <script type="text/JavaScript" language="JavaScript">
        function pageLoad() {
            var manager = Sys.WebForms.PageRequestManager.getInstance();
            manager.add_endRequest(endRequest);
            manager.add_beginRequest(OnBeginRequest);
        }
        function OnBeginRequest(sender, args) {

            document.getElementById("OrderDesk").disabled = true;
        }
        function endRequest(sender, args) {

            document.getElementById("OrderDesk").disabled = false;
        }
    </script>

    <div class="OrderDesk" id="OrderDesk">
        <asp:UpdatePanel ID="UpdatePanelOrderDesk" runat="server" UpdateMode="Conditional"
            ChildrenAsTriggers="true" RenderMode="Inline">
            <ContentTemplate>
                <div class="OrderDeskContent">
                    <div class="LeftFloat" style="width: 60%">
                        <h1>Create an Order</h1>
                    </div>
                    <div class="ClearBoth">
                    </div>
                    <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
                    <!-- Ajax Modal Popup Box  -->
                    <asp:Panel ID="pnlOrderDesk" runat="server">
                        <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
                        <asp:Button ID="btnShowPopup1" runat="server" Style="display: none" />
                        <asp:Button ID="btnShowQuickOrder" runat="server" Style="display: none" />
                        <ajaxToolKit:ModalPopupExtender ID="mdlPopup" runat="server" TargetControlID="btnShowPopup"
                            PopupControlID="pnlSelectUsers" BackgroundCssClass="modalBackground" />
                        <ajaxToolKit:ModalPopupExtender ID="mdlCreateUserPopup" runat="server" TargetControlID="btnShowPopup1"
                            PopupControlID="pnlCreateUser" BackgroundCssClass="modalBackground" />
                        <ajaxToolKit:ModalPopupExtender ID="mdlQuickOrderPopup" runat="server" TargetControlID="btnShowQuickOrder"
                            PopupControlID="pnlQuickOrder" BackgroundCssClass="modalBackground" />



                        <div>
                            <div style="float: left;">
                                <!-- Store Section -->
                                <asp:Panel ID="pnlSelectStore" runat="server">
                                    <div class="ValueStyle">
                                        <asp:DropDownList ID="ddlPortal" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlPortal_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvPortal" runat="server" SetFocusOnError="true"
                                            ControlToValidate="ddlPortal" InitialValue="0"
                                            ErrorMessage="Please select store"
                                            ToolTip="Please select store"
                                            ValidationGroup="AdminOrderGRP" CssClass="Error"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </asp:Panel>
                            </div>

                            <div style="float: left; margin-left: 50px;">
                                <!-- Catalog Section -->
                                <asp:Panel ID="pnlSelectCatalog" runat="server">
                                    <div class="ValueStyle">
                                        <asp:DropDownList ID="ddlCatalog" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlCatalog_SelectedIndexChanged">
                                        </asp:DropDownList>
                                         <asp:RequiredFieldValidator ID="rfvCatalog" runat="server" SetFocusOnError="true"
                                            ControlToValidate="ddlCatalog" InitialValue="0"
                                            ErrorMessage="Please select catalog"
                                            ToolTip="Please select catalog"
                                            ValidationGroup="AdminOrderGRP" CssClass="Error"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </asp:Panel>
                                <!-- Customer Section -->
                            </div>
                        </div>
                        <div class="ClearBoth"></div>

                        <asp:Panel ID="pnltitle" runat="server" Visible="false">
                            <h4 class="SubTitle" style="padding-bottom: 0px;">Customer</h4>
                        </asp:Panel>

                        <asp:Panel ID="ui_pnlAcctInfo" runat="server" Visible="false">

                            <div class="LeftFloat" style="padding-bottom: 20px;">
                                <div>
                                    <strong>Profile</strong>&nbsp;&nbsp;&nbsp;<asp:Label ID="ui_lblProfile" runat="server"
                                        Text=''></asp:Label>
                                </div>
                                <div>
                                    <strong>User ID</strong>&nbsp;<asp:Label ID="ui_lblUserID" runat="server" Text=''></asp:Label>
                                </div>
                            </div>
                            <div class="ClearBoth"></div>
                            <div class="LeftFloat" style="width: 300px; word-wrap: break-word;">
                                <strong>Billing Address:</strong><br />
                                <asp:Label ID="ui_BillingAddress" runat="server" Text=''></asp:Label>
                            </div>
                            <div class="LeftFloat" style="width: 400px; word-wrap: break-word; padding-left: 20px">
                                <strong>Shipping Address:</strong><br />
                                <asp:Label ID="ui_ShippingAddress" runat="server" Text=''></asp:Label>
                            </div>
                            <div class="ClearBoth" style="padding-top: 10px;">
                                <div class="LeftFloat ImageButtons" style="width: 300px;">
                                    <asp:Button ID="ui_Edit" runat="server" CausesValidation="false" Text="Edit Address"
                                        CssClass="Size210" Width="210px" OnClick="Ui_Edit_Click" />
                                </div>

                            </div>
                        </asp:Panel>
                        <div class="ClearBoth"></div>
                        <div>
                            <uc1:Spacer ID="Spacer1" SpacerWidth="1" SpacerHeight="10" runat="server" />
                        </div>
                        <asp:Panel ID="pnlSelectCustomer" runat="server">

                            <asp:UpdatePanel ID="UpdatePnlSelecCustomer" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="LinkButton">
                                        <asp:LinkButton CausesValidation="true" ID="linkSearchCustomer" runat="server" Text="Search Existing Customers"  ValidationGroup="AdminOrderGRP"
                                            OnClick="LinkSearchCustomer_Click" />
                                    </div>
                                    <div class="LinkButton">
                                        <asp:LinkButton CausesValidation="false" ID="linkNewCustomer" runat="server" Text="Add New Customer"
                                            OnClick="LinkNewCustomer_Click" Visible="false" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>
                        <asp:Panel ID="pnlSelectUsers" runat="server" Style="display: none;" CssClass="PopupStyle">
                            <div>
                                <ZNode:CustomerSearch ID="uxCustomerSearch" runat="server" />
                            </div>
                        </asp:Panel>

                        <!-- Customer Section -->
                        <!-- Create Customer Popup -->
                        <asp:Panel ID="pnlCreateUser" Style="display: none;" runat="server" CssClass="PopupStyle">
                            <div>
                                <ZNode:CreateUser ID="uxCreateUser" runat="server" />
                            </div>
                        </asp:Panel>
                        <!-- Quick Order Section-->
                        <asp:Panel ID="pnlQuickOrder" runat="server" CssClass="PopupStyle" Style="display: none;">
                            <asp:UpdatePanel ID="UpdatePanelQuickOrder" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="QuickOrder">
                                        <ZNode:QuickOrder ID="uxQuickOrder" runat="server" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>
                        <!-- Shopping Cart section -->
                        <asp:Panel ID="pnlCart" runat="server" CssClass="ShoppingCart" Visible="false">
                            <h4 class="SubTitle">Shopping Cart</h4>
                            <div>
                                <asp:LinkButton ID="linkbtnQuickOrder" runat="server" Text="Search Products" OnClick="LinkbtnQuickOrder_Click"></asp:LinkButton>
                            </div>
                            <div>
                                <uc1:Spacer ID="Spacer" SpacerWidth="1" SpacerHeight="10" runat="server" />
                            </div>
                            <div>
                                <ZNode:ShoppingCart ID="uxShoppingCart" ShowTaxShipping="true" runat="server" />
                            </div>
                        </asp:Panel>
                        <!-- Payment and Shipping Section -->
                        <asp:Panel ID="pnlPaymentShipping" runat="server" CssClass="Payment" Visible="false">
                            <div>
                                <ZNode:Payment ID="uxPayment" runat="server" />
                            </div>
                        </asp:Panel>
                        <div>
                            <asp:Label ID="lblError" runat="server" EnableViewState="false" CssClass="Error"></asp:Label>
                        </div>
                        <div>
                            <uc1:Spacer ID="Spacer8" SpacerWidth="1" SpacerHeight="15" runat="server" />
                        </div>
                        <div align="right" class="ImageButtons">
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit Order" ValidationGroup="groupPayment"
                                CssClass="Size100" OnClick="Submit_Click" Visible="false" />
                            <asp:ImageButton ID="btnCancelOrder" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                                runat="server" AlternateText="Cancel" OnClick="BtnCancelOrder_Click" />

                            <div>
                                <uc1:Spacer ID="Spacer9" SpacerWidth="1" SpacerHeight="10" runat="server" />
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:UpdateProgress ID="uxSearchUpdateProgress" runat="server" AssociatedUpdatePanelID="UpdatePanelOrderDesk"
        DisplayAfter="1">
        <ProgressTemplate>
            <div id="ajaxProgressBg">
            </div>
            <div id="ajaxProgress">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
