<%@ Control Language="C#" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Orders.OrderManagement.OrderDesk.CustomerSearch" CodeBehind="CustomerSearch.ascx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<div>
    <h4 class="SubTitle">Search Customers</h4>
    <asp:UpdatePanel ID="UpdatePnlSearch" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel CssClass="CustomerSearch" ID="ui_pnlSearchParams" runat="server" DefaultButton="ui_Search">

                <div class="SearchForm">
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle">First Name</span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="FirstName" runat="server" ValidationGroup="groupSearch"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">Last Name</span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="LastName" runat="server" ValidationGroup="groupSearch"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">Company Name</span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="CompanyName" runat="server" ValidationGroup="groupSearch"></asp:TextBox></span>
                        </div>
                    </div>
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle">Zip</span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="ZipCode" runat="server" ValidationGroup="groupSearch"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">User ID</span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtUserID" runat="server" ValidationGroup="groupSearch"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">Order ID</span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtOrderID" runat="server" ValidationGroup="groupSearch"></asp:TextBox>
                                <asp:RegularExpressionValidator ValidationGroup="groupSearch" ValidationExpression="^\d{1,7}$"
                                    ID="val1" ControlToValidate="txtOrderID" ErrorMessage="*invalid" runat="server"
                                    Display="Dynamic" CssClass="Error" SetFocusOnError="true"></asp:RegularExpressionValidator></span>
                        </div>
                    </div>
                    <div class="ClearBoth">
                        <asp:Label CssClass="Error" ID="lblSearhError" runat="server" EnableViewState="false" /><br />
                        <br />
                        <asp:ImageButton ID="ui_Search" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                            onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                            runat="server" OnClick="Ui_Search_Click" AlternateText="Search" />
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <asp:Panel ID="pnlSelectUsers" runat="server" Width="100%">
        <asp:UpdatePanel ID="updatePnlFoundUsers" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div>
                    <h4 class="GridTitle">Select Customer</h4>
                   <div class="ImageButtons">
                        <asp:Button CausesValidation="false" ID="linkNewCustomerSearch" runat="server" Text="Add New Customer" PostBackUrl="~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/editcustomer.aspx?pagefrom=ORDER" />                        
                    </div>
                   
                    <asp:GridView ID="ui_FoundUsers" CellPadding="0" CssClass="Grid" runat="server" AutoGenerateColumns="False"
                        OnSelectedIndexChanged="Ui_FoundUsers_SelectedIndexChanged" AllowPaging="True" OnRowCommand="ui_FoundUsers_RowCommand"
                        OnPageIndexChanging="Ui_FoundUsers_PageIndexChanging" Width="100%" GridLines="None">
                        <Columns>
                            <asp:ButtonField ButtonType="Link" Text="Select" CommandName="Browse" />
                            <asp:CommandField ShowSelectButton="true" ValidationGroup="groupSearch" ButtonType="Link"
                                SelectText="SELECT" HeaderStyle-HorizontalAlign="Left" Visible="false" />
                            <asp:TemplateField HeaderText="Login" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="ui_CustomerIdSelect" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"AccountID") %>'></asp:Label></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Last Name" DataField="BillingLastName" HeaderStyle-HorizontalAlign="Left" />
                            <asp:BoundField HeaderText="First Name" DataField="BillingFirstName" HeaderStyle-HorizontalAlign="Left" />
                            <asp:BoundField HeaderText="Company" DataField="BillingCompanyName" HeaderStyle-HorizontalAlign="Left" />
                            <asp:TemplateField HeaderText="Address" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "BillingStreet") + " " + DataBinder.Eval(Container.DataItem, "BillingStreet1")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="City" DataField="BillingCity" HeaderStyle-HorizontalAlign="Left" />
                            <asp:BoundField HeaderText="St" DataField="BillingStateCode" HeaderStyle-HorizontalAlign="Left" />
                            <asp:BoundField HeaderText="Zip" DataField="BillingPostalCode" HeaderStyle-HorizontalAlign="Left" />
                            <asp:BoundField HeaderText="Phone" DataField="BillingPhoneNumber" HeaderStyle-HorizontalAlign="Left" />
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="User ID">
                                <ItemTemplate>
                                    <%# BindUserName(DataBinder.Eval(Container.DataItem, "UserName")) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField DataField="UserName" HeaderStyle-HorizontalAlign="Left" HeaderText="User ID" />--%>
                        </Columns>
                        <EditRowStyle CssClass="EditRowStyle" />
                        <FooterStyle CssClass="FooterStyle" />
                        <RowStyle CssClass="RowStyle" />
                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                        <PagerStyle CssClass="PagerStyle" />
                        <HeaderStyle CssClass="HeaderStyle" />
                        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    </asp:GridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div>
            &nbsp;
        </div>
        <div align="right" class="ImageButtons">
            <%-- <asp:Button ID="btnClose" runat="server" CausesValidation="false" CssClass="Size100"
                Text="Close" OnClick="BtnClose_Click" />--%>


            <%--   <asp:ImageButton ID="btnClearSearch" CausesValidation="false" runat="server"  
                                ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif" />--%>


            <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="btnCancel_Click" />

        </div>
    </asp:Panel>
</div>
