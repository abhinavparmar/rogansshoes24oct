<%@ Control Language="C#" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Orders.OrderManagement.OrderDesk.CreateUser"
    CodeBehind="CreateUser.ascx.cs" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/spacer.ascx" TagName="spacer" TagPrefix="uc1" %>
<div class="FormView">
    <h4 class="SubTitle">update customer address</h4>
    <div>
        <uc1:spacer ID="Spacer1" SpacerHeight="4" SpacerWidth="10" runat="server"></uc1:spacer>
    </div>
    <asp:UpdatePanel ID="pnlCustomerDetail" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div>
                <div class="LeftFloat">
                    <div>
                        <div class="HeaderStyle" colspan="2">
                            Billing Address
                        </div>
                    </div>
                    <div runat="server" id="divBillingAddressName">
                        <div class="FieldStyle">
                            <asp:Label ID="lbl" runat="server" Text="Address Name"></asp:Label>
                        </div>
                        <div class="ValueStyle">
                            <asp:DropDownList ID="ddlBillingAddressName" Width="180px" runat="server" AutoPostBack="True"
                                OnSelectedIndexChanged="DdlAddressName_SelectedIndexChanged">
                            </asp:DropDownList>
                            <br />
                        </div>
                    </div>
                    <div class="FieldStyle">
                        First Name<span class="Asterix">*</span>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtBillingFirstName" ValidationGroup="groupCreateUser" runat="server"
                            Width="130" Columns="30" MaxLength="100"></asp:TextBox><div>
                                <asp:RequiredFieldValidator ID="req1" ErrorMessage="Enter First Name" ControlToValidate="txtBillingFirstName"
                                    runat="server" Display="Dynamic" CssClass="Error" ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="FieldStyle">
                        Last Name<span class="Asterix">*</span>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtBillingLastName" ValidationGroup="groupCreateUser" runat="server"
                            Width="130" Columns="30" MaxLength="100"></asp:TextBox><div>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" ErrorMessage="Enter Last Name"
                                    ControlToValidate="txtBillingLastName" runat="server" Display="Dynamic" CssClass="Error"
                                    ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="FieldStyle">
                        Company Name
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtBillingCompanyName" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox>
                    </div>
                    <div class="FieldStyle">
                        Street 1<span class="Asterix">*</span>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtBillingStreet1" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox><div>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" ErrorMessage="Enter Street"
                                ControlToValidate="txtBillingStreet1" runat="server" Display="Dynamic" CssClass="Error"
                                ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="FieldStyle">
                        Street 2
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtBillingStreet2" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox>
                    </div>
                    <div class="FieldStyle">
                        City<span class="Asterix">*</span>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtBillingCity" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox><div>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" ErrorMessage="Enter City"
                                ControlToValidate="txtBillingCity" runat="server" Display="Dynamic" CssClass="Error"
                                ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="FieldStyle">
                        State<span class="Asterix">*</span>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtBillingState" runat="server" Width="30" Columns="10" MaxLength="2"></asp:TextBox><div>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" ErrorMessage="Enter State"
                                ControlToValidate="txtBillingState" runat="server" Display="Dynamic" CssClass="Error"
                                ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="FieldStyle">
                        Postal Code<span class="Asterix">*</span>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtBillingPostalCode" runat="server" Width="130" Columns="30" MaxLength="10"></asp:TextBox><div>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator12" ErrorMessage="Enter Postal Code"
                                ControlToValidate="txtBillingPostalCode" runat="server" Display="Dynamic" CssClass="Error"
                                ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="FieldStyle">
                        Country
                    </div>
                    <div class="ValueStyle">
                        <asp:DropDownList ID="lstBillingCountryCode" Width="200" runat="server">
                        </asp:DropDownList>
                        <div>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator13" ErrorMessage="Select Country Code"
                                ControlToValidate="lstBillingCountryCode" runat="server" Display="Dynamic" CssClass="Error"
                                ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="FieldStyle">
                        Phone Number<span class="Asterix">*</span>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtBillingPhoneNumber" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox><div>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" ErrorMessage="Enter Phone Number"
                                ControlToValidate="txtBillingPhoneNumber" runat="server" Display="Dynamic" CssClass="Error"
                                ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="FieldStyle">
                        Email Address<span class="Asterix">*</span>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtEmailAddress" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox><div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" Width="450px"
                                ValidationGroup="groupCreateUser" ControlToValidate="txtEmailAddress" CssClass="Error"
                                Display="Dynamic" ErrorMessage="Enter Email Address">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revEmail" runat="server" Width="10px" ControlToValidate="txtEmailAddress"
                                ErrorMessage='<%$ Resources:CommonCaption, EmailValidErrorMessage %>' Display="Dynamic"
                                ValidationExpression='<%$ Resources:CommonCaption, EmailValidationExpression%>'
                                ValidationGroup="groupCreateUser" CssClass="Error">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                </div>
                <div class="LeftFloat" style="padding-left: 50px">
                    <asp:Panel ID="pnlShipping" runat="server" Visible="true">
                        <div class="Form">
                            <div class="HeaderStyle" colspan="2">
                                Shipping Address
                            </div>
                            <div runat="server" id="divShippingAddressName">
                                <div class="FieldStyle">
                                    <asp:Label ID="Label1" runat="server" Text="Address Name"></asp:Label>
                                </div>
                                <div class="ValueStyle">
                                    <asp:DropDownList ID="ddlShippingAddressName" runat="server" AutoPostBack="True"
                                        Width="180px" OnSelectedIndexChanged="DdlAddressName_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <br />
                                </div>
                            </div>
                            <div class="FieldStyle">
                                First Name<span class="Asterix">*</span>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtShippingFirstName" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" ErrorMessage="Enter First Name"
                                        ControlToValidate="txtShippingFirstName" runat="server" Display="Dynamic" CssClass="Error"
                                        ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="FieldStyle">
                                Last Name<span class="Asterix">*</span>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtShippingLastName" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator7" ErrorMessage="Enter Last Name"
                                        ControlToValidate="txtShippingLastName" runat="server" Display="Dynamic" CssClass="Error"
                                        ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="FieldStyle">
                                Company Name
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtShippingCompanyName" runat="server" Width="130" Columns="30"
                                    MaxLength="100"></asp:TextBox>
                            </div>
                            <div class="FieldStyle">
                                Street 1<span class="Asterix">*</span>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtShippingStreet1" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator9" ErrorMessage="Enter Street"
                                        ControlToValidate="txtShippingStreet1" runat="server" Display="Dynamic" CssClass="Error"
                                        ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="FieldStyle">
                                Street 2
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtShippingStreet2" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox>
                            </div>
                            <div class="FieldStyle">
                                City<span class="Asterix">*</span>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtShippingCity" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator10" ErrorMessage="Enter City"
                                        ControlToValidate="txtShippingCity" runat="server" Display="Dynamic" CssClass="Error"
                                        ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="FieldStyle">
                                State<span class="Asterix">*</span>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtShippingState" runat="server" Width="30" Columns="10" MaxLength="2"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator11" ErrorMessage="Enter State"
                                        ControlToValidate="txtShippingState" runat="server" Display="Dynamic" CssClass="Error"
                                        ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="FieldStyle">
                                Postal Code<span class="Asterix">*</span>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtShippingPostalCode" runat="server" Width="130" Columns="30" MaxLength="10"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator14" ErrorMessage="Enter Postal Code"
                                        ControlToValidate="txtShippingPostalCode" runat="server" Display="Dynamic" CssClass="Error"
                                        ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="FieldStyle">
                                Country
                            </div>
                            <div class="ValueStyle">
                                <asp:DropDownList ID="lstShippingCountryCode" Width="200" runat="server">
                                </asp:DropDownList>
                                <div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator15" ErrorMessage="Select Country Code"
                                        ControlToValidate="lstShippingCountryCode" runat="server" Display="Dynamic" CssClass="Error"
                                        ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="FieldStyle">
                                Phone Number<span class="Asterix">*</span>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtShippingPhoneNumber" runat="server" Width="130" Columns="30"
                                    MaxLength="100"></asp:TextBox><div>
                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator8" ErrorMessage="Enter Phone Number"
                                            ControlToValidate="txtShippingPhoneNumber" runat="server" Display="Dynamic" CssClass="Error"
                                            ValidationGroup="groupCreateUser"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <div class="ClearBoth">
                </div>
                <div class="FieldStyle">
                </div>
                <div class="ValueStyle">
                    <asp:CheckBox ID="chkSameAsBilling" runat="server" Text="Shipping Address is same as Billing"
                        Checked="false" OnCheckedChanged="ChkSameAsBilling_CheckedChanged" AutoPostBack="true" />
                </div>
                <div class="Error">
                    <asp:Literal ID="lblError" runat="server" EnableViewState="false"></asp:Literal>
                </div>
                <div>
                    <uc1:spacer ID="Spacer2" SpacerHeight="4" SpacerWidth="10" runat="server"></uc1:spacer>
                </div>
                <br />
                <div>
                    <div class="ClearBoth">
                        <div class="ImageButtons" align="center">
                            <asp:Button ID="btnUpdate" runat="server" Text="Create Account" OnClick="BtnUpdate_Click"
                                CssClass="Size200" ValidationGroup="groupCreateUser" />
                            <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
