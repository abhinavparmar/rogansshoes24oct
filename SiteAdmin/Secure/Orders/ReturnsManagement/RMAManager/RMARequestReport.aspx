﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="RMARequestReport.aspx.cs"
    Inherits="SiteAdmin.Secure.Orders.ReturnsManagement.RMAManager.RMARequestReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<html>
<head>
    <title>RMA Request Report</title>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />

    <style type="text/css">
        #ImageButtons .RMA100
        {
            clear: both;
            background-image: url(../../../../Themes/Images/buttons/button_empty_100.gif);
            background-position: center center;
            background-repeat: no-repeat;
            text-align: center;
            border: none;
            width: 100px;
            height: 19px;
            background-color: White;
            color: White;
            font-size: 11px;
            font-weight: normal;
            vertical-align: 5px;
            text-transform: uppercase;
        }

            #ImageButtons .RMA100:hover
            {
                clear: both;
                background-image: url(../../../../Themes/Images/buttons/button_empty_highlight_100.gif);
                background-position: center center;
                background-repeat: no-repeat;
                text-align: center;
                border: none;
                width: 100px;
                height: 19px;
                background-color: White;
                color: White;
                font-size: 11px;
                font-weight: normal;
                vertical-align: 5px;
                text-transform: uppercase;
                cursor: pointer;
            }

        .Msg
        {
            font-family: arial,Calibri;
            font-size: 12px;
        }
    </style>
</head>
<body>
    <form runat="server" id="rmareport">
        <ajaxToolKit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            <Scripts>
                <asp:ScriptReference Path="~/js/Safari3AjaxHack.js" />
            </Scripts>
        </ajaxToolKit:ToolkitScriptManager>
        <div>
            <div id="ImageButtons" style="text-align: right;">
                <asp:Button CssClass="RMA100" ID="btnSendMail" Text="Send Mail" runat="server" OnClick="btnSendMail_Click"></asp:Button>

                <asp:Button CssClass="RMA100" ID="Button1" Text="Close" runat="server" OnClientClick="javascript:window.close();"></asp:Button>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div>
                <asp:Label ID="lblmsg" runat="server" CssClass="Msg"></asp:Label>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div>
                <rsweb:ReportViewer ShowBackButton="False" ID="objReportViewer" runat="server" Width="100%" AsyncRendering="false" Height="650px"
                    Font-Names="Verdana" Font-Size="8pt" ShowExportControls="true" ShowPageNavigationControls="true"
                    ShowCredentialPrompts="false" ShowDocumentMapButton="false" ShowFindControls="false"
                    ShowPrintButton="true" ShowRefreshButton="false" ShowZoomControl="false">
                </rsweb:ReportViewer>
            </div>
        </div>
    </form>
</body>
</html>
