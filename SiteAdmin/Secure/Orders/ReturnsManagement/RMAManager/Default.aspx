﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SiteAdmin.Secure.Orders.ReturnsManagement.RMAManager.Default" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div>
        <div>
            <div class="LeftFloat" style="width: 70%; text-align: left">
                <h1>RMA Manager</h1>
                <p>
                    Manage pending return requests in your store.
                </p>
            </div>
            <div align="left">
                <h4 class="SubTitle">Search RMA<span style="text-transform:lowercase;">s</span></h4>
                <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
                    <div class="SearchForm">
                        <div class="RowStyle">
                            <div class="ItemStyle">
                                <span class="FieldStyle">Store Name</span><br />
                                <span class="ValueStyle">
                                    <asp:DropDownList ID="ddlPortal" runat="server">
                                    </asp:DropDownList>
                                </span>
                            </div>
                            <div class="RowStyle">
                                <div class="ItemStyle">
                                    <span class="FieldStyle">RMA ID</span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtRMAID" runat="server"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="regRMAID" runat="server" ControlToValidate="txtRMAID"
                                            Display="Dynamic" ErrorMessage="<br />Enter valid RMA ID" SetFocusOnError="true"
                                            ValidationExpression="(\d)+" ValidationGroup="grpSearch" CssClass="Error"></asp:RegularExpressionValidator>
                                    </span>
                                </div>
                                <div class="ItemStyle">
                                    <span class="FieldStyle">Order ID</span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtorderid" runat="server"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtorderid"
                                            Display="Dynamic" ErrorMessage="<br />Enter valid Order ID" SetFocusOnError="true"
                                            ValidationExpression="(\d)+" ValidationGroup="grpSearch" CssClass="Error"></asp:RegularExpressionValidator>
                                    </span>
                                </div>
                                <div class="ItemStyle">
                                    <span class="FieldStyle">First Name</span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtfirstname" runat="server"></asp:TextBox></span>
                                </div>
                                <div class="ItemStyle">
                                    <span class="FieldStyle">Last Name</span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtlastname" runat="server"></asp:TextBox></span>
                                </div>
                            </div>

                            <div class="RowStyle">
                                <div class="ItemStyle">
                                    <span class="FieldStyle">Begin Date</span>
                                    <br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtStartDate" Text='' runat="server" />&nbsp;<asp:ImageButton ID="imgbtnStartDt"
                                            runat="server" ImageAlign="AbsMiddle" ImageUrl="~/SiteAdmin/Themes/images/SmallCalendar.gif" /><br />

                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtStartDate"
                                            CssClass="Error" Display="Dynamic" ErrorMessage="* Enter Valid Date in MM/DD/YYYY format"
                                            ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"
                                            ValidationGroup="grpSearch"></asp:RegularExpressionValidator>
                                        <ajaxToolKit:CalendarExtender ID="CalendarExtender1" Enabled="true" PopupButtonID="imgbtnStartDt"
                                            runat="server" TargetControlID="txtStartDate">
                                        </ajaxToolKit:CalendarExtender>
                                    </span>
                                </div>
                                <div class="ItemStyle">
                                    <span class="FieldStyle">End Date</span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtEndDate" Text='' runat="server" />&nbsp;<asp:ImageButton ID="ImgbtnEndDt"
                                            runat="server" ImageAlign="AbsMiddle" ImageUrl="~/SiteAdmin/Themes/images/SmallCalendar.gif" /><br />

                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEndDate"
                                            CssClass="Error" Display="Dynamic" ErrorMessage="* Enter Valid Date in MM/DD/YYYY format"
                                            ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"
                                            ValidationGroup="grpSearch"></asp:RegularExpressionValidator>
                                        <ajaxToolKit:CalendarExtender ID="CalendarExtender2" Enabled="true" PopupButtonID="ImgbtnEndDt"
                                            runat="server" TargetControlID="txtEndDate">
                                        </ajaxToolKit:CalendarExtender>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtStartDate"
                                            ValidationGroup="grpSearch"
                                            ControlToValidate="txtEndDate" CssClass="Error" Display="Dynamic" ErrorMessage=" End Date must be greater than Begin date"
                                            Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>
                                    </span>
                                </div>

                                <div class="ItemStyle">
                                    <span class="FieldStyle">Status</span><br />
                                    <span class="ValueStyle">
                                        <asp:DropDownList ID="listRequestStatus" runat="server">
                                        </asp:DropDownList>
                                    </span>
                                </div>
                            </div>
                            <div class="ClearBoth">
                                <asp:ImageButton ID="btnClear" onmouseover="this.src='../../../../Themes/Images/buttons/button_clear_highlight.gif';"
                                    onmouseout="this.src='../../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                                    runat="server" CausesValidation="false" OnClick="BtnClearSearch_Click" AlternateText="Clear" />
                                <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                                    onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                                    runat="server" OnClick="BtnSearch_Click" ValidationGroup="grpSearch" AlternateText="Search"/>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <br />
                <h4 class="SubTitle">RMA List
                </h4>

            </div>
            <div>
                <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
            </div>
            <div>
                <asp:GridView ID="uxGrid" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    CaptionAlign="Left" CellPadding="4" CssClass="Grid" EmptyDataText="No RMA Request exist in the database."
                    EnableSortingAndPagingCallbacks="False" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
                    OnRowCommand="UxGrid_RowCommand" PageSize="25" Width="100%">
                    <Columns>
                        <asp:HyperLinkField DataNavigateUrlFields="RMARequestID,OrderID" DataNavigateUrlFormatString='ViewRMARequest.aspx?itemid={0}&orderid={1}'
                            DataTextField="RMARequestID" HeaderText="RMA ID" SortExpression="RMARequestID" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="OrderID" HeaderText="Order ID" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="RequestDate" HeaderText="Date" SortExpression="OrderDate" ItemStyle-Width="120"
                            DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText="Store" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# GetStoreName(DataBinder.Eval(Container.DataItem,"PortalID").ToString()) %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField SortExpression="BillingFirstName" HeaderStyle-HorizontalAlign="Left">
                            <HeaderTemplate>
                                <asp:Label ID="headerTotal" Text="Customer Name" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="CustomerName" Text='<%# ReturnName(Eval("BillingFirstName"),Eval("BillingLastName")) %>'
                                    runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField SortExpression="OrderStateID" HeaderText="Status" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="RequestStatus" Text='<%# Eval("RequestStatus") %>'
                                    runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="ACTIONS" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="220px">
                            <ItemTemplate>

                                <%# GetViewURL(DataBinder.Eval(Container.DataItem,"RMARequestID"),DataBinder.Eval(Container.DataItem,"OrderId")) %>
                                <%# GetEditURL(DataBinder.Eval(Container.DataItem,"RMARequestID"),DataBinder.Eval(Container.DataItem,"OrderId"),DataBinder.Eval(Container.DataItem,"RequestStatus")) %>
                                <%# GetAppendURL(DataBinder.Eval(Container.DataItem,"RMARequestID"),DataBinder.Eval(Container.DataItem,"OrderId"),DataBinder.Eval(Container.DataItem,"RequestStatus")) %>
                                <%# GetDeleteURL(DataBinder.Eval(Container.DataItem,"RMARequestID"),DataBinder.Eval(Container.DataItem,"RequestNumber")) %>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle CssClass="FooterStyle" />
                    <RowStyle CssClass="RowStyle" />
                    <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>
