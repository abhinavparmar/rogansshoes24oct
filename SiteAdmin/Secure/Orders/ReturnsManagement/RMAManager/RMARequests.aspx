﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master"
    AutoEventWireup="true" CodeBehind="RMARequests.aspx.cs" Inherits="SiteAdmin.Secure.Orders.ReturnsManagement.RMAManager.RMARequests" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="uxMainContent">
    <div align="center">
        <div>
            <div class="LeftFloat" style="width: auto; text-align: left">
                <h1>
                    <asp:Label ID="lblRMARequestHeader" runat="server" Text="" /></h1>
            </div>
            <asp:HiddenField ID="hdnreturned" runat="server" Value="false" />
            <div class="ClearBoth">
            </div>
            <div class="LeftFloat" style="width: 50%; text-align: left">
                <div class="ViewForm100">
                    <div class="FieldStyle">
                        Order Id :
                        <asp:Label ID="lblOrderID" runat="server" />
                    </div>
                </div>
            </div>
            <div style="float: right;" class="ViewForm100">
                <div class="FieldStyle">
                    Date :
                    <asp:Label ID="lblRequestDate" runat="server" />
                </div>
            </div>
            <div class="ClearBoth">
            </div>
            <div align="left">
                <h4 class="GridTitle">Order Line Items</h4>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:GridView ID="uxGrid" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
                            runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
                            CssClass="Grid" Width="100%" GridLines="None" EmptyDataText="No orderline items found"
                            AllowPaging="True" OnRowCreated="UxGrid_RowCreated" OnRowDataBound="UxGrid_OnRowDataBound" PageSize="10" OnPageIndexChanging="UxGrid_PageIndexChanging">
                            <Columns>
                                <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkItem" AutoPostBack="True" OnCheckedChanged="chkItem_CheckedChanged" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Is Valid Return" HeaderStyle-HorizontalAlign="Left" Visible="false">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkReturnItem" AutoPostBack="True" OnCheckedChanged="chkItem_CheckedChanged" Checked='<%#  !EnableCheck(Eval("ISReturnable"))  %>' Enabled='<%# EnableCheck(Eval("ISReturnable")) %>' runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="OrderLineItemID" HeaderText="Line Item ID" HeaderStyle-HorizontalAlign="Left"
                                    HeaderStyle-Width="100px" />
                                <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText="Product Name" HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="ProductNum" HeaderText="Product Code" HeaderStyle-HorizontalAlign="Left" Visible="false" />
                                <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%# DataBinder.Eval(Container.DataItem, "Description") %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="SKU" HeaderText="SKU" HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="RMAQuantity" HeaderText="Quantity" HeaderStyle-HorizontalAlign="Left" />
                                <asp:TemplateField HeaderText="Quantity" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="uxQty" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Quantity_SelectedIndexChanged"
                                            CssClass="Quantity">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Reason for Return" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%# GetReasonName(Eval("ReasonForReturnID"))%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Reason for Return" HeaderStyle-HorizontalAlign="Left" ItemStyle-Wrap="false">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="uxReason" runat="server">
                                        </asp:DropDownList>

                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Price" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%#GetPrice(Eval("price"), Eval("DiscountAmount"))%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sub Total" HeaderStyle-HorizontalAlign="Left" meta:resourcekey="TemplateFieldResource6">
                                    <ItemTemplate>
                                      <%#GetTotal(Eval("price"), Eval("DiscountAmount"), Eval("RMAQuantity"))%>
                                    </ItemTemplate>

                                    <ItemStyle HorizontalAlign="Right" />
                                    <HeaderStyle HorizontalAlign="Right" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="MaxQuantity" HeaderText="Max Quantity" HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="RMAMaxQuantity" HeaderText="RMA Max Quantity" HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="SalesTax" HeaderText="Tax" HeaderStyle-HorizontalAlign="Left" />

                            </Columns>
                            <EmptyDataTemplate>
                                No orderline items found
                            </EmptyDataTemplate>
                            <RowStyle CssClass="RowStyle" />
                            <HeaderStyle CssClass="HeaderStyle" />
                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                            <FooterStyle CssClass="FooterStyle" />
                            <PagerStyle CssClass="PagerStyle" />
                        </asp:GridView>

                        <asp:Label ID="lblErrorMsg" CssClass="Error" Text="" runat="server"></asp:Label>
                        <div>
                            <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>
                        <div>
                            <div class="ImageButtons">
                                <asp:Button CssClass="Size100" ID="btnDeselectAll" Text="Deselect All" runat="server"
                                    OnClick="btnDeselectAll_Click"></asp:Button>
                                <asp:Button CssClass="Size100" ID="btnSelectAll" Text="Select All" runat="server"
                                    OnClick="btnSelectAll_Click"></asp:Button>
                            </div>
                        </div>
                        <div class="FieldStyleRMATotal">
                            <div class="ViewForm100">
                                <div class="FieldStyleRMA">Sub Total </div>
                                <div class="ValueStyleRMA">
                                    <asp:Label ID="lblSubTotal" Text="$0.00" runat="server"></asp:Label></div>
                                <div></div>
                                <div class="FieldStyleRMA">Tax </div>
                                <div class="ValueStyleRMA">
                                    <asp:Label ID="lblTax" Text="$0.00" runat="server"></asp:Label></div>
                                <div></div>
                                <div class="FieldStyleRMA">Total </div>
                                <div class="ValueStyleRMA">
                                    <asp:Label ID="lblTotal" Text="$0.00" runat="server"></asp:Label></div>
                                <div></div>
                            </div>
                        </div>
                        <div>
                            <ZNode:Spacer ID="Spacer10" SpacerHeight="50" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="ClearBoth">
            </div>
            <div class="ViewForm100">
                <div class="FieldGiftCardPanel">
                    <asp:Panel ID="pnlGiftcard" runat="server" GroupingText="Gift Card" Visible="false">
                        <asp:Repeater ID="rptGiftCard" runat="server">
                            <ItemTemplate>
                                <div style="text-align: left">
                                    <asp:Label ID="lblGiftcard" CssClass="ValueStyle" runat="Server" Text='<%# FormatGiftCard(Eval("CardNumber"),Eval("Amount"),Eval("ExpirationDate")) %>' />
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </asp:Panel>
                </div>
            </div>
            <div class="FieldStyleRMARight">
                Comments<br />
                <script type="text/javascript">
                    function textboxMultilineMaxNumber(txt, maxLen) {
                        try {
                            if (txt.value.length > (maxLen - 1))
                                return false;
                        } catch (e) {
                        }
                    }
                </script>
                <asp:TextBox ID="txtComments" runat="server" Width="320px" Rows="5" TextMode="MultiLine"></asp:TextBox>
                <asp:HiddenField ID="hdnpaymentid" runat="server" Value="" />
            </div>
            <div class="ClearBoth">
            </div>
            <div>
                <ZNode:Spacer ID="Spacer100" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>

            <div class="ImageButtons" style="float: right;">
                <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="btnCancel_Click" />
                <asp:ImageButton ID="btnSubmit" onmouseover="this.src='../../../../Themes/Images/buttons/button_Submit_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_Submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_Submit.gif"
                    runat="server" AlternateText="Submit" OnClick="btnSubmit_Click" ValidationGroup="General" />
                <asp:Button CssClass="Size100" ID="btnDenyRMA" Text="Deny RMA" Visible="false" runat="server"
                    OnClick="btnDenyRMA_Click"></asp:Button>
                <asp:Button CssClass="Size100" ID="btnAuthorizeRMA" Text="Authorize RMA" Visible="false"
                    runat="server" OnClick="btnAuthorizeRMA_Click"></asp:Button>
                <asp:Button CssClass="Size100" ID="btnVoid" Text="Void" Visible="false" runat="server"
                    OnClick="btnVoid_Click"></asp:Button>
                <asp:Button CssClass="Size100" ID="btnRefund" Text="Refund" Visible="false" runat="server"
                    OnClick="btnRefund_Click"></asp:Button>
                <asp:Button CssClass="Size125" ID="btnIssueGC" Text="Issue Gift Card" Visible="false"
                    runat="server" OnClick="btnIssueGC_Click"></asp:Button>
            </div>
            <div class="ClearBoth">
            </div>

        </div>
    </div>
    <!-- Modal Dialogs -->
    <asp:Button ID="btnGetStartedDialog" runat="server" Style="display: none" />

    <ajaxToolKit:ModalPopupExtender ID="GetAuthorizeDialog" runat="server" TargetControlID="btnGetStartedDialog"
        PopupControlID="PopupAuthorizePanel" BackgroundCssClass="modalBackground" />
    <asp:Panel ID="PopupAuthorizePanel" runat="server" CssClass="PopupAuthorize" Style="display: none;"
        GroupingText="RMA Report">
        <asp:UpdatePanel ID="UpdatePanelAuthorize" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="text-align: center">
                    <asp:CheckBox ID="chkEmail" runat="server" AutoPostBack="true" Text="EMail RMA Report"
                        OnCheckedChanged="chk_CheckedChanged" /><br />
                    <asp:CheckBox ID="chkPrint" runat="server" AutoPostBack="true" Text="Print RMA Report"
                        OnCheckedChanged="chk_CheckedChanged" />
                    <br />
                    <br />
                    <div class="ImageButtons">
                        <asp:Button CssClass="Size75" ID="btnOk" Text="OK" runat="server" Enabled="false"
                            OnClick="btnOk_Click"></asp:Button>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <ajaxToolKit:ModalPopupExtender ID="GetGiftCardDialog" runat="server" TargetControlID="btnGetStartedDialog"
        PopupControlID="pnlGC" BackgroundCssClass="modalBackground" />
    <asp:Panel ID="pnlGC" runat="server" CssClass="PopupConfirmation" Style="display: none;"
        GroupingText="Gift Card">
        <div>
            <br />
        </div>
        <div>
            Gift card issued:<asp:Label ID="lblCardNo" runat="server"></asp:Label>
            for
            <asp:Label ID="lblGiftAmount" runat="server"></asp:Label>.
            <asp:HiddenField ID="hdnExpiry" runat="server" Value="" />
            Would you like to email customer?
        </div>
        <br />
        <div class="ImageButtons">
            <asp:Button CssClass="Size75" ID="btnGCYes" Text="Yes" runat="server" OnClick="btnGCYes_Click"></asp:Button>
            <asp:Button CssClass="Size75" ID="btnGCNo" Text="No" runat="server" OnClick="btnGCNo_Click"></asp:Button>
        </div>
    </asp:Panel>
</asp:Content>
