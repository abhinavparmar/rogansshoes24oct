﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master"
    AutoEventWireup="true" CodeBehind="ViewRMARequest.aspx.cs" Inherits="SiteAdmin.Secure.Orders.ReturnsManagement.RMAManager.ViewRMARequest" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="uxMainContent">
    <script type="text/javascript">
        // Print the DIV tag.
        function PrintReceipt() {
            var headString = "<html><head><title></title></head><body>";
            var footString = "</body>";
            var newString = document.getElementById('<%= divRMARequest.ClientID %>').innerHTML;
            var oldString = document.body.innerHTML;
            document.body.innerHTML = headString + newString + footString;
            window.print();

            // Reset the page layout.            
            document.body.innerHTML = oldString;
            return false;
        }
    </script>
    <div align="center" class="yes-print">
        <div id="divRMARequest" runat="server">
            <div class="LeftFloat">
                <h1>RMA Request : #<asp:Label ID="lblRMARequestHeader" runat="server" Text="" /></h1>
            </div>
            <div class="ClearBoth">
            </div>
            <div class="LeftFloat" style="width: 30%; text-align: left">
                <div class="ViewForm100">
                    <div class="FieldStyle">
                        Order Id :
                        <asp:Label ID="lblOrderID" runat="server" />
                    </div>
                </div>
            </div>
            <div style="float: right" class="ViewForm100">
                <div class="FieldStyle">
                    Date :
                    <asp:Label ID="lblRequestDate" runat="server" />
                </div>
            </div>
            <div class="ClearBoth">
            </div>
            <div align="left">
                <h4 class="GridTitle">Order Line Items</h4>
                <asp:GridView ID="uxGrid" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
                    runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
                    PageSize="10" CssClass="Grid" Width="100%" GridLines="None" EmptyDataText="No orderline items found"
                    OnPageIndexChanging="UxGrid_PageIndexChanging" AllowPaging="True">
                    <Columns>
                        <asp:BoundField DataField="OrderLineItemID" HeaderText="Line Item ID" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="100px" />
                        <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText="Product Name" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="ProductNum" HeaderText="Product Code" Visible="false"
                            HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "Description") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="SKU" HeaderText="SKU" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="RMAMaxQuantity" HeaderText="Quantity" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText="Reason for Return" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# GetReasonName(Eval("ReasonForReturnID"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Price" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%#GetPrice(Eval("price"), Eval("DiscountAmount"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total" HeaderStyle-HorizontalAlign="Left" meta:resourcekey="TemplateFieldResource6">
                            <ItemTemplate>
                                <%#GetTotal(Eval("price"), Eval("DiscountAmount"), Eval("RMAMaxQuantity"))%>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                            <HeaderStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No orderline items found
                    </EmptyDataTemplate>
                    <RowStyle CssClass="RowStyle" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    <FooterStyle CssClass="FooterStyle" />
                    <PagerStyle CssClass="PagerStyle" />
                </asp:GridView>
                <div>
                    <ZNode:Spacer ID="Spacer10" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                </div>
                <div class="FieldStyleRMATotal">
                    <div class="ViewForm100">
                        <div class="FieldStyleRMA">
                            Sub Total
                        </div>
                        <div class="ValueStyleRMA">
                            <asp:Label ID="lblSubTotal" Text="$0.00" runat="server"></asp:Label>
                        </div>
                        <div class="FieldStyleRMA">
                            Tax
                        </div>
                        <div class="ValueStyleRMA">
                            <asp:Label ID="lblTax" Text="$0.00" runat="server"></asp:Label>
                        </div>
                        <div class="FieldStyleRMA">
                            Total
                        </div>
                        <div class="ValueStyleRMA">
                            <asp:Label ID="lblTotal" Text="$0.00" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="ClearBoth">
                </div>
                <div class="ViewForm100">
                    <div class="FieldGiftCardPanel">
                        <asp:Panel ID="pnlGiftcard" runat="server" GroupingText="Gift Card" Visible="false">
                            <asp:Repeater ID="rptGiftCard" runat="server">
                                <ItemTemplate>
                                    <div>
                                        <asp:Label ID="lblGiftcard" CssClass="ValueStyle" runat="Server" Text='<%# FormatGiftCard(Eval("CardNumber"),Eval("Amount"),Eval("ExpirationDate")) %>' />
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </asp:Panel>
                    </div>
                </div>
                <div class="FieldStyleRMARight">
                    Comments<br />
                    <asp:TextBox ID="txtComments" runat="server" Width="320px" Rows="5" TextMode="MultiLine"
                        Enabled="false"></asp:TextBox>
                </div>
                <div class="FieldStyleRMARight">

                    <div class="ImageButtons">
                        <asp:Button CssClass="RMA100" ID="btnOk" Text="Ok" runat="server" OnClick="btnOk_Click" />
                        <asp:Button CssClass="RMA100" ID="btnPrint" Text="Print" runat="server" />
                    </div>
                </div>
            </div>
            <div class="ClearBoth">
            </div>
        </div>
    </div>
</asp:Content>
