using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Orders.CustomerManagement.Customers
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_sales_customers_add class
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region Protected Member Variables 
        private string PageFrom = string.Empty;
        #endregion
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // Call Bind Profile Method
                this.BindProfile();
            }

            if (Request.Params["pagefrom"] != null)
            {
                PageFrom = Request.QueryString["pagefrom"].ToString();
            }
        }
        
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ZNode.Libraries.Admin.AccountAdmin _UserAccountAdmin = new ZNode.Libraries.Admin.AccountAdmin();
            ZNode.Libraries.DataAccess.Entities.Account _UserAccount = new Account();

            if (ListProfileType.SelectedIndex != -1)
            {
                _UserAccount.ProfileID = int.Parse(ListProfileType.SelectedValue);
            }

            // pre-set properties
            _UserAccount.UserID = null;

            _UserAccount.ActiveInd = true;
            _UserAccount.ParentAccountID = null;
            _UserAccount.AccountTypeID = 0;
            _UserAccount.CreateDte = System.DateTime.Now;
            _UserAccount.UpdateDte = System.DateTime.Now;
            _UserAccount.CreateUser = HttpContext.Current.User.Identity.Name;

            // Add New Contact
            bool Check = _UserAccountAdmin.Add(_UserAccount);

            // Check Boolean Value
            if (Check)
            {
                string AssociateName = "Creation of Account  " + _UserAccount.UserID;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, " Account ");

                if (PageFrom == "ADMIN")
                {
                    Response.Redirect("~/SiteAdmin/Secure/sales/customers/list.aspx");
                }
                else
                {
                    Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/Default.aspx");
                }
            }
            else
            {
                ErrorMessage.Text = "Could not create new contact. Please contact customer support.";
                return;
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (PageFrom == "ADMIN")
            {
                Response.Redirect("~/SiteAdmin/Secure/sales/customers/list.aspx");
            }
            else
            {
                Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/Default.aspx");
            }
        }

        /// <summary>
        /// Binds Account Type drop-down list
        /// </summary>
        private void BindProfile()
        {
            ZNode.Libraries.Admin.ProfileAdmin _Profileadmin = new ProfileAdmin();
            ListProfileType.DataSource = _Profileadmin.GetAll();
            ListProfileType.DataTextField = "name";
            ListProfileType.DataValueField = "profileID";
            ListProfileType.DataBind();
        }
    }
}