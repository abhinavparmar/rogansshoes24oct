<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="SiteAdmin.Secure.Orders.CustomerManagement.Customers.AddPermission"
    Title="Untitled Page" Codebehind="AddPermission.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/Accounts/Permission.ascx" TagName="Permission" TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/StoreName.ascx" TagName="StoreName" TagPrefix="ZNode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <ZNode:Permission ID="uxPermission" runat="server" RoleName = "" />
</asp:Content>
