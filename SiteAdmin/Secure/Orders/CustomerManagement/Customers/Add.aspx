<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Orders.CustomerManagement.Customers.Add" ValidateRequest="false" CodeBehind="Add.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>Quick Add Account</h1>
            <ZNode:DemoMode ID="DemoMode1" runat="server" />
        </div>
        <div style="text-align: right">
            <asp:ImageButton ID="btnSubmitTop" runat="server" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                OnClick="BtnSubmit_Click" onmouseout="this.src='~/SiteAdmin/Themes/Images/buttons/button_submit.gif';"
                onmouseover="this.src='~/SiteAdmin/Themes/Images/buttons/button_submit_highlight.gif';" />
            <asp:ImageButton ID="btnCancelTop" runat="server" CausesValidation="false" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                OnClick="BtnCancel_Click" onmouseout="this.src='~/SiteAdmin/Themes/Images/buttons/button_cancel.gif';"
                onmouseover="this.src='~/SiteAdmin/Themes/Images/buttons/button_cancel_highlight.gif';" />
        </div>
        <div class="ClearBoth">
            <ZNode:Spacer ID="Spacer8" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div class="FieldStyle">
            Select a Profile for this Account
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ListProfileType" runat="server" Width="100px" />
        </div>
        <div class="FieldStyle">
            First Name<span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtBillingFirstName" runat="server" Width="130" Columns="30" MaxLength="100" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="* Enter BillingFirstName"
                ControlToValidate="txtBillingFirstName" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            Last Name<span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtBillingLastName" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="* Enter Billing LastName"
                ControlToValidate="txtBillingLastName" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="Error" DisplayMode="List"
                ShowMessageBox="True" ShowSummary="False" />
        </div>
        <div class="FieldStyle">
            Phone Number<span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtBillingPhoneNumber" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="* Enter Billing Phone Number"
                ControlToValidate="txtBillingPhoneNumber" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            Email Address<span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtBillingEmail" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator ID="regemailID" runat="server" ControlToValidate="txtBillingEmail"
                ErrorMessage="*Please use a valid email address." Display="Dynamic" ValidationExpression="[\w\.-]+(\+[\w-]*)?@([\w-]+\.)+[\w-]+"
                ValidationGroup="1" CssClass="Error"></asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="* Enter Email ID"
                ControlToValidate="txtBillingEmail" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
        </div>
        <div>
            <asp:Label ID="ErrorMessage" runat="server"></asp:Label>
            <ZNode:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="3" runat="server" />
        </div>
        <div class="ClearBoth">
            <asp:ImageButton ID="btnSubmit" runat="server" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                OnClick="BtnSubmit_Click" onmouseout="this.src='~/SiteAdmin/Themes/Images/buttons/button_submit.gif';"
                onmouseover="this.src='~/SiteAdmin/Themes/Images/buttons/button_submit_highlight.gif';" AlternateText="Submit"/>
            <asp:ImageButton ID="btnCancel" runat="server" CausesValidation="false" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                OnClick="BtnCancel_Click" onmouseout="this.src='~/SiteAdmin/Themes/Images/buttons/button_cancel.gif';"
                onmouseover="this.src='~/SiteAdmin/Themes/Images/buttons/button_cancel_highlight.gif';" AlternateText="Cancel" />
        </div>
</asp:Content>
