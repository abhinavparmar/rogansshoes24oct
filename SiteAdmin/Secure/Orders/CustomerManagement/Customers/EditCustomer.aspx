﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" CodeBehind="EditCustomer.aspx.cs" Inherits="SiteAdmin.Secure.Orders.CustomerManagement.Customers.EditCustomer" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/Accounts/EditAccount.ascx" TagName="Account" TagPrefix="ZNode" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
<ZNode:Account ID="uxCustomer" runat="server" PageFrom="CUSTOMER" />
</asp:Content>
