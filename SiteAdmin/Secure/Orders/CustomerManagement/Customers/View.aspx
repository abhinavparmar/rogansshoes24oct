﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" CodeBehind="View.aspx.cs" Inherits="SiteAdmin.Secure.Orders.CustomerManagement.Customers.View" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/Accounts/View.ascx" TagName="CustomerList" TagPrefix="ZNode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
<ZNode:CustomerList ID="uxCustomer" runat="server" RoleName = "" />

</asp:Content>
