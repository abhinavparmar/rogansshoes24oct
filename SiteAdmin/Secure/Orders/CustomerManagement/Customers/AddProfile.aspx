<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" ValidateRequest="false" Inherits="SiteAdmin.Secure.Orders.CustomerManagement.Customers.AddProfile" Codebehind="AddProfile.aspx.cs" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/Accounts/Profile.ascx" TagName="Profile" TagPrefix="ZNode" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <ZNode:Profile ID="uxProfile" runat="server" RoleName = "" />
</asp:Content>

