﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" CodeBehind="Default.aspx.cs" Inherits="SiteAdmin.Secure.Orders.CustomerManagement.Customers.Default" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/Accounts/Customer.ascx" TagName="CustomerList" TagPrefix="ZNode" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <div align="center">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>Customers</h1>
        </div>
        <div class="ClearBoth">
        </div>
        <div class="LeftFloat">
            <p>
                Search customer, partner, and vendor accounts, view order history, and access service notes.
            </p>
        </div>

        <div style="float: right;">
            <zn:LinkButton ID="AddContact" runat="server" CausesValidation="false" Text="Add Customer"
                CommandArgument="CUSTOMER" OnCommand="AddContact_Command"
                ButtonType="Button" ButtonPriority="Primary" />
        </div>
    </div>
    <ZNode:CustomerList ID="uxCustomer" runat="server" RoleName="" />
</asp:Content>
