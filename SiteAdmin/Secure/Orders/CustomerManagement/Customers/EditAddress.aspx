﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" CodeBehind="EditAddress.aspx.cs" Inherits="SiteAdmin.Secure.Orders.CustomerManagement.Customers.EditAddress" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/Accounts/Address.ascx" TagName="Address" TagPrefix="ZNode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
   <ZNode:Address ID="uxAddress" runat="server" />
</asp:Content>