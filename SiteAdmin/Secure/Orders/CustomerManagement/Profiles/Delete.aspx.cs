using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Orders.CustomerManagement.Profiles
{
    /// <summary>
    /// Represents the SiteAdmin -  Admin_Secure_settings_Profile_Delete class
    /// </summary>
    public partial class Delete : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId;
        private string _ProfileName = string.Empty;
        private string CancelLink = "~/SiteAdmin/Secure/Orders/CustomerManagement/Profiles/Default.aspx";
        #endregion

        /// <summary>
        /// Gets or sets the Profile Name
        /// </summary>
        public string ProfileName
        {
            get
            {
                return this._ProfileName;
            }

            set
            {
                this._ProfileName = value;
            }
        }

        #region Bind Data
        /// <summary>
        /// Bind the data for a Product id
        /// </summary>
        public void BindData()
        {
            ProfileAdmin profileAdmin = new ProfileAdmin();
            ZNode.Libraries.DataAccess.Entities.Profile profileEntity = profileAdmin.GetByProfileID(this.ItemId);

            if (profileEntity != null)
            {
                this.ProfileName = profileEntity.Name;
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring   
            if (Request.Params["Itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["Itemid"].ToString());
                this.BindData();
            }
            else
            {
                this.ItemId = 0;
            }
        }
        #endregion
       
        #region Events
        /// <summary>
        /// Delete Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            ProfileAdmin profileAdmin = new ProfileAdmin();
            ZNode.Libraries.DataAccess.Entities.Profile profileEntity = profileAdmin.GetByProfileID(this.ItemId);

            bool status = false;
            string message = "The profile can not be deleted until all associated items are removed. Please ensure that this profile does not associated with accounts, stores or promotions. If it does, then delete these Items first.";

            // Check the profile IsDefualt or not
            ZNode.Libraries.DataAccess.Service.PortalProfileService portalProfileService = new ZNode.Libraries.DataAccess.Service.PortalProfileService();
            ZNode.Libraries.DataAccess.Service.PortalService portalService = new ZNode.Libraries.DataAccess.Service.PortalService();
            TList<PortalProfile> portalProfileList = portalProfileService.GetByProfileID(this.ItemId);

            if (portalProfileList.Count == 0)
            {
                string AssociateName = "Delete Profile - " + profileEntity.Name;
                string ProfileName = profileEntity.Name;
                status = profileAdmin.Delete(profileEntity);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, ProfileName);
            }

            if (status)
            {
                Response.Redirect(this.CancelLink);
            }
            else
            {
                lblErrorMessage.Text = message;
            }
        }

        /// <summary>
        /// Cancel Button click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.CancelLink);
        }
        #endregion
    }
}