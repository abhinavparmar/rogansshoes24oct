<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Orders.CustomerManagement.Profiles.Delete" Codebehind="Delete.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <h5>
        Please Confirm</h5>
    <uc1:DemoMode ID="DemoMode1" runat="server"></uc1:DemoMode>
    <p>
        Please confirm if you want to delete the Profile <b>
            <%=ProfileName%></b>. This change cannot be undone.</p>
    <div>
        <asp:Label ID="lblErrorMessage" CssClass="Error" runat="server" Text=''></asp:Label></div>
    <div>
        <uc1:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <div>
        <asp:ImageButton ID="btnDelete" onmouseover="this.src='../../../../Themes/Images/buttons/button_delete_highlight.gif';"
            onmouseout="this.src='../../../../Themes/Images/buttons/button_delete.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_delete.gif"
            runat="server" AlternateText="Delete" OnClick="BtnDelete_Click" CausesValidation="true" />
        <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
            onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
            runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" /></div>
    <div>
        <uc1:Spacer ID="Spacer1" SpacerHeight="25" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
</asp:Content>
