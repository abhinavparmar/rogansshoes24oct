<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" ValidateRequest="false" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Orders.CustomerManagement.Profiles.Add" CodeBehind="Add.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/StoreName.ascx" TagName="StoreName" TagPrefix="ZNode" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/spacer.ascx" TagName="Spacer" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
                <uc2:DemoMode ID="DemoMode1" runat="server" />
            </h1>
        </div>
        <div style="text-align: right">
            <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
        <div class="ClearBoth">
            <asp:Label ID="lblErrorMsg" CssClass="Error" runat="server"></asp:Label>
        </div>
        <div class="FieldStyle">
            Profile Name<span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="ProfileName" runat="server" Columns="25"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ProfileName"
                Display="Dynamic" CssClass="Error" ErrorMessage="* Enter Profile name" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            Profile Code<br />
            <small>Internal profile code used by your ERP system.</small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="ExternalAccountNum" runat="server" Columns="25"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            Weighting<span class="Asterix">*</span><br />
            <small>Profiles with higher weighting will get precedence.</small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtWeighting" runat="server" MaxLength="9" Columns="5"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" CssClass="Error" runat="server" Display="Dynamic"
                ErrorMessage="* Enter Weighting" ControlToValidate="txtWeighting"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator2" CssClass="Error" runat="server" ControlToValidate="txtWeighting"
                Display="Dynamic" ErrorMessage="* Enter a whole number." MaximumValue="999999999"
                MinimumValue="1" Type="double"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">Settings</div>
        <div class="ValueStyle">
            <asp:CheckBox ID="chkShowPrice" runat="server" Text="Display product price for this profile" />
        </div>
        <div class="FieldStyle"></div>
        <div class="ValueStyle">
            <asp:CheckBox ID="chkUseWholesalePrice" runat="server" Text="Enable wholesale price for this profile" />
        </div>
        <div class="FieldStyle"></div>
        <div class="ValueStyle">
            <asp:CheckBox ID="chkShowOnPartner" runat="server" Text="Enable affiliate sign-up option for this profile" />
        </div>
        <div class="FieldStyle"></div>
        <div class="ValueStyle">
            <asp:CheckBox ID="chkTaxExempt" runat="server" Text="This profile has Tax Exempt status" />
        </div>
        <div>
            <uc1:Spacer ID="Spacer1" SpacerHeight="20" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="ClearBoth">
            <br />
        </div>
        <div>
            <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>
