<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Orders.CustomerManagement.ServiceRequests.View" ValidateRequest="false" Codebehind="View.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div  class="FormView">
        <div style="float: right" class="ImageButtons">
            <asp:Button ID="CaseList" runat="server" OnClick="CaseList_Click" Text="< Back"
                Width="100px" />
            <asp:Button ID="EditCase" runat="server" OnClick="CaseEdit_Click" Text="Edit Case"
                Width="100px" />
            <asp:Button ID="ReplyToCase" runat="server" OnClick="ReplyToCase_Click" Text="Reply to Customer"
                Width="150px" />
        </div>
        <div class="LeftFloat">
            <h1>
                Service Request:
                <asp:Label ID="lblTitle" runat="server" Text="Label" /></h1>
        </div>
        <div class="ClearBoth">
        </div>
        <h4 class="SubTitle">
            General Information</h4>
        <div class="ServiceRequest">
            <div class="FieldStyleA">Title</div>
            <div class="ValueStyleA">
                <asp:Label ID="lblCaseTitle" runat="server" Text="Label"></asp:Label></div>
            <div class="FieldStyle">Status</div>
            <div class="ValueStyle">
                <asp:Label ID="lblCaseStatus" runat="server" Text="Label"></asp:Label></div>
            <div class="FieldStyleA">Priority</div>
            <div class="ValueStyleA">
                <asp:Label ID="lblCasePriority" runat="server" Text="Label"></asp:Label></div>
            <div class="FieldStyle">Message</div>
            <div class="ValueStyle">
                <asp:Label runat="server" ID="lblCaseDescription" Width="519px"></asp:Label></div>
            <div class="FieldStyleA">Created Date</div>
            <div class="ValueStyleA">
                <asp:Label ID="lblCreatedDate" runat="server"></asp:Label></div>
        </div>
        <h4 class="SubTitle">
            Customer Information
        </h4>
       <div class="ServiceRequest">
            <div class="FieldStyleA">First Name</div>
            <div class="ValueStyleA"><asp:Label ID="lblFirstName" runat="server" Text="Label"></asp:Label></div>

            <div class="FieldStyle">Last Name</div>
            <div class="ValueStyle"><asp:Label ID="lblLastName" runat="server" Text="Label"></asp:Label></div>
            
            <div class="FieldStyleA">Company Name</div>
            <div class="ValueStyleA"><asp:Label ID="lblCompanyName" runat="server" Text="Label"></asp:Label>&nbsp;</div>
           
            <div class="FieldStyle"> Email ID</div>
            <div class="ValueStyle"><asp:Label ID="lblEmailID" runat="server" Text="Label"></asp:Label></div>
            
            <div class="FieldStyleA">Phone Number</div>
            <div class="ValueStyleA"><asp:Label ID="lblPhoneNumber" runat="server" Text="Label"></asp:Label>&nbsp;</div>
        </div>
        <h4 class="SubTitle">
            Notes
        </h4>
        <div class="ImageButtons" style="text-align: right">
            <asp:Button ID="AddNewNote" runat="server" Text="Add Note" Width="80px" OnClick="AddNewNote_Click" />
        </div>
        <div class="ServiceRequest">
        <asp:Repeater ID="CustomerNotes" runat="server">
            <ItemTemplate>
            <div class="NotesA"><asp:Label ID="Label1"  runat="Server" Text='<%# FormatCustomerNote(Eval("NoteTitle"),Eval("CreateUser"),Eval("CreateDte")) %>' />
               </div><br />
            <div class="NotesB"><asp:Label ID="Label2"  runat="Server" Text='<%# Eval("NoteBody") %>' /></div>
                <div>
                <uc1:Spacer ID="Spacer1" SpacerHeight="20" SpacerWidth="3" runat="server"></uc1:Spacer>
                    </div>
                    <br />      
            </ItemTemplate>
        </asp:Repeater>
       </div>
        <uc1:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
</asp:Content>
