using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Orders.CustomerManagement.ServiceRequests
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_sales_cases_Default class
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region Protected Member Variables
        private string CancelLink = "~/SiteAdmin/Secure/Orders/CustomerManagement/ServiceRequests/Default.aspx";
        private int ItemId = 0;
        #endregion

        #region General Events

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (!Page.IsPostBack)
            {
                // Bind data to the fields on the page
                this.BindData();

                if (this.ItemId > 0)
                {
                    lblTitle.Text = "Edit Service Request";
                    txtCaseDescription.ReadOnly = true;
                    txtCaseTitle.ReadOnly = true;
                    this.BindEditData();
                }
                else
                {
                    lblTitle.Text = "Create a New Service Request";
                    lblCaseOrigin.Text = "Admin";
                    lblCaseDate.Text = System.DateTime.Now.ToShortDateString();
                }
            }
        }

        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            CaseAdmin _CaseAdmin = new CaseAdmin();
            CaseRequest _CaseList = new CaseRequest();

            // If edit mode then retrieve data first
            if (this.ItemId > 0)
            {
                _CaseList = _CaseAdmin.GetByCaseID(this.ItemId);
            }

            // Set Null Values
            _CaseList.OwnerAccountID = null;
            _CaseList.CaseOrigin = lblCaseOrigin.Text;
            _CaseList.CreateUser = System.Web.HttpContext.Current.User.Identity.Name;
            _CaseList.CreateDte = System.DateTime.Now;
            _CaseList.PortalID = uxStoreName.SelectedValue;

            // Set Values 
            _CaseList.Title = Server.HtmlEncode(txtCaseTitle.Text.Trim());
            _CaseList.Description = Server.HtmlEncode(txtCaseDescription.Text.Trim());
            _CaseList.EmailID = txtEmailID.Text.Trim();
            _CaseList.FirstName = Server.HtmlEncode(txtFirstName.Text.Trim());
            _CaseList.LastName = Server.HtmlEncode(txtLastName.Text.Trim());
            _CaseList.PhoneNumber = Server.HtmlEncode(txtPhoneNo.Text.Trim());
            _CaseList.CompanyName = Server.HtmlEncode(txtCompanyName.Text.Trim());

            _CaseList.AccountID = null;

            if (lstCasePriority.SelectedIndex != -1)
            {
                _CaseList.CasePriorityID = int.Parse(lstCasePriority.SelectedValue);
            }
            
            if (lstCaseStatus.SelectedValue != null)
            {
                _CaseList.CaseStatusID = int.Parse(lstCaseStatus.SelectedValue);
            }

            bool retval = false;

            if (this.ItemId > 0)
            {
                retval = _CaseAdmin.Update(_CaseList);
                string AssociateName = "Edit of Service Request - " + txtCaseTitle.Text.Trim();

                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, txtCaseTitle.Text.Trim());
            }
            else
            {
                retval = _CaseAdmin.Add(_CaseList);
                string AssociateName = "Creation of Service Request - " + txtCaseTitle.Text.Trim();
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, txtCaseTitle.Text.Trim());
            }

            if (HiddenOldStatus.Value != lstCaseStatus.SelectedItem.Text)
            {
                NoteAdmin _Noteadmin = new NoteAdmin();
                Note _NoteAccess = new Note();

                _NoteAccess.CaseID = _CaseList.CaseID;
                _NoteAccess.AccountID = null;
                _NoteAccess.CreateDte = System.DateTime.Now;
                _NoteAccess.CreateUser = HttpContext.Current.User.Identity.Name;
                _NoteAccess.NoteTitle = "Status Changed ";
                _NoteAccess.NoteBody = "Status Changed from " + HiddenOldStatus.Value.ToUpper() + " To " + lstCaseStatus.SelectedItem.Text.ToUpper() +
                                        " by " + ZNodeUserAccount.CurrentAccount().BillingAddress.FirstName + " " + ZNodeUserAccount.CurrentAccount().BillingAddress.LastName;

                bool Check = _Noteadmin.Insert(_NoteAccess);
            }

            if (retval)
            {
                // Redirect to list page
                Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/ServiceRequests/Default.aspx");
            }
            else
            {
                // Display error message
                lblMsg.Text = "An error occurred while updating. Please try again.";
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.CancelLink);
        }

        #endregion

        #region Bind Event

        /// <summary>
        /// Bind Data Method
        /// </summary>
        private void BindData()
        {
            CaseAdmin _AdminAccess = new CaseAdmin();

            // Bind Case status DropdownList
            lstCaseStatus.DataSource = _AdminAccess.GetAllCaseStatus();
            lstCaseStatus.DataTextField = "CaseStatusNme";
            lstCaseStatus.DataValueField = "CaseStatusID";
            lstCaseStatus.DataBind();

            // Bind case priority DropdownList
            lstCasePriority.DataSource = _AdminAccess.GetAllCasePriority();
            lstCasePriority.DataTextField = "CasePriorityNme";
            lstCasePriority.DataValueField = "CasePriorityID";
            lstCasePriority.DataBind();
        }

        /// <summary>
        /// Bind Data Method
        /// </summary>
        private void BindEditData()
        {
            CaseAdmin _CaseAdmin = new CaseAdmin();
            CaseRequest _CaseList = _CaseAdmin.GetByCaseID(this.ItemId);

            if (_CaseList != null)
            {
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (profiles.StoreAccess != "AllStores")
                {
                    string[] stores = profiles.StoreAccess.Split(',');
                    Array Stores = (Array)stores;
                    int found = Array.IndexOf(Stores, _CaseList.PortalID.ToString());
                    if (found == -1)
                    {
                        Response.Redirect("list.aspx", true);
                    }
                }

                if (lstCasePriority.Items.Count > 0)
                {
                    lstCaseStatus.SelectedValue = _CaseList.CaseStatusID.ToString();
                    HiddenOldStatus.Value = lstCaseStatus.SelectedItem.Text;
                }

                if (lstCaseStatus.Items.Count > 0)
                {
                    lstCasePriority.SelectedValue = _CaseList.CasePriorityID.ToString();
                }

                // Set Values 
                uxStoreName.PreSelectValue = _CaseList.PortalID.ToString();
                lblCaseOrigin.Text = _CaseList.CaseOrigin;
                txtCaseTitle.Text = Server.HtmlDecode(_CaseList.Title);
                txtCaseDescription.Text = Server.HtmlDecode(_CaseList.Description).Replace("<br>", Environment.NewLine);
                txtFirstName.Text = Server.HtmlDecode(_CaseList.FirstName);
                txtLastName.Text = Server.HtmlDecode(_CaseList.LastName);
                txtCompanyName.Text = Server.HtmlDecode(_CaseList.CompanyName);
                txtEmailID.Text = _CaseList.EmailID;
                txtPhoneNo.Text = Server.HtmlDecode(_CaseList.PhoneNumber);
                lblCaseDate.Text = _CaseList.CreateDte.ToShortDateString();
            }
        }
        #endregion
    }
}