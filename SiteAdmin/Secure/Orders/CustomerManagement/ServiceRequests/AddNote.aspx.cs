using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Orders.CustomerManagement.ServiceRequests
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_sales_Note_Add class
    /// </summary>
    public partial class AddNote : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId;
        private string CancelLink = "~/SiteAdmin/Secure/Orders/CustomerManagement/ServiceRequests/view.aspx";
        #endregion

        #region General Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
                CaseAdmin _CaseAdmin = new CaseAdmin();
                CaseRequest _CaseList = _CaseAdmin.GetByCaseID(this.ItemId);

                if (_CaseList != null)
                {
                    lblHeading.Text = "Add Note to Service Request: " + _CaseList.Title;
                    ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                    if (profiles.StoreAccess != "AllStores")
                    {
                        string[] stores = profiles.StoreAccess.Split(',');
                        Array Stores = (Array)stores;
                        int found = Array.IndexOf(Stores, _CaseList.PortalID.ToString());
                        if (found == -1)
                        {
                            Response.Redirect("Default.aspx", true);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            NoteAdmin _Noteadmin = new NoteAdmin();
            Note _NoteAccess = new Note();

            _NoteAccess.CaseID = this.ItemId;
            _NoteAccess.AccountID = null;
            _NoteAccess.CreateDte = System.DateTime.Now;
            _NoteAccess.CreateUser = HttpContext.Current.User.Identity.Name;
            _NoteAccess.NoteTitle = Server.HtmlEncode(txtNoteTitle.Text.Trim());
            _NoteAccess.NoteBody = ctrlHtmlText.Html;

            bool Check = _Noteadmin.Insert(_NoteAccess);

            if (Check)
            {
                CaseAdmin _CaseAdmin = new CaseAdmin();
                CaseRequest _CaseList = _CaseAdmin.GetByCaseID(this.ItemId);

                string AssociateName = "Added Note " + txtNoteTitle.Text.Trim() + " to Service Request " + _CaseList.Title;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, _CaseList.Title);

                // Redirect to main page
                Response.Redirect(this.CancelLink + "?itemid=" + this.ItemId);
            }
            else
            {
                // Display error message
                lblError.Text = "An error occurred while updating. Please try again.";
                lblError.Visible = true;
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.CancelLink + "?itemid=" + this.ItemId);
        }
        #endregion
    }
}