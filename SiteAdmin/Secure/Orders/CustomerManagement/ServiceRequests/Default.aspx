<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Orders.CustomerManagement.ServiceRequests.Default" ValidateRequest="false" CodeBehind="Default.aspx.cs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>Service Requests</h1>
            <p style="width: 700px;">
                Respond to service requests submitted by your customers using the Contact-Us form on your website.
            </p>
        </div>
        <div class="ButtonStyle">
            <zn:LinkButton ID="btnAdd" runat="server" CausesValidation="False"
                ButtonType="Button" OnClick="BtnAdd_Click" Text="Create Service Request"
                ButtonPriority="Primary" />
        </div>
        <div class="ClearBoth">
        </div>
        <div align="left">
            <h4 class="SubTitle">Search Service Requests
            </h4>
            <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
                <div class="SearchForm">
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle">Store Name</span><br />
                            <span class="ValueStyle">
                                <asp:DropDownList ID="ddlPortal" runat="server">
                                </asp:DropDownList>
                            </span>
                        </div>
                    </div>
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle">Case ID</span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtcaseid" runat="server"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">Title</span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txttitle" runat="server"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">Case Status</span><br />
                            <span class="ValueStyle">
                                <asp:DropDownList runat="server" ID="ListCaseStatus" />
                            </span>
                        </div>
                    </div>
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle">First Name</span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtfirstname" runat="server"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">Last Name</span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtlastname" runat="server"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">Company Name</span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtcompanyname" runat="server"></asp:TextBox></span>
                        </div>
                    </div>
                    <div class="ClearBoth">
                        <asp:ImageButton ID="btnClear" onmouseover="this.src='../../../../Themes/Images/buttons/button_clear_highlight.gif';"
                            onmouseout="this.src='../../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                            runat="server" CausesValidation="false" OnClick="BtnClearSearch_Click" AlternateText="Clear"/>
                        <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                            onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                            runat="server" OnClick="BtnSearch_Click" AlternateText="Search"/>
                    </div>
                </div>
            </asp:Panel>
            <br />
            <h4 class="GridTitle">Service Request List
            </h4>
            <div>
                <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
                    CellPadding="4" DataKeyNames="accountid" EmptyDataText="No Service Requests were found."
                    OnRowCommand="UxGrid_RowCommand" OnPageIndexChanging="UxGrid_PageIndexChanging"
                    PageSize="50" AllowPaging="True" GridLines="None" AutoGenerateColumns="False"
                    CaptionAlign="Left" OnSorting="UxGrid_Sorting" OnRowDataBound="uxGrid_RowDataBound">
                    <Columns>
                        <asp:HyperLinkField HeaderText="ID" DataTextField="CaseID" DataNavigateUrlFormatString="~/SiteAdmin/Secure/Orders/CustomerManagement/ServiceRequests/View.aspx?itemid={0}"
                            DataNavigateUrlFields="caseid" HeaderStyle-HorizontalAlign="Left" />
                        <asp:HyperLinkField HeaderText="Title" DataTextField="Title" DataNavigateUrlFormatString="~/SiteAdmin/Secure/Orders/CustomerManagement/ServiceRequests/View.aspx?itemid={0}"
                            DataNavigateUrlFields="CaseID" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="StoreName" HeaderText="Store Name" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText="Case Status" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblCaseStatus" Text='<%# Eval("CaseStatusNme") %>'
                                    runat="server" Font-Bold="true" Font-Size="Smaller"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="CasePriorityNme" HeaderText="Case Priority" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText="Created Date" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# (DataBinder.Eval(Container.DataItem, "CreateDte", "{0:d}"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Actions" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <div class="LeftFloat" style="width: 17%; text-align: left">
                                    <asp:LinkButton ID="butView" CssClass="LinkButton" runat="server" CommandArgument='<%# Eval("caseid") %>'
                                        CommandName="View" Text="Manage&raquo" />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle CssClass="FooterStyle" />
                    <RowStyle CssClass="RowStyle" />
                    <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                </asp:GridView>
            </div>
            <asp:Label ID="lblError" runat="server" Text="" CssClass="Error"></asp:Label>
        </div>
    </div>
</asp:Content>
