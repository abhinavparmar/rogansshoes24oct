<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Orders.CustomerManagement.ServiceRequests.EmailCase"
    ValidateRequest="false" CodeBehind="EmailCase.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">

    <div>
        <asp:Label ID="lblErrorMessage" runat="server" CssClass="Error"></asp:Label><br />
        <br />
    </div>

    <div>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>Reply to Customer<uc2:DemoMode ID="DemoMode1" runat="server" />
            </h1>
        </div>
        <div style="text-align: right">
            <asp:ImageButton ID="btnSubmitTop" runat="server" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                OnClick="BtnSubmit_Click" onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';"
                onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';" AlternateText="Submit"/>
            <asp:ImageButton ID="btnCancelTop" runat="server" CausesValidation="false" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                OnClick="BtnCancel_Click" onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';"
                onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';" AlternateText="Cancel"/>
        </div>
        <uc1:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>

        <h4 class="SubTitle">Customer Information
        </h4>
        <div class="FormView">
            <div class="RowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle">Full Name </span><span class="ValueStyle">
                        <asp:Label ID="lblCustomerName" runat="server" Text="Label"></asp:Label>
                    </span>
                </div>
            </div>
            <div class="AlternatingRowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle">Company Name </span><span class="ValueStyle">
                        <asp:Label ID="lblCompanyName" runat="server" Text="Label"></asp:Label>
                    </span>
                </div>
            </div>
            <div class="RowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle">Email ID </span><span class="ValueStyle">
                        <asp:Label ID="lblEmailTo" runat="server" Text="Label"></asp:Label>
                    </span>
                </div>
            </div>
            <div class="AlternatingRowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle">Phone Number </span><span class="ValueStyle">
                        <asp:Label ID="lblPhoneNumber" runat="server" Text="Label"></asp:Label>
                    </span>
                </div>
            </div>
        </div>
        <h4 class="SubTitle">Service Request
        </h4>

        <div class="FormView">
            <div class="RowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle">Title</span> <span class="ValueStyle">
                        <asp:Label ID="lblCaseTitle" runat="server" Text="Label"></asp:Label>
                    </span>
                </div>
            </div>
            <div class="RowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle">Status</span> <span class="ValueStyle">
                        <asp:Label ID="lblCaseStatus" runat="server" Text="Label"></asp:Label>
                    </span>
                </div>
            </div>
            <div class="RowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle">Priority </span><span class="ValueStyle">
                        <asp:Label ID="lblCasePriority" runat="server" Text="Label"></asp:Label>
                    </span>
                </div>
            </div>

            <div class="RowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle">Message</span><span class="ValueStyle">
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtCaseDescription" Height="59px"
                            TextMode="MultiLine" Width="600px" />
                    </span>
                </div>
            </div>
        </div>

        <h4 class="SubTitle">Compose Email
        </h4>
        <div class="FormView">
            <div class="RowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle">Email Subject </span><span class="ValueStyle">
                        <asp:TextBox ID="txtEmailSubj" runat="server" Width="279px" />
                    </span>
                </div>
            </div>
            <div class="RowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle">Email Message </span><span class="ValueStyle">
                        <uc1:HtmlTextBox ID="ctrlHtmlText" Mode="1" runat="server"></uc1:HtmlTextBox>
                    </span>
                </div>
            </div>
            <div class="RowStyle">
                <div class="ItemStyle">
                    <span valign="top" class="FieldStyle">Attachments </span><span class="ValueStyle">
                        <input id="FileBrowse" type="file" size="47" name="File1" runat="server" tabindex="8">
                    </span>
                </div>
            </div>
        </div>
        <asp:Label ID="lblEmailid" runat="server" Visible="false" />
        <div class="ClearBoth">
            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                OnClick="BtnSubmit_Click" onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';"
                onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';" AlternateText="Submit"/>
            <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="false" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                OnClick="BtnCancel_Click" onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';"
                onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';" AlternateText="Cancel"/>
        </div>
        <asp:HiddenField ID="txtAccountId" runat="server" />
    </div>
</asp:Content>
