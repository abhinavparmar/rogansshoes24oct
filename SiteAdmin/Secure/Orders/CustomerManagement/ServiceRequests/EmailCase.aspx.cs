using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Orders.CustomerManagement.ServiceRequests
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_sales_cases_case_email class
    /// </summary>
    public partial class EmailCase : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId = 0;
        private string RedirectLink = "~/SiteAdmin/Secure/Orders/CustomerManagement/ServiceRequests/Default.aspx";
        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (!Page.IsPostBack)
            {
                this.BindData();
                if (this.ItemId > 0)
                {
                    this.BindValues();
                }
            }
        }

        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            string strFileName = string.Empty;
            int portalID = 0;
            if (Session["PortalID"] != null)
            {
                portalID = int.Parse(Session["PortalID"].ToString());
            }

            MailMessage EmailContent = new MailMessage(ZNodeConfigManager.SiteConfig.AdminEmail, lblEmailid.Text);
            EmailContent.Subject = txtEmailSubj.Text;
            //EmailContent.Body = ctrlHtmlText.Html.ToString();//Old Code
            EmailContent.Body = GetEmailContent();
            EmailContent.IsBodyHtml = true;

            // Attachment Steps
            // Get the file name 
            strFileName = Path.GetFileName(FileBrowse.PostedFile.FileName);

            if (!strFileName.Equals(string.Empty))
            {
                // Email Attachment
                Attachment attach = new Attachment(FileBrowse.PostedFile.InputStream, strFileName);

                // Attach the created email attachment 
                EmailContent.Attachments.Add(attach);
            }

            ZNodeEncryption encrypt = new ZNodeEncryption();

            string SMTPServer = string.Empty;
            string SMTPUsername = string.Empty;
            string SMTPPassword = string.Empty;

            // To get the SMTP settings based on PortalID
            ZNodeConfigManager.AliasSiteConfig(portalID);

            if (ZNodeConfigManager.SiteConfig.SMTPServer != null)
            {
                SMTPServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            }

            if (ZNodeConfigManager.SiteConfig.SMTPUserName != null)
            {
                SMTPUsername = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.SMTPUserName);
            }

            if (ZNodeConfigManager.SiteConfig.SMTPPassword != null)
            {
                SMTPPassword = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.SMTPPassword);
            }

            try
            {
                // Create mail client and send email
                System.Net.Mail.SmtpClient emailClient = new System.Net.Mail.SmtpClient();
                emailClient.Host = SMTPServer;
                emailClient.Credentials = new NetworkCredential(SMTPUsername, SMTPPassword);

                // Send MailContent
                emailClient.Send(EmailContent);

                EmailContent.Attachments.Dispose();

                /* Delete the attachements if any */
                if (strFileName != null && strFileName != string.Empty)
                {
                    File.Delete(Server.MapPath(strFileName));
                }

                NoteAdmin _Noteadmin = new NoteAdmin();
                Note _NoteAccess = new Note();

                _NoteAccess.CaseID = this.ItemId;

                if (txtAccountId.Value.Length > 0)
                {
                    _NoteAccess.AccountID = int.Parse(txtAccountId.Value);
                }

                _NoteAccess.CreateDte = System.DateTime.Now;
                _NoteAccess.CreateUser = HttpContext.Current.User.Identity.Name;
                _NoteAccess.NoteTitle = "Email Reply (Sub: " + lblCaseTitle.Text.Trim() + ")";
                _NoteAccess.NoteBody = ctrlHtmlText.Html;

                bool Check = _Noteadmin.Insert(_NoteAccess);

                // Log Activity
                AccountAdmin _accountAdmin = new AccountAdmin();
                Account _account = new Account();
                if (txtAccountId.Value.Length > 0)
                {
                    _account = _accountAdmin.GetByAccountID(int.Parse(txtAccountId.Value));
                    MembershipUser _user = Membership.GetUser(_account.UserID);
                    string UserName = _user.UserName;
                    string AssociateName = "Email Reply to - '" + UserName + "' Account";
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, UserName);
                }

                Response.Redirect(this.RedirectLink);
            }
            catch (Exception ex)
            {
                lblErrorMessage.Text = "There was a problem sending the email, Please verify smtp settings and try again";

                // Log exception
                ExceptionPolicy.HandleException(ex, "ZNODE_GLOBAL_EXCEPTION_POLICY");
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.RedirectLink);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Bind Data Method
        /// </summary>
        private void BindData()
        {
            CaseAdmin _CaseAdmin = new CaseAdmin();
            CaseRequest _CaseList = _CaseAdmin.GetByCaseID(this.ItemId);

            if (_CaseList != null)
            {
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (profiles.StoreAccess != "AllStores")
                {
                    string[] stores = profiles.StoreAccess.Split(',');
                    Array Stores = (Array)stores;
                    int found = Array.IndexOf(Stores, _CaseList.PortalID.ToString());
                    if (found == -1)
                    {
                        Response.Redirect("Default.aspx", true);
                    }
                }

                txtEmailSubj.Text = Server.HtmlDecode(_CaseList.Title);
                lblEmailid.Text = _CaseList.EmailID;
                Session.Add("PortalID", _CaseList.PortalID);
            }
        }

        /// <summary>
        /// Bind Values Method
        /// </summary>
        private void BindValues()
        {
            CaseAdmin _CaseAdmin = new CaseAdmin();
            CaseRequest _CaseList = _CaseAdmin.GetByCaseID(this.ItemId);

            if (_CaseList != null)
            {
                // Set General Case Information     
                if (_CaseList.AccountID.HasValue)
                {
                    txtAccountId.Value = _CaseList.AccountID.Value.ToString();
                }

                lblCaseTitle.Text = _CaseList.Title;
                lblCaseStatus.Text = this.GetCaseStatusByCaseID(_CaseList.CaseStatusID);
                lblCasePriority.Text = this.GetCasePriorityByCaseID(_CaseList.CasePriorityID);
                txtCaseDescription.Text = Server.HtmlDecode(_CaseList.Description).Replace("<br>", "\r\n");

                // Set Customer Information
                lblCustomerName.Text = _CaseList.FirstName + " " + _CaseList.LastName;
                lblCompanyName.Text = _CaseList.CompanyName;
                lblEmailTo.Text = _CaseList.EmailID;
                lblPhoneNumber.Text = _CaseList.PhoneNumber;
            }
        }

        /// <summary>
        /// Get AccountType By AccountID
        /// </summary>
        /// <param name="fieldValue">The value of fieldvalue</param>
        /// <returns>Returns the AccountID</returns>
        private string GetAccountTypeByAccountID(object fieldValue)
        {
            if (fieldValue == null)
            {
                return string.Empty;
            }
            else
            {
                CaseAdmin _CaseAdmin = new CaseAdmin();
                AccountType _AccountTypeList = _CaseAdmin.GetByAccountTypeID(int.Parse(fieldValue.ToString()));

                if (_AccountTypeList != null)
                {
                    return _AccountTypeList.AccountTypeNme;
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        /// <summary>
        /// Get Case Status By CaseID
        /// </summary>
        /// <param name="fieldValue">The value of fieldvalue</param>
        /// <returns>Returns the Case Status</returns>
        private string GetCaseStatusByCaseID(object fieldValue)
        {
            if (fieldValue == null)
            {
                return String.Empty;
            }
            else
            {
                CaseAdmin _CaseStatusAdmin = new CaseAdmin();
                CaseStatus _caseStatusList = _CaseStatusAdmin.GetByCaseStatusID(int.Parse(fieldValue.ToString()));
                if (_caseStatusList == null)
                {
                    return string.Empty;
                }
                else
                {
                    return _caseStatusList.CaseStatusNme;
                }
            }
        }

        /// <summary>
        /// Get Case Priority By CaseID
        /// </summary>
        /// <param name="fieldValue">The value of fieldValue</param>
        /// <returns>Returns the Case Priority</returns>
        private string GetCasePriorityByCaseID(object fieldValue)
        {
            if (fieldValue == null)
            {
                return String.Empty;
            }
            else
            {
                CaseAdmin _CasePriorityAdmin = new CaseAdmin();
                CasePriority _CasePriority = _CasePriorityAdmin.GetByCasePriorityID(int.Parse(fieldValue.ToString()));
                if (_CasePriority == null)
                {
                    return string.Empty;
                }
                else
                {
                    return _CasePriority.CasePriorityNme;
                }
            }
        }
        #endregion

        #region Zeon Custom Code

        /// <summary>
        /// Get Email Content 
        /// </summary>
        /// <returns>string</returns>
        private string GetEmailContent()
        {
            string emailContent = string.Empty;
            string defaultTemplatePath = string.Empty;
            string mailBody = ctrlHtmlText.Html.ToString();
            defaultTemplatePath = Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ZCustomerServiceResponse.html"));

            StreamReader streamReader = new StreamReader(defaultTemplatePath);
            string messageText = streamReader.ReadToEnd();

            string spacerImage = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/Themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/Images/space.png";
            string domainPath = Request.Url.GetLeftPart(UriPartial.Authority);
            int portalId = ZNodeConfigManager.SiteConfig.PortalID;

            ZEmailHelper zEmailHelper = new ZEmailHelper();
            ZeonMessageConfig zeonMessageConfig = new ZeonMessageConfig();
            string copyRightText = zeonMessageConfig.GetMessageKey("FooterCopyrightText");

            string messageTextHeader = zEmailHelper.EmailHeaderContent(domainPath, portalId, spacerImage);
            string messageTextFooter = zEmailHelper.EmailFooterContent(domainPath, portalId, copyRightText, spacerImage);

            //To add header to the template
            Regex rxHeaderContent = new Regex("#HeaderContent#", RegexOptions.IgnoreCase);
            messageText = rxHeaderContent.Replace(messageText, messageTextHeader);

            //To add footer to the template
            Regex rxFooterContent = new Regex("#FooterContent#", RegexOptions.IgnoreCase);
            messageText = rxFooterContent.Replace(messageText, messageTextFooter);

            Regex rxCustomerResponseText = new Regex("#CustomerResponseText#", RegexOptions.IgnoreCase);
            messageText = rxCustomerResponseText.Replace(messageText, txtEmailSubj.Text);

            Regex mailContent = new Regex("#Message#", RegexOptions.IgnoreCase);
            messageText = mailContent.Replace(messageText, mailBody);

            return messageText;
        }

        #endregion
    }
}