using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Setup.Content.Pages
{
    /// <summary>
    /// Represents the Site Admin - Default class
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Protected Member Variables
        private string ListLink = "~/SiteAdmin/Secure/Setup/Content/Pages/Default.aspx";
        private string AddLink = "~/SiteAdmin/Secure/Setup/Content/Pages/Add.aspx";
        private string RevertLink = "~/SiteAdmin/Secure/Setup/Content/Pages/Revert.aspx";
        private string EditLink = "~/SiteAdmin/Secure/Setup/Content/Pages/Add.aspx";
        private string DeleteLink = "~/SiteAdmin/Secure/Setup/Content/Pages/Delete.aspx";
        #endregion

        #region Helper Methods

        /// <summary>
        /// Search the page based on the search criteria Portal Id, Locale Id and page name.
        /// </summary>
        public void BindContentPage()
        {
            ContentPageService contentPageService = new ContentPageService();
            StringBuilder expression = new StringBuilder();
            string pageName = txtPageName.Text.Replace("'", string.Empty).Trim();

            ContentPageQuery query = new ContentPageQuery();
            if (!string.IsNullOrEmpty(pageName))
            {
                query.Append(ContentPageColumn.Name, "*" + pageName + "*");
            }

                query.AppendEquals(ContentPageColumn.LocaleId, "43");
          
            if (ddlPortal.SelectedValue != "0")
            {
                query.AppendEquals(ContentPageColumn.PortalID, ddlPortal.SelectedValue);
            }

            TList<ContentPage> contentPageList = null;
            contentPageList = contentPageService.Find(query.GetParameters());
            uxGrid.DataSource = contentPageList;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Returns the path to open the html content pages
        /// </summary>
        /// <param name="pageName">The value of Page Name</param>
        /// <param name="seoURL"> The value of seoURL</param>
        /// <returns>Returns the page URL</returns>
        public string GetPageURL(string pageName, object seoURL)
        {
            // If page name is Home, then it must be open with default page.
            if (pageName.Equals("Home"))
            {
                return "~/";
            }

            string seoUrl = string.Empty;

            if (seoURL != null)
            {
                if (seoURL.ToString().Length > 0)
                {
                    seoUrl = seoURL.ToString();
                }
            }

            // Otherwise the content pages should be open with content.aspx page
            // More Specific to Content pages
            return ZNodeSEOUrl.MakeURL(pageName, SEOUrlType.ContentPage, seoUrl);
        }

        /// <summary>
        /// Get Store Name by PortalId
        /// </summary>
        /// <param name="portalID">The value of portalID</param>
        /// <returns>Returns the Store Name</returns>
        public string GetStoreName(string portalID)
        {
            PortalAdmin portalAdmin = new PortalAdmin();

            return portalAdmin.GetStoreNameByPortalID(portalID);
        }

        /// <summary>
        /// Get Store Name by PortalId
        /// </summary>
        /// <param name="localeID">The value of localeID</param>
        /// <returns>Returns the Locale Name</returns>
        public string GetLocaleName(string localeID)
        {
            LocaleAdmin localeAdmin = new LocaleAdmin();

            return localeAdmin.GetLocaleNameByLocaleID(localeID);
        }
        #endregion

        #region Page Load Event

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindPortal();              
                this.BindGridData();
            }
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGridData();
        }

        /// <summary>
        /// Event triggered when an item is deleted from the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindGridData();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "page")
            {
            }
            else
            {
                string Id = Convert.ToString(e.CommandArgument);

                if (e.CommandName == "Edit")
                {
                    this.EditLink = this.EditLink + "?itemid=" + Id;
                    Response.Redirect(this.EditLink);
                }
                else if (e.CommandName == "Publish")
                {
                    ContentPageAdmin pageAdmin = new ContentPageAdmin();
                    ContentPage contentPage = pageAdmin.GetPageByID(int.Parse(Id));
                    pageAdmin.PublishPage(contentPage);
                    Response.Redirect(this.ListLink);
                }
                else if (e.CommandName == "Revert")
                {
                    this.RevertLink = this.RevertLink + "?itemid=" + Id;
                    Response.Redirect(this.RevertLink);
                }
                else if (e.CommandName == "Delete")
                {
                    Response.Redirect(this.DeleteLink + "?itemid=" + Id);
                }
            }
        }

        #endregion

        #region Other Events

        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, ImageClickEventArgs e)
        {
            this.BindContentPage();
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        /// <summary>
        /// Add Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddLink);
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind Portals
        /// </summary>
        private void BindPortal()
        {
            PortalAdmin portalAdmin = new PortalAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Portal> portals = portalAdmin.GetAllPortals();

            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (profiles.StoreAccess != "AllStores")
            {
                portals.Filter = string.Concat("PortalID IN [", profiles.StoreAccess, "]");
            }
            else
            {
                Portal po = new Portal();
                po.StoreName = "ALL STORES";
                portals.Insert(0, po);
            }

            // Portal Drop Down List
            ddlPortal.DataSource = portals;
            ddlPortal.DataTextField = "StoreName";
            ddlPortal.DataValueField = "PortalID";
            ddlPortal.DataBind();

            foreach (ListItem listItem in ddlPortal.Items)
            {
                listItem.Text = Server.HtmlDecode(listItem.Text);
            }
        }

        

        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindGridData()
        {
            this.BindContentPage();
        }

        #endregion
    }
}