<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" ValidateRequest="false"
    AutoEventWireup="True" Inherits="SiteAdmin.Secure.Setup.Content.Pages.Add"
    Title="Manage Pages - Add" CodeBehind="Add.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/StoreName.ascx" TagName="StoreName" TagPrefix="ZNode" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div style="width: 870px; display: table-cell;">
        <div class="LeftFloat" style="width: 50%">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
            <uc2:DemoMode ID="DemoMode1" runat="server" />
        </div>
        <div class="LeftFloat" align="right" style="width: 50%">
            <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
    <div class="ClearBoth">
    </div>
    <div class="FormView DesignPageAdd">
        <div>
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>
        <div>
            <uc1:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <h4 class="SubTitle">General Settings</h4>
        <div class="FieldStyle">
            Page Name (ex: AboutUs)<span class="Asterix">*</span>
        </div>
        <div>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtName"
                Display="Dynamic" ErrorMessage="Enter a name for the page" SetFocusOnError="True"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
                ErrorMessage="The page name can only contain letters, numbers, '_' and '-'."
                ControlToValidate="txtName" ValidationExpression="^[a-zA-Z0-9_-]+$"></asp:RegularExpressionValidator>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtName" runat="server" MaxLength="50" Columns="50"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            Page Title (ex: "About Our Company")
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtTitle" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
        </div>

        <div style="margin: 10px 0px 15px 0px;">
            <ZNode:StoreName ID="uxStoreName" VisibleLocaleDropdown="false" runat="server" />
        </div>

        <div class="FieldStyle">
            Select Theme
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlThemeslist" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlThemeslist_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
        <asp:Panel ID="pnlTemplateList" runat="server" Visible="false">
            <div class="FieldStyle">
                Master Page Template
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlPageTemplateList" runat="server">
                </asp:DropDownList>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlCssList" runat="server" Visible="false">
            <div class="FieldStyle">
                CSS
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlCSSList" runat="server">
                </asp:DropDownList>
            </div>
        </asp:Panel>
        <h4 class="SubTitle">SEO Settings</h4>
        <div class="FieldStyle">
            SEO Title
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtSEOTitle" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            SEO Keywords
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtSEOMetaKeywords" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            SEO Description
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtSEOMetaDescription" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            Enter Additional Meta Information<br />
            <small>Enter the full HTML that you would like to be entered as a new meta tag (e.g. &lt;meta name="google-site-verification" content="xyz123" />)</small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtMetaTagAdditional" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
        </div>
        <asp:Panel ID="pnlSEOURL" runat="server">
            <div class="FieldStyle">
                SEO Friendly Page Name<br />
                <small>Use only characters a-z and 0-9. Use "-" instead of spaces. Do not use a file
                    extension or parameters in your product name.</small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOUrl" runat="server" MaxLength="100" Columns="50"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtSEOUrl"
                    CssClass="Error" Display="Dynamic" ErrorMessage="Enter valid SEO friendly URL"
                    SetFocusOnError="True" ValidationExpression="([A-Za-z0-9-_]+)"></asp:RegularExpressionValidator>
            </div>
            <div class="FieldStyle"></div>
            <div class="ValueStyleText">
                <asp:CheckBox ID="chkAddURLRedirect" runat="server" Text=' Add 301 redirect on URL changes.' />
            </div>
            <div>
                <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
        </asp:Panel>
        <h4 class="SubTitle">Page Content</h4>
        <div class="ValueStyle" style="width: 99%;">
            <uc1:HtmlTextBox ID="ctrlHtmlText" runat="server"></uc1:HtmlTextBox>
        </div>
        <div class="ClearBoth" align="left">
        </div>
        <div>
            <asp:ImageButton ID="btnSubmit" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Submit" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>
