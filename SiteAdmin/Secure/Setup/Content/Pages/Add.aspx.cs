using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Setup.Content.Pages
{
    /// <summary>
    /// Represents the SiteAdmin - Add class
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region Protected Variables
        private int ItemId;
        private string CancelLink = "~/SiteAdmin/Secure/Setup/Content/Pages/Default.aspx";
        private string NextLink = "~/SiteAdmin/Secure/Setup/Content/Pages/Default.aspx";
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (!Page.IsPostBack)
            {
                this.BindThemeList();

                // If edit func then bind the data fields
                if (this.ItemId > 0)
                {
                    lblTitle.Text = "Edit Content Page";
                    txtName.Enabled = false;
                }
                else
                {
                    lblTitle.Text = "Add a Content Page";
                }

                // Bind data to the fields on the page
                this.BindData();
            }
        }
        #endregion

        #region General Events

        /// <summary>
        /// Themes List Dropdown Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlThemeslist_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlTemplateList.Visible = true;
            pnlCssList.Visible = true;
            ddlPageTemplateList.Items.Clear();
            ddlCSSList.Items.Clear();
            this.BindMasterPageTemplates();
            this.BindCssList();

            if (ddlThemeslist.SelectedValue == "0")
            {
                pnlTemplateList.Visible = false;
                pnlCssList.Visible = false;
                ddlPageTemplateList.SelectedValue = "0";
                ddlCSSList.SelectedValue = "0";
            }
        }

        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ContentPageAdmin pageAdmin = new ContentPageAdmin();
            ContentPage contentPage = new ContentPage();
            string mappedSEOUrl = string.Empty;

            bool allowDelete = true;

            // If edit mode then retrieve data first
            if (this.ItemId > 0)
            {
                contentPage = pageAdmin.GetPageByID(this.ItemId);

                // Override this setting
                allowDelete = contentPage.AllowDelete;
                if (contentPage.SEOURL != null)
                {
                    mappedSEOUrl = contentPage.SEOURL;
                }
            }
            else
            {
                // Add mode - check if this page name already exists
                if (!pageAdmin.IsNameAvailable(txtName.Text.Trim(), uxStoreName.SelectedValue, ZNodeCatalogManager.CatalogConfig.LocaleID))
                {
                    lblMsg.Text = "A page with this name already exists in the database. Please enter a different name.";
                    return;
                }
            }

            // Set values
            contentPage.ActiveInd = true;
            contentPage.Name = txtName.Text.Trim();
            contentPage.PortalID = uxStoreName.SelectedValue;
            contentPage.LocaleId = uxStoreName.LocaleSelectedValue;
            contentPage.SEOMetaDescription = txtSEOMetaDescription.Text;
            contentPage.MetaTagAdditional = txtMetaTagAdditional.Text;
            contentPage.SEOMetaKeywords = txtSEOMetaKeywords.Text;
            contentPage.SEOTitle = txtSEOTitle.Text;
            contentPage.SEOURL = null;

            if (txtSEOUrl.Text.Trim().Length > 0 && !contentPage.Name.ToLower().Equals("home"))
            {
                contentPage.SEOURL = txtSEOUrl.Text.Trim().Replace(" ", "-");
            }

            contentPage.Title = txtTitle.Text;
            contentPage.AllowDelete = allowDelete;

            if (contentPage.Name.ToLower().Equals("home"))
            {
                contentPage.AllowDelete = false;
            }

            if (ddlThemeslist.SelectedValue != "0")
            {
                contentPage.Theme = ddlThemeslist.SelectedValue;
            }
            else
            {
                contentPage.Theme = null;
            }

            if (ddlPageTemplateList.SelectedValue != "0")
            {
                contentPage.MasterPage = ddlPageTemplateList.SelectedValue;
            }
            else
            {
                contentPage.MasterPage = null;
            }

            if (ddlCSSList.SelectedValue != "0")
            {
                contentPage.CSS = ddlCSSList.SelectedValue;
            }
            else
            {
                contentPage.CSS = null;
            }

            bool retval = false;

            if (this.ItemId > 0)
            {
                string oldcontent = string.Empty;
                if (ViewState["Oldcontent"] != null)
                {
                    oldcontent = ViewState["Oldcontent"].ToString();
                }

                // Update code here
                retval = pageAdmin.UpdatePage(contentPage, ctrlHtmlText.Html, oldcontent, uxStoreName.SelectedValue, uxStoreName.LocaleSelectedValue.ToString(), HttpContext.Current.User.Identity.Name, mappedSEOUrl, chkAddURLRedirect.Checked);

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, lblTitle.Text, txtName.Text.Trim());
            }
            else
            {
                // Add code here
                retval = pageAdmin.AddPage(contentPage, ctrlHtmlText.Html, uxStoreName.SelectedValue, uxStoreName.LocaleSelectedValue.ToString(), HttpContext.Current.User.Identity.Name, mappedSEOUrl, chkAddURLRedirect.Checked);

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, lblTitle.Text, txtName.Text.Trim());
            }

            if (retval)
            {
                // Redirect to main page
                Response.Redirect(this.NextLink);
            }
            else
            {
                if (contentPage.SEOURL != null)
                {
                    // Display error message
                    lblMsg.Text = "Failed to update page. Please check with SEO Url settings and try again.";
                }
                else
                {
                    // Display error message
                    lblMsg.Text = "Failed to update page. Please try again.";
                }
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.CancelLink);
        }

        #endregion

        #region Bind Data

        /// <summary>
        /// Bind MasterPages
        /// </summary>
        protected void BindMasterPageTemplates()
        {
            if (ddlThemeslist.SelectedIndex != 0)
            {
                // Create instance for direcoryInfo and specify the directory 'MasterPages/Product'
                DirectoryInfo directoryInfo = new DirectoryInfo(Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ApplicationPath + "/Themes/" + ddlThemeslist.SelectedItem.Text + "/MasterPages/"));

                // Determine whether the directory 'MasterPages/Product' exists.
                if (directoryInfo.Exists)
                {
                    // Returns a master file list from the current directory.
                    FileInfo[] masterFiles = directoryInfo.GetFiles("*.master");

                    foreach (FileInfo masterPage in masterFiles)
                    {
                        string fileName = masterPage.Name;
                        fileName = fileName.Replace(".master", string.Empty); // Name only

                        ddlPageTemplateList.Items.Add(fileName);
                    }
                }

                // Master template
                ListItem li = new ListItem("Select Template", "0");
                ddlPageTemplateList.Items.Insert(0, li);
            }
        }

        /// <summary>
        /// Bind data to the fields 
        /// </summary>
        protected void BindData()
        {
            ContentPageAdmin pageAdmin = new ContentPageAdmin();
            ContentPage contentPage = new ContentPage();

            if (this.ItemId > 0)
            {
                contentPage = pageAdmin.GetPageByID(this.ItemId);

                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (profiles.StoreAccess != "AllStores")
                {
                    string[] stores = profiles.StoreAccess.Split(',');
                    Array Stores = (Array)stores;
                    int found = Array.IndexOf(Stores, contentPage.PortalID.ToString());
                    if (found == -1)
                    {
                        Response.Redirect("Default.aspx", true);
                    }
                }

                // Set fields          
                txtName.Text = contentPage.Name.Trim();
                txtSEOMetaDescription.Text = contentPage.SEOMetaDescription;
                txtMetaTagAdditional.Text = contentPage.MetaTagAdditional;
                txtSEOMetaKeywords.Text = contentPage.SEOMetaKeywords;
                txtSEOTitle.Text = contentPage.SEOTitle;
                txtSEOUrl.Text = contentPage.SEOURL;
                txtTitle.Text = contentPage.Title;

                if (contentPage.Theme != null)
                {
                    ddlThemeslist.SelectedValue = contentPage.Theme;
                }

                if (ddlThemeslist.SelectedValue != "0")
                {
                    pnlTemplateList.Visible = true;
                    pnlCssList.Visible = true;
                    this.BindMasterPageTemplates();
                    this.BindCssList();

                    if (contentPage.MasterPage != null)
                    {
                        ddlPageTemplateList.SelectedValue = contentPage.MasterPage;
                    }

                    if (contentPage.CSS != null)
                    {
                        ddlCSSList.SelectedValue = contentPage.CSS;
                    }
                }

                uxStoreName.PreSelectValue = contentPage.PortalID.ToString();
                uxStoreName.LocalePreSelectValue = contentPage.LocaleId.ToString();

                // Get content
                ctrlHtmlText.Html = pageAdmin.GetPageHTMLByName(contentPage.Name, contentPage.PortalID, contentPage.LocaleId.ToString());
                ViewState["Oldcontent"] = ctrlHtmlText.Html;
                if (contentPage.Name.Contains("Home"))
                {
                    pnlSEOURL.Visible = false;
                }
            }
        }

        /// <summary>
        /// Binds the themes to the DropDownList
        /// </summary>
        private void BindThemeList()
        {
            System.IO.DirectoryInfo path = new System.IO.DirectoryInfo(Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ApplicationPath + "/Themes/"));
            ddlThemeslist.DataSource = path.GetDirectories();
            ddlThemeslist.DataBind();
            ListItem li = new ListItem("Site Theme", "0");
            ddlThemeslist.Items.Insert(0, li);

            // Remove the svn hidden path folder.
            ddlThemeslist.Items.Remove(".svn");
        }

        /// <summary>
        /// Bind the CSS to the DropDownList
        /// </summary>
        private void BindCssList()
        {
            if (ddlThemeslist.SelectedIndex != 0)
            {
                pnlCssList.Visible = true;
                System.IO.DirectoryInfo path = new System.IO.DirectoryInfo(Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ApplicationPath + "/Themes/" + ddlThemeslist.SelectedItem.Text + "/"));
                DirectoryInfo[] directoryInfoList = path.GetDirectories("Css");

                foreach (DirectoryInfo directory in directoryInfoList)
                {
                    // Returns a master file list from the current directory.
                    FileInfo[] masterFiles = directory.GetFiles("*.css");

                    foreach (FileInfo masterPage in masterFiles)
                    {
                        string fileName = masterPage.Name;

                        // Name only
                        fileName = fileName.Replace(".css", string.Empty);

                        ddlCSSList.Items.Add(fileName);
                    }
                }

                ListItem li = new ListItem("Site CSS", "0");
                ddlCSSList.Items.Insert(0, li);
            }
        }

        #endregion
    }
}