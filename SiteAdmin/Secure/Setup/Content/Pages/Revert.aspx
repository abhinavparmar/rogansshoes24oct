<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Setup.Content.Pages.Revert" Title="Manage Pages - Revert" CodeBehind="Revert.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div>
        <div class="LeftFloat" style="width: 50%">
            <h1>Revisions for Page:
                <%=PageName %><uc2:DemoMode ID="DemoMode1" runat="server" />
            </h1>
        </div>
        <div class="LeftFloat ImageButtons" style="width: 49%" align="right">
            <asp:Button CssClass="Size100" ID="btnBack" CausesValidation="False" Text="< Go Back"
                runat="server" OnClick="BtnBack_Click" />
        </div>
    </div>
    <div class="ClearBoth" align="left">
    </div>
    <p>
        If you need to revert back to a previous version of this page, click on the "Revert"
        button next to the target revision.
    </p>
    <div>
        <asp:Label ID="lblMsg" runat="server" Font-Bold="True" ForeColor="#00C000"></asp:Label>
    </div>
    <div>
        <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
    </div>
    <h4 class="GridTitle">Revisions</h4>
    <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
        CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
        CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" PageSize="25"
        OnRowDeleting="UxGrid_RowDeleting" AllowSorting="True" EmptyDataText="No content pages exist in the database.">
        <Columns>
            <asp:BoundField DataField="RevisionID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
            <asp:BoundField DataField="UpdateUser" HeaderText="Updated By" HeaderStyle-HorizontalAlign="Left" />
            <asp:BoundField DataField="UpdateDate" HeaderText="Updated On" HeaderStyle-HorizontalAlign="Left" />
            <asp:BoundField DataField="Description" HeaderText="Description" HeaderStyle-HorizontalAlign="Left"
                ItemStyle-Width="20%" />
            <asp:ButtonField CommandName="Revert" Text="Revert to this Version &raquo" ButtonType="Link">
                <ControlStyle CssClass="Button" />
            </asp:ButtonField>
        </Columns>
        <FooterStyle CssClass="FooterStyle" />
        <RowStyle CssClass="RowStyle" />
        <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
        <HeaderStyle CssClass="HeaderStyle" />
        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
        <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
    </asp:GridView>
    <div>
        <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
    </div>
</asp:Content>
