<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master"
    AutoEventWireup="True" Inherits="SiteAdmin.Secure.Setup.Content.Messages.Default" Title="Manage Messages"
    CodeBehind="Default.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/StoreName.ascx" TagName="StoreName"
    TagPrefix="ZNode" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode"
    TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <uc2:DemoMode ID="DemoMode1" runat="server" />
    <div class="Form">
        <div class="ManageMessageHeading">
            <h1>Manage Messages
            </h1>
            <p class="ManageMessageText">
                Edit messages displayed in different areas of your store. Messages can be plain text or rich text and can include images.
            </p>
            <div>
                <uc1:Spacer ID="Spacer6" SpacerHeight="20" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
        </div>
        <div class="ButtonStyle">
            <zn:LinkButton ID="btnAdd" runat="server" CausesValidation="False"
                ButtonType="Button" OnClick="BtnAdd_Click" Text="Add Message"
                ButtonPriority="Primary" />
        </div>
        <div>

            <h4 class="SubTitle">Search Messages</h4>
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">Store Name</span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlPortal" runat="server">
                            </asp:DropDownList>
                        </span>
                    </div>
                   
                    <div class="ItemStyle">
                        <span class="FieldStyle">Message</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtMessageKey" runat="server"></asp:TextBox>
                            <div>
                                <asp:RegularExpressionValidator ID="MessageValidator" runat="server" ErrorMessage="Please enter a search string which does not contain special characters."
                                    ControlToValidate="txtMessageKey" meta:resourceKey="MessageValidator" Display="Dynamic"
                                    ValidationExpression='[A-Za-z0-9\s]*' CssClass="Error"></asp:RegularExpressionValidator>
                            </div>
                        </span>
                    </div>
                </div>
            </div>
            <div class="ClearBoth" align="left">
            </div>
            <div>
                <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                    runat="server" AlternateText="Search" OnClick="BtnSearch_Click" CausesValidation="true" />
                <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnClear_Click" />
            </div>
            <div>
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
            </div>
            <br />
            <h4 class="GridTitle">Available Custom Messages</h4>
            <asp:GridView ID="uxGrid" runat="server" PageSize="50" CssClass="Grid" AllowPaging="True"
                AutoGenerateColumns="False" CellPadding="4" OnRowCommand="UxGrid_RowCommand"
                GridLines="None" CaptionAlign="Left" Width="100%" AllowSorting="True" EmptyDataText="No custom messages available."
                OnRowCreated="UxGrid_RowCreated" OnRowDataBound="UxGrid_DataBound"
                OnPageIndexChanging="UxGrid_PageIndexChanging" OnRowDeleting="UxGrid_RowDeleting">
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                <Columns>
                    <asp:BoundField DataField="Description" HtmlEncode="false" HeaderText="Message" HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="Key" HeaderText="Location" HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText="Store Name" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="lblStoreName" runat="server" Text='<%# GetStoreName(DataBinder.Eval(Container.DataItem,"PortalID").ToString()) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                   
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="Button" runat="server" Text="Manage &raquo" CommandName="Edit"
                                CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Key") %>' CssClass="actionlink" />
                            <asp:LinkButton ID="lbDelete" runat="server" Text="Delete Message &raquo" CommandName="Delete"
                                CommandArgument='<%#DataBinder.Eval(Container.DataItem,"MessageID") %>' CssClass="actionlink" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="PortalID" HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="LocaleID" HeaderStyle-HorizontalAlign="Left" />
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
