<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True"
    Inherits="SiteAdmin.Secure.Setup.Content.Messages.AddMessage" CodeBehind="AddMessage.aspx.cs"
    ValidateRequest="false" Title="Manage Messages - Edit" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/StoreName.ascx" TagName="StoreName"
    TagPrefix="ZNode" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Form">
        <asp:XmlDataSource ID="XmlDataSource1" runat="server"></asp:XmlDataSource>
        <br />
        <h1>
            <asp:Label ID="lbltitle" runat="server"></asp:Label></h1>
        <br />
        <div class="FormView">
            <div class="FieldStyle">
                Message Key
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtMessageKey" runat="server" Width="350"> </asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvMessageKey" runat="server" ErrorMessage="* Enter message key"
                    ControlToValidate="txtMessageKey" CssClass="Error" Display="Dynamic" SetFocusOnError="True" ValidationGroup="Messages"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                    ControlToValidate="txtMessageKey" CssClass="Error" Display="Dynamic" ValidationGroup="Messages" ErrorMessage="Enter a valid message key."
                    SetFocusOnError="True" ValidationExpression="([A-Za-z0-9]+)"></asp:RegularExpressionValidator>
            </div>
            <div class="ClearBoth" align="left">
            </div>

            <div class="FieldStyle">
                Description
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtDescription" runat="server" Width="350"> </asp:TextBox>
            </div>
            <div class="ClearBoth" align="left">
            </div>
            <div class="ValueStyle">
                <ZNode:StoreName ID="uxStoreName" runat="server" VisibleLocaleDropdown="false"/>
            </div>
        </div>
        <div class="ClearBoth" align="left">
        </div>
        <div class="ValueStyle">
            <uc1:HtmlTextBox ID="ctrlHtmlText" runat="server"></uc1:HtmlTextBox>
        </div>
        <br />
        <div>
            <asp:ImageButton ID="SubmitButton" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" ValidationGroup="Messages"
                CausesValidation="true" />
            <asp:ImageButton ID="CancelButton" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
        <br />
        <div>
            <asp:Label ID="lblmsg" runat="server" Font-Bold="True" CssClass="Error"></asp:Label>
        </div>
        <asp:HiddenField ID="hdnMessageID" runat="server" />
        <asp:HiddenField ID="hdnDescription" runat="server" />
    </div>
</asp:Content>
