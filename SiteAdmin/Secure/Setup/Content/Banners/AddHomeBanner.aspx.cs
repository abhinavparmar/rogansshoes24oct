﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace WebApp.SiteAdmin.Secure.Setup.Content.Banners
{
    public partial class AddHomeBanner : System.Web.UI.Page
    {
        #region Private Member

        private int itemId = 0;
        private string portalId = string.Empty;

        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.QueryString["itemid"] != null)
            {
                int.TryParse(Request.QueryString["itemid"], out this.itemId);
            }

            if (!Page.IsPostBack)
            {
                if (this.itemId == 0)
                {
                    lbltitle.Text = "Add Home Banner";
                }

                this.Bind();
            }
        }

        #endregion

        #region Helper Methods
        /// <summary>
        /// Bind Method
        /// </summary>
        public void Bind()
        {
            PerfiHomePageBannerService bannerService = new PerfiHomePageBannerService();
            PerfiHomePageBanner homeBanner = new PerfiHomePageBanner();
            if (this.itemId > 0)
            {
                homeBanner = bannerService.GetByBannerID(this.itemId);
                if (homeBanner != null && homeBanner.BannerID > 0)
                {
                    ctrlHtmlText.Html = homeBanner.BannerContent;
                    string TitleMsg = homeBanner.BannerName;
                    lbltitle.Text = "Manage Home Banner - " + TitleMsg.Replace("-", string.Empty);
                    uxStoreName.PreSelectValue = Convert.ToString(homeBanner.PortalID);
                    uxStoreName.LocalePreSelectValue = Convert.ToString(43);
                    hdnHomePageBannerID.Value = homeBanner.BannerID.ToString();
                    txtBannerName.Text = HttpUtility.HtmlEncode(homeBanner.BannerName.ToString());
                    txtStartDate.Text = homeBanner.StartDate.ToShortDateString();
                    txtEndDate.Text = homeBanner.EndDate.ToShortDateString();
                    txtDisplayOrder.Text = homeBanner.DisplayOrder.ToString();
                    chkIsActive.Checked = homeBanner.ActiveInd != null && homeBanner.ActiveInd == true ? true : false;
                }
            }
            else
            {
                uxStoreName.EnableControl = true;
                txtStartDate.Text = System.DateTime.Today.Date.ToShortDateString();
                txtEndDate.Text = System.DateTime.Today.Date.AddDays(7).ToShortDateString();
            }



        }
        #endregion

        #region Protected Events

        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            PerfiHomePageBannerService perfiBannerService = new PerfiHomePageBannerService();
            PerfiHomePageBanner perfiBanner = new PerfiHomePageBanner();

            bool Status = false;

            if (!string.IsNullOrEmpty(hdnHomePageBannerID.Value) && int.Parse(hdnHomePageBannerID.Value) > 0)
            {
                perfiBanner.BannerID = int.Parse(hdnHomePageBannerID.Value);
            }

            perfiBanner.BannerName = HttpUtility.HtmlEncode(txtBannerName.Text);
            perfiBanner.BannerContent = ctrlHtmlText.Html;
            perfiBanner.StartDate = Convert.ToDateTime(txtStartDate.Text.Trim());
            perfiBanner.EndDate = Convert.ToDateTime(txtEndDate.Text.Trim());
            perfiBanner.PortalID = uxStoreName.SelectedValue;
            perfiBanner.DisplayOrder = int.Parse(txtDisplayOrder.Text);
            perfiBanner.ActiveInd = chkIsActive.Checked;
            try
            {
                if (perfiBanner.BannerID > 0)
                {
                    Status = perfiBannerService.Update(perfiBanner);
                }
                else
                {
                    Status = perfiBannerService.Insert(perfiBanner);
                }

                if (!Status)
                {
                    lblmsg.Text = "Unable to update the Message Config. Please try again.";
                    return;
                }
                else
                {
                    string extractStr = ctrlHtmlText.Html.ToString();
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit Messages ", extractStr.Length > 255 ? extractStr.Substring(0, 255) : extractStr);
                }
                Response.Redirect("~/SiteAdmin/Secure/Setup/Content/Banners/HomePageBanners.aspx");

            }
            catch (Exception ex)
            {
                // Display error message
                lblmsg.Text = "An error occurred while updating. " + ex.Message + " Please try again. ";
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("An error occurred while updating home page banner. " + ex.InnerException.ToString());
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Setup/Content/Banners/HomePageBanners.aspx");
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Check if Start date and End date are valid
        /// </summary>
        /// <param name="startDate">DateTime</param>
        /// <param name="enddate">DateTime</param>
        /// <returns>bool</returns>
        private bool isValidBanner(DateTime startDate, DateTime endDate, int portalID)
        {
            bool isValidBannerDuration = false;
            PerfiBannerHelper bannerHelper = new PerfiBannerHelper();
            isValidBannerDuration = bannerHelper.ValidateBanner(startDate, endDate, this.itemId, portalID);
            return isValidBannerDuration;
        }
        #endregion
    }
}