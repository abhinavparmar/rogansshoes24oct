using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Setup.Checkout.Shipment
{
    /// <summary>
    /// Represents the SiteAdmin- Admin_Secure_settings_ship_Default class
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Protected Member Variables
        private string CatalogImagePath = string.Empty;
        private string AddLink = "~/SiteAdmin/Secure/Setup/Checkout/Shipment/add.aspx";
        private string AddRuleLink = "~/SiteAdmin/Secure/Setup/Checkout/Shipment/addRule.aspx";
        private string ViewLink = "~/SiteAdmin/Secure/Setup/Checkout/Shipment/view.aspx";
        private string EditLink = "~/SiteAdmin/Secure/Setup/Checkout/Shipment/add.aspx";
        private string DeleteLink = "~/SiteAdmin/Secure/Setup/Checkout/Shipment/delete.aspx";
        #endregion

        #region Public Methods
        /// <summary>
        /// Method to show "Add Shipping Rule" button for Custom Shipping options
        /// </summary>
        /// <param name="value">Represents a value</param>
        /// <returns>Returns a bool value</returns>
        public bool CheckForCustomShipping(string value)
        {
            if (int.Parse(value) == 1)
            {
                return true;
            }

            return false;
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindGridData();
            }
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGridData();
        }

        /// <summary>
        /// Event triggered when an item is deleted from the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
           this.BindGridData();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // property to an Integer.
                string Id = e.CommandArgument.ToString();

                if (e.CommandName == "Edit")
                {
                    this.EditLink = this.EditLink + "?itemid=" + Id;
                    Response.Redirect(this.EditLink);
                }

                if (e.CommandName == "View")
                {
                    this.ViewLink = this.ViewLink + "?itemid=" + Id;
                    Response.Redirect(this.ViewLink);
                }
                else if (e.CommandName == "Delete")
                {
                    Response.Redirect(this.DeleteLink + "?itemid=" + Id);
                }
            }
        }

        #endregion

        #region Other Events
        /// <summary>
        /// Add Shipping Option Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddLink);
        }

        /// <summary>
        /// Add Shipping Rule Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnShippingRules_Click(object sender, EventArgs e)
        {
            string shippingId = ((LinkButton)sender).CommandArgument;
            Response.Redirect(this.AddRuleLink + "?sid=" + shippingId);
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindGridData()
        {
            ShippingAdmin shipAdmin = new ShippingAdmin();
            uxGrid.DataSource = UserStoreAccess.CheckProfileAccess(shipAdmin.GetAllShippingOptions().Tables[0], true);
            uxGrid.DataBind();
        }
        #endregion

        #region Validate CountryCode
        public string DestinationCountryCode(object strArgument)
        {
            string aa = "ALL";
            if (strArgument.ToString().Trim() != "")
                aa =strArgument.ToString();
            return aa;
        }
        #endregion 

    }
}