<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Setup.Checkout.Shipment.Delete" Title="Manage Shipping - Delete" CodeBehind="Delete.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <h5>Please Confirm<uc1:DemoMode ID="DemoMode1" runat="server" />
    </h5>
    <p>
        Please confirm if you want to delete the shipping option named <b>"<% = ShippingOptionName %>"</b>.
        Note that this change cannot be undone.
    </p>
    <div>
        <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label><br />
        <br />
    </div>
    <asp:ImageButton ID="btnDelete" onmouseover="this.src='../../../../Themes/Images/buttons/button_delete_highlight.gif';"
        onmouseout="this.src='../../../../Themes/Images/buttons/button_delete.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_delete.gif"
        runat="server" AlternateText="Delete" OnClick="BtnDelete_Click" CausesValidation="true" />
    <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
        onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
        runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
    <br />
    <br />
    <br />
</asp:Content>
