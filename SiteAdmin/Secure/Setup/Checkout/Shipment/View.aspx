<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" ValidateRequest="false" Inherits="SiteAdmin.Secure.Setup.Checkout.Shipment.View" Title="Untitled Page" Codebehind="View.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div>
        <div class="ShippingHeader">
            <h1>
                Shipping Option -
                <asp:Label ID="lblDescription" runat="server"></asp:Label></h1>
        </div>
        <div align="right" class="RightFloat ImageButtons">
            <asp:Button ID="EditShipping" runat="server" CssClass="Size100" Text="Edit Shipping"
                OnClick="EditShipping_Click" ImageUrl="~/SiteAdmin/Themes/images/button_EditShipping.jpg" />
            <asp:Button ID="btnBack" runat="server" CssClass="Size100" Text="<< Back" OnClick="BtnBack_Click"
                ImageUrl="~/SiteAdmin/Themes/images/button_back.jpg" />
        </div>
        <div class="ClearBoth">
            <br />
        </div>
        <h4 class="SubTitle">
            General Information</h4>
        <div class="ViewForm200">
            <div class="FieldStyle">
                Profile Name</div>
            <div class="ValueStyle">
                <asp:Label ID="lblProfileName" runat="server" Text=""></asp:Label></div>
            <div class="FieldStyleA">
                Shipping Code</div>
            <div class="ValueStyleA">
                <asp:Label ID="lblShippingCode" runat="server"></asp:Label></div>
            <div class="FieldStyle">
                Shipping Type</div>
            <div class="ValueStyle">
                <asp:Label ID="lblShippingType" runat="server"></asp:Label></div>
            <div class="FieldStyleA">
                Destination Country</div>
            <div class="ValueStyleA">
                <asp:Label ID="lblDestinationCountry" runat="server"></asp:Label></div>
            <div class="FieldStyle">
                Handling Charge</div>
            <div class="ValueStyle"> 
          <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;
                <asp:Label ID="lblHandlingCharge" runat="server"></asp:Label></div>
            <div class="FieldStyleA">
                Display Order</div>
            <div class="ValueStyleA">
                <asp:Label ID="lblDisplayOrder" runat="server"></asp:Label></div>
            <div class="FieldStyle">
                Enabled</div>
            <div class="ValueStyle">
                <img id="imgActive" runat="server" alt="" /></div>
            <asp:Panel ID="pnlShippingRuletypes" runat="server" Visible="false">
                <h4 class="SubTitle">
                    Shipping Rules</h4>
                <div style="float: left; width: 70%;">
                    Shipping rules determine shipping costs based on quantity, weight and other parameters.
                    <b>You will need to add at least one rule</b> in order to use a shipping option.
                </div>
                <div class="ButtonStyle">
                     <zn:LinkButton ID="btnAddRule" runat="server" 
                ButtonType="Button" OnClick="BtnAddRule_Click" Text="Add Rule" 
                ButtonPriority="Primary"/>
                </div><br />
                <div style="clear: both;">
                    <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
                        CellPadding="4" ForeColor="#333333" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
                        CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" PageSize="25"
                        OnRowDeleting="UxGrid_RowDeleting" AllowSorting="True" EmptyDataText="No rules have been added.">
                        <Columns>
                            <asp:BoundField DataField="ShippingRuleID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="ShippingRuleTypeName" HeaderText="Rule Type" HeaderStyle-HorizontalAlign="Left" />
                            <asp:TemplateField HeaderText="Base Cost" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "BaseCost","{0:c}").ToString()%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Per Unit Cost" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "PerItemCost","{0:c}").ToString()%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="LowerLimit" HeaderText="Lower Limit" HeaderStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="UpperLimit" HeaderText="Upper Limit" HeaderStyle-HorizontalAlign="Left" />
                            <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="150">
                                <ItemTemplate>
                                    <div class="LeftFloat" style="width: 50%; text-align: left">
                                        <asp:LinkButton ID="editbutton" runat="server" Text="EDIT &raquo" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ShippingRuleID")%>'
                                            CommandName="Edit"></asp:LinkButton>
                                    </div>
                                    <div>
                                        <asp:LinkButton ID="deletebutton" runat="server" Text="DELETE &raquo" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ShippingRuleID")%>'
                                            CommandName="Delete"></asp:LinkButton>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle CssClass="FooterStyle" />
                        <RowStyle CssClass="RowStyle" />
                        <PagerStyle CssClass="PagerStyle" />
                        <HeaderStyle CssClass="HeaderStyle" />
                        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                        <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                    </asp:GridView>
                </div>
            </asp:Panel>
        </div>
        </div>
</asp:Content>
