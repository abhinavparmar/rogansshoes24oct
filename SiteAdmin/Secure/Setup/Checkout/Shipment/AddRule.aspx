<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True"  Inherits="SiteAdmin.Secure.Setup.Checkout.Shipment.AddRule"
    Title="Manage Shipping - Add Rule" ValidateRequest="false" Codebehind="AddRule.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <div style="text-align: right">
            <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
        <div class="LeftFloat" style="width: auto; text-align: left ;margin-top :-20px;">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
                <uc2:DemoMode ID="DemoMode1" runat="server" />
            </h1>
            <p>Create a rule to calculate shipping cost for this option during checkout. Rules can be based on flat rate, quantity, weight or specific products.</p>
        </div>
        
        <div class="ClearBoth">
            <asp:Label ID="lblMsg" CssClass="Error" runat="server"></asp:Label></div>
        <div>
            <uc1:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <h4 class="SubTitle">
            Select a Rule Type</h4>
        <div class="ValueStyle">
            <asp:DropDownList ID="lstShippingRuleType" runat="server" OnSelectedIndexChanged="LstShippingRuleType_SelectedIndexChanged"
                AutoPostBack="true">
            </asp:DropDownList>
        </div>
        <h4 class="SubTitle">
            Set Pricing</h4>
        <div class="FieldStyle">
            Base Cost<br />
            <small>Enter the base cost which is applied irrespective of the number of items.</small></div>
        <div class="ValueStyle">
            <span class="DollarSymbol">$ </span>
            <asp:TextBox ID="txtBaseCost" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtBaseCost" Display="Dynamic"
                ErrorMessage="Enter a Base Cost" SetFocusOnError="True"></asp:RequiredFieldValidator><asp:CompareValidator
                    ID="CompareValidator2" runat="server" ControlToValidate="txtBaseCost" Type="Currency"
                    Display="Dynamic" Operator="DataTypeCheck" /></div>
                    
        <asp:Panel ID="pnlNonFixedRate" runat="server">
            <div class="FieldStyle">
                Per Unit Cost
                <br />
                <small>Enter the shipping cost to be applied to each unit in the order.</small></div>
           
            <div class="ValueStyle">
                <span class="DollarSymbol">$ </span>
                <asp:TextBox ID="txtPerItemCost" runat="server"></asp:TextBox>
               <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtPerItemCost"
                    Display="Dynamic" ErrorMessage="Enter a Per Item Cost" SetFocusOnError="True"></asp:RequiredFieldValidator>
               <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txtPerItemCost"
                    Type="Currency" Display="Dynamic" Operator="DataTypeCheck" /></div>
        </asp:Panel>
        <asp:Panel ID="pnlNonFlat" runat="server" Visible="false">
            <h4 class="SubTitle">
                Enter Limits for tiered pricing</h4>
            <p>
                The lower and upper limits will create a tiered pricing scheme. For example you
                may want to create a rule where shipping for items from 0-10 cost $10. In this case
                you would set the lower limit to 0 and upper limit to 10
            </p>
            <div class="FieldStyle">
                Lower Limit (<% =TierText %>)</div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtLowerLimit" runat="server">0.00</asp:TextBox><asp:RequiredFieldValidator
                    ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtLowerLimit"
                    Display="Dynamic" ErrorMessage="Enter a Lower Limit" SetFocusOnError="True"></asp:RequiredFieldValidator><asp:CompareValidator
                        ID="CompareValidator4" runat="server" ControlToValidate="txtLowerLimit" Type="Double"
                        Operator="DataTypeCheck" Display="Dynamic" ErrorMessage="You must enter a valid lower limit (ex: 2)" /></div>
            <div class="FieldStyle">
                Upper Limit (<% =TierText %>)</div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtUpperLimit" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                    ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtUpperLimit"
                    Display="Dynamic" ErrorMessage="Enter a Upper Limit" SetFocusOnError="True"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="txtUpperLimit"
                    Type="Double" Operator="DataTypeCheck" ErrorMessage="You must enter a valid upper limit (ex: 2)"
                    Display="Dynamic" /></div>
        </asp:Panel>
        <div class="ClearBoth">
            <br />
        </div>
        <div>
            <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>
