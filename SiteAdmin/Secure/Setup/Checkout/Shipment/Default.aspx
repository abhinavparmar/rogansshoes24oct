<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Setup.Checkout.Shipment.Default" Title="Manage Shipping - List" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>Shipping</h1>
        </div>
        <div class="ButtonStyle">
            <zn:LinkButton ID="btnAdd" runat="server" CausesValidation="False"
                ButtonType="Button" OnClick="BtnAdd_Click" Text="Add New Shipping Option"
                ButtonPriority="Primary" />
        </div>
        <div class="ClearBoth">
            <p>
                Set up shipping options such as Custom Rates, UPS, and FedEx.
            </p>
        </div>


        <div>
            <uc1:Spacer ID="Spacer8" runat="server" SpacerHeight="15" SpacerWidth="3" />
        </div>
        <h4 class="GridTitle">Shipping Options List</h4>
        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
            CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" PageSize="25"
            OnRowDeleting="UxGrid_RowDeleting" AllowSorting="True" EmptyDataText="No records exist in the database.">
            <Columns>
                <asp:BoundField DataField="ShippingID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText="Shipping Code" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <a href='view.aspx?itemid=<%# DataBinder.Eval(Container.DataItem, "ShippingID").ToString()%>'>
                            <%# Server.HtmlEncode(DataBinder.Eval(Container.DataItem, "ShippingCode").ToString())%></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <a href='view.aspx?itemid=<%# DataBinder.Eval(Container.DataItem, "ShippingID").ToString()%>'>
                            <%# Server.HtmlEncode(DataBinder.Eval(Container.DataItem, "Description").ToString())%></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="ProfileName" HtmlEncode="false" HeaderText="Profile Name" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText="Country Code" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# DestinationCountryCode(DataBinder.Eval(Container.DataItem, "DestinationCountryCode"))%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Handling Charge" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "HandlingCharge","{0:c}").ToString()%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="DisplayOrder" HeaderText="Display Order" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText="Enabled" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img id="Img1" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="250px" HeaderText="ACTION" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <div class="LeftFloat" style="width: 30%; text-align: left">
                            <asp:LinkButton ID="btnShippingRules" CommandName="ShippingRules" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ShippingID")%>'
                                Text="ADD RULE &raquo" runat="server" Visible='<%#CheckForCustomShipping(DataBinder.Eval(Container.DataItem,"ShippingTypeId").ToString()) %>'
                                OnClick="BtnShippingRules_Click" />
                        </div>
                        <div class="LeftFloat" style="width: 19%; text-align: left">
                            <asp:LinkButton ID="btnView" CommandName="View" Text="VIEW &raquo" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ShippingID")%>'
                                runat="server"></asp:LinkButton>
                        </div>
                        <div class="LeftFloat" style="width: 17%; text-align: left">
                            <asp:LinkButton ID="btnEdit" CommandName="Edit" Text="EDIT &raquo" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ShippingID")%>'
                                runat="server"></asp:LinkButton>
                        </div>
                        <div class="LeftFloat" style="width: 26%; text-align: left">
                            <asp:LinkButton ID="btnDelete" CommandName="Delete" Text="DELETE &raquo" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ShippingID")%>'
                                runat="server"></asp:LinkButton>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
    </div>
    <div>
        <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
    </div>
</asp:Content>
