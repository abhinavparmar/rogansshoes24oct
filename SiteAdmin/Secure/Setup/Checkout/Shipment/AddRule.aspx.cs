using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Shipping;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Setup.Checkout.Shipment
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_settings_shipping_AddRule class
    /// </summary>
    public partial class AddRule : System.Web.UI.Page
    {
        #region Protected Variables
        private int ItemId;
        private int ShippingId;
        private string _TierText;
        private string AssociateName = string.Empty;
        #endregion

        /// <summary>
        /// Gets or sets the tier text
        /// </summary>
        public string TierText
        {
            get { return _TierText; }
            set { _TierText = value; }
        }

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (Request.Params["sid"] != null)
            {
                this.ShippingId = int.Parse(Request.Params["sid"]);
            }
            else
            {
                this.ShippingId = 0;
            }

            if (Page.IsPostBack == false)
            {
                // If edit func then bind the data fields
                if (this.ItemId > 0)
                {
                    lblTitle.Text = "Edit Shipping Rule for: ";
                }
                else
                {
                    lblTitle.Text = "Add a Shipping Rule for: ";
                }

                txtBaseCost.Text = 0.00.ToString("N");
                txtPerItemCost.Text = 0.00.ToString("N");
                CompareValidator2.Text = "You must enter a valid base cost (ex: " + 123.45.ToString("N") + ")";
                CompareValidator3.Text = "You must enter a valid per-item cost (ex: " + 123.45.ToString("N") + ")";

                // Bind data to the fields on the page
                this.BindData();
            }
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the fields 
        /// </summary>
        protected void BindData()
        {
            ShippingAdmin shipAdmin = new ShippingAdmin();
            ZNode.Libraries.DataAccess.Entities.Shipping shippingOption = shipAdmin.GetShippingOptionById(this.ShippingId);

            // Set Title
            lblTitle.Text = lblTitle.Text + Server.HtmlEncode(shippingOption.Description);

            TList<ShippingRuleType> shippingRuleTypes = shipAdmin.GetShippingRuleTypes();

            // Get shipping rule types
            foreach (ShippingRuleType ruleType in shippingRuleTypes)
            {
                if (shippingOption.ShippingTypeID != 3 || shippingOption.ShippingTypeID != 2)
                {
                    ListItem li = new ListItem(ruleType.Description, ruleType.ShippingRuleTypeID.ToString());
                    lstShippingRuleType.Items.Add(li);
                }
            }

            lstShippingRuleType.SelectedIndex = 0;

            if (this.ItemId > 0)
            {
                ShippingRule shippingRule = shipAdmin.GetShippingRule(this.ItemId);

                lstShippingRuleType.SelectedValue = shippingRule.ShippingRuleTypeID.ToString();

                this.SetShippingTypeOptions(shippingRule.ShippingRuleTypeID);

                if (shippingRule.BaseCost != 0)
                {
                    txtBaseCost.Text = shippingRule.BaseCost.ToString("N2");
                }
                else
                {
                    txtBaseCost.Text = "0";
                }

                if (shippingRule.PerItemCost != 0)
                {
                    txtPerItemCost.Text = shippingRule.PerItemCost.ToString("N2");
                }
                else
                {
                    txtPerItemCost.Text = "0";
                }

                if (shippingRule.LowerLimit != null)
                {
                    txtLowerLimit.Text = shippingRule.LowerLimit.ToString();
                }
                else
                {
                    txtLowerLimit.Text = "0";
                }

                if (shippingRule.UpperLimit != null)
                {
                    txtUpperLimit.Text = shippingRule.UpperLimit.ToString();
                }
                else
                {
                    txtUpperLimit.Text = "0";
                }
            }
            else
            {
                pnlNonFlat.Visible = false;
            }
        }

        #endregion

        #region General Events
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ShippingAdmin shipAdmin = new ShippingAdmin();
            ShippingRule shipRule = new ShippingRule();

            // Set shipping id for this rule
            shipRule.ShippingID = this.ShippingId;

            // If edit mode then retrieve data first
            if (this.ItemId > 0)
            {
                shipRule = shipAdmin.GetShippingRule(this.ItemId);
            }

            // Set values
            shipRule.ShippingRuleTypeID = int.Parse(lstShippingRuleType.SelectedValue);
            shipRule.BaseCost = decimal.Parse(txtBaseCost.Text);

            // "PerItemCost" field is not saving for Quantity and Weight based Rules         
            shipRule.PerItemCost = decimal.Parse(txtPerItemCost.Text);

            if (shipRule.ShippingRuleTypeID != (int)ZNodeShippingRuleType.FixedRatePerItem && shipRule.ShippingRuleTypeID != (int)ZNodeShippingRuleType.FlatRatePerItem)
            {
                shipRule.LowerLimit = decimal.Parse(txtLowerLimit.Text);
                shipRule.UpperLimit = decimal.Parse(txtUpperLimit.Text);
            }
            else
            {
                shipRule.LowerLimit = null;
                shipRule.UpperLimit = null;
            }

            if (this.IsRuleExists(shipRule))
            {
                // Display error message
                lblMsg.Text = "Same shipping rule type cannot be added twice";
                return;
            }

            // Update or Add
            bool retval = false;

            ShippingService shipService = new ShippingService();
            ZNode.Libraries.DataAccess.Entities.Shipping shipOption = new ZNode.Libraries.DataAccess.Entities.Shipping();
            shipOption = shipService.GetByShippingID(this.ShippingId);
            string ShippingName = shipOption.Description;

            if (this.ItemId > 0)
            {
                this.AssociateName = "Edit Shipping Rule " + lstShippingRuleType.SelectedItem.Text + " - " + ShippingName;

                retval = shipAdmin.UpdateShippingRule(shipRule);

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, ShippingName);
            }
            else
            {
                retval = shipAdmin.AddShippingRule(shipRule);

                this.AssociateName = "Added Shipping Rule " + lstShippingRuleType.SelectedItem.Text + " - " + ShippingName;

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, ShippingName);
            }

            if (retval)
            {
                // Redirect to main page
                Response.Redirect("~/SiteAdmin/Secure/Setup/Checkout/Shipment/view.aspx?itemid=" + this.ShippingId.ToString());
            }
            else
            {
                // Display error message
                lblMsg.Text = "An error occurred while updating. Please try again.";
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            // Redirect to main page
            Response.Redirect("~/SiteAdmin/Secure/Setup/Checkout/Shipment/view.aspx?itemid=" + this.ShippingId.ToString());
        }

        /// <summary>
        /// Rule type changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstShippingRuleType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.SetShippingTypeOptions(int.Parse(lstShippingRuleType.SelectedItem.Value));
        }

        #endregion

        #region Helper Functions
        /// <summary>
        /// Set Shipping Type Options
        /// </summary>
        /// <param name="ShippingTypeID">The value of ShippingTypeID</param>
        private void SetShippingTypeOptions(int ShippingTypeID)
        {
            pnlNonFixedRate.Visible = true;

            if (ShippingTypeID == (int)ZNodeShippingRuleType.FixedRatePerItem)
            {
                pnlNonFixedRate.Visible = false;
                pnlNonFlat.Visible = false;
            }

            if (ShippingTypeID == (int)ZNodeShippingRuleType.FlatRatePerItem)
            {
                pnlNonFlat.Visible = false;
            }
            else if (ShippingTypeID == (int)ZNodeShippingRuleType.RateBasedonQUANTITY)
            {
                pnlNonFlat.Visible = true;
                this.TierText = "# Items";
            }
            else if (ShippingTypeID == (int)ZNodeShippingRuleType.RateBasedonWEIGHT)
            {
                pnlNonFlat.Visible = true;
                this.TierText = "lbs";
            }
        }

        /// <summary>
        /// To check the rule type already exists for this shipping
        /// </summary>
        /// <param name="shippingRule">ShippingRule object</param>
        /// <returns>Returns true or false </returns>
        private bool IsRuleExists(ShippingRule shippingRule)
        {
            ShippingRuleService shippingRuleService = new ShippingRuleService();
            ShippingRuleQuery query = new ShippingRuleQuery();

            query.Append(ShippingRuleColumn.ShippingID, shippingRule.ShippingID.ToString());
            query.Append(ShippingRuleColumn.ShippingRuleTypeID, shippingRule.ShippingRuleTypeID.ToString());
            if (this.ItemId > 0)
            {
                query.AppendNotEquals(ShippingRuleColumn.ShippingRuleID, shippingRule.ShippingRuleID.ToString());
            }

            TList<ShippingRule> list = shippingRuleService.Find(query.GetParameters());

            if (shippingRule.ShippingRuleTypeID == 0 && list.Count > 0)
            {
                return true;
            }

            if (list.Count > 0)
            {
                foreach (ShippingRule listitem in list)
                {
                    if ((shippingRule.LowerLimit >= listitem.LowerLimit) && (shippingRule.LowerLimit <= listitem.UpperLimit))
                    {
                        return true;
                    }

                    if ((shippingRule.UpperLimit >= listitem.LowerLimit) && (shippingRule.UpperLimit <= listitem.UpperLimit))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        #endregion
    }
}