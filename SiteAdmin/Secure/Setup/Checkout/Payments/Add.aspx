<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Setup.Checkout.Payments.Add" ValidateRequest="false" Title="Manage Shipping - Add" CodeBehind="Add.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView Size350">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
                <uc2:DemoMode ID="DemoMode1" runat="server" />
            </h1>
        </div>
        <div style="text-align: right">
            <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
        <div class="ClearBoth">
            <asp:Label ID="lblMsg" CssClass="Error" runat="server"></asp:Label>
        </div>
        <div>
            <div>
                <uc1:Spacer ID="Spacer8" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
            <h4 class="SubTitle">General Settings</h4>
            <div class="FieldStyle">
                Select Profile<span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="lstProfile" runat="server">
                </asp:DropDownList>
            </div>
            <div class="FieldStyle">
                Select Payment Type<span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="lstPaymentType" runat="server" OnSelectedIndexChanged="LstPaymentType_SelectedIndexChanged"
                    AutoPostBack="true">
                </asp:DropDownList>
            </div>
            <div class="GoogleCheckout" runat="server" id="googleNote" visible="false">
                <p class="ClearBoth">
                    Note: For the Google checkout payment type, ensure that FedEx or UPS are specified
                    as a Shipping Option.
                </p>
            </div>
            <div class="FieldStyle">
                Display Setting
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkActiveInd" runat="server" Checked="true" Text="Enable this payment option during checkout" />
            </div>
            <div class="FieldStyle">
                Display Order<span class="Asterix">*</span><br />
                <small>Enter a number. Items with a lower number are displayed first on the page.</small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtDisplayOrder" runat="server" MaxLength="9" Columns="5"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Display="Dynamic"
                    ErrorMessage="* Enter a Display Order" ControlToValidate="txtDisplayOrder"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtDisplayOrder"
                    Display="Dynamic" ErrorMessage="Enter a whole number." MaximumValue="999999999"
                    MinimumValue="1" Type="Integer"></asp:RangeValidator>
            </div>
            <asp:Panel ID="pnlCreditCard" runat="server" Visible="false">
                <h4 class="SubTitle">Merchant Gateway Settings</h4>
                <asp:Panel ID="pnlGatewayList" runat="server" Visible="true">
                    <div class="FieldStyle">
                        Select Gateway<span class="Asterix">*</span>
                    </div>
                    <div class="ValueStyle">
                        <asp:DropDownList ID="lstGateway" runat="server" OnSelectedIndexChanged="LstGateway_SelectedIndexChanged"
                            AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlLogin" runat="server" Visible="true">
                    <div class="FieldStyle">
                        <asp:Label ID="lblGatewayUserName" runat="server" Text="Merchant Login" /><span
                            class="Asterix">*</span>
                    </div>

                    <div class="ValueStyle">
                        <asp:TextBox ID="txtGatewayUserName" runat="server" MaxLength="50" Columns="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtGatewayUserName"
                            Display="Dynamic" CssClass="Error" ErrorMessage="* Enter merchant login name" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlPassword" runat="server" Visible="true">
                    <div class="FieldStyle">
                        <asp:Label ID="lblGatewaypassword" runat="server" Text="Merchant account password" /><span class="Asterix">*</span>
                    </div>
                    <div class="ValueStyle" style="font-weight: bold">
                        <asp:TextBox ID="txtGatewayPassword" runat="server" MaxLength="50" Columns="50" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtGatewayPassword"
                            Display="Dynamic" ErrorMessage="* Enter merchant password" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlAuthorizeNet" runat="server" Visible="false">
                    <div class="FieldStyle">
                        <asp:Label ID="lblTransactionKey" runat="server">Transaction Key (Authorize.Net only)</asp:Label>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtTransactionKey" runat="server" MaxLength="70" Columns="50"></asp:TextBox>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlVerisignGateway" runat="server" Visible="false">
                    <div class="FieldStyle">
                        <asp:Label ID="lblPartner" runat="server">Partner</asp:Label><span class="Asterix">*</span><br />
                        <small>Enter your Verisign partner. If you don't know your partner try using PayPal.
                            Note this field is case sensitive.</small>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtPartner" runat="server" MaxLength="70" Columns="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtPartner"
                            Display="Dynamic" ErrorMessage="* Enter partner name" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                    <div class="FieldStyle">
                        <asp:Label ID="lblVendor" runat="server">Vendor</asp:Label><span class="Asterix">*</span><br />
                        <small>Enter your Verisign Vendor ID. If you do not know your Vendor ID try using your
                            Merchant account login.</small>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtVendor" runat="server" MaxLength="70" Columns="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtVendor"
                            Display="Dynamic" ErrorMessage="* Enter vendor name" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlRMA" runat="server" Visible="true">
                    <div class="FieldStyle">
                        Return Merchandise Authorization<br />
                        <small>By default, refunds through RMA are only available via Authorize.net.</small>
                    </div>
                    <div class="ValueStyle">
                        <asp:CheckBox ID="chkRMA" runat="server" Checked="false" Text="Enable RMA" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlTestMode" runat="server">
                    <div class="FieldStyle">
                        Gateway TEST mode
                    </div>
                    <div class="ValueStyle">
                        <asp:CheckBox ID="chkTestMode" runat="server" Checked="false" Text="Enable Test Mode (Your gateway may not support this mode)" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlGatewayOptions" runat="server" Visible="false">
                    <div class="FieldStyle">
                        Credit Card Authorization
                    </div>
                    <div class="ValueStyle">
                        <asp:CheckBox ID="chkPreAuthorize" runat="server" Checked="false" Text="Pre-Authorize transactions without capturing" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlCreditCardOptions" runat="server">
                    <div class="FieldStyle">
                        Accepted Cards
                    </div>
                    <div class="ValueStyle">
                        <asp:CheckBox ID="chkEnableVisa" runat="server" Checked="true" Text="Visa" />&nbsp;&nbsp;<asp:CheckBox
                            ID="chkEnableMasterCard" runat="server" Checked="true" Text="MasterCard" />&nbsp;&nbsp;<asp:CheckBox
                                ID="chkEnableAmex" runat="server" Checked="true" Text="American Express" />&nbsp;&nbsp;<asp:CheckBox
                                    ID="chkEnableDiscover" runat="server" Checked="true" Text="Discover" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnl2COGateway" runat="server" Visible="false">
                    <div class="FieldStyle">
                        <asp:Label ID="Label1" runat="server" Text="Additional Fee" />
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtAdditionalFee" runat="server" MaxLength="11" Columns="50"></asp:TextBox>
                        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtAdditionalFee"
                            Display="Dynamic" ErrorMessage="Enter a whole number." MaximumValue="999999999"
                            MinimumValue="0" Type="Double"></asp:RangeValidator>
                    </div>
                </asp:Panel>
            </asp:Panel>
            <div class="ClearBoth">
            </div>
            <div>
                <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
                <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
            </div>
        </div>
    </div>
</asp:Content>
