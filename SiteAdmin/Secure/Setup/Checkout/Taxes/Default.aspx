<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Setup.Checkout.Taxes.Default" Title="Manage Taxes - List" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="LeftFloat" style="width: 70%; text-align: left">
        <h1>Tax Class
             <div class="tooltip">
                 <a href="javascript:void(0);" class="learn-more"><span>
                     <asp:Localize ID="Localize2" runat="server"></asp:Localize></span></a>
                 <div class="content">
                     <h6>Help</h6>
                     <p>
                         <b>Steps to define tax rules for your products:</b><br />
                         <br />
                         Step 1: Create a new Tax Class<br />
                         Step 2: Associate rules to this Tax Class<br />
                         Step 3: Associate products with this Tax Class<br />
                     </p>
                 </div>
             </div>

        </h1>
    </div>
    <div class="ButtonStyle">
        <zn:LinkButton ID="btnAddTaxClass" runat="server" CausesValidation="False"
            ButtonType="Button" OnClick="BtnAddTaxClass_Click" Text="Add Tax Class"
            ButtonPriority="Primary" />
    </div>
    <div class="ClearBoth">
        <p>
            Set up rules to compute the correct sales taxes during checkout.
        </p>


    </div>
    <div>
        <uc1:Spacer ID="Spacer8" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <div>
        <asp:Label ID="lblErrorMsg" EnableViewState="false" runat="server" CssClass="Error"></asp:Label>
    </div>
    <div>
        <uc1:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <h4 class="GridTitle">Tax Class List</h4>
    <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
        CellPadding="4" ForeColor="#333333" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
        CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" PageSize="25"
        OnRowDeleting="UxGrid_RowDeleting" AllowSorting="True" EmptyDataText="No tax class exist in the database.">
        <Columns>
            <asp:BoundField DataField="TaxClassID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
            <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-HorizontalAlign="Left" />
            <asp:BoundField DataField="DisplayOrder" HeaderText="Display Order" HeaderStyle-HorizontalAlign="Left" />
            <asp:TemplateField HeaderText="Enabled" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <img alt="" id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                        runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ACTION" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <div class="LeftFloat" style="width: 30%; text-align: left">
                        <asp:LinkButton ID="btnAddTaxRules" CommandName="TaxRules" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TaxClassID")%>'
                            Text="ADD TAX RULE &raquo" runat="server" CssClass="actionlink" OnClick="BtnAddTaxRules_Click" />
                    </div>
                    <div class="LeftFloat" style="width: 20%; text-align: left">
                        <asp:LinkButton ID="btnView" CommandName="View" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TaxClassID")%>'
                            Text="VIEW &raquo" runat="server"></asp:LinkButton>
                    </div>
                    <div class="LeftFloat" style="width: 20%; text-align: left">
                        <asp:LinkButton ID="btnEdit" CommandName="Edit" Text="EDIT &raquo" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TaxClassID")%>'
                            runat="server"></asp:LinkButton>
                    </div>
                    <div class="LeftFloat" style="width: 25%; text-align: left">
                        <asp:LinkButton ID="btnDelete" CommandName="Delete" Text="DELETE &raquo" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TaxClassID")%>'
                            runat="server"></asp:LinkButton>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <FooterStyle CssClass="FooterStyle" />
        <RowStyle CssClass="RowStyle" />
        <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
        <HeaderStyle CssClass="HeaderStyle" />
        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
        <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
    </asp:GridView>
    <div>
        <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
    </div>
</asp:Content>
