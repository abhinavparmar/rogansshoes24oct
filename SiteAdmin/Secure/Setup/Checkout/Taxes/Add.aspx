<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Setup.Checkout.Taxes.Add" ValidateRequest="false" Title="Manage Taxes - Add" CodeBehind="Add.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
                <uc2:DemoMode ID="DemoMode1" runat="server" />
            </h1>
        </div>
        <div style="text-align: right">
            <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
        <div class="ClearBoth">
            <asp:Label ID="Label1" CssClass="Error" runat="server"></asp:Label>
        </div>
        <div class="Error">
            <asp:Label ID="lblMsg" runat="server"></asp:Label>
        </div>
        <div class="FieldStyle">
            Tax Class Name<span class="Asterix">*</span>
        </div>

        <div class="ValueStyle">
            <asp:TextBox ID="txtTaxClassName" runat="server" MaxLength="15"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtTaxClassName"
                Display="Dynamic" CssClass="Error" ErrorMessage="* Enter Tax class name." SetFocusOnError="True"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            Display Order<span class="Asterix">*</span><br />
            <small>Enter a whole number. This determines the order in which this tax rule will be processed.</small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtDisplayOrder" runat="server" MaxLength="9" Columns="5"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Display="Dynamic"
                ErrorMessage="* Enter Display Order" CssClass="Error" ControlToValidate="txtDisplayOrder"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator2" CssClass="Error" runat="server" ControlToValidate="txtDisplayOrder"
                Display="Dynamic" ErrorMessage="Enter Whole number." MaximumValue="999999999"
                MinimumValue="1" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
            Enable
        </div>
        <div class="ValueStyle">
            <asp:CheckBox Checked="true" ID="chkActiveInd" Text="Enable this tax class in your store"
                runat="server" />
        </div>
        <div class="ClearBoth">
            <br />
        </div>
        <div>
            <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>
