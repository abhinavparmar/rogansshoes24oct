﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace SiteAdmin.Secure.Setup.Checkout.Taxes
{
    public partial class Delete : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId;
        private string _TaxClassName = string.Empty;
        private string CancelLink = "~/SiteAdmin/Secure/Setup/Checkout/Taxes/default.aspx";
        #endregion

        /// <summary>
        /// Gets or sets the Profile Name
        /// </summary>
        public string TaxClassName
        {
            get
            {
                return this._TaxClassName;
            }

            set
            {
                this._TaxClassName = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring   
            if (Request.Params["Itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["Itemid"].ToString());
                this.BindData();
            }
            else
            {
                this.ItemId = 0;
            }
        }

        /// <summary>
        /// Bind the data for a Product id
        /// </summary>
        public void BindData()
        {
            TaxRuleAdmin taxRuleAdmin = new TaxRuleAdmin();
            TaxClass taxRuleEntity = taxRuleAdmin.GetByTaxClassID(this.ItemId);

            if (taxRuleEntity != null)
            {
                this.TaxClassName = taxRuleEntity.Name;
            }
        }
        #region Events
        /// <summary>
        /// Delete Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                bool status = false;
                TaxRuleAdmin taxRuleAdmin = new TaxRuleAdmin();
                status = taxRuleAdmin.DeleteTaxClass(int.Parse(this.ItemId.ToString()));

                string AssociateName = "Deleted '" + TaxClassName + "' Tax Class";

                if (!status)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, TaxClassName);

                    lblErrorMessage.Text = "Delete action could not be completed because the Tax class is in use.";
                }
                else
                {
                    Response.Redirect(this.CancelLink);
                }
            }
            catch
            {
                lblErrorMessage.Text = "Delete action could not be completed because the Tax class is in use.";
            }
           
        }

        /// <summary>
        /// Cancel Button click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.CancelLink);
        }
        #endregion
    }
}