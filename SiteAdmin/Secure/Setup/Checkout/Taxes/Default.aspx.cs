using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Setup.Checkout.Taxes
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_settings_taxes_Default class
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Private Variables
        private string AddRuleLink = "~/SiteAdmin/Secure/Setup/Checkout/Taxes/AddRule.aspx";
        private string ViewLink = "~/SiteAdmin/Secure/Setup/Checkout/Taxes/View.aspx";
        private string EditLink = "~/SiteAdmin/Secure/Setup/Checkout/Taxes/Add.aspx";
        private string DeleteLink = "~/SiteAdmin/Secure/Setup/Checkout/Taxes/Delete.aspx";
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindGridData();
            }
        }

        #endregion

        #region Grid Events
        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGridData();
        }
        
        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // property to an Integer.
                string Id = e.CommandArgument.ToString();

                if (e.CommandName == "View")
                {
                    this.ViewLink = this.ViewLink + "?taxid=" + Id;
                    Response.Redirect(this.ViewLink);
                }
                else if (e.CommandName == "Edit")
                {
                    this.EditLink = this.EditLink + "?itemId=" + Id;
                    Response.Redirect(this.EditLink);
                }
                else if (e.CommandName == "Delete")
                {
                    this.DeleteLink = this.DeleteLink + "?Itemid=" + Id;
                    Response.Redirect(this.DeleteLink);
                }
            }
        }
        
        /// <summary>
        /// Event triggered when an item is deleted from the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindGridData();
        }
        #endregion

        #region Other Events
        /// <summary>
        /// Add Tax Class button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddTaxClass_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.EditLink);
        }
        
        /// <summary>
        /// Add Tax Rules button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddTaxRules_Click(object sender, EventArgs e)
        {
            string taxId = ((LinkButton)sender).CommandArgument;
            Response.Redirect(this.AddRuleLink + "?taxid=" + taxId);
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind data to the grid
        /// </summary>
        private void BindGridData()
        {
            TaxRuleAdmin taxRuleAdmin = new TaxRuleAdmin();
            uxGrid.DataSource = UserStoreAccess.CheckStoreAccess(taxRuleAdmin.GetAllTaxClass());
            uxGrid.DataBind();
        }

        #endregion
    }
}