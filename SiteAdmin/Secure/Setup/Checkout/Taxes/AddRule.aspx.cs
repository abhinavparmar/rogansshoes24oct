using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Zeon.Libraries.Avatax;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Setup.Checkout.Taxes
{
    /// <summary>
    /// Represents the Site Admin Admin_Secure_settings_tax_AddRule class.
    /// </summary>
    public partial class AddRule : System.Web.UI.Page
    {
        #region Private Variables
        private int itemId;
        private int taxId = 0;
        private string associateName = string.Empty;
        #endregion

        #region Constant Variables

        private const string TaxClassName = "ZeonTaxAvatax";

        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.itemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.itemId = 0;
            }

            // Get tax classid from querystring        
            if (Request.Params["taxId"] != null)
            {
                this.taxId = int.Parse(Request.Params["taxId"]);
            }
            else
            {
                this.taxId = 0;
            }

            if (Page.IsPostBack == false)
            {
                this.BindTaxRuleTypes();

                this.BindPortal();

                // Bind Country
                this.BindCountry();

                // If edit func then bind the data fields
                if (this.itemId > 0)
                {
                    lblTitle.Text = "Edit Tax Rule";
                    this.BindEditData();
                }
                else
                {
                    lblTitle.Text = "Add a Tax Rule";
                }
            }
            //Zeon Custom Code : Start
            SetVisibilityOfTaxRuleType();
            SetAvataxCredentials();
            //Zeon Custom Code : End
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            TaxRuleAdmin taxRuleAdmin = new TaxRuleAdmin();
            TaxRule taxRule = new TaxRule();
            //Zeon Custom Code : Start
            int porID = 0;
            int.TryParse(ddlPortal.SelectedValue, out porID);
            if (porID > 0)
            {
                taxRule.PortalID = porID;
            }
            //Zeon Custom Code : End
            // If edit mode then retrieve data first
            if (this.itemId > 0)
            {
                taxRule = taxRuleAdmin.GetTaxRule(this.itemId);
            }

            // TaxClassID
            taxRule.TaxClassID = this.taxId;

            // Destination Country
            if (lstCountries.SelectedValue.Equals("*"))
            {
                taxRule.DestinationCountryCode = null;
            }
            else
            {
                taxRule.DestinationCountryCode = lstCountries.SelectedValue;
            }

            // Destination State
            if (lstStateOption.SelectedValue != "0")
            {
                taxRule.DestinationStateCode = lstStateOption.SelectedValue;
            }
            else
            {
                taxRule.DestinationStateCode = null;
            }

            // CountyFIPS
            if (lstCounty.SelectedValue != "0")
            {
                taxRule.CountyFIPS = lstCounty.SelectedValue;
            }
            else
            {
                taxRule.CountyFIPS = null;
            }

            //Zeon Custom Code: Put all the tax values as null if Avatax is set
            if (SetVisibilityOfTaxRuleType())
            {
                taxRule.SalesTax = null;
                taxRule.VAT = null;
                taxRule.GST = null;
                taxRule.PST = null;
                taxRule.HST = null;
            }
            else
            {
                // Tax Rates
                taxRule.SalesTax = Convert.ToDecimal(txtSalesTax.Text);
                taxRule.VAT = Convert.ToDecimal(txtVAT.Text);
                taxRule.GST = Convert.ToDecimal(txtGST.Text);
                taxRule.PST = Convert.ToDecimal(txtPST.Text);
                taxRule.HST = Convert.ToDecimal(txtHST.Text);
            }

            // Tax Preferences        
            taxRule.InclusiveInd = chkTaxInclusiveInd.Checked;
            taxRule.Precedence = int.Parse(txtPrecedence.Text);

            // Zeon Custom Code : Store the security credientials to Custom1 field for Avatax Webservice in Encrypted format.
            SetAvalaraTaxCredentials(taxRule);

            if (ddlRuleTypes.SelectedIndex != -1)
            {
                taxRule.TaxRuleTypeID = int.Parse(ddlRuleTypes.SelectedValue);
            }

            bool isSuccess = false;
            TaxClass taxClass = new TaxClass();
            taxClass = taxRuleAdmin.GetByTaxClassID(this.taxId);

            if (this.itemId > 0)
            {
                isSuccess = taxRuleAdmin.UpdateTaxRule(taxRule);
                this.associateName = "Edit Tax Rule - " + taxClass.Name;

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.associateName, taxClass.Name);
            }
            else
            {
                isSuccess = taxRuleAdmin.AddTaxRule(taxRule);
                this.associateName = "Creation of Tax Rule - " + taxClass.Name;

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.associateName, taxClass.Name);
            }

            if (isSuccess)
            {
                System.Web.HttpContext.Current.Cache.Remove("InclusiveTaxRules");

                // Redirect to view page
                Response.Redirect("~/SiteAdmin/Secure/Setup/Checkout/Taxes/view.aspx?taxId=" + this.taxId);
            }
            else
            {
                // Display error message
                lblMsg.Text = "An error occurred while updating. Please try again.";
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Setup/Checkout/Taxes/view.aspx?taxId=" + this.taxId);
        }

        /// <summary>
        /// Country option changed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstCountries_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstCountries.SelectedValue.Equals("*"))
            {
                lstStateOption.Items.Clear();
                ListItem li = new ListItem("Apply to ALL States", "0");
                lstStateOption.Items.Insert(0, li);
            }
            else
            {
                this.BindStates();
            }
        }

        /// <summary>
        /// state option changed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstStateOption_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindCounty();
        }

        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the fields in edit mode
        /// </summary>
        private void BindEditData()
        {
            TaxRuleAdmin taxRuleAdmin = new TaxRuleAdmin();
            TaxRule taxRule = new TaxRule();

            if (this.itemId > 0)
            {
                taxRule = taxRuleAdmin.GetTaxRule(this.itemId);

                // Destination CountryCode
                if (taxRule.DestinationCountryCode != null)
                {
                    ListItem listItem = lstCountries.Items.FindByValue(taxRule.DestinationCountryCode);
                    if (listItem != null)
                    {
                        lstCountries.SelectedIndex = lstCountries.Items.IndexOf(listItem);
                    }

                    if (lstCountries.SelectedIndex != -1)
                    {
                        this.BindStates();
                    }
                }

                // Destination StateCode
                if (taxRule.DestinationStateCode != null)
                {
                    ListItem listItem = lstStateOption.Items.FindByValue(taxRule.DestinationStateCode);
                    if (listItem != null)
                    {
                        lstStateOption.SelectedIndex = lstStateOption.Items.IndexOf(listItem);
                    }

                    if (lstStateOption.SelectedIndex != -1)
                    {
                        this.BindCounty();
                    }
                }

                // CountyFIPS
                if (taxRule.CountyFIPS != null)
                {
                    ListItem listItem = lstCounty.Items.FindByValue(taxRule.CountyFIPS);
                    if (listItem != null)
                    {
                        lstCounty.SelectedIndex = lstCounty.Items.IndexOf(listItem);
                    }
                }

                // Tax Rates
                txtSalesTax.Text = taxRule.SalesTax.ToString();
                txtVAT.Text = taxRule.VAT.ToString();
                txtGST.Text = taxRule.GST.ToString();
                txtPST.Text = taxRule.PST.ToString();
                txtHST.Text = taxRule.HST.ToString();

                // Tax Preferences
                txtPrecedence.Text = taxRule.Precedence.ToString();

                chkTaxInclusiveInd.Checked = taxRule.InclusiveInd;

                ddlRuleTypes.SelectedValue = taxRule.TaxRuleTypeID.GetValueOrDefault().ToString();

                // Retrieve encrypted credientials and show in the edit screen.
                BindTaxCredientials(taxRule);
            }
        }

        /// <summary>
        /// Bind the tax rule types.
        /// </summary>
        private void BindTaxRuleTypes()
        {
            TaxRuleAdmin taxRuleAdmin = new TaxRuleAdmin();

            TList<TaxRuleType> taxRuleTypeList = taxRuleAdmin.GetTaxRuleTypes();
            ddlRuleTypes.DataSource = taxRuleTypeList.FindAll(TaxRuleTypeColumn.ActiveInd, true);
            ddlRuleTypes.DataTextField = "Name";
            ddlRuleTypes.DataValueField = "TaxRuleTypeId";
            ddlRuleTypes.DataBind();
        }

        /// <summary>
        /// Bind the Countries
        /// </summary>
        private void BindCountry()
        {
            ShippingAdmin shipAdmin = new ShippingAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Country> countryList = shipAdmin.GetDestinationCountries();

            // Country
            lstCountries.DataSource = countryList;
            lstCountries.DataTextField = "Name";
            lstCountries.DataValueField = "Code";
            lstCountries.DataBind();
        }

        /// <summary>
        /// Bind the States
        /// </summary>
        private void BindStates()
        {
            StateService stateService = new StateService();
            StateQuery filters = new StateQuery();

            // Parameters
            filters.Append(StateColumn.CountryCode, lstCountries.SelectedItem.Value);

            // Get States list
            TList<State> StatesList = stateService.Find(filters.GetParameters());

            lstStateOption.DataSource = StatesList;
            lstStateOption.DataTextField = "Name";
            lstStateOption.DataValueField = "Code";
            lstStateOption.DataBind();
            ListItem li = new ListItem("Apply to ALL States", "0");
            lstStateOption.Items.Insert(0, li);
        }

        /// <summary>
        /// Bind the County from ZnodeZipcode table
        /// </summary>
        private void BindCounty()
        {
            TaxRuleAdmin taxRuleAdmin = new TaxRuleAdmin();
            DataSet countryListDataSet = taxRuleAdmin.GetCountyCodeByStateAbbr(lstStateOption.SelectedValue);

            lstCounty.DataSource = countryListDataSet;
            lstCounty.DataTextField = "CountyName";
            lstCounty.DataValueField = "CountyFIPS";
            lstCounty.DataBind();
            ListItem li = new ListItem("Apply to ALL Counties", "0");
            lstCounty.Items.Insert(0, li);
        }

        #endregion

        #region Zeon Custom Code

        /// <summary>
        /// Bind Portals
        /// </summary>
        private void BindPortal()
        {
            PortalAdmin portalAdmin = new PortalAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Portal> portals = portalAdmin.GetAllPortals();

            // Portal Drop Down List
            ddlPortal.DataSource = portals;
            ddlPortal.DataTextField = "StoreName";
            ddlPortal.DataValueField = "PortalID";
            ddlPortal.DataBind();
           
        }

        /// <summary>
        /// Set Avalara Tax Credentials in Custom1 column for 
        /// </summary>
        /// <param name="taxRule"></param>
        private void SetAvalaraTaxCredentials(TaxRule taxRule)
        {
            if (!string.IsNullOrEmpty(lblAccount.Text.Trim()))
            {
                string encryptedAccount = lblAccount.Text.Trim();
                string encryptedLicense = lblLicence.Text.Trim();

                // Encrypt the the key for the first time.
                if (lblAccount.Text.Trim().Length < 100)  // check to see if its already encrypted.
                {
                    encryptedAccount = lblAccount.Text;
                }
                if (lblLicence.Text.Trim().Length < 100)
                {
                    encryptedLicense = lblLicence.Text;
                }

                taxRule.Custom1 = string.Format("AccountID={0}|LicenseKey={1}|Company={2}|url={3}", encryptedAccount, encryptedLicense, lblCompany.Text.Trim(), lblURL.Text);
            }
        }

        /// <summary>
        /// Bind Avalara Tax Credentials
        /// </summary>
        /// <param name="taxRule"></param>
        private void BindTaxCredientials(TaxRule taxRule)
        {
            if (!string.IsNullOrEmpty(taxRule.Custom1))
            {
                string[] credientails = taxRule.Custom1.Split('|');
                if (credientails.Length > 0 && credientails.Length >= 4)
                {
                    lblAccount.Text = credientails[0].Split('=')[1];
                    lblLicence.Text = credientails[1].Split('=')[1];
                    lblCompany.Text = credientails[2].Split('=')[1];
                    lblURL.Text = credientails[3].Split('=')[1];
                }
            }
        }

        /// <summary>
        /// Set Visibility Of Tax Rule Type
        /// </summary>
        private bool SetVisibilityOfTaxRuleType()
        {
            bool isAvataxVisible = false;
            TaxRuleAdmin taxAdmin = new TaxRuleAdmin();
            TaxRuleType taxRuleType = taxAdmin.GetTaxRuleTypes().Find("TaxRuleTypeID", Convert.ToInt32(ddlRuleTypes.SelectedValue));
            if (taxRuleType != null)
            {
                pnlAvalaraCredientials.Visible = (taxRuleType.ClassName.Equals(TaxClassName));
                pnlTaxRate.Visible = !(taxRuleType.ClassName.Equals(TaxClassName));
                isAvataxVisible = taxRuleType.ClassName.Equals(TaxClassName);
            }
            return isAvataxVisible;
        }

        /// <summary>
        /// Set the Avatax Credentials from ZNode table
        /// </summary>
        private void SetAvataxCredentials()
        {
            AvataxHelper helper = new AvataxHelper();
            DataSet dsAvataxCredentials = helper.GetAvataxDetails(ZNodeConfigManager.DomainConfig.PortalID);
            if (dsAvataxCredentials != null && dsAvataxCredentials.Tables != null && dsAvataxCredentials.Tables[0].Rows.Count > 0)
            {
                lblAccount.Text = dsAvataxCredentials.Tables[0].Rows[0]["AccountID"].ToString();
                lblCompany.Text = dsAvataxCredentials.Tables[0].Rows[0]["CompanyName"].ToString();
                lblLicence.Text = dsAvataxCredentials.Tables[0].Rows[0]["LicenceKey"].ToString();
                lblURL.Text = dsAvataxCredentials.Tables[0].Rows[0]["Url"].ToString();
            }
        }


        #endregion
    }
}