<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Setup.Checkout.Taxes.View" ValidateRequest="false" Title="Manage Taxes - View" Codebehind="View.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                Tax Class Details -
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
        </div>
        <div align="right" class="ImageButtons">
            <asp:Button ID="btnEdit" CausesValidation="False" Text="Edit Tax Class" runat="server"
                OnClick="BtnEdit_Click" />
            &nbsp;&nbsp;<asp:Button ID="btnBack" CausesValidation="False" Text="<< Back" CssClass="Size100"
                runat="server" OnClick="BtnBack_Click" />
        </div>
        <div class="ClearBoth">
            <br />
        </div>
        <h4 class="SubTitle">
            General Information</h4>
        <div class="ViewForm200">
            <div class="FieldStyle">
                Name</div>
            <div class="ValueStyle">
                <asp:Label ID="lblProfileName" runat="server"></asp:Label></div>
            <div class="FieldStyle">
                Display Order</div>
            <div class="ValueStyle">
                <asp:Label ID="lblDisplayOrder" runat="server"></asp:Label></div>
            <div class="FieldStyle">
                Enabled</div>
            <div class="ValueStyle">
                <img id="imgActive" runat="server" alt="" /></div>
        </div>
        <div class="ClearBoth">
        </div>
        <h4 class="SubTitle">
            Tax Rule List</h4>
        <p>
            Tax Rules are applied in the order of precedence. <b>For Example</b>, to implement
            a tax rule
            <br />
            to apply 5% tax to residents of Alaska and 6.5% for all other US States do the following:<br />
            <br />
            <img id="Img3" src="~/SiteAdmin/Themes/images/Right_arrow.gif" runat="server" />&nbsp;&nbsp;Add
            a rule with Country=US, State=AK, Tax=5%, Precedence=1
            <br />
            <img id="Img4" src="~/SiteAdmin/Themes/images/Right_arrow.gif" runat="server" />&nbsp;&nbsp;Add
            a second rule with Country=US, State=ALL States, Tax=6.5%, Precedence=2
        </p>
        <div>
        <asp:Label ID="lblMsg" CssClass="Error" runat="server"></asp:Label>
        </div>
        <div class="ButtonStyle">
              <zn:LinkButton ID="Button1" runat="server" 
                ButtonType="Button" OnClick="BtnAdd_Click" CausesValidation="False" Text="Add Tax Rule"
                ButtonPriority="Primary"/>
        </div>
        <br />
        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
            CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" PageSize="25"
            OnRowDeleting="UxGrid_RowDeleting" AllowSorting="True" EmptyDataText="No tax rules exist in the database.">
            <Columns>
                <asp:BoundField DataField="TaxRuleID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText="Sales Tax" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "SalesTax")%>%
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Country Code" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# GetDefaultRegionCode(DataBinder.Eval(Container.DataItem, "DestinationCountryCode"))%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="State Code" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# GetDefaultRegionCode(DataBinder.Eval(Container.DataItem, "DestinationStateCode"))%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="County" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# GetCountyName(DataBinder.Eval(Container.DataItem, "CountyFIPS"))%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ACTIONS" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <div class="LeftFloat" style="width: 40%; text-align: left">
                            <asp:LinkButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("TaxRuleID") %>'
                                CommandName="Edit" Text="EDIT &raquo" CssClass="LinkButton" />
                        </div>
                        <div class="LeftFloat" style="width: 50%; text-align: left">
                            <asp:LinkButton ID="btnDelete" runat="server" CommandArgument='<%# Eval("TaxRuleID") %>'
                                CommandName="Delete" Text="DELETE &raquo " CssClass="LinkButton" />
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
        <div>
            <uc1:Spacer ID="Spacer2" spacerheight="10" spacerwidth="10" runat="server"></uc1:Spacer>
        </div>
    </div>
</asp:Content>
