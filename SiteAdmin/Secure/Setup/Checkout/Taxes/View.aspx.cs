using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Setup.Checkout.Taxes
{
    /// <summary>
    /// Represents the SiteAdmin Admin_Secure_settings_tax_View class
    /// </summary>
    public partial class View : System.Web.UI.Page
    {
        #region Private Member Variables
        private int ItemId;
        private int TaxId;
        private string CatalogImagePath = string.Empty;
        private string DefaultLink = "~/SiteAdmin/Secure/Setup/Checkout/Taxes/default.aspx";
        private string EditLink = "~/SiteAdmin/Secure/Setup/Checkout/Taxes/add.aspx";
        private string AddRuleLink = "~/SiteAdmin/Secure/Setup/Checkout/Taxes/addRule.aspx";
        private string EditRuleLink = "~/SiteAdmin/Secure/Setup/Checkout/Taxes/addRule.aspx";
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            // Get TaxId from querystring
            if (Request.Params["taxId"] != null)
            {
                this.TaxId = int.Parse(Request.Params["taxId"]);
            }
            else
            {
                this.TaxId = 0;
            }

            if (!Page.IsPostBack)
            {
                this.BindData();
                lblTitle.Text = lblProfileName.Text;
            }
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindData();
        }

        /// <summary>
        /// Event triggered when an item is deleted from the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindData();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // property to an Integer.
                string Id = e.CommandArgument.ToString();

                if (e.CommandName == "Edit")
                {
                    this.EditRuleLink = this.EditRuleLink + "?itemid=" + Id + "&taxId=" + this.TaxId;
                    Response.Redirect(this.EditRuleLink);
                }
                else if (e.CommandName == "Delete")
                {
                    TaxRuleAdmin taxAdmin = new TaxRuleAdmin();
                    TaxRule taxRule = taxAdmin.GetTaxRule(int.Parse(Id));

                    if (taxRule != null)
                    {
                        TaxClass taxClass = taxAdmin.GetByTaxClassID(taxRule.TaxClassID.Value);
                        ZNode.Libraries.DataAccess.Service.TaxClassService service = new ZNode.Libraries.DataAccess.Service.TaxClassService();
                        service.DeepLoad(taxClass);
                        if ((taxClass.AddOnValueCollection.Count > 0 ||
                                taxClass.ProductCollection.Count > 0) &&
                                taxClass.TaxRuleCollection.Count == 1)
                        {
                            lblMsg.Text = "You cannot delete the last TaxRule when this tax class is associated with products/Addons.";
                            return;
                        }

                        taxRule.TaxRuleID = int.Parse(Id);

                        // Get tax class name and tax rule name
                        taxClass = taxAdmin.GetByTaxClassID(this.TaxId);
                        TaxRuleService _taxRuleService = new TaxRuleService();
                        TaxRule taxRuleName = new TaxRule();
                        taxRuleName = _taxRuleService.GetByTaxRuleID(int.Parse(Id));
                        _taxRuleService.DeepLoad(taxRuleName);

                        string TaxRuleName = taxRuleName.TaxRuleTypeIDSource.Name;
                        string TaxClassName = taxClass.Name;
                        string AssociateName = "Delete Tax Rule " + TaxRuleName + " for Tax Class " + TaxClassName;
                        bool status = taxAdmin.DeleteTaxRule(taxRule);

                        if (status)
                        {
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, TaxClassName);
                            System.Web.HttpContext.Current.Cache.Remove("InclusiveTaxRules");
                        }
                    }
                }
            }
        }

        #endregion

        #region Other Events
        /// <summary>
        /// Add Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddRuleLink + "?taxid=" + this.TaxId);
        }

        /// <summary>
        /// Edit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.EditLink + "?itemId=" + this.TaxId);
        }

        /// <summary>
        /// Back button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.DefaultLink);
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Get a formatted region code for grid display
        /// </summary>
        /// <param name="regionCode">The value of Region Code</param>
        /// <returns>Returns the formatted region code</returns>
        protected string GetDefaultRegionCode(object regionCode)
        {
            if (regionCode == null)
            {
                return "ALL";
            }
            else if (regionCode.ToString().Length == 0)
            {
                return "ALL";
            }
            else
            {
                return regionCode.ToString();
            }
        }
        
        /// <summary>
        /// Get a County Name for grid display
        /// </summary>
        /// <param name="regionCode">The value of regionCode</param>
        /// <returns>Returns the County Name</returns>
        protected string GetCountyName(object regionCode)
        {
            string countyName = string.Empty;

            if (regionCode == null)
            {
                countyName = "ALL";
            }
            else if (regionCode.ToString().Length == 0)
            {
                countyName = "ALL";
            }
            else
            {
                TaxRuleAdmin taxruleAdmin = new TaxRuleAdmin();
                countyName = taxruleAdmin.GetNameByCountyFIPS(regionCode.ToString());
            }

            return countyName;
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindData()
        {
            if (this.TaxId > 0)
            {
                TaxRuleAdmin taxAdmin = new TaxRuleAdmin();
                TaxClass taxClass = taxAdmin.DeepLoadByTaxClassId(this.TaxId);
                taxClass.TaxRuleCollection.Sort("Precedence");
                lblProfileName.Text = Server.HtmlEncode(taxClass.Name);
                if (taxClass.DisplayOrder.HasValue)
                {
                    lblDisplayOrder.Text = taxClass.DisplayOrder.ToString();
                }

                imgActive.Src = ZNode.Libraries.Admin.Helper.GetCheckMark((bool)taxClass.ActiveInd);
                
                //Zeon Custom Code: Start
                bool hideSlaesTaxColumn = true;

                foreach (TaxRule item in taxClass.TaxRuleCollection)
                {
                    if (item.SalesTax == null || Convert.ToInt32(item.SalesTax).Equals(0))
                    {
                        hideSlaesTaxColumn = false;
                    }
                }

                uxGrid.Columns[1].Visible = hideSlaesTaxColumn;
                //Zeon Custom Code: End
                uxGrid.DataSource = taxClass.TaxRuleCollection;
                uxGrid.DataBind();
            }
        }
        #endregion
    }
}