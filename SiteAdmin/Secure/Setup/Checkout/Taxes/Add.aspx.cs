using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Setup.Checkout.Taxes
{
    /// <summary>
    /// Represents the Site Admin Admin_Secure_settings_taxes_Add class.
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region Private Variable
        private int itemId;
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get tax classid from querystring        
            if (Request.Params["itemId"] != null)
            {
                this.itemId = int.Parse(Request.Params["itemId"]);
            }
            else
            {
                this.itemId = 0;
            }

            if (Page.IsPostBack == false)
            {
                // If edit func then bind the data fields
                if (this.itemId > 0)
                {
                    lblTitle.Text = "Edit Tax Class";
                    this.BindEditData();
                }
                else
                {
                    lblTitle.Text = "Add a Tax Class";
                }
            }
        }
        #endregion       

        #region General Events
        /// <summary>
        /// Submit button click event to store in Tax Class
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            TaxRuleAdmin taxRuleAdmin = new TaxRuleAdmin();
            TaxClass taxClass = new TaxClass();

            if (this.itemId > 0)
            {
                taxClass = taxRuleAdmin.GetByTaxClassID(this.itemId);
            }

            taxClass.Name = txtTaxClassName.Text;
            taxClass.DisplayOrder = int.Parse(txtDisplayOrder.Text);
            taxClass.ActiveInd = chkActiveInd.Checked;

            bool isSuccess = false;

            if (this.itemId > 0)
            {
                isSuccess = taxRuleAdmin.UpdateTaxClass(taxClass);

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit of Tax Class - " + txtTaxClassName.Text, txtTaxClassName.Text);                
      
            }
            else
            {
                isSuccess = taxRuleAdmin.InsertTaxClass(taxClass);

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Creation of Tax Class - " + txtTaxClassName.Text, txtTaxClassName.Text);
            }

            if (isSuccess)
            {
                if (this.itemId > 0)
                {
                    // Redirect to view page  
                    Response.Redirect("~/SiteAdmin/Secure/Setup/Checkout/Taxes/view.aspx?taxId=" + taxClass.TaxClassID);
                }
                else
                {
                    // Redirect to Add tax class page  
                    Response.Redirect("~/SiteAdmin/Secure/Setup/Checkout/Taxes/addRule.aspx?taxId=" + taxClass.TaxClassID);
                }
            }
            else
            {
                // Display error message
                lblMsg.Text = "An error occurred while updating. Please try again.";
            }
        }

        /// <summary>
        /// Cancel Button click event to cancel the action
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (this.itemId > 0)
            {
                Response.Redirect("~/SiteAdmin/Secure/Setup/Checkout/Taxes/view.aspx?taxId=" + this.itemId);
            }
            else
            {
                Response.Redirect("~/SiteAdmin/Secure/Setup/Checkout/Taxes/default.aspx");
            }
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data in the edit mode
        /// </summary>        
        private void BindEditData()
        {
            TaxRuleAdmin taxRuleAdmin = new TaxRuleAdmin();
            TaxClass taxClass = new TaxClass();

            if (this.itemId > 0)
            {
                taxClass = taxRuleAdmin.GetByTaxClassID(this.itemId);
                lblTitle.Text = lblTitle.Text + " - " + Server.HtmlEncode(taxClass.Name);
                txtTaxClassName.Text = Server.HtmlDecode(taxClass.Name);
                txtDisplayOrder.Text = taxClass.DisplayOrder.ToString();
                chkActiveInd.Checked = taxClass.ActiveInd;
            }
        }

        #endregion
    }
}