﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" CodeBehind="EditAvataxSettings.aspx.cs" Inherits="WebApp.SiteAdmin.Secure.Setup.Storefront.Stores.EditAvataxSettings" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>

<asp:Content id="avataxContent" ContentPlaceHolderID="uxMainContent" runat="server">
    <div class="FormView">
        <div class="LeftFloat" style="width: 65%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
                <ZNode:DemoMode ID="DemoMode1" runat="server" />
            </h1>
        </div>
        <div style="text-align: right; padding-right: 10px;">
            <asp:ImageButton ID="btnTestConnectionTop" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_testconnection.gif"
                onmouseover="this.src='../../../../Themes/Images/buttons/button_testconnection_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_testconnection.gif';"
                runat="server" AlternateText="TestConnection" OnClick="btnTestConnection_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" Enabled="false" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Submit" OnClick="BtnCancel_Click" />
        </div>
        <div align="left">
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label></div>
        <div class="ClearBoth">
        </div>
        <h4 class="SubTitle">
            Avatax Settings</h4>
        <div class="FieldStyle">
            Account ID</div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtAccountID" runat="server" Width="152px" MaxLength="100"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvAccount" runat="server" ErrorMessage="Please enter Account ID."
                ControlToValidate="txtAccountID" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            License</div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtLicense" runat="server" Width="152px" MaxLength="100"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvLicence" runat="server" ErrorMessage="Please enter License."
                ControlToValidate="txtLicense" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            Company</div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtCompany" runat="server" Width="152px" MaxLength="50"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvCompany" runat="server" ErrorMessage="Please enter Company."
                ControlToValidate="txtCompany" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            Webservice URL</div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtWebserviceURL" runat="server" Width="152px" MaxLength="50"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvUrl" runat="server" ErrorMessage="Please enter Webservice URL."
                ControlToValidate="txtWebserviceURL" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </div>
        <asp:HiddenField ID="hdnAvataxID" runat="server" />
        <div class="ClearBoth"><br /></div>
        <div>
            <asp:ImageButton ID="btnTestConnectionBottom" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_testconnection.gif"
                onmouseover="this.src='../../../../Themes/Images/buttons/button_testconnection_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_testconnection.gif';"
                runat="server" AlternateText="TestConnection" OnClick="btnTestConnection_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" Enabled="false" />
            <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Submit" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>