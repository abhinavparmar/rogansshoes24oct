<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" Title="Manage Stores - View" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Setup.Storefront.Stores.View" ValidateRequest="false" CodeBehind="View.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc1" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="MobileSettingsList" Src="~/SiteAdmin/Controls/Default/MobileSettingsList.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>
        <div style="text-align: right" class="ImageButtons">
            <asp:Button ID="Button2" CssClass="Size100" runat="server" CausesValidation="True"
                OnClick="BtnBack_Click" Text="<< Back" />
        </div>

        <div>
            <ZNode:Spacer ID="Spacer10" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div style="clear: both;">
            <ajaxToolKit:TabContainer ID="tabStoreSettings" runat="server">
                <ajaxToolKit:TabPanel ID="pnlGeneral" runat="server">
                    <HeaderTemplate>
                        General
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div class="ViewForm200">
                            <div>
                                <ZNode:Spacer ID="Spacer11" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <div class="ImageButtons">
                                <asp:Button ID="btnSubmitTop" runat="server" CausesValidation="False" OnClick="BtnEdit_Click"
                                    Text="Edit Store Settings"></asp:Button>
                            </div>
                            <div>
                                <ZNode:Spacer ID="Spacer18" runat="server" SpacerHeight="15" SpacerWidth="3"></ZNode:Spacer>
                            </div>
                            <h4 class="SubTitle">Store Identity</h4>
                            <div class="FieldStyleA">
                                Store Name
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblStoreName" runat="server"></asp:Label>
                                &#160;
                            </div>
                            <div class="FieldStyle">
                                Brand Name
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblCompanyName" runat="server"></asp:Label>
                            </div>
                            <div class="FieldStyle">

                                <asp:Label ID="lblLocale" runat="server" Text=" Default Locale" Visible="False"></asp:Label>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblDefaultLocale" runat="server" Visible="False"></asp:Label>
                            </div>
                            <h4 class="SubTitle">Store Logo Image</h4>
                            <div class="Image">
                                <asp:Image ID="imgLogo" runat="server" />
                            </div>
                            <h4 class="SubTitle">Security</h4>
                            <div class="FieldStyleA">
                                Secure Socket Layer (SSL) Enabled?
                            </div>
                            <div class="ValueStyleA">
                                <img id="chkEnableSSL" runat="server" />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                            <h4 class="SubTitle">Store Contact Information</h4>
                            <div class="FieldStyle">
                                Administrator's Email
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblAdminEmail" runat="server"></asp:Label>
                            </div>
                            <div class="FieldStyleA">
                                Sales Department Email
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblSalesEmail" runat="server"></asp:Label>
                                &#160;
                            </div>
                            <div class="FieldStyle">
                                Customer Service Email
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblCustomerServiceEmail" runat="server"></asp:Label>
                            </div>
                            <div class="FieldStyleA">
                                Sales Department Phone Number
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblSalesPhoneNumber" runat="server"></asp:Label>
                                &#160;
                            </div>
                            <div class="FieldStyle">
                                Customer Service Phone Number
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblCustomerServicePhoneNumber" runat="server"></asp:Label>
                            </div>
                            <h4 class="SubTitle">Default Settings</h4>
                            <div class="FieldStyle">
                                Default Customer Review Status
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblDefaultReviewStatus" runat="server"></asp:Label>
                            </div>
                            <div class="FieldStyleA">
                                Default Order Status
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblDefaultOrderStatus" runat="server"></asp:Label>
                                &#160;
                            </div>
                            <div>
                                <div class="FieldStyle">
                                    Include taxes in product price
                                </div>
                                <div class="ValueStyle">
                                    <img id="chkInclusiveTax" runat="server" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </div>
                            </div>
                            <div>
                                <div class="FieldStyleA">
                                    Enable Persistent Cart
                                </div>
                                <div class="ValueStyleA">
                                    <img id="chkPersistentCart" runat="server" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </div>
                            </div>
                            <div>
                                <div class="FieldStyle">
                                    Enable Address Validation 
                                </div>
                                <div class="ValueStyle">
                                    <img id="imgEnableAddressValidation" runat="server" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </div>
                            </div>
                            <div>
                                <div class="FieldStyleA">
                                    Require Validated Address 
                                </div>
                                <div class="ValueStyleA">
                                    <img id="imgRequireValidatedAddress" runat="server" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </div>
                            </div>
                            <div class="FieldStyle">
                                Default Product Review Status
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblDefaultProductReviewStatus" runat="server" />
                            </div>
                        </div>
                        <div class="ClearBoth">
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="TabPanel2" runat="server">
                    <HeaderTemplate>
                        Catalog
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div class="ViewForm200">
                            <div>
                                <ZNode:Spacer ID="Spacer1" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <p class="ClearBoth">
                            </p>
                            <div class="TextLine">
                                <div class="ButtonStyle">
                                    <%--<zn:LinkButton ID="lbAddCatalogThemen" runat="server" ButtonType="Button" OnClick="LbAddCatalogThemen_Click"
                                        Text="Associate Catalog" ValidationGroup="grpUrl" ButtonPriority="Primary" />--%>
                                </div>
                                <div>
                                    <ZNode:Spacer ID="Spacer8" runat="server" SpacerHeight="15" SpacerWidth="3" />
                                </div>
                            </div>

                            <div colspan="2" valign="middle">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:GridView ID="gvCatalogTheme" ShowHeader="true" CaptionAlign="Left" runat="server"
                                            ForeColor="Black" CellPadding="4" AutoGenerateColumns="False" CssClass="Grid"
                                            Width="100%" GridLines="None" EmptyDataText="No Product Catalog exist in the database."
                                            AllowPaging="True" PageSize="5" OnRowCommand="GvCatalogTheme_RowCommand" OnRowCreated="GvCatalogTheme_RowCreated"
                                            OnPageIndexChanging="GvCatalogTheme_PageIndexChanging">
                                            <Columns>
                                                <asp:BoundField DataField="PortalCatalogId" HeaderText="ID" HeaderStyle-HorizontalAlign="Left">
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText="Catalog" HeaderStyle-HorizontalAlign="Left">
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Theme" HeaderText="Theme" HeaderStyle-HorizontalAlign="Left">
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CSS" HeaderText="CSS" HeaderStyle-HorizontalAlign="Left">
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="LocaleDescription" HeaderText="Locale" HeaderStyle-HorizontalAlign="Left" Visible="false">
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PortalID" HeaderStyle-HorizontalAlign="Left">
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="LocaleID" HeaderStyle-HorizontalAlign="Left">
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="ACTION" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ValidationGroup="grpUrl" ID="EditProductView" Text="EDIT &raquo"
                                                            CommandArgument='<%# Eval("PortalCatalogId") %>' Visible='<%# HideDeleteButton(Eval("Theme").ToString()) %>'
                                                            CommandName="Edit" runat="Server" />&nbsp;&nbsp;&nbsp;&nbsp;
                                                     <%--   <asp:LinkButton ID="Delete" Text="DELETE &raquo" CommandArgument='<%# Eval("PortalCatalogId") %>'
                                                            CommandName="RemoveItem" Visible='<%# HideCatalogDeleteButton(Eval("PortalCatalogId").ToString()) %>' OnClientClick='return confirm("Are you sure do you want to delete the catalog ?")'
                                                            runat="server" />--%>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="RowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                            <FooterStyle CssClass="FooterStyle" />
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="ClearBoth">
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="pnlDomainSettings" runat="server">
                    <HeaderTemplate>
                        URL
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div class="ViewForm200">
                            <div>
                                <ZNode:Spacer ID="Spacer2" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <p class="ClearBoth">
                                Select one or more URLs to use with this storefront.
                            </p>
                            <div class="TextLine">
                                <div class="ButtonStyle">
                                    <zn:LinkButton ID="Button1" runat="server"
                                        ButtonType="Button" OnClick="AddNewDomain_Click" ValidationGroup="grpUrl" Text="Add URL"
                                        ButtonPriority="Primary" />
                                </div>
                            </div>
                            <div>
                                <ZNode:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <div>
                                <div class="Error">
                                    <asp:Label ID="lblDomainError" runat="server" />
                                </div>
                            </div>
                            <div>
                                <div colspan="2" valign="middle">
                                    <asp:UpdatePanel ID="updPnlGrid" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:GridView ID="uxDomainGrid" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
                                                runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
                                                CssClass="Grid" Width="100%" GridLines="None" OnRowCommand="UxDomainGrid_RowCommand"
                                                AllowPaging="True" OnPageIndexChanging="UxDomainGrid_PageIndexChanging" OnRowDeleting="UxDomainGrid_RowDeleting"
                                                PageSize="5">
                                                <Columns>
                                                    <asp:BoundField DataField="DomainId" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:TemplateField HeaderText="URL Name" HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <%# "http://" + Eval("DomainName")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Active" HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <img id="Img1" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "IsActive").ToString()))%>'
                                                                runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ACTION" HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ValidationGroup="grpUrl" ID="EditProductView" Text="EDIT &raquo"
                                                                CommandArgument='<%# Eval("DomainID") %>' Visible='<%# HideDeleteButton(Eval("DomainName").ToString()) %>'
                                                                CommandName="Edit" runat="Server" />&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <asp:LinkButton ID="Delete" Text="DELETE &raquo" CommandArgument='<%# Eval("DomainID") %>'
                                                                CommandName="RemoveItem" Visible='<%# HideDeleteButton(Eval("DomainName").ToString()) %>'
                                                                runat="server" />
                                                            <asp:HiddenField ID="hdnDomainName" runat="server" Value='<%# Eval("DomainName") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle CssClass="RowStyle" />
                                                <HeaderStyle CssClass="HeaderStyle" />
                                                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                                <FooterStyle CssClass="FooterStyle" />
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            </table>
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="pnlProfile" runat="server">
                    <HeaderTemplate>
                        Profiles
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div class="ViewForm200">
                            <div>
                                <ZNode:Spacer ID="Spacer5" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <div>
                                <div class="Error">
                                    <div>
                                        <div>
                                            <asp:Label ID="lblProfileError" runat="server" Text="" />
                                        </div>
                                        <div class="ButtonStyle">
                                            <zn:LinkButton ID="btnAddProfile" runat="server" ButtonType="Button" OnClick="AddProfile_Click" Text="Add Profile" ValidationGroup="grpProfiles"
                                                ButtonPriority="Primary" />
                                        </div>
                                        <div>
                                            <ZNode:Spacer ID="Spacer6" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div valign="middle">
                                    <asp:UpdatePanel ID="updProfileGrid" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:GridView ID="uxProfileGrid" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
                                                runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
                                                CssClass="Grid" Width="100%" GridLines="None" OnRowCommand="UxProfileGrid_RowCommand"
                                                AllowPaging="True" OnPageIndexChanging="UxProfileGrid_PageIndexChanging" OnRowDeleting="UxProfileGrid_RowDeleting"
                                                PageSize="5">
                                                <Columns>
                                                    <asp:BoundField DataField="ProfileID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <%# Eval("Name")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Is Default Anonymous" HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <img alt="" id="UseWholeSalePricingImg" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "IsDefaultAnonymousProfile").ToString())) %>'
                                                                runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Is Default Registered" HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <img alt="" id="ShowPricingImg" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "IsDefaultRegisteredProfile").ToString())) %>'
                                                                runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ACTION" HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="Edit" Text="EDIT &raquo" ValidationGroup="grpProfiles" CommandArgument='<%# Eval("PortalProfileID") %>'
                                                                CommandName="Edit" runat="server" />&nbsp;&nbsp;&nbsp;
                                                            <asp:LinkButton ID="Delete" Text="REMOVE &raquo" ValidationGroup="grpProfiles" CommandArgument='<%# Eval("PortalProfileID") %>'
                                                                CommandName="Remove" runat="server" OnClientClick="return confirm('Are you sure you want to delete this Profile?')" />
                                                            <asp:HiddenField ID="hdnProfileName" runat="server" Value='<%# Eval("Name") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle CssClass="RowStyle" />
                                                <HeaderStyle CssClass="HeaderStyle" />
                                                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                                <FooterStyle CssClass="FooterStyle" />
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="pnlDisplay" runat="server">
                    <HeaderTemplate>
                        Display
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div class="ViewForm300">
                            <div>
                                <ZNode:Spacer ID="Spacer13" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <div class="ImageButtons">
                                <asp:Button ID="btnEditDisplay" runat="server" CausesValidation="False" OnClick="BtnEditDisplay_Click"
                                    Text="Edit Display Settings" />
                            </div>
                            <div>
                                <ZNode:Spacer ID="Spacer22" runat="server" SpacerHeight="15" SpacerWidth="3" />
                            </div>
                            <h4 class="SubTitle">PRODUCT GRID SETTINGS</h4>
                            <div class="FieldStyle">
                                Number of Product Columns
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblMaxCatalogDisplayColumns" runat="server"></asp:Label>
                            </div>
                            <div class="FieldStyleA">
                                Number of Thumbnail Columns
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblMaxSmallThumbnailsDisplay" runat="server"></asp:Label>
                            </div>
                            <div class="FieldStyle">
                                Display popular products on top of the list?
                            </div>
                            <div class="ValueStyle">
                                <img id="chkDynamicDisplayOrder" runat="server" />
                            </div>
                            <h4 class="SubTitle">AUTO IMAGE RESIZE SETTINGS</h4>
                            <div class="FieldStyle">
                                Large Image
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblMaxCatalogItemLargeWidth" runat="server"></asp:Label>
                                pixels
                            </div>
                            <div class="FieldStyleA">
                                Medium Image
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblMaxCatalogItemMediumWidth" runat="server"></asp:Label>&nbsp;pixels
                            </div>
                            <div class="FieldStyle">
                                Small Image
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblMaxCatalogItemSmallWidth" runat="server"></asp:Label>&nbsp;pixels
                            </div>
                            <div class="FieldStyleA">
                                Cross-Sell Image
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblMaxCatalogItemCrossSellWidth" runat="server"></asp:Label>&nbsp;pixels
                            </div>
                            <div class="FieldStyle">
                                Thumbnail Image
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblMaxCatalogItemThumbnailWidth" runat="server"></asp:Label>&nbsp;pixels
                            </div>
                            <div class="FieldStyleA">
                                Small Thumbnail Image
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblMaxCatalogItemSmallThumbnailWidth" runat="server"></asp:Label>&nbsp;pixels
                            </div>
                            <h4 class="SubTitle">DEFAULT PRODUCT IMAGE</h4>
                            <div class="Image">
                                <asp:Image ID="ItemImage" runat="server" />
                            </div>
                        </div>
                        <div class="ClearBoth">
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="pnlAdvanced" HeaderText="Units" runat="server">
                    <ContentTemplate>
                        <div class="ViewForm200">
                            <div>
                                <ZNode:Spacer ID="Spacer14" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <div class="ImageButtons">
                                <asp:Button ID="btnEditUnits" runat="server" CausesValidation="false" OnClick="BtnEditUnitSettings_Click"
                                    Text="Edit Unit Settings" />
                            </div>
                            <div>
                                <ZNode:Spacer ID="Spacer25" runat="server" SpacerHeight="15" SpacerWidth="3" />
                            </div>
                            <h4 class="SubTitle">Unit settings</h4>
                            <div class="FieldStyle">
                                Weight Unit
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblWeightUnit" runat="server"></asp:Label>
                            </div>
                            <div class="FieldStyleA">
                                Dimensions Unit
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblDimensionUnit" runat="server"></asp:Label>&nbsp;
                            </div>
                            <h4 class="SubTitle">Currency Settings</h4>
                            <div class="FieldStyle">
                                Currency Preview
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblCurrency" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="ClearBoth">
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="pnlCountries" HeaderText="Countries" runat="server">
                    <ContentTemplate>
                        <div class="ViewForm200">
                            <div>
                                <ZNode:Spacer ID="Spacer17" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <div class="ButtonStyle">
                                <zn:LinkButton ID="AddCountries" runat="server" ButtonType="Button" OnClick="AddCountries_Click" Text="Add Countries to Store" ValidationGroup="grpCountries"
                                    ButtonPriority="Primary" />
                            </div>
                            <div>
                                <ZNode:Spacer ID="Spacer4" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <asp:UpdatePanel ID="updPnlCountryGrid" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:GridView ID="uxCountryGrid" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
                                        runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
                                        CssClass="Grid" Width="100%" GridLines="None" OnRowCommand="UxCountryGrid_RowCommand"
                                        AllowPaging="True" OnPageIndexChanging="UxCountryGrid_PageIndexChanging" PageSize="5">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Country Name" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <%# Eval("CountryCodeSource.Name")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="CountryCode" HeaderText="Country Code" HeaderStyle-HorizontalAlign="Left" />
                                            <asp:TemplateField HeaderText="Display Country in Lists" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <img alt="" id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "BillingActive").ToString()))%>'
                                                        runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Allow Shipping to this Country" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <img alt="" id="Img2" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ShippingActive").ToString()))%>'
                                                        runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ACTION" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="Delete" Text="REMOVE &raquo" CommandArgument='<%# Eval("PortalCountryId") %>'
                                                        CommandName="RemoveItem" ValidationGroup="grpCountries" runat="server" />
                                                    <asp:HiddenField ID="hdnCountryName" runat="server" Value='<%# Eval("CountryCodeSource.Name")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="RowStyle" />
                                        <HeaderStyle CssClass="HeaderStyle" />
                                        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                        <FooterStyle CssClass="FooterStyle" />
                                    </asp:GridView>
                                    <asp:Label CssClass="Error" ID="lblCountryMsg" Text="" runat="server" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="TabPanel1" HeaderText="Shipping" runat="server">
                    <ContentTemplate>
                        <div class="ViewForm200">
                            <div>
                                <ZNode:Spacer ID="Spacer16" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <div class="ImageButtons">
                                <asp:Button ID="btnEditShippingThreshold" runat="server" CausesValidation="false" OnClick="btnEditShippingThreshold_Click"
                                    Text="Edit Shipping Threshold" />
                            </div>


                            <h4 class="SubTitle">Shipping Threshold</h4>
                            <div class="FieldStyle">
                                Shipping Threshold Amount
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblShippingThresholdAmount" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div>
                                <ZNode:Spacer ID="Spacer12" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <br />
                            <br />
                            <div class="ImageButtons">
                                <asp:Button ID="btnSetUSPSShippingDetails" runat="server" CausesValidation="false" OnClick="btnSetUSPSShippingDetails_Click"
                                    Text="Edit USPS Shipping Details" />
                            </div>
                            <h4 class="SubTitle">USPS</h4>
                            <div class="FieldStyle">
                                USPS User Name
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblUSPSUserName" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyleA">
                                USPS Password
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblUSPSPassword" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div>
                                <br />
                                <br />
                            </div>
                            <h4 class="SubTitle"></h4>
                            <div class="ImageButtons">
                                <asp:Button ID="btnEditShipping" runat="server" CausesValidation="false" OnClick="BtnEditShipping_Click"
                                    Text="Edit Shipping Settings" />
                            </div>
                            <h4 class="SubTitle">Shipping Origin <span style="text-transform: lowercase">or </span>Warehouse Location</h4>
                            <div class="FieldStyle">
                                Shipping Origin Address 1
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblShippingAddress1" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyleA">
                                Shipping Origin Address 2
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblShippingAddress2" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyle">
                                Shipping Origin City
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblShippingCity" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyleA">
                                Shipping Origin State Code
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblShippingStateCode" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyle">
                                Shipping Origin Zip Code
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblShippingZipCode" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyleA">
                                Shipping Origin Country Code
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblOriginCountryCode" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyle">
                                Shipping Origin Phone
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblShippingPhone" runat="server"></asp:Label>&nbsp;
                            </div>
                            <h4 class="SubTitle">FedEx</h4>
                            <div class="FieldStyle">
                                FedEx&reg; Account Number
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblAccountNum" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyleA">
                                FedEx&reg; Meter Number
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblMeterNum" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyle">
                                FedEx Production key
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblProductionAccessKey" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyleA">
                                FedEx Security Code
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblSecurityCode" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyle">
                                Select FedEx Drop-Off Type
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lbldropOffTypes" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyleA">
                                Select FedEx Packaging Type
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblPackageTypeCodes" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyleImg">
                                FedEx Discount Rate Enabled?
                            </div>
                            <div class="ValueStyleImg">
                                <img id="chkFedExDiscountRate" runat="server" alt="" src="" />
                            </div>
                            <div class="FieldStyleImgA">
                                Insurance Enabled?
                            </div>
                            <div class="ValueStyleImgA">
                                <img id="chkAddInsurance" runat="server" alt="" src="" />&nbsp;
                            </div>
                            <h4 class="SubTitle">UPS</h4>
                            <div class="FieldStyle">
                                UPS User Name
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblUPSUserName" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyleA">
                                UPS Password
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblUPSPassword" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyle">
                                UPS Access key
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblUPSKey" runat="server"></asp:Label>&nbsp;
                            </div>
                        </div>
                        <div class="ClearBoth">
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="pnlGoogleAnalystics" HeaderText="JavaScript" runat="server">
                    <ContentTemplate>
                        <div class="ViewForm300">
                            <div class="ClearBoth">
                                <p>Enter custom Javascript code such as analytics to be injected into your store pages.</p>
                            </div>
                            <div class="ImageButtons">
                                <asp:Button ID="btnEditAnalytics" runat="server" CausesValidation="false" CssClass="Size175"
                                    OnClick="BtnEditAnalytics_Click" Text="EDIT JAVASCRIPT" />
                            </div>
                            <h4 class="SubTitle">JAVASCRIPT</h4>
                            <div class="FieldStyle">
                                Site Wide Javascript (Top)
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblSiteWideTopJavaScript" runat="server"></asp:Label>
                            </div>
                            <div class="MainStyleA">
                                <div class="FieldStyleA">
                                    Site Wide Javascript (Bottom)
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblSiteWideBottomJavaScript" runat="server"></asp:Label>&nbsp;
                                </div>
                            </div>
                            <div class="FieldStyle">
                                Site Wide Javascript (except Order Receipt page)
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblSiteWideAnalyticsJavascript" runat="server"></asp:Label>
                            </div>
                            <div class="MainStyleA">
                                <div class="FieldStyleA">
                                    Order Receipt Javascript
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblOrderReceiptJavaScript" runat="server"></asp:Label>&nbsp;
                                </div>
                            </div>
                        </div>
                        <div class="ClearBoth">
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="pnlSMTPSettings" HeaderText="SMTP" runat="server">
                    <ContentTemplate>
                        <div class="ViewForm200">
                            <div>
                                <ZNode:Spacer ID="Spacer15" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <div class="ImageButtons">
                                <asp:Button ID="btnSMtp" runat="server" CausesValidation="false" OnClick="BtnSMTPSettings_Click"
                                    Text="Edit SMTP Settings" />
                            </div>
                            <div>
                                <ZNode:Spacer ID="Spacer31" runat="server" SpacerHeight="15" SpacerWidth="3" />
                            </div>
                            <h4 class="SubTitle">SMTP Settings</h4>
                            <div class="FieldStyle">
                                SMTP Port
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblSMTPPort" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyleA">
                                SMTP Server
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblSMTPServer" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyle">
                                SMTP Server User Name
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblSMTPUserName" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyleA">
                                SMTP Server Password
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblSMTPPassword" runat="server"></asp:Label>&nbsp;
                            </div>
                        </div>
                        <div class="ClearBoth">
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="tabAvatax" runat="server">
                    <HeaderTemplate>
                        Avatax Settings
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div class="ViewForm200">
                            <div>
                                <ZNode:Spacer ID="Spacer7" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <div class="ImageButtons">
                                <asp:Button ID="btnEditSettings" runat="server" CausesValidation="false" OnClick="btnEditSettings_Click"
                                    Text="Edit Avatax Settings"></asp:Button>
                            </div>
                            <div>
                                <ZNode:Spacer ID="Spacer9" runat="server" SpacerHeight="15" SpacerWidth="3"></ZNode:Spacer>
                            </div>
                            <h4 class="SubTitle">Avatax Settings</h4>
                            <div class="FieldStyleA">
                                Account ID
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblAvataxAccountID" runat="server"></asp:Label>
                                &#160;
                            </div>
                            <div class="FieldStyle">
                                License Key
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblAvataxLicenseKey" runat="server"></asp:Label>
                            </div>
                            <div class="FieldStyleA">
                                Company
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblAvataxCompany" runat="server"></asp:Label>
                            </div>
                            <div class="FieldStyle">
                                Webservice URL
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblAvataxWebServiceUrl" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="ClearBoth">
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="tabMobileSettings" HeaderText="Mobile " runat="server">
                    <ContentTemplate>
                        <div class="ViewForm300">
                            <div class="ClearBoth">
                                <p>Configure the theme and display settings for the mobile browser version of your store.</p>

                            </div>
                            <h4 class="SubTitle">Mobile Settings</h4>
                            <div>
                                <ZNode:MobileSettingsList ID="uxMobileSettings" runat="server"></ZNode:MobileSettingsList>
                            </div>
                        </div>
                        <div class="ClearBoth">
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
            </ajaxToolKit:TabContainer>
        </div>
    </div>
</asp:Content>
