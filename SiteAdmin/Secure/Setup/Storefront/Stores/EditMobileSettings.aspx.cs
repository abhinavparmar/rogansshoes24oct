﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web.UI;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;

namespace SiteAdmin.Secure.Setup.Storefront.Stores
{
    /// <summary>
    /// Represents the EditMobileSettings class.
    /// </summary>
    public partial class EditMobileSettings : System.Web.UI.Page
    {
        #region Private Variables
        private string viewPageUrl = "View.aspx?mode=mobilesettings&itemid=";
        private int itemId = 0;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.itemId = Convert.ToInt32(Request.QueryString["itemid"]);

            if (this.itemId == 0)
            {
                Response.Redirect(this.viewPageUrl);
            }

            if (!IsPostBack)
            {
                this.BindTheme();

                this.BindMobileSettings();
            }

        }

        protected void BtnCancel_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect(this.viewPageUrl + this.itemId);
        }

        protected void BtnSubmit_Click(object sender, ImageClickEventArgs e)
        {
            this.SubmitMobileSettings();
        }

        /// <summary>
        /// Bind theme file list
        /// </summary>
        private void BindTheme()
        {
            System.IO.DirectoryInfo path = new System.IO.DirectoryInfo(Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ApplicationPath + ConfigurationManager.AppSettings["WebThemePath"].ToString()));
            ddlMobileThemes.DataSource = path.GetDirectories();
            ddlMobileThemes.DataBind();
            ddlMobileThemes.SelectedIndex = 0;

            // Remove the svn hidden path folder.
            ddlMobileThemes.Items.Remove(".svn");
        }

        /// <summary>
        /// Bind mobile settings.
        /// </summary>
        private void SubmitMobileSettings()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            StoreSettingsAdmin storeSettingsAdmin = new StoreSettingsAdmin();
            PortalAdmin portalAdmin = new PortalAdmin();

            // set logo path
            System.IO.FileInfo fileInfo = null;
            string fileName = string.Empty;
            string allowedExtension = ".jpeg .jpg .png .gif";

            if (radNewImage.Checked == true)
            {
                // Check for Product Image
                fileInfo = new System.IO.FileInfo(System.IO.Path.GetFileNameWithoutExtension(UploadImage.PostedFile.FileName) + "_" + DateTime.Now.ToString("ddMMyyhhmmss") + System.IO.Path.GetExtension(UploadImage.PostedFile.FileName));
                fileName = System.IO.Path.GetFileNameWithoutExtension(UploadImage.PostedFile.FileName) + "_" + DateTime.Now.ToString("ddMMyyhhmmss") + System.IO.Path.GetExtension(UploadImage.PostedFile.FileName);

                if ((fileInfo != null) && (fileName != string.Empty))
                {
                    // Check is posted file extesion is valid
                    List<string> extensionList = new List<string>();
                    extensionList.AddRange(allowedExtension.Split(new char[] { ' ' }));
                    string postedFileExtension = Path.GetExtension(fileInfo.FullName.ToLower());
                    if (extensionList.Contains(postedFileExtension))
                    {
                        string spalshFileFullName = ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + fileName;
                        byte[] imageData = new byte[UploadImage.PostedFile.InputStream.Length];
                        UploadImage.PostedFile.InputStream.Read(imageData, 0, (int)UploadImage.PostedFile.InputStream.Length);
                        ZNodeStorageManager.WriteBinaryStorage(imageData, spalshFileFullName);

                        // Resize the splash image.
                        ZNodeImage znodeImage = new ZNodeImage();
                        znodeImage.ResizeMobileSplashImage(spalshFileFullName);
                    }
                    else
                    {
                        lblImageError.Text = "Select a valid jpg, jpeg, gif or png image.";
                        return;
                    }
                }
            }

            Portal portal = portalAdmin.GetByPortalId(this.itemId);
            if (portal != null)
            {
                portal.MobileTheme = ddlMobileThemes.SelectedItem.Text;
                if (radNewImage.Checked)
                {
                    portal.SplashImageFile = fileInfo.Name;
                }

                // Remove the current image.
                if (rbRemoveCurrentImage.Checked)
                {
                    portal.SplashImageFile = null;
                }

                bool isUpdated = storeSettingsAdmin.Update(portal);
                if (isUpdated)
                {
                    Response.Redirect(this.viewPageUrl + this.itemId);
                }
                else
                {
                    lblError.Text = "Mobile settings update failed. Please try again.";
                }
            }
        }

        /// <summary>
        /// Get the source splash image directory.
        /// </summary>
        /// <returns>Returns the splash image directory.</returns>
        private string GetSplashImageDirectory()
        {
            string dir = Path.Combine(ZNodeConfigManager.EnvironmentConfig.DataPath, "Images/Splash");

            // If splash image directory doesn't exist then create a new directory
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            return dir;
        }

        /// <summary>
        /// Bind the mobile settings.
        /// </summary>
        private void BindMobileSettings()
        {
            PortalAdmin portalAdmin = new PortalAdmin();
            Portal portal = portalAdmin.GetByPortalId(this.itemId);
            ddlMobileThemes.SelectedIndex = ddlMobileThemes.Items.IndexOf(ddlMobileThemes.Items.FindByText(portal.MobileTheme));
            lblStoreName.Text = portal.StoreName;

            ZNodeImage znodeImage = new ZNodeImage();
            if (!string.IsNullOrEmpty(portal.SplashImageFile))
            {
                string splashImageFullName = Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.OriginalImagePath, portal.SplashImageFile));
                if (File.Exists(splashImageFullName))
                {
                    imgLogo.ImageUrl = znodeImage.GetImageBySize(320, portal.SplashImageFile);
                }
                else
                {
                    imgLogo.ImageUrl = znodeImage.GetImageHttpPathMedium(string.Empty);
                }
            }
            else
            {
                imgLogo.ImageUrl = znodeImage.GetImageHttpPathMedium(string.Empty);
            }
        }

        /// <summary>
        /// Get category name by category Id.
        /// </summary>
        /// <param name="categoryId">Category Id to get the category object.</param>
        /// <returns>Returns the category name.</returns>
        private string GetCategoryName(int? categoryId)
        {
            string categoryName = string.Empty;
            if (categoryId.HasValue)
            {
                CategoryAdmin categoryAdmin = new CategoryAdmin();
                Category category = categoryAdmin.GetByCategoryId(Convert.ToInt32(categoryId));
                if (category != null)
                {
                    categoryName = category.Name;
                }
            }

            return categoryName;
        }
    }
}
