<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master"
    Inherits="SiteAdmin.Secure.Setup.Storefront.Stores.Countries" Title="Manage Stores - Add Countries" CodeBehind="Countries.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode"
    TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>Add Countries to Store</h1>
        </div>
        <div style="text-align: right; padding-right: 10px;" class="ImageButtons">
            <asp:Button ID="btnBack" CssClass="Size100" Text="<< Back" runat="server" CausesValidation="False"
                OnClick="BtnBack_Click" />
        </div>
        <ZNode:DemoMode ID="DemoMode1" runat="server"></ZNode:DemoMode>
        <!-- Search product panel -->
        <asp:Panel ID="pnlSearch" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <h4 class="SubTitle">Search Country</h4>
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">Keyword</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtkeyword" runat="server"></asp:TextBox></span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_clear_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                        runat="server" AlternateText="Clear" OnClick="BtnClear_Click" />
                    <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                        runat="server" AlternateText="Search" OnClick="BtnSearch_Click" />
                </div>
            </div>
        </asp:Panel>
        <br />
         <div>
                <asp:Label ID="lblError" runat="server" Text="" Visible="false" CssClass="Error"
                    ForeColor="red"></asp:Label>
            </div>
        <!-- Product List -->
        <asp:Panel ID="pnlCountryList" runat="server" Visible="false">
            <h4 class="SubTitle">Country List</h4>
            <p>
                Select all the countries that you need to associate with this store and click on
                Submit.
            </p>
            <div align="right">
                <asp:ImageButton ID="Button1" ImageUrl="~/SiteAdmin/Themes/images/button_submit.gif"
                    Text="Submit" OnClick="AddNew_Click" runat="server" AlternateText="Submit"/>
                <asp:ImageButton ID="Button2" ImageUrl="~/SiteAdmin/Themes/images/button_cancel.gif"
                    Text="Cancel" OnClick="Cancel_Click" runat="server" AlternateText="Cancel"/>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer4" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
           
            <div>
                <ZNode:Spacer ID="Spacer5" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <asp:GridView ID="uxCountryGrid" runat="server" CssClass="Grid" CaptionAlign="Left"
                AutoGenerateColumns="False" GridLines="None" EmptyDataText="No Countries exist in the database."
                Width="100%" CellPadding="4">
                <Columns>
                    <asp:TemplateField HeaderText="Select" Visible="false">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkCountry" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Display Country in Lists" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkBillingActive" runat="server" Checked='<%# bool.Parse(DataBinder.Eval(Container.DataItem,"EnableCountry").ToString())%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Allow Shipping to this Country" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkShippingActive" runat="server" Checked='<%# bool.Parse(DataBinder.Eval(Container.DataItem,"EnableShipping").ToString())%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Code" HeaderText="Country Code" HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="Name" HeaderText="Country Name" HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText="Is Active" Visible="false" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img alt="" id="chMark" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
            <div>
                <ZNode:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div class="ClearBoth">
                <br />
            </div>
            <div>
                <asp:ImageButton ID="btnAddNew" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" OnClick="AddNew_Click" CausesValidation="true" />
                <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cacnel" OnClick="Cancel_Click" />
            </div>
        </asp:Panel>
        <div>
            <ZNode:Spacer ID="Spacer3" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
    </div>
</asp:Content>
