﻿using System;
using System.Data;
using System.Web.UI;
using Zeon.Libraries.Avatax;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace WebApp.SiteAdmin.Secure.Setup.Storefront.Stores
{
    public partial class EditAvataxSettings : System.Web.UI.Page
    {
        #region Protected Member Variables

        private int itemId = 0;
        private string redirectUrl = "view.aspx?ItemId={0}&mode=avatax";

        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get portalId value from querystring
            if (Request.Params["itemid"] != null)
            {
                this.itemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (!Page.IsPostBack)
            {
                this.Bind();
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Bind the store shipping details.
        /// </summary>
        private void Bind()
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            Portal portal = storeAdmin.GetByPortalId(itemId);
            AvataxHelper helper = new AvataxHelper();

            DataSet dsAvataxCredentials = helper.GetAvataxDetails(this.itemId);

            if (portal != null)
            {
                lblTitle.Text = "Edit Avatax Credentials for \"" + portal.StoreName + "\"";
                if (dsAvataxCredentials != null && dsAvataxCredentials.Tables != null && dsAvataxCredentials.Tables[0].Rows.Count > 0)
                {
                    try
                    {
                        txtAccountID.Text = dsAvataxCredentials.Tables[0].Rows[0]["AccountID"].ToString();
                        txtLicense.Text = dsAvataxCredentials.Tables[0].Rows[0]["LicenceKey"].ToString();
                        txtCompany.Text = dsAvataxCredentials.Tables[0].Rows[0]["CompanyName"].ToString();
                        txtWebserviceURL.Text = dsAvataxCredentials.Tables[0].Rows[0]["Url"].ToString();
                        hdnAvataxID.Value = dsAvataxCredentials.Tables[0].Rows[0]["AvataxCredID"].ToString();
                    }
                    catch
                    {
                        // Ignore decryption errors
                    }
                }
            }
        }

        /// <summary>
        /// Update the Avatax Credentials
        /// </summary>
        private bool UpdateAvataxCredentials(int avataxID)
        {
            AvataxHelper helper = new AvataxHelper();
            bool isUpdated = false;
            string jointRule = string.Format("AccountID={0}|LicenseKey={1}|Company={2}|url={3}", txtAccountID.Text.Trim(), txtLicense.Text.Trim(), txtCompany.Text.Trim(), txtWebserviceURL.Text.Trim());
            int iUpdated = helper.UpdateAvataxCredentials(avataxID, this.itemId, txtAccountID.Text.Trim(), txtLicense.Text.Trim(), txtCompany.Text.Trim(), txtWebserviceURL.Text.Trim(), jointRule);

            if (iUpdated > 0)
            {
                isUpdated = true;
            }

            return isUpdated;
        }

        /// <summary>
        /// Insert the Avatax Credentials
        /// </summary>
        private bool InsertAvataxCredentials()
        {
            AvataxHelper helper = new AvataxHelper();
            bool isInserted = false;
            int iInserted = helper.InsertAvataxCredentials(this.itemId, txtAccountID.Text.Trim(), txtLicense.Text.Trim(), txtCompany.Text.Trim(), txtWebserviceURL.Text.Trim());

            if (iInserted > 0)
            {
                isInserted = true;
            }

            return isInserted;
        }

        #endregion

        #region Custom Events

        /// <summary>
        /// TestConnection Button click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnTestConnection_Click(object sender, EventArgs e)
        {
            Avatax avaTax = new Avatax();
            DateTime? dtExpiresOn = avaTax.TestConnection(txtWebserviceURL.Text.Trim(), txtAccountID.Text.Trim(), txtLicense.Text.Trim());
            if (dtExpiresOn != null && Convert.ToDateTime(dtExpiresOn) > DateTime.MinValue)
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "alert", "alert('Configuration validated successfully. Your license is valid till " + Convert.ToDateTime(dtExpiresOn).ToString("MM/dd/yyyy") + ". ');", true);
                btnSubmitTop.Enabled = true;
                btnSubmitBottom.Enabled = true;
                txtAccountID.Enabled = false;
                txtCompany.Enabled = false;
                txtLicense.Enabled = false;
                txtWebserviceURL.Enabled = false;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "alert", "alert('Test connection failed.');", true);
                btnSubmitTop.Enabled = false;
                btnSubmitBottom.Enabled = false;
            }
        }

        /// <summary>
        /// Submit Button click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            Portal portal = null;
            bool isUpdated = false;

            if (this.itemId > 0)
            {
                portal = storeAdmin.GetByPortalId(this.itemId);
            }

            if (portal != null)
            {
                int avataxID = 0;
                if (int.TryParse(hdnAvataxID.Value, out avataxID))
                {
                    if (avataxID > 0)
                    {
                        isUpdated = UpdateAvataxCredentials(avataxID);
                    }
                    else
                    {
                        isUpdated = InsertAvataxCredentials();
                    }
                }
                else
                {
                    isUpdated = InsertAvataxCredentials();
                }
            }

            if (isUpdated)
            {
                Response.Redirect(string.Format(this.redirectUrl, this.itemId));
            }
            else
            {
                lblMsg.Text = "An error ocurred while updating the avatax credentials. Please try again.";
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format(this.redirectUrl, this.itemId));
        }

        #endregion
    }
}