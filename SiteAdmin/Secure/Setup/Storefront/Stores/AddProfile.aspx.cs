using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Setup.Storefront.Stores
{
    /// <summary>
    /// Represents the SiteAdmin Admin_Secure_settings_Stores_AddProfile class
    /// </summary>
    public partial class AddProfile : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId = 0;
        private int NodeId = 0;
        private string ViewPage = "View.aspx?itemid=";
        private string AssociateName = string.Empty;
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (Request.Params["NodeId"] != null)
            {
                this.NodeId = int.Parse(Request.Params["NodeId"].ToString());
            }

            if (!Page.IsPostBack)
            {
                this.BindProfile();
                if (this.NodeId > 0)
                {
                    this.BindData();
                    ddlProfileList.SelectedValue = this.NodeId.ToString();
                    ddlProfileList.Enabled = false;
                    lblTitle.Text = "Edit Profiles to Store - " + ddlProfileList.SelectedItem.Text;
                }
            }
        }

        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Submit_Click(object sender, EventArgs e)
        {
            // Adding profile in the portal profile
            PortalService portalService = new PortalService();
            PortalAdmin portalAdmin = new PortalAdmin();
            PortalProfileAdmin portalProfileAdmin = new PortalProfileAdmin();
            PortalProfile portalProfile = new PortalProfile();
            portalProfile.ProfileID = int.Parse(ddlProfileList.SelectedValue);
            portalProfile.PortalID = this.ItemId;

            Portal portal = portalAdmin.GetByPortalId(this.ItemId);

            if (this.NodeId == 0)
            {
                portalProfileAdmin.Insert(portalProfile);

                // Log Activity
                this.AssociateName = "Associate Profile " + ddlProfileList.SelectedItem.Text + " to Store " + portal.StoreName;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, portal.StoreName);
            }

            // Updating the portal table
            if (chkIsDefaultAnonymous.Checked)
            {
                portal.DefaultAnonymousProfileID = int.Parse(ddlProfileList.SelectedValue);
            }
            else
            {
                if (portal.DefaultAnonymousProfileID == int.Parse(ddlProfileList.SelectedValue))
                {
                    portal.DefaultAnonymousProfileID = null;
                }
                else
                {
                    portal.DefaultAnonymousProfileID = portal.DefaultAnonymousProfileID;
                }
            }

            if (chkIsDefaultRegistered.Checked)
            {
                portal.DefaultRegisteredProfileID = int.Parse(ddlProfileList.SelectedValue);
            }
            else
            {
                if (portal.DefaultRegisteredProfileID == int.Parse(ddlProfileList.SelectedValue))
                {
                    portal.DefaultRegisteredProfileID = null;
                }
                else
                {
                    portal.DefaultRegisteredProfileID = portal.DefaultRegisteredProfileID;
                }
            }

            portalService.Update(portal);

            // Log Activity
            this.AssociateName = "Edit Association between Profile  " + ddlProfileList.SelectedItem.Text + " and Store " + portal.StoreName;
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, portal.StoreName);

            // To Reset the Profile
            HttpContext.Current.Session.Remove("ProfileCache");
            Response.Redirect(this.ViewPage + this.ItemId + "&Mode=profiles");
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ViewPage + this.ItemId + "&Mode=profiles");
        }

        /// <summary>
        /// Back Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ViewPage + this.ItemId + "&Mode=profiles");
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Bind Profile Method
        /// </summary>
        private void BindProfile()
        {
            ProfileAdmin profileAdmin = new ProfileAdmin();
            if (this.NodeId > 0)
            {
                ddlProfileList.DataSource = profileAdmin.GetAll();
            }
            else
            {
                ddlProfileList.DataSource = profileAdmin.GetProfilesByPortalID(this.ItemId);
            }

            ddlProfileList.DataTextField = "Name";
            ddlProfileList.DataValueField = "ProfileID";
            ddlProfileList.DataBind();
            foreach (ListItem liprofile in ddlProfileList.Items)
            {
                liprofile.Text = Server.HtmlDecode(liprofile.Text);
            }
        }

        /// <summary>
        /// Bind Data Method
        /// </summary>
        private void BindData()
        {
            PortalService portalService = new PortalService();
            Portal currentPortal = portalService.GetByPortalID(this.ItemId);
            if (currentPortal.DefaultAnonymousProfileID == this.NodeId)
            {
                chkIsDefaultAnonymous.Checked = true;
            }

            if (currentPortal.DefaultRegisteredProfileID == this.NodeId)
            {
                chkIsDefaultRegistered.Checked = true;
            }
        }
        #endregion
    }
}