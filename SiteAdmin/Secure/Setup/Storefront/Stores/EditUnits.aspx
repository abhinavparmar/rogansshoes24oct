<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Title="Manage Stores - Edit Units" Inherits="SiteAdmin.Secure.Setup.Storefront.Stores.EditUnits" CodeBehind="EditUnits.aspx.cs" ValidateRequest="false" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label><ZNode:DemoMode ID="DemoMode1"
                    runat="server" />
            </h1>
        </div>
        <div style="text-align: right; padding-right: 10px;">
            <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
        <div align="left">
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>
        <div class="ClearBoth">
        </div>
        <h4 class="SubTitle">Unit settings</h4>
        <div class="FieldStyle">
            Weight Unit
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlWeightUnits" runat="server">
                <asp:ListItem Selected="True" Text="LBS"></asp:ListItem>
                <asp:ListItem Text="KGS"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="FieldStyle">
            Dimensions Unit
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlDimensions" runat="server">
                <asp:ListItem Selected="True" Text="IN"></asp:ListItem>
                <asp:ListItem Text="CM"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <h4 class="SubTitle">Currency Settings</h4>
        <asp:UpdatePanel ID="updPnlcurrencySettings" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="FieldStyle">
                    Active Currency
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlCurrencyTypes" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DdlCurrencyTypes_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
                <div class="FieldStyle">
                    Currency Suffix
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtCurrencySuffix" runat="server" AutoPostBack="True" OnTextChanged="TxtCurrencySuffix_TextChanged"></asp:TextBox>
                </div>
                <div class="FieldStyle">
                    Preview:
                    <asp:Label ID="lblPrice" runat="server"></asp:Label>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="ClearBoth">
            <ZNode:Spacer ID="Spacer8" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div>
            <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>
