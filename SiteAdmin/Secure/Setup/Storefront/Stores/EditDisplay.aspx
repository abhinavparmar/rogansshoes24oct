<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" Title="Manage Stores - Edit Display" AutoEventWireup="True" ValidateRequest="false" Inherits="SiteAdmin.Secure.Setup.Storefront.Stores.EditDisplay" CodeBehind="EditDisplay.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/ImageUploader.ascx" TagName="UploadImage" TagPrefix="ZNode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView Size350">
        <div>
            <div class="LeftFloat" style="width: 70%; text-align: left">
                <h1>
                    <asp:Label ID="lblTitle" runat="server"></asp:Label><ZNode:DemoMode ID="DemoMode1"
                        runat="server" />
                </h1>
            </div>
            <div style="text-align: right; padding-right: 10px;">
                <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
                <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
            </div>
            <div align="left">
                <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
            </div>
        </div>
        <div class="ClearBoth">
        </div>
        <h4 class="SubTitle">PRODUCT GRID SETTINGS</h4>
        <div class="FieldStyle">
            Number of Product Columns <span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtMaxCatalogDisplayColumns" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtMaxCatalogDisplayColumns"
                ErrorMessage="* Enter Max Catalog Display Columns" CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtMaxCatalogDisplayColumns"
                CssClass="Error" Display="Dynamic" ErrorMessage="* Enter a number between 1-10"
                MaximumValue="10" MinimumValue="1" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
            Number of Thumbnail Columns<span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtMaxSmallThumbnailsDisplay" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                ID="RequiredFieldValidator17" runat="server" ControlToValidate="txtMaxSmallThumbnailsDisplay"
                CssClass="Error" Display="dynamic" ErrorMessage="* Enter Max Small Thumbnail Image Width"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator8" runat="server" ControlToValidate="txtMaxSmallThumbnailsDisplay"
                CssClass="Error" Display="dynamic" ErrorMessage="Enter a whole number" MaximumValue="1000"
                MinimumValue="1" SetFocusOnError="true" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
            Product Display Order
        </div>
        <div class="ValueStyle">
            <asp:CheckBox ID="chkDynamicDisplayOrder" runat="server" Checked="false" Text=""></asp:CheckBox>
            Display popular products on top of the list
        </div>

        <h4 class="SubTitle">AUTO IMAGE RESIZE SETTINGS</h4>
        <div class="FieldStyle">
            Large Image<span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtMaxCatalogItemLargeWidth" runat="server"></asp:TextBox>&nbsp;pixels&nbsp;<asp:RequiredFieldValidator
                ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtMaxCatalogItemLargeWidth"
                ErrorMessage="* Enter Max Large Image Width" CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator4" runat="server" ControlToValidate="txtMaxCatalogItemLargeWidth"
                CssClass="Error" Display="Dynamic" ErrorMessage="* Enter a number between 1-1000"
                MaximumValue="1000" MinimumValue="1" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
            Medium Image<span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtMaxCatalogItemMediumWidth" runat="server"></asp:TextBox>&nbsp;pixels&nbsp;<asp:RequiredFieldValidator
                ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtMaxCatalogItemMediumWidth"
                ErrorMessage="* Enter Max Medium Image Width" CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="txtMaxCatalogItemMediumWidth"
                CssClass="Error" Display="Dynamic" ErrorMessage="* Enter a number between 1-1000"
                MaximumValue="1000" MinimumValue="1" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
            Small Image<span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtMaxCatalogItemSmallWidth" runat="server"></asp:TextBox>&nbsp;pixels&nbsp;<asp:RequiredFieldValidator
                ID="RequiredFieldValidator16" runat="server" ControlToValidate="txtMaxCatalogItemSmallWidth"
                ErrorMessage="* Enter Max Small Image Width" CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="txtMaxCatalogItemSmallWidth"
                CssClass="Error" Display="dynamic" ErrorMessage="*Enter a whole number" MaximumValue="1000"
                MinimumValue="1" SetFocusOnError="true" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
            Cross-Sell Image<span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtMaxCatalogItemCrossSellWidth" runat="server"></asp:TextBox>&nbsp;pixels&nbsp;<asp:RequiredFieldValidator
                ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtMaxCatalogItemSmallWidth"
                ErrorMessage="* Enter Max Small Image Width" CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator12" runat="server" ControlToValidate="txtMaxCatalogItemCrossSellWidth"
                CssClass="Error" Display="dynamic" ErrorMessage="*Enter a whole number" MaximumValue="1000"
                MinimumValue="1" SetFocusOnError="true" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
            Thumbnail Image<span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtMaxCatalogItemThumbnailWidth" runat="server"></asp:TextBox>&nbsp;pixels&nbsp;<asp:RequiredFieldValidator
                ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtMaxCatalogItemThumbnailWidth"
                ErrorMessage="* Enter Max Thumbnail Image Width" CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator7" runat="server" ControlToValidate="txtMaxCatalogItemThumbnailWidth"
                CssClass="Error" Display="dynamic" ErrorMessage="Enter a whole number." MaximumValue="1000"
                MinimumValue="1" SetFocusOnError="true" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
            Small Thumbnail Image<span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtMaxCatalogItemSmallThumbnailWidth" runat="server"></asp:TextBox>&nbsp;pixels&nbsp;<asp:RequiredFieldValidator
                ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtMaxCatalogItemSmallWidth"
                ErrorMessage="* Enter Max Small Image Width" CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="txtMaxCatalogItemSmallThumbnailWidth"
                CssClass="Error" Display="dynamic" ErrorMessage="*Enter a whole number" MaximumValue="1000"
                MinimumValue="1" SetFocusOnError="true" Type="Integer"></asp:RangeValidator>
        </div>

        <h4 class="SubTitle">DEFAULT PRODUCT IMAGE</h4>
        <small>Upload a default image to display if a product image is not available. Only JPG, GIF and PNG images are supported.
                The file size should be less than 1.5 Meg. Your image will automatically be scaled
                so it displays correctly in the catalog.</small>
        <div id="tblShowImage" runat="server" visible="true" style="margin: 10px 0px 10px 0px;">
            <div class="LeftFloat" style="width: 300px; border-right: solid 1px #cccccc;" runat="server" id="pnlImage">
                <asp:Image ID="Image1" runat="server" />
            </div>
            <div class="StoreLeftFloat" style="width: 400px; margin: 10px;" runat="server" id="pnlUploadSection">
                <asp:Panel runat="server" ID="pnlShowImage">
                    <div class="FieldStyle" style="margin-bottom:4px;">
                        Select an Option
                    </div>
                    <div class="ClearBoth"></div>
                    <div>
                        <asp:RadioButton ID="RadioProductCurrentImage" Text="Keep Current Image" runat="server"
                            GroupName="Department Image" AutoPostBack="True" OnCheckedChanged="RadioProductCurrentImage_CheckedChanged"
                            Checked="True" /><br />
                        <asp:RadioButton ID="RadioProductNewImage" Text="Upload New Image" runat="server"
                            GroupName="Department Image" AutoPostBack="True" OnCheckedChanged="RadioProductNewImage_CheckedChanged" />
                    </div>
                </asp:Panel>
                <br />
                <div id="tblProductDescription" runat="server" visible="false">
                    <div class="FieldStyle">
                        Select an Image:<br />
                        <ZNode:UploadImage ID="uxProductImage" runat="server"></ZNode:UploadImage>
                    </div>
                </div>
                <asp:Panel ID="pnlImageName" runat="server" Visible="false">
                    <div class="FieldStyle">
                        Image File Name
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtimagename" runat="server"></asp:TextBox>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div class="ClearBoth">
            <br />
        </div>
        <div>
            <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>
