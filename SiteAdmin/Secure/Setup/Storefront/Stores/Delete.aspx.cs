using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Setup.Storefront.Stores
{
    /// <summary>
    /// Represents the SiteAdmin  Admin_Secure_settings_Stores_Delete class
    /// </summary>
    public partial class Delete : System.Web.UI.Page
    {
        #region Private Member Variables
        private int ItemId;
        private string _StoreName = string.Empty;
        private string CancelLink = "~/SiteAdmin/Secure/Setup/Storefront/Stores/Default.aspx";
        private StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
        #endregion

        /// <summary>
        /// Gets or sets the store name.
        /// </summary>
        public string StoreName
        {
            get { return _StoreName; }
            set { _StoreName = value; }
        }


        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring   
            if (Request.Params["Itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["Itemid"].ToString());
            }
            else
            {
                this.ItemId = 0;
            }

            if (ZNodeConfigManager.SiteConfig.PortalID == this.ItemId)
            {
                Response.Redirect(this.CancelLink);
            }

            if (!Page.IsPostBack)
            {
                if (this.ItemId > 0)
                {
                    Portal portal = this.storeAdmin.GetByPortalId(this.ItemId);
                    this.StoreName = portal.StoreName;
                }
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Delete Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            // Remove the store 
            if (ZNodeConfigManager.SiteConfig.PortalID == this.ItemId)
            {
                lblErrorMessage.Text = "The store can not be deleted until all associated items are removed.";
                return;
            }

            bool check = this.storeAdmin.DeleteByPortalId(this.ItemId);

            if (check)
            {
                // Remove custom messages
                this.storeAdmin.DeleteCustomMessage(this.ItemId.ToString());

                Response.Redirect(this.CancelLink);
            }
            else
            {
                Portal portal = this.storeAdmin.GetByPortalId(this.ItemId);
                this.StoreName = portal.StoreName;
                lblErrorMessage.Text = "The store can not be deleted until all associated items are removed.";
            }
        }

        /// <summary>
        /// Cancel Button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.CancelLink);
        }
        #endregion
    }
}