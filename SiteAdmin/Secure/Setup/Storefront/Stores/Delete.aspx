<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" Title="Manage Stores - Delete" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Setup.Storefront.Stores.Delete" CodeBehind="Delete.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <h5>Please Confirm</h5>
    <ZNode:DemoMode ID="DemoMode1" runat="server"></ZNode:DemoMode>
    <p>
        Please confirm if you want to delete the store named <b>
            <%=StoreName%></b>. This change cannot be undone.
    </p>
    <p>
        This includes the following items for this store:
    </p>
    <ul>
        <li>Orders</li>
        <li>Account Payments</li>
        <li>Tax Rule Associations</li>
        <li>Store Tracking Events</li>
        <li>Case Requests</li>
        <li>Content Pages</li>
        <li>Store Information</li>
    </ul>
    <asp:Label CssClass="Error" ID="lblErrorMessage" runat="server" Text=''></asp:Label>
    <div>
        <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></ZNode:Spacer>
    </div>
    <div>
        <asp:ImageButton ID="btnDelete" onmouseover="this.src='../../../../Themes/Images/buttons/button_delete_highlight.gif';"
            onmouseout="this.src='../../../../Themes/Images/buttons/button_delete.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_delete.gif"
            runat="server" AlternateText="Delete" OnClick="BtnDelete_Click" CausesValidation="true" />
        <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
            onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
            runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
    </div>
    <div>
        <ZNode:Spacer ID="Spacer1" SpacerHeight="25" SpacerWidth="10" runat="server"></ZNode:Spacer>
    </div>
</asp:Content>
