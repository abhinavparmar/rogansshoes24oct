<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Setup.Storefront.Categories.Add" Title="Manage Catagories - Add" ValidateRequest="false" CodeBehind="Add.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">

    <div class="FormView">
        <div>
            <div class="LeftFloat" style="width: 70%; text-align: left">
                <h1>
                    <asp:Label ID="lblTitle" runat="server"></asp:Label>
                    <uc2:DemoMode ID="DemoMode1" runat="server" />
                </h1>
            </div>
            <div style="text-align: right">
                <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
                <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
            </div>
            <div style="clear: both">
                <asp:Label ID="lblMsg" CssClass="Error" runat="server"></asp:Label>
            </div>
            <div>
                <uc1:Spacer ID="Spacer8" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
            <h4 class="SubTitle">General Settings</h4>
            <div class="FieldStyle">
                Category Name<span class="Asterix">*</span>
                <br />
                <small>Displayed in the navigation menu.</small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtName" runat="server" MaxLength="50" Columns="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                    Display="Dynamic" ErrorMessage="* Enter Category name"
                    CssClass="Error"></asp:RequiredFieldValidator>
            </div>
            <div class="FieldStyle">
                Category Title<span class="Asterix">*</span>
                <br />
                <small>Displayed as the category page title.</small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID='txtTitle' runat='server' MaxLength="255" Columns="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvTitle" runat="server" ControlToValidate="txtTitle"
                    Display="Dynamic" ErrorMessage="* Enter Category Title"
                    CssClass="Error"></asp:RequiredFieldValidator>
            </div>
            <h4 class="SubTitle">Display Settings</h4>
            <div class="FieldStyle">
                Display Order<span class="Asterix">*</span>
                <br />
                <small>Categories with a lower display order are displayed first on the navigation menu.</small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="DisplayOrder" runat="server" MaxLength="9" Columns="9" Text="1"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" Display="Dynamic"
                    ErrorMessage="* Enter a Display Order" ControlToValidate="DisplayOrder"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="DisplayOrder"
                    Display="Dynamic" ErrorMessage="Enter a whole number." MaximumValue="999999999"
                    MinimumValue="1" Type="Integer"></asp:RangeValidator>
            </div>
            <div style="display: none">
                <div class="FieldStyle">
                    Enable this Category
                </div>
                <div class="ValueStyle">
                    <asp:CheckBox ID='VisibleInd' runat='server' Checked="true" Text="Check this box if this category should be displayed on the navigation menu."></asp:CheckBox>
                </div>
            </div>
            <div class="FieldStyle">
                Child Categories
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID='chkSubCategoryGridVisibleInd' runat='server' Checked="true" Text="Display child categories on the category page"></asp:CheckBox>
            </div>
            <%-- Perficient Custom Code:Starts--%>
            <div class="FieldStyle">
                Show in Navigation
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID='chkHideCategory' runat='server' Checked="true" Text="Display in navigation"></asp:CheckBox>
            </div>
            <div class="FieldStyle">
                Follow Index
                <br />
                <small>Disabling this will add "NOINDEX, FOLLOW" in robot meta tag inspite of "INDEX, FOLLOW"  in page source.<br />
                    Will stop page indexing.</small>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID='chkIndFollowed' runat='server' Checked="true" Text="Follow Index"></asp:CheckBox>
            </div>
            <div id="dvShowSubCategory" runat="server">
                <div class="FieldStyle">
                    Show SubCategory List
                     <br />
                    <small>Enable this to show category listing page and disable this to show product listing page.</small>
                </div>
                <div class="ValueStyle">
                    <asp:CheckBox ID='chkShowSubCategory' runat='server' Checked="false" Text="Show SubCategory List"></asp:CheckBox>
                </div>
            </div>
            <%-- Perficient Custom Code:Ends--%>
            <h4 class="SubTitle">Category Image</h4>
            <small>Upload a suitable image for your category. Only JPG, GIF and PNG images are
                supported. The file size should be less than 1.5 Meg. Your image will automatically
                be scaled so it displays correctly in the catalog.</small>

            <div id="tblShowImage" runat="server" visible="false" style="margin: 10px 0px 10px 0px;">
                <div class="LeftFloat" style="width: 300px; border-right: solid 1px #cccccc;" runat="server" id="pnlImage">
                    <asp:Image ID="Image1" runat="server" />
                </div>
                <div class="LeftFloat" style="margin-left: -1px;" runat="server" id="pnlImageUpload">
                    <asp:Panel runat="server" ID="pnlImageOption">
                        <div>
                            Select an Option
                        </div>
                        <div>
                            <asp:RadioButton ID="RadioCategoryCurrentImage" Text="Keep Current Image" runat="server"
                                GroupName="Category Image" AutoPostBack="True" OnCheckedChanged="RadioCategoryCurrentImage_CheckedChanged"
                                Checked="True" /><br />
                            <asp:RadioButton ID="RadioCategoryNewImage" Text="Upload New Image" runat="server"
                                GroupName="Category Image" AutoPostBack="True" OnCheckedChanged="RadioCategoryNewImage_CheckedChanged" />
                        </div>
                    </asp:Panel>
                    <br />
                    <div id="tblCategoryDescription" runat="server" visible="false">
                        <div class="FieldStyle">
                            Select Image<br />
                            <small>Image Size should be <b style="color: red;">240X312 PX</b></small>
                        </div>
                        <div class="ValueStyle">
                            <asp:FileUpload ID="UploadCategoryImage" runat="server" BorderStyle="Inset" EnableViewState="true" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="UploadCategoryImage"
                                CssClass="Error" Display="dynamic" ValidationExpression=".*(\.[Jj][Pp][Gg]|\.[Gg][Ii][Ff]|\.[Jj][Pp][Ee][Gg]|\.[Pp][Nn][Gg])"
                                ErrorMessage="Please select a valid JPEG, JPG, PNG or GIF image"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="FieldStyle">
                        Image Alt Text<br />
                        <small>The Alt Text is used for SEO and accessibility.</small>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtImageAltTag" runat="server"></asp:TextBox>
                    </div>
                    <div>
                        <asp:Label ID="lblCategoryImageError" runat="server" CssClass="Error" ForeColor="Red"
                            Text="" Visible="False"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="ClearBoth">
                <br />
            </div>
            <h4 class="SubTitle">SEO Settings</h4>

            <div class="DesignPageAdd">
                <div class="FieldStyle">
                    SEO Title
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtSEOTitle" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
                </div>
                <div class="FieldStyle">
                    SEO Keywords
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtSEOMetaKeywords" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
                </div>
                <div class="FieldStyle">
                    SEO Description
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtSEOMetaDescription" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
                </div>
                <div class="FieldStyle">
                    SEO Friendly Page Name<br />
                    <small>Use only characters a-z and 0-9. Use "-" instead of spaces. Do not use a file
                        extension or parameters in your url name.</small>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtSEOURL" runat="server" MaxLength="100" Columns="50"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtSEOURL"
                        CssClass="Error" Display="Dynamic" ErrorMessage="Enter valid SEO firendly URL"
                        SetFocusOnError="True" ValidationExpression="([A-Za-z0-9-_]+)"></asp:RegularExpressionValidator>
                </div>
                <div class="FieldStyle" style="display: none"></div>
                <div class="ValueStyleText" style="display: none">
                    <asp:CheckBox ID="chkAddURLRedirect" runat="server" Text=' Add 301 redirect on URL changes.' Height="30px" />
                </div>
            </div>
            <br />
            <h4 class="SubTitle">Description</h4>
            <div class="FieldStyle">
                Short Description
                <br />
                <small>The short description is displayed on search result listings. Enter 100 characters or less.</small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtshortdescription" runat="server" Width="300px" TextMode="MultiLine"
                    Height="75px" MaxLength="100"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                Long Description<span>
                    <br />
                    <small>The long description is dispalyed on the category page. It can include rich text and images.</small></span>
            </div>
            <div class="ValueStyle">
                <uc1:HtmlTextBox ID="ctrlHtmlText" runat="server"></uc1:HtmlTextBox>
            </div>
            <div class="FieldStyle">
                Additional Description<br />
                <small>The additional description can potentially be positioned to appear at a different section of the category page using the included templates.</small>
            </div>
            <div class="ValueStyle">
                <uc1:HtmlTextBox ID="ctrlHtmlText1" runat="server"></uc1:HtmlTextBox>
            </div>
            <div>
                <uc1:Spacer ID="Spacer9" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
            <br />
            <h4 class="SubTitle">Banner</h4>
            <div class="FieldStyle">
                Category Banner
                <br />
                <small>The banner will be displayed on respective categories.</small>
            </div>
            <div class="ValueStyle">
                <uc1:HtmlTextBox ID="ctrlCategoryBanner" runat="server"></uc1:HtmlTextBox>
            </div>
            <div>
                <uc1:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
            <div class="ClearBoth">
            </div>
            <div>
                <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
                <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
            </div>
        </div>
    </div>
</asp:Content>
