using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;

namespace SiteAdmin.Secure.Setup.Storefront.Categories
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_catalog_product_category_View class
    /// </summary>
    public partial class View : System.Web.UI.Page
    {
        #region Protected Variables
        private int ItemId;
        private int Mode = 0;
        private string AssociateName = string.Empty;
        private string AddRelatedProductLink = "Addrelatedproducts.aspx?itemid=";
        private ProductService productService = new ProductService();

        #endregion

        #region Public Methods
        /// <summary>
        /// Bind Related Products 
        /// </summary>
        public void BindRelatedProducts()
        {
            ProductCategoryAdmin prodCategoryAdmin = new ProductCategoryAdmin();
            DataSet MyDataSet = prodCategoryAdmin.GetByCategoryID(this.ItemId);
            DataView dv = MyDataSet.Tables[0].DefaultView;
            dv.Sort = "DisplayOrder";
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            // Get ItemId from querystring        
            if (Request.Params["Mode"] != null)
            {
                this.Mode = int.Parse(Request.Params["Mode"]);
            }
            else
            {
                this.Mode = 0;
            }

            if (this.Mode == 1)
            {
                tabCategory.ActiveTabIndex = 1;
            }

            if (Page.IsPostBack == false)
            {
                this.BindRelatedProducts();

                // If edit function then bind the data fields
                if (this.ItemId > 0)
                {
                    this.BindEditData();
                }
                else
                {
                    lblTitle.Text = "Add Category";
                }
            }

            this.uxManage.SubmitButtonClicked += new EventHandler(this.ModalPopClose_Click);
            this.uxManage.CancelButtonClicked += new EventHandler(this.ModalPopClose_Click);
            if (IsCustomMulitipleAttributePortal())
            {
                dvShowSubCategory.Visible = false;
            }
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the fields on the edit screen
        /// </summary>
        protected void BindEditData()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            Category category = categoryAdmin.GetByCategoryId(this.ItemId);

            if (category != null)
            {
                // Checking the user profile for roles and permission and then loading the data based on roles
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (!UserStoreAccess.CheckUserRoleInCategory(profiles, this.ItemId))
                {
                    Response.Redirect("list.aspx", true);
                }

                lblTitle.Text = "Manage Category - " + category.Name;
                lblName.Text = category.Name;
                lblshortdescription.Text = category.ShortDescription;
                lblLongDescription.Text = category.Description;
                lblAlternativeDesc.Text = category.AlternateDescription;
                lblDisplayOrder.Text = category.DisplayOrder.GetValueOrDefault(1).ToString();
                chkCategoryEnabled.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(category.VisibleInd.ToString()));
                lblCategoryTitle.Text = category.Title;
                chkDisplaySubCategory.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(category.SubCategoryGridVisibleInd.ToString()));
                lblSEOMetaDescription.Text = category.SEODescription;
                lblSEOMetaKeywords.Text = category.SEOKeywords;
                lblSEOTitle.Text = category.SEOTitle;
                lblSEOURL.Text = category.SEOURL;
                imgHideCategory.Src = string.IsNullOrWhiteSpace(category.Custom1) || category.Custom1.Equals("1") ? ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse("True")) : ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse("False"));
                ApplyCategoryBanner(category.CategoryID);

                ZNodeImage znodeImage = new ZNodeImage();
                CategoryImage.ImageUrl = znodeImage.GetImageHttpPathMedium(category.ImageFile);
            }
            else
            {
                throw new ApplicationException("The Category could not be found.");
            }
        }
        #endregion

        #region General Events

        /// <summary>
        /// Edit Category Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnEditCategory_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Setup/Storefront/Categories/Add.aspx?itemid=" + this.ItemId);
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Setup/Storefront/Categories/Default.aspx");
        }

        /// <summary>
        /// Add Related Products Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Addrelatedproducts_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddRelatedProductLink + this.ItemId);
        }

        /// <summary>
        /// Modal Pop Close Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ModalPopClose_Click(object sender, EventArgs e)
        {
            this.BindRelatedProducts();
            updPnlRelatedProductGrid.Update();
            mdlPopup.Hide();
        }

        #endregion

        #region Grid Events

        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindRelatedProducts();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "Manage")
                {
                    uxManage.ItemId = int.Parse(e.CommandArgument.ToString());
                    uxManage.BindThemeList();
                    uxManage.BindMasterPageTemplates();
                    uxManage.BindData();
                    mdlPopup.Show();
                }
                else if (e.CommandName == "RemoveItem")
                {
                    CategoryAdmin categoryAdmin = new CategoryAdmin();
                    Category category = categoryAdmin.GetByCategoryId(this.ItemId);

                    int Id = Convert.ToInt32(e.CommandArgument.ToString());

                    Product node = new Product();
                    node = this.productService.GetByProductID(Id);
                    this.productService.DeepLoad(node);
                    string productName = node.Name;

                    ProductCategoryAdmin prodCategoryAdmin = new ProductCategoryAdmin();

                    bool Check = prodCategoryAdmin.RemoveProduct(int.Parse(e.CommandArgument.ToString()), this.ItemId);
                    if (Check)
                    {
                        this.AssociateName = "Deleted the Association " + productName + " - " + category.Name;
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, category.Name);

                        this.BindRelatedProducts();
                    }
                }
            }
        }
        #endregion

        #region Zeon Custom Code
        /// <summary>
        /// Find if current portal is Mulitiple attribute customize portal
        /// </summary>
        /// <returns>bool</returns>
        public static bool IsCustomMulitipleAttributePortal()
        {
            bool result = false;
            if (ConfigurationManager.AppSettings["CheerAndPomPortalID"] != null && !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["CheerAndPomPortalID"].ToString()))
            {
                string[] portalIdArray = ConfigurationManager.AppSettings["CheerAndPomPortalID"].ToString().Split(',');

                if (portalIdArray.Length > 0)
                {
                    foreach (string portalIdStr in portalIdArray)
                    {
                        if (!string.IsNullOrWhiteSpace(portalIdStr) && ZNodeConfigManager.SiteConfig.PortalID.ToString().Equals(portalIdStr))
                        {
                            result = true;
                            break;
                        }
                    }
                }
            }
            return result;
        }
        /// <summary>
        /// Set the category banner from CategoryExtn table
        /// </summary>
        /// <param name="catID"></param>
        private void ApplyCategoryBanner(int catID)
        {
            CategoryExtnService catService = new CategoryExtnService();
            TList<CategoryExtn> categoryList = catService.GetByCategoryID(catID);

            if (categoryList != null && categoryList.Count > 0)
            {
                lblCategoryBanner.Text = categoryList[0].CategoryBanner;
                imgIndexFollow.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(categoryList[0].IsIndexFollow);
                if (!IsCustomMulitipleAttributePortal())
                {
                    if (categoryList[0].Custom1 != null)
                    {
                        imgSubCatgory.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(Convert.ToBoolean(categoryList[0].Custom1));
                    }
                }
            }
        }

        #endregion
    }
}