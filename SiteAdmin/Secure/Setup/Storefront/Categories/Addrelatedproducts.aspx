<%@ Page Language="C#" AutoEventWireup="True" Title="Manage Categories - Add Related Products" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" ValidateRequest="false" Inherits="SiteAdmin.Secure.Setup.Storefront.Categories.Addrelatedproducts" CodeBehind="Addrelatedproducts.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">

    <script type="text/javascript">

        function AutoCompleteSelected_Category(source, eventArgs) {

        }

        function AutoCompleteSelected_Brands(source, eventArgs) {

        }

        function AutoCompleteSelected_ProductType(source, eventArgs) {
            var hiddenTextValue = $get("<%= ProductTypeId.ClientID %>");
            hiddenTextValue.value = eventArgs.get_value();
        }

    </script>

    <div class="Form">
        <div>
            <div class="LeftFloat" style="width: 50%">
                <h1 style="width: 562px;">Search and Add Products to Category -
                    <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
                <ZNode:DemoMode ID="DemoMode1" runat="server"></ZNode:DemoMode>
            </div>
            <div class="LeftFloat ImageButtons" style="width: 49%" align="right">
                <asp:Button CssClass="Size175" ID="btnBack" CausesValidation="False" Text="<< Back to List Page"
                    runat="server" OnClick="BtnBack_Click"></asp:Button>
            </div>
        </div>
        <div class="ClearBoth" align="left">
            <br />
        </div>
        <!-- Search product panel -->
        <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
            <h4 class="SubTitle">Search for Products to Add</h4>
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">Product Name</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtproductname" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">Product Number</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtproductnumber" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">SKU</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtsku" runat="server"></asp:TextBox></span>
                    </div>
                </div>
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">Brands</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtManufacturer" runat="server"></asp:TextBox></span>
                        <!-- Auto Complete Extender -->
                        <ajaxToolKit:AutoCompleteExtender ID="autoCompleteExtender1" runat="server" TargetControlID="txtManufacturer"
                            ServicePath="~/SiteAdmin/Controls/Default/ZNodeMultifrontService.asmx" ServiceMethod="GetManufacturer" UseContextKey="true"
                            MinimumPrefixLength="1" EnableCaching="false" CompletionSetCount="10" CompletionInterval="1"
                            FirstRowSelected="false" OnClientItemSelected="AutoCompleteSelected_Brands" />
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">Product Type</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtProductType" runat="server"></asp:TextBox>
                            <asp:HiddenField runat="server" ID="ProductTypeId" />
                        </span>
                        <ajaxToolKit:AutoCompleteExtender ID="autoCompleteExtender2" runat="server" TargetControlID="txtProductType"
                            ServicePath="~/SiteAdmin/Controls/Default/ZNodeMultifrontService.asmx" ServiceMethod="GetProductTypes" UseContextKey="true"
                            MinimumPrefixLength="1" EnableCaching="false" CompletionSetCount="10" CompletionInterval="1"
                            FirstRowSelected="false" OnClientItemSelected="AutoCompleteSelected_ProductType" />
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">Category</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtCategory" runat="server"></asp:TextBox></span>
                        <!-- Auto Complete Extender -->
                        <ajaxToolKit:AutoCompleteExtender ID="autoCompleteExtender" runat="server" TargetControlID="txtCategory"
                            ServicePath="~/SiteAdmin/Controls/Default/ZNodeMultifrontService.asmx" ServiceMethod="GetCategory" UseContextKey="true"
                            MinimumPrefixLength="1" EnableCaching="false" CompletionSetCount="10" CompletionInterval="1"
                            FirstRowSelected="false" OnClientItemSelected="AutoCompleteSelected_Category" />
                    </div>
                </div>
                <div class="ClearBoth">
                </div>
                <div>
                    <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_clear_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                        runat="server" AlternateText="Clear" OnClick="BtnClear_Click" />
                    <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                        runat="server" AlternateText="Search" OnClick="BtnSearch_Click" />
                </div>
            </div>
        </asp:Panel>
        <br />
        <!-- Product List -->
        <asp:Panel ID="pnlProductList" runat="server" Visible="false">
            <h4 class="GridTitle">Product List               
            </h4>
            <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" CaptionAlign="Left" AutoGenerateColumns="False"
                GridLines="None" AllowPaging="True" PageSize="10" OnPageIndexChanging="UxGrid_PageIndexChanging"
                EmptyDataText="No Products exist in the database." Width="100%" CellPadding="4" OnRowDataBound="UxGrid_RowDataBound">
                <Columns>
                    <asp:TemplateField HeaderText="Select" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkProduct" runat="server" Enabled='<%# !(int.Parse(DataBinder.Eval(Container.DataItem, "ProductId").ToString()) == ItemId) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ProductId" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText="Image" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img id="prodImage" alt=" " src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>'
                                runat="server" style="border: none" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Name" HeaderText="Name" HtmlEncode="false" HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText="Is Active" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img alt="" id="chMark" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
            <div>
                <ZNode:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div>
                <asp:Label ID="lblError" runat="server" Text="Label" Visible="false" CssClass="Error"
                    ForeColor="red"></asp:Label>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div>
                <div class="LeftFloat ImageButtons" style="width: 185px;">
                    <asp:Button CssClass="Size175" ID="butAddNew" CausesValidation="true" Text="ADD SELECTED PRODUCTS" runat="server" OnClick="Submit_Click"></asp:Button>
                </div>
                <div>
                    <asp:ImageButton ID="Cancel" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                        runat="server" AlternateText="Cancel" OnClick="Cancel_Click" />
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
