<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" Title="Manage Categories - List" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Setup.Storefront.Categories.Default" ValidateRequest="false" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
    <div class="Form" align="center">
        <div>
            <div class="LeftFloat" style="width: auto; text-align: left">
                <h1>Categories
                 <%--<div class="tooltip" >
                        <a href="javascript:void(0);" class="learn-more" ><span>
                            <asp:Localize ID="Localize2" runat="server"></asp:Localize></span></a>
                        <div class="content">
                            <h6>
                                Help</h6>
                            <p>
                          Example of Categories are Gifts, Clothing, Jewelry and etc...
                            </p>
                        </div>
              </div>--%>
                </h1>
                <p>
                    Categories are hierarchical groupings of products in your catalog (example: Apparel, Electronics, etc).
                     </p>
                    <p style ="color: red">Note: After adding or deleting a category a manual re-index is required.</p>
               
            </div>
            <div class="ButtonStyle">
                <zn:LinkButton ID="btnAddCategory" runat="server"
                    ButtonType="Button" OnClick="BtnAddCategory_Click" CausesValidation="False" Text="Add New Category"
                    ButtonPriority="Primary" />
            </div>
            <div align="left" class="SearchForm">
                <asp:Panel ID="Test" runat="server" DefaultButton="btnSearch">
                    <h4 class="SubTitle">Search Categories</h4>
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle">CATEGORY NAME</span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtCategoryName" runat="server"></asp:TextBox></span>
                        </div>

                        <div class="ItemStyle">
                            <span class="FieldStyle">Catalog</span><br />
                            <span class="ValueStyle">
                                <asp:DropDownList ID="ddlCatalog" runat="server" EnableViewState="true"></asp:DropDownList></span>
                        </div>
                    </div>
                    <div class="ClearBoth">
                    </div>
                    <div>
                        <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_clear_highlight.gif';"
                            onmouseout="this.src='../../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                            runat="server" AlternateText="Clear" OnClick="BtnClear_Click" />
                        <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                            onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                            runat="server" AlternateText="Search" OnClick="BtnSearch_Click" />
                    </div>
                </asp:Panel>
            </div>
            <uc1:Spacer ID="Spacer3" runat="server" SpacerHeight="30" />
            <div align="left">
                <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
                <h4 class="GridTitle">Category List</h4>
                <div align="center">
                    <asp:GridView ID="uxGrid" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        CaptionAlign="Left" CellPadding="4" CssClass="Grid" EmptyDataText="No categories exist in the database."
                        EnableSortingAndPagingCallbacks="False" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
                        OnRowCommand="UxGrid_RowCommand" OnRowDeleting="UxGrid_RowDeleting" PageSize="25"
                        Width="100%">
                        <Columns>
                            <asp:BoundField DataField="CategoryID" HeaderStyle-HorizontalAlign="Left" HeaderText="ID" />
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Name">
                                <ItemTemplate>
                                    <a href='<%# "View.aspx?itemid=" + DataBinder.Eval(Container.DataItem,"CategoryId").ToString()%>'>
                                        <%# DataBinder.Eval(Container.DataItem, "Name").ToString()%></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="CatalogName" HtmlEncode="false" HeaderStyle-HorizontalAlign="Left" HeaderText="Catalog Name" />
                            <asp:BoundField DataField="DisplayOrder" HeaderStyle-HorizontalAlign="Left" HeaderText="Display Order" />
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Enabled" Visible="false">
                                <ItemTemplate>
                                    <img id="Img1" runat="server" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "VisibleInd").ToString()))%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ACTIONS" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <div class="LeftFloat" style="width: 40%; text-align: left">
                                        <asp:LinkButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("CategoryID") %>'
                                            CommandName="Manage" Text="EDIT &raquo" CssClass="LinkButton" />
                                    </div>
                                    <div class="LeftFloat" style="width: 50%; text-align: left">
                                        <asp:LinkButton ID="btnDelete" runat="server" CommandArgument='<%# Eval("CategoryID") %>'
                                            CommandName="Delete" Text="DELETE &raquo " CssClass="LinkButton" />
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle CssClass="FooterStyle" />
                        <RowStyle CssClass="RowStyle" />
                        <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
                        <HeaderStyle CssClass="HeaderStyle" />
                        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                        <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                    </asp:GridView>
                </div>
            </div>
            <div>
                <uc1:Spacer ID="Spacer2" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
