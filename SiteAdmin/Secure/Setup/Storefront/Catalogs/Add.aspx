﻿<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="SiteAdmin.Secure.Setup.Storefront.Catalogs.Add" Title="Manage Catalogs - Add" CodeBehind="Add.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <asp:Panel ID="pnlCatalog" runat="server">
        <div>
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div>
            <div class="LeftFloat ImageButtons" style="width: 100%" align="right">
                <asp:Button CssClass="Size75" ID="btnBack" CausesValidation="False" Text="<< Back"
                    runat="server" OnClick="BtnBack_Click"></asp:Button>
            </div>
        </div>
        <div class="ClearBoth">
        </div>
        <ajaxToolKit:TabContainer ID="tabCatalogSettings" runat="server"
            ActiveTabIndex="0">
            <ajaxToolKit:TabPanel ID="pnlSettings" runat="server">
                <HeaderTemplate>
                    Settings
                </HeaderTemplate>
                <ContentTemplate>
                    <asp:Panel ID="pnlButtons" runat="server" DefaultButton="btnSubmit">
                        <div class="FieldStyle">
                            Catalog Name<span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtcatalogName" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                                ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtcatalogName"
                                ErrorMessage="* Enter Catalog Name" CssClass="Error" Display="dynamic" ValidationGroup="grpTitle"></asp:RequiredFieldValidator>
                        </div>
                        <div>
                            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
                        </div>
                        <div>
                            <asp:Label ID="lblCategoryError" runat="server" CssClass="Error" Text="* Select at least one root node for this Category"
                                Visible="False" />
                        </div>
                        <br />
                        <div>

                            <asp:ImageButton ID="btnSubmit" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click"
                                CausesValidation="true" ValidationGroup="grpTitle" />
                            <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />

                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="pnlDepartment" runat="server" Visible="false">
                <HeaderTemplate>
                    Categories
                </HeaderTemplate>
                <ContentTemplate>
                    <asp:Panel ID="pnlCategories" runat="server">
                        <div>
                            <div class="ButtonStyle" style="text-transform: none;">
                                <zn:LinkButton ID="btnAddCategory" runat="server" CausesValidation="False"
                                    ButtonType="Button" OnClick="BtnAddCategory_Click" Text="Associate a Category"
                                    ButtonPriority="Primary" />
                            </div>
                        </div>
                        <div class="ClearBoth">
                            <br />
                            <p>Associate specific categories with your catalog. Products added to those categories will then automatically get included in this catalog.</p>
                            <p style="color: red">Note: After enabling, disabling or deleting a category a manual re-index is required.</p>
                        </div>
                        <div class="SearchForm">
                            <h4 class="SubTitle">Search Categories</h4>
                            <asp:Panel ID="pnlsearch" DefaultButton="btnSearch" runat="server">
                                <div class="RowStyle">
                                    <div class="ItemStyle">
                                        <span class="FieldStyle">Category Name</span><br />
                                        <span class="ValueStyle">
                                            <asp:TextBox ID="txtCategoryName" runat="server"></asp:TextBox></span>
                                    </div>
                                </div>
                                <div class="ClearBoth">
                                </div>
                                <div>
                                    <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_clear_highlight.gif';"
                                        onmouseout="this.src='../../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                                        runat="server" AlternateText="Clear" OnClick="BtnClear_Click" />
                                    <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                                        onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                                        runat="server" AlternateText="Search" OnClick="BtnSearch_Click" />
                                </div>
                            </asp:Panel>
                        </div>
                        <br />
                        <h4 class="GridTitle">Category List</h4>
                        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
                            CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
                            CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" EnableSortingAndPagingCallbacks="False"
                            PageSize="25" OnRowDeleting="UxGrid_RowDeleting" AllowSorting="True" EmptyDataText="No categories are associated with this catalog.">
                            <Columns>
                                <asp:BoundField DataField="CategoryID" HeaderText="ID" HeaderStyle-HorizontalAlign="left" />
                                <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText="Name" HeaderStyle-HorizontalAlign="left" />
                                <asp:BoundField DataField="DisplayOrder" HeaderText="Display Order" HeaderStyle-HorizontalAlign="left" />
                                <asp:TemplateField HeaderText="Enabled" HeaderStyle-HorizontalAlign="left">
                                    <ItemTemplate>
                                        <img alt="" id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                                            runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEdit" CssClass="actionlink" runat="server" CommandName="Edit"
                                            Text="Edit &raquo" CommandArgument='<%# Eval("CategoryNodeID") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnDelete" CssClass="actionlink" runat="server" CommandName="Delete" OnClientClick='return confirm("Are you sure do you want to delete the Associated Category ?")'
                                            Text="Delete &raquo" CommandArgument='<%# Eval("CategoryNodeID") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle CssClass="FooterStyle" />
                            <RowStyle CssClass="RowStyle" />
                            <PagerStyle CssClass="PagerStyle" />
                            <HeaderStyle CssClass="HeaderStyle" />
                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                        </asp:GridView>
                        <div>
                            <ZNode:Spacer ID="Spacer22" SpacerHeight="10" SpacerWidth="10" runat="server"></ZNode:Spacer>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
        </ajaxToolKit:TabContainer>


    </asp:Panel>
</asp:Content>
