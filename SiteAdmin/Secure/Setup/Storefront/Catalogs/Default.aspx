<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" Title="Manage Catalogs - List" Inherits="SiteAdmin.Secure.Setup.Storefront.Catalogs.Default" ValidateRequest="false" CodeBehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <div class="Form">
        <div>
            <div class="LeftFloat" style="width: 50%">
                <h1>Catalogs</h1>
            </div>
            <div class="ButtonStyle">
                <zn:LinkButton ID="btnAddcatalog" runat="server"
                    ButtonType="Button" OnClick="BtnAddcatalog_Click" Text="Add New Catalog"
                    ButtonPriority="Primary" />
            </div>
        </div>
        <div class="ClearBoth" align="left">
            <p>Catalogs are groupings of selected categories that you want to display in your store (example: Summer Catalog).</p>
            <br />
        </div>
        <h4 class="GridTitle">Catalogs List</h4>
        <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" CaptionAlign="Left" AutoGenerateColumns="False"
            GridLines="None" AllowPaging="True" PageSize="10" OnPageIndexChanging="UxGrid_PageIndexChanging"
            OnRowCommand="UxGrid_RowCommand" EmptyDataText="No Catalogs exist in the database."
            Width="100%" CellPadding="4">
            <Columns>
                <asp:BoundField DataField="CatalogID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText="Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="50%" />
                <asp:TemplateField HeaderText="Is Active" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img alt="" id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "IsActive").ToString()))%>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Action" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <div class="LeftFloat" style="width: 25%; text-align: left">
                            <asp:LinkButton ID="Button1" CommandName="Manage" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"catalogID")%>'
                                Text="Manage &raquo" runat="server" CssClass="actionlink" />
                        </div>
                        <div class="LeftFloat" style="width: 25%; text-align: left">
                            <asp:LinkButton ID="Button5" CommandName="RemoveItem"
                                CommandArgument='<%#DataBinder.Eval(Container.DataItem,"catalogID")%>' Text="Delete &raquo" Visible='<%# HideDeleteButton(Eval("catalogID").ToString()) %>'
                                runat="server" CssClass="actionlink" />
                        </div>
                        <div class="LeftFloat" style="width: 25%; text-align: left">
                            <asp:LinkButton ID="lnkbtnCopy" CommandName="Copy"
                                CommandArgument='<%#DataBinder.Eval(Container.DataItem,"catalogID")%>' Text="Copy &raquo"
                                runat="server" CssClass="actionlink" />
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
    </div>
</asp:Content>
