using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Setup.Storefront.Catalogs
{
    /// <summary>
    /// Represents the SiteAdmin Admin_Secure_catalog_catalogs_SearchDepartment user control class
    /// </summary>
    public partial class SearchDepartment : System.Web.UI.UserControl
    {
        #region Public Methods
        /// <summary>
        /// Bind data to grid
        /// </summary>
        public void BindGridData()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            DataSet ds = categoryAdmin.GetCategoriesBySearchData(Server.HtmlEncode(txtCategoryName.Text.Trim()), ddlCatalog.SelectedValue);
            DataView dv = new DataView(ds.Tables[0]);
            dv.Sort = "Name";
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindCatalogData();
                this.BindGridData();
            }

            string script = @"<script>function Close(var1, var2){";

            if (Request.Params["sourceId"] != null && Request.Params["source"] != null)
            {
                script += "window.opener.document.getElementById('" + Request.Params["sourceId"].ToString() + "').value = var1;";
                script += "var control = window.opener.document.getElementById('" + Request.Params["source"].ToString() + "'); " + Environment.NewLine +
                            "if(control != null && control.type == \"text\") " + Environment.NewLine +
                            "{ " + Environment.NewLine +
                            "   //control is textbox " + Environment.NewLine +
                            "   control.value = unescape(var2); " + Environment.NewLine +
                            "} " + Environment.NewLine +
                            "else if(control != null && control.type == \"select-one\") " + Environment.NewLine +
                            "{ " + Environment.NewLine +
                            "   //control is dropdownlist  " + Environment.NewLine +
                            "   control.options.selectedIndex = control.options.indexOf(var1);" + Environment.NewLine +
                            "} ";
            }
            
            script += "window.close();}</script>";

            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "ClosePopup", script);
        }
        #endregion

        #region Bind Grid

        /// <summary>
        /// Bind Search data
        /// </summary>
        protected void BindSearchData()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            DataSet ds = categoryAdmin.GetCategoriesBySearchData(Server.HtmlEncode(txtCategoryName.Text.Trim()), ddlCatalog.SelectedValue);
            DataView dv = new DataView(ds.Tables[0]);
            dv.Sort = "Name";
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Bind Store Catalogs
        /// </summary>
        protected void BindCatalogData()
        {
            CatalogAdmin catalogAdmin = new CatalogAdmin();
            TList<Catalog> catalogList = catalogAdmin.GetAllCatalogs();
            
            foreach (Catalog catalog in catalogList)
            {
                catalog.Name = Server.HtmlDecode(catalog.Name);
            }
            
            ddlCatalog.DataSource = catalogList;
            ddlCatalog.DataTextField = "Name";
            ddlCatalog.DataValueField = "CatalogID";
            ddlCatalog.DataBind();
            ListItem item = new ListItem("ALL", "0");
            ddlCatalog.Items.Insert(0, item);
            ddlCatalog.SelectedIndex = 0;
        }

        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchData();
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            txtCategoryName.Text = string.Empty;
            ddlCatalog.SelectedValue = "0";
            this.BindGridData();
        }

        /// <summary>
        /// Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindSearchData();
        }
        
        /// <summary>
        /// Get Name Method
        /// </summary>
        /// <param name="name">The value of Name</param>
        /// <returns>Returns the Name</returns>
        protected string GetName(object name)
        {
            string val = string.Empty;

            if (name != null)
            {
                val = name.ToString().Replace("'", "\\'");
            }

            return val;
        }
        #endregion
    }
}