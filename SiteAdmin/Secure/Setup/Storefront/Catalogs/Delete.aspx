<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" Title="Manage Catalogs - Delete" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Setup.Storefront.Catalogs.Delete" CodeBehind="Delete.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <h5>Please Confirm</h5>
    <ZNode:DemoMode ID="DemoMode1" runat="server"></ZNode:DemoMode>
    <p>
        Please confirm if you want to delete this catalog named <b>
            <%=CatalogName%></b>.
    </p>
    <p style="font-weight: bold; color: Red">WARNING: This change cannot be undone.</p>
    <asp:Label CssClass="Error" ID="lblErrorMessage" runat="server" Text=''></asp:Label>
    <div>
        <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></ZNode:Spacer>
    </div>
    <div>
        Select Option:
        <asp:RadioButtonList runat="server" ID="rdoPreserve">
            <asp:ListItem Value="true" Selected='True'>Delete Catalog only and Preserve Categories and Products</asp:ListItem>
            <asp:ListItem Value="false">Delete Associated Categories and Products</asp:ListItem>
        </asp:RadioButtonList>
    </div>
    <div>
        <ZNode:Spacer ID="Spacer3" SpacerHeight="20" SpacerWidth="10" runat="server"></ZNode:Spacer>
    </div>
    <div>
        <asp:ImageButton ID="btnDelete" onmouseover="this.src='../../../../Themes/Images/buttons/button_delete_highlight.gif';"
            onmouseout="this.src='../../../../Themes/Images/buttons/button_delete.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_delete.gif"
            runat="server" AlternateText="Submit" OnClick="BtnDelete_Click" CausesValidation="true" />
        <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
            onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
            runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
    </div>
    <div>
        <ZNode:Spacer ID="Spacer1" SpacerHeight="25" SpacerWidth="10" runat="server"></ZNode:Spacer>
    </div>
</asp:Content>
