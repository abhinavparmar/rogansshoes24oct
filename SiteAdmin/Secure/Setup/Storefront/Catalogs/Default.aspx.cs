using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Setup.Storefront.Catalogs
{
    /// <summary>
    /// Represents the SiteAdmin Admin_Secure_catalog_catalogs_list class.
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region protected member variables
        private string editlink = "~/SiteAdmin/Secure/Setup/Storefront/Catalogs/add.aspx?ItemId=";
        private string deletelink = "~/SiteAdmin/Secure/Setup/Storefront/Catalogs/Delete.aspx?ItemId=";
        private string addlink = "~/SiteAdmin/Secure/Setup/Storefront/Catalogs/add.aspx";
        private string copylink = "~/SiteAdmin/Secure/Setup/Storefront/Catalogs/copy.aspx?ItemId=";
        #endregion

        #region public properties
        /// <summary>
        /// Gets or sets the Account Object from/to Cache
        /// </summary>
        public Account UserAccount
        {
            get
            {
                if (ViewState["AccountObject"] != null)
                {
                    // The account info with 'AccountObject' is retrieved from the cache using Get Method and
                    // It is converted to a Entity Account object
                    return (Account)ViewState["AccountObject"];
                }

                return new Account();
            }

            set
            {
                // Customer account object is placed in the session and assigned a key, AccountObject.            
                ViewState["AccountObject"] = value;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// HideDelete Button Method
        /// </summary>
        /// <param name="value">Represents a value</param>
        /// <returns>Returns a bool value</returns>
        public bool HideDeleteButton(object value)
        {
            return ZNodeCatalogManager.CatalogConfig.CatalogID != int.Parse(value.ToString());
        }

        /// <summary>
        /// Bind data to grid
        /// </summary>
        public void BindGridData()
        {
            CatalogAdmin catalogAdmin = new CatalogAdmin();
            Account account = this.UserAccount;
            MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            string roleList = string.Empty;

            // Get roles for this User account
            string[] roles = Roles.GetRolesForUser(user.UserName);

            foreach (string Role in roles)
            {
                roleList += Role + "<br>";
            }

            string rolename = roleList;

            // Hide the Delete button if a NonAdmin user has entered this page
            if (!Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "ADMIN"))
            {
                if (Roles.IsUserInRole(user.UserName, "CATALOG EDITOR"))
                {
                    string portalIDs = string.Empty;
                    portalIDs = UserStoreAccess.GetAvailablePortals;
                    string catalogIDs = string.Empty;
                    catalogIDs = catalogAdmin.GetCatalogIDsByPortalIDs(portalIDs);

                    TList<Catalog> catalogList = catalogAdmin.GetAllCatalogs();
                    DataSet ds = catalogList.ToDataSet(true);

                    DataView dv = new DataView(ds.Tables[0]);
                    if (catalogIDs != "0")
                    {
                        dv.RowFilter = "CatalogId in (" + catalogIDs + ")";
                    }

                    uxGrid.DataSource = dv;
                    uxGrid.DataBind();
                }
            }
            else
            {
                TList<Catalog> catalogList = catalogAdmin.GetAllCatalogs();
                DataSet ds = catalogList.ToDataSet(false);
                uxGrid.DataSource = UserStoreAccess.CheckStoreAccess(ds.Tables[0], true);
                uxGrid.DataBind();
            }
        }
        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Add Catalog Button Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddcatalog_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.addlink);
        }

        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGridData();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "Manage")
                {
                    Response.Redirect(this.editlink + e.CommandArgument);
                }
                else if (e.CommandName == "RemoveItem")
                {
                    Response.Redirect(this.deletelink + e.CommandArgument);
                }
                else if (e.CommandName.ToLower() == "copy")
                {
                    Response.Redirect(this.copylink + e.CommandArgument);
                }
            }
        }

        #endregion

        #region Page Load Event
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindGridData();
            }
        }
        #endregion
    }
}