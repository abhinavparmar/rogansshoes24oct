<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="true" Title="Access Denied" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/spacer.ascx" TagName="spacer" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" Runat="Server">
<h1>Could not complete request</h1>
This feature is not available in this edition of Znode.
<div><uc1:spacer id="Spacer1" SpacerHeight="20" SpacerWidth="10" runat="server"></uc1:spacer></div>
<div><a href="javascript: history.go(-1)">< Back</a></div>
<div><uc1:spacer id="Spacer8" SpacerHeight="200" SpacerWidth="10" runat="server"></uc1:spacer></div>
</asp:Content>

