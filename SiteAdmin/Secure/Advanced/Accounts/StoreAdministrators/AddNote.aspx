<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Advanced.Accounts.StoreAdministrators.AddNote" ValidateRequest="false"
    Title="Untitled Page" CodeBehind="AddNote.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/Accounts/AddNote.ascx" TagName="NotesAdd" TagPrefix="ZNode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <ZNode:NotesAdd ID="uxNotesAdd" runat="server" RoleName="admin" />
</asp:Content>
