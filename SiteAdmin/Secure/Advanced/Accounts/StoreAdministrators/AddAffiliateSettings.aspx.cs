using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Advanced.Accounts.StoreAdministrators
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_sales_customers_AddAffiliateSettings class
    /// </summary>
    public partial class AddAffiliateSettings : System.Web.UI.Page
    {
        
    }
}