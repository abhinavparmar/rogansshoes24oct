<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="SiteAdmin.Secure.Advanced.Accounts.StoreAdministrators.AddAffiliateSettings"
    Title="Affiliate Info" Codebehind="AddAffiliateSettings.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/Accounts/AffiliateSettings.ascx" TagName="AffiliateSettings" TagPrefix="ZNode" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <ZNode:AffiliateSettings ID="uxPermission" runat="server" RoleName = "admin" />

</asp:Content>
