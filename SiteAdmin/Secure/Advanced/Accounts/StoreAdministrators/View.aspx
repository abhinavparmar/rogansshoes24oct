﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" CodeBehind="View.aspx.cs" Inherits="SiteAdmin.Secure.Advanced.Accounts.StoreAdministrators.View" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/Accounts/View.ascx" TagName="SiteadminList" TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
 <ZNode:SiteadminList ID="uxCustomer" runat="server" RoleName = "admin" />
</asp:Content>
