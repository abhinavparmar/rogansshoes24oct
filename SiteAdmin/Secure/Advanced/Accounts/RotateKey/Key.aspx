<%@ Page Language="C#" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Advanced.Accounts.RotateKey.Key"
    MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" Codebehind="Key.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <h1>
        Key Rotation - Confirmation Required</h1>
    <p>Generate a new encryption key for your store.</p>

    <p>
        Please confirm if you want to generate a new encryption key for this store.
        You should create a new encryption key instead of using the key that is shipped
        out of the box for enhanced security.</p>
    <br />
    <b>CAUTION!</b>
    <p>
        Generating a new key will make previously encrypted data un-readable (for ex: Merchant
        Gateway Settings, SMTP Settings, etc). Key generation should only be done for a
        new installation and should never be attempted on a store running in production.</p>
    <p>
        Key generation will not impact customer or product data.</p>
     <div class="ImageButtons">
        <asp:Button ID="btnGenerate" CausesValidation="False" Text="Generate" CssClass="Size100"
            runat="server" OnClick="BtnGenerate_Click" />
        <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/Images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
    </div>
    <div style="margin-top: 20px; margin-bottom: 50px; font-weight: bold; color: #009900;">
        <asp:Label runat="server" ID="lblMsg"></asp:Label>
    </div>
    <a id="A1" href="~/SiteAdmin/Secure/Advanced/Default.aspx?mode=ipcommerce" runat="server">Back to
        Advanced page</a>
</asp:Content>
