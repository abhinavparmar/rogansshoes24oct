﻿<%@ Page Language="C#" AutoEventWireup="True" ValidateRequest="false" CodeBehind="Default.aspx.cs"
    MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" Inherits="SiteAdmin.Secure.Advanced.Accounts.RulesEngine.Default" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="TaxRule" Src="~/SiteAdmin/Secure/Advanced/Accounts/RulesEngine/TaxRule/DefaultTax.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ShippingRule" Src="~/SiteAdmin/Secure/Advanced/Accounts/RulesEngine/ShippingRule/DefaultShipping.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="PromotionRule" Src="~/SiteAdmin/Secure/Advanced/Accounts/RulesEngine/PromotionRule/DefaultPromotion.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="SupplierRule" Src="~/SiteAdmin/Secure/Advanced/Accounts/RulesEngine/SupplierRule/DefaultSupplier.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <script type="text/javascript">
        function ActiveTabChanged(sender, e) {
            var Tab = document.getElementById('<%=tabRuleEngine.ClientID%>');
             var activeTabIndex = document.getElementById('<%=hdnActiveTabIndex.ClientID%>');
             activeTabIndex.value = sender.get_activeTabIndex();
         }
    </script>
    <asp:HiddenField ID="hdnActiveTabIndex" runat="server" Value="0" />
    <div>
        <div class="LeftFloat" style="width: 50%">
            <h1>Rules Engine</h1>
        </div>
        <div class="ClearBoth">
            <p>
               Add custom business rules to your store using .NET. Advanced functionality for experienced developers only.
            </p>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>

    </div>
    <div style="clear: both;">
        <ajaxToolKit:TabContainer ID="tabRuleEngine" runat="server" ActiveTabIndex="0" OnClientActiveTabChanged="ActiveTabChanged">
            <ajaxToolKit:TabPanel ID="pnlTaxRule" runat="server">
                <HeaderTemplate>
                    Tax Rules
                </HeaderTemplate>
                <ContentTemplate>
                    <ZNode:TaxRule ID="TaxRule" runat="server"></ZNode:TaxRule>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="pnlShippingRule" runat="server">
                <HeaderTemplate>
                    Shipping Rules
                </HeaderTemplate>
                <ContentTemplate>
                  <ZNode:ShippingRule ID="ShippingRule" runat="server"></ZNode:ShippingRule>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="pnlPromotionRule" runat="server">
                <HeaderTemplate>
                    Promotion Rules
                </HeaderTemplate>
                <ContentTemplate>
                    <ZNode:PromotionRule ID="PromotionRule" runat="server"></ZNode:PromotionRule>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="pnlSupplierRule" runat="server">
                <HeaderTemplate>
                    Supplier Rules
                </HeaderTemplate>
                <ContentTemplate>
                   <ZNode:SupplierRule ID="SupplierRule" runat="server"></ZNode:SupplierRule>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
        </ajaxToolKit:TabContainer>
    </div>
</asp:Content>
