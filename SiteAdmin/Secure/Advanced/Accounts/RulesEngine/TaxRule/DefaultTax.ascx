﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DefaultTax.ascx.cs" Inherits="SiteAdmin.Secure.Advanced.Accounts.RulesEngine.TaxRule.DefaultTax" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>


<asp:UpdatePanel ID="updatepanelProductList" runat="server">
    <ContentTemplate>
        <div class="ButtonStyle">
            <zn:LinkButton ID="btnAddRuleType" runat="server" CausesValidation="False"
                ButtonType="Button" OnClick="BtnAddRuleType_Click" Text="Add a New Tax Rule Type"
                ButtonPriority="Primary" />
        </div>
        <div>
            <ZNode:Spacer id="Spacer6" spacerheight="10" spacerwidth="3" runat="server"></ZNode:Spacer>
        </div>

        </div>
    <div class="ClearBoth" align="left">
    </div>
        <div class="Form">
            <div>
                <asp:Label ID="lblErrorMsg" EnableViewState="false" runat="server" CssClass="Error"></asp:Label>
            </div>
            <div>
                <ZNode:Spacer id="Spacer1" spacerheight="5" spacerwidth="3" runat="server"></ZNode:Spacer>
            </div>
            <h4 class="GridTitle">.NET Tax Class List</h4>
            <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
                CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
                CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" OnRowDeleting="UxGrid_RowDeleting" Width="100%" AllowSorting="True"
                EmptyDataText="No tax rule types available to display.">
                <Columns>
                    <asp:BoundField DataField="TaxRuleTypeId" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="ClassName" HtmlEncode="false" HeaderText="Class Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="30%" />
                    <asp:BoundField DataField="Name" HeaderText="Name" HtmlEncode="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="30%" />
                    <asp:TemplateField HeaderText="Enabled" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img alt="" id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:ButtonField CommandName="Edit" Text="Edit &raquo" ButtonType="Link">
                        <ControlStyle CssClass="actionlink" />
                    </asp:ButtonField>
                    <asp:ButtonField CommandName="Delete" Text="Delete &raquo" ButtonType="Link">
                        <ControlStyle CssClass="actionlink" />
                    </asp:ButtonField>
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
    </ContentTemplate>
</asp:UpdatePanel>
