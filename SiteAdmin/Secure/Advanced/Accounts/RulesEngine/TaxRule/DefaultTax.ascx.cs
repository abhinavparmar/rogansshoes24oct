﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Advanced.Accounts.RulesEngine.TaxRule
{
    public partial class DefaultTax : System.Web.UI.UserControl
    {
        #region Private Member Variables
        private RuleTypeAdmin ruleTypeAdmin = new RuleTypeAdmin();
        private string AddPageLink = "~/SiteAdmin/Secure/Advanced/Accounts/RulesEngine/TaxRule/Add.aspx";
        #endregion

        #region Page Load Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Bind();
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Add Rule Type Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddRuleType_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddPageLink + "?ruletype=2");
        }

        #endregion

        #region Grid Events
        /// <summary>
        /// Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.Bind();
        }
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.Bind();
        }

        /// <summary>
        /// Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                int index = Convert.ToInt32(e.CommandArgument);

                // Get the values from the appropriate
                // cell in the GridView control.
                GridViewRow selectedRow = uxGrid.Rows[index];

                TableCell Idcell = selectedRow.Cells[0];
                string Id = Idcell.Text;

                if (e.CommandName == "Edit")
                {
                    this.AddPageLink = this.AddPageLink + "?ItemID=" + Id + "&ruletype=2";
                    Response.Redirect(this.AddPageLink);
                }
                else if (e.CommandName == "Delete")
                {
                    try
                    {
                        bool status = this.ruleTypeAdmin.DeleteTaxRuleType(int.Parse(Id));
                        string AssociateName = "Delete Tax Rule - " + selectedRow.Cells[2].Text;
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, selectedRow.Cells[2].Text);

                        if (!status)
                        {
                            lblErrorMsg.Text = "The taxrule type '" + selectedRow.Cells[2].Text + "' can not be deleted until all associated items are removed. Please ensure that this taxrule type does not contain tax rules. If it does, then delete tax rules first.";
                        }
                        else
                        {
                            this.Bind();
                        }
                    }
                    catch (Exception ex)
                    {
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
                        lblErrorMsg.Text = "The taxrule type '" + selectedRow.Cells[2].Text + "' can not be deleted until all associated items are removed. Please ensure that this taxrule type does not contain tax rules. If it does, then delete tax rules first.";
                    }
                }
            }
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind Method
        /// </summary>
        private void Bind()
        {
            uxGrid.DataSource = this.ruleTypeAdmin.GetTaxRuleTypes();
            uxGrid.DataBind();
        }
        #endregion

    }
}