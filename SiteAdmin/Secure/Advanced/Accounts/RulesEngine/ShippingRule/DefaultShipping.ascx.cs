﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.Framework.Business;


namespace SiteAdmin.Secure.Advanced.Accounts.RulesEngine.ShippingRule
{
    public partial class DefaultShipping : System.Web.UI.UserControl
    {
        #region Private Member Variables
        private RuleTypeAdmin ruleTypeAdmin = new RuleTypeAdmin();
        private string AddPageLink = "~/SiteAdmin/Secure/Advanced/Accounts/RulesEngine/ShippingRule/Add.aspx";
        #endregion

        #region Page Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindShippingTypes();
            }
        }

        #endregion

        #region Grid Events

        /// <summary>
        /// Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindShippingTypes();
        }

        /// <summary>
        /// Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                int index = Convert.ToInt32(e.CommandArgument);

                // Get the values from the appropriate
                // cell in the GridView control.
                GridViewRow selectedRow = uxGrid.Rows[index];

                TableCell Idcell = selectedRow.Cells[0];
                string Id = Idcell.Text;

                if (e.CommandName == "Edit")
                {
                    Response.Redirect(this.AddPageLink + "?ItemID=" + Id + "&ruletype=1");
                }
                else if (e.CommandName == "Delete")
                {
                    try
                    {
                        bool status = this.ruleTypeAdmin.DeleteShippingType(int.Parse(Id));
                        string AssociateName = "Delete Shipping Rule - " + selectedRow.Cells[2].Text;
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, selectedRow.Cells[2].Text);

                        if (!status)
                        {
                            lblErrorMsg.Text = "The shipping type '" + selectedRow.Cells[2].Text + "' can not be deleted until all associated items are removed. Please ensure that this shipping type does not contain shipping options. If it does, then delete shipping options first.";
                        }
                        else
                        {
                            this.BindShippingTypes();
                        }
                    }
                    catch (Exception ex)
                    {
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
                        lblErrorMsg.Text = "The shipping type '" + selectedRow.Cells[2].Text + "' can not be deleted until all associated items are removed. Please ensure that this shipping type does not contain shipping options. If it does, then delete shipping options first.";
                    }
                }
            }
        }

        /// <summary>
        /// Grid Row Deleting Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindShippingTypes();
        }
        #endregion

        #region Events
        /// <summary>
        /// Add Rule Type Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddRuleType_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddPageLink + "?ruletype=1");
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/RulesEngine/Default.aspx?mode=shipping");
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind Shipping Types Method
        /// </summary>
        private void BindShippingTypes()
        {
            uxGrid.DataSource = this.ruleTypeAdmin.GetShippingTypes();
            uxGrid.DataBind();
        }
        #endregion
    }
}