﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DefaultShipping.ascx.cs" Inherits="SiteAdmin.Secure.Advanced.Accounts.RulesEngine.ShippingRule.DefaultShipping" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>

<asp:UpdatePanel ID="updatepanelProductList" runat="server">
    <ContentTemplate>
        <div>
            <div class="ButtonStyle">
                <zn:LinkButton ID="btnAddRuleType" runat="server"
                    ButtonType="Button" OnClick="BtnAddRuleType_Click" Text="Add a New Shipping Rule Type"
                    ButtonPriority="Primary" />
            </div>
        </div>
        <div class="ClearBoth" align="left">
            <br />
        </div>
        <div class="Form">
            <div>
                <ZNode:Spacer ID="Spacer8" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div>
                <asp:Label ID="lblErrorMsg" EnableViewState="false" runat="server" CssClass="Error"></asp:Label>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <h4 class="GridTitle">.NET Shipping Class List</h4>
            <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
                CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
                CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" AllowSorting="True"
                OnRowDeleting="UxGrid_RowDeleting" EmptyDataText="No shipping types available to display.">
                <Columns>
                    <asp:BoundField DataField="ShippingTypeId" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="ClassName" HeaderText="Class Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="30%" />
                    <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText="Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="30%" />
                    <asp:TemplateField HeaderText="Is Active" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img alt="" id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "IsActive").ToString()))%>'
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:ButtonField CommandName="Edit" Text="Edit &raquo" ButtonType="Link">
                        <ControlStyle CssClass="actionlink" />
                    </asp:ButtonField>
                    <asp:ButtonField CommandName="Delete" Text="Delete &raquo" ButtonType="Link">
                        <ControlStyle CssClass="actionlink" />
                    </asp:ButtonField>
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
