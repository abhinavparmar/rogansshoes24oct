﻿<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="true"
    Title="Maintenance" %>

<script runat="server">
    protected void Page_Init(object sender, EventArgs e)
    {
        Session["SelectedMenuItem"] = Request.Url.PathAndQuery;
    }    
</script>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <h1>Accounts & Security</h1>
    <hr />
    <div class="ImageAlign">
        <div class="Image">
            <img src="../../Themes/Images/SITE-admin.png" />
        </div>
        <div class="Shortcut"><a id="A4" href="~/SiteAdmin/Secure/Advanced/Accounts/StoreAdministrators/Default.aspx" runat="server">Store Administrators</a></div>
        <div class="LeftAlign">
            <p>Manage store administrator accounts, roles and permissions.</p>
        </div>
    </div>

     <div class="ImageAlign">
        <div class="Image">
            <img src="../../Themes/Images/TaxRuleEngine.png" />
        </div>
        <div class="Shortcut">
            <a id="A2" href="~/SiteAdmin/Secure/Advanced/Accounts/RulesEngine/Default.aspx" runat="server">Rules Engine</a>
        </div>
        <p>
          Add custom rules to your store using .NET. Advanced functionality for experienced developers only.
        </p>
    </div> 

    <h1>Search</h1>
    <hr />
    <div class="ImageAlign">
        <div class="Image">
            <img src="../../Themes/Images/ManageIndex.png" />
        </div>
        <div class="LeftAlign">
            <div class="Shortcut"><a id="A5" href="~/SiteAdmin/Secure/Advanced/Search/SearchIndex/Default.aspx" runat="server">Manage Search Index</a></div>
            <p>Create or rebuild the Lucene search engine index.</p>
        </div>
    </div>
</asp:Content>
