﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IndexStatusList.ascx.cs" Inherits="SiteAdmin.Secure.Advanced.Search.SearchIndex.IndexStatusList" %>

<div class="ManageIndexText">
<asp:UpdatePanel ID="pnl" runat="server" UpdateMode="Conditional">
<ContentTemplate>
<asp:GridView ID="gvIndexStatusList" CellPadding="4" runat="server" CssClass="IndexGrid" AutoGenerateColumns="False" PageSize="10" 
    AllowPaging="True"  GridLines="None" Width="100%"  DataKeyNames="LuceneIndexMonitorID" ViewStateMode="Enabled" EmptyDataText="Indexes have not been created" >
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
             <input id='hid<%# Eval("LuceneIndexMonitorID") %>') value='0' type="hidden" />
                <a href="javascript:GetChildGrid('<%# Eval("LuceneIndexMonitorID") %>');">
                    <img id='img<%# Eval("LuceneIndexMonitorID") %>' alt="plus" border="0" src="../../../../../SiteAdmin/Themes/Images/plus.png" />
                </a>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField HeaderText="Monitor ID" DataField="LuceneIndexMonitorID" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="10" HeaderStyle-Width="10" />
        <asp:BoundField HeaderText="Source ID" DataField="SourceID" HeaderStyle-HorizontalAlign="Left" NullDisplayText=" " />
        <asp:BoundField HeaderText="Source Type" DataField="SourceType" HeaderStyle-HorizontalAlign="Left" NullDisplayText=" "/>
        <asp:BoundField HeaderText="Source Transaction Type" DataField="SourceTransactionType" HeaderStyle-HorizontalAlign="Left" NullDisplayText=" " ItemStyle-Width="50" HeaderStyle-Width="50"  />
        <asp:BoundField HeaderText="Transaction DateTime" DataField="TransactionDateTime" HeaderStyle-HorizontalAlign="Left" NullDisplayText=" " />
        <asp:BoundField HeaderText="Is Duplicate" DataField="IsDuplicate" HeaderStyle-HorizontalAlign="left" NullDisplayText=" " />
        <asp:BoundField HeaderText="Column Change" DataField="AffectedType" HeaderStyle-HorizontalAlign="Left" NullDisplayText=" " />
        <asp:BoundField HeaderText="Modified By" DataField="IndexerStatusChangedBy" HeaderStyle-HorizontalAlign="Left" NullDisplayText=" "/>

        <asp:TemplateField  >
            <ItemTemplate>
                         </td></tr>
                        <tr >
                            <td id="td<%# Eval("LuceneIndexMonitorID") %>" colspan="9">
                                <div id="div<%# Eval("LuceneIndexMonitorID") %>" style="width:100%;display:none;">
                                   
                                </div>
                    </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <EditRowStyle CssClass="EditRowStyle" />
    <FooterStyle CssClass="FooterStyle" />
    <RowStyle CssClass="RowStyle" />
    <SelectedRowStyle CssClass="SelectedRowStyle" />
    <HeaderStyle CssClass="HeaderStyle" />
    <PagerStyle CssClass="PagerStyle" />
    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
</asp:GridView>
<br />
<asp:HiddenField ID="hdnpage" runat="server" />
<asp:HiddenField ID="hdnTotal" runat="server" />
<asp:LinkButton ID="lbfirst" runat="server" Text="&lt;&lt;" CssClass="Button" OnClick="lbfirst_Click"></asp:LinkButton>
<asp:LinkButton ID="lbprev" runat="server" Text="&lt;" CssClass="Button" OnClick="lbprev_Click"></asp:LinkButton>
<asp:LinkButton ID="lbnext" runat="server" Text="&gt;" CssClass="Button" OnClick="lbnext_Click"></asp:LinkButton>
<asp:LinkButton ID="lblast" runat="server" Text="&gt;&gt;" CssClass="Button" OnClick="lblast_Click"></asp:LinkButton>
</ContentTemplate>
</asp:UpdatePanel>
    </div>