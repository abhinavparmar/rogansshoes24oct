﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StatusGridBuilder.aspx.cs" Inherits="WebApp.SiteAdmin.Secure.Advanced.Search.SearchIndex.StatusGridBuilder" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:GridView ID="gvServerStatus" CssClass="IndexGrid" CellPadding="2" runat="server"
            EmptyDataText="Indexes have not been created" AutoGenerateColumns="false" DataKeyNames="LuceneIndexMonitorID"
            Width="80%">
            <Columns>
                <asp:BoundField DataField="ServerName" HeaderText="Server Name" />
                <asp:TemplateField HeaderText="Status" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="IndexServerStatus" Text='<%#(ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Services.ZNodeStatusNames)Enum.Parse(typeof(ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Services.ZNodeStatusNames), Eval("Status").ToString()) %>'
                            runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="StartTime" HeaderText="Start Time" />
                <asp:BoundField DataField="EndTime" HeaderText="End Time" />
            </Columns>
        </asp:GridView>
    </div>
    </form>
</body>
</html>
