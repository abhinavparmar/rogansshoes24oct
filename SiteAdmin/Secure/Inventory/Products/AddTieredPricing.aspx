<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Title="Manage Products - Add Tiered Pricing" Inherits="SiteAdmin.Secure.Inventory.Products.AddTieredPricing" CodeBehind="AddTieredPricing.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblHeading" runat="server" /></h1>
            <ZNode:DemoMode ID="DemoMode1" runat="server"></ZNode:DemoMode>

        </div>
        <div class="ClearBoth">
        </div>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        </div>
        <asp:Label ID="lblError" CssClass="Error" runat="server"></asp:Label>
        <div class="FormView">
            <div class="FieldStyle">
                Select Profile
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlProfiles" runat="server">
                </asp:DropDownList>
            </div>
            <div class="FieldStyle">
                Tier Start<span class="Asterix">*</span><br />
                <small>Minimum quantity of items that must be ordered for this pricing to apply</small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtTierStart" runat="server" Width="80px"></asp:TextBox>

                <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ControlToValidate="txtTierStart"
                    ErrorMessage="* Enter minimum quantity" CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator10" runat="server" ControlToValidate="txtTierStart"
                    CssClass="Error" Display="dynamic" ErrorMessage="Enter a whole number." MaximumValue="10000"
                    MinimumValue="1" SetFocusOnError="true" Type="Integer"></asp:RangeValidator>
            </div>
            <div class="FieldStyle">
                Tier End<span class="Asterix">*</span><br />
                <small>Maximum quantity of items that must be ordered for this pricing to apply</small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtTierEnd" runat="server" Width="80px"></asp:TextBox>

                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTierEnd"
                    ErrorMessage="* Enter maximum quantity" CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtTierEnd"
                    CssClass="Error" Display="dynamic" ErrorMessage="Enter a whole number." MaximumValue="10000"
                    MinimumValue="1" SetFocusOnError="true" Type="Integer"></asp:RangeValidator>
                <asp:CompareValidator ID="cmpTierLimitsValidator" runat="server" ControlToValidate="txtTierEnd"
                    ControlToCompare="txtTierStart" Display="Dynamic" Type="Integer" ErrorMessage="Tier maximum quantity must be greater or equal to tier minimum quantity."
                    CssClass="Error" Operator="GreaterThanEqual"></asp:CompareValidator>
            </div>

            <div class="FieldStyle">
                Price<span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;<asp:TextBox
                    ID="txtPrice" runat="server" MaxLength="10"></asp:TextBox>

                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="* Enter a discounted price for this tier."
                    ControlToValidate="txtPrice" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtPrice"
                    ErrorMessage="You must enter a discounted price value between $0 and $999,999.99"
                    MaximumValue="99999999" Type="Currency" MinimumValue="0" Display="Dynamic" CssClass="Error"></asp:RangeValidator>
            </div>
            <div class="ClearBoth">
                <br />
            </div>
            <div>
                <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
                <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
            </div>
        </div>
    </div>
</asp:Content>
