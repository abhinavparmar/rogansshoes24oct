using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Repository;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.DataAccess.EntityFramework.POCO;


namespace SiteAdmin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Site Admin Admin_Secure_products_view class
    /// </summary>
    public partial class View : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId;
        private string Mode = string.Empty;
        private string EditPageLink = "Add.aspx?itemid=";
        private string EditSEOLink = "EditSeo.aspx?itemid=";
        private string EditAdvancedPageLink = "EditAdvancedSettings.aspx?itemid=";
        private string EditAdditionalInfoLink = "EditAdditionalInfo.aspx?itemid=";
        private string AddRelatedItemLink = "AddRelatedItems.aspx?itemid=";
        private string AddTieredPricingPageLink = "AddTieredPricing.aspx?itemid=";
        private string AddDigitalAssetPageLink = "AddDigitalAsset.aspx?itemid=";
        private string AddProductAddOnLink = "AddAddons.aspx?";
        private string ListLink = "Default.aspx";
        private string PreviewLink = "/product.aspx?zpid=";
        private string AddSKULink = "AddSku.aspx?itemid=";
        private string AddViewlink = "AddView.aspx?itemid=";
        private string AddHighlightPageLink = "AddHighlights.aspx?itemid=";
        private string EditHighlightPageLink = "~/SiteAdmin/Secure/Inventory/ReferenceTypes/Highlights/Add.aspx?itemid=";
        private string AddBundleProductsLink = "AddBundleProduct.aspx?itemid=";
        private string associateCategoryPageLink = "AddProductCategory.aspx?itemid=";
        private bool _HasAttributes = false;
        private ZNodeImage znodeImage = new ZNodeImage();
        private bool _isXCommercePluginInstalled;

        private string AddSpecItemLink = "AddSpecItems.aspx?itemid=";
        protected string ManageSKUSpecLink = "~/SiteAdmin/Secure/Inventory/Products/ViewSkuSpec.aspx?itemid=";

        #endregion

        protected void Page_InitComplete(object sender, EventArgs e)
        {
            _isXCommercePluginInstalled = AppDomain.CurrentDomain.GetAssemblies().Any(assembly => assembly.GetName().Name.Equals("X-ComAdmin"));
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (_isXCommercePluginInstalled)
            {
                var bootstrap = new HtmlLink { };
                bootstrap.Href = "~/bootstrap/css/bootstrap.min.css";
                bootstrap.Attributes.Add("rel", "stylesheet");
                bootstrap.Attributes.Add("type", "text/css");

                Page.Header.Controls.Add(bootstrap);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether product has attributes.
        /// </summary>
        public bool HasAttributes
        {
            get { return this._HasAttributes; }
            set { this._HasAttributes = value; }
        }

        public int SelectedTabIndex
        {
            get
            {
                if (Session["SelectedTabIndex"] != null)
                {
                    return Convert.ToInt32(Session["SelectedTabIndex"]);
                }

                return 0;
            }

            set
            {
                Session["SelectedTabIndex"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value of Product ProductTypeID.
        /// </summary>
        private int ProductTypeID
        {
            get
            {
                int value;
                int.TryParse(ViewState["ProductTypeID"].ToString(), out value);
                return value;
            }

            set
            {
                ViewState["ProductTypeID"] = value;
            }
        }

        #region Public Methods

        /// <summary>
        /// Get Active Tab Name Method
        /// </summary>
        /// <param name="tabIndex">The value of tabIndex</param>
        /// <returns>Returns the Active Tab Name</returns>
        public string GetActiveTabName(int tabIndex)
        {
            string tabName = string.Empty;
            switch (tabIndex)
            {
                case 1:
                    tabName = "advanced";
                    break;

                case 2:
                    tabName = "departments";
                    break;

                case 3:
                    tabName = "inventory";
                    break;

                case 4:
                    tabName = "bundle";
                    break;

                case 5:
                    tabName = "tag";
                    break;

                case 6:
                    tabName = "crosssell";
                    break;

                case 7:
                    tabName = "views";
                    break;

                case 8:
                    tabName = "addons";
                    break;

                case 9:
                    tabName = "tieredPricing";
                    break;

                case 10:
                    tabName = "highlight";
                    break;

                case 11:
                    tabName = "digitalAsset";
                    break;
            }

            return tabName;
        }

        /// <summary>
        /// Returns a Format Weight string
        /// </summary>
        /// <param name="fieldValue">field value</param>
        /// <returns>Returns the Formatted Product Weight</returns>
        public string FormatProductWeight(object fieldValue)
        {
            if (fieldValue == null)
            {
                return String.Empty;
            }
            else
            {
                if (Convert.ToDecimal(fieldValue.ToString()) == 0)
                {
                    return string.Empty;
                }
                else
                {
                    return fieldValue.ToString() + " " + ZNodeConfigManager.SiteConfig.WeightUnit;
                }
            }
        }

        /// <summary>
        /// Format the Price of a Product
        /// </summary>
        /// <param name="fieldValue">field value</param>
        /// <returns>Returns the Formatted Price</returns>
        public string FormatPrice(object fieldValue)
        {
            if (fieldValue == null)
            {
                return String.Empty;
            }
            else
            {
                if (Convert.ToDecimal(fieldValue) == 0)
                {
                    return String.Empty;
                }
                else
                {
                    return String.Format("{0:c}", fieldValue);
                }
            }
        }

        /// <summary>
        /// Validate for Null Values and return a Boolean Value
        /// </summary>
        /// <param name="fieldvalue">field value</param>
        /// <returns>Returns a bool value true or false</returns>
        public bool DisplayVisible(object fieldvalue)
        {
            if (fieldvalue == DBNull.Value)
            {
                return false;
            }
            else
            {
                if (Convert.ToInt32(fieldvalue) == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        /// <summary>
        /// Gets the name of the Addon for this AddonId
        /// </summary>
        /// <param name="addOnId">The value of addOnId</param>
        /// <returns>Returns the AddOnName</returns>
        public string GetAddOnName(object addOnId)
        {
            ProductAddOnAdmin AdminAccess = new ProductAddOnAdmin();
            AddOn _addOn = AdminAccess.GetByAddOnId(int.Parse(addOnId.ToString()));

            if (_addOn != null)
            {
                return _addOn.Name;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the title of the Addon for this AddonId
        /// </summary>
        /// <param name="addOnId">The value of Addon Title </param>
        /// <returns>Returns the AddOn Title</returns>
        public string GetAddOnTitle(object addOnId)
        {
            ProductAddOnAdmin AdminAccess = new ProductAddOnAdmin();
            AddOn _addOn = AdminAccess.GetByAddOnId(int.Parse(addOnId.ToString()));

            if (_addOn != null)
            {
                return _addOn.Title;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the Name of the Profile for this ProfileID
        /// </summary>
        /// <param name="ProfileID">The value of ProfileID</param>
        /// <returns>Returns the Profile Name</returns>
        public string GetProfileName(object ProfileID)
        {
            ProfileAdmin AdminAccess = new ProfileAdmin();
            if (ProfileID == null)
            {
                return "All Profile";
            }
            else
            {
                Profile profile = AdminAccess.GetByProfileID(int.Parse(ProfileID.ToString()));

                if (profile != null)
                {
                    return profile.Name;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the name of the Addon for this AddonId
        /// </summary>
        /// <param name="shippingRuleTypeID">The value of shippingRuleTypeID</param>
        /// <returns>Returns the Shipping Rule Type Name</returns>
        public string GetShippingRuleTypeName(int shippingRuleTypeID)
        {
            ShippingRuleTypeAdmin _ShippingRuleTypeAdmin = new ShippingRuleTypeAdmin();

            ShippingRuleType _shippingRuleType = _ShippingRuleTypeAdmin.GetByShippingRuleTypeID(shippingRuleTypeID);

            if (_shippingRuleType != null)
            {
                return _shippingRuleType.Name;
            }

            return string.Empty;
        }

        /// <summary>
        /// Swap image with the new image file path
        /// </summary>
        /// <param name="imagepath">The value of image path</param>
        public void Changeimage(string imagepath)
        {
            ItemImage.ImageUrl = this.znodeImage.GetImageHttpPathSmall(imagepath);
        }

        /// <summary>
        /// Return cross-mark image path
        /// </summary>
        /// <returns>Returns the Check Mark Image Path</returns>
        public string SetCheckMarkImage()
        {
            return ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse("false"));
        }

        /// <summary>
        /// Returns Is Digital Asset value assigned for the product
        /// </summary>
        /// <param name="orderLineItemId">The value of OrderLineItemId</param>
        /// <returns>Returns bool value true or false</returns>
        public bool IsDigitalAssetAssigned(object orderLineItemId)
        {
            if (orderLineItemId == null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Get SKU Quantity method
        /// </summary>
        /// <param name="skuId">The value of SKU Id</param>
        /// <returns>Returns the SKU Quantity</returns>
        public string GetSkuQuantity(int skuId)
        {
            ZNode.Libraries.DataAccess.Service.SKUService ss = new ZNode.Libraries.DataAccess.Service.SKUService();
            return SKUAdmin.GetQuantity(ss.GetBySKUID(skuId)).ToString();
        }

        /// <summary>
        /// Get Sku Reorder Level method
        /// </summary>
        /// <param name="skuId">The value of SKU Id</param>
        /// <returns>Returns the SKU Reorder level</returns>
        public string GetSkuReorderLevel(int skuId)
        {
            ZNode.Libraries.DataAccess.Service.SKUService ss = new ZNode.Libraries.DataAccess.Service.SKUService();
            return SKUAdmin.GetInventory(ss.GetBySKUID(skuId)).ReOrderLevel.ToString();
        }

        #endregion

        #region Protected Methods and Events

        /// <summary>
        /// Get http image path.
        /// </summary>
        /// <param name="imageFileName">Image file name.</param>
        /// <returns>Returns the image file path with name.</returns>
        public string GetImagePath(string imageFileName)
        {
            ZNodeImage znodeImage = new ZNodeImage();
            return znodeImage.GetImageHttpPathThumbnail(imageFileName);
        }

        /// <summary>
        /// Get the product name by product Id
        /// </summary>
        /// <param name="productId">Product Id</param>
        /// <returns>Return the name of the product</returns>
        protected string GetProductName(object productId)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            return productAdmin.GetByProductId(Convert.ToInt32(productId)).Name;
        }

        /// <summary>
        /// Get the details product is active 
        /// </summary>
        /// <param name="productId">Product Id</param>
        /// <returns>Returns Boolean value</returns>
        protected bool GetProductIsActive(object productId)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            return productAdmin.GetByProductId(Convert.ToInt32(productId)).ActiveInd;
        }

        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get mode value from querystring        
            if (Request.Params["mode"] != null)
            {
                this.Mode = Request.Params["mode"];
            }

            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (this.IsPostBack)
            {
                this.SelectedTabIndex = string.IsNullOrEmpty(hdnActiveTabIndex.Value) ? this.SelectedTabIndex : Convert.ToInt32(hdnActiveTabIndex.Value);

                // Set the selected tab if locale selected.
                tabProductDetails.ActiveTabIndex = this.SelectedTabIndex;
            }

            if (!Page.IsPostBack)
            {
                this.ResetTab();

                if (this.ItemId > 0)
                {
                    this.BindViewData();
                    ZNodeUrl _Url = new ZNodeUrl();
                    this.PreviewLink = ZNodeConfigManager.EnvironmentConfig.ApplicationPath + "/product.aspx?zpid=" + this.ItemId;
                }
                else
                {
                    throw new ApplicationException("Product Requested could not be found.");
                }
            }

            // Add Client Side Script
            StringBuilder StringBuild = new StringBuilder();
            StringBuild.Append("<script language=JavaScript>");
            StringBuild.Append("    function  PreviewProduct() {");
            StringBuild.Append("  window.open('" + this.PreviewLink + "');");
            StringBuild.Append("    }");
            StringBuild.Append("<" + "/script>");

            if (!ClientScript.IsStartupScriptRegistered("Preview"))
            {
                ClientScript.RegisterStartupScript(GetType(), "Preview", StringBuild.ToString());
            }

            // Bind the Attribute List
            this.BindSkuAttributes();

            if (!Page.IsPostBack)
            {
                // Bind Grid - Inventory
                this.BindSKU();
            }
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Add Client side event to the Delete Button in the Grid.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridProductAddOns_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Retrieve the Button control from the Seventh column.
                LinkButton DeleteButton = (LinkButton)e.Row.Cells[2].FindControl("btnDelete");

                // Set the Button's CommandArgument property with the row's index.
                DeleteButton.CommandArgument = e.Row.RowIndex.ToString();

                // Add Client Side confirmation
                DeleteButton.OnClientClick = "return confirm('Are you sure you want to delete this item?');";
            }
        }

        /// <summary>
        /// Product Add On Row command event - occurs when delete button is fired.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridProductAddOns_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // property to an Integer.
                int index = Convert.ToInt32(e.CommandArgument);

                // Get the values from the appropriate
                // Cell in the GridView control.
                GridViewRow selectedRow = uxGridProductAddOns.Rows[index];

                TableCell Idcell = selectedRow.Cells[0];
                string Id = Idcell.Text;

                if (e.CommandName == "Remove")
                {
                    ProductAddOnAdmin AdminAccess = new ProductAddOnAdmin();
                    ProductAdmin prodAdmin = new ProductAdmin();
                    Product product = prodAdmin.GetByProductId(this.ItemId);

                    ProductAddOnService productaddonService = new ProductAddOnService();
                    ProductAddOn addon = new ProductAddOn();
                    addon = productaddonService.GetByProductAddOnID(int.Parse(Id));
                    productaddonService.DeepLoad(addon);
                    string addonname = addon.AddOnIDSource.Name;

                    string Associatename = "Delete Association between  Addon " + addonname + " and Product " + product.Name;

                    if (AdminAccess.DeleteProductAddOn(int.Parse(Id)))
                    {
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, Associatename, product.Name);
                    }

                    this.BindProductAddons();
                }
            }
        }

        /// <summary>
        /// Product AddOn grid Items Page Index Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridProductAddOns_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGridProductAddOns.PageIndex = e.NewPageIndex;
            this.BindProductAddons();
        }

        #endregion

        #region Related to CrossSell Grid Events
        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "RemoveItem")
                {
                    ProductCrossSellAdmin _prodCrossSellAdmin = new ProductCrossSellAdmin();
                    bool Check = _prodCrossSellAdmin.Delete(int.Parse(e.CommandArgument.ToString()), this.ItemId);
                    //bool Check = _prodCrossSellAdmin.Delete(int.Parse(e.CommandArgument.ToString()));
                    if (Check)
                    {
                        this.BindRelatedItems();
                    }
                }
            }
        }

        /// <summary>
        /// Add Client side event to the Delete Button in the Related Items Grid.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Retrieve the Button control from the Fourth column.
                LinkButton DeleteButton = (LinkButton)e.Row.Cells[2].FindControl("Delete");

                // Add Client Side confirmation
                DeleteButton.OnClientClick = "return confirm('Are you sure you want to delete this item?');";
            }
        }

        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindRelatedItems();
        }

        /// <summary>
        /// Related Items Page Index Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindRelatedItems();
        }
        #endregion

        #region Events for Bundle Product Grid
        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxBundleProductGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                ProductService _prodService = new ProductService();
                ProductAdmin _prodAdmin = new ProductAdmin();
                Product _prodParentEntity = _prodAdmin.GetByProductId(this.ItemId);
                bool Check = false;

                if (e.CommandName == "RemoveItem")
                {
                    ParentChildProductAdmin _parentChildProdAdmin = new ParentChildProductAdmin();

                    Check = _parentChildProdAdmin.Delete(int.Parse(e.CommandArgument.ToString()));

                    if (Check)
                    {
                        this.BindBundleProducts();
                    }
                }
            }
        }

        /// <summary>
        /// Add Client side event to the Delete Button in the Grid.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxBundleProductGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Retrieve the Button control from the Fourth column.
                LinkButton DeleteButton = (LinkButton)e.Row.Cells[3].FindControl("Delete");

                // Add Client Side confirmation
                DeleteButton.OnClientClick = "return confirm('Are you sure you want to delete this item?');";
            }
        }


        protected void UxBundleProductGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindBundleProducts();
        }

        /// <summary>
        /// Bundle Product Items Page Index Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxBundleProductGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxBundleProductGrid.PageIndex = e.NewPageIndex;
            this.BindBundleProducts();
        }
        #endregion

        #region InventoryDisplay Grid related Methods
        /// <summary>
        /// Event triggered when a command button is clicked on the grid (InventoryDisplay Grid)
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridInventoryDisplay_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandArgument.ToString() == "page")
            {
            }
            else
            {
                if (e.CommandName == "Edit")
                {
                    // Redirect Edit SKUAttrbute Page
                    Response.Redirect(this.AddSKULink + this.ItemId + "&skuid=" + e.CommandArgument.ToString() + "&typeid=" + this.ProductTypeID);
                }
                else if (e.CommandName == "Delete")
                {
                    // Delete SKU and SKU Attribute
                    SKUAdmin _AdminAccess = new SKUAdmin();

                    int skuId = int.Parse(e.CommandArgument.ToString());
                    TList<SKU> skuList = _AdminAccess.GetByProductID(this.ItemId);

                    // If product has only one SKU then do not delete.
                    if (skuList.Count == 1)
                    {
                        lblSkuErrorMsg.Text = "Could not delete the product last sku.";
                        return;
                    }

                    bool check = _AdminAccess.Delete(skuId);
                    if (check)
                    {
                        _AdminAccess.DeleteBySKUId(skuId);
                    }
                }
                else if (e.CommandName.ToLower().Equals("managespec"))
                {
                    //Redirect to Product SKU Specifications page
                    Response.Redirect(ManageSKUSpecLink + ItemId + "&skuid=" + e.CommandArgument.ToString());
                }
            }
        }

        /// <summary>
        /// Event triggered when the Grid Row is Deleted
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridInventoryDisplay_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindSearchData();
        }

        /// <summary>
        /// Event triggered when the grid(Inventory) page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridInventoryDisplay_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGridInventoryDisplay.PageIndex = e.NewPageIndex;
            this.BindSearchData();
        }

        #endregion

        #region Related to Product Views Grid Events

        /// <summary>
        /// Related Items Page Index Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GridThumb_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridThumb.PageIndex = e.NewPageIndex;
            this.BindImageDatas();
        }

        /// <summary>
        /// Grid Row Deleting  Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GridThumb_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindImageDatas();
        }

        /// <summary>
        /// Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GridThumb_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "Edit")
                {
                    Response.Redirect(this.AddViewlink + this.ItemId + "&productimageid=" + e.CommandArgument.ToString() + "&typeid=" + this.ProductTypeID);
                }

                if (e.CommandName == "RemoveItem")
                {
                    ZNode.Libraries.Admin.ProductViewAdmin prodadmin = new ProductViewAdmin();
                    bool Status = prodadmin.Delete(int.Parse(e.CommandArgument.ToString()));
                    if (Status)
                    {
                        this.BindImageDatas();
                    }
                }
            }
        }
        #endregion

        #region Related to Product - Tiered Pricing
        /// <summary>
        /// Add Client side event to the Delete Button in the Grid.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridTieredPricing_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Retrieve the Button control from the Seventh column.
                LinkButton DeleteButton = (LinkButton)e.Row.Cells[5].FindControl("btnRemoveTieredPricing");

                // Add Client Side confirmation
                DeleteButton.OnClientClick = "return confirm('Are you sure you want to delete this item?');";
            }
        }

        /// <summary>
        /// Related Items Page Index Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridTieredPricing_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridThumb.PageIndex = e.NewPageIndex;
            this.BindTieredPricing();
        }

        /// <summary>
        /// Tiered Prcing Delete Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridTieredPricing_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindTieredPricing();
        }


        /// <summary>
        /// Grid Row Command Event - Grid Tiered Pricing
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridTieredPricing_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "Edit")
                {
                    Response.Redirect(this.AddTieredPricingPageLink + this.ItemId + "&tierid=" + e.CommandArgument.ToString());
                }

                if (e.CommandName == "Delete")
                {
                    ZNode.Libraries.Admin.ProductAdmin productAdmin = new ProductAdmin();
                    bool Status = productAdmin.DeleteProductTierById(int.Parse(e.CommandArgument.ToString()));
                    if (Status)
                    {
                        Product product = productAdmin.GetByProductId(this.ItemId);
                        string Associatename = "Delete Tiered Pricing - " + product.Name;
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, Associatename, product.Name);
                        this.BindTieredPricing();
                    }
                }
            }
        }
        #endregion

        #region Related to product Highlights
        /// <summary>
        /// Add Client side event to the Delete Button in the Grid.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridHighlights_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Retrieve the Button control from the Seventh column.
                LinkButton DeleteButton = (LinkButton)e.Row.Cells[1].FindControl("DeleteHighlight");

                // Add Client Side confirmation
                DeleteButton.OnClientClick = "return confirm('Are you sure you want to delete this item?');";
            }
        }

        /// <summary>
        /// Highlights grid Row Deleting Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridHighlights_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindProductHighlights();
        }

        /// <summary>
        /// product highlight Items Page Index Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridHighlights_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGridHighlights.PageIndex = e.NewPageIndex;
            this.BindProductHighlights();
        }

        /// <summary>
        /// Product highlights row command event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridHighlights_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "Delete")
                {
                    // Delete button is triggered
                    ZNode.Libraries.Admin.ProductAdmin productAdmin = new ProductAdmin();

                    ProductHighlightService productHighlightService = new ProductHighlightService();
                    ProductHighlight productHighlight = new ProductHighlight();
                    productHighlight = productHighlightService.GetByProductHighlightID(int.Parse(e.CommandArgument.ToString()));
                    productHighlightService.DeepLoad(productHighlight);
                    string highlightname = productHighlight.HighlightIDSource.Name;

                    bool Status = productAdmin.DeleteProductHighlight(int.Parse(e.CommandArgument.ToString()));

                    Product product = productAdmin.GetByProductId(this.ItemId);
                    string Associatename = "Delete Association between  Highlights " + highlightname + " - " + product.Name;
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, Associatename, product.Name);

                    this.BindProductHighlights();
                }
                else if (e.CommandName == "Edit")
                {
                    Response.Redirect(this.EditHighlightPageLink + e.CommandArgument + "&productid=" + this.ItemId);
                }
            }
        }
        #endregion

        #region Related to Digital Asset
        /// <summary>
        /// Add Client side event to the Delete Button in the Grid.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridDigitalAsset_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Retrieve the Button control from the Seventh column.
                LinkButton DeleteButton = (LinkButton)e.Row.Cells[3].FindControl("btnRemoveDigitalAsset");

                // Add Client Side confirmation
                DeleteButton.OnClientClick = "return confirm('Are you sure you want to delete this item?');";
            }
        }

        /// <summary>
        /// Digital asset grid Row Deleting Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridDigitalAsset_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindDigitalAssets();
        }

        /// <summary>
        /// Digital asset grid Page Index Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridDigitalAsset_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGridDigitalAsset.PageIndex = e.NewPageIndex;
            this.BindDigitalAssets();
        }

        /// <summary>
        /// Grid Row Command Event - Digital Asset
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridDigitalAsset_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "Delete")
                {
                    ZNode.Libraries.Admin.ProductAdmin productAdmin = new ProductAdmin();

                    DigitalAssetService digitalService = new DigitalAssetService();
                    DigitalAsset digitalAsset = new DigitalAsset();
                    digitalAsset = digitalService.GetByDigitalAssetID(int.Parse(e.CommandArgument.ToString()));
                    digitalService.DeepLoad(digitalAsset);
                    string digitalassetname = digitalAsset.DigitalAsset.ToString();

                    bool Status = productAdmin.DeleteDigitalAsset(int.Parse(e.CommandArgument.ToString()));

                    Product product = productAdmin.GetByProductId(this.ItemId);
                    string Associatename = "Delete Digital Asset " + digitalassetname + " - " + product.Name;
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, Associatename, product.Name);

                    if (Status)
                    {
                        this.BindDigitalAssets();
                    }
                }
            }
        }
        #endregion

        #region Events for ProductAddon

        /// <summary>
        /// Add AddOn Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddNewAddOn_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddProductAddOnLink + "&itemid=" + this.ItemId);
        }

        #endregion

        #region Events for Highlights
        /// <summary>
        /// Add New Highlight button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddHighlight_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddHighlightPageLink + this.ItemId);
        }

        /// <summary>
        /// Add New bundle button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddBundleProducts_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddBundleProductsLink + this.ItemId);
        }

        /// <summary>
        /// Add Digital asset button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddDigitalAsset_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddDigitalAssetPageLink + this.ItemId);
        }

        /// <summary>
        /// Add Tiered pricing button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddTieredPricing_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddTieredPricingPageLink + this.ItemId);
        }

        /// <summary>
        /// Add New product sku button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddSKU_Click(object sender, EventArgs e)
        {
            ZNode.Libraries.Admin.ProductAdmin prodAdmin = new ProductAdmin();
            Product product = prodAdmin.GetByProductId(this.ItemId);
            Response.Redirect(this.AddSKULink + this.ItemId + "&typeid=" + product.ProductTypeID);
        }

        /// <summary>
        /// Redirecting to add new Related Item for a Product
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddRelatedItems_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddRelatedItemLink + this.ItemId);
        }

        /// <summary>
        /// Redirecting to Product Edit Page
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void EditProduct_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.EditPageLink + this.ItemId);
        }

        /// <summary>
        /// Redirecting to product advance setting page
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void EditAdvancedSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.EditAdvancedPageLink + this.ItemId);
        }

        /// <summary>
        /// Redirecting to product SEO setting page
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void EditSEOSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.EditSEOLink + this.ItemId);
        }

        /// <summary>
        /// Redirecting to product additional Info page
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void EditAdditionalInfo_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.EditAdditionalInfoLink + this.ItemId);
        }

        /// <summary>
        /// Redirecting to Product List Page
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ProductList_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ListLink);
        }

        /// <summary>
        /// Redirecting to product views page
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddProductView_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddViewlink + this.ItemId);
        }

        /// <summary>
        /// Back Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ListLink);
        }

        /// <summary>
        /// Search button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchData();
        }

        /// <summary>
        /// Clear button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            ProductAdmin _adminAccess = new ProductAdmin();
            DataSet ds = _adminAccess.GetProductDetails(this.ItemId);

            // Check for Number of Rows
            if (ds.Tables[0].Rows.Count != 0)
            {
                // Check For Product Type
                this.ProductTypeID = int.Parse(ds.Tables[0].Rows[0]["ProductTypeId"].ToString());
            }

            // For Attribute value.
            string Attributes = String.Empty;

            // GetAttribute for this ProductType
            DataSet MyAttributeTypeDataSet = _adminAccess.GetAttributeTypeByProductTypeID(this.ProductTypeID);

            foreach (DataRow MyDataRow in MyAttributeTypeDataSet.Tables[0].Rows)
            {
                System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + MyDataRow["AttributeTypeId"].ToString());
                lstControl.SelectedIndex = 0;
            }

            this.BindSKU();
        }

        /// <summary>
        /// Redirect to Locale based Product view page.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        //protected void DdlLocales_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    // Redirect, if user select other language of same product.
        //    string url = "view.aspx?itemid=" + ddlLocales.SelectedValue.Split('-')[0];

        //    if (ddlLocales.SelectedValue.Split('-').Length > 1)
        //    {
        //        url += "&l=" + ddlLocales.SelectedValue.Split('-')[1] + "&mode=" + this.GetActiveTabName(this.SelectedTabIndex);
        //    }

        //    Response.Redirect(url);
        //}
        #endregion

        #region Events for Departments Tab

        protected void GvProductDepartment_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProductDepartment.PageIndex = e.NewPageIndex;
            this.BindProductDepartment();
        }

        protected void GvProductDepartment_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "RemoveItem")
                {
                    ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();
                    bool isDeleted = productCategoryAdmin.Delete(int.Parse(e.CommandArgument.ToString()));
                    if (isDeleted)
                    {
                        this.BindProductDepartment();
                    }
                }
            }
        }

        protected void BtnAssociateDepartment_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.associateCategoryPageLink + this.ItemId);
        }
        #endregion

        #region Bind Methods

        /// <summary>
        /// Bind the locale dropdown, if this product have different locale.
        /// </summary>
        /// <param name="productNum">The value of Product Number</param>
        //private void BindLocaleDropdown(string productNum)
        //{
        //    // Get the Locale information with product id.
        //    DataSet dataSetLocale = ProductAdmin.GetLocaleIdsByProductId(productNum);
        //    dataSetLocale.Tables[0].Columns.Add("ddlValue", typeof(string));
        //    string locale = string.Empty;
        //    foreach (DataRow dataRow in dataSetLocale.Tables[0].Rows)
        //    {
        //        dataRow["ddlValue"] = dataRow["ProductId"] + "-" + dataRow["LocaleId"];
        //        if (dataRow["ProductId"].ToString() == this.ItemId.ToString())
        //        {
        //            locale = "-" + dataRow["LocaleId"].ToString();
        //        }
        //    }

        //    // Bind the dropdown.
        //    ddlLocales.DataSource = dataSetLocale.Tables[0];
        //    ddlLocales.DataTextField = "LocaleDescription";
        //    ddlLocales.DataValueField = "ddlValue";
        //    ddlLocales.DataBind();

        //    AssociatedLocale.Visible = true;

        //    if (Request.QueryString["l"] != null)
        //    {
        //        locale = "-" + Request.QueryString["l"].ToString();
        //    }

        //    if (ddlLocales.Items.Count == 0)
        //    {
        //        AssociatedLocale.Visible = false;
        //    }
        //    else
        //    {
        //        // Select the current language.
        //        ddlLocales.SelectedValue = this.ItemId.ToString() + locale;
        //    }
        //}

        /// <summary>
        /// Bind Attributes List.
        /// </summary>
        private void BindSkuAttributes()
        {
            ProductAdmin adminAccess = new ProductAdmin();

            DataSet MyDataSet = adminAccess.GetAttributeTypeByProductTypeID(this.ProductTypeID);

            int i = 1;
            Table tbl = new Table();
            tbl.ID = "tbl1";
            foreach (DataRow dr in MyDataSet.Tables[0].Rows)
            {
                // Bind Attributes
                DataSet AttributeDataSet = adminAccess.GetAttributesByAttributeTypeIdandProductID(int.Parse(dr["attributetypeid"].ToString()), this.ItemId);

                System.Web.UI.WebControls.DropDownList lstControl = new DropDownList();
                lstControl.ID = "lstAttribute" + dr["AttributeTypeId"].ToString();

                string fname = dr["Name"].ToString();
                ListItem li = new ListItem("Select " + fname, "0");
                lstControl.DataSource = AttributeDataSet;
                lstControl.DataTextField = "Name";
                lstControl.DataValueField = "AttributeId";
                lstControl.DataBind();

                lstControl.Items.Insert(0, li);
                Label lblname = new Label();
                lblname.Text = fname;
                lblname.Font.Name = "Verdana";
                lblname.Font.Size = 10;

                TableRow rw = new TableRow();

                TableCell name = new TableCell();
                name.Controls.Add(lblname);
                rw.Cells.Add(name);
                name = new TableCell();
                name.Text = ":";
                rw.Cells.Add(name);
                TableCell control = new TableCell();
                control.Controls.Add(lstControl);
                rw.Cells.Add(control);

                tbl.Rows.Add(rw);

            }
            ControlPlaceHolder.Controls.Add(tbl);
            this._HasAttributes = MyDataSet.Tables[0].Rows.Count > 0;
            pnlSKUAttributes.Visible = MyDataSet.Tables[0].Rows.Count > 0;
        }


        /// <summary>
        /// Bind Inventory Grid
        /// </summary>
        private void BindSKU()
        {
            SKUAdmin SkuAdmin = new SKUAdmin();
            TList<SKU> skuList = SkuAdmin.GetByProductID(this.ItemId);

            skuList.Sort("SKU");

            uxGridInventoryDisplay.DataSource = skuList;
            uxGridInventoryDisplay.DataBind();
        }

        /// <summary>
        /// Binds the Search data
        /// </summary>
        private void BindSearchData()
        {
            ProductAdmin adminAccess = new ProductAdmin();
            DataSet ds = adminAccess.GetProductDetails(this.ItemId);

            // Check for Number of Rows
            if (ds.Tables[0].Rows.Count != 0)
            {
                // Check For Product Type
                this.ProductTypeID = int.Parse(ds.Tables[0].Rows[0]["ProductTypeId"].ToString());
            }

            // For Attribute value.
            string Attributes = String.Empty;

            // GetAttribute for this ProductType
            DataSet MyAttributeTypeDataSet = adminAccess.GetAttributeTypeByProductTypeID(this.ProductTypeID);

            foreach (DataRow MyDataRow in MyAttributeTypeDataSet.Tables[0].Rows)
            {
                System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + MyDataRow["AttributeTypeId"].ToString());

                if (lstControl != null)
                {
                    int selValue = int.Parse(lstControl.SelectedValue);

                    if (selValue > 0)
                    {
                        Attributes += selValue.ToString() + ",";
                    }
                }
            }

            // If Attributes length is more than zero.
            if (Attributes.Length > 0)
            {
                // Split the string
                string Attribute = Attributes.Substring(0, Attributes.Length - 1);

                if (Attribute.Length > 0)
                {
                    SKUAdmin _SkuAdmin = new SKUAdmin();
                    DataSet MyDatas = _SkuAdmin.GetBySKUAttributes(this.ItemId, Attribute);

                    DataView Sku = new DataView(MyDatas.Tables[0]);
                    Sku.Sort = "SKU";
                    uxGridInventoryDisplay.DataSource = Sku;
                    uxGridInventoryDisplay.DataBind();
                }
            }
            else
            {
                this.BindSKU();
            }
        }

        /// <summary>
        /// Binding Product tiered pricing for this product
        /// </summary>
        private void BindTieredPricing()
        {
            // Create Instance for Product Admin and Product entity
            ZNode.Libraries.Admin.ProductAdmin ProdAdmin = new ProductAdmin();
            TList<ProductTier> productTierList = ProdAdmin.GetTieredPricingByProductId(this.ItemId);

            uxGridTieredPricing.DataSource = productTierList;
            uxGridTieredPricing.DataBind();
        }

        /// <summary>
        /// Binding Product Values into label Boxes
        /// </summary>
        private void BindViewData()
        {
            // Create Instance for Product Admin and Product entity
            ZNode.Libraries.Admin.ProductAdmin prodAdmin = new ProductAdmin();
            Product product = prodAdmin.GetByProductId(this.ItemId);

            DataSet ds = prodAdmin.GetProductDetails(this.ItemId);
            int Count = 0;
            bool isGiftCard = false;

            // Check for Number of Rows
            if (ds.Tables[0].Rows.Count != 0)
            {
                // Checking the user profile for roles and permission and then loading the data based on roles
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (!UserStoreAccess.CheckUserRoleInProduct(profiles, this.ItemId))
                {
                    Response.Redirect("Default.aspx", true);
                }

                // Bind ProductType,Manufacturer,Supplier
                lblProdType.Text = ds.Tables[0].Rows[0]["producttype name"].ToString();
                if (ds.Tables[0].Rows[0]["IsGiftCard"] != DBNull.Value)
                {
                    isGiftCard = Convert.ToBoolean(ds.Tables[0].Rows[0]["IsGiftCard"]);
                }

                // Check For Product Type
                this.ProductTypeID = int.Parse(ds.Tables[0].Rows[0]["ProductTypeId"].ToString());
                Count = prodAdmin.GetAttributeCount(int.Parse(ds.Tables[0].Rows[0]["ProductTypeId"].ToString()));

                // If the attribute count is not zero, then its not default product type 
                // So we will hide the bundle tab, if the product doesn't have default product type
                if (Count != 0)
                {
                    pnlBundleProduct.Enabled = false;
                }
            }

            if (product != null)
            {

                // Bind Locale dropdown for the this product.
                // this.BindLocaleDropdown(product.ProductNum);

                //Bind marketplace publishing button.
                this.BindMarketplacePublishingButton();

                // General Information
                lblProdName.Text = product.Name;
                lblProdNum.Text = Server.HtmlEncode(product.ProductNum.ToString());

                if (string.IsNullOrEmpty(ds.Tables[0].Rows[0]["Supplier Name"].ToString()))
                {
                    lblSupplierName.Text = "All";
                }
                else
                {
                    lblSupplierName.Text = ds.Tables[0].Rows[0]["Supplier Name"].ToString();
                }

                if (product.MinQty.HasValue)
                {
                    lblMinQuantity.Text = product.MinQty.Value.ToString();
                }

                if (product.MaxQty.HasValue)
                {
                    lblMaxQuantity.Text = product.MaxQty.Value.ToString();
                }

                // If product type is gift card then display the expiration detaiils.
                if (isGiftCard)
                {
                    divExpirationPeriod.Visible = true;
                    int expirationFrequency = product.ExpirationFrequency.Value;
                    string frequency = "Day(s)";
                    if (expirationFrequency == 7)
                    {
                        frequency = "Week(s)";
                    }

                    if (expirationFrequency == 30)
                    {
                        frequency = "Month(s)";
                    }

                    if (expirationFrequency == 365)
                    {
                        frequency = "Year(s)";
                    }

                    lblExpirationPeriod.Text = string.Format("{0} {1}", product.ExpirationPeriod.Value.ToString(), frequency);
                }

                ZNodeImage znodeImage = new ZNodeImage();
                ItemImage.ImageUrl = znodeImage.GetImageHttpPathMedium(product.ImageFile);

                // Product Description and Features
                lblShortDescription.Text = Server.HtmlDecode(product.ShortDescription);
                lblProductDescription.Text = Server.HtmlDecode(product.Description);
                lblProductFeatures.Text = Server.HtmlDecode(product.FeaturesDesc);
                lblproductspecification.Text = Server.HtmlDecode(product.Specifications);
                lbladditionalinfo.Text = Server.HtmlDecode(product.AdditionalInformation);
                lblDownloadLink.Text = product.DownloadLink;

                // Product Attributes
                if (product.RetailPrice.HasValue)
                {
                    lblRetailPrice.Text = product.RetailPrice.Value.ToString("c");
                }

                if (product.WholesalePrice.HasValue)
                {
                    lblWholeSalePrice.Text = product.WholesalePrice.Value.ToString("c");
                }

                lblSalePrice.Text = this.FormatPrice(product.SalePrice);
                lblWeight.Text = this.FormatProductWeight(product.Weight);

                if (product.Height.HasValue)
                {
                    lblHeight.Text = product.Height.Value.ToString("N2") + " " + ZNodeConfigManager.SiteConfig.DimensionUnit;
                }

                if (product.Width.HasValue)
                {
                    lblWidth.Text = product.Width.Value.ToString("N2") + " " + ZNodeConfigManager.SiteConfig.DimensionUnit;
                }

                if (product.Length.HasValue)
                {
                    lblLength.Text = product.Length.Value.ToString("N2") + " " + ZNodeConfigManager.SiteConfig.DimensionUnit;
                }

                // Display Settings
                chkProductEnabled.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(product.ActiveInd.ToString()));
                lblProdDisplayOrder.Text = product.DisplayOrder.ToString();
                chkIsSpecialProduct.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(this.DisplayVisible(product.HomepageSpecial));
                chkIsNewItem.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(this.DisplayVisible(product.NewProductInd));
                chkProductPricing.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(this.DisplayVisible(product.CallForPricing));
                chkproductInventory.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(this.DisplayVisible(product.InventoryDisplay));
                chkFranchisable.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(this.DisplayVisible(product.Franchisable));

                // Display Tax Class Name
                if (product.TaxClassID.HasValue)
                {
                    TaxRuleAdmin taxRuleAdmin = new TaxRuleAdmin();
                    TaxClass taxClass = taxRuleAdmin.GetByTaxClassID(product.TaxClassID.Value);

                    if (taxClass != null)
                    {
                        lblTaxClass.Text = taxClass.Name;
                    }
                }

                // Recurring Billing
                imgRecurringBillingInd.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(this.DisplayVisible(product.RecurringBillingInd));
                if (product.RecurringBillingInitialAmount.HasValue)
                    lblRecurringBillingInitialAmount.Text = product.RecurringBillingInitialAmount.Value.ToString("c");
                lblBillingPeriod.Text = product.RecurringBillingPeriod;

                shipSeparatelyInd.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(product.ShipSeparately);

                // SEO 
                lblSEODescription.Text = product.SEODescription;
                lblSEOKeywords.Text = product.SEOKeywords;
                lblSEOTitle.Text = product.SEOTitle;
                lblSEOURL.Text = product.SEOURL;

                // Inventory Setting - Out of Stock Options
                if (product.TrackInventoryInd.HasValue && product.AllowBackOrder.HasValue)
                {
                    if (product.TrackInventoryInd.Value && product.AllowBackOrder.Value == false)
                    {
                        chkCartInventoryEnabled.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse("true"));
                        chkIsBackOrderEnabled.Src = this.SetCheckMarkImage();
                        chkIstrackInvEnabled.Src = this.SetCheckMarkImage();
                    }
                    else if (product.TrackInventoryInd.Value && product.AllowBackOrder.Value)
                    {
                        chkCartInventoryEnabled.Src = this.SetCheckMarkImage();
                        chkIsBackOrderEnabled.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse("true"));
                        chkIstrackInvEnabled.Src = this.SetCheckMarkImage();
                    }
                    else if ((product.TrackInventoryInd.Value == false) && (product.AllowBackOrder.Value == false))
                    {
                        chkCartInventoryEnabled.Src = this.SetCheckMarkImage();
                        chkIsBackOrderEnabled.Src = this.SetCheckMarkImage();
                        chkIstrackInvEnabled.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse("true"));
                    }
                }
                else
                {
                    chkCartInventoryEnabled.Src = this.SetCheckMarkImage();
                    chkIsBackOrderEnabled.Src = this.SetCheckMarkImage();
                    chkIstrackInvEnabled.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse("true"));
                }

                // Inventory Setting - Stock Messages
                lblInStockMsg.Text = product.InStockMsg;
                lblOutofStock.Text = product.OutOfStockMsg;
                lblBackOrderMsg.Text = product.BackOrderMsg;

                if (product.DropShipInd.HasValue)
                {
                    IsDropShipEnabled.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(product.DropShipInd.Value);
                }
                else
                {
                    IsDropShipEnabled.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse("false"));
                }

                if (product.FeaturedInd)
                {
                    FeaturedProduct.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
                }
                else
                {
                    FeaturedProduct.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(false);
                }

                // Bind ShippingRule type
                if (product.ShippingRuleTypeID.HasValue)
                {
                    lblShippingRuleTypeName.Text = this.GetShippingRuleTypeName(product.ShippingRuleTypeID.Value);

                    if (product.ShippingRate.HasValue && product.ShippingRuleTypeID.Value == (int)ZNode.Libraries.ECommerce.Shipping.ZNodeShippingRuleType.FixedRatePerItem)
                    {
                        lblShippingRate.Text = product.ShippingRate.Value.ToString("c");
                    }
                }

                if (product.FreeShippingInd.HasValue)
                {
                    freeShippingInd.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(product.FreeShippingInd.Value);
                }
                else
                {
                    freeShippingInd.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(false);
                }

                if (ZNodeConfigManager.EnvironmentConfig.LicenseType == ZNodeLicenseType.Server)
                {
                    pnlFranchise.Visible = false;
                }
                else
                {
                    pnlFranchise.Visible = true;
                }

                //Zeon Custom Code:Start
                SetProductExtensionData(product.ProductID);
                //Zeon Custom Code:Ends

                // Bind Grid - Product Related Items
                this.BindRelatedItems();

                // Bind Grid - Bundle Products
                this.BindBundleProducts();

                // Bind Image
                this.BindImageDatas();

                // Bind Grid - Product Addons
                this.BindProductAddons();

                // Tiered Pricing
                this.BindTieredPricing();

                // Digital Asset
                this.BindDigitalAssets();

                // Bind product highlights
                this.BindProductHighlights();

                this.BindProductDepartment();

                //Zeon Custom Code: Bind Specification Grid
                this.BindSpecificationItems();

                this.hdnActiveTabIndex.Value = this.SelectedTabIndex.ToString();
            }
            else
            {
                throw new ApplicationException("Product Requested could not be found.");
            }
        }

        private void BindMarketplacePublishingButton()
        {
            //            if(_isXCommercePluginInstalled)
            //            {
            //                var rep = new ZNode.Libraries.DataAccess.Repository.ZNodeRepository(new ZNodeContext("name=ZNodeEntities"));
            //                var authorizedAccounts = rep.Get<XCommerceMarketplaceAuthorizations>().ToList().Select(a => string.Format(@"<li><a href=""/plugins/x-commerce/admin/product/mapping?productId={0}&accountid={1}"">{2}</a></li>", this.ItemId, a.AccountID, a.UserID));

            //                btnXCommerceAdmin.InnerHtml =
            //                    string.Format(@"<div class=""btn-group"" style=""float: right;margin-top: -3px;"">
            //                        <button class=""btn btn-mini btn-primary dropdown-toggle"" data-toggle=""dropdown"">Publish <span class=""caret""></span></button>
            //                        <ul class=""dropdown-menu"" id=""lstMarketplacePublishOptions"">
            //                            {0}
            //                            <li class=""divider""></li>
            //                            <li><a href=""/plugins/x-commerce/admin/MarketplaceAccounts?productId={1}"">{2}</a></li>
            //                        </ul>
            //                    </div>", string.Join(System.Environment.NewLine, authorizedAccounts), this.ItemId, authorizedAccounts.Any() ? "Manage Accounts" : "Add Account");
            //            }
        }

        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindProductHighlights()
        {
            ProductViewAdmin imageadmin = new ProductViewAdmin();
            uxGridHighlights.DataSource = imageadmin.GetAllHighlightImage(this.ItemId);
            uxGridHighlights.DataBind();
        }

        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindProductAddons()
        {
            ProductAddOnAdmin ProdAddonAdminAccess = new ProductAddOnAdmin();

            // Bind Associated Addons for this product
            uxGridProductAddOns.DataSource = ProdAddonAdminAccess.GetByProductId(this.ItemId);
            uxGridProductAddOns.DataBind();
        }

        /// <summary>
        /// Bind Related Products 
        /// </summary>
        private void BindRelatedItems()
        {
            //Zeon Custom Code : Start 
            //ProductCrossSellAdmin ProdCrossSellAccess = new ProductCrossSellAdmin();
            //TList<ProductCrossSell> productCrossSellList = ProdCrossSellAccess.GetRelatedItemsByProductId(this.ItemId);
            //uxGrid.DataSource = productCrossSellList;
            //uxGrid.DataBind();

            ProductCrossSellAdmin ProdCrossSellAccess = new ProductCrossSellAdmin();
            DataSet dsProductCrossSellItems = ProdCrossSellAccess.GetByProductID(ItemId);
            uxGrid.DataSource = dsProductCrossSellItems;
            uxGrid.DataBind();

            //Zeon Custom Code : End
        }

        /// <summary>
        /// Bind Bundle Products
        /// </summary>
        private void BindBundleProducts()
        {
            ParentChildProductAdmin parentChildProduct = new ParentChildProductAdmin();
            TList<ParentChildProduct> parentChildProductList = parentChildProduct.GetParentChildProductByProductId(this.ItemId);
            uxBundleProductGrid.DataSource = parentChildProductList;
            uxBundleProductGrid.DataBind();
        }

        /// <summary>
        /// Bind Image Method
        /// </summary>
        private void BindImage()
        {
            ZNode.Libraries.Admin.StoreSettingsAdmin imageadmin = new StoreSettingsAdmin();
            GridThumb.DataSource = imageadmin.Getall();
            GridThumb.DataBind();
        }

        /// <summary>
        /// Bind Related ImageViews 
        /// </summary>
        private void BindImageDatas()
        {
            try
            {
                ProductViewAdmin imageadmin = new ProductViewAdmin();
                DataSet dsAlternateImage = new DataSet();
                dsAlternateImage = imageadmin.GetAllAlternateImage(this.ItemId);
                if (dsAlternateImage.Tables[0].Rows.Count > 0)
                {
                    for (int index = 0; index < dsAlternateImage.Tables[0].Rows.Count; index++)
                    {
                        dsAlternateImage.Tables[0].Rows[index]["Name"] = Server.HtmlDecode(dsAlternateImage.Tables[0].Rows[index]["Name"].ToString());
                    }
                }

                GridThumb.DataSource = dsAlternateImage;
                GridThumb.DataBind();
            }
            catch (Exception ex)
            {
                GridThumb.Visible = false;
                ZNodeLogging.LogMessage(ex.StackTrace.ToString());
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "Image File Currupted", "View Product", "In View.BindImageDatas", "Fail", ex.StackTrace.ToString());
                Zeon.Libraries.Elmah.ElmahErrorManager.Log(ex.ToString());
            }

        }

        /// <summary>
        /// Bind Digital assets for this product
        /// </summary>
        private void BindDigitalAssets()
        {
            ZNode.Libraries.Admin.ProductAdmin productAdmin = new ProductAdmin();
            uxGridDigitalAsset.DataSource = productAdmin.GetDigitAssetByProductId(this.ItemId);
            uxGridDigitalAsset.DataBind();
        }

        /// <summary>
        /// Bind the product department. 
        /// </summary>
        private void BindProductDepartment()
        {
            ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();
            DataSet ds = productCategoryAdmin.GetByProductID(this.ItemId);
            gvProductDepartment.DataSource = ds.Tables[0];
            gvProductDepartment.DataBind();
        }

        /// <summary>
        /// This will automatically pre-select the tab according the query string value(mode)
        /// </summary>
        private void ResetTab()
        {
            if (this.Mode.Equals("advanced"))
            {
                // Set Advanced Settings as active tab 
                tabProductDetails.ActiveTab = pnlAdvancedSettings;
            }
            else if (this.Mode.Equals("departments"))
            {
                // Activate the Departments tab.
                tabProductDetails.ActiveTab = TabDepartments;
            }
            else if (this.Mode.Equals("inventory"))
            {
                // For Manage Inventory
                tabProductDetails.ActiveTab = pnlManageinventory;
            }
            else if (this.Mode.Equals("bundle"))
            {
                // For Bundle Products
                tabProductDetails.ActiveTab = pnlBundleProduct;
            }
            else if (this.Mode.Equals("tag"))
            {
                // For Tags
                tabProductDetails.ActiveTab = pnlTags;
            }
            else if (this.Mode.Equals("crosssell"))
            {
                // For Related Items
                tabProductDetails.ActiveTab = pnlRelatedItems;
            }
            else if (this.Mode.Equals("views"))
            {
                // For Related Images
                tabProductDetails.ActiveTab = pnlAlternateImages;
            }
            else if (this.Mode.Equals("addons"))
            {
                // For Product Options
                tabProductDetails.ActiveTab = pnlProductOptions;
            }
            else if (this.Mode.Equals("tieredPricing"))
            {
                // For Product Tiered pricing tab
                tabProductDetails.ActiveTab = pnlTieredPricing;
            }
            else if (this.Mode.Equals("highlight"))
            {
                // For Product Highlights tab
                tabProductDetails.ActiveTab = pnlHighlights;
            }
            else if (this.Mode.Equals("digitalAsset"))
            {
                // For product digital assets tab
                tabProductDetails.ActiveTab = pnlDigitalAsset;
            }
            else if (this.Mode.Equals("Spec"))
            {
                //For Specification tab
                tabProductDetails.ActiveTab = pnlProductSpecifications;
            }
            else
            {
                // Product Info
                tabProductDetails.ActiveTabIndex = 0;
            }
        }

        #endregion

        #region Zeon Custom Code

        /// <summary>
        /// Redirecting to add Product Specification
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddSpecItems_Click(object sender, EventArgs e)
        {
            Response.Redirect(AddSpecItemLink + ItemId);
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void uxSpecGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("RemoveItem"))
            {
                ZeonProductSpecificationsService SpecService = new ZeonProductSpecificationsService();
                bool Check = SpecService.Delete((int.Parse(e.CommandArgument.ToString())));

                if (Check)
                {
                    this.BindSpecificationItems();
                }
            }
        }

        /// <summary>
        /// Product Specification Index Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void uxSpecGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxSpecGrid.PageIndex = e.NewPageIndex;
            this.BindSpecificationItems();
        }

        /// <summary>
        /// Zeon Custom code
        /// Bind Products Specification
        /// </summary>
        private void BindSpecificationItems()
        {
            ProductSpecificationsHelper ProdSpec = new ProductSpecificationsHelper();
            uxSpecGrid.DataSource = ProdSpec.GetProductSpecifications_ByProductIdSkuId(ItemId, 0);
            uxSpecGrid.DataBind();
        }

        /// <summary>
        /// set Product Extn Data
        /// </summary>
        /// <param name="productID">int</param>
        private void SetProductExtensionData(int productID)
        {
            ProductExtnService productExtnSer = new ProductExtnService();
            TList<ProductExtn> productExtnList = productExtnSer.GetByProductID(productID);
            bool isMapPrice = false, isPreOrder = false,isIndFollow=false;
            if (productExtnList != null && productExtnList.Count > 0)
            {
                isPreOrder = productExtnList[0].PreOrderFlag;
                isMapPrice = productExtnList[0].IsMapPrice;
                isIndFollow= productExtnList[0].IsIndexFollow;
                lblVideoUrl.Text = productExtnList[0].VideoUrl;
            }
            imgPreOrder.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(isPreOrder);
            imgMapPrice.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(isMapPrice);
            imgIndexFollow.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(isIndFollow);
        }

        #endregion
    }
}