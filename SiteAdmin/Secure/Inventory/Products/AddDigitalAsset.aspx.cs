using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the SiteAdmin  Admin_Secure_catalog_DigitalAsset_add class
    /// </summary>
    public partial class AddDigitalAsset : System.Web.UI.Page
    {
        #region Protected Variables
        private int ItemId;
        #endregion

        #region Bind Property
        /// <summary>
        /// Gets the product name
        /// </summary>
        protected string SetProductName
        {
            get
            {
                ProductAdmin AdminAccess = new ProductAdmin();
                Product entity = AdminAccess.GetByProductId(this.ItemId);

                if (entity != null)
                {
                    return entity.Name;
                }

                return string.Empty;
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (Page.IsPostBack == false)
            {
                // If edit func then bind the data fields
                if (this.ItemId > 0)
                {
                    lblTitle.Text += this.SetProductName;
                }
                else
                {
                    throw new ApplicationException("Product Requested could not be found.");
                }
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Event fierd when submit button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ProductAdmin AdminAccess = new ProductAdmin();

            DigitalAsset entity = new DigitalAsset();
            entity.DigitalAsset = Server.HtmlEncode(txtDigitalAsset.Text.Trim());
            entity.ProductID = this.ItemId;
            entity.OrderLineItemID = null;

            bool Check = AdminAccess.AddDigitalAsset(entity);

            if (Check)
            {
                ProductAdmin productAdmin = new ProductAdmin();
                Product product = productAdmin.GetByProductId(this.ItemId);
                string Associatename = "Added Digital Asset " + txtDigitalAsset.Text + " - " + product.Name;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, Associatename, product.Name);

                Response.Redirect("~/SiteAdmin/Secure/Inventory/Products/View.aspx?mode=digitalAsset&itemid=" + this.ItemId);
            }
            else
            {
                lblMsg.Text = "Could not add the product category. Please try again.";
                return;
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Inventory/Products/View.aspx?mode=digitalAsset&itemid=" + this.ItemId);
        }
        #endregion
    }
}