<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Title="Manage Products - Add Addons" Inherits="SiteAdmin.Secure.Inventory.Products.AddAddons" CodeBehind="AddAddons.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <h1>
            <asp:Label ID="lblTitle" runat="server" Text="Associate Add-Ons for: "></asp:Label></h1>
        <div class="LeftFloat" style="width: 70%; text-align: left">

            <ZNode:DemoMode ID="DemoMode1" runat="server" />
        </div>
        <div class="ClearBoth">
        </div>
        <div>
            <asp:Label CssClass="Error" ID="lblAddOnErrorMessage" runat="server" EnableViewState="false"
                Visible="false"></asp:Label>
        </div>
        <h4 class="SubTitle">Search Add-Ons</h4>

        <asp:Panel ID="pnlAddOnSearch" DefaultButton="btnAddOnSearch" runat="server">
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">Name</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtAddonName" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">Title</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtAddOnTitle" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">SKU or Product#</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtAddOnsku" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle" runat="server" id="lblLocale">Locale</span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlLocales" runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Value="">All</asp:ListItem>
                            </asp:DropDownList>
                        </span>
                    </div>
                </div>
                <div class="ClearBoth">
                </div>
                <div>
                    <asp:ImageButton ID="btnAddOnClear" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_clear_highlight.gif';"
                        onmouseout="this.src='../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                        runat="server" AlternateText="Clear" OnClick="BtnAddOnClear_Click" />
                    <asp:ImageButton ID="btnAddOnSearch" onmouseover="this.src='../../../Themes/Images/buttons/button_search_highlight.gif';"
                        onmouseout="this.src='../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                        runat="server" AlternateText="Search" OnClick="BtnAddOnSearch_Click" />
                </div>
            </div>
        </asp:Panel>
        <br />
        <h4 class="GridTitle">Add-On List</h4>
        <!-- Update Panel for grid paging that are used to avoid the postbacks -->
        <asp:UpdatePanel ID="updPnlAddOnGrid" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:GridView ID="uxAddOnGrid" runat="server" CssClass="Grid" AllowPaging="True"
                    AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None"
                    OnPageIndexChanging="UxAddOnGrid_PageIndexChanging" CaptionAlign="Left" Width="100%"
                    EnableSortingAndPagingCallbacks="False" PageSize="15" AllowSorting="True" PagerSettings-Visible="true"
                    OnRowCommand="UxAddOnGrid_RowCommand" EmptyDataText="No product Add-Ons exist in the database.">
                    <Columns>
                        <asp:TemplateField HeaderText="Select" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkProductAddon" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="AddOnId" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="Title" HtmlEncode="false" HeaderText="Title" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# Eval("Name") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DisplayOrder" HeaderText="DisplayOrder" HeaderStyle-HorizontalAlign="Left" />
                    </Columns>
                    <FooterStyle CssClass="FooterStyle" />
                    <RowStyle CssClass="RowStyle" />
                    <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div>
            <uc1:Spacer ID="Spacer3" SpacerHeight="20" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="ImageButtons">
            <asp:Button ID="btnAddSelectedAddons" runat="server" Text="Associate Selected Add-ons"
                AlternateText="Submit" CssClass="Size175" OnClick="BtnAddSelectedAddons_Click" />&nbsp;
            <asp:ImageButton ID="btnBottomCancel" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>
