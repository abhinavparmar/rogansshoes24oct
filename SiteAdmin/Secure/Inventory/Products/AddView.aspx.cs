using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.DataAccess.Service;

namespace SiteAdmin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Site Admin  Admin_Secure_catalog_product_add_view class
    /// </summary>
    public partial class AddView : System.Web.UI.Page
    {
        #region Protected member variable
        private int ItemId = 0;
        private int ProductImageId = 0;
        private int productTypeID = 0;
        private string AssociateName = string.Empty;
        private string EditLink = "~/SiteAdmin/Secure/Inventory/Products/View.aspx?itemid=";
        private string CancelLink = "~/SiteAdmin/Secure/Inventory/Products/View.aspx?itemid=";
        private ProductViewAdmin imageAdmin = new ProductViewAdmin();
        private ProductImage productImage = new ProductImage();
        private ProductAdmin productAdmin = new ProductAdmin();
        private Product product = new Product();
        //Zeon Custom Code:Starts
        private int skuId = 0;
        private ZeonSKUImage skuImage = new ZeonSKUImage();
        private SKU _SKU = new SKU();
        private string skuEditLink ="AddSku.aspx?itemid=";
        //Zeon Custom Code:Ends
        #endregion

        #region Page load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (Request.Params["productimageid"] != null)
            {
                this.ProductImageId = int.Parse(Request.Params["productimageid"].ToString());
            }

            if (Request.Params["typeid"] != null)
            {
                this.productTypeID = int.Parse(Request.Params["typeid"].ToString());
            }
            //Zeon Custom Code:Start
            //SkuImage Implementation
            if (Request.Params["skuid"] != null)
            {
                this.skuId = int.Parse(Request.Params["skuid"].ToString());
            }
            //Zeon Custom Code:Ends
            if (!Page.IsPostBack)
            {
                product = productAdmin.GetByProductId(this.ItemId);
                if (((this.ItemId > 0) && (this.ProductImageId > 0)) && this.skuId<=0)
                {
                    lblHeading.Text = "Edit Alternate Image for " + this.product.Name;
                    this.BindDatas();
                    this.BindImageType();

                    tblShowImage.Visible = true;
                    //tblShowProductSwatchImage.Visible = true;
                    txtimagename.Visible = true;
                }
                else
                {
                    if (!LoadSKUImageSetting())//zeon code:checking for sku image setup
                    {
                        // Show the image upload control, when adding new product.
                        tblShowImage.Visible = true;
                        tblProductDescription.Visible = true;
                        pnlShowOption.Visible = false;
                        Image1.Visible = false;

                        lblHeading.Text = "Add Alternate Image for " + this.product.Name;
                        this.BindImageType();
                    }
                }
            }
        }
        #endregion

        #region General events
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            if (this.skuId > 0)
            {
                SaveSKUImage();
            }
            else
            {
                product = productAdmin.GetByProductId(this.ItemId);

                System.IO.FileInfo fileInfo = null;
                bool swatchSelected = false;
                bool newProductImage = false;

                if (this.ProductImageId > 0)
                {
                    this.productImage = this.imageAdmin.GetByProductImageID(this.ProductImageId);
                }

                if (this.productImage.ProductImageTypeID.GetValueOrDefault(0) != Convert.ToInt32(ImageType.SelectedValue))
                {
                    // If swatch is selected
                    swatchSelected = true && Convert.ToInt32(ImageType.SelectedValue) == 2;
                }

                this.productImage.Name = Server.HtmlEncode(txttitle.Text);
                this.productImage.ActiveInd = VisibleInd.Checked;
                this.productImage.ShowOnCategoryPage = VisibleCategoryInd.Checked;
                this.productImage.ProductID = this.ItemId;
                this.productImage.ProductImageTypeID = Convert.ToInt32(ImageType.SelectedValue);
                this.productImage.DisplayOrder = int.Parse(DisplayOrder.Text.Trim());
                this.productImage.ImageAltTag = txtImageAltTag.Text.Trim();
                this.productImage.AlternateThumbnailImageFile = txtAlternateThumbnail.Text.Trim();
                this.productImage.ReviewStateID = Convert.ToInt32(ZNodeProductReviewState.Approved);
                UploadProductImage.AppendToFileName = "-" + this.ItemId.ToString();
                // Validate image
                if ((this.ProductImageId == 0) || (RadioProductNewImage.Checked == true))
                {
                    if (!UploadProductImage.IsFileNameValid())
                    {
                        return;
                    }

                    // Check for Product View Image
                    fileInfo = UploadProductImage.FileInformation;
                    if (fileInfo != null)
                    {
                        if (!string.IsNullOrEmpty(product.PortalID.ToString()))
                        {
                            this.productImage.ImageFile = "Turnkey/" + product.PortalID + "/" + fileInfo.Name;
                        }
                        else if (!string.IsNullOrEmpty(product.AccountID.ToString()))
                        {
                            this.productImage.ImageFile = "Mall/" + product.AccountID + "/" + fileInfo.Name;
                        }
                        else
                        {
                            this.productImage.ImageFile = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + fileInfo.Name;
                            if (ImageType.SelectedItem.Value == "2")
                            {
                                this.productImage.AlternateThumbnailImageFile = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + fileInfo.Name;
                            }
                        }
                    }
                }
                else
                {
                    this.productImage.ImageFile = this.productImage.ImageFile;
                }


                // Upload File if this is a new product or the New Image option was selected for an existing product
                if (RadioProductNewImage.Checked || this.ProductImageId == 0)
                {
                    if (fileInfo != null)
                    {
                        if (!string.IsNullOrEmpty(product.PortalID.ToString()))
                        {
                            UploadProductImage.SaveImage("fadmin", product.PortalID.ToString(), "0");
                        }
                        else if (!string.IsNullOrEmpty(product.AccountID.ToString()))
                        {
                            UploadProductImage.SaveImage("madmin", "0", product.AccountID.ToString());
                        }
                        else
                        {
                            UploadProductImage.SaveImage("sadmin", ZNodeConfigManager.SiteConfig.PortalID.ToString(), "0");
                        }
                        // Release all resources used
                        UploadProductImage.Dispose();
                    }
                }

                if (swatchSelected && this.ProductImageId > 0 && !newProductImage)
                {
                    // Create fileInfo for Original product View Image
                    fileInfo = UploadProductImage.FileInformation;
                    if (fileInfo != null)
                    {
                        this.productImage.AlternateThumbnailImageFile = fileInfo.Name;
                    }
                }

                bool check = false;

                string AlternateImage = string.Empty;

                product = productAdmin.GetByProductId(this.ItemId);

                if (this.ProductImageId > 0)
                {
                    // Update the Imageview
                    this.AssociateName = "Edit Alternate Images " + this.productImage.ImageFile + " - " + product.Name;
                    check = this.imageAdmin.Update(this.productImage);
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, product.Name);
                }
                else
                {
                    this.AssociateName = "Added Alternate Images " + this.productImage.ImageFile + " - " + product.Name;
                    check = this.imageAdmin.Insert(this.productImage);
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, product.Name);
                }

                if (check)
                {
                    Response.Redirect(this.EditLink + this.ItemId + "&mode=views");
                }
                else
                {
                    // Display error message
                    lblError.Text = "An error occurred while updating. Please try again.";
                }
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            //Response.Redirect(this.CancelLink + this.ItemId + "&mode=views");//znode old code
            //zeon custom code to customize redirects
            if (this.skuId > 0)
            {
                Response.Redirect(this.skuEditLink + this.ItemId + "&skuid=" + this.skuId + "&typeid=" + this.productTypeID);
            }
            else
            {
                Response.Redirect(this.CancelLink + this.ItemId + "&mode=views");
            }
            
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioProductCurrentImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductDescription.Visible = false;
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioProductNewImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductDescription.Visible = true;
        }

        #endregion

        #region Bind event
        /// <summary>
        /// Bind Datas
        /// </summary>
        private void BindDatas()
        {
            this.productImage = this.imageAdmin.GetByProductImageID(this.ProductImageId);
            ZNodeImage znodeImage = new ZNodeImage();
            if (this.productImage != null)
            {
                txtimagename.Text = Server.HtmlDecode(this.productImage.ImageFile);
                txttitle.Text = Server.HtmlDecode(this.productImage.Name);
                VisibleInd.Checked = this.productImage.ActiveInd;
                VisibleCategoryInd.Checked = this.productImage.ShowOnCategoryPage;
                Image1.ImageUrl = znodeImage.GetImageHttpPathSmall(this.productImage.ImageFile);
                ImageType.SelectedValue = this.productImage.ProductImageTypeID.ToString();
                DisplayOrder.Text = this.productImage.DisplayOrder.GetValueOrDefault().ToString();
                txtImageAltTag.Text = this.productImage.ImageAltTag;
                txtAlternateThumbnail.Text = this.productImage.AlternateThumbnailImageFile;

                if (txtAlternateThumbnail.Text != string.Empty)
                {
                    Image1.ImageUrl = znodeImage.GetImageHttpPathSmall(this.productImage.AlternateThumbnailImageFile);
                }
                if (string.IsNullOrEmpty(Image1.ImageUrl))
                {
                    Image1.ImageUrl = znodeImage.GetImageHttpPathSmall(string.Empty);
                }
            }
        }

        /// <summary>
        /// Binds ImageType Type drop-down list
        /// </summary>
        private void BindImageType()
        {
            ZNode.Libraries.Admin.ProductViewAdmin imageadmin = new ProductViewAdmin();
            ImageType.DataSource = imageadmin.GetImageType();
            ImageType.DataTextField = "Name";
            ImageType.DataValueField = "ProductImageTypeID";
            ImageType.DataBind();

            //this.SetVisibleData();
        }

        /// <summary>
        /// Set Visible Data Method
        /// </summary>
        private void SetVisibleData()
        {
            AlternateThumbnail.Visible = false;
            SwatchHint.Visible = false;
            ProductHint.Visible = false;
            Image1.Visible = false;

            if (ImageType.SelectedItem.Value == "1")
            {
                AlternateThumbnail.Visible = false;
                lblImage.Text = "Product Image";
                lblImageName.Text = "Product Image File Name";
                ProductHint.Visible = true;
            }
            else if (ImageType.SelectedItem.Value == "2")
            {
                AlternateThumbnail.Visible = true;
                lblImage.Text = "Swatch Image";
                lblImageName.Text = "Swatch Image File Name";
                SwatchHint.Visible = true;
                Image1.Visible = true;
            }
        }
        #endregion

        #region Zeon Custom Code

        #region Private Methods

        /// <summary>
        /// Bind SKU IMAGE data
        /// </summary>
        private void BindSKUImageData()
        {
            ZeonSKUImageService zSKUImageSer = new ZeonSKUImageService();
            this.skuImage = zSKUImageSer.GetByZeonSKUImageID(this.ProductImageId);
            ZNodeImage znodeImage = new ZNodeImage();
            if (skuImage != null)
            {
                txtimagename.Text = Server.HtmlDecode(this.skuImage.ImageFile);
                txttitle.Text = Server.HtmlDecode(this.skuImage.Name);
                VisibleInd.Checked = this.skuImage.ActiveInd;
                VisibleCategoryInd.Checked = this.skuImage.ShowOnCategoryPage;
                Image1.ImageUrl = znodeImage.GetImageHttpPathSmall(this.skuImage.ImageFile);
                DisplayOrder.Text = this.skuImage.DisplayOrder.GetValueOrDefault().ToString();
                txtImageAltTag.Text = this.skuImage.ImageAltTag;
                txtAlternateThumbnail.Text = this.skuImage.AlternateThumbnailImageFile;

                if (string.IsNullOrEmpty(Image1.ImageUrl))
                {
                    Image1.ImageUrl = znodeImage.GetImageHttpPathSmall(string.Empty);
                }
            }
        }

        /// <summary>
        /// load sku image stting 
        /// </summary>
        /// <returns>bool</returns>
        private bool LoadSKUImageSetting()
        {
            SKUAdmin SkuAdmin = new SKUAdmin();
            _SKU = SkuAdmin.GetBySKUID(this.skuId);
            bool isSKUView = false;
            if ((this.skuId > 0) && (this.ProductImageId > 0))
            {
                //Edit Mode Setting
                
                lblHeading.Text = "Edit Alternate Image for " + this._SKU.SKU;
                BindSKUImageData();

                tblShowImage.Visible = true;
                txtimagename.Visible = true;
                isSKUView = true;
                divImageType.Visible = false;
            }
            else if (this.skuId > 0 && this.ProductImageId<=0)
            {
                //Add Mode Setting
                // Show the image upload control, when adding new product.
                tblShowImage.Visible = true;
                tblProductDescription.Visible = true;
                pnlShowOption.Visible = false;
                Image1.Visible = false;

                lblHeading.Text = "Add Alternate Image for " + this._SKU.SKU;
                isSKUView = true;
                divImageType.Visible = false;
            }
            else
            {
                divImageType.Visible = true;
            }
            return isSKUView;
        }

        /// <summary>
        /// save SKU image
        /// </summary>
        private void SaveSKUImage()
        {
           ZeonSKUImageService zSKUImageSer = new ZeonSKUImageService();

           try
           {
               System.IO.FileInfo fileInfo = null;
               bool newProductImage = false;

               if (this.ProductImageId > 0)
               {
                   this.skuImage = zSKUImageSer.GetByZeonSKUImageID(this.ProductImageId);
               }

               this.skuImage.Name = Server.HtmlEncode(txttitle.Text);
               this.skuImage.ActiveInd = VisibleInd.Checked;
               this.skuImage.ShowOnCategoryPage = VisibleCategoryInd.Checked;
               this.skuImage.SKUID = this.skuId;
               this.skuImage.ProductImageTypeID = 2; //default alternate Images;
               this.skuImage.DisplayOrder = int.Parse(DisplayOrder.Text.Trim());
               this.skuImage.ImageAltTag = txtImageAltTag.Text.Trim();
               this.skuImage.AlternateThumbnailImageFile = txtAlternateThumbnail.Text.Trim();

               UploadProductImage.AppendToFileName = "-" + this.ItemId.ToString();
               // Validate image
               if ((this.ProductImageId == 0) || (RadioProductNewImage.Checked == true))
               {
                   if (!UploadProductImage.IsFileNameValid())
                   {
                       return;
                   }
                   // Check for Product View Image
                   fileInfo = UploadProductImage.FileInformation;
                   if (fileInfo != null)
                   {
                       if (!string.IsNullOrEmpty(product.PortalID.ToString()))
                       {
                           this.skuImage.ImageFile = "Turnkey/" + product.PortalID + "/" + fileInfo.Name;
                       }
                       else if (!string.IsNullOrEmpty(product.AccountID.ToString()))
                       {
                           this.skuImage.ImageFile = "Mall/" + product.AccountID + "/" + fileInfo.Name;
                       }
                       else
                       {
                           this.skuImage.ImageFile = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + fileInfo.Name;
                       }
                   }
               }
               else
               {
                   this.skuImage.ImageFile = this.skuImage.ImageFile;
               }


               // Upload File if this is a new product or the New Image option was selected for an existing product
               if (RadioProductNewImage.Checked || this.ProductImageId == 0)
               {
                   if (fileInfo != null)
                   {
                       if (!string.IsNullOrEmpty(product.PortalID.ToString()))
                       {
                           UploadProductImage.SaveImage("fadmin", product.PortalID.ToString(), "0");
                       }
                       else if (!string.IsNullOrEmpty(product.AccountID.ToString()))
                       {
                           UploadProductImage.SaveImage("madmin", "0", product.AccountID.ToString());
                       }
                       else
                       {
                           UploadProductImage.SaveImage("sadmin", ZNodeConfigManager.SiteConfig.PortalID.ToString(), "0");
                       }
                       // Release all resources used
                       UploadProductImage.Dispose();
                   }
               }

               if (this.ProductImageId > 0 && !newProductImage)
               {
                   // Create fileInfo for Original product View Image
                   fileInfo = UploadProductImage.FileInformation;
                   if (fileInfo != null)
                   {
                       this.skuImage.AlternateThumbnailImageFile = fileInfo.Name;
                   }
               }

               bool check = false;

               string AlternateImage = string.Empty;

               if (this.ProductImageId > 0)
               {
                   // Update the Imageview
                   this.AssociateName = "Edit Alternate Images " + this.skuImage.ImageFile + " - " + _SKU.SKU;
                   check = zSKUImageSer.Update(this.skuImage);
                   ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, product.Name);
               }
               else
               {
                   this.AssociateName = "Added Alternate Images " + this.skuImage.ImageFile + " - " + _SKU.SKU;
                   check = zSKUImageSer.Insert(this.skuImage);
                   ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, product.Name);
               }

               if (check)
               {
                   Response.Redirect(this.skuEditLink + this.ItemId + "&skuid=" + this.skuId + "&typeid=" + this.productTypeID);
               }
               else
               {
                   // Display error message
                   lblError.Text = "An error occurred while updating. Please try again.";
               }
           }
           catch (Exception ex)
           {
               Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in Save Sku alternate image in!!" + this.Request.Url + _SKU.SKUID + ex.ToString());
           }
        }

        #endregion

        #endregion
    }
}