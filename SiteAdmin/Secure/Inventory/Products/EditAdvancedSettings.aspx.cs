using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Zeon.Libraries.Elmah;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Site Admin Admin_Secure_catalog_product_edit_advancedsettings class
    /// </summary>
    public partial class EditAdvancedSettings : System.Web.UI.Page
    {
        #region Protected Member Variables
        private string AssociateName = string.Empty;
        private int ItemId = 0;
        private string ManagePageLink = "~/SiteAdmin/Secure/Inventory/Products/View.aspx?mode=advanced&itemid=";
        private string CancelPageLink = "~/SiteAdmin/Secure/Inventory/Products/View.aspx?mode=advanced&itemid=";
        //Zeon Custom Code
        Dictionary<int, int> newArrival = new Dictionary<int, int>();
        TList<ProductCategory> productCategoryList = null;
        int CatalogId = 0;
        //Zeon Custom Code

        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (!Page.IsPostBack)
            {

                if (this.ItemId > 0)
                {
                    lblTitle.Text = "Edit Settings for ";

                    // Bind Details
                    this.Bind();
                }
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit Button Click Event - Fires when Submit button is triggered
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();
            Product product = new Product();
            UrlRedirectAdmin urlRedirectAdmin = new UrlRedirectAdmin();
            string mappedSEOUrl = string.Empty;

            // If edit mode then get all the values first
            if (this.ItemId > 0)
            {
                product = productAdmin.GetByProductId(this.ItemId);

                if (product.SEOURL != null)
                {
                    mappedSEOUrl = product.SEOURL;
                }
            }

            // Set properties Display Settings
            product.ActiveInd = CheckEnabled.Checked;
            product.HomepageSpecial = CheckHomeSpecial.Checked;
            product.InventoryDisplay = Convert.ToByte(false);
            product.CallForPricing = CheckCallPricing.Checked;
            product.NewProductInd = CheckNewItem.Checked;
            product.FeaturedInd = ChkFeaturedProduct.Checked;
            product.Franchisable = CheckFranchisable.Checked;


            // Set properties
            product.SEOTitle = Server.HtmlEncode(txtSEOTitle.Text.Trim());
            product.SEOKeywords = Server.HtmlEncode(txtSEOMetaKeywords.Text.Trim());
            product.SEODescription = Server.HtmlEncode(txtSEOMetaDescription.Text.Trim());
            product.SEOURL = null;

            if (txtSEOUrl.Text.Trim().Length > 0)
            {
                product.SEOURL = txtSEOUrl.Text.Trim().Replace(" ", "-");
            }

            if (string.Compare(product.SEOURL, mappedSEOUrl, true) != 0)
            {
                if (urlRedirectAdmin.SeoUrlExists(product.SEOURL, product.ProductID))
                {
                    lblError.Text = "The SEO Friendly URL you entered is already in use on another page. Please select another name for your URL";
                    return;
                }
            }


            if (!CheckFranchisable.Checked)
            {
                bool Check = productCategoryAdmin.RemoveProductCategory(product.ProductID);
                if (Check)
                {
                    this.AssociateName = "Deleted the Franchisable of " + product.Name;
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, product.Name);
                }
            }
            //Zeon Custom Code:Starts
            if (this.ItemId > 0)
            {
                SetItemNewCategoriesAssociation(CheckNewItem.Checked);
            }
            //Zeon Custom Code:Ends

            // Inventory Setting - Out of Stock Options
            if (InvSettingRadiobtnList.SelectedValue.Equals("1"))
            {
                // Only Sell if Inventory Available - Set values
                product.TrackInventoryInd = true;
                product.AllowBackOrder = false;
            }
            else if (InvSettingRadiobtnList.SelectedValue.Equals("2"))
            {
                // Allow Back Order - Set values
                product.TrackInventoryInd = true;
                product.AllowBackOrder = true;
            }
            else if (InvSettingRadiobtnList.SelectedValue.Equals("3"))
            {
                // Don't Track Inventory - Set property values
                product.TrackInventoryInd = false;
                product.AllowBackOrder = false;
            }

            // Inventory Setting - Stock Messages
            if (txtOutofStock.Text.Trim().Length == 0)
            {
                product.OutOfStockMsg = "Out of Stock";
            }
            else
            {
                product.OutOfStockMsg = Server.HtmlEncode(txtOutofStock.Text.Trim());
            }

            product.InStockMsg = Server.HtmlEncode(txtInStockMsg.Text.Trim());
            product.BackOrderMsg = Server.HtmlEncode(txtBackOrderMsg.Text.Trim());
            product.DropShipInd = chkDropShip.Checked;

            // Recurring Billing settings
            if (chkRecurringBillingInd.Checked)
            {
                product.RecurringBillingInitialAmount = Convert.ToDecimal(txtRecurringBillingInitialAmount.Text);
                product.RecurringBillingInd = chkRecurringBillingInd.Checked;
                product.RecurringBillingPeriod = ddlBillingPeriods.SelectedItem.Value;
                product.RecurringBillingFrequency = "1";
                product.RecurringBillingTotalCycles = 0;
            }
            else
            {
                product.RecurringBillingInd = false;
                product.RecurringBillingInstallmentInd = false;
                product.RecurringBillingInitialAmount = null;
            }

            bool status = false;

            try
            {
                if (this.ItemId > 0)
                {
                    // PRODUCT UPDATE
                    status = productAdmin.Update(product);
                }

                if (status)
                {
                    this.AssociateName = "Edit Advanced Settings - " + product.Name;
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, product.Name);

                    if (chkAddURLRedirect.Checked)
                    {
                        urlRedirectAdmin.AddUrlRedirectTable(SEOUrlType.Product, mappedSEOUrl, product.SEOURL, product.ProductID.ToString());
                    }

                    //Zeon Custom Code:Starts
                    SaveProductExtnDetails();
                    //Zeon Custom Code:Ends
                    urlRedirectAdmin.UpdateUrlRedirect(mappedSEOUrl);

                    product = productAdmin.GetByProductId(this.ItemId);

                    Response.Redirect(this.ManagePageLink + this.ItemId);
                }
                else
                {
                    lblError.Text = "Unable to update product advanced settings. Please try again.";
                }
            }
            catch (Exception)
            {
                lblError.Text = "Unable to update product advanced settings. Please try again.";
                return;
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.CancelPageLink + this.ItemId);
        }

        /// <summary>
        /// Recurring Billing Ind Checked Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ChkRecurringBillingInd_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRecurringBillingInd.Checked)
            {
                pnlRecurringBilling.Visible = true;
            }
            else
            {
                pnlRecurringBilling.Visible = false;
            }
        }

        /// <summary>
        /// Billing Period Selected Index Changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlBillingPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {
            //this.BindBillingFrequency();
        }
        #endregion

        #region Helper Methods
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind Method
        /// </summary>
        private void Bind()
        {
            // Create Instance for Product Admin and Product entity
            ZNode.Libraries.Admin.ProductAdmin ProdAdmin = new ProductAdmin();
            Product product = ProdAdmin.GetByProductId(this.ItemId);

            if (product != null)
            {
                // Display Settings                                             
                CheckEnabled.Checked = product.ActiveInd;
                CheckHomeSpecial.Checked = product.HomepageSpecial;
                CheckCallPricing.Checked = product.CallForPricing;
                ChkFeaturedProduct.Checked = product.FeaturedInd;

                if (product.NewProductInd.HasValue)
                {
                    CheckNewItem.Checked = product.NewProductInd.Value;
                }

                // Inventory Setting - Out of Stock Options
                if (product.AllowBackOrder.HasValue && product.TrackInventoryInd.HasValue)
                {
                    if (product.TrackInventoryInd.Value && product.AllowBackOrder.Value == false)
                    {
                        InvSettingRadiobtnList.Items[0].Selected = true;
                    }
                    else if (product.TrackInventoryInd.Value && product.AllowBackOrder.Value)
                    {
                        InvSettingRadiobtnList.Items[1].Selected = true;
                    }
                    else if (product.TrackInventoryInd.Value == false && product.AllowBackOrder.Value == false)
                    {
                        InvSettingRadiobtnList.Items[2].Selected = true;
                    }
                }

                // Inventory Setting - Stock Messages
                txtInStockMsg.Text = Server.HtmlDecode(product.InStockMsg);
                txtOutofStock.Text = Server.HtmlDecode(product.OutOfStockMsg);
                txtBackOrderMsg.Text = Server.HtmlDecode(product.BackOrderMsg);

                if (product.DropShipInd.HasValue)
                {
                    chkDropShip.Checked = product.DropShipInd.Value;
                }

                lblTitle.Text += "\"" + product.Name + "\"";

                CheckFranchisable.Checked = product.Franchisable;

                // Recurring Billing
                chkRecurringBillingInd.Checked = product.RecurringBillingInd;
                pnlRecurringBilling.Visible = product.RecurringBillingInd;
                if (product.RecurringBillingInitialAmount.HasValue)
                    txtRecurringBillingInitialAmount.Text = product.RecurringBillingInitialAmount.Value.ToString("N");
                ddlBillingPeriods.SelectedValue = product.RecurringBillingPeriod;

                // SEO
                txtSEOTitle.Text = Server.HtmlDecode(product.SEOTitle);
                txtSEOMetaKeywords.Text = Server.HtmlDecode(product.SEOKeywords);
                txtSEOMetaDescription.Text = Server.HtmlDecode(product.SEODescription);
                txtSEOUrl.Text = product.SEOURL;

                if (ZNodeConfigManager.EnvironmentConfig.LicenseType == ZNodeLicenseType.Server)
                {
                    pnlFranchise.Visible = false;
                }
                else
                {
                    pnlFranchise.Visible = true;
                }

                SetProductExtensionData(product.ProductID);
            }
        }
        #endregion

        #region Zeon Custom Code

        #region New Arrival Implementation

        private void SetItemNewCategoriesAssociation(bool isNewItemChecked)
        {
            try
            { //set new arrival categories
                GetNewArrivalCategoryIDs();
                productCategoryList = GetItemAllCategory();
                if (isNewItemChecked)
                {
                    //Associate New Categories
                    if (productCategoryList == null || productCategoryList.Count <= 0)
                    {
                        AssociateAllNewCategoryToItem();
                    }
                    else
                    {
                        GetCatalogID();
                        bool NewArrivalCatExist = GetCurrentNewArrivalCategory();
                        //insert new arrival categories as per catalog
                        if (!NewArrivalCatExist)
                        {
                            AssociateNewArrivalCategoryToItem();
                        }
                    }
                }
                else
                {
                    //Remove Association of  New Categories if already associated
                    if (productCategoryList != null && productCategoryList.Count > 0)
                    {
                        GetCatalogID();
                        bool NewArrivalCatExist = GetCurrentNewArrivalCategory();
                        if (NewArrivalCatExist)
                        {
                            RemoveItemNewArrivalAssociation();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ElmahErrorManager.Log(ex.StackTrace);
            }
        }

        /// <summary>
        /// check if new categories are already associated to this product.
        /// </summary>
        /// <returns></returns>
        private TList<ProductCategory> GetItemAllCategory()
        {
            ProductCategoryService productCategorySer = new ProductCategoryService();
            TList<ProductCategory> productCategoryList = productCategorySer.GetByProductID(this.ItemId);
            return productCategoryList;
        }

        /// <summary>
        /// get category Id of New Products 
        /// </summary>
        private void GetNewArrivalCategoryIDs()
        {
            string filterString = GetFilterString();
            TList<Category> lstCategory = new TList<Category>();
            if (!string.IsNullOrEmpty(filterString))
            {
                CategoryService catService = new CategoryService();
                CategoryQuery catQuery = new CategoryQuery();
                catQuery.AppendInQuery(CategoryColumn.Name, filterString.ToString());
                lstCategory = catService.Find(catQuery.GetParameters());
            }
            if (lstCategory != null && lstCategory.Count > 0)
            {
                foreach (Category cat in lstCategory)
                {
                    int catalogID = GetCategoryCatalogID(cat.CategoryID);
                    if (catalogID > 0)
                    {
                        newArrival.Add(cat.CategoryID, catalogID);
                    }
                }
            }

        }

        /// <summary>
        /// Build Query string for finding all new categories
        /// </summary>
        /// <returns>string</returns>
        private string GetFilterString()
        {
            StringBuilder matchstring = new StringBuilder();
            if (ConfigurationManager.AppSettings["NewArrivalCategories"] != null && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["NewArrivalCategories"].ToString()))
            {
                string[] newCategories = ConfigurationManager.AppSettings["NewArrivalCategories"].ToString().Split(',');
                foreach (string cat in newCategories)
                {
                    matchstring = matchstring.Append("'" + cat.ToString().Trim() + "'" + ","); ;
                }
                return matchstring.ToString().TrimEnd(',');
            }
            return matchstring.ToString();
        }

        /// <summary>
        /// get Catalog ID by category Id
        /// </summary>
        /// <param name="categoryID">int</param>
        /// <returns>int</returns>
        private int GetCategoryCatalogID(int categoryID)
        {
            int catalogID = 0;
            CategoryNodeService catNodeSer = new CategoryNodeService();
            TList<CategoryNode> listCat = catNodeSer.GetByCategoryID(categoryID);
            if (listCat != null && listCat.Count > 0)
            {
                int.TryParse(listCat[0].CatalogID.ToString(), out catalogID);
            }
            return catalogID;
        }

        /// <summary>
        /// associate All new category to Item
        /// </summary>
        private void AssociateAllNewCategoryToItem()
        {
            ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();
            if (newArrival != null)
            {
                foreach (KeyValuePair<int, int> pair in newArrival)
                {
                    productCategoryAdmin.Insert(this.ItemId, pair.Key);
                }
            }
        }

        /// <summary>
        /// associate All new category to Item
        /// </summary>
        private void AssociateNewArrivalCategoryToItem()
        {
            ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();
            if (newArrival != null)
            {
                foreach (KeyValuePair<int, int> pair in newArrival)
                {
                    if (CatalogId > 0 && pair.Value == CatalogId)
                    {
                        productCategoryAdmin.Insert(this.ItemId, pair.Key);
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// get Catalog ID of current prooduct
        /// </summary>
        private void GetCatalogID()
        {
            string newArrivalId = string.Empty;
            if (newArrival != null && newArrival.Count > 0)
            {
                foreach (KeyValuePair<int, int> pair in newArrival)
                {
                    newArrivalId = newArrivalId + pair.Value + ",";
                }
                newArrivalId = newArrivalId.TrimEnd(',');

                foreach (ProductCategory productCat in productCategoryList)
                {
                    if (newArrivalId.Length>0)
                    {
                        if (newArrivalId.IndexOf(',') > 0)
                        {
                            int cata2 = newArrivalId.Split(',').Length > 1 ? int.Parse(newArrivalId.Split(',')[1]) : 0;

                            if (productCat.CategoryID != int.Parse(newArrivalId.Split(',')[0]) && (cata2 > 0) && (productCat.CategoryID != cata2))
                            {
                                this.CatalogId = GetCategoryCatalogID(productCat.CategoryID);
                                break;
                            }
                        }
                        else
                        {
                            if (productCat.CategoryID != int.Parse(newArrivalId.Split(',')[0]))
                            {
                                this.CatalogId = GetCategoryCatalogID(productCat.CategoryID);
                                break;
                            }
                        }
                    }
                    

                    if (this.CatalogId > 0) { break; }
                }
            }
        }

        /// <summary>
        /// Get current all new arrival categories assciated.
        /// </summary>
        /// <returns>string</returns>
        private bool GetCurrentNewArrivalCategory()
        {
            bool NewArrivalCatExist = false;
            foreach (ProductCategory productCat in productCategoryList)
            {
                foreach (KeyValuePair<int, int> pair in newArrival)
                {
                    if (CatalogId > 0)
                    {
                        if (pair.Value != CatalogId && pair.Key == productCat.CategoryID)
                        {
                            RemoveItemCategory(productCat.ProductCategoryID);
                        }
                        if (pair.Value == CatalogId && pair.Key == productCat.CategoryID)
                        {
                            NewArrivalCatExist = true;
                            break;
                        }
                    }
                    else
                    {
                        if (pair.Key == productCat.CategoryID)
                        {
                            NewArrivalCatExist = true;
                            break;
                        }
                    }
                }
            }
            return NewArrivalCatExist;
        }

        /// <summary>
        /// remove category from Item
        /// </summary>
        /// <param name="categoryId">int</param>
        private void RemoveItemCategory(int prodCategoryId)
        {
            ProductCategoryService prodCatSer = new ProductCategoryService();
            prodCatSer.Delete(prodCategoryId);
        }

        /// <summary>
        /// remove association with New Arrival Category
        /// </summary>
        private void RemoveItemNewArrivalAssociation()
        {
            foreach (ProductCategory productCat in productCategoryList)
            {
                foreach (KeyValuePair<int, int> pair in newArrival)
                {
                    if (pair.Key == productCat.CategoryID)
                    {
                        RemoveItemCategory(productCat.ProductCategoryID);
                    }

                }
            }
        }
        #endregion

        #region ProductExtn Implementation

        /// <summary>
        /// set Product Extn Data
        /// </summary>
        /// <param name="productID">int</param>
        private void SetProductExtensionData(int productID)
        {
            ProductExtnService productExtnSer = new ProductExtnService();
            TList<ProductExtn> productExtnList = productExtnSer.GetByProductID(productID);
            if (productExtnList != null && productExtnList.Count > 0)
            {
                chkPreOrder.Checked = productExtnList[0].PreOrderFlag;
                chkMapPrice.Checked = productExtnList[0].IsMapPrice;
                chkFollInd.Checked = productExtnList[0].IsIndexFollow;
            }
        }

        /// <summary>
        /// save ProductExtn Data
        /// </summary>
        private void SaveProductExtnDetails()
        {
            if (this.ItemId > 0)
            {
                ProductExtnService productExtnSer = new ProductExtnService();
                TList<ProductExtn> productExtnList = productExtnSer.GetByProductID(this.ItemId);
                ProductExtn productExtn = new ProductExtn();
                if (productExtnList != null && productExtnList.Count > 0)
                {
                    //Update Data
                    productExtn = productExtnList[0];
                    productExtn.PreOrderFlag = chkPreOrder.Checked;
                    productExtn.IsMapPrice = chkMapPrice.Checked;
                    productExtn.IsIndexFollow = chkFollInd.Checked;
                    productExtnSer.Update(productExtn);
                }
                else
                {
                    //Save Data
                    productExtn.PreOrderFlag = chkPreOrder.Checked;
                    productExtn.IsMapPrice = chkMapPrice.Checked;
                    productExtn.IsIndexFollow = chkFollInd.Checked;
                    productExtn.ProductID = this.ItemId;
                    productExtnSer.Save(productExtn);
                }
            }
        }
        #endregion
        #endregion
    }
}