using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;

namespace SiteAdmin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Site Admin Admin_Secure_products_list class
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Protected Variables
        private string AddLink = "Add.aspx";
        private string PreviewLink = string.Empty;
        private string DeleteLink = "Delete.aspx?itemid=";
        private string DetailsLink = "~/SiteAdmin/Secure/Inventory/Products/View.aspx?itemid=";
        private string portalIds = string.Empty;
        #endregion

        #region public properties
        /// <summary>
        /// Gets or sets the Account Object from/to Cache
        /// </summary>
        public Account UserAccount
        {
            get
            {
                if (ViewState["AccountObject"] != null)
                {
                    // The account info with 'AccountObject' is retrieved from the cache using Get Method and
                    // It is converted to a Entity Account object
                    return (Account)ViewState["AccountObject"];
                }

                return new Account();
            }

            set
            {
                // Customer account object is placed in the session and assigned a key, AccountObject.            
                ViewState["AccountObject"] = value;
            }
        }
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            this.PreviewLink = "http://" + UserStoreAccess.DomainName + "/product.aspx";
            this.portalIds = UserStoreAccess.GetAvailablePortals;

            if (!Page.IsPostBack)
            {
                this.BindDropDownData();
                this.BindSearchProduct();
            }

            txtCategory.ContextKey = ddlCatalog.SelectedValue;
            txtCategory.PopulateDropDown();
        }
        #endregion

        #region Grid Events

        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindSearchProduct();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "Manage")
                {
                    Response.Redirect(this.DetailsLink + e.CommandArgument);
                }
                else if (e.CommandName == "Copy")
                {
                    int copyProductId = 0;
                    ProductAdmin admin = new ProductAdmin();
                    copyProductId = admin.CopyProduct(Convert.ToInt32(e.CommandArgument));
                    Response.Redirect(this.DetailsLink + copyProductId.ToString());
                }
                else if (e.CommandName == "Delete")
                {
                    Response.Redirect(this.DeleteLink + e.CommandArgument);
                }
            }
        }

        #endregion

        #region General Events
        /// <summary>
        /// AddNew Product button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddProduct_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddLink);
        }

        /// <summary>
        /// Search button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchProduct();
        }

        /// <summary>
        /// Clear button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            txtproductname.Text = string.Empty;
            txtproductnumber.Text = string.Empty;
            txtsku.Text = string.Empty;
            txtCategory.Text = string.Empty;
            txtProductType.Text = string.Empty;
            txtProductType.Value = string.Empty;
            txtCategory.Value = "0";
            txtManufacturer.Text = string.Empty;
            txtManufacturer.Value = "0";
            this.BindDropDownData();
            this.BindSearchProduct();
        }

        #endregion

        #region Helper Functions

        /// <summary>
        /// Format the Price of a product and return in string format
        /// </summary>
        /// <param name="fieldvalue">The field value</param>
        /// <returns>Returns the formatted price</returns>
        protected string DisplayPrice(object fieldvalue)
        {
            if (fieldvalue == DBNull.Value)
            {
                return "$0.00";
            }
            else
            {
                if (Convert.ToInt32(fieldvalue) == 0)
                {
                    return "$0.00";
                }
                else
                {
                    return String.Format("${0:#.00}", fieldvalue);
                }
            }
        }

        /// <summary>
        /// Get ImagePath
        /// </summary>
        /// <param name="Imagefile">The Image File</param>
        /// <returns>Returns the Image Path</returns>
        protected string GetImagePath(string Imagefile)
        {
            ZNodeImage znodeImage = new ZNodeImage();
            return znodeImage.GetImageHttpPathThumbnail(Imagefile);
        }

        #endregion

        #region FormatPrice Helper method
        /// <summary>
        /// Returns formatted price to the grid
        /// </summary>
        /// <param name="productPrice">Product Price</param>
        /// <returns>Returns the formatted price</returns>
        protected string FormatPrice(object productPrice)
        {
            if (productPrice != null)
            {
                if (productPrice.ToString().Length > 0)
                {
                    return decimal.Parse(productPrice.ToString()).ToString("c");
                }
            }

            return string.Empty;
        }
        #endregion

        #region Bind Grid
        /// <summary>
        /// Search a Product by name,productnumber,sku,manufacturer,category,producttype
        /// </summary>
        private void BindSearchProduct()
        {
            try
            {
                ProductAdmin prodadmin = new ProductAdmin();
                DataSet ds = prodadmin.SearchProduct(Server.HtmlEncode(txtproductname.Text.Trim()), txtproductnumber.Text.Trim(), txtsku.Text.Trim(), txtManufacturer.Value, txtProductType.Value, txtCategory.Value, ddlCatalog.SelectedValue);
                DataView dv = new DataView(ds.Tables[0]);
                dv.Sort = "DisplayOrder";
                uxGrid.DataSource = dv;
                uxGrid.DataBind();
            }
            catch (System.Data.SqlClient.SqlException sex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(sex.ToString());
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
            }
        }

        /// <summary>
        /// Bind data to the drop down list
        /// </summary>
        private void BindDropDownData()
        {
            CatalogAdmin catalogAdmin = new CatalogAdmin();

            Account _account = this.UserAccount;
            MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            string roleList = string.Empty;

            // Get roles for this User account
            string[] roles = Roles.GetRolesForUser(user.UserName);

            foreach (string Role in roles)
            {
                roleList += Role + "<br>";
            }

            string rolename = roleList;

            // Hide the Delete button if a NonAdmin user has entered this page
            if (!Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "ADMIN"))
            {
                if (Roles.IsUserInRole(user.UserName, "CATALOG EDITOR"))
                {
                    string portalIDs = string.Empty;
                    portalIDs = UserStoreAccess.GetAvailablePortals;
                    string catalogIDs = string.Empty;
                    catalogIDs = catalogAdmin.GetCatalogIDsByPortalIDs(portalIDs);

                    TList<Catalog> catalogList = catalogAdmin.GetAllCatalogs();
                    DataSet ds = catalogList.ToDataSet(true);

                    DataView dv = new DataView(ds.Tables[0]);
                    if (catalogIDs != "0")
                    {
                        dv.RowFilter = "CatalogId in (" + catalogIDs + ")";
                    }

                    ddlCatalog.DataSource = dv;
                    ddlCatalog.DataTextField = "Name";
                    ddlCatalog.DataValueField = "CatalogID";
                    ddlCatalog.DataBind();

                    foreach (ListItem listCatalog in ddlCatalog.Items)
                    {
                        listCatalog.Text = Server.HtmlDecode(listCatalog.Text);
                    }
                }
            }
            else
            {
                TList<Catalog> catalogList = catalogAdmin.GetAllCatalogs();
                DataSet ds = catalogList.ToDataSet(false);
                ddlCatalog.DataSource = UserStoreAccess.CheckStoreAccess(ds.Tables[0], true);
                ddlCatalog.DataTextField = "Name";
                ddlCatalog.DataValueField = "CatalogID";
                ddlCatalog.DataBind();

                ListItem li = new ListItem("ALL", "0");
                ddlCatalog.Items.Insert(0, li);

                foreach (ListItem listCatalog in ddlCatalog.Items)
                {
                    listCatalog.Text = Server.HtmlDecode(listCatalog.Text);
                }
            }
        }

        #endregion
    }
}