<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Inventory.Products.AddNext" Title="Manage Products - Add Next" CodeBehind="AddNext.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <h1>Select Next Step</h1>
    <p>Your product was successfully added. Select your next step below:</p>
    <br />
    <br />
    <asp:Button CssClass="Button" ID="btnAddProduct" CausesValidation="False" Text="Add another Product" runat="server" OnClick="BtnAddProduct_Click1"></asp:Button>
    <br />
    <br />
    <asp:Button CssClass="Button" ID="btnProductList" CausesValidation="False" Text="Back to Product List" runat="server" OnClick="BtnProductList_Click"></asp:Button>
    <br />
    <br />
</asp:Content>

