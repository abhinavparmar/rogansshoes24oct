﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace SiteAdmin.Secure.Inventory.Products {
    
    
    public partial class EditAdditionalInfo {
        
        /// <summary>
        /// lblTitle control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblTitle;
        
        /// <summary>
        /// DemoMode1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::SiteAdmin.Controls.Default.DemoMode DemoMode1;
        
        /// <summary>
        /// btnSubmitTop control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton btnSubmitTop;
        
        /// <summary>
        /// btnCancelTop control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton btnCancelTop;
        
        /// <summary>
        /// lblError control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblError;
        
        /// <summary>
        /// ctrlHtmlPrdFeatures control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::SiteAdmin.Controls.Default.HtmlTextBox ctrlHtmlPrdFeatures;
        
        /// <summary>
        /// ctrlHtmlPrdSpec control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::SiteAdmin.Controls.Default.HtmlTextBox ctrlHtmlPrdSpec;
        
        /// <summary>
        /// CtrlHtmlProdInfo control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::SiteAdmin.Controls.Default.HtmlTextBox CtrlHtmlProdInfo;
        
        /// <summary>
        /// btnSubmitBottom control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton btnSubmitBottom;
        
        /// <summary>
        /// btnCancelBottom control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton btnCancelBottom;
    }
}
