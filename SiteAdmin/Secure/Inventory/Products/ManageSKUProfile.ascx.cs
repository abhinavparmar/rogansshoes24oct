﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.Framework.Business;
using System.Data;

namespace SiteAdmin.Secure.Inventory.Products
{
    public partial class ManageSKUProfile : System.Web.UI.UserControl
    {
        #region Properties

        private int SkuProfileId
        {
            get
            {
                if (ViewState["SkuProfileId"] != null)
                {
                    return Convert.ToInt32(ViewState["SkuProfileId"]);
                }

                return 0;
            }

            set
            {
                ViewState["SkuProfileId"] = value;
            }
        }

        protected int SkuID
        {
            get
            {
                int skuId = 0;
                if (Request.QueryString["skuid"] != null)
                {
                    int.TryParse(Request.QueryString["skuid"], out skuId);
                }
                return skuId;
            }
        }

        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            this.pnlSkuProfile.Visible = SkuID > 0;

            if (!this.IsPostBack && SkuID > 0)
            {
                BindGridData();
            }
        }

        #endregion

        #region Control Events

        protected void Update_Click(object sender, EventArgs e)
        {

            SKUProfile skuProfile = new SKUProfile();
            if (this.SkuProfileId > 0)
            {
                skuProfile = DataRepository.SKUProfileProvider.GetBySKUProfileID(this.SkuProfileId);
            }

            skuProfile.SKUID = this.SkuID;
            skuProfile.ProfileID = Convert.ToInt32(this.ddlProfile.SelectedValue);
            skuProfile.ProfileLimit = Convert.ToInt32(this.txtQuantityList.Text);
            skuProfile.ExpirationDate = Convert.ToDateTime(this.txtExpirationDate.Text);
            DataRepository.SKUProfileProvider.Save(skuProfile);

            // rebind the grid.
            uxGrid.EditIndex = -1;
            BindGridData();
            this.pnlEditSkuProfile.Visible = false;
            ((SiteAdmin.Secure.Inventory.Products.AddSku)this.Page).ShowSubmitButton = true;
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            uxGrid.EditIndex = -1;
            BindGridData();
            this.pnlEditSkuProfile.Visible = false;
            ((SiteAdmin.Secure.Inventory.Products.AddSku)this.Page).ShowSubmitButton = true;
            //pnlSKUButtons.Visible = false;
        }

        protected void Add_Click(object sender, EventArgs e)
        {
            lblError.Text = string.Empty;
            this.SkuProfileId = 0;
            this.pnlEditSkuProfile.Visible = true;
            if (BindProfile())
            {
                if (this.ddlProfile.Items.Count > 0)
                {
                    this.ddlProfile.SelectedIndex = 0;
                }

                this.BindEditData();
            }
        }

        protected void uxGrid_EditCommand(object sender, GridViewEditEventArgs e)
        {


            lblError.Text = string.Empty;
            this.SkuProfileId = Convert.ToInt32(uxGrid.DataKeys[e.NewEditIndex].Value);
            this.pnlEditSkuProfile.Visible = true;

            BindProfile();
            BindEditData();

        }


        protected void uxGrid_DeleteCommand(object sender, GridViewDeleteEventArgs e)
        {
            this.SkuProfileId = Convert.ToInt32(uxGrid.DataKeys[e.RowIndex].Value);
            DataRepository.SKUProfileProvider.Delete(SkuProfileId);
            BindGridData();
        }

        #endregion

        #region Helper Methods

        private void BindGridData()
        {
            SKUAdmin skuAdmin = new SKUAdmin();
            DataSet skuProfiles = skuAdmin.GetSkuProfileBySkuID(SkuID);
            this.uxGrid.DataSource = skuProfiles;
            this.uxGrid.DataBind();

            uxGrid.EditIndex = -1;
        }

        private void BindEditData()
        {
            if (this.SkuProfileId > 0)
            {
                lblTitle.Text = "Edit Details";
                SKUProfile skuProfile = DataRepository.SKUProfileProvider.GetBySKUProfileID(SkuProfileId);
                this.ddlProfile.SelectedValue = skuProfile.ProfileID.ToString();

                this.txtQuantityList.Text = skuProfile.ProfileLimit.ToString();
                this.txtExpirationDate.Text = skuProfile.ExpirationDate.ToShortDateString();

            }

            else
            {
                lblTitle.Text = "Add Details";
                this.txtQuantityList.Text = "0";
                this.txtExpirationDate.Text = DateTime.Now.ToShortDateString();
            }

            ((SiteAdmin.Secure.Inventory.Products.AddSku)this.Page).ShowSubmitButton = false;
        }

        private bool BindProfile()
        {
            // Get profiles
            StoreSettingsAdmin settingsAdmin = new StoreSettingsAdmin();
            TList<Profile> profiles = UserStoreAccess.CheckProfileAccess(settingsAdmin.GetProfiles());
            TList<SKUProfile> skuProfile = DataRepository.SKUProfileProvider.GetBySKUID(SkuID);
            skuProfile.ApplyFilter(delegate(SKUProfile sp) { return sp.SKUProfileID != SkuProfileId; });

            profiles.ApplyFilter(delegate(Profile p)
            {
                return skuProfile.FirstOrDefault(delegate(SKUProfile sp)
                {
                    return sp.ProfileID == p.ProfileID;
                }) == null;
            });
            this.ddlProfile.DataSource = profiles;
            this.ddlProfile.DataTextField = "Name";
            this.ddlProfile.DataValueField = "ProfileID";
            this.ddlProfile.DataBind();

            if (ddlProfile.Items.Count == 0)
            {
                lblError.Text = "You have already associated all profiles to this SKU.";
                pnlEditSkuProfile.Visible = false;
                return false;
            }

            return true;
        }

        #endregion


    }
}