<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Inventory.Products.Default" Title="Manage Products - List" ValidateRequest="false" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="CategoryAutoComplete" Src="~/SiteAdmin/Controls/Default/CategoryAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ManufacturerAutoComplete" Src="~/SiteAdmin/Controls/Default/ManufacturerAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ProductTypeAutoComplete" Src="~/SiteAdmin/Controls/Default/ProductTypeAutoComplete.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div>
        <div>
            <div class="LeftFloat" style="width: 50%">
                <h1>Products
                </h1>
            </div>
            <div class="ButtonStyle">
                <zn:LinkButton ID="btnAddProduct" runat="server" CausesValidation="False"
                    ButtonType="Button" OnClick="BtnAddProduct_Click" Text="Add New Product"
                    ButtonPriority="Primary" />
            </div>
        </div>
        <div class="ClearBoth" align="left">
            <p>
                Manage products and inventory in your store.
            </p>
            <br />
        </div>
        <h4 class="SubTitle">Search Products</h4>
        <asp:Panel ID="SearchPanel" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">Product Name</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtproductname" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">Product Number</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtproductnumber" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">SKU</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtsku" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">Catalog</span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlCatalog" runat="server" AutoPostBack="true"></asp:DropDownList></span>
                    </div>
                </div>
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">Brand</span><br />
                        <span class="ValueStyle">
                            <ZNode:ManufacturerAutoComplete runat="server" ID="txtManufacturer" />
                        </span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">Product Type</span><br />
                        <span class="ValueStyle">
                            <ZNode:ProductTypeAutoComplete ID="txtProductType" runat="server" />
                        </span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">Product Category</span><br />
                        <span class="ValueStyle">
                            <ZNode:CategoryAutoComplete ID="txtCategory" runat="server" />
                        </span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <br />
                </div>
                <div>
                    <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_clear_highlight.gif';"
                        onmouseout="this.src='../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                        runat="server" AlternateText="Clear" OnClick="BtnClear_Click" />
                    <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../Themes/Images/buttons/button_search_highlight.gif';"
                        onmouseout="this.src='../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                        runat="server" AlternateText="Search" OnClick="BtnSearch_Click" />
                </div>
            </div>
        </asp:Panel>
        <br />
        <h4 class="GridTitle">Product List</h4>
        <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" CaptionAlign="Left" AutoGenerateColumns="False"
            GridLines="None" AllowPaging="True" PageSize="10" OnPageIndexChanging="UxGrid_PageIndexChanging"
            OnRowCommand="UxGrid_RowCommand" EmptyDataText="No Products exist in the database."
            Width="100%" CellPadding="4">
            <Columns>
                <asp:BoundField DataField="productid" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText="Image" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <a href='<%# "view.aspx?itemid=" + DataBinder.Eval(Container.DataItem,"productid")%>'
                            id="LinkView">
                            <img alt=" " src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>'
                                runat="server" style="border: none" />
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:HyperLinkField DataNavigateUrlFields="productid" DataNavigateUrlFormatString="view.aspx?itemid={0}"
                    DataTextField="name" HeaderText="Name" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText="Retail Price" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# FormatPrice(DataBinder.Eval(Container.DataItem,"RetailPrice")) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Sale Price" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# FormatPrice(DataBinder.Eval(Container.DataItem,"SalePrice")) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Wholesale Price" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# FormatPrice(DataBinder.Eval(Container.DataItem,"WholesalePrice")) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="In Stock" DataField="Quantityonhand" HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField HeaderText="Display Order" DataField="displayorder" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText="Is Active" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img alt="" id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:LinkButton ID="Button1" CommandName="Manage" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"productid")%>'
                            Text="Manage &raquo" runat="server" CssClass="actionlink" />
                        <asp:LinkButton ID="LinkButton1" CommandName="Copy" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"productid")%>'
                            Text="Copy &raquo" runat="server" CssClass="actionlink" />
                        &nbsp;<asp:LinkButton ID="Button5" CommandName="Delete" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"productid")%>'
                            Text="Delete &raquo" runat="server" CssClass="actionlink" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
    </div>
</asp:Content>
