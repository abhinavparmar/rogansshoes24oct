﻿<%@ Control Language="C#" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Inventory.Products.ProductTags" CodeBehind="ProductTags.ascx.cs" %>
<%@ Register TagPrefix="znode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Panel ID="pnlCategories" runat="server">
</asp:Panel>
<!-- Product List -->
<asp:UpdatePanel runat="server" ID="upProductTags">
    <ContentTemplate>
        <asp:Panel ID="pnlTagsList" runat="server">
            <h4 class="GridTitle">Associated Facets</h4>
            <div>
                <div class="TabDescription" style="width: 80%;">
                        <p>
                        Facets enable customers to filter product search results based on various criteria such as "Price Range", etc.
                    </p>
                    <asp:Label runat="server" ID="lblEmptyGrid">No facets associated with this <%=ItemType.Replace("Id","") %></asp:Label>
                </div>
              <div class="ButtonStyle" style="clear: right; float: right; padding-top: 10px; padding-right: 10px;"> 
                    <zn:LinkButton ID="LinkButton1" runat="server"
                        ButtonType="Button" OnClick="Add_Click" CausesValidation="False" Text="Associate Facet"
                        ButtonPriority="Primary" />
                </div>
            </div>

            <znode:Spacer ID="Spacer3" runat="server" SpacerHeight="10" />
            <div style="clear: both;">
                <asp:GridView ID="uxGrid" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                    CaptionAlign="Left" CellPadding="4" CssClass="Grid" GridLines="None" Width="100%"
                    OnRowCommand="UxGrid_RowCommand" OnDataBound="UxGrid_DataBound" OnRowDataBound="UxGrid_RowDataBound"
                    OnRowDeleting="UxGrid_RowDeleting">
                    <Columns>
                        <asp:BoundField DataField="TagGroupLabel" HeaderText="Facet Group Name" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText="Facet Names" ItemStyle-Width="60%">
                            <ItemTemplate>
                                <asp:DataList runat="server" ID="dlTagNames" DataSource='<%# GetTags(Eval("TagGroupId")) %>'
                                    RepeatDirection="Horizontal" RepeatLayout="Flow">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblTagNames" Text='<%# Eval("TagName") %>'></asp:Label>,
                                    </ItemTemplate>
                                </asp:DataList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="10%">
                            <ItemTemplate>
                                <asp:LinkButton ID="EditTag" CommandName="TagEdit" CommandArgument='<%# Eval("TagGroupID") %>'
                                    runat="server" CssClass="actionlink" Text="Edit &raquo" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="10%">
                            <ItemTemplate>
                                <asp:LinkButton ID="DeleteTag" CommandName="TagDelete" CommandArgument='<%# Eval("TagGroupID") %>'
                                    runat="server" CssClass="actionlink" Text="Delete &raquo" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="RowStyle" />
                    <EditRowStyle CssClass="EditRowStyle" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    <EmptyDataTemplate>
                        No facets associated with this
                    <%=ItemType.Replace("Id","") %>
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </asp:Panel>


        <div>
        </div>

        <asp:Panel runat="server" ID="pnlEditTags" Visible="false" CssClass="FormView">
            <h4 class="SubTitle">
                <asp:Label runat="server" ID="lblAddEdit"></asp:Label>Select Facets to Associate</h4>

            <div>
                <asp:Label ID="lblError" runat="server" Text="Label" Visible="false" CssClass="Error"
                    ForeColor="red"></asp:Label>
            </div>
            <znode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></znode:Spacer>
            <div class="FieldStyle">
                Facet Group<br />
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlTagGroups" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlTagGroups_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="FieldStyle">
                Facets<br />
            </div>
            <div class="ValueStyle">
                <asp:CheckBoxList ID="chklstTags" RepeatColumns="5" RepeatLayout="Table" RepeatDirection="Horizontal"
                    runat="server">
                </asp:CheckBoxList>
                <asp:Label runat="server" ID="lblNoTags" Visible="false">No facets found</asp:Label>
            </div>

            <znode:Spacer ID="Spacer1" runat="server" SpacerHeight="10" />
            <div class="ClearBoth">
                <br />
            </div>
            <div>
                <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" OnClick="Update_Click" CausesValidation="true" />
                <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="Cancel_Click" />
            </div>
            <znode:Spacer ID="Spacer4" runat="server" SpacerHeight="10" />
        </asp:Panel>

    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdateProgress ID="uxTagUpdateProgress" runat="server" AssociatedUpdatePanelID="upProductTags" DisplayAfter="10">
    <ProgressTemplate>
        <div id="ajaxProgressBg"></div>
        <div id="ajaxProgress"></div>
    </ProgressTemplate>
</asp:UpdateProgress>
