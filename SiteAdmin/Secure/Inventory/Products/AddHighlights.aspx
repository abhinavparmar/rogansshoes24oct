<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True"
    ValidateRequest="false" Inherits="SiteAdmin.Secure.Inventory.Products.AddHighlights"
    Title="Manage Products - Add Highlights" CodeBehind="AddHighlights.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode"
    TagPrefix="ZNode" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <h1>
            <asp:Label ID="lblTitle" runat="server" Text="Associate Highlights to"></asp:Label></h1>
        <div style="text-align: right; display: none;" class="ImageButtons">
            <asp:Button ID="btntopAddSelectedAddons" runat="server" Text="Add Selected Items"
                AlternateText="Submit" OnClick="BtnAddSelectedAddons_Click" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
        <div>
            <asp:Label CssClass="Error" ID="lblErrorMessage" runat="server" EnableViewState="false"
                Visible="false"></asp:Label>
        </div>
        <h4 class="SubTitle">Highlight Search</h4>
        <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">Name</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtName" runat="server"></asp:TextBox></span><div>
                                <asp:RegularExpressionValidator ID="regName" runat="server" ControlToValidate="txtName"
                                    ErrorMessage="Enter valid Name" ValidationGroup="grpSearch" Display="Dynamic"
                                    ValidationExpression="[0-9A-Za-z\s\',.:&%#$@_-]+" CssClass="Error"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">Type</span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlHighlightType" runat="server">
                            </asp:DropDownList>
                        </span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle" runat="server" id="lblLocale">Locale</span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlLocales" runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Value="">All</asp:ListItem>
                            </asp:DropDownList>
                        </span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <asp:ImageButton ID="btnClearSearch" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_clear_highlight.gif';"
                        onmouseout="this.src='../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                        runat="server" AlternateText="Clear" OnClick="BtnClearSearch_Click" />
                    <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../Themes/Images/buttons/button_search_highlight.gif';"
                        onmouseout="this.src='../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                        runat="server" AlternateText="Search" OnClick="BtnSearch_Click" />
                </div>
            </div>
        </asp:Panel>
        <br />
        <h4 class="GridTitle">Product Highlight List</h4>
        <div>
            <uc1:Spacer ID="Spacer132" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <!-- Update Panel for grid paging that are used to avoid the postbacks -->
        <asp:UpdatePanel ID="updPnlHightlight" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" EmptyDataText="No product Highlights exist in the database."
                    PagerSettings-Visible="true" AllowSorting="True" PageSize="15" EnableSortingAndPagingCallbacks="False"
                    Width="100%" CaptionAlign="Left" GridLines="None" CellPadding="4" AutoGenerateColumns="False"
                    AllowPaging="True" OnPageIndexChanging="UxGrid_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField HeaderText="Select" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkProductHighlight" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="HighlightId" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText="Name" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText="Type" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# Eval("HighlightTypeIDSource.Name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--
                        <asp:BoundField HeaderText="Type" DataField="HighlightName" HeaderStyle-HorizontalAlign="Left" />--%>
                        <asp:BoundField HeaderText="Display Order" DataField="DisplayOrder" HeaderStyle-HorizontalAlign="Left" />
                    </Columns>
                    <FooterStyle CssClass="FooterStyle" />
                    <RowStyle CssClass="RowStyle" />
                    <PagerStyle CssClass="PagerStyle" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div>
            <uc1:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="ImageButtons">
            <asp:Button ID="btnAddSelectedAddons" runat="server" CssClass="Size210" Text="Associate Selected Highlights" AlternateText="Submit"
                OnClick="BtnAddSelectedAddons_Click" />
            <asp:ImageButton ID="btnBottomCancel" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>
