using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Site Admin Admin_Secure_catalog_product_add_Highlights class
    /// </summary>
    public partial class AddHighlights : System.Web.UI.Page
    {
        #region Private Variables
        private string ViewPageLink = "~/SiteAdmin/Secure/Inventory/Products/View.aspx?mode=highlight";
        private int ItemId = 0;
        private string portalIds = string.Empty;
        private string AssociateName = string.Empty;
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get Portals
            this.portalIds = UserStoreAccess.GetAvailablePortals;

            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (!Page.IsPostBack)
            {
                this.Bind();
                this.BindProductName();
                this.BindHighLightType();
                this.BindLocaleDropdown();
            }
        }
        #endregion

        #region Events

        /// <summary>
        /// Add Selected Addons Button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddSelectedAddons_Click(object sender, EventArgs e)
        {
            ProductAdmin AdminAccess = new ProductAdmin();
            StringBuilder sb = new StringBuilder();
            StringBuilder highlightName = new StringBuilder();

            // Loop through the grid values
            foreach (GridViewRow row in uxGrid.Rows)
            {
                CheckBox check = (CheckBox)row.Cells[0].FindControl("chkProductHighlight") as CheckBox;

                // Get AddOnId
                int HighlightId = int.Parse(row.Cells[1].Text);
                int DisplayOrder = int.Parse(row.Cells[4].Text);
                string name = row.Cells[2].Text;

                if (check.Checked)
                {
                    ProductHighlight entity = new ProductHighlight();

                    // Set Properties
                    entity.ProductID = this.ItemId;
                    entity.HighlightID = HighlightId;
                    entity.DisplayOrder = DisplayOrder;
                    highlightName.Append(name + ",");

                    if (!AdminAccess.IsHighlightExists(this.ItemId, HighlightId))
                    {
                        AdminAccess.AddProductHighlight(entity);
                        check.Checked = false;
                    }
                    else
                    {
                        sb.Append(name + ",");
                        lblErrorMessage.Visible = true;
                    }
                }
            }

            if (sb.ToString().Length > 0)
            {
                sb.Remove(sb.ToString().Length - 1, 1);

                // Display Error message
                lblErrorMessage.Text = "The following highlight(s) are already associated with this product.<br/>" + sb.ToString();
            }
            else
            {
                ProductAdmin prodAdmin = new ProductAdmin();
                Product entity = prodAdmin.GetByProductId(this.ItemId);
                if (highlightName.Length > 0)
                {
                    highlightName.Remove(highlightName.Length - 1, 1);

                    this.AssociateName = "Associated Highlights " + highlightName + " to Product " + entity.Name;
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, entity.Name);
                    Response.Redirect(this.ViewPageLink + "&itemid=" + this.ItemId);
                }
                else
                {
                    lblErrorMessage.Text = "At least one Highlight should be selected.";
                    lblErrorMessage.Visible = true;
                }
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ViewPageLink + "&itemid=" + this.ItemId);
        }

        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            uxGrid.DataSource = this.BindSearchData();
            uxGrid.DataBind();
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClearSearch_Click(object sender, EventArgs e)
        {
            ddlHighlightType.ClearSelection();
            ddlLocales.ClearSelection();
            txtName.Text = string.Empty;
            this.BindHighLightType();
            this.Bind();
        }

        #endregion

        #region Grid Events
        /// <summary>
        /// Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.Bind();
        }

        /// <summary>
        /// Get Hightlight Type Name
        /// </summary>
        /// <param name="obj">The value of Object</param>
        /// <returns>Returns the Highlight Type Name</returns>
        protected string GetHighlightTypeName(object obj)
        {
            if (obj is Highlight)
            {
                return (obj as Highlight).HighlightTypeIDSource.Name;
            }

            return null;
        }

        #endregion

        #region Bind Methods

        /// <summary>
        /// Bind the locale dropdown
        /// </summary>
        private void BindLocaleDropdown()
        {
            HighlightAdmin AdminAccess = new HighlightAdmin();

            // bind the dropdown.
            ddlLocales.DataSource = AdminAccess.GetAllLocaleId();
            ddlLocales.DataTextField = "LocaleDescription";
            ddlLocales.DataValueField = "LocaleId";
            ddlLocales.DataBind();

            if (ddlLocales.Items.Count == 0)
            {
                lblLocale.Visible = false;
                ddlLocales.Visible = false;
            }
        }

        /// <summary>
        /// Bind highlight Grid - all Addons
        /// </summary>
        private void Bind()
        {
            // List of product Highlights
            TList<Highlight> ListHighLight = UserStoreAccess.CheckStoreAccess(this.BindSearchData());
            if (ListHighLight.Count > 0)
            {
                uxGrid.DataSource = ListHighLight;
                uxGrid.DataBind();
            }
            else
            {
                btnAddSelectedAddons.Enabled = false;
                btnBottomCancel.Enabled = false;
            }
        }

        private void BindProductName()
        {
            ProductAdmin ProductAdminAccess = new ProductAdmin();
            Product entity = ProductAdminAccess.GetByProductId(this.ItemId);
            if (entity != null)
            {
                lblTitle.Text = lblTitle.Text + ": " + entity.Name;
            }
        }

        /// <summary>
        /// Binds Highlight Type drop-down list
        /// </summary>
        private void BindHighLightType()
        {
            ZNode.Libraries.Admin.HighlightAdmin highlight = new HighlightAdmin();
            ddlHighlightType.DataSource = highlight.GetAllHighLightType();
            ddlHighlightType.DataTextField = "Name";
            ddlHighlightType.DataValueField = "HighlightTypeId";
            ddlHighlightType.DataBind();
            ListItem item2 = new ListItem("ALL", string.Empty);
            ddlHighlightType.Items.Insert(0, item2);
            ddlHighlightType.SelectedIndex = 0;
        }

        /// <summary>
        /// Binds Search Data
        /// </summary>
        /// <returns>Returns the Highlight Name</returns>
        private TList<Highlight> BindSearchData()
        {
            HighlightService hs = new HighlightService();
            HighlightQuery hq = new HighlightQuery();

            if (txtName.Text.Trim().Length > 0)
            {
                hq.Append("Name", "*" + Server.HtmlEncode(txtName.Text) + "*");
            }

            if (ddlHighlightType.SelectedValue.Trim().Length > 0)
            {
                hq.Append("HighlightTypeID", ddlHighlightType.SelectedValue);
            }

            if (ddlLocales.SelectedValue.Trim().Length > 0)
            {
                hq.Append("LocaleId", ddlLocales.SelectedValue);
            }

            TList<Highlight> highlightList = hs.Find(hq.GetParameters());

            if (highlightList != null)
            {
                hs.DeepLoad(highlightList, true, DeepLoadType.IncludeChildren, typeof(HighlightType));

                highlightList.Sort("DisplayOrder");
            }

            return highlightList;
        }

        #endregion
    }
}