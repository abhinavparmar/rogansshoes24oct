using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Site Admin Admin_Secure_catalog_product_edit_additionalInfo class
    /// </summary>
    public partial class EditAdditionalInfo : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId = 0;
        private string ManagePageLink = "~/SiteAdmin/Secure/Inventory/Products/View.aspx?mode=additional&itemid=";
        private string CancelPageLink = "~/SiteAdmin/Secure/Inventory/Products/View.aspx?mode=additional&itemid=";
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (!Page.IsPostBack)
            {
                if (this.ItemId > 0)
                {
                    lblTitle.Text = "Edit Additional Info for ";

                    // Bind Sku Details
                    this.Bind();
                }
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit Button Click Event - Fires when Submit button is triggered
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            Product product = new Product();

            // If edit mode then get all the values first
            if (this.ItemId > 0)
            {
                product = productAdmin.GetByProductId(this.ItemId);
            }

            product.FeaturesDesc = ctrlHtmlPrdFeatures.Html.Trim();
            product.Specifications = ctrlHtmlPrdSpec.Html.Trim();
            product.AdditionalInformation = CtrlHtmlProdInfo.Html.Trim();

            bool status = false;

            try
            {
                if (this.ItemId > 0)
                {
                    // Product Name
                    status = productAdmin.Update(product);
                }

                if (status)
                {
                    string EditedValue = string.Empty;
                    string AssociateName = "Edit Additional Info - " + product.Name;
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, product.Name);

                    Response.Redirect(this.ManagePageLink + this.ItemId);
                }
                else
                {
                    lblError.Text = "Unable to update product additional information settings. Please try again.";
                }
            }
            catch (Exception)
            {
                lblError.Text = "Unable to update product additional information settings. Please try again.";
                return;
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.CancelPageLink + this.ItemId);
        }
        #endregion

        #region Bind Methods
        private void Bind()
        {
            // Create Instance for Product Admin and Product entity
            ZNode.Libraries.Admin.ProductAdmin ProdAdmin = new ProductAdmin();
            Product _Product = ProdAdmin.GetByProductId(this.ItemId);

            if (_Product != null)
            {
                ctrlHtmlPrdFeatures.Html = _Product.FeaturesDesc;
                ctrlHtmlPrdSpec.Html = _Product.Specifications;
                CtrlHtmlProdInfo.Html = _Product.AdditionalInformation;
                lblTitle.Text += "\"" + _Product.Name + "\"";
            }
        }
        #endregion
    }
}