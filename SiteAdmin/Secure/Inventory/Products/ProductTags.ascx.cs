﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Site Admin  Admin_Secure_catalog_product_product_tags class
    /// </summary>
    public partial class ProductTags : System.Web.UI.UserControl
    {
        #region Private Member Variables
        private int ItemId = 0;
        private int SkuId = 0;
        private string _ItemType = "ProductId";
        private string AssociateName = string.Empty;

        /// <summary>
        /// Gets or sets the Item Type
        /// </summary>
        public string ItemType
        {
            get
            {
                return this._ItemType;
            }

            set
            {
                this._ItemType = value;
            }
        }
        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }

            // Get ItemId from querystring        
            if (Request.Params["skuid"] != null)
            {
                this.SkuId = int.Parse(Request.Params["skuid"]);
            }

            if (!Page.IsPostBack)
            {
                this.BindGrid();
            }

        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Grid Data Bound Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_DataBound(object sender, EventArgs e)
        {
            bool visible = false;
            bool row = true;
            foreach (GridViewRow gvr in uxGrid.Rows)
            {
                gvr.Visible = (gvr.FindControl("dlTagNames") as DataList).Items.Count > 0;

                if (gvr.Visible)
                {
                    gvr.CssClass = row ? "RowStyle" : "AlternatingRowStyle";
                    row = row ? false : true;
                }

                visible = visible || gvr.Visible;
            }

            uxGrid.Visible = visible;

            lblEmptyGrid.Visible = !uxGrid.Visible;
        }

        /// <summary>
        /// Add Client side event to the Delete Button in the Grid.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Retrieve the Button control from the Fourth column.
                LinkButton DeleteButton = (LinkButton)e.Row.Cells[3].FindControl("DeleteTag");

                // Add Client Side confirmation
                DeleteButton.OnClientClick = "return confirm('Are you sure you want to delete this item?');";
            }
        }

        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindTags();
        }

        /// <summary>
        /// Bind tag Group
        /// </summary>
        /// <param name="tgg">The value of TagGroup TList</param>
        protected void BindTagGroup(TList<TagGroup> tgg)
        {
            this.ddlTagGroups.DataSource = tgg;
            this.ddlTagGroups.DataTextField = "TagGroupLabel";
            this.ddlTagGroups.DataValueField = "TagGroupId";
            this.ddlTagGroups.DataBind();

            if (tgg.Count == 0)
            {
                lblError.Text = "There are no Tag Groups associated with this product's category";
                lblError.Visible = true;
            }
        }

        /// <summary>
        /// Bind Tags Method
        /// </summary>
        protected void BindTags()
        {
            if (!string.IsNullOrEmpty(ddlTagGroups.SelectedValue))
            {
                TagService ts = new TagService();

                this.chklstTags.DataSource = ts.GetByTagGroupID(Convert.ToInt32(ddlTagGroups.SelectedValue));
                this.chklstTags.DataTextField = "TagName";
                this.chklstTags.DataValueField = "TagId";
                this.chklstTags.DataBind();

                TagProductSKUService tpss = new TagProductSKUService();
                TList<TagProductSKU> tpsList;
                if (this._ItemType == "SkuId")
                {
                    tpsList = tpss.GetBySKUID(this.SkuId);
                }
                else
                {
                    tpsList = tpss.GetByProductID(this.ItemId);
                }

                if (tpsList.Count > 0)
                {
                    foreach (ListItem li in chklstTags.Items)
                    {
                        li.Selected = tpsList.Find("TagID", Convert.ToInt32(li.Value)) != null;
                    }
                }
            }
        }

        /// <summary>
        /// Bind Grid method
        /// </summary>
        protected void BindGrid()
        {
            TList<TagGroup> tgg = this.GetAssociatedTagGroups();
            this.BindTagGroup(tgg);

            uxGrid.Visible = true;

            uxGrid.DataSource = tgg;

            uxGrid.DataBind();
        }

        /// <summary>
        /// Get Tag Names Method
        /// </summary>
        /// <param name="tagGroupId">The value of Tag Group id</param>
        /// <returns>Returns Tag Names</returns>
        protected string GetTagNames(object tagGroupId)
        {
            TagProductSKUService tpss = new TagProductSKUService();
            TList<TagProductSKU> tpsList;
            if (this._ItemType == "SkuId")
            {
                tpsList = tpss.GetBySKUID(this.SkuId);
            }
            else
            {
                tpsList = tpss.GetByProductID(this.ItemId);
            }

            TagService ts = new TagService();
            string tagNames = string.Empty;
            if (tpsList.Count > 0)
            {
                foreach (TagProductSKU tps in tpsList)
                {
                    Tag tag = ts.GetByTagID(tps.TagID.Value);
                    if (tag.TagGroupID == Convert.ToInt32(tagGroupId))
                    {
                        if (!string.IsNullOrEmpty(tagNames))
                        {
                            tagNames += ",";
                        }

                        tagNames += tag.TagName;
                    }
                }
            }

            return tagNames;
        }

        /// <summary>
        /// Get Tags Method
        /// </summary>
        /// <param name="tagGroupId">The value of tagGroupID</param>
        /// <returns>Returns Tag TList</returns>
        protected TList<Tag> GetTags(object tagGroupId)
        {
            TagProductSKUService tpss = new TagProductSKUService();
            TList<TagProductSKU> tpsList;
            if (this._ItemType == "SkuId")
            {
                tpsList = tpss.GetBySKUID(this.SkuId);
            }
            else
            {
                tpsList = tpss.GetByProductID(this.ItemId);
            }

            TagService ts = new TagService();
            TList<Tag> ttag = new TList<Tag>();
            if (tpsList.Count > 0)
            {
                foreach (TagProductSKU tps in tpsList)
                {
                    Tag tag = ts.GetByTagID(tps.TagID.Value);
                    if (tag.TagGroupID == Convert.ToInt32(tagGroupId))
                    {
                        ttag.Add(tag);
                    }
                }
            }

            return ttag;
        }

        /// <summary>
        /// Update Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Update_Click(object sender, EventArgs e)
        {
            int flag = 0;
            StringBuilder sb = new StringBuilder();

            TagProductSKUService service = new TagProductSKUService();
            TList<TagProductSKU> tpsList;
            if (this._ItemType == "SkuId")
            {
                tpsList = service.GetBySKUID(this.SkuId);
            }
            else
            {
                tpsList = service.GetByProductID(this.ItemId);
            }

            foreach (ListItem li in chklstTags.Items)
            {
                if (li.Selected == true)
                {

                    flag = 1;
                    lblError.Visible = false;

                    if (tpsList.Find("TagID", Convert.ToInt32(li.Value)) == null)
                    {
                        TagProductSKU tps = new TagProductSKU();
                        tps.TagID = Convert.ToInt32(li.Value);
                        if (this._ItemType == "SkuId")
                        {
                            tps.SKUID = this.SkuId;
                        }
                        else
                        {
                            tps.ProductID = this.ItemId;
                        }

                        service.Save(tps);
                    }

                    sb.Append(li.Text + ",");
                }

                else
                {
                    TagProductSKU tp;
                    if ((tp = tpsList.Find("TagID", Convert.ToInt32(li.Value))) != null)
                    {

                        service.Delete(tp);
                    }

                }


            }

            if (flag == 0)
            {
                lblError.Visible = true;
                lblError.Text = "Please select any facets";
                return;
            }


            ProductAdmin prodAdmin = new ProductAdmin();
            Product entity = prodAdmin.GetByProductId(this.ItemId);

            sb.Remove(sb.Length - 1, 1);
            this.AssociateName = "Associated  Facet " + sb + " to Product " + entity.Name;
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, entity.Name);
            pnlTagsList.Visible = true;
            tpsList.Dispose();
            service = null;
            this.BindGrid();
            this.pnlEditTags.Visible = false;



        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            this.pnlEditTags.Visible = false;
            pnlTagsList.Visible = true;
            lblError.Visible = false;
        }

        /// <summary>
        /// Add Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Add_Click(object sender, EventArgs e)
        {
            lblError.Visible = false;
            this.pnlEditTags.Visible = true;
            pnlTagsList.Visible = false;
            if (ddlTagGroups.Items.Count > 0)
            {
                this.ddlTagGroups.SelectedIndex = 0;
            }

            this.BindTags();
        }

        /// <summary>
        /// Tag Group Drop Down list Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlTagGroups_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindTags();
        }

        /// <summary>
        /// Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ProductAdmin prodAdmin = new ProductAdmin();
            Product entity = prodAdmin.GetByProductId(this.ItemId);

            if (e.CommandName == "TagEdit")
            {
                ListItem li = this.ddlTagGroups.Items.FindByValue(e.CommandArgument as string);
                if (li != null)
                {
                    this.ddlTagGroups.SelectedIndex = this.ddlTagGroups.Items.IndexOf(li);
                    this.BindTags();
                    this.pnlEditTags.Visible = true;
                    pnlTagsList.Visible = false;
                }

                this.AssociateName = "Associated Facets " + li.Text + " to Product " + entity.Name;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, entity.Name);

            }
            else if (e.CommandName == "TagDelete")
            {
                TagService service = new TagService();
                TList<Tag> tagList = service.GetByTagGroupID(Convert.ToInt32(e.CommandArgument));

                StringBuilder sb = new StringBuilder();
                foreach (Tag tag in tagList)
                {
                    sb.Append(tag.TagName + ",");
                    this.RemoveTagProductSku(tag);
                }

                this.AssociateName = "Delete Association between Facet " + sb + " and Product " + entity.Name;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, entity.Name);
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Remove Tag Product SKU Method
        /// </summary>
        /// <param name="tag">Instance of Tag</param>
        private void RemoveTagProductSku(Tag tag)
        {
            TagProductSKUService service = new TagProductSKUService();
            TList<TagProductSKU> tps = service.GetByTagID(tag.TagID);

            foreach (TagProductSKU tp in tps)
            {
                if (tp.ProductID == this.ItemId)
                {
                    service.Delete(tp);
                }
            }

            this.BindGrid();
        }

        /// <summary>
        /// Get Associated Tag Groups
        /// </summary>
        /// <returns>Returns the TagGroup TList</returns>
        private TList<TagGroup> GetAssociatedTagGroups()
        {
            TagGroupService tgs = new TagGroupService();
            TagService ts = new TagService();
            TList<TagGroup> tgg = new TList<TagGroup>();

            if (this.ItemId > 0)
            {
                ProductService ps = new ProductService();

                Product product = ps.DeepLoadByProductID(this.ItemId, true, ZNode.Libraries.DataAccess.Data.DeepLoadType.IncludeChildren, typeof(TList<ProductCategory>));

                if (product != null && product.ProductCategoryCollection != null)
                {
                    foreach (ProductCategory pc in product.ProductCategoryCollection)
                    {
                        foreach (TagGroup tg in tgs.GetByCategoryIDFromTagGroupCategory(pc.CategoryID))
                        {
                            if (tgg.IndexOf(tg) == -1 && ts.GetByTagGroupID(tg.TagGroupID).Count > 0)
                            {
                                tgg.Add(tg);
                            }
                        }
                    }
                }
            }

            return tgg;
        }
        #endregion
    }
}