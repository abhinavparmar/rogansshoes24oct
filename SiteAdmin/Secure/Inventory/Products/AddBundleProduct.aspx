<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" ValidateRequest="false" Title="Manage Products - Add Bundle Products" Inherits="SiteAdmin.Secure.Inventory.Products.AddBundleProduct" CodeBehind="AddBundleProduct.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="CategoryAutoComplete" Src="~/SiteAdmin/Controls/Default/CategoryAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ProductTypeAutoComplete" Src="~/SiteAdmin/Controls/Default/ProductTypeAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div>
        <div>
            <div class="LeftFloat" style="width: 50%">
                <h1>Add a Bundle Product</h1>
                <p style="width: 870px;">You can use this page to add individual products to a bundled product offering. Please note that adding items to a product bundle will not change the price for the bundle.</p>
            </div>
            <div class="LeftFloat ImageButtons" style="width: 49%;" align="right">
                <asp:Button CssClass="Size175" ID="btnBack" CausesValidation="False" Text="<< Back to Product Detail"
                    runat="server" OnClick="BtnBack_Click"></asp:Button>
            </div>
        </div>
        <!-- Search product panel -->
        <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <h4 class="SubTitle">Search Product</h4>
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">Product Name</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtproductname" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">Product Number</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtproductnumber" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">SKU</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtsku" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">Catalog</span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlCatalog" runat="server"></asp:DropDownList></span>
                    </div>
                </div>
                <div class="RowStyle">

                    <div class="ItemStyle">
                        <span class="FieldStyle">Product Type</span><br />
                        <span class="ValueStyle">
                            <ZNode:ProductTypeAutoComplete ID="dproducttype" runat="server" />
                        </span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">Product Categories</span>
                        <br />
                        <span class="ValueStyle">
                            <ZNode:CategoryAutoComplete ID="dproductcategory" runat="server" />
                        </span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <br />
                </div>
                <div>
                    <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_clear_highlight.gif';"
                        onmouseout="this.src='../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                        runat="server" AlternateText="Clear" OnClick="BtnClear_Click" />
                    <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../Themes/Images/buttons/button_search_highlight.gif';"
                        onmouseout="this.src='../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                        runat="server" AlternateText="Search" OnClick="BtnSearch_Click" />
                </div>
            </div>
        </asp:Panel>
        <!-- Product List -->
        <asp:Panel ID="pnlProductList" runat="server" Visible="false">
            <h4 class="GridTitle">Product List</h4>  
            <br />
            <small><asp:Label ID="lblSelect" runat="server" Text="Select product to add."></asp:Label></small>
            <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" CaptionAlign="Left" AutoGenerateColumns="False"
                GridLines="None" AllowPaging="True" PageSize="10" OnPageIndexChanging="UxGrid_PageIndexChanging"
                OnRowDataBound="UxGrid_RowDataBound" EmptyDataText="No Products exist in the database."
                Width="100%" CellPadding="4">
                <Columns>
                    <asp:TemplateField HeaderText="Select" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkProduct" runat="server" Enabled='<%# !(int.Parse(DataBinder.Eval(Container.DataItem, "ProductId").ToString()) == ItemId) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ProductId" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText="Image" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img id="prodImage" alt=" " src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>'
                                runat="server" style="border: none" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText="Name" HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText="Is Active?" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img alt="" id="chMark" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
            <div>
                <ZNode:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div>
                <asp:Label ID="lblError" runat="server" Text="Label" Visible="false" CssClass="Error"
                    ForeColor="red"></asp:Label>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div class="ClearBoth">
                <br />
            </div>
            <div>
                <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" OnClick="Update_Click" CausesValidation="true" />
                <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="Cancel_Click" />
            </div>
        </asp:Panel>
        <div>
            <ZNode:Spacer ID="Spacer3" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
    </div>
</asp:Content>
