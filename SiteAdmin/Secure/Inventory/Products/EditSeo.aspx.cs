using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Site Admin  Admin_Secure_catalog_product_edit_seo
    /// </summary>
    public partial class EditSeo : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId = 0;
        private string ManagePageLink = "~/SiteAdmin/Secure/Inventory/Products/View.aspx?mode=seo&itemid=";
        private string CancelPageLink = "~/SiteAdmin/Secure/Inventory/Products/View.aspx?mode=seo&itemid=";
        private string AssociateName = string.Empty;
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (!Page.IsPostBack)
            {
                if (this.ItemId > 0)
                {
                    lblTitle.Text = "Edit SEO for ";

                    // Bind Sku Details
                    this.Bind();
                }
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit Button Click Event - Fires when Submit button is triggered
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            UrlRedirectAdmin urlRedirectAdmin = new UrlRedirectAdmin();
            ProductAdmin productAdmin = new ProductAdmin();
            Product product = new Product();
            string mappedSEOUrl = string.Empty;

            // If edit mode then get all the values first
            if (this.ItemId > 0)
            {
                product = productAdmin.GetByProductId(this.ItemId);

                if (product.SEOURL != null)
                {
                    mappedSEOUrl = product.SEOURL;
                }
            }

            // Set properties
            product.SEOTitle = Server.HtmlEncode(txtSEOTitle.Text.Trim());
            product.SEOKeywords = Server.HtmlEncode(txtSEOMetaKeywords.Text.Trim());
            product.SEODescription = Server.HtmlEncode(txtSEOMetaDescription.Text.Trim());
            product.SEOURL = null;

            if (txtSEOUrl.Text.Trim().Length > 0)
            {
                product.SEOURL = txtSEOUrl.Text.Trim().Replace(" ", "-");
            }

            if (string.Compare(product.SEOURL, mappedSEOUrl, true) != 0)
            {
                if (urlRedirectAdmin.SeoUrlExists(product.SEOURL, product.ProductID))
                {
                    lblError.Text = "The SEO Friendly URL you entered is already in use on another page. Please select another name for your URL";
                    return;
                }
            }

            bool status = false;

            if (this.ItemId > 0)
            {
                // PRODUCT UPDATE
                status = productAdmin.Update(product);
            }

            if (status)
            {
                if (chkAddURLRedirect.Checked)
                {
                    urlRedirectAdmin.AddUrlRedirectTable(SEOUrlType.Product, mappedSEOUrl, product.SEOURL, product.ProductID.ToString());
                }

                urlRedirectAdmin.UpdateUrlRedirect(mappedSEOUrl);

                product = productAdmin.GetByProductId(this.ItemId);

                this.AssociateName = "Edit SEO Settings - " + product.Name;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, product.Name);

                Response.Redirect(this.ManagePageLink + this.ItemId);
            }
            else
            {
                lblError.Text = "Unable to update product SEO settings. Please try again.";
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.CancelPageLink + this.ItemId);
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind Method
        /// </summary>
        private void Bind()
        {
            // Create Instance for Product Admin and Product entity
            ZNode.Libraries.Admin.ProductAdmin ProdAdmin = new ProductAdmin();
            Product _Product = ProdAdmin.GetByProductId(this.ItemId);

            if (_Product != null)
            {
                txtSEOTitle.Text = Server.HtmlDecode(_Product.SEOTitle);
                txtSEOMetaKeywords.Text = Server.HtmlDecode(_Product.SEOKeywords);
                txtSEOMetaDescription.Text = Server.HtmlDecode(_Product.SEODescription);
                txtSEOUrl.Text = _Product.SEOURL;
                lblTitle.Text += "\"" + _Product.Name + "\"";
            }
        }
        #endregion
    }
}