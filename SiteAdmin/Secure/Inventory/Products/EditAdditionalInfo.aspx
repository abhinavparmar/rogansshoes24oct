<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="SiteAdmin.Secure.Inventory.Products.EditAdditionalInfo"
    Title="Manage Products - Edit Additional Info" CodeBehind="EditAdditionalInfo.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc1" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="ZNode" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server" Text="Edit Additional Info for"></asp:Label></h1>
            <uc1:DemoMode ID="DemoMode1" runat="server"></uc1:DemoMode>
        </div>
        <div style="text-align: right">
            <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
        <div class="ClearBoth">
        </div>
        <asp:Label ID="lblError" CssClass="Error" runat="server"></asp:Label>
        <div>
            <p>
                Use this page to edit product features,specifications for this product.
            </p>
            <h4 class="SubTitle">Enter Product Features</h4>
            <p class="HintStyle">
                Add product features here to have a "Features" tab be displayed on the product page
                <br />
                (you must use the StoreTabs.master template with this feature).
            </p>
            <div class="ValueStyle">
                <ZNode:HtmlTextBox ID="ctrlHtmlPrdFeatures" runat="server"></ZNode:HtmlTextBox>
            </div>
            <h4 class="SubTitle">Enter Product Specifications</h4>
            <p class="HintStyle">
                Add product specifications here to have a "Specifications" tab be displayed on the
                product page
                <br />
                (you must use the StoreTabs.master template with this feature).
            </p>
            <div class="ValueStyle">
                <ZNode:HtmlTextBox ID="ctrlHtmlPrdSpec" runat="server"></ZNode:HtmlTextBox>
            </div>
            <h4 class="SubTitle">Enter Shipping Information</h4>
            <p class="HintStyle">
                Add additional product information here to have a "Shipping Information" tab be
                displayed on the product page
                <br />
                (you must use the StoreTabs.master template with this feature).
            </p>
            <div class="ValueStyle">
                <ZNode:HtmlTextBox ID="CtrlHtmlProdInfo" runat="server"></ZNode:HtmlTextBox>
            </div>
        </div>
        <div class="ClearBoth"></div>
        <div>
            <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cacnel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>
