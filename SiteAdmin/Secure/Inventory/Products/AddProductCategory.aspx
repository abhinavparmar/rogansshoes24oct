﻿<%@ Page Title="Manage Products - Add Product Category" Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master"
    AutoEventWireup="true" CodeBehind="AddProductCategory.aspx.cs"  Inherits="SiteAdmin.Secure.Inventory.Products.AddProductCategory" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode"
    TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <div>
        <div>
            <div class="LeftFloat" style="width: 80%">
                <h1>Add Categories to Product :
                    <asp:Label ID="lblProdName" runat="server" Text="Label"></asp:Label>
                </h1>
            </div>
            <br />
            <div class="LeftFloat ImageButtons" style="width: 19%" align="right">
                <asp:Button CssClass="Size175" ID="btnBack" CausesValidation="False" Text="<< Back to Product Detail"
                    runat="server" OnClick="BtnBack_Click"></asp:Button>
            </div>
        </div>
        <ZNode:DemoMode ID="DemoMode1" runat="server"></ZNode:DemoMode>
        <!-- Search product panel -->
        <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <h4 class="SubTitle">Search Category</h4>
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">Category</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtDepartmentName" runat="server"></asp:TextBox></span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <br />
                </div>
                <div>
                    <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_clear_highlight.gif';"
                        onmouseout="this.src='../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                        runat="server" AlternateText="Clear" OnClick="BtnClear_Click" />
                    <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../Themes/Images/buttons/button_search_highlight.gif';"
                        onmouseout="this.src='../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                        runat="server" AlternateText="Search" OnClick="BtnSearch_Click" />
                </div>
            </div>
            <div style="margin-bottom: 10px;"></div>
        </asp:Panel>
        <!-- Department List -->
        <asp:Panel ID="pnlDepartmentList" runat="server" Visible="false">
            <h4 class="GridTitle">Category List</h4>
            <br />
            <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" CaptionAlign="Left" AutoGenerateColumns="False"
                GridLines="None" AllowPaging="True" PageSize="10" OnPageIndexChanging="UxGrid_PageIndexChanging"
                OnRowDataBound="UxGrid_RowDataBound" EmptyDataText="No category exist in the database."
                Width="100%" CellPadding="4">
                <Columns>
                    <asp:TemplateField HeaderText="Select" ItemStyle-Width="50" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkCategory" runat="server" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="CategoryId" HtmlEncode="false" HeaderText="ID" HeaderStyle-HorizontalAlign="Left">
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Title" HtmlEncode="false" HeaderText="Title" HeaderStyle-HorizontalAlign="Left">
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
            <div>
                <ZNode:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div>
                <asp:Label ID="lblError" runat="server" Text="Label" Visible="false" CssClass="Error"
                    ForeColor="red"></asp:Label>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div class="ClearBoth">
                <br />
            </div>
            <div class="LeftFloat ImageButtons" style="width: 19%" align="right">
                <asp:Button CssClass="Size175" ID="Button2" CausesValidation="False" Text="Add Selected Categories"
                    runat="server" OnClick="Update_Click"></asp:Button>
            </div>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="Cancel_Click" />

        </asp:Panel>
        <div>
            <ZNode:Spacer ID="Spacer3" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
    </div>
</asp:Content>
