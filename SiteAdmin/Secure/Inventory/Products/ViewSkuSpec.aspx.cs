﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace WebApp.SiteAdmin.Secure.Inventory.Products
{
    public partial class ViewSkuSpec : System.Web.UI.Page
    {
        # region Protected Member Variables

        protected int ItemID = 0; 
        protected int productSKUID = 0;

        protected string Associatename = string.Empty;
        protected string viewLink = "~/SiteAdmin/Secure/Inventory/Products/view.aspx?itemid=";
        protected string AddSpecItemLink = "AddSpecItems.aspx?itemid=";
        
        # endregion

        # region Protected Properties

        /// <summary>
        /// Retrieves the product name
        /// </summary>
        protected string GetProductName
        {
            get
            {
                ProductAdmin productAdmin = new ProductAdmin();
                Product product = productAdmin.GetByProductId(ItemID);

                if (product != null)
                {
                    return product.Name;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Retrieves the SKU name
        /// </summary>
        protected string GetSKUName
        {
            get
            {
                SKUAdmin SkuAdmin = new SKUAdmin();
                SKU SpecName = new SKU();
                SpecName = SkuAdmin.GetBySKUID(productSKUID);

                if (!string.IsNullOrEmpty(SpecName.SKU))
                {
                    return SpecName.SKU;
                }

                return string.Empty;
            }
        }

        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //Retrieve Product Id from Query string
            if (Request.Params["itemid"] != null)
            {
                ItemID = int.Parse(Request.Params["itemid"].ToString());
            }

            //Retrieve SKU Id from Query string
            if (Request.Params["skuid"] != null)
            {
                productSKUID = int.Parse(Request.Params["skuid"].ToString());
            }
            
            if (!IsPostBack)
            {
                lblProductSKU.Text = "View Specification - " + GetProductName + " - " + GetSKUName;
                // Bind Grid - Product Specification Items
                this.BindSpecificationItems();
            }
        }

        #endregion

        #region Bind Method

        /// <summary>
        /// Bind Products Specification
        /// </summary>
        public void BindSpecificationItems()
        {
            ProductSpecificationsHelper ProdSpec = new ProductSpecificationsHelper();
            uxSpecGrid.DataSource = ProdSpec.GetProductSpecifications_ByProductIdSkuId(ItemID, productSKUID);
            uxSpecGrid.DataBind();
        }

        #endregion

        #region Custom Events

        /// <summary>
        /// Redirecting to add Product Specification
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddSpecItems_Click(object sender, EventArgs e)
        {
            Response.Redirect(AddSpecItemLink + ItemID + "&skuid=" + productSKUID);
        }

        /// <summary>
        /// Cancle button events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(viewLink + ItemID + "&mode=inventory");
        }

        #endregion

        # region Related to SKU Product Specification Grid Events
        
        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void uxSpecGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("RemoveItem"))
            {
                ZeonProductSpecificationsService SpecService = new ZeonProductSpecificationsService();

                bool Check = SpecService.Delete((int.Parse(e.CommandArgument.ToString())));
                
                if (Check)
                {
                    this.BindSpecificationItems();
                }
            }
        }

        /// <summary>
        /// SKU Product Specification Index Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void uxSpecGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxSpecGrid.PageIndex = e.NewPageIndex;
            this.BindSpecificationItems();
        }

        #endregion
    }
}