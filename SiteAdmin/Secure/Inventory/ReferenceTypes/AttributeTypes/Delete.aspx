<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Inventory.ReferenceTypes.AttributeTypes.Delete" Codebehind="Delete.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc1" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">

<h5>Please Confirm<uc1:DemoMode ID="DemoMode1" runat="server" />
</h5>
    <p>
        Please confirm if you want to
        delete this Product Attribute Type titled "<b><%=ProductAttributeTypeName%></b>". Note that you will be unable to delete this type if there are products currently associated with this attribute type.
        This change cannot be undone.
    </p>
    <p><asp:Label ID="lblErrorMsg" runat="server" Visible="False" CssClass="Error"></asp:Label></p>
    <div>
        <asp:ImageButton ID="btnDelete" onmouseover="this.src='../../../../Themes/Images/buttons/button_delete_highlight.gif';"
            onmouseout="this.src='../../../../Themes/Images/buttons/button_delete.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_delete.gif"
            runat="server" AlternateText="Delete" OnClick="BtnDelete_Click" CausesValidation="true" />
        <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
            onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
            runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
    </div>
    <br /><br /><br />
 
</asp:Content>


