<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="SiteAdmin.Secure.Inventory.ReferenceTypes.AttributeTypes.AddAttribute" Codebehind="AddAttribute.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                    <asp:Label ID="lblTitle" runat="server"></asp:Label>
                <ZNode:DemoMode ID="DemoMode1" runat="server" />
            </h1>
        </div>
        
        <div class="ClearBoth">
        </div>
        <asp:Label ID="lblError" runat="server" />
        <div class="FieldStyle">
            Attribute Value <span class="Asterix">*</span>
            
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="Name" runat="server" MaxLength="50" Columns="30"></asp:TextBox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Name"
                Display="Dynamic" ErrorMessage="* Enter a Attribute Value" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            Display Order<span class="Asterix">*</span><br /></div>
        
        <div class="ValueStyle">
            <asp:TextBox ID="DisplayOrder" runat="server" MaxLength="9" Columns="5"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Display="Dynamic"
                ErrorMessage="* Enter a Display Order" ControlToValidate="DisplayOrder"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="DisplayOrder"
                Display="Dynamic" ErrorMessage="Enter a whole number." MaximumValue="999999999"
                MinimumValue="1" Type="Integer"></asp:RangeValidator>
        </div>
         <div class="ClearBoth">
        </div><div>
            <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>
