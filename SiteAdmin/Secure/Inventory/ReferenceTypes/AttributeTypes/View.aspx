<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" 
    Inherits="SiteAdmin.Secure.Inventory.ReferenceTypes.AttributeTypes.View" Codebehind="View.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <div class="LeftFloat" style="width: 50%; text-align: left">
            <h1>
                Attribute Values For:
                <asp:Label ID="lblAttributeType" runat="server"></asp:Label>
            </h1>
        </div>
        <div style="text-align: right" class="ImageButtons">
            <asp:Button ID="AttributeTypeList" runat="server" Text="<< Back To Attributes" OnClick="AttributeTypeList_Click" />
            <asp:Button ID="AddAttribute" runat="server" CssClass="Size100" Text="Add Value"
                OnClick="AddAttribute_Click" />       
        </div>
        <div align="left">
            <span style="float: left">
                <asp:Label ID="FailureText" runat="Server" EnableViewState="false" CssClass="Error" />                                        
            </span>     
            
        </div>
        <h4 class="GridTitle">
            Value List
        </h4>
        <uc1:Spacer ID="Spacer" runat="server" spacerheight="10" />
        <asp:GridView ID="uxGrid" runat="server" AllowPaging="True" AutoGenerateColumns="False"
            CaptionAlign="Left" CellPadding="4" CssClass="Grid" EmptyDataText="No Product Attributes exist in the database."
            ForeColor="#333333" GridLines="None" Width="100%" OnPageIndexChanging="UxGrid_PageIndexChanging"
            OnRowCommand="UxGrid_RowCommand" OnRowDeleting="UxGrid_RowDeleting">
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Attribute Value" HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="DisplayOrder" HeaderText="Display Order" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="EditAttribute" CommandName="Edit" CommandArgument='<%# Eval("AttributeId") %>'
                            runat="server" CssClass="actionlink" Text="Edit &raquo" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="DeleteAttribute" CommandName="Delete" CommandArgument='<%# Eval("AttributeId") %>'
                            runat="server" CssClass="actionlink" Text="Delete &raquo" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle CssClass="RowStyle" />
            <EditRowStyle CssClass="EditRowStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
        </asp:GridView>
        <uc1:Spacer ID="Spacer5" SpacerHeight="1" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
</asp:Content>
