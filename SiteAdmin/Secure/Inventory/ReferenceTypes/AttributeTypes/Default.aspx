<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" ValidateRequest="false" Inherits="SiteAdmin.Secure.Inventory.ReferenceTypes.AttributeTypes.Default" Codebehind="Default.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                Attribute Types
            </h1>
        </div>
        
        <div class="ButtonStyle">
               <zn:LinkButton ID="btnAddAttributeTypes" runat="server" 
                ButtonType="Button" OnClick="BtnAdd_Click" CausesValidation="False" Text="Add Attribute Type"
                ButtonPriority="Primary"/>

        </div>
       <p class="ClearBoth">
            Manage product attribute types such as "size", "color", etc.
        </p>
        <div class="ClearBoth" align="left">
            <br />
        </div>

        <h4 class="SubTitle">
            Search Attributes</h4>
        <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">Name</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtAttributeName" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle" runat="server" id="lblLocale">Locale</span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlLocales" runat="server" AppendDataBoundItems="true">
                            <asp:ListItem Value="">All</asp:ListItem>
                            </asp:DropDownList>
                        </span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_clear_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                        runat="server" AlternateText="Clear" OnClick="BtnClear_Click" />
                    <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                        runat="server" AlternateText="Search" OnClick="BtnSearch_Click" />
                </div>
            </div>
    </asp:Panel><br />
    <h4 class="GridTitle">
        Attribute List</h4>
    <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
    <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
        CellPadding="4" GridLines="None" OnSorting="UxGrid_Sorting" OnPageIndexChanging="UxGrid_PageIndexChanging"
        CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" EnableSortingAndPagingCallbacks="False"
        PageSize="25" AllowSorting="false" EmptyDataText="No Product Attributes exist in the database.">
        <Columns>
            <asp:BoundField DataField="AttributeTypeId" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
            <asp:BoundField SortExpression="Name" HtmlEncode="false" DataField="Name" HeaderText="Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="40%" />
            <asp:BoundField DataField="DisplayOrder" HeaderText="Display Order" HeaderStyle-HorizontalAlign="Left" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="btnView" Width="90px" CssClass="actionlink" runat="server" CommandName="View" Text="See Values &raquo"
                        ButtonType="Button" CommandArgument='<%# Eval("AttributeTypeId") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="btnEdit" Width="60px" CssClass="actionlink" runat="server" CommandName="Edit" Text="Edit &raquo"
                        ButtonType="Button" CommandArgument='<%# Eval("AttributeTypeId") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="btndelete" Width="60px" CssClass="actionlink" runat="server" CommandName="Delete"
                        Text="Delete &raquo" ButtonType="Button" CommandArgument='<%# Eval("AttributeTypeId") %>' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <FooterStyle CssClass="FooterStyle" />
        <RowStyle CssClass="RowStyle" />
        <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
        <HeaderStyle CssClass="HeaderStyle" />
        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
        <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
    </asp:GridView>
    <div>
        <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
    </div>
    </div>
</asp:Content>
