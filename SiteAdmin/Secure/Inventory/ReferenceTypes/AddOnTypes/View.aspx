<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Inventory.ReferenceTypes.AddOnTypes.View" ValidateRequest="false" Title="Product AddOn Type - View" Codebehind="View.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Display">
        <div class="LeftFloat" style="width: 50%; text-align: left">
            <h1>
                Product Add-On :
                <asp:Label ID="lblTitle" runat="server" Text="Label"></asp:Label></h1>
        </div>
        <div style="text-align: right" class="ImageButtons">
            <asp:Button ID="btneditAddon" runat="server" OnClick="EditAddOn_Click" Text="Edit Add-On" />
            <asp:Button ID="btnBack" runat="server" OnClick="BacktoAddOn_Click" Text="< Back to Add-On" />
        </div>
        <div class="ClearBoth">
        </div>
        <h4 class="SubTitle">
            General Information</h4>
        <div class="FieldStyle" nowrap="nowrap">
            Name</div>
        <div class="ValueStyle">
            <asp:Label ID="lblName" runat="server"></asp:Label>&nbsp;
        </div>
        <div class="FieldStyleA" nowrap="nowrap">
            Title</div>
        <div class="ValueStyleA">
            <asp:Label ID="lblAddOnTitle" runat="server"></asp:Label>&nbsp;
        </div>
        <div class="FieldStyle">
            Display Order</div>
        <div class="ValueStyle">
            <asp:Label ID="lblDisplayOrder" runat="server"></asp:Label></div>
        <div class="FieldStyleA">
            Display Type</div>
        <div class="ValueStyleA">
            <asp:Label ID="lblDisplayType" runat="server"></asp:Label>&nbsp;</div>
        <div class="FieldStyle">
            Optional</div>
        <div class="ValueStyle">
            <img id="chkOptionalInd" runat="server" alt="" src="" /></div>
        <div class="FieldStyleA">
            Locale</div>
        <div class="ValueStyleA">
            <asp:Label ID="lblLocale" runat="server"></asp:Label>&nbsp;</div>
        
        <div class="ClearBoth"><br /></div>
                
        <h4 class="SubTitle">
            Inventory Settings</h4>
        <div class="FieldStyleImg">
            <img id="chkCartInventoryEnabled" runat="server" alt="" src='' /></div>
        <div class="ValueStyleImg">
            Only Sell if Inventory Available (User can only add to cart if inventory is above
            0)
        </div>
        <div class="FieldStyleImgA">
            <img id="chkIsBackOrderEnabled" runat="server" alt="" src='' /></div>
        <div class="ValueStyleImgA">
            Allow Back Order (items can always be added to the cart. Inventory is reduced)</div>
        <div class="FieldStyleImg">
            <img id="chkIstrackInvEnabled" runat="server" alt="" src="" /></div>
        <div class="ValueStyleImg">
            Don't Track Inventory (items can always be added to the cart and inventory is not
            reduced)</div>
        <div class="FieldStyleA">
            In Stock Message</div>
        <div class="ValueStyleA">
            <asp:Label ID="lblInStockMsg" runat="server"></asp:Label>&nbsp;</div>
        <div class="FieldStyle" nowrap="nowrap">
            Out Of Stock Message</div>
        <div class="ValueStyle">
            <asp:Label ID="lblOutofStock" runat="server"></asp:Label>&nbsp;</div>
        <div class="FieldStyleA" nowrap="nowrap">
            Back Order Message</div>
        <div class="ValueStyleA">
            <asp:Label ID="lblBackOrderMsg" runat="server"></asp:Label>&nbsp;</div>
        <div class="ClearBoth"><br />
            <div style="width: 100%; text-align: right; display: inline-table;">
                <zn:LinkButton ID="btnAddNewAddOnValues" runat="server"
                    ButtonType="Button" OnClick="BtnAddNewAddOnValues_Click" Text="Add Value"
                    ButtonPriority="Primary" />
            </div>
            <div>
                <uc1:spacer id="Spacer6" spacerheight="10" spacerwidth="3" runat="server"></uc1:spacer>
            </div>
            <h4 class="GridTitle">
                Add-On Values
            </h4>
            <asp:GridView OnRowDataBound="UxGrid_RowDataBound" ID="uxGrid" runat="server" CssClass="Grid"
                AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                OnPageIndexChanging="UxGrid_PageIndexChanging" CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand"
                Width="100%" EnableSortingAndPagingCallbacks="False" PageSize="25" AllowSorting="True"
                EmptyDataText="No product Add-Ons exist in the database." 
                onrowdeleting="UxGrid_RowDeleting">
                <Columns>
                    <asp:BoundField DataField="AddOnValueId" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <a href='AddValues.aspx?itemid=<%=ItemId %>&AddOnValueId=<%# DataBinder.Eval(Container.DataItem, "AddOnvalueId").ToString()%>'>
                                <%# Eval("Name") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="SKU" HeaderText="SKU" HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText="Quantity On Hand">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="quantity" Text='<%# GetQuantity((int)Eval("AddOnvalueId")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Re-Order Level">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="ReOrderLevel" Text='<%# GetReOrderLevel((int)Eval("AddOnvalueId")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="DisplayOrder" HeaderText="DisplayOrder" HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText="Price" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "Retailprice","{0:c}").ToString()%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Default" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img alt="" id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "DefaultInd").ToString()))%>'
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:ButtonField CommandName="Edit" Text="Edit &raquo" ButtonType="Link">
                        <ControlStyle CssClass="actionlink" />
                    </asp:ButtonField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton CommandName="Delete" CausesValidation="false" ID="btnDelete" runat="server"
                                Text="Delete &raquo" CssClass="actionlink" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
        </div>

    </div>
</asp:Content>
