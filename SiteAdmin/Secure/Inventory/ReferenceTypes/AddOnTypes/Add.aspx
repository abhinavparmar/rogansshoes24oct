<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" 
    Inherits="SiteAdmin.Secure.Inventory.ReferenceTypes.AddOnTypes.Add" ValidateRequest="false" Title="Product AddOn Type - Add"
     Codebehind="Add.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
            <uc2:DemoMode ID="DemoMode1" runat="server" />
            <div>
                <asp:Label ID="lblMsg" CssClass="Error" EnableViewState="false" runat="server"></asp:Label></div>
        </div>
        <div style="text-align: right">
            <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" ValidationGroup="grpAddOn" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
        <div class="ClearBoth">
        </div>
        <div>
            <uc1:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <h4 class="SubTitle">
            General Settings</h4>
        <div class="FieldStyle">
            Name<span class="Asterix">*</span><br />
            <small>Enter an internal name for this Add-On (Ex. Model 1234 Color). This is not displayed
                to the customer</small></div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                ErrorMessage="* Enter Product Add-On Name" ValidationGroup="grpAddOn" CssClass="Error"
                Display="Dynamic"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            Title<span class="Asterix">*</span><br />
            <small>Enter a title for this product Add-On (Ex. "Color"). This is the label displayed
                for your customer.</small></div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtAddOnTitle" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAddOnTitle"
                ErrorMessage="* Enter Product Add-On Title" ValidationGroup="grpAddOn" CssClass="Error"
                Display="Dynamic"></asp:RequiredFieldValidator></div>
        <div class="FieldStyle">
            Enter a Description<br />
            <small>You can enter rich text description for this Add-On.</small></div>
        <div class="ValueStyle">
            <uc1:HtmlTextBox ID="ctrlHtmlText" runat="server"></uc1:HtmlTextBox>
        </div>
        <div class="FieldStyle">
            Display Order<span class="Asterix">*</span><br />
            <small>Enter a number. Items with a lower number are displayed first on the page.</small></div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtDisplayOrder" runat="server" MaxLength="9" Columns="5"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" CssClass="Error" runat="server" Display="Dynamic"
                ValidationGroup="grpAddOn" ErrorMessage="* Enter Display Order" ControlToValidate="txtDisplayOrder"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator1" runat="server" CssClass="Error" ControlToValidate="txtDisplayOrder"
                Display="Dynamic" ValidationGroup="grpAddOn" ErrorMessage="Enter Whole number."
                MaximumValue="999999999" MinimumValue="1" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
            Display Type</div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlDisplayType" runat="server">
                <asp:ListItem Value="DropDownList">Drop Down List</asp:ListItem>
                <asp:ListItem Value="RadioButton">Radio Buttons</asp:ListItem>
                <asp:ListItem Value="CheckBox">Check Boxes</asp:ListItem>
                <asp:ListItem Value="TextBox">Text Box</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="FieldStyle" runat="server" id="lblLocale" style="display:none;">
            Locale <span class="Asterix">*</span></div>
        <div class="ValueStyle" style="display:none;">
            <asp:DropDownList ID="ddlLocales" runat="server" AppendDataBoundItems="true">
                <asp:ListItem Value="">Select</asp:ListItem>
            </asp:DropDownList>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" CssClass="Error" ValidationGroup="grpAddOn"  runat="server" ControlToValidate="ddlLocales"
                Display="Dynamic" ErrorMessage="* Specify the Locale" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
        </div>
        <div class="FieldStyle"></div>
        <div class="ValueStyleText">
            <asp:CheckBox ID="chkOptionalInd" CssClass="HintStyle" runat="server" Text="Check this box if this Add-On is optional. " Height="30px" />
        </div>
        
        <h4 class="SubTitle">
            Inventory Settings</h4>
        <div class="FieldStyle">
            Out of Stock Options</div>
        <div class="ValueStyle">
            <asp:RadioButtonList ID="InvSettingRadiobtnList" runat="server">
                <asp:ListItem Selected="True" Value="1">Only Sell if Inventory Available (User can only add to cart if inventory is above 0)</asp:ListItem>
                <asp:ListItem Value="2">Allow Back Order (Items can always be added to the cart. Inventory is reduced)</asp:ListItem>
                <asp:ListItem Value="3">Don't Track Inventory (Items can always be added to the cart and inventory is not reduced)</asp:ListItem>
            </asp:RadioButtonList>
        </div>
        <div class="FieldStyle">
            In Stock Message<br />
            <small>Displayed on the catalog when items are in stock</small></div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtInStockMsg" runat="server"></asp:TextBox></div>
        <div class="FieldStyle">
            Out of Stock Message<br />
            <small>Displayed if this product is out of stock</small></div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtOutofStock" runat="server">Out of Stock</asp:TextBox></div>
        <div class="FieldStyle">
            Back Order Message<br />
            <small>Displayed if an item is on back order</small></div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtBackOrderMsg" runat="server"></asp:TextBox></div>
        
        <div class="ClearBoth">
        </div>
            
        <div>
            <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" ValidationGroup="grpAddOn" />
            <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>
