using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Inventory.ReferenceTypes.AddOnTypes
{
    /// <summary>
    /// Represents the SiteAdmin Admin_Secure_catalog_product_addons_add_Addonvalues class
    /// </summary>
    public partial class AddValues : System.Web.UI.Page
    {
        #region Protected Variables
        private int ItemId;
        private int AddOnValueId = 0;
        private string ViewLink = "~/SiteAdmin/Secure/Inventory/ReferenceTypes/AddOnTypes/view.aspx?itemid=";
        private string CancelLink = "~/SiteAdmin/Secure/Inventory/ReferenceTypes/AddOnTypes/view.aspx?itemid=";
        #endregion

        #region Page Load Event

        /// <summary>
        /// Page Load Event - fires while page is loading
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (Request.Params["AddOnValueId"] != null)
            {
                this.AddOnValueId = int.Parse(Request.Params["AddOnValueId"]);
            }

            if (!Page.IsPostBack)
            {
                this.BindTaxClasses();
               // this.BindBillingFrequency();
                this.BindShippingTypes();
                this.BindSuppliersTypes();

                // If edit func then bind the data fields
                if (this.AddOnValueId > 0)
                {
                    lblTitle.Text = "Edit Add-On Value : ";
                    this.BindEditData();
                }
                else
                {
                    lblTitle.Text = "Add Add-On Value";
                }

                // Bind Locale dropdown for the this AddOnValue.
                this.BindLocaleDropdown();
            }
        }

        #endregion

        #region General Events

        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddOnValueSubmit_Click(object sender, EventArgs e)
        {
            bool status = this.UpdateAddOnValue();

            if (status)
            {
                Response.Redirect(this.ViewLink + this.ItemId);
            }
            else
            {
                lblAddonValueMsg.Text = "Could not update the Add-On Value. Please try again.";
                return;
            }
        }

        /// <summary>
        /// Cancel Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.CancelLink + this.ItemId);
        }
        
        /// <summary>
        /// Shipping Type List Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ShippingTypeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.EnableValidators();
        }

        /// <summary>
        /// Recurring Billing Ind Checked Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ChkRecurringBillingInd_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRecurringBillingInd.Checked)
            {
                pnlRecurringBilling.Visible = true;
            }
            else
            {
                pnlRecurringBilling.Visible = false;
            }
        }

        /// <summary>
        /// Billing Period Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlBillingPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {
           // this.BindBillingFrequency();
        }

        /// <summary>
        /// Redirect to Locale based AddOnValue edit page.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlLocales_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool status = this.UpdateAddOnValue();

            if (status)
            {
                int id = 0;
                int.TryParse(ddlLocales.SelectedValue, out id);

                // Get the AddOnId for the selected AddOnValueId.
                ZNode.Libraries.DataAccess.Service.AddOnValueService aovs = new ZNode.Libraries.DataAccess.Service.AddOnValueService();
                AddOnValue aov = aovs.GetByAddOnValueID(id);
                if (aov != null)
                {
                    // Redirect, if user select other language of same add on.
                    Response.Redirect(string.Concat(Request.Path, "?itemId=", aov.AddOnID, "&AddOnValueId=", id));
                }
            }
            else
            {
                lblAddonValueMsg.Text = "Could not update the Add-On Value. Please try again.";
                return;
            }
        }

        /// <summary>
        /// Update the input fields to AddOnValue object/database.
        /// </summary>
        /// <returns>Returns bool value true or false</returns>
        private bool UpdateAddOnValue()
        {
            try
            {
                ProductAddOnAdmin AddOnValueAdmin = new ProductAddOnAdmin();
                AddOnValue addOnValue = new AddOnValue();

                if (this.AddOnValueId > 0)
                {
                    addOnValue = AddOnValueAdmin.GetByAddOnValueID(this.AddOnValueId);
                }

                // Set Properties
                // General Settings
                // AddOnId from querystring 
                addOnValue.AddOnID = this.ItemId; 
                addOnValue.Name = Server.HtmlEncode(txtAddOnValueName.Text.Trim());
                addOnValue.Description = Server.HtmlEncode(txtDescription.Text.Trim());
                addOnValue.RetailPrice = decimal.Parse(txtAddOnValueRetailPrice.Text.Trim());
                addOnValue.SalePrice = null;
                addOnValue.WholesalePrice = null;

                if (txtSalePrice.Text.Trim().Length > 0)
                {
                    addOnValue.SalePrice = decimal.Parse(txtSalePrice.Text.Trim());
                }

                if (txtWholeSalePrice.Text.Trim().Length > 0)
                {
                    addOnValue.WholesalePrice = decimal.Parse(txtWholeSalePrice.Text.Trim());
                }

                // Display Settings
                addOnValue.DisplayOrder = int.Parse(txtAddonValueDispOrder.Text.Trim());
                addOnValue.DefaultInd = chkIsDefault.Checked;

                // Inventory Settings
                addOnValue.SKU = txtAddOnValueSKU.Text.Trim();

                int qty = 0, reorder = 0;
                int.TryParse(txtAddOnValueQuantity.Text.Trim(), out qty);
                int.TryParse(txtReOrder.Text.Trim(), out reorder);
                ProductAdmin.UpdateQuantity(addOnValue, qty, reorder);

                if (txtAddOnValueWeight.Text.Trim().Length > 0)
                {
                    addOnValue.Weight = decimal.Parse(txtAddOnValueWeight.Text.Trim());
                }
               
                if (Height.Text.Trim().Length > 0)
                {
                    addOnValue.Height = decimal.Parse(Height.Text.Trim());
                }
                else 
                { 
                    addOnValue.Height = null; 
                }

                if (Width.Text.Trim().Length > 0)
                {
                    addOnValue.Width = decimal.Parse(Width.Text.Trim());
                }
                else 
                { 
                    addOnValue.Width = null; 
                }

                if (Length.Text.Trim().Length > 0)
                {
                    addOnValue.Length = decimal.Parse(Length.Text.Trim());
                }
                else
                { 
                    addOnValue.Length = null; 
                }

                // Set shipping type for this add-on item
                addOnValue.ShippingRuleTypeID = int.Parse(ShippingTypeList.SelectedValue);
                addOnValue.FreeShippingInd = chkFreeShippingInd.Checked;

                if (chkRecurringBillingInd.Checked)
                {
                    // Recurring Billing
                    addOnValue.RecurringBillingInd = chkRecurringBillingInd.Checked;
                    addOnValue.RecurringBillingInitialAmount = Convert.ToDecimal(txtRecurringBillingInitialAmount.Text);
                    addOnValue.RecurringBillingPeriod = ddlBillingPeriods.SelectedItem.Value;
                    addOnValue.RecurringBillingFrequency = "1";
                    addOnValue.RecurringBillingTotalCycles = 0;
                }
                else
                {
                    addOnValue.RecurringBillingInd = false;
                    addOnValue.RecurringBillingInstallmentInd = false;
                    addOnValue.RecurringBillingInitialAmount = null;
                }

                // Supplier
                if (ddlSupplier.SelectedIndex != -1)
                {
                    if (ddlSupplier.SelectedItem.Text.Equals("None"))
                    {
                        addOnValue.SupplierID = null;
                    }
                    else
                    {
                        addOnValue.SupplierID = Convert.ToInt32(ddlSupplier.SelectedValue);
                    }
                }

                // Tax Class
                if (ddlTaxClass.SelectedIndex != -1)
                {
                    addOnValue.TaxClassID = int.Parse(ddlTaxClass.SelectedValue);
                }

                bool status = false;

                if (this.AddOnValueId > 0)
                {
                    // Set update date
                    addOnValue.UpdateDte = System.DateTime.Now;

                    // Update option values
                    status = AddOnValueAdmin.UpdateAddOnValue(addOnValue);
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit of Addon Values - " + txtAddOnValueName.Text, txtAddOnValueName.Text);
                }
                else
                {
                    // Add new option values
                    status = AddOnValueAdmin.AddNewAddOnValue(addOnValue);
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Creation of Addon Values - " + txtAddOnValueName.Text, txtAddOnValueName.Text);
                }

                return status;
            }
            catch (Exception e)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(e.ToString());
            }

            return false;
        }

        #endregion

        #region Bind Methods

        /// <summary>
        /// Bind Shipping option list
        /// </summary>
        private void BindShippingTypes()
        {
            // Bind ShippingRuleTypes
            ShippingAdmin shippingAdmin = new ShippingAdmin();
            ShippingTypeList.DataSource = shippingAdmin.GetShippingRuleTypes();
            ShippingTypeList.DataTextField = "description";
            ShippingTypeList.DataValueField = "shippingruletypeid";
            ShippingTypeList.DataBind();
        }

        /// <summary>
        /// Bind tax classes list
        /// </summary>
        private void BindTaxClasses()
        {
            // Bind Tax Class
            TaxRuleAdmin TaxRuleAdmin = new TaxRuleAdmin();
            TList<TaxClass> taxClass = TaxRuleAdmin.GetAllTaxClass();
            taxClass.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.TaxClass tax) { return tax.ActiveInd == true; });
            ddlTaxClass.DataSource = taxClass;
            ddlTaxClass.DataTextField = "name";
            ddlTaxClass.DataValueField = "TaxClassID";
            ddlTaxClass.DataBind();
        }

        /// <summary>
        /// Bind supplier option list
        /// </summary>
        private void BindSuppliersTypes()
        {
            // Bind Supplier
            ZNode.Libraries.DataAccess.Service.SupplierService serv = new ZNode.Libraries.DataAccess.Service.SupplierService();
            TList<ZNode.Libraries.DataAccess.Entities.Supplier> list = serv.GetAll();
            list.Sort("DisplayOrder Asc");
            list.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.Supplier supplier)
            {
               return supplier.ActiveInd == true;
            });
            
            DataSet ds = list.ToDataSet(false);
            DataView dv = new DataView(ds.Tables[0]);
            ddlSupplier.DataSource = dv;
            ddlSupplier.DataTextField = "name";
            ddlSupplier.DataValueField = "supplierid";
            ddlSupplier.DataBind();
            ListItem li1 = new ListItem("None", "0");
            ddlSupplier.Items.Insert(0, li1);
        }

        /// <summary>
        /// Bind data to the fields on the edit screen
        /// </summary>
        private void BindEditData()
        {
            ProductAddOnAdmin AddOnAdmin = new ProductAddOnAdmin();
            AddOnValue AddOnValueEntity = AddOnAdmin.DeepLoadByAddOnValueID(this.AddOnValueId);

            if (AddOnValueEntity != null)
            {
                // General Settings
                lblTitle.Text += AddOnValueEntity.Name;
                txtAddOnValueName.Text = Server.HtmlDecode(AddOnValueEntity.Name);
                txtDescription.Text = Server.HtmlEncode(AddOnValueEntity.Description);
                txtAddOnValueRetailPrice.Text = AddOnValueEntity.RetailPrice.ToString("N");
                if (AddOnValueEntity.SalePrice.HasValue)
                {
                    txtSalePrice.Text = AddOnValueEntity.SalePrice.Value.ToString("N");
                }

                if (AddOnValueEntity.WholesalePrice.HasValue)
                {
                    txtWholeSalePrice.Text = AddOnValueEntity.WholesalePrice.Value.ToString("N");
                }

                if (ddlSupplier.SelectedIndex != -1)
                {
                    ddlSupplier.SelectedValue = AddOnValueEntity.SupplierID.ToString();
                }

                if (ddlTaxClass.SelectedIndex != -1)
                {
                    ddlTaxClass.SelectedValue = AddOnValueEntity.TaxClassID.GetValueOrDefault(0).ToString();
                }

                // Display Settings
                txtAddonValueDispOrder.Text = AddOnValueEntity.DisplayOrder.ToString();
                chkIsDefault.Checked = AddOnValueEntity.DefaultInd;

                // Inventory Settings
                txtAddOnValueSKU.Text = AddOnValueEntity.SKU;

                // Load the inventory record and assign to text boxes.
                SKUInventory avi = ProductAdmin.GetAddOnInventory(AddOnValueEntity);

                if (avi != null)
                {
                    txtAddOnValueQuantity.Text = avi.QuantityOnHand.ToString();

                    if (avi.ReOrderLevel.HasValue)
                    {
                        txtReOrder.Text = avi.ReOrderLevel.ToString();
                    }
                }

                txtAddOnValueWeight.Text = AddOnValueEntity.Weight.ToString();
                if (AddOnValueEntity.Height.HasValue)
                {
                    Height.Text = AddOnValueEntity.Height.Value.ToString("N2");
                }

                if (AddOnValueEntity.Width.HasValue)
                {
                    Width.Text = AddOnValueEntity.Width.Value.ToString("N2");
                }

                if (AddOnValueEntity.Length.HasValue)
                {
                    Length.Text = AddOnValueEntity.Length.Value.ToString("N2");
                }

                // Shipping Settings
                if (AddOnValueEntity.ShippingRuleTypeID.HasValue)
                {
                    ShippingTypeList.SelectedValue = AddOnValueEntity.ShippingRuleTypeID.Value.ToString();
                }

                this.EnableValidators();

                if (AddOnValueEntity.FreeShippingInd.HasValue)
                {
                    chkFreeShippingInd.Checked = AddOnValueEntity.FreeShippingInd.Value;
                }

                // Recurring Billing
                pnlRecurringBilling.Visible = AddOnValueEntity.RecurringBillingInd;
                chkRecurringBillingInd.Checked = AddOnValueEntity.RecurringBillingInd;
                if (AddOnValueEntity.RecurringBillingInitialAmount.HasValue)
                    txtRecurringBillingInitialAmount.Text = AddOnValueEntity.RecurringBillingInitialAmount.Value.ToString("N"); 
                               ddlBillingPeriods.SelectedValue = AddOnValueEntity.RecurringBillingPeriod;
                
                if (AddOnValueEntity.AddOnIDSource != null && string.Compare(AddOnValueEntity.AddOnIDSource.DisplayType, "TextBox", true) == 0)
                {
                    pnlAddOnTextBox.Visible = true;
                }
            }
            else
            {
                throw new ApplicationException("Add-On Value Requested could not be found.");
            }
        }

        /// <summary>
        /// Bind the locale dropdown, if this add on have different locale.
        /// </summary>
        private void BindLocaleDropdown()
        {
            // Get the Locale information with product id.
            DataSet datasetLocale = ProductAddOnAdmin.GetLocaleIdsByAddOnValueId(this.AddOnValueId);

            // Bind the dropdown.
            ddlLocales.DataSource = datasetLocale.Tables[0];
            ddlLocales.DataTextField = "LocaleDescription";
            ddlLocales.DataValueField = "AddOnValueId";
            ddlLocales.DataBind();

            if (ddlLocales.Items.Count < 2)
            {
                LocalePanel.Visible = false;
            }
            else
            {
                // Select the current language.
                ddlLocales.SelectedValue = this.AddOnValueId.ToString();
            }
        }

        /// <summary>
        /// Enable Validators Method
        /// </summary>
        private void EnableValidators()
        {
            if (ShippingTypeList.SelectedValue == "2")
            {
                weightBasedRangeValidator.Enabled = true;
                RequiredForWeightBasedoption.Enabled = true;
            }
            else
            {
                weightBasedRangeValidator.Enabled = false;
                RequiredForWeightBasedoption.Enabled = false;
            }
        }  
        #endregion
    }
}