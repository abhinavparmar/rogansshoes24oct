<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" 
    Inherits="SiteAdmin.Secure.Inventory.ReferenceTypes.AddOnTypes.AddValues" Codebehind="AddValues.aspx.cs" Title="AddOn Value" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
            <uc2:DemoMode ID="DemoMode1" runat="server" />
            <div>
                <uc1:Spacer ID="Spacer3" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
            <div>
                <asp:Label ID="lblAddonValueMsg" CssClass="Error" EnableViewState="false" runat="server"></asp:Label></div>
        </div>
        <div style="text-align: right">
            <asp:ImageButton ID="btnSubmitTop" ValidationGroup="grpAddOnValue" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnAddOnValueSubmit_Click" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
        <div class="ClearBoth" align="left">
            <div class="SubTitle"  runat="server" id="LocalePanel" >
                Associated Locales &nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList runat="server" ID="ddlLocales"
                    AutoPostBack="true" OnSelectedIndexChanged="DdlLocales_SelectedIndexChanged">
                    <asp:ListItem>Select Language</asp:ListItem>
                </asp:DropDownList>
            </div>          
        </div><br />
        <h4 class="SubTitle">
            General Settings</h4>
        <div class="FieldStyle">
            Label<span class="Asterix">*</span><br />
            <small>Enter the label for this option value (Ex : "Fire engine red").</small></div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtAddOnValueName" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                ValidationGroup="grpAddOnValue" ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAddOnValueName"
                ErrorMessage="Enter Label" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator></div>

       <asp:Panel ID="pnlAddOnTextBox" runat="server" Visible="false">
           <div class="FieldStyle">
                Description<span class="Asterix">*</span><br />
                <small>Enter the description for this option value.</small></div>
           <div class="ValueStyle">
                <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                    ValidationGroup="grpAddOnValue" runat="server" ControlToValidate="txtDescription"
                    ErrorMessage="Enter Description" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator></div>
        </asp:Panel>
        
        <div class="FieldStyle">
            Retail Price<span class="Asterix">*</span><br />
            <small>Enter the retail price for this product Add-On.</small></div>
        <div class="ValueStyle">
            <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;<asp:TextBox
                Text="0" ID="txtAddOnValueRetailPrice" runat="server" MaxLength="7"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="grpAddOnValue"
                runat="server" ErrorMessage="Enter a Retail Price for this Product" ControlToValidate="txtAddOnValueRetailPrice"
                CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator1" ValidationGroup="grpAddOnValue" runat="server"
                ControlToValidate="txtAddOnValueRetailPrice" Type="Currency" Operator="DataTypeCheck"
                ErrorMessage="You must enter a valid Retail Price (ex: 123.45)" CssClass="Error"
                Display="Dynamic" />
            <asp:RangeValidator ID="RangeValidator1" ValidationGroup="grpAddOnValue" runat="server"
                ControlToValidate="txtAddOnValueRetailPrice" ErrorMessage="Enter a price between 0-999999"
                MaximumValue="999999" MinimumValue="-999999" Type="Currency" Display="Dynamic"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
            Sale Price<br />
            <small>Enter the sale price for this product Add-On.</small></div>
        <div class="ValueStyle">
            <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;<asp:TextBox
                ID="txtSalePrice" runat="server" MaxLength="7"></asp:TextBox>
            <asp:RangeValidator CssClass="Error" ID="RangeValidator3" ValidationGroup="grpAddOnValue"
                runat="server" ControlToValidate="txtSalePrice" ErrorMessage="Enter a sale price between 0-999999"
                MaximumValue="999999" MinimumValue="-999999" Type="Currency" Display="Dynamic"></asp:RangeValidator></div>
        <div class="FieldStyle">
            Wholesale Price<br />
            <small>Enter the wholesale price for this product Add-On.</small></div>
        <div class="ValueStyle">
            <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;<asp:TextBox
                ID="txtWholeSalePrice" runat="server" MaxLength="7"></asp:TextBox><asp:RangeValidator
                    CssClass="Error" ID="RangeValidator4" ValidationGroup="grpAddOnValue" runat="server"
                    ControlToValidate="txtWholeSalePrice" ErrorMessage="Enter a wholesaleprice between 0-999999"
                    MaximumValue="999999" MinimumValue="-999999" Type="Currency" Display="Dynamic"></asp:RangeValidator></div>
        <div class="FieldStyle">
            Select Supplier<br />
            <small>Select where you get this product Add-On from.</small></div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlSupplier" runat="server" />
        </div>
        <div class="FieldStyle">
            Tax Class<br />
            <small>Select tax class for this product Add-on.</small></div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlTaxClass" runat="server" />
        </div>
        <h4 class="SubTitle">
            Display Settings</h4>
        <div class="FieldStyle">
            Display Order<span class="Asterix">*</span><br />
            <small>Enter a number. Items with a lower number are displayed first on the page.</small></div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtAddonValueDispOrder" runat="server" MaxLength="9" Columns="5"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                ValidationGroup="grpAddOnValue" CssClass="Error" ErrorMessage="* Enter a Display Order" ControlToValidate="txtAddonValueDispOrder"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="txtAddonValueDispOrder"
                Display="Dynamic" ValidationGroup="grpAddOnValue" ErrorMessage="Enter a whole number."
                MaximumValue="999999999" MinimumValue="1" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle"></div>
        <div class="ValueStyle">
            <asp:CheckBox ID="chkIsDefault" runat="server" Text="" />
            Check here to make this the default selected item in the list.<br />
        </div>
        
        <h4 class="SubTitle">
            Inventory Settings</h4>
        <div class="FieldStyle">
            SKU or Part#<span class="Asterix">*</span></div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtAddOnValueSKU" runat="server" MaxLength="100"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtAddOnValueSKU"
                CssClass="Error" ValidationGroup="grpAddOnValue" Display="Dynamic" ErrorMessage="Enter a Valid SKU or Part#"></asp:RequiredFieldValidator></div>
        <div class="FieldStyle">
            Quantity On Hand<span class="Asterix">*</span></div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtAddOnValueQuantity" runat="server" Rows="3">9999</asp:TextBox>
            <asp:RangeValidator ValidationGroup="grpAddOnValue" ID="RangeValidator2" runat="server"
                ControlToValidate="txtAddOnValueQuantity" CssClass="Error" Display="Dynamic"
                ErrorMessage="Enter a number between 0-999999" MaximumValue="999999" MinimumValue="0"
                SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
            <asp:RequiredFieldValidator ValidationGroup="grpAddOnValue" ID="RequiredFieldValidator6"
                runat="server" ControlToValidate="txtAddOnValueQuantity" CssClass="Error" Display="Dynamic"
                ErrorMessage="You Must Enter a Valid Quantity"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            Re-Order Level</div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtReOrder" runat="server"></asp:TextBox>
            <asp:RangeValidator ValidationGroup="grpAddOnValue" ID="RangeValidator5" runat="server"
                ControlToValidate="txtReOrder" CssClass="Error" Display="Dynamic" ErrorMessage="Enter a number between 0-999999"
                MaximumValue="999999" MinimumValue="0" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
        </div>
        <h4 class="SubTitle">
            Shipping Settings</h4>
        <div class="FieldStyle">
            Free Shipping<br />
            <small>This applies only for custom shipping options</small></div>
        <div class="ValueStyle">
            <asp:CheckBox ID="chkFreeShippingInd" Text="Check this box to enable free shipping for this product. All other shipping rules will be ignored."
                runat="server" /></div>
        <div class="FieldStyle">
            Select Shipping Type<span class="Asterix">*</span><br />
            <small>This setting determines the shipping rules that will be applied to this product.
                For ex: if you select "Flat Rate" then shipping will be calculated based on a flat
                rate per item.</small></div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ShippingTypeList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ShippingTypeList_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
        <div class="FieldStyle">
            Item Weight<br />
            <small>Leave blank if this does not apply. Note that the weight can be used to determine
                shipping cost.</small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtAddOnValueWeight" runat="server" Width="46px"></asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.WeightUnit %>
            <asp:RangeValidator Enabled="false" ID="weightBasedRangeValidator" runat="server"
                ControlToValidate="txtAddOnValueWeight" CssClass="Error" Display="Dynamic" ErrorMessage="This Shipping Type requires that Weight be greater than 0."
                MaximumValue="9999999" MinimumValue="0.1" CultureInvariantValues="true" Type="Double"
                ValidationGroup="grpAddOnValue"></asp:RangeValidator>
            <asp:RequiredFieldValidator Enabled="false" ID="RequiredForWeightBasedoption" runat="server"
                ErrorMessage="You must enter weight for this Shipping Type." ControlToValidate="txtAddOnValueWeight"
                CssClass="Error" Display="Dynamic" ValidationGroup="grpAddOnValue"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator3" ValidationGroup="grpAddOnValue" runat="server"
                ControlToValidate="txtAddOnValueWeight" Type="Currency" Operator="DataTypeCheck"
                ErrorMessage="You must enter a valid Weight(ex: 2.5)" CssClass="Error" Display="Dynamic" /></div>
        <div class="FieldStyle">
            Height<br />
            <small>Leave blank if this does not apply. Note that the height can be used to determine
                shipping cost.</small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="Height" runat="server" Width="46px"></asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.DimensionUnit %>
            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="Height"
                Type="Currency" Operator="DataTypeCheck" ErrorMessage="You must enter a valid Add-onvalue height(ex: 2.5)"
                CssClass="Error" Display="Dynamic" /></div>
        <div class="FieldStyle">
            Width<br />
            <small>Leave blank if this does not apply. Note that the width can be used to determine
                shipping cost.</small></div>
        <div>
            <div class="ValueStyle">
                <asp:TextBox ID="Width" runat="server" Width="46px"></asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.DimensionUnit %>
                <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="Width"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage="You must enter a valid Add-onvalue width(ex: 2.5)"
                    CssClass="Error" Display="Dynamic" /></div>
            <div class="FieldStyle">
                Length<br />
                <small>Leave blank if this does not apply. Note that the length can be used to determine
                    shipping cost.</small></div>
            <div class="ValueStyle">
                <asp:TextBox ID="Length" runat="server" Width="46px"></asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.DimensionUnit %>
                <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="Length"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage="You must enter a valid Add-onvalue length"
                    CssClass="Error" Display="Dynamic" /></div>
            <h4 class="SubTitle">
                Recurring Billing Settings</h4>
            <div class="FieldStyle">
                <small>Check this box to enable recurring billing subscription for this product add-On
                    value.</small></div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkRecurringBillingInd" AutoPostBack="true" OnCheckedChanged="ChkRecurringBillingInd_CheckedChanged"
                    runat="server" Text="Recurring Billing" /></div>
            <asp:Panel ID="pnlRecurringBilling" runat="server" Visible="false">
                <div class="FieldStyle">
                    Billing Amount<span class="Asterix">*</span><br />
                    <small>Enter amount for recurring bill.</small></div>
                <div class="ValueStyle">
                    <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;
                    <asp:TextBox ID="txtRecurringBillingInitialAmount" MaxLength="10" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"  ValidationGroup="grpAddOnValue" ErrorMessage="Enter a recurring billing amount"
                        ControlToValidate="txtRecurringBillingInitialAmount" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator><br />
                    <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="txtRecurringBillingInitialAmount"  ValidationGroup="grpAddOnValue"
                        Type="Currency" Operator="DataTypeCheck" ErrorMessage="You must enter a valid billing amount (ex: 123.45)"
                        CssClass="Error" Display="Dynamic" /><br />
                    <asp:RangeValidator ID="RangeValidator7" runat="server" ControlToValidate="txtRecurringBillingInitialAmount"  ValidationGroup="grpAddOnValue"
                        ErrorMessage="You must enter a Billing Amount value between $0 and $999,999.99"
                        MaximumValue="999999.99" Type="Currency" MinimumValue="0" Display="Dynamic" CssClass="Error"></asp:RangeValidator>
                </div>
                <div class="FieldStyle">
                    Billing Period<span class="Asterix">*</span><br />
                    <small>Select a value for billing during this subscription period.</small></div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlBillingPeriods" AutoPostBack="true" runat="server" OnSelectedIndexChanged="DdlBillingPeriods_SelectedIndexChanged">
                        <asp:ListItem Text="Day(s)" Value="DAY"></asp:ListItem>
                        <asp:ListItem Text="Weekly" Value="WEEK"></asp:ListItem>
                        <asp:ListItem Text="Monthly" Value="MONTH"></asp:ListItem>
                        <asp:ListItem Text="Yearly" Value="YEAR"></asp:ListItem>
                    </asp:DropDownList>
                </div> 
                 
            </asp:Panel>
            <div>
                <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
            <div class="ClearBoth">
            </div>
            <div>
                <asp:ImageButton ID="btnSubmitBottom" ValidationGroup="grpAddOnValue" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" OnClick="BtnAddOnValueSubmit_Click" CausesValidation="true" />
                <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
            </div>
        </div>
    </div>
</asp:Content>
