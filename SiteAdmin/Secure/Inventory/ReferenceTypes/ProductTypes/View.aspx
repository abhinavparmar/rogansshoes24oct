<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True"
    Inherits="SiteAdmin.Secure.Inventory.ReferenceTypes.ProductTypes.View" ValidateRequest="false" CodeBehind="View.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>Attributes for product type "<asp:Label ID="lblProductType" runat="server"></asp:Label>"
            </h1>
        </div>
        <div style="text-align: right" class="ImageButtons">
            <asp:Button ID="ProductTypeList" runat="server" CssClass="Size100" Text="< Back" OnClick="ProductTypeList_Click" />&nbsp;&nbsp;
            <asp:Button ID="Button1" runat="server" Text="Add Attribute" CssClass="Size100" OnClick="AddAttributeType_Click" />
        </div>
        <div class="ClearBoth"></div>
        <div>
            <asp:Label ID="lblError" CssClass="Error" runat="server"
                Text="Unable to add new attributes to a product type currently associated with products. Please remove the association from your products to add additional attributes to this product type or create a new product type." Visible="false"></asp:Label>
        </div>
        <uc1:Spacer ID="Spacer" runat="server" SpacerHeight="10" />
        <asp:GridView ID="uxGrid" runat="server" AllowPaging="True" AutoGenerateColumns="False"
            CaptionAlign="Left" CellPadding="4" CssClass="Grid" EmptyDataText="No Attributes exist in the database."
            GridLines="None" Width="100%" OnPageIndexChanging="UxGrid_PageIndexChanging"
            OnRowCommand="UxGrid_RowCommand" OnRowDeleting="UxGrid_RowDeleting">
            <Columns>
                <asp:BoundField DataField="ProductAttributeTypeID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText="Attribute" HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="DisplayOrder" HeaderText="Display Order" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="DeleteAttribute" CommandName="Delete" CssClass="actionlink" CommandArgument='<%# Eval("ProductAttributeTypeID") %>'
                            runat="server" Text="Delete &raquo" Width="100px" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle CssClass="RowStyle" />
            <EditRowStyle CssClass="EditRowStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
        </asp:GridView>
        <uc1:Spacer ID="Spacer5" SpacerHeight="100" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
</asp:Content>
