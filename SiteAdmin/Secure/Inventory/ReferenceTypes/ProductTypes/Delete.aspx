<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" 
    Inherits="SiteAdmin.Secure.Inventory.ReferenceTypes.ProductTypes.Delete" Title="Product Types - Delete" Codebehind="Delete.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <h5>
        Please Confirm<uc1:DemoMode ID="DemoMode1" runat="server"></uc1:DemoMode>
    </h5>
    <p>
        Please confirm if you want to delete the Product Type titled <b>"<%=ProductCategoryName%>"</b>.
        Note that this will also delete all the products of this Type. This change cannot
        be undone.</p>
    <p>
        <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label></p>
    <div>
        <asp:ImageButton ID="btnDelete" onmouseover="this.src='../../../../Themes/Images/buttons/button_delete_highlight.gif';"
            onmouseout="this.src='../../../../Themes/Images/buttons/button_delete.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_delete.gif"
            runat="server" AlternateText="Delete" OnClick="BtnDelete_Click" CausesValidation="true" />
        <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
            onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
            runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
    </div>
    <br />
    <br />
    <br />
</asp:Content>
