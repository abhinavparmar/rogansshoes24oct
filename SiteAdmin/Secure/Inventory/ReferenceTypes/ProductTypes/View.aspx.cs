using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Inventory.ReferenceTypes.ProductTypes
{
    /// <summary>
    /// Represents the SiteAdmin.Secure.Setup.ReferenceTypes.ProductTypes.View class
    /// </summary>
    public partial class View : System.Web.UI.Page
    {
        #region Private Member Variables
        private int ItemId = 0;
        private string AddAttributetypeLink = "~/SiteAdmin/Secure/Inventory/ReferenceTypes/ProductTypes/AssociateType.aspx?itemid=";
        private string ListLink = "~/SiteAdmin/Secure/Inventory/ReferenceTypes/ProductTypes/default.aspx";
        private string EditLink = "~/SiteAdmin/Secure/Inventory/ReferenceTypes/ProductTypes/add.aspx?itemid=";
        private string AssociateName = string.Empty;
        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }

            if (!Page.IsPostBack)
            { 
                ProductTypeAdmin _AdminAccess = new ProductTypeAdmin();
                ProductType _ProductTypeList = _AdminAccess.GetByProdTypeId(this.ItemId);
                if (_ProductTypeList.IsGiftCard == true)
                {
                    Response.Redirect(this.ListLink);
                }

                ProductTypeHelper _producttypeHelper = new ProductTypeHelper();
                if (_producttypeHelper.GetProductsByProductTypeId(ItemId) > 0)
                {
                    lblError.Visible = true;
                    Button1.Enabled = false;
                }
                else
                {
                    lblError.Visible = false;
                    Button1.Enabled = true;
                }
          
                this.BindData();
                this.BindGrid();
            }
        }
        
        #endregion

        #region General Events

        /// <summary>
        /// ProductTypeList click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ProductTypeList_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ListLink);
        }

        /// <summary>
        /// Edit Product Type Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void EditProductType_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.EditLink + this.ItemId);
        }

        /// <summary>
        /// Add Attribute Type Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddAttributeType_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddAttributetypeLink + this.ItemId);
        }
        #endregion

        #region Grid Events

        /// <summary>
        /// Grid Row Deleting Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindGrid();
        }

        /// <summary>
        /// Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGrid();
        }

        /// <summary>
        /// Grid Row Comman Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "page")
            {
            }
            else
            {
                // Get the Value from the command argument
                string Id = e.CommandArgument.ToString();

                if (e.CommandName == "Delete")
                {
                    AttributeTypeAdmin attributeTypeAdmin = new AttributeTypeAdmin();
                    ProductTypeAttribute prodTypeAttribute = new ProductTypeAttribute();
                    prodTypeAttribute.ProductAttributeTypeID = int.Parse(Id);

                    // Get Product Type Name
                    ProductTypeAttributeService service = new ProductTypeAttributeService();
                    ProductTypeAttribute pattribute = service.GetByProductAttributeTypeID(int.Parse(Id));
                    service.DeepLoad(pattribute);
                    string productTypeName = pattribute.ProductTypeIdSource.Name;

                    // Get Attribute Type Name
                    AttributeType attributeType = attributeTypeAdmin.GetByAttributeTypeId(pattribute.AttributeTypeId);
                    string attributeTypeName = attributeType.Name;

                    this.AssociateName = "Delete Association between " + attributeTypeName + " and " + productTypeName;
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, productTypeName);

                    bool Check = false;
                    Check = attributeTypeAdmin.DeleteProductTypeAttribute(prodTypeAttribute);
                }
            }
        }

        #endregion

        #region Bind Datas

        /// <summary>
        /// Bind Data Method
        /// </summary>
        private void BindData()
        {
            ProductTypeAdmin adminAccess = new ProductTypeAdmin();

            ProductType productTypeList = adminAccess.GetByProdTypeId(this.ItemId);

            if (productTypeList != null)
            {
                lblProductType.Text = productTypeList.Name;
            }
        }

        /// <summary>
        /// Binds the Grid
        /// </summary>
        private void BindGrid()
        {
            AttributeTypeHelper attributeAccess = new AttributeTypeHelper();

            DataSet ds = attributeAccess.GetAttributeTypesByProductType(this.ItemId);
            DataView dv = new DataView(ds.Tables[0]);
            dv.Sort = "AttributeTypeId DESC";
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }

        #endregion
    }
}