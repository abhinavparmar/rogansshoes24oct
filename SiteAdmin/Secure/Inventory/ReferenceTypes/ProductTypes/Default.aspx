<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master"
    AutoEventWireup="True" Inherits="SiteAdmin.Secure.Inventory.ReferenceTypes.ProductTypes.Default"
    Title="Manage Product Types" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>Product Types
            </h1>
        </div>
        <div class="ButtonStyle">
            <zn:LinkButton ID="btnAddProductType" runat="server" CausesValidation="False"
                ButtonType="Button" OnClick="BtnAddProductType_Click" Text="Add New Product Type"
                ButtonPriority="Primary" />
        </div>
        <div class="ClearBoth">
            <p>
                Product types are used to group products with similar characteristics and assign special behavior for those products. For example, you could automatically assign "color" and "size" attributes for product type of "apparel".
            </p>
            <br />
        </div>
        <h4 class="SubTitle">Search Product Types</h4>
        <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">Product Type</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtproductType" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">Description</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox></span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_clear_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                        runat="server" AlternateText="Clear" OnClick="BtnClear_Click" />
                    <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                        runat="server" AlternateText="Search" OnClick="BtnSearch_Click" />
                </div>
            </div>
        </asp:Panel>
        <br />
        <h4 class="GridTitle">Product Type List</h4>
        <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label><asp:GridView
            ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
            CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" OnSorting="UxGrid_Sorting"
            Width="100%" PageSize="25" OnRowDeleting="UxGrid_RowDeleting" AllowSorting="False"
            EmptyDataText="No Product Type exist in the database." OnRowDataBound="uxGrid_RowDataBound">
            <Columns>
                <asp:BoundField DataField="producttypeid" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText="Product Type" SortExpression="Name" HeaderStyle-HorizontalAlign="Left"
                    ItemStyle-Width="30%">
                    <ItemTemplate>
                        <asp:Label ID="lblProductTypeName" runat="server" Text='<%# GetProductTypeEditHyperlink(DataBinder.Eval(Container.DataItem, "Name"), DataBinder.Eval(Container.DataItem, "ProductTypeId"),
                        DataBinder.Eval(Container.DataItem, "IsGiftCard") )%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Description" HtmlEncode="false" HeaderText="Description"
                    HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="DisplayOrder" HeaderText="Display Order" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="btnView" runat="server" CssClass="actionlink" CommandName="View"
                            Text="Attributes &raquo" CommandArgument='<%# Eval("producttypeid") %>' Enabled='<%# (Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsGiftCard"))==true?false:true) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="btnEdit" runat="server" CssClass="actionlink" CommandName="Edit"
                            Text="Edit &raquo" CommandArgument='<%# Eval("producttypeid") %>' Enabled='<%# (Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsGiftCard"))==true?false:true) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="btnDelete" runat="server" CssClass="actionlink" CommandName="Delete"
                            Text="Delete &raquo" CommandArgument='<%# Eval("producttypeid") %>' Enabled='<%# (Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsGiftCard"))==true?false:true) %>' />
                        <asp:Label ID="lblGiftCard" Visible="false" runat="server" Text='<%# Eval("IsGiftCard") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        </div>
    </div>
</asp:Content>
