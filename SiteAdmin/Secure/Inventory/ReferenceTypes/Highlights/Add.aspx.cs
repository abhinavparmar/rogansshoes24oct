using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;

namespace SiteAdmin.Secure.Inventory.ReferenceTypes.Highlights
{
    /// <summary>
    /// Represents the SiteAdmin.Secure.Setup.ReferenceTypes.Highlights - Add class
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId;
        private int ProductId;
        private string ListPageLink = "~/SiteAdmin/Secure/Inventory/ReferenceTypes/Highlights/Default.aspx";
        private string ProductListPageLink = "~/SiteAdmin/Secure/Inventory/Products/View.aspx?";
        #endregion

        #region Public Methods
        /// <summary>
        /// Enable Panel 
        /// </summary>
        public void EnablePanel()
        {
            if (this.ItemId > 0)
            {
                if (rdbHyperlinkExternal.Checked)
                {
                    rdbEnableHyperlink.Checked = false;
                    pnlHyperLinkExternal.Visible = rdbHyperlinkExternal.Checked;
                    pnlHyperLinkInternal.Visible = rdbEnableHyperlink.Checked;
                }
                else
                {
                    pnlHyperLinkInternal.Visible = rdbEnableHyperlink.Checked;
                    pnlHyperLinkExternal.Visible = rdbHyperlinkExternal.Checked;
                }
            }
            else
            {
                rdbHyperlinkExternal.Checked = false;
                //rdbHyperlinkExternal.Visible = rdbEnableHyperlink.Checked;
                pnlHyperLinkExternal.Visible = rdbHyperlinkExternal.Checked && rdbEnableHyperlink.Checked;
                pnlHyperLinkInternal.Visible = !rdbHyperlinkExternal.Checked && rdbEnableHyperlink.Checked;
            }
        }

        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get Highlight Id 
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (Request.Params["productid"] != null)
            {
                this.ProductId = int.Parse(Request.Params["productid"].ToString());
            }

            if (!Page.IsPostBack)
            {
                // Bind the language dropdown.
                this.BindLocaleDropdown();

                this.BindHighLightType();

                if (this.ItemId > 0)
                {
                    lblHeading.Text = "Edit Product Highlight";
                    this.Bind();
                    this.EnablePanel();
                    textadd.Visible = false;
                    textedit.Visible = true;
                    tblShowImage.Visible = true;
                    pnlUploadImage.Attributes["style"] += "padding-left: 10px;border-left: solid 1px #cccccc;";
                }
                else
                {
                    textadd.Visible = true;
                    textedit.Visible = false;
                    tblHighlight.Visible = true;
                    tblShowImage.Visible = true;
                    pnlShowOption.Visible = false;
                    pnlImage.Visible = false;
                    this.EnablePanel();
                }
            }
        }
        #endregion

        #region Events

        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            System.IO.FileInfo _FileInfo = null;
            HighlightAdmin AdminAccess = new HighlightAdmin();
            Highlight entity = new Highlight();

            if (this.ItemId > 0)
            {
                entity = AdminAccess.GetByHighlightID(this.ItemId);
            }

            // Set properties
            entity.Name = Server.HtmlEncode(HighlightName.Text);

            // Using Regex remove carriage return and New line feed from the description text to avoid javascript "Unterminated string" error         
            entity.Description = System.Text.RegularExpressions.Regex.Replace(Description.Html.Trim(), ">\r\n<", "><");
            entity.Hyperlink = txtHyperlink.Text.Trim();
            entity.DisplayPopup = rdbEnableHyperlink.Checked;
            entity.HighlightTypeID = Convert.ToInt32(HighlightType.SelectedValue);
            entity.DisplayOrder = Convert.ToInt32(DisplayOrder.Text);
            entity.HyperlinkNewWinInd = rdbHyperlinkExternal.Checked;
            entity.ActiveInd = VisibleInd.Checked;
            entity.ImageAltTag = txtImageAltTag.Text.Trim();
            entity.LocaleId = int.Parse(ddlLocales.SelectedValue);

            if (entity.HighlightTypeID == 1 || entity.HighlightTypeID == 2 || entity.HighlightTypeID == 4)
            {
                // Validate image
                if (RadioHighlightNoImage.Checked == false)
                {
                    if ((this.ItemId == 0) || (RadioHighlightNewImage.Checked == true))
                    {
                        if (!UploadHighlightImage.IsFileNameValid())
                        {
                            return;
                        }

                        if (UploadHighlightImage.PostedFile.FileName != string.Empty)
                        {
                            // Check for Product Image
                            _FileInfo = UploadHighlightImage.FileInformation;

                            if (_FileInfo != null)
                            {
                                entity.ImageFile = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + _FileInfo.Name;
                            }
                        }
                    }
                    else
                    {
                        entity.ImageFile = entity.ImageFile;
                    }
                }
                else
                {
                    entity.ImageFile = string.Empty;
                }
            }
            else
            {
                entity.ImageFile = string.Empty;
            }

            // Upload File if this is a new Highlight or the New Image option was selected for an existing Highlight
            if (RadioHighlightNewImage.Checked || this.ItemId == 0)
            {
                if (_FileInfo != null)
                {
                    UploadHighlightImage.SaveImage("sadmin", ZNodeConfigManager.SiteConfig.PortalID.ToString(), "0");
                    UploadHighlightImage.Dispose();
                }
                else
                {
                    entity.ImageFile = string.Empty;
                }
            }

            bool status = false;

            if (this.ItemId > 0)
            {
                status = AdminAccess.Update(entity);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit of Highlights - " + HighlightName.Text, HighlightName.Text);
            }
            else
            {
                status = AdminAccess.Add(entity);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Creation of Highlights - " + HighlightName.Text, HighlightName.Text);
            }

            if (status)
            {
                if (this.ProductId > 0)
                {
                    Response.Redirect(this.ProductListPageLink + "mode=highlight&" + "itemid=" + this.ProductId);
                }
                else
                {
                    Response.Redirect(this.ListPageLink);
                }
            }
            else
            {
                lblError.Text = string.Empty;
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (this.ProductId > 0)
            {
                Response.Redirect(this.ProductListPageLink + "mode=highlight&" + "itemid=" + this.ProductId);
            }
            else
            {
                // Redirect to View Page
                Response.Redirect(this.ListPageLink);
            }
        }

        /// <summary>
        /// Radio Highlight Current Image Checked Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioHighlightCurrentImage_CheckedChanged(object sender, EventArgs e)
        {
            tblHighlight.Visible = false;
            HighlightImage.Visible = true;
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioHighlightNoImage_CheckedChanged(object sender, EventArgs e)
        {
            tblHighlight.Visible = false;
            HighlightImage.Visible = false;
        }

        /// <summary>
        /// Radio Highlight New Image Checked Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioHighlightNewImage_CheckedChanged(object sender, EventArgs e)
        {
            tblHighlight.Visible = true;
            HighlightImage.Visible = true;
        }

        /// <summary>
        /// Display Popup Checked Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DisplayPopup_CheckedChanged(object sender, EventArgs e)
        {
            rdbHyperlinkExternal.Checked = false;
            this.EnablePanel();
        }

        /// <summary>
        /// Hyperlink Checked Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void HyperlinkChk_CheckedChanged(object sender, EventArgs e)
        {
            pnlHyperLinkExternal.Visible = rdbHyperlinkExternal.Checked;
            rdbEnableHyperlink.Checked = false;
            pnlHyperLinkInternal.Visible = !rdbHyperlinkExternal.Checked;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Bind the locale dropdown
        /// </summary>
        private void BindLocaleDropdown()
        {
            LocaleService localeService = new LocaleService();
            TList<Locale> localeList = localeService.GetAll();
            localeList.Sort("LocaleDescription");

            // Bind the dropdown.
            ddlLocales.DataSource = localeList;
            ddlLocales.DataTextField = "LocaleDescription";
            ddlLocales.DataValueField = "LocaleId";
            ddlLocales.DataBind();

            if (ddlLocales.Items.Count == 0)
            {
                lblLocale.Visible = false;
                ddlLocales.Visible = false;
            }
            ddlLocales.SelectedValue = "43";
        }

        /// <summary>
        /// Bind Edit data
        /// </summary>
        private void Bind()
        {
            HighlightAdmin AdminAccess = new HighlightAdmin();
            Highlight entity = AdminAccess.GetByHighlightID(this.ItemId);

            if (entity != null)
            {
                lblHeading.Text += " - " + entity.Name;

                if (entity.ImageFile != null && entity.ImageFile != string.Empty)
                {
                    ZNodeImage znodeImage = new ZNodeImage();
                    HighlightImage.ImageUrl = znodeImage.GetImageHttpPathMedium(entity.ImageFile);
                }
                else
                {
                    RadioHighlightNoImage.Checked = true;
                    RadioHighlightCurrentImage.Visible = false;
                }
                rdbEnableHyperlink.Checked = entity.DisplayPopup;
                HighlightName.Text = Server.HtmlDecode(entity.Name);
                Description.Html = entity.Description;
                txtHyperlink.Text = entity.Hyperlink;
                HighlightType.SelectedValue = entity.HighlightTypeID.ToString();

                DisplayOrder.Text = Convert.ToString(entity.DisplayOrder);
                rdbHyperlinkExternal.Checked = Convert.ToBoolean(entity.HyperlinkNewWinInd);
                VisibleInd.Checked = Convert.ToBoolean(entity.ActiveInd);
                txtImageAltTag.Text = entity.ImageAltTag;

                // Bind the selected locale in the dropdown.
                ListItem li = ddlLocales.Items.FindByValue(entity.LocaleId.ToString());
                if (li != null)
                {
                    li.Selected = true;
                }
            }
        }

        /// <summary>
        /// Binds Highlight Type drop-down list
        /// </summary>
        private void BindHighLightType()
        {
            ZNode.Libraries.Admin.HighlightAdmin highlight = new HighlightAdmin();
            HighlightType.DataSource = highlight.GetAllHighLightType();
            HighlightType.DataTextField = "Name";
            HighlightType.DataValueField = "HighlightTypeId";
            HighlightType.DataBind();
        }
        #endregion
    }
}