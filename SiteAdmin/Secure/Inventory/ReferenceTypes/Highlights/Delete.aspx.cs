using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Inventory.ReferenceTypes.Highlights
{
    /// <summary>
    /// Represents the SiteAdmin.Secure.Setup.ReferenceTypes.Highlights - Delete class
    /// </summary>
    public partial class Delete : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId;
        private string _HighlightName = string.Empty;
        private string CancelLink = "~/SiteAdmin/Secure/Inventory/ReferenceTypes/Highlights/Default.aspx";
        #endregion

        /// <summary>
        /// Gets or sets the highlight name
        /// </summary>
        public string HighlightName
        {
            get { return _HighlightName; }
            set { _HighlightName = value; }
        }

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring   
            if (Request.Params["Itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["Itemid"].ToString());
            }
            else
            {
                this.ItemId = 0;
            }

            if (!Page.IsPostBack)
            {
                if (this.ItemId > 0)
                {
                    HighlightAdmin AdminAccess = new HighlightAdmin();
                    Highlight entity = AdminAccess.GetByHighlightID(this.ItemId);

                    this.HighlightName = entity.Name;
                }
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Delete Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            HighlightAdmin AdminAccess = new HighlightAdmin();
            Highlight entity = AdminAccess.GetByHighlightID(this.ItemId);
            this.HighlightName = entity.Name;

            bool check = AdminAccess.Delete(this.ItemId);
            if (check)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Delete Highlights - " + this.HighlightName, this.HighlightName);

                Response.Redirect(this.CancelLink);
            }
            else
            {
                lblErrorMessage.Text = "The highlight can not be deleted until all associated items are removed. Please ensure that this highlight does not associate with the catalog product. If it does, then delete these Items first.";
            }
        }

        /// <summary>
        /// Cancel Button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.CancelLink);
        }
        #endregion
    }
}