<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Inventory.ReferenceTypes.Highlights.Add" Title="Manage Highlights - Edit" ValidateRequest="false"
    CodeBehind="Add.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="ZNode" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/StoreName.ascx" TagName="StoreName" TagPrefix="ZNode" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/ImageUploader.ascx" TagName="UploadImage" TagPrefix="ZNode" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblHeading" Text='Create Highlight' runat="server" /></h1>
        </div>

        <div class="ClearBoth">
        </div>
        <ZNode:DemoMode ID="DemoMode1" runat="server"></ZNode:DemoMode>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        </div>
        <asp:Label ID="lblError" CssClass="Error" runat="server"></asp:Label>
        <h4 class="SubTitle">General Settings</h4>
        <div class="FieldStyle">
            Name<span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="HighlightName" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                ID="RequiredFieldValidator2" runat="server" ControlToValidate="HighlightName"
                ErrorMessage="* Enter name" CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regName" runat="server" ControlToValidate="HighlightName"
                ErrorMessage="Enter valid Name" Display="Dynamic" ValidationExpression="[0-9A-Za-z\s\',.:&%#$@_-]+"
                CssClass="Error"></asp:RegularExpressionValidator>
        </div>
        <div class="FieldStyle">
            Highlight Type
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="HighlightType" runat="server">
            </asp:DropDownList>
        </div>
        <div class="FieldStyle">
            Display Order<span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="DisplayOrder" runat="server" MaxLength="9" Columns="5"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" CssClass="Error" Display="Dynamic"
                ErrorMessage="* Enter Display Order" ControlToValidate="DisplayOrder"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator2" runat="server" CssClass="Error" ControlToValidate="DisplayOrder"
                Display="Dynamic" ErrorMessage="Enter whole number." MaximumValue="999999999"
                MinimumValue="1" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
        </div>
        <div class="ValueStyle">
            <asp:CheckBox ID='VisibleInd' runat='server' Checked="true" Text="Enable this highlight"></asp:CheckBox>
        </div>
        <div class="FieldStyle" runat="server" id="lblLocale" style="display: none;">
            Locale <span class="Asterix">*</span>
        </div>
        <div class="ValueStyle" style="display: none;">
            <asp:DropDownList ID="ddlLocales" runat="server" AppendDataBoundItems="true">
                <asp:ListItem Value="">Select</asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" CssClass="Error" runat="server" ControlToValidate="ddlLocales"
                Display="Dynamic" ErrorMessage="* Specify the Locale" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </div>
        <asp:Panel ID="HighlightImageHeader" runat="server">
            <h4 class="SubTitle">Highlight Image</h4>
            <small>
                <asp:Label runat="server" ID="textadd" Text="Supported image formats include JPEG, GIF and PNG."></asp:Label></small>
            <small>
                <asp:Label runat="server" ID="textedit" Text="Supported image formats include JPEG, GIF and PNG."></asp:Label></small>
            <div id="tblShowImage" runat="server" visible="false" style="margin: 10px 0px 10px 0px;">
                <div class="LeftFloat" style="width: 300px;" id="pnlImage" runat="server">
                    <asp:Image ID="HighlightImage" runat="server" />
                </div>
                <div class="LeftFloat" style="margin-left: -1px;" runat="server" id="pnlUploadImage">
                    <asp:Panel runat="server" ID="pnlShowOption">
                        <div>
                            Select an Option
                        </div>
                        <div>
                            <asp:RadioButton ID="RadioHighlightCurrentImage" Text="Keep Current Image" runat="server"
                                GroupName="Highlight Image" AutoPostBack="True" OnCheckedChanged="RadioHighlightCurrentImage_CheckedChanged"
                                Checked="True" />
                            <br />
                            <asp:RadioButton ID="RadioHighlightNewImage" Text="Upload New Image" runat="server"
                                GroupName="Highlight Image" AutoPostBack="True" OnCheckedChanged="RadioHighlightNewImage_CheckedChanged" />
                            <br />
                            <asp:RadioButton ID="RadioHighlightNoImage" Text="No Image" runat="server" GroupName="Highlight Image"
                                AutoPostBack="True" OnCheckedChanged="RadioHighlightNoImage_CheckedChanged" />
                        </div>
                    </asp:Panel>
                    <br />
                    <div id="tblHighlight" runat="server" visible="false">
                        <div class="FieldStyle">
                            Select Image
                        </div>
                        <div class="ValueStyle">
                            <ZNode:UploadImage ID="UploadHighlightImage" runat="server"></ZNode:UploadImage>
                        </div>
                    </div>
                    <div class="FieldStyle">
                        Image ALT Text
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtImageAltTag" runat="server"></asp:TextBox>
                    </div>
                    <div>
                        <asp:Label ID="llblErrorMsg" runat="server" CssClass="Error" ForeColor="Red" Text=""
                            Visible="False"></asp:Label>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <br />
        <h4 class="SubTitle">Highlight Click Settings</h4>
        <div class="FieldStyle">
            On Click Behavior
        </div>
        <div class="ValueStyle">
            <asp:RadioButton AutoPostBack ="true" Checked="true" OnCheckedChanged="DisplayPopup_CheckedChanged" GroupName="Display"
                 ID="rdbEnableHyperlink" Text="When user clicks on the highlight, display the text entered below" 
                 runat="server" /><br />
            <asp:RadioButton ID="rdbHyperlinkExternal" Text="When user clicks on the highlight, redirect to a URL" GroupName="Display"
               AutoPostBack="true" runat="server" OnCheckedChanged="HyperlinkChk_CheckedChanged" />
        </div>
        <asp:Panel ID="pnlHyperLinkInternal" runat="server" Visible="false">
            <div class="FieldStyle">
                Display Text<br />
                <small>Enter the text that will be displayed when this Highlight is clicked.</small>
            </div>
            <div class="ValueStyle">
                <ZNode:HtmlTextBox ID="Description" runat="server"></ZNode:HtmlTextBox>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlHyperLinkExternal" runat="server" Visible="false">
            <div class="FieldStyle">
                Hyperlink<br />
                <small>Enter a web address to link this Highlight to. The address must be the full path
                        to the page you want to go to (ex: http://www.znode.com).</small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtHyperlink" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtHyperlink" CssClass="Error"
                    ValidationExpression="http://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?" runat="server" ErrorMessage="Invalid URL"></asp:RegularExpressionValidator>
            </div>
        </asp:Panel>
        <div class="ClearBoth">
            <br />
        </div>
        <div>
            <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>
