using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Inventory.ImportExportData
{
    /// <summary>
    /// Represents the Download Tags - PlugIns_DataManager_DownloadTags class
    /// </summary>
    public partial class DownloadTags : System.Web.UI.UserControl
    {
        #region Private member Variables
        private string DefaultPageLink = "~/SiteAdmin/Secure/Inventory/ImportExportData/Default.aspx";
        private Helper adminHelper = new Helper();
        private CategoryHelper categoryhelper = new CategoryHelper();
        #endregion

        #region Helper Methods
        /// <summary>
        ///  Returns string for a Given Dataset Values
        /// </summary>
        /// <param name="strFileName">The value of strFileName</param>
        /// <param name="gridViewControl">The value of Grid View Control</param>
        public void ExportDataToExcel(string strFileName, GridView gridViewControl)
        {
            try
            {
                Response.ClearContent();
                Response.AddHeader("content-disposition", "attachment; filename=" + strFileName);

                // Set as Excel as the primary format
                Response.AddHeader("Content-Type", "application/Excel");
                Response.ContentType = "application/Excel";
                System.IO.StringWriter sw = new System.IO.StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                gridViewControl.RenderControl(htw);
                Response.Write(sw.ToString());

                gridViewControl.Dispose();

                Response.End();
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
            }

            Response.Clear();
        }

        /// <summary>
        ///  Returns string for a Given Dataset Values
        /// </summary>
        /// <param name="fileName">The value of FileName</param>
        /// <param name="Data">The value of Data</param>
        public void ExportDataToCSV(string fileName, byte[] Data)
        {
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Response.AddHeader("Content-Type", "application/Excel");

            // Set as text as the primary format
            Response.ContentType = "text/csv";
            Response.ContentType = "application/vnd.xls";
            Response.AddHeader("Pragma", "public");

            Response.BinaryWrite(Data);
            Response.End();
        }

        #endregion

        #region Page_Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(ltrlError.Text.Trim()))
                {
                    lnkLog.Visible = true;
                }
                else
                {
                    lnkLog.Visible = false;
                }

                // Bind Catalogs
                this.BindCatalog();
            }

            ltrlError.Text = string.Empty;
        }

        #endregion

        #region Events
        /// <summary>
        /// Event fired when Download Inventory Button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDownloadTagInventory_Click(object sender, EventArgs e)
        {
            DataDownloadAdmin adminAccess = new DataDownloadAdmin();
            DataManagerAdmin datamanagerAdmin = new DataManagerAdmin();

            try
            {
                int lstCategoryValue = 0;
                if (lstCategory.SelectedValue != string.Empty)
                {
                    lstCategoryValue = Convert.ToInt32(lstCategory.SelectedValue);
                }

                DataSet ds = datamanagerAdmin.GetDownloadTags(lstCategoryValue, Convert.ToInt32(ddlCatalog.SelectedValue));

                if (ds.Tables[0].Rows.Count != 0)
                {
                    ZNodeTagsDataSet downloadTags = new ZNodeTagsDataSet();
                    int parsedInt;

                    foreach (DataRow data in ds.Tables[0].Rows)
                    {
                        ZNodeTagsDataSet.ZNodeTagRow dataRow = downloadTags.ZNodeTag.NewZNodeTagRow();

                        dataRow.TagGroupLabel = data["TagGroupLabel"].ToString();
                        dataRow.ControlType = data["ControlType"].ToString();
                        if (int.TryParse(data["CatalogID"].ToString(), out parsedInt))
                        {
                            dataRow.CatalogID = parsedInt;
                        }

                        if (int.TryParse(data["DisplayOrder"].ToString(), out parsedInt))
                        {
                            dataRow.DisplayOrder = parsedInt;
                        }

                        dataRow.TagName = data["TagName"].ToString().ToString();

                        if (int.TryParse(data["TagDisplayOrder"].ToString(), out parsedInt))
                        {
                            dataRow.TagDisplayOrder = parsedInt;
                        }

                        dataRow.IconPath = data["IconPath"].ToString();

                        if (int.TryParse(data["ProductID"].ToString(), out parsedInt))
                        {
                            dataRow.ProductID = parsedInt;
                        }

                        if (int.TryParse(data["CategoryID"].ToString(), out parsedInt))
                        {
                            dataRow.CategoryID = parsedInt;
                        }

                        if (int.TryParse(data["CategoryDisplayOrder"].ToString(), out parsedInt))
                        {
                            dataRow.CategoryDisplayOrder = parsedInt;
                        }

                        downloadTags.ZNodeTag.AddZNodeTagRow(dataRow);
                    }

                    // Save as Excel Sheet
                    if (ddlFileSaveType.SelectedValue == ".xls")
                    {
                        // Temp Grid control
                        GridView gridView = new GridView();
                        gridView.DataSource = downloadTags.ZNodeTag;
                        gridView.DataBind();
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Imported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Download - Tags", "Tags");

                        this.ExportDataToExcel("Tags.xls", gridView);
                    }
                    else
                    {
                        // Save as CSV Format
                        // Set Formatted Data from dataset object
                        string strData = adminAccess.Export(ds, true);

                        byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(strData);

                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Imported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Download - Tags", "Tags");

                        this.ExportDataToCSV("Tags.csv", data);
                    }
                }
                else
                {
                    ltrlError.Text = "No Tags Found";
                    return;
                }
            }
            catch (Exception ex)
            {
                lnkLog.Visible = true;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                ltrlError.Text = "Failed to process your request. Please check the activity log for more information.";
            }
        }

        /// <summary>
        /// Event fired when Cancel Button is triggered
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.DefaultPageLink);
        }

        #endregion

        #region Bind Methods

        /// <summary>
        /// Bind Catalog List
        /// </summary>
        protected void BindCatalog()
        {
            CatalogAdmin admin = new CatalogAdmin();
            ListItem listitem = new ListItem("All", "0");

            DataSet ds = admin.GetAllCatalogs().ToDataSet(false);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                {
                    ds.Tables[0].Rows[index]["Name"] = Server.HtmlDecode(ds.Tables[0].Rows[index]["Name"].ToString());
                }
            }

            DataView dataView = ds.Tables[0].DefaultView;
            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);

            if (string.Compare(profiles.StoreAccess, "AllStores") != 0)
            {
                dataView.RowFilter = string.Concat("CatalogID IN (", admin.GetCatalogIDsByPortalIDs(profiles.StoreAccess), ")");
            }

            ddlCatalog.DataSource = dataView;
            ddlCatalog.DataTextField = "Name";
            ddlCatalog.DataValueField = "CatalogId";
            ddlCatalog.DataBind();

            if (string.Compare(profiles.StoreAccess, "AllStores") == 0)
            {
                ddlCatalog.Items.Insert(0, listitem);
            }

            int CatalogId = Convert.ToInt32(ddlCatalog.SelectedValue);
            this.BindCategory(CatalogId);
        }

        /// <summary>
        /// Catalog Dropdown Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Ddlcatalog_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            int CatalogId = Convert.ToInt32(ddlCatalog.SelectedValue);
            this.BindCategory(CatalogId);
            ltrlError.Text = string.Empty;
        }

        /// <summary>
        /// Category List Control Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstCategory_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            ltrlError.Text = string.Empty;
        }

        /// <summary>
        /// Bind Category Method
        /// </summary>
        /// <param name="CatalogId">The value of Catalog Id</param>
        protected void BindCategory(int CatalogId)
        {
            ZNode.Libraries.DataAccess.Custom.CategoryHelper categoryHelper = new ZNode.Libraries.DataAccess.Custom.CategoryHelper();
            DataSet categoryDs = new DataSet();
            if (CatalogId > 0)
            {
                categoryDs = categoryHelper.GetCategoryByCatalogID(CatalogId);
            }
            else
            {
                categoryDs = categoryHelper.GetCategories();
            }

            if (categoryDs.Tables[0].Rows.Count > 0)
            {
                for (int index = 0; index < categoryDs.Tables[0].Rows.Count; index++)
                {
                    categoryDs.Tables[0].Rows[index]["Name"] = Server.HtmlDecode(categoryDs.Tables[0].Rows[index]["Name"].ToString());
                }
            }

            lstCategory.DataSource = categoryDs;
            lstCategory.DataTextField = "Name";
            lstCategory.DataValueField = "CategoryID";
            lstCategory.DataBind();
            ListItem allItem = new ListItem("All", string.Empty);
            lstCategory.Items.Insert(0, allItem);
        }

        #endregion
    }
}