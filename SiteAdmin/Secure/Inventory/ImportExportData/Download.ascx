<%@ Control Language="C#" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Inventory.ImportExportData.Download" CodeBehind="Download.ascx.cs" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<div class="Form SearchForm">
    <h1>
        <asp:Literal ID="ltrlTitle" runat="server"></asp:Literal></h1>
    <div>
        <ZNode:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
    <asp:Panel ID="pnlProductTypes" runat="server" Visible="false">
        <div class="RowStyle">
            <div class="ItemStyle">
                <div class="FieldStyle">
                    Select Product Type
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlProductType" runat="server">
                        <asp:ListItem Text="All" Selected="True" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Products Only" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Attributes Only" Value="2"></asp:ListItem>
                        <asp:ListItem Text="AddOnValues Only" Value="3"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="RowStyle">
        <div class="ItemStyle">
            <div class="FieldStyle">
                File Type
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlFileSaveType" runat="server">
                    <asp:ListItem Text="Microsoft Excel (.xls)" Value=".xls"></asp:ListItem>
                    <asp:ListItem Text="Delimited File Format" Value=".csv" Selected="True"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="RowStyle" id="divCatalog" runat="server" visible="false">
        <div class="ItemStyle">
            <div class="FieldStyle">
                Select Catalog
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlCatalog" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlCatalog_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div>
        <ZNode:Spacer ID="Spacer2" SpacerHeight="1" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
    <div class="ClearBoth">
        <br />
        <div class="Error">
            <asp:Literal ID="ltrlError" runat="server"></asp:Literal>
        </div>
        <div>
            <a href="../../../Data/Default/Logs/ZNodeLog.txt" id="lnkLog" runat="server"
                visible="false" target="_blank">Click here to view log >></a>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer4" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <!-- Download product inventory Panel -->
        <asp:Panel ID="pnlDownloadInventory" runat="server" Visible="false">
            <div class="ImageButtons">
                <asp:Button ID="BtnDownloadInventory" runat="server" Text="Download Inventory" CommandArgument="1" OnClick="BtnDownloadProdInventory_Click" />
                <asp:ImageButton ID="InventoryCancelButton" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
            </div>
        </asp:Panel>
        <!-- Download sku inventory Panel -->
        <asp:Panel ID="pnlDownloadSkuInventory" runat="server" Visible="false">
            <div class="ImageButtons">
                <asp:Button ID="btnDownloadSkuInventory" runat="server" CommandArgument="2" Text="Download Inventory"
                    OnClick="BtnDownloadProdInventory_Click" />
                <asp:ImageButton ID="SkuInventoryCancelButton" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
            </div>
        </asp:Panel>
        <!-- Download AddOnValue inventory Panel -->
        <asp:Panel ID="pnlDownloadAddOnValueInventory" runat="server" Visible="false">
            <div class="ImageButtons">
                <asp:Button ID="btnDownloadAddOnValueInventory" runat="server" CommandArgument="3" Text="Download Inventory"
                    OnClick="BtnDownloadProdInventory_Click" />
                <asp:ImageButton ID="AddOnValueInventoryCancelButton" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
            </div>
        </asp:Panel>
        <!-- Download Product prices Panel -->
        <asp:Panel ID="pnlDownloadPricing" runat="server" Visible="false">
            <div class="ImageButtons">
                <asp:Button ID="btnDownloadProductPricing" runat="server" Text="Download Pricing"
                    OnClick="BtnDownloadProductPricing_Click" />
                <asp:ImageButton ID="PricingCancelButton" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
            </div>
        </asp:Panel>
        <!-- Download product Panel -->
        <asp:Panel ID="pnlDownloadProduct" runat="server" Visible="false" CssClass="ClearBoth">
            <div class="ImageButtons">
                <asp:Button ID="btnDownloadProduct" runat="server" Text="Download Products" OnClick="BtnDownloadProduct_Click" />
                <asp:ImageButton ID="ProductCancelButton" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
            </div>
        </asp:Panel>
    </div>
</div>
