﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.ECommerce.Catalog;

namespace WebApp.SiteAdmin.Secure.Inventory.ImportExportData
{
    public partial class ReplaceOldImages : System.Web.UI.Page
    {
        #region Page  Events
        protected void Page_Load(object sender, EventArgs e)
        {
            btnReplaceImage.Enabled = true;
        }
        #endregion

        #region General Protected Events

        /// <summary>
        /// Handeles Button Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReplaceImage_Click(object sender, ImageClickEventArgs e)
        {
            ZNodeResourceManager resourceManager = new ZNodeResourceManager();
            try
            {
                string replaceImgAppPath = System.Configuration.ConfigurationManager.AppSettings["DeleteConfictImageApp"] != null && !string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["DeleteConfictImageApp"].ToString()) ?System.Configuration.ConfigurationManager.AppSettings["DeleteConfictImageApp"].ToString() : string.Empty;
                if (!string.IsNullOrEmpty(replaceImgAppPath))
                {
                    lblStatusMessage.Text = resourceManager.GetGlobalResourceObject("CommonCaption", "ReplaceImageStartsMessage");
                    lblStatusMessage.ForeColor = System.Drawing.Color.Green;
                    btnReplaceImage.Enabled = false;
                    ProcessStartInfo psi = new ProcessStartInfo();
                    psi.FileName = replaceImgAppPath;
                    psi.WorkingDirectory = Environment.CurrentDirectory;
                    Process proc = Process.Start(psi);
                    
                }
            }
            catch (Exception ex)
            {
                lblStatusMessage.Text ="Error Occured while Replacing Conflict Images.Try Again." + ex.Message;
                lblStatusMessage.ForeColor = System.Drawing.Color.Red;
                btnReplaceImage.Enabled = true;
            }
        }

        /// <summary>
        /// Event fired when Cancel button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Inventory/ImportExportData/Default.aspx");
        }
        #endregion
    }
}