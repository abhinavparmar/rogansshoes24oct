<%@ Control Language="C#" AutoEventWireup="True"
    Inherits="SiteAdmin.Secure.Inventory.ImportExportData.DownloadOrderShipping" CodeBehind="DownloadOrderShipping.ascx.cs" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<div class="SearchForm">
    <h1>Download Shipping Status</h1>
    <div>
        <ZNode:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
    <div>
        <div class="RowStyle">
            <div class="ItemStyle">
                <span class="FieldStyle">Begin Date</span>
                <br />
                <span class="ValueStyle">
                    <asp:TextBox ID="txtStartDate" Text='' runat="server" />&nbsp;<asp:ImageButton ID="imgbtnStartDt"
                        runat="server" ImageAlign="AbsMiddle" ImageUrl="~/SiteAdmin/Themes/images/SmallCalendar.gif" /><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter Begin date<br />"
                        ControlToValidate="txtStartDate" ValidationGroup="grpReports" CssClass="Error"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtStartDate"
                        CssClass="Error" Display="Dynamic" ErrorMessage="* Enter Valid Date in MM/DD/YYYY format<br />"
                        ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"
                        ValidationGroup="grpReports"></asp:RegularExpressionValidator>
                    <ajaxToolKit:CalendarExtender ID="CalendarExtender1" Enabled="true" PopupButtonID="imgbtnStartDt"
                        PopupPosition="TopLeft" runat="server" TargetControlID="txtStartDate">
                    </ajaxToolKit:CalendarExtender>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtStartDate"
                        ControlToValidate="txtEndDate" CssClass="Error" Display="Dynamic" ErrorMessage=" End Date must be greater than Begin date"
                        ValidationGroup="grpReports" Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>
                </span>
            </div>
            <div class="ItemStyle">
                <span class="FieldStyle">End Date</span><br />
                <span class="ValueStyle">
                    <asp:TextBox ID="txtEndDate" Text='' runat="server" />&nbsp;<asp:ImageButton ID="ImgbtnEndDt"
                        runat="server" ImageAlign="AbsMiddle" ImageUrl="~/SiteAdmin/Themes/images/SmallCalendar.gif" /><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEndDate"
                        ErrorMessage="Enter End date<br />" ValidationGroup="grpReports" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEndDate"
                        CssClass="Error" Display="Dynamic" ErrorMessage="* Enter Valid Date in MM/DD/YYYY format<br />"
                        ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"
                        ValidationGroup="grpReports"></asp:RegularExpressionValidator>
                    <ajaxToolKit:CalendarExtender ID="CalendarExtender2" Enabled="true" PopupButtonID="ImgbtnEndDt" PopupPosition="TopLeft"
                        runat="server" TargetControlID="txtEndDate">
                    </ajaxToolKit:CalendarExtender>

                </span>
            </div>
        </div>
        <div class="RowStyle">
            <div class="ItemStyle">
                <div class="FieldStyle">
                    File Type
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlFileSaveType" runat="server">
                        <asp:ListItem Text="Microsoft Excel (.xls)" Value=".xls"></asp:ListItem>
                        <asp:ListItem Text="Delimited File Format" Value=".csv" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div>
                    <ZNode:Spacer ID="Spacer2" SpacerHeight="1" SpacerWidth="3" runat="server"></ZNode:Spacer>
                </div>
                <div class="ClearBoth">
                    <div class="Error">
                        <asp:Literal ID="ltrlError" runat="server"></asp:Literal>
                    </div>
                    <div>
                        <a href="../../../Data/Default/logs/ZnodeLog.txt" id="lnkLog" runat="server"
                            visible="false" target="_blank">Click here to view log >></a>
                    </div>
                    <div>
                        <ZNode:Spacer ID="Spacer4" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                    </div>
                </div>
            </div>
            <div class="ClearBoth">
            </div>
            <div class="ImageButtons">
                <asp:Button ID="btnDownloadInventory" CssClass="Size175" runat="server" Text="Download Shipping Status"
                    OnClick="BtnDownloadProdInventory_Click" ValidationGroup="grpReports" />
                 <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
            </div>
        </div>
    </div>
</div>
