﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Catalog;

namespace WebApp.SiteAdmin.Secure.Inventory.ImportExportData
{
    public partial class ExecuteSSISPackage : System.Web.UI.Page
    {

        #region[Page Event]
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion

        protected void btnProductImport_Click(object sender, ImageClickEventArgs e)
        {
            ZNodeResourceManager resourceManager = new ZNodeResourceManager();
            try
            {
                string packageName = System.Configuration.ConfigurationManager.AppSettings["SSISPackageName"] != null ?
                    System.Configuration.ConfigurationManager.AppSettings["SSISPackageName"].ToString() : string.Empty;
                if (!string.IsNullOrEmpty(packageName))
                {
                    DataManagerHelper dataMangerHelper = new DataManagerHelper();
                    dataMangerHelper.ExecuteSQLAgentJob(packageName);
                    lblStatusMessage.Text = resourceManager.GetGlobalResourceObject("CommonCaption", "JobExecutionSuccessMessage");
                    lblStatusMessage.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    lblStatusMessage.Text = resourceManager.GetGlobalResourceObject("CommonCaption", "JobExecutionInvalidJobName") + packageName;
                }
            }
            catch (Exception ex)
            {
                lblStatusMessage.Text = resourceManager.GetGlobalResourceObject("CommonCaption", "JobExecutionSuccessMessage") + ex.Message;
                lblStatusMessage.ForeColor = System.Drawing.Color.Red;
            }
        }

        /// <summary>
        /// Event fired when Cancel button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Inventory/ImportExportData/Default.aspx");
            //Response.Redirect("~/SiteAdmin/Secure/DataManager/Default.aspx");
        }
    }
}