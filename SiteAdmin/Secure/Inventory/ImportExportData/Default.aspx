<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="true" %>

<script runat="server">

    protected void Page_Init(object sender, EventArgs e)
    {
        //Session["SelectedMenuItem"] = Request.Url.PathAndQuery;
    }
    
</script>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div>
        <h1>Downloads</h1>
         <hr /> 

        <div class="ImageAlign">
             <div class="ImportExportDownloadsImage">
                <img src="/SiteAdmin/Themes/Images/DownloadInventory.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A5" href="~/SiteAdmin/Secure/Inventory/ImportExportData/Download.aspx?filter=Inventory" runat="server">Download Inventory</a></div>
         
        </div>
       
        <div class="ImageAlign">
             <div class="ImportExportDownloadsImage">
                <img src="/SiteAdmin/Themes/Images/DownloadPricing.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A4" href="~/SiteAdmin/Secure/Inventory/ImportExportData/Download.aspx?filter=pricing" runat="server">Download Pricing</a></div>
          
        </div>
        
        <div class="ImageAlign">
             <div class="ImportExportDownloadsImage">
                <img src="/SiteAdmin/Themes/Images/DownloadProducts.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A1" href="~/SiteAdmin/Secure/Inventory/ImportExportData/Download.aspx?filter=Product" runat="server">Download Products</a></div>
         
        </div>
          
        <div class="ImageAlign">
             <div class="ImportExportDownloadsImage">
                <img src="/SiteAdmin/Themes/Images/DownloadAttributes.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A11" href="~/SiteAdmin/Secure/Inventory/ImportExportData/DownloadAttributes.aspx" runat="server">Download Attributes</a></div>
           
        </div>
        
        <div class="ImageAlign">
             <div class="ImportExportDownloadsImage">
                <img src="/SiteAdmin/Themes/Images/DownloadSKU.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A8" href="~/SiteAdmin/Secure/Inventory/ImportExportData/DownloadSku.aspx" runat="server">Download SKUs</a></div>
           
        </div>
         
        <div class="ImageAlign">
             <div class="ImportExportDownloadsImage">
                <img src="/SiteAdmin/Themes/Images/DownloadFacets.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A12" href="~/SiteAdmin/Secure/Inventory/ImportExportData/DownloadTags.aspx" runat="server">Download Facets</a></div>
           
        </div>
        
        <div class="ImageAlign">
             <div class="ImportExportDownloadsImage">
                <img src="/SiteAdmin/Themes/Images/DownloadShippingStatus.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A14" href="~/SiteAdmin/Secure/Inventory/ImportExportData/DownloadOrderShipping.aspx" runat="server">Download Shipping Status</a></div>
          
        </div> 
        
        <h1>Uploads</h1>
        <p>Download the appropriate file, modify it and then upload to your store.</p>
        <hr />
        <div class="ImageAlign">
             <div class="ImportExportImage">
                <img src="/SiteAdmin/Themes/Images/UploadInventory.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A2" href="~/SiteAdmin/Secure/Inventory/ImportExportData/UpdateInventory.aspx" runat="server">Upload Inventory</a></div>
           
        </div>
        
        <div class="ImageAlign">
             <div class="ImportExportImage">
                <img src="/SiteAdmin/Themes/Images/UploadPricing.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A6" href="~/SiteAdmin/Secure/Inventory/ImportExportData/UpdatePricing.aspx" runat="server">Upload Pricing</a></div>
           
        </div>
        
        <div class="ImageAlign">
             <div class="ImportExportImage">
                <img src="/SiteAdmin/Themes/Images/UploadProducts.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A20" href="~/SiteAdmin/Secure/Inventory/ImportExportData/UploadProduct.aspx" runat="server">Upload Products</a></div>
           
        </div>
        
        <div class="ImageAlign">
             <div class="ImportExportImage">
                <img src="/SiteAdmin/Themes/Images/UploadAttributes.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A13" href="~/SiteAdmin/Secure/Inventory/ImportExportData/UploadAttributes.aspx" runat="server">Upload Attributes</a></div>
          
        </div>
       
        <div class="ImageAlign">
             <div class="ImportExportImage">
                <img src="/SiteAdmin/Themes/Images/UploadSKU.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A9" href="~/SiteAdmin/Secure/Inventory/ImportExportData/UpdateSku.aspx" runat="server">Upload SKUs</a></div>
           
        </div>
         
        <div class="ImageAlign">
             <div class="ImportExportImage">
                <img src="/SiteAdmin/Themes/Images/UploadFacets.png" />
            </div>
            <div class="ImportExportShortcuts" ><a id="A10" href="~/SiteAdmin/Secure/Inventory/ImportExportData/UploadTags.aspx" runat="server">Upload Facets</a></div>
          
        </div>
         
        <div class="ImageAlign">
             <div class="ImportExportImage">
                <img src="/SiteAdmin/Themes/Images/UploadShippingStatus.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A3" href="~/SiteAdmin/Secure/Inventory/ImportExportData/UpdateOrderShipping.aspx" runat="server">Upload Shipping Status</a></div>
          
        </div>
        
        <div class="ImageAlign">
             <div class="ImportExportImage" >
                <img src="/SiteAdmin/Themes/Images/UploadZipCodeData.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A7" href="~/SiteAdmin/Secure/Inventory/ImportExportData/UploadZipcode.aspx" runat="server">Upload Zip Code Data</a></div>
           
        </div>
        <div class="ImageAlign">
             <div class="ImportExportImage" >
                <img src="/SiteAdmin/Themes/Images/UploadProducts.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A15" href="~/SiteAdmin/Secure/Inventory/ImportExportData/ExecuteSSISPackage.aspx" runat="server">Execute SSIS Package</a></div>
           
        </div>
        <div class="ImageAlign">
             <div class="ImportExportImage" >
                <img src="/SiteAdmin/Themes/Images/UploadProducts.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A16" href="~/SiteAdmin/Secure/Inventory/ImportExportData/ReplaceOldImages.aspx" runat="server">Execute Replace Old Image App </a></div>
           
        </div>
  </div>
</asp:Content>
