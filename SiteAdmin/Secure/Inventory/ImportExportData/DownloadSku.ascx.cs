using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Inventory.ImportExportData
{
    /// <summary>
    /// Represents the SiteAdmin - PlugIns_DataManager_DownloadSku class
    /// </summary>
    public partial class DownloadSku : System.Web.UI.UserControl
    {
        #region Private member Variables
        private int productId = 0;
        private int productTypeId = 0;
        private Helper adminHelper = new Helper();
        private string DefaultPageLink = "~/SiteAdmin/Secure/Inventory/ImportExportData/Default.aspx";
        #endregion

        #region Helper Methods
        /// <summary>
        ///  Returns string for a Given Dataset Values
        /// </summary>
        /// <param name="strFileName">The value of FileName</param>
        /// <param name="gridViewControl">The value of Grid View Control</param>
        public void ExportDataToExcel(string strFileName, GridView gridViewControl)
        {
            Response.Clear();
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=" + strFileName);

            // Set as Excel as the primary format
            Response.AddHeader("Content-Type", "application/Excel");
            Response.ContentType = "application/Excel";
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            // Remove controls from Column Headers
            if (gridViewControl.HeaderRow != null && gridViewControl.HeaderRow.Cells != null)
            {
                for (int rw = 0; rw < gridViewControl.Rows.Count; rw++)
                {
                    GridViewRow row = gridViewControl.Rows[rw];
                    for (int ct = 0; ct < row.Cells.Count; ct++)
                    {
                        // Save header text if found
                        string headerText = row.Cells[ct].Text;

                        // Check for controls in header
                        if (row.Cells[ct].HasControls())
                        {
                            // Check for link buttons (used in sorting)
                            if (row.Cells[ct].Controls[0].GetType().ToString() == "System.Web.UI.WebControls.CheckBox")
                            {
                                // Link button found, get text
                                headerText = ((CheckBox)row.Cells[ct].Controls[0]).Checked.ToString();
                            }

                            // Remove controls from header
                            row.Cells[ct].Controls.Clear();
                        }

                        // Reassign header text
                        row.Cells[ct].Text = headerText;
                    }
                }
            }

            gridViewControl.RenderControl(htw);
            Response.Write(sw.ToString());
            gridViewControl.Dispose();

            Response.End();
        }

        /// <summary>
        ///  Returns string for a Given Dataset Values
        /// </summary>
        /// <param name="fileName">The value of filename</param>
        /// <param name="Data">The value of Data</param>
        public void ExportDataToCSV(string fileName, byte[] Data)
        {
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Response.AddHeader("Content-Type", "application/Excel");

            // Set as text as the primary format
            Response.ContentType = "text/csv";
            Response.ContentType = "application/vnd.xls";
            Response.AddHeader("Pragma", "public");

            Response.BinaryWrite(Data);
            Response.End();
        }

        #endregion

        #region Page_Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(ltrlError.Text.Trim()))
                {
                    lnkLog.Visible = true;
                }
                else
                {
                    lnkLog.Visible = false;
                }

                // Bind all catalogs
                this.BindCatalog();
            }

            lstProduct.AutoPostBack = true;
            lstProduct.OnSelectedIndexChanged = this.LstProduct_SelectedIndexChanged;

            // BindSkuAttributes
            this.BindSkuAttributes();
        }

        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Event fired when Download Inventory Button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDownloadProdInventory_Click(object sender, EventArgs e)
        {
            DataDownloadAdmin adminAccess = new DataDownloadAdmin();

            try
            {
                DataSet ds = this.BindSearchData();

                if (ds.Tables[0].Rows.Count != 0)
                {
                    // Move data to SKU Typed Dataset
                    ZNodeSKUDataSet dssku = new ZNodeSKUDataSet();
                    int parsedInt;
                    decimal parsedDec;
                    foreach (DataRow data in ds.Tables[0].Rows)
                    {
                        ZNodeSKUDataSet.ZNodeSKURow dr = dssku.ZNodeSKU.NewZNodeSKURow();
                        dr.SKUID = 0;
                        if (int.TryParse(data["SKUID"].ToString(), out parsedInt))
                        {
                            dr.SKUID = parsedInt;
                        }

                        dr.ProductID = int.Parse(data["ProductID"].ToString());
                        dr.SKU = data["SKU"].ToString();
                        dr.Note = data["Note"].ToString();
                        if (decimal.TryParse(data["WeightAdditional"].ToString(), out parsedDec))
                        {
                            dr.WeightAdditional = parsedDec;
                        }
                        dr.SKUPicturePath = data["SKUPicturePath"].ToString();
                        dr.ImageAltTag = data["ImageAltTag"].ToString();
                        if (int.TryParse(data["DisplayOrder"].ToString(), out parsedInt))
                        {
                            dr.DisplayOrder = parsedInt;
                        }

                        if (decimal.TryParse(data["RetailPriceOverride"].ToString(), out parsedDec))
                        {
                            dr.RetailPriceOverride = parsedDec;
                        }

                        if (decimal.TryParse(data["SalePriceOverride"].ToString(), out parsedDec))
                        {
                            dr.SalePriceOverride = parsedDec;
                        }

                        if (decimal.TryParse(data["WholesalePriceOverride"].ToString(), out parsedDec))
                        {
                            dr.WholesalePriceOverride = parsedDec;
                        }

                        dr.ActiveInd = bool.Parse(data["ActiveInd"].ToString());
                        dssku.ZNodeSKU.AddZNodeSKURow(dr);
                    }

                    // Save as Excel Sheet
                    if (ddlFileSaveType.SelectedValue == ".xls")
                    {
                        // Temp Grid control
                        GridView gridView = new GridView();
                        gridView.DataSource = dssku.ZNodeSKU;
                        gridView.DataBind();
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Imported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Download - SKUs", "SKUs");

                        this.ExportDataToExcel("Sku.xls", gridView);
                    }
                    else
                    {
                        // Save as CSV Format
                        // Set Formatted Data from dataset object
                        string strData = adminAccess.Export(dssku, true);

                        byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(strData);
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Imported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Download - SKUs", "SKUs");

                        this.ExportDataToCSV("Sku.csv", data);
                    }
                }
                else
                {
                    ltrlError.Text = "No Sku Found";
                    return;
                }
            }
            catch (Exception ex)
            {
                lnkLog.Visible = true;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                ltrlError.Text = "Failed to process your request. Please check the activity log for more information.";
            }
        }

        /// <summary>
        /// Event fired when Cancel Button is triggered
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.DefaultPageLink);
        }

        /// <summary>
        /// Catalog Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlCatalog_SelectedIndexChanged(object sender, EventArgs e)
        {
            lstProduct.ContextKey = ddlCatalog.SelectedValue;
            ltrlError.Text = string.Empty;
        }

        /// <summary>
        /// Product option changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Clear the controls
            ControlPlaceHolder.Controls.Clear();
            ltrlError.Text = string.Empty;
            this.BindSkuAttributes();
        }

        /// <summary>
        /// Bind Catalog List
        /// </summary>
        protected void BindCatalog()
        {
            CatalogAdmin admin = new CatalogAdmin();
            ListItem listitem = new ListItem("All", "0");

            DataSet ds = admin.GetAllCatalogs().ToDataSet(false);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                {
                    ds.Tables[0].Rows[index]["Name"] = Server.HtmlDecode(ds.Tables[0].Rows[index]["Name"].ToString());
                }
            }

            DataView dataView = ds.Tables[0].DefaultView;
            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (string.Compare(profiles.StoreAccess, "AllStores") != 0)
            {
                dataView.RowFilter = string.Concat("CatalogID IN (", admin.GetCatalogIDsByPortalIDs(profiles.StoreAccess), ")");
            }

            ddlCatalog.DataSource = dataView;
            ddlCatalog.DataTextField = "Name";
            ddlCatalog.DataValueField = "CatalogId";
            ddlCatalog.DataBind();

            if (string.Compare(profiles.StoreAccess, "AllStores") == 0)
            {
                ddlCatalog.Items.Insert(0, listitem);
            }
            else
            {
                lstProduct.ContextKey = ddlCatalog.SelectedValue;
            }

            ltrlError.Text = string.Empty;
        }

        /// <summary>
        /// Bind Attributes List.
        /// </summary>
        protected void BindSkuAttributes()
        {
            // Set ProductId
            this.productId = 0;

            int.TryParse(lstProduct.Value, out this.productId);

            if (this.productId != 0)
            {
                ProductAdmin _adminAccess = new ProductAdmin();

                DataSet ds = _adminAccess.GetProductDetails(this.productId);

                // Check for Number of Rows
                if (ds.Tables[0].Rows.Count != 0)
                {
                    // Check For Product Type
                    this.productTypeId = int.Parse(ds.Tables[0].Rows[0]["ProductTypeId"].ToString());
                }

                DataSet MyDataSet = _adminAccess.GetAttributeTypeByProductTypeID(this.productTypeId);

                if (MyDataSet.Tables[0].Rows.Count > 0)
                {
                    // Repeats until Number of AttributeType for this Product
                    foreach (DataRow dr in MyDataSet.Tables[0].Rows)
                    {
                        // Bind Attributes
                        DataSet _AttributeDataSet = _adminAccess.GetAttributesByAttributeTypeIdandProductID(int.Parse(dr["attributetypeid"].ToString()), this.productId);

                        System.Web.UI.WebControls.DropDownList lstControl = new DropDownList();
                        lstControl.ID = "lstAttribute" + dr["AttributeTypeId"].ToString();

                        ListItem li = new ListItem(dr["Name"].ToString(), "0");
                        li.Selected = true;

                        lstControl.DataSource = _AttributeDataSet;
                        lstControl.DataTextField = "Name";
                        lstControl.DataValueField = "AttributeId";
                        lstControl.DataBind();
                        lstControl.Items.Insert(0, li);

                        // Add Dynamic Attribute DropDownlist in the Placeholder
                        ControlPlaceHolder.Controls.Add(lstControl);

                        Literal lit1 = new Literal();
                        lit1.Text = "&nbsp;&nbsp;";
                        ControlPlaceHolder.Controls.Add(lit1);
                    }

                    pnlAttribteslist.Visible = true;
                }
                else
                {
                    pnlAttribteslist.Visible = false;
                }
            }
            else
            {
                pnlAttribteslist.Visible = false;
            }

            ltrlError.Text = string.Empty;
        }

        /// <summary>
        /// Bind Search Data Method
        /// </summary>
        /// <returns>Returns the DataSet</returns>
        protected DataSet BindSearchData()
        {
            string Attributes = String.Empty;
            string AttributeList = string.Empty;
            DataSet MyDatas = new DataSet();
            SKUAdmin _SkuAdmin = new SKUAdmin();
            ProductAdmin _adminAccess = new ProductAdmin();

            this.productId = 0;

            int.TryParse(lstProduct.Value, out this.productId);
            if (productId == 0)
            {
                productId = _adminAccess.GetProductIdByName(lstProduct.Text);
            }
            if (this.productId != 0)
            {
                DataSet ds = _adminAccess.GetProductDetails(this.productId);

                // Check for Number of Rows
                if (ds.Tables[0].Rows.Count != 0)
                {
                    // Check For Product Type
                    this.productTypeId = int.Parse(ds.Tables[0].Rows[0]["ProductTypeId"].ToString());
                }

                // GetAttribute for this ProductType
                DataSet MyAttributeTypeDataSet = _adminAccess.GetAttributeTypeByProductTypeID(this.productTypeId);

                foreach (DataRow MyDataRow in MyAttributeTypeDataSet.Tables[0].Rows)
                {
                    System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + MyDataRow["AttributeTypeId"].ToString());

                    if (lstControl != null)
                    {
                        int selValue = int.Parse(lstControl.SelectedValue);

                        if (selValue > 0)
                        {
                            Attributes += selValue.ToString() + ",";
                        }
                    }
                }

                if (Attributes != string.Empty)
                {
                    // Split the string
                    AttributeList = Attributes.Substring(0, Attributes.Length - 1);
                }
            }

            if (Attributes.Length == 0 && this.productId == 0 && lstProduct.Text.Length == 0 && ddlCatalog.SelectedValue != "0")
            {
                SKUHelper helper = new SKUHelper();
                MyDatas = helper.GetSkuByCatalogID(int.Parse(ddlCatalog.SelectedValue));
            }
            else if (Attributes.Length == 0 && this.productId == 0 && lstProduct.Text.Length == 0 && ddlCatalog.SelectedValue == "0")
            {
                MyDatas = _SkuAdmin.GetAllSKU().ToDataSet(false);
            }
            else
            {
                MyDatas = _SkuAdmin.GetBySKUAttributes(this.productId, AttributeList);
            }

            return MyDatas;
        }
        #endregion
    }
}