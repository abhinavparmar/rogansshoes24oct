using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Inventory.ImportExportData
{
    /// <summary>
    /// Represents the SiteAdmin - PlugIns_DataManager_Download class
    /// </summary>
    public partial class Download : System.Web.UI.UserControl
    {
        #region Private member Variables
        private string Filter = string.Empty;
        private string DefaultPageLink = "~/SiteAdmin/Secure/Inventory/ImportExportData/Default.aspx";
        private Helper adminHelper = new Helper();
        #endregion

        #region public properties
        /// <summary>
        /// Gets or sets the Account Object from/to Cache
        /// </summary>
        public Account UserAccount
        {
            get
            {
                if (ViewState["AccountObject"] != null)
                {
                    // The account info with 'AccountObject' is retrieved from the cache using Get Method and
                    // It is converted to a Entity Account object
                    return (Account)ViewState["AccountObject"];
                }

                return new Account();
            }

            set
            {
                // Customer account object is placed in the session and assigned a key, AccountObject.            
                ViewState["AccountObject"] = value;
            }
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Strip Html Method
        /// </summary>
        /// <param name="html">The value of html</param>
        /// <param name="allowHarmlessTags">Bool value true or false allowHarmlessTags</param>
        /// <returns>Returns the string</returns>
        public static string StripHtml(string html, bool allowHarmlessTags)
        {
            if (html == null || html == string.Empty)
            {
                return string.Empty;
            }

            if (allowHarmlessTags)
            {
                return System.Text.RegularExpressions.Regex.Replace(html, string.Empty, string.Empty);
            }

            return System.Text.RegularExpressions.Regex.Replace(html, "<(.|\n)+?>", string.Empty);
        }

        /// <summary>
        ///  Returns string for a Given Dataset Values
        /// </summary>
        /// <param name="strFileName">The value of File Name</param>
        /// <param name="gridViewControl">Grid View Control</param>
        public void ExportDataToExcel(string strFileName, GridView gridViewControl)
        {
            Response.Clear();
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=" + strFileName);

            // Set as Excel as the primary format
            Response.AddHeader("Content-Type", "application/Excel");
            Response.ContentType = "application/Excel";
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            // Remove controls from Column Headers
            if (gridViewControl.HeaderRow != null && gridViewControl.HeaderRow.Cells != null)
            {
                for (int rw = 0; rw < gridViewControl.Rows.Count; rw++)
                {
                    GridViewRow row = gridViewControl.Rows[rw];
                    for (int ct = 0; ct < row.Cells.Count; ct++)
                    {
                        // Save header text if found
                        string headerText = row.Cells[ct].Text;

                        // Check for controls in header
                        if (row.Cells[ct].HasControls())
                        {
                            // Check for link buttons (used in sorting)
                            if (row.Cells[ct].Controls[0].GetType().ToString() == "System.Web.UI.WebControls.CheckBox")
                            {
                                // Link button found, get text
                                headerText = ((CheckBox)row.Cells[ct].Controls[0]).Checked.ToString();
                            }

                            // Remove controls from header
                            row.Cells[ct].Controls.Clear();
                        }

                        // Reassign header text
                        row.Cells[ct].Text = headerText;
                    }
                }
            }

            gridViewControl.RenderControl(htw);
            Response.Write(sw.ToString());

            gridViewControl.Dispose();

            Response.End();
        }

        /// <summary>
        ///  Returns string for a Given Dataset Values
        /// </summary>
        /// <param name="fileName">The value of File Name</param>
        /// <param name="Data">The value of Data</param>
        public void ExportDataToCSV(string fileName, byte[] Data)
        {
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Response.AddHeader("Content-Type", "application/Excel");

            // Set as text as the primary format
            Response.ContentType = "text/csv";
            Response.ContentType = "application/vnd.xls";
            Response.AddHeader("Pragma", "public");

            Response.BinaryWrite(Data);
            Response.End();
        }
        #endregion

        #region Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get filter value from request parameter
            if (Request.Params["filter"] != null)
            {
                this.Filter = Request.Params["filter"];
            }

            if (this.Filter.Length == 0)
            {
                Response.Redirect(this.DefaultPageLink);
            }

            if (!Page.IsPostBack)
            {
                ltrlError.Text = string.Empty;
                lnkLog.Visible = false;

                if (string.Compare(this.Filter, "pricing", true) == 0)
                {
                    ltrlTitle.Text = "Download Pricing";
                    pnlDownloadPricing.Visible = true;
                }
                else if (string.Compare(this.Filter, "inventory", true) == 0)
                {
                    ltrlTitle.Text = "Download Inventory";
                    pnlDownloadInventory.Visible = true;
                }
                else if (string.Compare(this.Filter, "product", true) == 0)
                {
                    this.BindCatalog();
                    ltrlTitle.Text = "Download Products ";
                    divCatalog.Visible = true;
                    pnlDownloadProduct.Visible = true;
                }
                else
                {
                    Response.Redirect(this.DefaultPageLink);
                }
            }
        }

        /// <summary>
        /// Event fired when Download Inventory Button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDownloadProdInventory_Click(object sender, EventArgs e)
        {
            try
            {
                DataDownloadAdmin adminAccess = new DataDownloadAdmin();
                DataManagerAdmin dataManager = new DataManagerAdmin();
                DataSet ds = null;
                ZNodeSKUInventoryDataset impProduct = new ZNodeSKUInventoryDataset();
                CatalogAdmin catalogAdmin = new CatalogAdmin();
                Account account = this.UserAccount;
                MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                string roleList = string.Empty;

                // Get roles for this User account
                string[] roles = Roles.GetRolesForUser(user.UserName);

                foreach (string Role in roles)
                {
                    roleList += Role + "<br>";
                }

                string rolename = roleList;

                // Hide the Delete button if a NonAdmin user has entered this page
                if (!Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "ADMIN"))
                {
                    if (Roles.IsUserInRole(user.UserName, "CATALOG EDITOR"))
                    {
                        string portalIDs = string.Empty;
                        portalIDs = UserStoreAccess.GetAvailablePortals;
                        string catalogIDs = string.Empty;
                        catalogIDs = catalogAdmin.GetCatalogIDsByPortalIDs(portalIDs);

                        SKUHelper skuhelper = new SKUHelper();
                        int parsedInt;
                        SKUInventoryService aovis = new SKUInventoryService();
                        SKUInventory skuInv = new SKUInventory();
                        if (catalogIDs != "0")
                        {
                            string[] catalog = catalogIDs.Split(',');

                            for (int index = 0; index <= catalog.Length - 1; index++)
                            {
                                ds = skuhelper.GetSkuByCatalogID(int.Parse(catalog[index]));

                                foreach (DataRow data in ds.Tables[0].Rows)
                                {
                                    ZNodeSKUInventoryDataset.ZNodeSKUInventoryRow dataRow = impProduct.ZNodeSKUInventory.NewZNodeSKUInventoryRow();
                                    dataRow.SKU = data["SKU"].ToString();

                                    skuInv = null;
                                    skuInv = aovis.GetBySKU(data["SKU"].ToString());

                                    if (int.TryParse(skuInv.QuantityOnHand.ToString(), out parsedInt))
                                    {
                                        dataRow.QuantityOnHand = parsedInt;
                                    }

                                    if (int.TryParse(skuInv.ReOrderLevel.ToString(), out parsedInt))
                                    {
                                        dataRow.ReOrderLevel = parsedInt;
                                    }

                                    impProduct.ZNodeSKUInventory.AddZNodeSKUInventoryRow(dataRow);
                                }
                            }

                            ds = (DataSet)impProduct;
                        }
                    }
                }
                else
                {
                    ds = dataManager.GetAllSkuInventory();
                }

                if (ds.Tables[0].Rows.Count > 0 || impProduct.Tables[0].Rows.Count > 0)
                {
                    if (ddlFileSaveType.SelectedValue == ".xls")
                    {
                        // Temp Grid control
                        GridView gridView = new GridView();
                        gridView.DataSource = ds;
                        gridView.DataBind();
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Imported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Download - Inventory", "Inventory");
                        this.ExportDataToExcel("Inventory.xls", gridView);
                    }
                    else
                    {
                        // Set Formatted Data from dataset
                        string strData = adminAccess.Export(ds, true);
                        byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(strData);
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Imported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Download - Inventory", "Inventory");
                        this.ExportDataToCSV("Inventory.csv", data);
                    }
                }
                else
                {
                    ltrlError.Text = "Unable to download Product Inventory. No inventory found.";
                    return;
                }
            }
            catch (Exception ex)
            {
                lnkLog.Visible = true;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                ltrlError.Text = "Failed to process your request. Please check the activity log for more information";
            }
        }

        /// <summary>
        /// Event fired when Download Pricing button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDownloadProductPricing_Click(object sender, EventArgs e)
        {
            try
            {
                DataDownloadAdmin adminAccess = new DataDownloadAdmin();
                DataManagerAdmin dataManager = new DataManagerAdmin();
                DataSet ds = null;

                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (profiles.StoreAccess != "AllStores")
                {
                    ds = dataManager.GetProductPrices(int.Parse(ddlProductType.SelectedValue), profiles.StoreAccess);
                }
                else
                {
                    ds = dataManager.GetProductPrices(int.Parse(ddlProductType.SelectedValue));
                }

                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (ddlFileSaveType.SelectedValue == ".xls")
                    {
                        // Temp Grid control
                        GridView gridView = new GridView();
                        gridView.DataSource = ds;
                        gridView.DataBind();
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Imported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Download - Pricing", "Pricing");
                        this.ExportDataToExcel("Pricing.xls", gridView);
                    }
                    else
                    {
                        // Set Formatted Data from dataset
                        string strData = adminAccess.Export(ds, true);
                        byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(strData);
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Imported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Download - Pricing", "Pricing");
                        this.ExportDataToCSV("Pricing.csv", data);
                    }
                }
                else
                {
                    ltrlError.Text = "Unable to download Product Pricing. No Product pricing found";
                    return;
                }
            }
            catch (Exception ex)
            {
                lnkLog.Visible = true;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                ltrlError.Text = "Failed to process your request. Please check the activity log for more information.";
            }
        }

        /// <summary>
        /// Event fired when Download Product Button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDownloadProduct_Click(object sender, EventArgs e)
        {
            try
            {
                DataDownloadAdmin adminAccess = new DataDownloadAdmin();
                DataManagerAdmin dataManager = new DataManagerAdmin();
                DataSet ds = new DataSet();
                ProductAdmin prodadmin = new ProductAdmin();
                if (ddlCatalog.SelectedValue == "0")
                {
                    TList<Product> ProductList = prodadmin.GetAllProducts();
                    if (ProductList != null)
                    {
                        ds = ProductList.ToDataSet(false);
                    }
                    else
                    {
                        ltrlError.Text = "No products found. Please try another catalog. ";
                        return;
                    }
                }
                else
                {
                    ds = prodadmin.GetProductsByCatalogID(int.Parse(ddlCatalog.SelectedValue.ToString()));
                }

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ZNodeProductDataset impProduct = new ZNodeProductDataset();
                    int parsedInt;
                    decimal parsedDec;
                    bool parsedBool;
                    foreach (DataRow data in ds.Tables[0].Rows)
                    {
                        ZNodeProductDataset.ZNodeProductRow dataRow = impProduct.ZNodeProduct.NewZNodeProductRow();

                        if (int.TryParse(data["ProductID"].ToString(), out parsedInt))
                        {
                            dataRow.ProductID = parsedInt;
                        }

                        dataRow.SKU = string.Empty;
                        dataRow.QuantityOnHand = 0;
                        dataRow.Name = data["Name"].ToString();
                        dataRow.ShortDescription = Server.HtmlDecode(data["ShortDescription"].ToString());
                        dataRow.Description = Server.HtmlDecode(data["Description"].ToString());
                        dataRow.FeaturesDesc = Server.HtmlDecode(data["FeaturesDesc"].ToString());
                        dataRow.ProductNum = data["ProductNum"].ToString();
                        dataRow.ProductTypeID = int.Parse(data["ProductTypeID"].ToString());
                        if (int.TryParse(data["ExpirationPeriod"].ToString(), out parsedInt))
                        {
                            dataRow.ExpirationPeriod = parsedInt;
                        }

                        if (int.TryParse(data["ExpirationFrequency"].ToString(), out parsedInt))
                        {
                            dataRow.ExpirationFrequency = parsedInt;
                        }

                        if (decimal.TryParse(data["RetailPrice"].ToString(), out parsedDec))
                        {
                            dataRow.RetailPrice = parsedDec;
                        }

                        if (decimal.TryParse(data["SalePrice"].ToString(), out parsedDec))
                        {
                            dataRow.SalePrice = parsedDec;
                        }

                        if (decimal.TryParse(data["WholesalePrice"].ToString(), out parsedDec))
                        {
                            dataRow.WholesalePrice = parsedDec;
                        }

                        if (int.TryParse(data["ReviewStateID"].ToString(), out parsedInt))
                        {
                            dataRow.ReviewStateID = parsedInt;
                        }

                        if (bool.TryParse(data["Franchisable"].ToString(), out parsedBool))
                        {
                            dataRow.Franchisable = parsedBool;
                        }

                        dataRow.ImageFile = data["ImageFile"].ToString();

                        if (decimal.TryParse(data["Weight"].ToString(), out parsedDec))
                        {
                            dataRow.Weight = parsedDec;
                        }

                        if (decimal.TryParse(data["Length"].ToString(), out parsedDec))
                        {
                            dataRow.Length = parsedDec;
                        }

                        if (decimal.TryParse(data["Width"].ToString(), out parsedDec))
                        {
                            dataRow.Width = parsedDec;
                        }

                        if (decimal.TryParse(data["Height"].ToString(), out parsedDec))
                        {
                            dataRow.Height = parsedDec;
                        }

                        if (int.TryParse(data["DisplayOrder"].ToString(), out parsedInt))
                        {
                            dataRow.DisplayOrder = parsedInt;
                        }

                        if (int.TryParse(data["ReviewStateID"].ToString(), out parsedInt))
                        {
                            dataRow.ReviewStateID = parsedInt;
                        }

                        dataRow.ActiveInd = bool.Parse(data["ActiveInd"].ToString());
                        dataRow.CallForPricing = bool.Parse(data["CallForPricing"].ToString());
                        dataRow.HomepageSpecial = bool.Parse(data["HomepageSpecial"].ToString());
                        dataRow.CategorySpecial = bool.Parse(data["CategorySpecial"].ToString());
                        dataRow.InventoryDisplay = int.Parse(data["InventoryDisplay"].ToString());

                        dataRow.Keywords = data["Keywords"].ToString();

                        if (int.TryParse(data["ManufacturerID"].ToString(), out parsedInt))
                        {
                            dataRow.ManufacturerID = parsedInt;
                        }

                        if (int.TryParse(data["ShippingRuleTypeID"].ToString(), out parsedInt))
                        {
                            dataRow.ShippingRuleTypeID = parsedInt;
                        }

                        dataRow.SEOTitle = data["SEOTitle"].ToString();
                        dataRow.SEOKeywords = data["SEOKeywords"].ToString();
                        dataRow.SEODescription = data["SEODescription"].ToString();
                        dataRow.InStockMsg = data["InStockMsg"].ToString();
                        dataRow.OutOfStockMsg = data["OutOfStockMsg"].ToString();

                        if (bool.TryParse(data["TrackInventoryInd"].ToString(), out parsedBool))
                        {
                            dataRow.TrackInventoryInd = parsedBool;
                        }

                        if (bool.TryParse(data["FreeShippingInd"].ToString(), out parsedBool))
                        {
                            dataRow.FreeShippingInd = parsedBool;
                        }

                        if (bool.TryParse(data["NewProductInd"].ToString(), out parsedBool))
                        {
                            dataRow.NewProductInd = parsedBool;
                        }

                        dataRow.SEOURL = data["SEOURL"].ToString();

                        if (int.TryParse(data["MaxQty"].ToString(), out parsedInt))
                        {
                            dataRow.MaxQty = parsedInt;
                        }

                        if (bool.TryParse(data["ShipSeparately"].ToString(), out parsedBool))
                        {
                            dataRow.ShipSeparately = parsedBool;
                        }

                        if (bool.TryParse(data["FeaturedInd"].ToString(), out parsedBool))
                        {
                            dataRow.FeaturedInd = parsedBool;
                        }

                        dataRow.RecurringBillingInd = data["RecurringBillingInd"].ToString();
                        dataRow.RecurringBillingInstallmentInd = data["RecurringBillingInstallmentInd"].ToString();
                        dataRow.PortalID = 0;

                        if (int.TryParse(data["PortalId"].ToString(), out parsedInt))
                        {
                            dataRow.PortalID = parsedInt;
                        }

                        dataRow.ReviewStateID = int.Parse(data["ReviewStateID"].ToString());
                        dataRow.Franchisable = bool.Parse(data["Franchisable"].ToString());

                        impProduct.ZNodeProduct.AddZNodeProductRow(dataRow);
                    }

                    if (ddlFileSaveType.SelectedValue == ".xls")
                    {
                        // Temp Grid control
                        GridView gridView = new GridView();
                        gridView.DataSource = impProduct;
                        gridView.DataBind();
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Imported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Download - Products", "Products");
                        this.ExportDataToExcel("Product.xls", gridView);
                    }
                    else
                    {
                        // Set Formatted Data from dataset object
                        string strData = adminAccess.Export(impProduct, true);

                        byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(strData);
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Imported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Download - Products", "Products");
                        this.ExportDataToCSV("Product.csv", data);
                    }
                }
                else
                {
                    ltrlError.Text = "Unable to download Product data. Please try another catalog";
                }
            }
            catch (Exception ex)
            {
                lnkLog.Visible = true;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                ltrlError.Text = "Failed to process your request. Please check the activity log for more information.";
            }
        }

        /// <summary>
        /// Event fired when Cancel Button is triggered
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.DefaultPageLink);
        }

        /// <summary>
        /// Catalog Dropdown list Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlCatalog_SelectedIndexChanged(object sender, EventArgs e)
        {
            ltrlError.Text = string.Empty;
        }

        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind Catalog List
        /// </summary>
        protected void BindCatalog()
        {
            CatalogAdmin admin = new CatalogAdmin();
            ListItem listitem = new ListItem("All", "0");

            DataSet ds = admin.GetAllCatalogs().ToDataSet(false);
            DataView dataView = ds.Tables[0].DefaultView;
            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (string.Compare(profiles.StoreAccess, "AllStores") != 0)
            {
                dataView.RowFilter = string.Concat("CatalogID IN (", admin.GetCatalogIDsByPortalIDs(profiles.StoreAccess), ")");
            }

            ddlCatalog.DataSource = dataView;
            ddlCatalog.DataTextField = "Name";
            ddlCatalog.DataValueField = "CatalogId";
            ddlCatalog.DataBind();

            if (string.Compare(profiles.StoreAccess, "AllStores") == 0)
            {
                ddlCatalog.Items.Insert(0, listitem);
            }
        }
        #endregion
    }
}