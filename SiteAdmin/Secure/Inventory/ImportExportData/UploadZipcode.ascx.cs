using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Inventory.ImportExportData
{
    /// <summary>
    /// Represents the SiteAdmin - PlugIns_DataManager_UploadZipcode Class
    /// </summary>
    public partial class UploadZipcode : System.Web.UI.UserControl
    {
        #region Events

        /// <summary>
        /// Event fired when Submit button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            string dataPath = ZNode.Libraries.Framework.Business.ZNodeConfigManager.EnvironmentConfig.DataPath;
            string filePath = string.Empty;
            DataDownloadAdmin dataAdmin = new DataDownloadAdmin();
            DataManagerAdmin manager = new DataManagerAdmin();

            try
            {
                string dataFilePath = Server.MapPath(ZNode.Libraries.Framework.Business.ZNodeConfigManager.EnvironmentConfig.DataPath);

                // Set the file path
                filePath = txtInputFile.Text;

                FileInfo filefound = new FileInfo(filePath);

                /* To Check Whether File is Exists Or Not*/
                if (filefound.Exists)
                {
                    StreamReader fileReader = new StreamReader(filePath);

                    int ErrorCount = 0;
                    int SqlErrorCount = 0;
                    bool status = false;
                    int indexRowCount = 0;
                    int parsedInt;
                    decimal parsedDec;
                    int MaxErrorCount = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ImportMaxErrorCount"].ToString());
                    ZNodeZipCodeDataSet impZipCode = new ZNodeZipCodeDataSet();
                    string dataRecord = fileReader.ReadLine();
                    if (dataRecord != null)
                        dataRecord = fileReader.ReadLine();

                    while (dataRecord != null)
                    {
                        indexRowCount++;

                        if (ErrorCount >= MaxErrorCount)
                        {
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Upload Zip Code: Maximum error count reached. Please see the log details for additional information.");
                            ltrlError.Text = "Uploads fails, Please see the log details for the detail information.";
                            return;
                        }

                        string[] data = dataRecord.Split(',');

                        if (data.Length != 16)
                        {
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(string.Format("Upload Zip Code: There are input fields missing in row:{0}", indexRowCount));
                            ErrorCount++;

                            dataRecord = fileReader.ReadLine();
                            continue;
                        }

                        try
                        {
                            ZNodeZipCodeDataSet.ZNodeZipCodeRow dr = impZipCode.ZNodeZipCode.NewZNodeZipCodeRow();

                            dr.ZIP = data[0];
                            dr.ZIPType = data[1];
                            dr.CityName = data[2];
                            dr.CityType = data[3];
                            dr.CountyName = data[4];
                            dr.CountyFIPS = data[5];
                            dr.StateName = data[6];
                            dr.StateAbbr = data[7];
                            dr.StateFIPS = data[8];

                            dr.MSACode = data[9];
                            dr.AreaCode = data[10];
                            dr.TimeZone = data[11];
                            if (decimal.TryParse(data[12], out parsedDec))
                            {
                                dr.UTC = parsedDec;
                            }

                            dr.DST = data[13];
                            if (decimal.TryParse(data[14], out parsedDec))
                            {
                                dr.Latitude = parsedDec;
                            }

                            if (decimal.TryParse(data[15], out parsedDec))
                            {
                                dr.Longitude = parsedDec;
                            }


                            impZipCode.ZNodeZipCode.AddZNodeZipCodeRow(dr);
                        }
                        catch (Exception ex)
                        {
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                            ErrorCount++;
                        }

                        dataRecord = fileReader.ReadLine();
                    }

                    if (impZipCode.Tables[0].Rows.Count > 0)
                    {
                        DataManagerAdmin managerAdmin = new DataManagerAdmin();
                        status = managerAdmin.UploadZipCode(impZipCode.ZNodeZipCode, out SqlErrorCount);
                    }

                    if ((ErrorCount > 0 || SqlErrorCount > 0) && status)
                    {
                        ltrlError.Text = "Upload failed, Please see the activity log for more information.";
                        return;
                    }

                    if (!status)
                    {
                        ltrlError.Text = "Upload failed, Please see the activity log for more information.";
                    }
                    else
                    {
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Exported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Upload ZIP Code Data", "ZIP Code Data");
                        Response.Redirect("~/SiteAdmin/Secure/Inventory/ImportExportData/Default.aspx");
                    }
                }
                else
                {
                    ltrlError.Text = "Input file not found. Please check the file path and try again.";
                    return;
                }
            }
            catch (Exception ex)
            {
                // Generic exception handler
                Helper adminHelper = new Helper();
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                ltrlError.Text = "Failed to process your request. Please check the activity log to view more details.";
                return;
            }

            ltrlmsg.Text = "File successfully uploaded.";
            ltrlmsg.Visible = true;
            btnGoback.Visible = true;
            uploadPanel.Visible = false;
        }

        /// <summary>
        /// Event fired when Cancel button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Inventory/ImportExportData/Default.aspx");
        }
        #endregion
    }
}