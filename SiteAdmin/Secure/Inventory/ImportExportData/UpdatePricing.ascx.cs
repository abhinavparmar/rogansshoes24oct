using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Inventory.ImportExportData
{
    /// <summary>
    /// Represents the SiteAdmin - PlugIns_DataManager_UpdatePricing class
    /// </summary>
    /// 

    public partial class UpdatePricing : System.Web.UI.UserControl
    {
        #region Private Member Variables
        private string DefaultPageLink = "~/SiteAdmin/Secure/Inventory/ImportExportData/Default.aspx";
        #endregion

        #region Events

        /// <summary>
        /// Event fired when Submit button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        /// 
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            Stream fileStream = Request.Files[0].InputStream;
            StreamReader fileReader = new StreamReader(fileStream);

            int ErrorCount = 0;
            int SqlErrorCount = 0;
            bool status = false;
            int indexRowCount = 0;
            int parsedInt;
            int lincenoCount = 0;
            decimal parsedDec;  
            int MaxErrorCount = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ImportMaxErrorCount"].ToString());
            ZNodePricingDataSet impPricing = new ZNodePricingDataSet();
            string dataRecord = fileReader.ReadLine();
            if (dataRecord == null)
            {
                ltrlError.Text = "No Records found";
                return;
            }
            
            dataRecord = fileReader.ReadLine();
            bool IsfirstSkuvalue = false;
            int lastproductId = 0;
            string firstretailPrice;
            while (dataRecord != null)
            {
                indexRowCount++;

                if (ErrorCount >= MaxErrorCount)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Upload Products: Maximum error count reached. Please see the log details for additional information.");
                    ltrlError.Text = "Uploads failed, Please see the activity log for more information.";                    
                    return;
                }

                string[] data = dataRecord.Split('|');

                if (data.Length !=9)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(string.Format("Upload Products: There are input fields missing in row:{0}", indexRowCount));
                    ErrorCount++;

                    dataRecord = fileReader.ReadLine();
                    continue;
                }  
                try
                { 

                    ZNodePricingDataSet.ZNodeProductRow dr = impPricing.ZNodeProduct.NewZNodeProductRow();
                    if (int.TryParse(data[0], out parsedInt)) 
                    { 
                        dr.ProductID = parsedInt;
                        if (lastproductId != dr.ProductID)
                        {
                            lastproductId = dr.ProductID;
                            IsfirstSkuvalue = true;
                        }
                        else
                        {
                            IsfirstSkuvalue = false;
                        } 
                       
                    }
                    
                    if (int.TryParse(data[1], out parsedInt)) 
                    { 
                        dr.SKUId = parsedInt;
                    }
                    
                    if (int.TryParse(data[2], out parsedInt)) 
                    { 
                        dr.AddOnvalueId = parsedInt; 
                    }

                    dr.Name = data[3].ToString();
                    dr.ProductNum = data[4];
                    dr.Sku = data[5];


                    if (IsfirstSkuvalue == true)
                    {
                        firstretailPrice = data[6].ToString();
                        if (firstretailPrice != string.Empty)
                        {
                            if (decimal.TryParse(data[6], out parsedDec))
                            {
                                dr.RetailPrice = parsedDec;
                            }
                        }
                        else
                        { 
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Upload Products: Maximum error count reached. Please see the log details for additional information.");
                            lincenoCount = indexRowCount + 1;
                            ltrlError.Text = "Retail price should not be empty on line number : " + lincenoCount +" for the productid : " + lastproductId;
                            return;
                        }

                    }
                    else
                    {
                        if (decimal.TryParse(data[6], out parsedDec))
                        {
                            dr.RetailPrice = parsedDec;
                        }
                    } 
                    
                    if (decimal.TryParse(data[7], out parsedDec)) 
                    { 
                        dr.SalePrice = parsedDec; 
                    }
                    
                    if (decimal.TryParse(data[8], out parsedDec)) 
                    { 
                        dr.WholesalePrice = parsedDec; 
                    }
                    dr.IsfirstSku = IsfirstSkuvalue;
                    impPricing.ZNodeProduct.AddZNodeProductRow(dr);
                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                    ErrorCount++;
                }
                
                dataRecord = fileReader.ReadLine();
            }

            if (impPricing.Tables[0].Rows.Count > 0)
            {
                DataManagerAdmin managerAdmin = new DataManagerAdmin();
                status = managerAdmin.UploadPricing(impPricing.ZNodeProduct, out SqlErrorCount);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Exported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Upload Pricing", "Pricing");
            }

            if ((ErrorCount > 0 || SqlErrorCount > 0) && status)
            {
                ltrlError.Text = "Upload failed, Please see the activity log for more information.";                
                return;
            }

            if (!status)
            {
                ltrlError.Text = "Upload failed, Please see the activity log for more information.";
            }
            else
            {
                Response.Redirect("~/SiteAdmin/Secure/Inventory/ImportExportData/Default.aspx");
            }
        }

        /// <summary>
        /// Event fired when Cancel button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.DefaultPageLink);
        }
        #endregion
    }
}