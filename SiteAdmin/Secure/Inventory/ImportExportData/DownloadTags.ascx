<%@ Control Language="C#" AutoEventWireup="True"
    Inherits="SiteAdmin.Secure.Inventory.ImportExportData.DownloadTags" CodeBehind="DownloadTags.ascx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="CategoryAutoComplete" Src="~/SiteAdmin/Controls/Default/CategoryAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<div class="Form">
    <h1>Download Facets</h1>
    <div>
        <ZNode:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
    <div class="SearchForm">
        <div class="RowStyle">
            <div class="ItemStyle">
                <div class="FieldStyle">
                    Catalog
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlCatalog" AutoPostBack="true" OnSelectedIndexChanged="Ddlcatalog_OnSelectedIndexChanged"
                        runat="server">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="RowStyle">
            <div class="ItemStyle">
                <div class="FieldStyle">
                    Category Name
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="lstCategory" AutoPostBack="true" runat="server" OnSelectedIndexChanged="LstCategory_OnSelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer5" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div class="RowStyle">
            <div class="ItemStyle">
                <div class="FieldStyle">
                    File Type
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlFileSaveType" runat="server">
                        <asp:ListItem Text="Microsoft Excel (.xls)" Value=".xls"></asp:ListItem>
                        <asp:ListItem Text="Delimited File Format" Value=".csv" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer2" SpacerHeight="1" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div class="ClearBoth">
            <div class="Error">
                <asp:Literal ID="ltrlError" runat="server"></asp:Literal>
            </div>
            <div>
                <a href="../../../Data/Default/Logs/ZnodeLog.txt" id="lnkLog" runat="server"
                    visible="false" target="_blank">Click here to view log >></a>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer4" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div class="ClearBoth">
            </div>
            <div class="ImageButtons">
                <asp:Button ID="btnDownloadTag" runat="server" Text="Download Facet Data" OnClick="BtnDownloadTagInventory_Click" />
                <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
            </div>
        </div>
    </div>
</div>
