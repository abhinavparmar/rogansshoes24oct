﻿using System;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    public partial class EditAddress : CommonPageBase
    {
        protected override void Page_PreInit(object sender, EventArgs e)
        {
            // If request comes from mobile then load the theme from ZNodePortal.MobileTheme
            if (Session["cs"] != null && ZNodeConfigManager.SiteConfig.MobileTheme != null)
            {
                this.MasterPageFile = "~/themes/" + ZNodeConfigManager.SiteConfig.MobileTheme + "/MasterPages/EditAddress.master";
            }
            else
            {
                // Clear the current category session theme if some other category selected.
                Session["CurrentCategoryTheme"] = null;

                this.MasterPageFile = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/MasterPages/EditAddress.master";
            }           
        }
    }
}
