<%@ Application Language="C#" Inherits="System.Web.HttpApplication" %>

<script RunAt="server">
    void Application_Start(object sender, EventArgs e)
    {
        //Check database connection
        try
        {
            //Create Instance of Connection and Command Object
            System.Data.SqlClient.SqlConnection myConnection = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString);

            //Open Connection
            myConnection.Open();

            //Close Connection
            myConnection.Close();

            //Release resources
            myConnection.Dispose();

        }
        catch (Exception SQLException)
        {
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Could not connect to the database. Additional Information: " + SQLException.Message);
            throw (new ApplicationException("Could not connect to the database. Additional Information: " + SQLException.Message));
        }

        //Check permissions on data folders
        try
        {
            System.IO.FileInfo file = new System.IO.FileInfo(Server.MapPath("~/Data/Default/") + "test.txt");
            System.IO.FileStream filestream = file.Create();
            filestream.Close();
            file.Delete();
        }
        catch (Exception exception)
        {
            throw (new ApplicationException("Could not Read/Write to the Data folder. Please check the permissions on this folder. Additional Information: " + exception.Message));
        }

        //Cache promotions
        try
        {
            ZNode.Libraries.ECommerce.Promotions.ZNodePromotionOption.CacheActivePromotions();
        }
        catch (Exception exception)
        {
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Could not cache promotions. Additional Information: " + exception.Message);
        }

        //Configure MVC admin plugins
        try
        {
            var isXComerceAdminAssemblyFound = AppDomain.CurrentDomain.GetAssemblies().Any(assembly => assembly.GetName().Name.Equals("X-ComAdmin"));
            if (isXComerceAdminAssemblyFound)
            {
                var xcomadmin = AppDomain.CurrentDomain.GetAssemblies().Single(
                    assembly => assembly.GetName().Name.Equals("X-ComAdmin"));

                var type = xcomadmin.GetType("X_ComAdmin.Infrastructure.Bootstrapper");

                //type.GetMethod("Bootstrap").Invoke(null, null);

            }
        }
        catch (Exception exception)
        {
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Could not initialize MVC plugins. Additional Information: " + exception.Message);
        }

        // Log that our application hast started.
        try
        {
            // Log our application start. Later we will log the time it takes to start the appliction.
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity(2000);
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Application Start");
        }
        catch (Exception exception)
        {
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Could not log the application start. Additional Information: " + exception.Message);
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Application_PreRequestHandlerExecute(Object sender, EventArgs e)
    {        
        // This call must be placed in the Pre-Request Handler for order desk to work.
        if (Request.Path.IndexOf("OrderDesk.aspx", StringComparison.OrdinalIgnoreCase) < 0
            && HttpContext.Current.Session != null)
        {
            if (Request.Path.IndexOf("OrderDeskReceipt.aspx", StringComparison.OrdinalIgnoreCase) < 0)
                ZNode.Libraries.Framework.Business.ZNodeConfigManager.UnAliasSiteConfig();
        }

        // 
        if (HttpContext.Current.Session != null)
        {
            // Set culture settings
            ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.SetPageCulture();


            // This call must be placed to remove session object used for category and search page.
            if (Request.Path.IndexOf("Search.aspx", StringComparison.OrdinalIgnoreCase) < 0 &&
                Request.Path.IndexOf("Category.aspx", StringComparison.OrdinalIgnoreCase) < 0 &&
                Request.Path.IndexOf("default.aspx", StringComparison.OrdinalIgnoreCase) < 0 &&
                 Request.Path.IndexOf("quickwatch.aspx", StringComparison.OrdinalIgnoreCase) < 0)
                HttpContext.Current.Session.Remove("ProductList");
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Application_BeginRequest(Object sender, EventArgs e)
    {
        // Set the store config for the URL that has been requested. This call must be placed in the Begin Request for Multi Stores to work.
        if (ZNode.Libraries.Framework.Business.ZNodeConfigManager.CheckSiteConfigCache() == false)
        {
            if (ZNode.Libraries.Framework.Business.ZNodeConfigManager.SetSiteConfig() == false)
            {
                // The URL was not found in our config. Send out a 404 error.
                System.Web.HttpContext.Current.Response.StatusCode = 404;
                System.Web.HttpContext.Current.Response.SuppressContent = true;
                System.Web.HttpContext.Current.ApplicationInstance.CompleteRequest();
                return;
            }
        }

        // Check that DomainConfig Exists
        if (ZNode.Libraries.Framework.Business.ZNodeConfigManager.DomainConfig != null)
        {
            // Check the Url, if its disabled then return 404 error page. 
            if (ZNode.Libraries.Framework.Business.ZNodeConfigManager.DomainConfig.IsActive == false)
            {
                // The URL was not found in our config. Send out a 404 error.
                System.Web.HttpContext.Current.Response.StatusCode = 404;
                System.Web.HttpContext.Current.Response.SuppressContent = true;
                System.Web.HttpContext.Current.ApplicationInstance.CompleteRequest();
                return;
            }
        }
        
        //Check that Message Config Exists in Cache
        if (this.CheckSiteMessageConfigCache() == false)
        {
            ZNode.Libraries.Admin.MessageConfigAdmin messageConfigAdmin = new ZNode.Libraries.Admin.MessageConfigAdmin();
            messageConfigAdmin.ClearMessageConfigCache();
            messageConfigAdmin.LoadMessageConfigDataInCache();
        }

        string newPath = string.Empty;
        // This section rewrites product/cateogy/content path
        try
        {
            ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.SetLocaleCode();

            string currentURL = Request.Path.ToLower();

            // Only rewrite paths to our ASPX pages, not any internal links.

            #region[commented code : remove condition if (currentURL.Contains(".aspx")) with    if (IsURLRewriteRequired(currentURL) == true)]
            //if (currentURL.Contains(".aspx"))
            #endregion

            if (IsURLRewriteRequired(currentURL) == true)
            {
                if (currentURL.IndexOf(".aspx") != currentURL.LastIndexOf(".aspx"))
                {
                    // Rewrites the URL, if .aspx repeats more than once.
                    HttpContext.Current.RewritePath(Request.Path);
                }
                else if (ZNode.Libraries.ECommerce.SEO.ZNodeSEOUrl.LocaleID > 0)
                {
                    int localeID = ZNode.Libraries.ECommerce.SEO.ZNodeSEOUrl.LocaleID;

                    //Zeon Customization for SEOUrl Rewrite to remove .aspx extension & handle URL format "C1/C2" rule #start

                    //string replacePath = ZNode.Libraries.ECommerce.SEO.ZNodeSEOUrl.RewriteUrlPath(currentURL, out localeID);

                    string replacePath = ZNode.Libraries.ECommerce.SEO.ZNodeSEOUrl.RewriteUrlPathExtn(currentURL, ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.PortalID, out localeID);

                    //Zeon Customization for SEOUrl Rewrite to remove .aspx extension & handle URL format "C1/C2" rule #end

                    if (localeID != ZNode.Libraries.ECommerce.SEO.ZNodeSEOUrl.LocaleID)
                    {
                        newPath = string.Format("~/default.aspx?nlid={0}&redir={1}&{2}", localeID, HttpUtility.UrlEncode(replacePath), Request.QueryString.ToString());
                    }

                    if (replacePath.Length > 0 && newPath.Length == 0)
                    {
                        // Mark this URL as being found.
                        HttpContext.Current.Items.Add("SeoUrlFound", "1");

                        // Get query string
                        string queryString = Request.Url.Query.Replace("?", "&");

                        // Add it back to the URL and rewrite the requiest.
                        HttpContext.Current.RewritePath(replacePath + queryString);
                    }
                    else if (currentURL.ToLower().Contains("/login.aspx"))
                    {
                        //redirect depending on the returnUrl?
                        string returnUrl = Request.QueryString["returnurl"];
                        //Added Null check to solve Extra Log Error Issue On 2 April 2015
                        if (!string.IsNullOrWhiteSpace(returnUrl))
                        {
                            if (returnUrl.ToLower().Contains("/siteadmin"))
                            {
                                Response.Redirect(string.Format("~/siteadmin/default.aspx?ReturnUrl={0}", HttpUtility.UrlEncode(returnUrl)));
                            }
                            if (returnUrl.ToLower().Contains("/franchiseadmin"))
                            {
                                Response.Redirect(string.Format("~/franchiseadmin/default.aspx?ReturnUrl={0}", HttpUtility.UrlEncode(returnUrl)));
                            }
                            if (returnUrl.ToLower().Contains("/malladmin"))
                            {
                                Response.Redirect(string.Format("~/MallAdmin/default.aspx?ReturnUrl={0}", HttpUtility.UrlEncode(returnUrl)));
                            }
                        }
                    }
                }
            }
        }
        catch (Exception exception)
        {
            //Log Seo URl related excepion in the file for debugging
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage
                        ("Could not re-write URL path for SEO friendly URLs. Additional Information: Request Path:" + Request.Path +"Detail:" + exception.StackTrace);
        }

        if (newPath.Length > 0)
            Response.Redirect(newPath, true);
    }

    void Application_End(object sender, EventArgs e)
    {
        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity(2001);
    }

    void Application_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();

        if (ex is HttpRequestValidationException)
        {
            Response.Clear();
            Response.StatusCode = 200;
            StringBuilder outputCode = new StringBuilder();
            outputCode.Append("&lt;html&gt;&lt;head&gt;&lt;title&gt;HTML Not Allowed&lt;/title&gt; ");
            outputCode.Append(" &lt;script language='JavaScript'&gt; ");
            outputCode.Append(" function back() { history.go(-1); } ");
            outputCode.Append(@"&lt;/script&gt; &lt;/head&gt;&lt;body style='font-family: Arial, Sans-serif;'&gt; ");
            outputCode.Append("&lt;h1&gt;Oops!&lt;/h1&gt; ");
            outputCode.Append("&lt;p&gt;We are sorry but you have entered invalid characters.&lt;/p&gt; ");
            outputCode.Append("&lt;p&gt;Please correct your entry and try again.");
            outputCode.Append("&lt;p&gt;&lt;a href='javascript:back()'&gt;Go back&lt;/a&gt;&lt;/p&gt; ");
            outputCode.Append("&lt;/body&gt;&lt;/html&gt;");
            Response.Write(HttpUtility.HtmlDecode(outputCode.ToString()));
            Response.End();
        }
    }

    void Session_Start(object sender, EventArgs e)
    {
        // Set culture settings
        ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.SetPageCulture();


        // Cache currency settings
        try
        {
            ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.CacheCurrencySetting();
        }
        catch (Exception exception)
        {
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Could not cache currency settings. Additional Information: " + exception.StackTrace);
        }

        // Make sure the Profile cache exists and is cleared.
        HttpContext.Current.Session.Add("ProfileCache", null);

        try
        {
            // Initialize the tracking information.
            ZNode.Libraries.ECommerce.Analytics.ZNodeTracking tracker = new ZNode.Libraries.ECommerce.Analytics.ZNodeTracking();
            tracker.LogTrackingEvent("Entering Site");
        }
        catch (Exception exception)
        {
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Affiliate Tracking could not be initialized. Additional Information: " + exception.StackTrace);
        }

        // Ignore SavedCart when admin are logins/using the iste
        string currentURL = Request.Path.ToLower();
        if (!(currentURL.Contains("/siteadmin/") || currentURL.Contains("/malladmin/") || currentURL.Contains("/franchiseadmin/")))
        {
            // Restore PersistentCart if any
            ZNode.Libraries.ECommerce.ShoppingCart.ZNodeSavedCart savedCart = new ZNode.Libraries.ECommerce.ShoppingCart.ZNodeSavedCart();
            savedCart.InitializeShoppingCart();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void Session_End(object sender, EventArgs e)
    {
        if (HttpContext.Current != null)
        {
            // Remove session
            HttpContext.Current.Session.Remove("ProfileCache");

            // Remove session
            HttpContext.Current.Session.Remove("ZnodeMultifrontRefer");

            HttpContext.Current.Session.Remove("ProductList");
        }
    }

    #region Zeon Custom methods

    /// <summary>
    /// Compressing data before sending over network saves time required to send and receive data and reduces network traffic. There is a down side to compression that this will increase the processing of the sending and receiving system, but this processing is very negligible when compared the with the time required to send the data. 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Application_PostReleaseRequestState(object sender, EventArgs e)
    {
        string contentType = Response.ContentType;
        if (!Request.Path.Contains(".axd"))
        {
            if (contentType == "text/css" || contentType == "application/x-javascript" || contentType == "text/javascript")
            {
                Response.Cache.VaryByHeaders["Accept-Encoding"] = true;
                string acceptEncoding = Request.Headers["Accept-Encoding"];
                if (!string.IsNullOrEmpty(acceptEncoding))
                {
                    HttpApplication app = sender as HttpApplication;
                    System.IO.Stream prevUncompressedStream = app.Response.Filter;
                    if (acceptEncoding.Contains("gzip"))
                    {
                        // gzip
                        app.Response.Filter = new System.IO.Compression.GZipStream(prevUncompressedStream, System.IO.Compression.CompressionMode.Compress);
                        app.Response.AppendHeader("Content-Encoding", "gzip");
                    }
                    else if (acceptEncoding.Contains("deflate") || acceptEncoding == "*")
                    {
                        // defalte
                        app.Response.Filter = new System.IO.Compression.DeflateStream(prevUncompressedStream, System.IO.Compression.CompressionMode.Compress);
                        app.Response.AppendHeader("Content-Encoding", "deflate");
                    }
                    app.Response.Cache.SetExpires(DateTime.Now.AddMonths(3));
                    app.Response.Cache.SetCacheability(HttpCacheability.Public);
                    app.Response.Cache.SetMaxAge(new TimeSpan(90, 0, 0));
                }
            }
        }
    }

    /// <summary>
    /// Check Message Config Data is exists in site admin
    /// </summary>
    /// <returns></returns>
    private bool CheckSiteMessageConfigCache()
    {
        //Zeon Custom Code: Start
        bool checkSiteMessageConfigCache = false;
        try
        {
            ZNode.Libraries.Admin.MessageConfigAdmin messageConfigAdmin = new ZNode.Libraries.Admin.MessageConfigAdmin();
            checkSiteMessageConfigCache = messageConfigAdmin.CheckSiteMessageConfigCache();
        }
        catch (Exception exception)
        {
            //ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Could not cache messageconfig data. Additional Information: " + exception.StackTrace); //Comment 6 April 2015          
            checkSiteMessageConfigCache = false;
        }
        return checkSiteMessageConfigCache;
        //Zeon Custom Code:End
    }  
    
    #endregion

    #region[Zeon Custom Code :: Helper Methods]

    /// <summary>
    /// Check Url is Rewite Required Or Not
    /// </summary>
    /// <param name="currentURL"></param>
    /// <returns></returns>
    private bool IsURLRewriteRequired(string currentURL)
    {
        bool bRewriteRequired = true;
        if (!string.IsNullOrEmpty(currentURL))
        {
            //do not execute seo rewrite for static pages like .axd, .css etc
            if (currentURL.Contains(".axd") ||
                currentURL.Contains(".js") ||
                currentURL.Contains(".css") ||
                currentURL.Contains(".jpg") ||
                currentURL.Contains(".jpeg") ||
                currentURL.Contains(".png") ||
                currentURL.Contains(".gif") ||
                currentURL.Contains(".tif") ||
                currentURL.Contains(".ico"))
            {
                bRewriteRequired = false;
            }
        }
        return bRewriteRequired;
    }
    
    #endregion
       
</script>

