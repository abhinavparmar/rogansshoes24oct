﻿using System;
using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace WebApp.Controls.CheerandPom.Brand
{
    public partial class ZHomeAllBrands : System.Web.UI.UserControl
    {
        #region Public Properties

        public int MaxBrandsCount { get; set; }

        #endregion

        #region Page Events
        /// <summary>
        /// Handeles Page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            BindAllBrands();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Bind All Brands
        /// </summary>
        private void BindAllBrands()
        {
            DataTable manufacturerList = new DataTable();
            manufacturerList = (DataTable)System.Web.HttpRuntime.Cache["HomeShopByBrands" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId];
            if (manufacturerList == null)
            {
                manufacturerList = new ManufacturerHelper().GetActiveBrandsByPortal(ZNodeConfigManager.SiteConfig.PortalID, ZNodeConfigManager.SiteConfig.LocaleID, MaxBrandsCount);
                //Insert into cache
                ZNodeCacheDependencyManager.Insert("HomeShopByBrands" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId,
                                            manufacturerList,
                                             DateTime.Now.AddHours(12),
                                             System.Web.Caching.Cache.NoSlidingExpiration,
                                             null);
            }
            if (manufacturerList != null && manufacturerList.Rows.Count > 0)
            {
                dlAllBrands.DataSource = manufacturerList;
                dlAllBrands.DataBind();
            }
            else
            {
                this.Visible = false;
            }
        }

        /// <summary>
        /// Get Image Path
        /// </summary>
        /// <param name="imageName">string</param>
        /// <returns>string</returns>
        protected string GetImagePath(string imageName)
        {
            return new ZNodeImage().GetImageHttpPathCrossSell(imageName);
        }

        /// <summary>
        /// Get Brands Navigate URL
        /// </summary>
        /// <param name="catgryID">string</param>
        /// <param name="seoURL">string</param>
        /// <returns></returns>
        protected string GetBrandURL(string catgryID, string seoURL)
        {
            string navigateURL = string.Empty;
            if (!string.IsNullOrEmpty(seoURL))
            {
                navigateURL = seoURL;
            }
            else
            {
                navigateURL = !string.IsNullOrEmpty(catgryID) ? ResolveUrl("~/category.aspx?zcid=" + catgryID) :"javascript: void(0)";
            }
            return navigateURL;
        }

        #endregion

    }
}