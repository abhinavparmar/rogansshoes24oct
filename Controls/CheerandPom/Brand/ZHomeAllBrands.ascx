﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ZHomeAllBrands.ascx.cs" Inherits="WebApp.Controls.Default.Brand.ZHomeAllBrands" %>
<%@ OutputCache Duration="21600" VaryByParam="none" %>
<div>
    <h2 class="media-heading">
        <span>
            <asp:Literal ID="ltrshopByBrand" meta:resourcekey="ltrshopByBrandres" runat="server" EnableViewState="false"></asp:Literal>
            <a href="shopbybrand.aspx"><em class="RedText">
                <asp:Literal ID="ltrSeeAllBrands" meta:resourcekey="ltrSeeAllBrandsres" runat="server" EnableViewState="false"></asp:Literal></em></a>
        </span>
    </h2>
    <div class="BestSeller">
        <ul>
            <asp:Repeater ID="dlAllBrands" runat="server" EnableViewState="false" ClientIDMode="Static">
                <ItemTemplate>
                    <li><a href='<%# GetBrandURL(DataBinder.Eval(Container.DataItem, "CategoryID").ToString(),DataBinder.Eval(Container.DataItem, "SEOURL").ToString())%>'>
                        <img src="images/grey1px.gif" class="lazy" border="0" data-original='<%# ResolveUrl(GetImagePath(DataBinder.Eval(Container.DataItem, "Image").ToString()))%>' alt='<%# DataBinder.Eval(Container.DataItem, "Name").ToString()%>' title='<%# DataBinder.Eval(Container.DataItem, "Name").ToString()%>' style="display:inline-block;float:left;margin:5px"
                             onerror="this.onerror=null; this.src='Data/Default/Images/Catalog/97/noimage.gif';" /></a></li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>
</div>