﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApp.Controls.Default.Product;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;


namespace WebApp.Controls.CheerandPom.Product
{
    public partial class ZProductSkuImages : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProduct product;
        private ZNodeSKU selectedSKU = new ZNodeSKU();
        private string defaultAttributeType = string.Empty;
        private bool isValid = false;
        private int bundleProductID = 0;
        private bool isQuickWatch = false;
        int skuID = 0;
        private string sAttributeTypeComboInitial = "lstAttribute";

        #endregion



        #region Public Properties
        public string DefaultAttributeType
        {
            get
            {
                return this.defaultAttributeType;
            }

            set
            {
                this.defaultAttributeType = value;
            }
        }

        /// <summary>
        /// get or set values of _SkuID
        /// </summary>
        public int SKUID
        {
            get
            {
                GetSKUID();
                return this.skuID;
            }

            set
            {
                this.skuID = value;
            }
        }

        /// <summary>
        /// get or set default SKUID
        /// </summary>
        public int DefaultSKU { get; set; }

        public bool IsLastAttributeSelectedIndexChanged { get; set; }

        /// <summary>
        /// get or set color is last attribute
        /// </summary>
        public bool IsColorIsLastAttribute { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether QuickWatch is enabled or not
        /// </summary>
        public bool IsQuickWatch
        {
            get
            {
                return this.isQuickWatch;
            }

            set
            {
                this.isQuickWatch = value;
            }
        }

        #endregion

        #region Bind Data

        /// <summary>
        /// Bind control display based on properties set
        /// </summary>
        public void Bind()
        {
            if (this.product.ZNodeAttributeTypeCollection.Count > 0)
            {
                int counter = 1;
                int sizeCounter = 1;
                pnlOptions.Visible = true;
                ControlPlaceHolder.Controls.Add(new LiteralControl("<div>"));

                foreach (ZNodeAttributeType AttributeType in this.product.ZNodeAttributeTypeCollection)
                {
                    //Zeon Custom Code:Starts
                    if (AttributeType.Name.ToString().ToLower().Contains("color"))
                    {
                        this.BindColorAttribute(AttributeType);
                    }

                    sizeCounter++;
                }
                ControlPlaceHolder.Controls.Add(new LiteralControl("</div>"));
            }
            else
            {
                pnlOptions.Visible = false;
                return;
            }
        }

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this.product = (ZNodeProduct)HttpContext.Current.Items["Product"];

            this.Bind();
            if (!IsPostBack)
            {
                this.LoadAlternateImageControl();
                this.LoadDefaultCatalogImage();
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            this.SetSelectedCssForColorAttribute();
        }

        #endregion

        #region Zeon Custom Methods

        #region  Protected Methods and  Events

        /// <summary>
        /// Color Attribute Selected Index Change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LstRadioControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            System.Web.UI.WebControls.RadioButtonList radiolist = (System.Web.UI.WebControls.RadioButtonList)sender;
            System.Web.UI.WebControls.Label lblSelColor = (System.Web.UI.WebControls.Label)ControlPlaceHolder.FindControl("lblSelectdColor");
            string selectedColor = radiolist.SelectedItem.Attributes["color"].ToString();
            if (lblSelColor != null)
            {
                lblSelColor.Text = radiolist.SelectedItem.Attributes["color"].ToString();
            }
            this.LoadAlternateImageControl();
            this.LoadDefaultCatalogImage();
        }


        #endregion

        #region Private Methods

        /// <summary>
        /// Get SKU Image Text
        /// </summary>
        /// <param name="AttributeId"></param>
        /// <param name="skuID"></param>
        /// <returns></returns>
        private string GetSKUImageText(int AttributeId, out string skuID)
        {
            skuID = string.Empty;
            ZNodeImage znodeImage = new ZNodeImage();
            string skuImagePath = string.Empty;
            string imageHTML = string.Empty;
            if (!IsQuickWatch)
            {
                imageHTML = "<img class='SwatchImage' src='{0}' alt='{2}' title='{3}'  onclick=\"{1}\"/>";
            }
            else
            {
                imageHTML = "<img class='SwatchImage' src='{0}' alt='{2}' title='{3}' onclick=\"document.getElementById('CatalogItemImage').src='{1}';\"/>";
            }

            string productImage = string.Empty;
            if (this.product.ProductAllAttributeCollection != null && this.product.ProductAllAttributeCollection.Rows.Count > 0)
            {
                foreach (DataRow row in this.product.ProductAllAttributeCollection.Rows)
                {
                    if (row["AttributeID"].ToString() == AttributeId.ToString() && !string.IsNullOrEmpty(row["SkuPicturePath"].ToString()))
                    {
                        skuImagePath = ResolveUrl(znodeImage.GetImageHttpPathThumbnail(row["SkuPicturePath"].ToString()));
                        skuImagePath = skuImagePath.Replace("-swatch.", ".");
                        productImage = ResolveUrl(znodeImage.GetImageHttpPathMedium(row["SkuPicturePath"].ToString()));
                        //Create Large Image
                        znodeImage.GetImageHttpPathLarge(row["SkuPicturePath"].ToString());
                        if (!IsQuickWatch)
                        {
                            skuImagePath = string.Format(imageHTML, skuImagePath, "objProduct.UpdateAlternateImage('" + productImage + "')", productImage, productImage);
                        }
                        else
                        {
                            skuImagePath = string.Format(imageHTML, skuImagePath, productImage, productImage, productImage);
                        }
                        skuID = row["SkuId"].ToString();
                        break;
                    }
                }
            }
            return skuImagePath;
        }

        /// <summary>
        /// GET SKUID
        /// </summary>
        private void GetSKUID()
        {
            int selSku = 0;
            foreach (ZNodeAttributeType AttributeType in this.product.ZNodeAttributeTypeCollection)
            {
                if (AttributeType.Name.ToString().ToLower().Contains("color"))
                {
                    System.Web.UI.WebControls.RadioButtonList lstControl = (System.Web.UI.WebControls.RadioButtonList)ControlPlaceHolder.FindControl("lstAttribute" + AttributeType.AttributeTypeId.ToString());
                    if (lstControl != null && lstControl.SelectedItem.Attributes["SKU"] != null)
                    {
                        selSku = int.Parse(lstControl.SelectedItem.Attributes["SKU"].ToString());
                        //Set Default SKU
                        this.DefaultSKU = int.Parse(lstControl.Items[0].Attributes["SKU"].ToString());
                        this.SKUID = selSku;
                    }
                }
            }
        }

        /// <summary>
        /// load alternate image control
        /// </summary>
        private void LoadAlternateImageControl()
        {
            ZProductAlternateImages alternateImage = (ZProductAlternateImages)this.Parent.FindControl("uxAternateImages");
            UpdatePanel updatepnl = (UpdatePanel)this.Parent.FindControl("upnlAlternateImage");
            if (alternateImage != null)
            {
                //set default SKU
                int skuId = this.SKUID;
                alternateImage.DefaultSKU = DefaultSKU;
                alternateImage.SKUID = skuId;
                if (updatepnl != null)
                {
                    updatepnl.Update();
                }
            }
        }

        /// <summary>
        /// Load Default Catalog Image
        /// </summary>
        private void LoadDefaultCatalogImage()
        {
            Controls_Default_Product_CatalogImage znodecatlogImage = (Controls_Default_Product_CatalogImage)this.Parent.FindControl("CatalogItemImage");
            UpdatePanel upCatalogImage = (UpdatePanel)this.Parent.FindControl("upCatalogImage");
            if (znodecatlogImage != null)
            {
                znodecatlogImage.SkuID = this.SKUID;
                if (upCatalogImage != null)
                {
                    upCatalogImage.Update();
                }
            }
        }

        /// <summary>
        /// Bind Color Attribute Data
        /// </summary>
        /// <param name="AttributeType"></param>
        private void BindColorAttribute(ZNodeAttributeType AttributeType)
        {
            if (AttributeType != null && AttributeType.ZNodeAttributeCollection != null && AttributeType.ZNodeAttributeCollection.Count > 0)
            {
                System.Web.UI.WebControls.RadioButtonList lstRadioControl = new RadioButtonList();
                lstRadioControl.ID = "lstAttribute" + AttributeType.AttributeTypeId.ToString();
                lstRadioControl.AutoPostBack = true;
                lstRadioControl.CssClass = "AttributeRadio";
                lstRadioControl.RepeatDirection = RepeatDirection.Vertical;
                lstRadioControl.RepeatLayout = RepeatLayout.UnorderedList;
                //lstRadioControl.RepeatColumns = 4;

                lstRadioControl.Attributes.Add("data-attrtypeid", AttributeType.AttributeTypeId.ToString());

                this.DefaultAttributeType = lstRadioControl.ID;
                lstRadioControl.SelectedIndexChanged += new EventHandler(this.LstRadioControl_SelectedIndexChanged);

                int defaultSelectedColor = AttributeType.ZNodeAttributeCollection[0].AttributeId;
                foreach (ZNodeAttribute Attribute in AttributeType.ZNodeAttributeCollection)
                {
                    ListItem li1 = new ListItem();
                    string skuID = string.Empty;
                    string listItemText = GetSKUImageText(Attribute.AttributeId, out skuID);
                    if (!string.IsNullOrEmpty(listItemText))
                    {
                        li1.Attributes.Add("color", Attribute.Name);
                        li1.Attributes.Add("SKU", skuID);
                        li1.Text = listItemText;
                        li1.Value = Attribute.AttributeId.ToString();
                    }
                    else
                    {
                        li1.Text = Attribute.Name;
                        li1.Value = Attribute.AttributeId.ToString();
                    }
                    li1.Attributes.Add("class", "item ItemColor");

                    li1.Attributes.Add("title", "" + Attribute.Name + "");
                    lstRadioControl.Items.Add(li1);

                }
                lstRadioControl.SelectedValue = defaultSelectedColor.ToString(); ;
                if (!AttributeType.IsPrivate)
                {
                    //Creating Div Structure
                    Literal ltlDiv = new Literal();
                    ltlDiv.Text = "<div style='float:left;'>" + AttributeType.Name + " - </div><div>";
                    ControlPlaceHolder.Controls.Add(ltlDiv);

                    System.Web.UI.WebControls.Label lblSelColors = new Label();
                    lblSelColors.ID = "lblSelectdColor";
                    lblSelColors.CssClass = "SelectedColor";
                    lblSelColors.Text = AttributeType.ZNodeAttributeCollection[0].Name;
                    ControlPlaceHolder.Controls.Add(lblSelColors);
                    Literal ltlDivColor = new Literal();
                    ltlDivColor.Text = "</div>";
                    ControlPlaceHolder.Controls.Add(ltlDivColor);

                    Literal ltlDivRadioStart = new Literal();
                    ltlDivRadioStart.Text = "<div ID='divRadioList'>";
                    ControlPlaceHolder.Controls.Add(ltlDivRadioStart);

                    ControlPlaceHolder.Controls.Add(lstRadioControl);

                    ControlPlaceHolder.Controls.Add(new LiteralControl("</div>"));

                    //Creating Div Structure
                }
                IsColorIsLastAttribute = product.ZNodeAttributeTypeCollection != null && product.ZNodeAttributeTypeCollection.Count <= 1 ? true : false;
            }
        }

        /// <summary>
        /// Set Selected CSS
        /// </summary>
        private void SetSelectedCssForColorAttribute()
        {
            var colorAttributeList = this.product.ZNodeAttributeTypeCollection.OfType<ZNodeAttributeType>().AsQueryable().Where(o => o.Name.Equals("color", StringComparison.OrdinalIgnoreCase)).ToList();
            if (colorAttributeList != null && colorAttributeList.Count > 0)
            {
                System.Web.UI.WebControls.RadioButtonList lstControl = (System.Web.UI.WebControls.RadioButtonList)ControlPlaceHolder.FindControl("lstAttribute" + colorAttributeList[0].AttributeTypeId.ToString());
                if (lstControl != null)
                {
                    foreach (ListItem li in lstControl.Items)
                    {
                        if (li.Selected)
                        {
                            li.Attributes.Add("class", "item ItemColor Selected");
                            break;
                        }
                    }
                }
            }
        }

        #endregion

        #endregion
    }
}