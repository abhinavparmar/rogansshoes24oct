﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ZTopSeller.ascx.cs" Inherits="WebApp.Controls.Default.Specials.ZTopSeller" EnableViewState="false" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Default/Reviews/ProductAverageRating.ascx" TagName="ProductAverageRating"
    TagPrefix="ZNode" %>

<asp:Panel ID="pnlProductList" runat="server" Visible="true">
    <div id="TopSellers">
        <div class="Title">
            <asp:Literal ID="ltrTitle" runat="server" Text='<%$ Resources:CommonCaption, HomePageTopSellersTitle %>'></asp:Literal>
        </div>
            <div id="relProdTopSeller" runat="server"> 
                    <asp:Repeater ID="RepeaterProducts" EnableViewState="false" runat="server">
                        <ItemTemplate>
                            <div class="item">
                                <div class="SpecialItem">
                                    <div class="DetailLink">
                                        <asp:HyperLink ID="hlName" runat="server" CssClass='DetailLink' Text='<%# DataBinder.Eval(Container.DataItem, "Name").ToString()%>'
                                            NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()%>'></asp:HyperLink>
                                        <span class="rs-new" id="spnRSNew" runat="server" visible='<%# DataBinder.Eval(Container.DataItem, "NewProductInd") %>'><span class="rs-new-text">New</span></span>
                                        <!--<asp:Image ID="NewItemImage" ImageUrl='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("new.png") %>'
                                            runat="server" Visible='<%# DataBinder.Eval(Container.DataItem, "NewProductInd") %>' AlternateText="New" alt="New" />-->
                                        <span style="width: 1px;"></span>
                                        <span class="Price">
                                            <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FormattedPrice") %>'
                                                Visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && SpecialProfile.ShowPrice %>'></asp:Label>
                                            <asp:Image ID="FeaturedItemImage" runat="server" ImageUrl='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("sale.png") %>'
                                                Visible='<%# DataBinder.Eval(Container.DataItem, "FeaturedInd") %>' AlternateText="FeaturedItem" ToolTip="FeaturedItem" />
                                        </span>
                                    </div>
                                    <div class="ShortDescription">
                                        <asp:Label ID="ShortDescription" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ShortDescription").ToString() %>' />
                                    </div>
                                    <div class="Image" enableviewstate="false">
                                        <a id="A1" href='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()%>'
                                            runat="server">
                                            <img id="img1" name='<%# "img"+DataBinder.Eval(Container.DataItem, "ProductID")%>'
                                                border='0' alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag")%>' title='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag")%>' src='<%# DataBinder.Eval(Container.DataItem, "SmallImageFilePath")%>'
                                                runat="server" />
                                        </a>
                                    </div>
                                    <div class="CallForPrice">
                                        <asp:Label ID="uxCallForPricing" EnableViewState="false" runat="server" Text='<%# CheckForCallForPricing(DataBinder.Eval(Container.DataItem, "CallForPricing"), DataBinder.Eval(Container.DataItem, "CallMessage")) %>'
                                            CssClass="Price" />
                                    </div>
                                    <div class="StarRating">
                                        <ZNode:ProductAverageRating ID="uxProductAverageRating"
                                            ProductName='<%# DataBinder.Eval(Container.DataItem, "Name").ToString()%>'
                                            TotalReviews='<%# DataBinder.Eval(Container.DataItem, "TotalReviews") %>'
                                            ViewProductLink='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink")%>'
                                            ReviewRating='<%# DataBinder.Eval(Container.DataItem, "ReviewRating")%>' runat="server" />
                                    </div>
                                    <div style="padding-top: 7px;">
                                        <asp:HyperLink ID="hlView" CssClass="Button View" EnableViewState="false" Text="<%$ Resources:CommonCaption, View%>"
                                            runat="server" Visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && SpecialProfile.ShowAddToCart %>'
                                            NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink")%>'>&raquo;</asp:HyperLink>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
            </div>
    </div>
    <div class="horizontalline">&nbsp;</div>
</asp:Panel>