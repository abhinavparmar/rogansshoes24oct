﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace WebApp.Controls.Default.Specials
{
    public partial class ZTopSeller : System.Web.UI.UserControl
    {

        #region Private Variables
        private ZNodeProfile _SpecialProfile = new ZNodeProfile();
        private ZNodeProductList _ProductList;
        private string _Title = string.Empty;
        private string BuyImage = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/ViewAdmin.gif";
        private int _DisplayItem = 10;
        private int zcid = 0;
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the profile.
        /// </summary>
        public ZNodeProfile SpecialProfile
        {
            get { return this._SpecialProfile; }
            set { this._SpecialProfile = value; }
        }

        /// <summary>
        /// Gets or sets the Product list
        /// </summary>
        public ZNodeProductList ProductList
        {
            get
            {
                return this._ProductList;
            }

            set
            {
                this._ProductList = value;
            }
        }

        /// <summary>
        /// Gets or sets the title for this control
        /// </summary>
        public string Title
        {
            get
            {
                return this._Title;
            }

            set
            {
                this._Title = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of items to be displayed
        /// </summary>
        public int DisplayItem
        {
            get
            {
                return this._DisplayItem;
            }

            set
            {
                this._DisplayItem = value;
            }
        }
        #endregion

        #region Page Load
       
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this._ProductList = ZNodeProductList.GetBestSellers(ZNodeCatalogManager.CatalogConfig.CatalogID, this.DisplayItem, this.zcid, ZNodeConfigManager.SiteConfig.PortalID);
                this.BindProducts();
            }
        }

        /// <summary>
        /// Page Prerender event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            RegisterClientScriptClasses();
        }

        #endregion

        #region Bind Data
        /// <summary>
        /// Bind display based on a product list
        /// </summary>
        public void BindProducts()
        {
            if (this._ProductList.ZNodeProductCollection.Count == 0)
            {
                pnlProductList.Visible = false;
            }
            else
            {
                pnlProductList.Visible = true;

                RepeaterProducts.DataSource = this._ProductList.ZNodeProductCollection;
                RepeaterProducts.DataBind();

                // Disable view state for data list item
                foreach (RepeaterItem item in RepeaterProducts.Items)
                {
                    item.EnableViewState = false;
                }
            }
        }
        #endregion

        #region Helper Functions
        /// <summary>
        /// Represents the Check for Call For Pricing method
        /// </summary>
        /// <param name="fieldValue">The field value</param>
        /// <returns>Returns the Call For Pricing message</returns>
        public string CheckForCallForPricing(object fieldValue, object callMessage)
        {
            MessageConfigAdmin mconfig = new MessageConfigAdmin();
            bool Status = bool.Parse(fieldValue.ToString());

            if (Status)
            {
                if (callMessage != null && !string.IsNullOrEmpty(callMessage.ToString()))
                {
                    return callMessage.ToString();
                }

                return mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, Convert.ToInt32(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0)), 43);
            }
            else if (!this.SpecialProfile.ShowPrice)
            {
                return mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, Convert.ToInt32(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0)), 43);
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Represents the ShowPricing Method
        /// </summary>
        /// <param name="CallForPricing">Call For Pricing</param>
        /// <returns>Returns a bool value to show Call For Pricing</returns>
        private bool ShowPricing(bool CallForPricing)
        {
            if (CallForPricing)
            {
                return false;
            }
            else if (!this.SpecialProfile.ShowPrice)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Registers the Script
        /// </summary>
        private void RegisterClientScriptClasses()
        {
            StringBuilder script = new StringBuilder();

            script.Append("var zeonSlideProductsTop = new ZeonSlideProducts({");
            script.Append("'sliderID':'" + relProdTopSeller.ClientID + "',");
            script.Append("'MaxItemCount':'5',");
            script.Append("'MobPortraitItemCount':'3',");
            script.Append("'MobLandItemCount':'3',");
            script.Append("'TabletItemCount':'3',");
            script.Append("'MinimumItemsShown':'2',");
            script.Append("});zeonSlideProductsTop.Init();");
            this.Page.ClientScript.RegisterStartupScript(GetType(), "SliderTopSeller", script.ToString(), true);
        }

        #endregion
    }
}