using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the Navigation price user control class.
    /// </summary>
    public partial class Controls_Default_Price_NavigationPrice : System.Web.UI.UserControl
    {
        #region Private Variables
        private string _selectedCategoryId = string.Empty;
        private string _title = string.Empty;
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ZNodeProfile profile = new ZNodeProfile();
                pnlProductList.Visible = profile.ShowPrice;

                if (profile.ShowPrice)
                {
                    this.BindData();
                }
            }
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the treeview
        /// </summary>
        private void BindData()
        {
            ZNodeNavigation navigation = new ZNodeNavigation();
            navigation.PopulatePricedTreeView(ctrlNavigation);
        }
        #endregion
    }
}