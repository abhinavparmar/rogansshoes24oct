﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ZMyStore.ascx.cs" Inherits="WebApp.Controls.Default.Home.ZMyStore" %>

<div class="MyStore">
    <div id="StoreLocatorRedirectLink" class="StoreLocatorLink" runat="server" enableviewstate="false">
        <asp:HyperLink ID="hlnkStoreLocator" runat="server" NavigateUrl="~/storelocator.aspx" 
            meta:resourcekey="hlnkStoreLocatorResource1" EnableViewState="false"></asp:HyperLink>

    </div>
   <%-- <div id="SelectedStoreName" class="StoreLocatorName" runat="server">
        <asp:Label ID="lblSelectedStoreName" runat="server" meta:resourcekey="lblSelectedStoreNameResource1"></asp:Label>
    </div>--%>    
</div> 