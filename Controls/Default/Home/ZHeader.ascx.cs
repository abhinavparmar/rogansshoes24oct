﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace WebApp.Controls.Default.Home
{
    public partial class ZHeader : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AddH1TagForHomePage();
            uxMyStore.Visible = uxMyStore.IsVisible = !(ZCommonHelper.IsCustomMulitipleAttributePortal());
            HomeMenu.Visible = HomeMenu.IsVisible = (ZCommonHelper.IsCustomMulitipleAttributePortal());
            PrftNavigationMenu.Visible = !(ZCommonHelper.IsCustomMulitipleAttributePortal());
        }

        /// <summary>
        /// Add H1 Tag For Home Page
        /// </summary>
        private void AddH1TagForHomePage()
        {
            MessageConfigAdmin admin = new MessageConfigAdmin();
            MessageConfig messageconfig = new MessageConfig();
            messageconfig = admin.GetByKeyPortalIDLocaleID("HeaderRightTopcontent", ZNodeConfigManager.SiteConfig.PortalID, ZNodeConfigManager.SiteConfig.LocaleID);
            if (messageconfig != null)
            {
                string messages = messageconfig.Value;
                string files = Page.MasterPageFile;
                int position = files.LastIndexOf("/");
                string mastername = string.Empty;
                if (position > 0)
                {
                    mastername = files.Substring(position).Replace("/", "");
                }
                if (mastername.ToLower().Equals("home.master"))
                {
                    messages = "<H1>" + messageconfig.Value + "</H1>";
                }
                ltlHeaderRightText.Text = messages;
            }
        }
    }
}