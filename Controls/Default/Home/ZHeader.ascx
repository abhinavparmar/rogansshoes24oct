﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ZHeader.ascx.cs" Inherits="WebApp.Controls.Default.Home.ZHeader" %>
<%@ Register Src="~/Controls/Default/Common/Logo.ascx" TagName="Logo" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Common/LoginName.ascx" TagName="LoginName" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Common/CartItemCount.ascx" TagName="CartItemCount" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/SearchEngine/TopSearch.ascx" TagName="TopSearch" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Navigation/ZHomeMenu.ascx" TagName="HomeMenu" TagPrefix="Zeon" %>
<%@ Register Src="~/Controls/Default/Navigation/PrftNavigationMenu.ascx" TagName="PrftNavigationMenu" TagPrefix="Zeon" %>
<%@ Register Src="~/Controls/Default/Common/CustomerServicePhone.ascx" TagName="CustomerServicePhoneNo" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Home/ZMyStore.ascx" TagName="MyStore" TagPrefix="Zeon" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="Znode" %>
<div class="overlay"></div>

<link rel="apple-touch-icon" href="\apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="76x76" href="\apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="120x120" href="\apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="\apple-touch-icon-152x152.png">

<link rel="apple-touch-icon-precomposed" href="\apple-touch-icon-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="\apple-touch-icon-76x76-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="\apple-touch-icon-120x120-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="\apple-touch-icon-152x152-precomposed.png">



<div id="HeaderWrapper">
    <div class="container">
        <div id="Header">
            <div class="row hidden-xs">
                <div class="col-lg-5 col-md-4">
                    <div id="Logo">
                        <a id="A1" href="~/" runat="server">
                            <Znode:Logo ID="LOGO1" runat="server"></Znode:Logo>
                        </a>
                    </div>
                    <Znode:CustomMessage ID="ucHeaderTopMessage" runat="server" EnableViewState="false" MessageKey="HeaderTopMessage" />
                </div>
                <div class="col-lg-5 col-md-6">
                    <div class="RightTopcontent">
                        <div class="TitleText">
                            <asp:Literal runat="server" ID="ltlHeaderRightText" EnableViewState="false"></asp:Literal>
                            <%-- <Znode:CustomMessage ID="ucHeaderRightText" runat="server" MessageKey="HeaderRightTopcontent" />--%>
                        </div>
                        <div class="UserAccount clearfix">
                            <span class="Link hidden">
                                <a id="A4" href="~/account.aspx" runat="server">
                                    <asp:Label ID="topLinkMyAccount" runat="server" EnableViewState="false" ToolTip="<%$ Resources:main.master, topLinkMyAccountResource%>" Text="<%$ Resources:main.master, topLinkMyAccountResource%>" ClientIDMode="Static" />
                                </a>
                            </span>
                            <ul>
                                <li class="user">
                                    <Znode:LoginName EnableViewState="false" ID="UserLoginName" runat="server" />
                                </li>
                            </ul>
                        </div>
                        <div class="ContactLink">
                            <Zeon:MyStore ID="uxMyStore" EnableViewState="false" runat="server" />
                            <ul>
                                <li class="hidden-md"><span class="CustomerServiceLinksText">
                                    <Znode:CustomerServicePhoneNo EnableViewState="false" ID="uxCustomerService" runat="server" />
                                </span></li>
                                <li class="hidden-md"><em>-or-</em></li>
                                <li><span class="Link"><a id="A5" href="~/contact.aspx" runat="server">
                                    <asp:Label ID="lblContactUs" runat="server" EnableViewState="false" ToolTip="<%$ Resources:main.master, topLinkContactUsResource%>" Text="<%$ Resources:main.master, topLinkContactUsResource%>" ClientIDMode="Static" /></a></span></li>
                            </ul>
                             <%--<div class="call-us" style="width:50px; border:1px solid red;float:left">
                          <a id="A2" href="tel:+1-800-976-4267" runat="server">
                                        <span class="fa fa-phone"></span>
                                        <em>CALL US</em> 
                                      </a>  
                    </div>--%>
                        </div> 

                        <div class="CustomerServiceLinks"> 
                            <div> 
                                <div class="SearchButton">
                                    <a><span class="fa fa-search hidefordesktop"></span><em>
                                        <asp:Literal ID="ltlSearchText" EnableViewState="false" runat="server" Text="SEARCH"></asp:Literal></em></a>
                                </div>
                                <Znode:TopSearch ID="TopSearch" runat="server" ClientIDMode="Static"></Znode:TopSearch>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 cart-section">
                    <div id="ShoppingCart">
                        <Znode:CartItemCount ID="CART_ITEM_COUNT" runat="server"></Znode:CartItemCount>
                    </div>
                    <div class="call-us hidden-lg hidden-sm"><a href="tel:+1-800-976-4267" id="lnkTelPhone" runat="server"><span class="fa fa-phone"></span><em>call us</em></a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container no-padding-mobile">
    <div id="Menu">
        <Zeon:HomeMenu ID="HomeMenu" EnableViewState="false" runat="server" IsVisible="false" />
        <Zeon:PrftNavigationMenu ID="PrftNavigationMenu" EnableViewState="false" runat="server" IsVisible="false" />

    </div>
    <div id="divSiteWideBanner" class="PromoSection">
        <div class="text-content less-content">
            <div class="text-block"><Znode:CustomMessage ID="ucSiteWideBanner" runat="server" EnableViewState="false" MessageKey="SiteWideBanner" /></div>
        </div>
        <div class="moreless-box"><span class="more"> ...</span><span class="less"></span></div>
    </div>
</div>

