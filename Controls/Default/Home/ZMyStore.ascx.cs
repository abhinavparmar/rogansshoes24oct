﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.ECommerce.UserAccount;

namespace WebApp.Controls.Default.Home
{
    public partial class ZMyStore : System.Web.UI.UserControl
    {
        #region Public Properties

        /// <summary>
        /// Set or get isVisible Property value
        /// </summary>
        public bool IsVisible{get; set; }
        
        #endregion
        /// <summary>
        /// Page Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsVisible)
            {
                HttpCookie storeCookie = null;
                int accID = ZNodeUserAccount.CurrentAccount() == null ? 0 : ZNodeUserAccount.CurrentAccount().AccountID;
                if (accID > 0)
                {
                    string cookieName = Convert.ToString(accID) + "_StoreCookie";
                    storeCookie = Request.Cookies[cookieName];
                }
                else
                {
                    storeCookie = Request.Cookies["StoreCookie"];
                }

                if (storeCookie != null && !string.IsNullOrEmpty(storeCookie.Value))
                {
                    StoreLocatorRedirectLink.Visible = true;
                    hlnkStoreLocator.ToolTip = "Store Name: " +
                                               HttpContext.Current.Server.UrlDecode(
                                                   storeCookie["StoreCookieName"].ToString()) + "\nPhone Number: " +
                                               HttpContext.Current.Server.UrlDecode(
                                                   storeCookie["StoreCookiePhone"].ToString()) + "\nZip Code: " +
                                               HttpContext.Current.Server.UrlDecode(
                                                   storeCookie["StoreCookieZipCode"].ToString());
                }
                else
                {
                    StoreLocatorRedirectLink.Visible = true;
                }
            }
        }
    }
} 