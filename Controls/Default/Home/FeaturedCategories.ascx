<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Home_FeaturedCategories" Codebehind="FeaturedCategories.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="uc1" %>

<div class="HomeFeaturedCategories">
    <div class="Title"><uc1:CustomMessage id="CustomMessage1" MessageKey="CategoryFeaturedCategoriesTitle" runat="server"></uc1:CustomMessage></div>
    <asp:DataList ID="uxdatalist" runat="server"  RepeatDirection="Horizontal">
        <ItemTemplate>
            <div class="SubCategoryListItem">                  
                <div class='CategoryLink'>
                <a id="A1" href= '<%# "~/category.aspx?zcid=" + DataBinder.Eval(Container.DataItem, "CategoryId") %>' runat="server">
                    <img id="Img2" alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag")%>' title='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag")%>' runat="server" style="border:none"  src='<%# znodeImage.GetImageHttpPathSmall(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>'/>                               
                </a>
                </div>                 
                <div class="CategoryLink">
                      <img id="RightArrow" alt="Arrow" title="Arrow" runat="server" src= '<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("right_arrow.gif") %>' /><a id="A2" href= '<%# "~/category.aspx?zcid=" + DataBinder.Eval(Container.DataItem, "CategoryId") %>' runat="server">
                     <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Name")%>' ></asp:Label>
                    </a> 
                </div>                                                                    
            </div>                    
        </ItemTemplate>                  
    </asp:DataList>
</div>