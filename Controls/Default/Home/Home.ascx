<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Home_Home" CodeBehind="Home.ascx.cs" %>
<%@ Register Src="~/Controls/Default/Specials/ZTopSeller.ascx" TagName="TopSeller" TagPrefix="Zeon" %>
<%@ Register Src="~/Controls/Default/zNewArrival/ZNewArrivalProducts.ascx" TagName="NewArrival" TagPrefix="Zeon" %>
<%@ Register Src="~/Controls/Default/Brand/ZHomeAllBrands.ascx" TagName="AllBrands" TagPrefix="Zeon" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="Znode" %>

<div>
    <asp:Label ID="lblHtml" runat="server" EnableViewState="false"></asp:Label>
</div>
<Znode:CustomMessage ID="uvHomeMiddleContent" runat="server" ClientIDMode="Static" MessageKey="HomeMiddleBlock" EnableViewState="false" />
<div class="HomePage">
    <div id="NewArrivalID" class="NewArrival">
        <Zeon:NewArrival ID="uxNewArrival" runat="server" EnableViewState="false"></Zeon:NewArrival>
    </div>
</div>
<div id="HomeBrands" class=" hidden-xs">
    <div class="col-lg-12">
        <Zeon:AllBrands ID="uxAllBrands" runat="server" MaxBrandsCount="7" EnableViewState="false" />
    </div>
</div>

<Znode:CustomMessage ID="ucHomeFooterWeekReview" runat="server" ClientIDMode="Static" MessageKey="HomeBottomLeftBlock" EnableViewState="false"/>
<Znode:CustomMessage ID="ucHomeFooterRightSectionMessage" runat="server" ClientIDMode="Static" MessageKey="HomeBottomRightBlock" EnableViewState="false"/>
