<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_ShoppingCart_NavigationCart" CodeBehind="NavigationCart.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="ZNode" %>

<asp:Panel ID="pnlShoppingCartNavigation" runat="server" Visible="false">
    <div id="NavigationCart">
        <div id="Header">
            <div class="MyCartTitle">
                <ZNode:CustomMessage ID="CustomMessage1" EnableViewState="false" MessageKey="ShoppingCartNavigationTitle" runat="server"></ZNode:CustomMessage>
            </div>
            <div class="Total">
                Total :
                <asp:Label ID="lblTotal" EnableViewState="false" runat="server"></asp:Label>
            </div>
        </div>
        <div id="NavigationCart_outer">
            <div id="NavigationCart_inner">
                <div id="NavigationCart_Items">
                    <asp:DataList ID="ShoppingCartItems" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                        <ItemTemplate>
                            <div class="NavigationCartItem">
                                <div class="GrayBorder"></div>
                                <div class="Image">
                                    <a href='<%# ResolveUrl( DataBinder.Eval(Container.DataItem, "Product.ViewProductLink").ToString())%>' runat="server">
                                        <img alt='<%# DataBinder.Eval(Container.DataItem, "Product.ImageAltTag")%>' border='0'
                                            title='<%# DataBinder.Eval(Container.DataItem, "Product.ImageAltTag")%>'
                                            src='<%#ResolveUrl(DataBinder.Eval(Container.DataItem, "Product.CrossSellImagePath").ToString())%>' runat="server" />
                                    </a>
                                </div>
                                <div class="GrayBorder"></div>
                                <div class="Detail">
                                    <span class="Name"><%# DataBinder.Eval(Container.DataItem, "Product.Name").ToString()%></span>
                                    <span class="Price">
                                        <asp:Label ID="Price" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Product.FormattedPrice") %>'></asp:Label></span>
                                </div>
                                <div class="BuyButton">
                                    <asp:LinkButton ID="ViewEditCartImage" PostBackUrl="/shoppingCart.aspx" runat="server" AlternateText="View/Edit Cart" CssClass="Button" meta:resourcekey="ViewEditcart"></asp:LinkButton>
                                </div>

                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>
