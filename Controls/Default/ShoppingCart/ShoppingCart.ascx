<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="WebApp.Controls_Default_ShoppingCart_ShoppingCart" CodeBehind="ShoppingCart.ascx.cs" %>
<%@ Register Src="~/Controls/Default/Common/SalesDepartmentPhone.ascx" TagName="SalesDepartmentPhoneNo"
    TagPrefix="ZNode" %>
<%@ Register Src="../CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="ZNode" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/common/spacer.ascx" %>
<asp:Panel ID="shoppingcartpannel" runat="server" DefaultButton="Checkout1">
    <div>
        <asp:GridView class="TableContainer" ID="uxCart" runat="server" AutoGenerateColumns="False"
            EmptyDataText="Shopping Cart Empty" GridLines="None" CellPadding="4" OnRowCommand="Cart_RowCommand"
            OnRowDataBound="UxCart_RowDataBound" CssClass="Grid" meta:resourcekey="uxCartResource1">
            <Columns>
                <asp:TemplateField HeaderText="Product Detail" HeaderStyle-HorizontalAlign="Left" meta:resourcekey="TemplateFieldResource3">
                    <ItemTemplate>
                        <div class="ProductImage">
                            <a enableviewstate="false" id="A1" href='<%# DataBinder.Eval(Container.DataItem, "Product.ViewProductLink").ToString() %>'
                                runat="server">
                                <img id="Img1" enableviewstate="false" alt='<%# DataBinder.Eval(Container.DataItem, "Product.ImageAltTag") %>'
                                    border='0' src='<%# DataBinder.Eval(Container.DataItem, "Product.ThumbnailImageFilePath") %>' title='<%# DataBinder.Eval(Container.DataItem, "Product.ImageAltTag") %>'
                                    runat="server" />
                            </a>
                        </div>
                        <div class="Description">
                            <div class="ProductName">
                                <a enableviewstate="false" id="A2" href='<%# DataBinder.Eval(Container.DataItem, "Product.ViewProductLink").ToString() %>'
                                    runat="server" title='<%# DataBinder.Eval(Container.DataItem, "Product.Name").ToString()%>'>
                                    <%# DataBinder.Eval(Container.DataItem, "Product.Name").ToString()%></a>
                               <div class="Price only-mobile">
                            <div id="mPrice" runat="server" visible='<%# !(Roles.IsUserInRole("ADMIN") || Roles.IsUserInRole("CUSTOMER SERVICE REP"))%>'><%# DataBinder.Eval(Container.DataItem, "UnitPrice","{0:c}") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix()%> </div>
                            <asp:Label ID="lblRetailPricem" runat="server" Visible='<%# (Roles.IsUserInRole("ADMIN") || Roles.IsUserInRole("CUSTOMER SERVICE REP"))%>'></asp:Label>
                            <asp:Label ID="lblTeamPricem" runat="server" Visible='<%# (Roles.IsUserInRole("ADMIN") || Roles.IsUserInRole("CUSTOMER SERVICE REP")) %>'></asp:Label> 
                        </div> 
                            </div>
                            <div class="ProductDesc">
                                <p><strong>Item#</strong><%# DataBinder.Eval(Container.DataItem, "Product.ProductNum").ToString()%></p>
                                <%# DataBinder.Eval(Container.DataItem, "Product.ShoppingCartDescription").ToString()%>
                            </div>
                            <div id="divItemNotes" runat="server" visible='<%# NotesEnabled %>' enableviewstate="false">
                                <asp:Label runat="server" ID="lblItemNotes" meta:resourcekey="lblItemNotes" EnableViewState="false"></asp:Label>
                                <asp:TextBox ID="txtItemNotes" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Notes").ToString()%>' EnableViewState="false"><br />
                                </asp:TextBox>
                                <asp:HiddenField ID="hdnGUID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "GUID").ToString() %>' />
                            </div>
                        </div>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SubTotal" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <div class="Price desktop-only">
                            <div id="divPrice" runat="server" visible='<%# !(Roles.IsUserInRole("ADMIN") || Roles.IsUserInRole("CUSTOMER SERVICE REP"))%>'><%# DataBinder.Eval(Container.DataItem, "UnitPrice","{0:c}") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix()%> </div>
                            <asp:TextBox ID="txtUnitPrice" Visible='<%# Roles.IsUserInRole("ADMIN") || Roles.IsUserInRole("CUSTOMER SERVICE REP")%>' ValidationGroup="groupCart" runat="server" Width="50" Style="text-align: center;"></asp:TextBox>
                            <asp:Label ID="lblRetailPrice" runat="server" Visible='<%# (Roles.IsUserInRole("ADMIN") || Roles.IsUserInRole("CUSTOMER SERVICE REP"))%>'></asp:Label>
                            <asp:Label ID="lblTeamPrice" runat="server" Visible='<%# (Roles.IsUserInRole("ADMIN") || Roles.IsUserInRole("CUSTOMER SERVICE REP")) %>'></asp:Label>
                            <ajaxToolKit:FilteredTextBoxExtender FilterType="Custom" ValidChars="0123456789." EnableViewState="false" TargetControlID="txtUnitPrice" ID="fittxtUnitPrice" runat="server" />
                            <asp:RequiredFieldValidator ID="rfvtxtUnitPrice" EnableViewState="false" CssClass="Error" runat="server" ControlToValidate="txtUnitPrice" ValidationGroup="groupCart" ErrorMessage="Price is required." Display="Dynamic" SetFocusOnError="true" />
                            <asp:RegularExpressionValidator ID="revtxtUnitPrice" EnableViewState="false" runat="server" ControlToValidate="txtUnitPrice" ValidationGroup="groupCart"
                                CssClass="Error" ErrorMessage="Invalid price."
                                ToolTip="Invalid price." ValidationExpression="\d+(\.\d\d)?$"
                                Display="Dynamic"></asp:RegularExpressionValidator>
                        </div>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Qty" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Panel ID="upnlQuantity" runat="server" DefaultButton="lnkUpdateItem">
                            <div class="qtytextbox">
                                <asp:TextBox ID="txtQty" Text="1" MaxLength="5" ValidationGroup="groupCart" runat="server" Width="50" Style="text-align: center;" alt="Quantity" ToolTip="Quantity"></asp:TextBox>
                                <asp:RangeValidator ValidationGroup="groupCart" ID="rgvQuantity" MinimumValue="0" CssClass="Error"
                                    ControlToValidate="txtQty" MaximumValue='<%# CheckInventory(DataBinder.Eval(Container.DataItem, "GUID").ToString()) %>' Visible='<%# !(Roles.IsUserInRole("ADMIN") || Roles.IsUserInRole("CUSTOMER SERVICE REP")) %>'
                                    SetFocusOnError="True" runat="server" Type="Integer" Display="Dynamic" meta:resourcekey="RangeValidator1Resource1" Enabled='<%# !(Roles.IsUserInRole("ADMIN") || Roles.IsUserInRole("CUSTOMER SERVICE REP")) %>'></asp:RangeValidator>
                            </div>
                            <ajaxToolKit:FilteredTextBoxExtender FilterType="Numbers" TargetControlID="txtQty" ID="fitTxtQty" runat="server" />
                            <asp:RequiredFieldValidator ID="rfvQty" CssClass="Error" runat="server" ControlToValidate="txtQty" ValidationGroup="groupCart" ErrorMessage="Qty is required." Display="Dynamic" SetFocusOnError="true" />
                            <asp:HiddenField ID="GUID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "GUID").ToString() %>' />
                            <asp:LinkButton ID="lnkUpdateItem" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "GUID").ToString() %>' Text="Update" ToolTip="Update" CssClass="Edit" CommandName="UpdateItem" CausesValidation="true" ValidationGroup="groupCart"></asp:LinkButton>
                            <asp:LinkButton ID="lnkbtnRemove" runat="server" CssClass="Delete" Text="Delete" ToolTip="Delete" CommandName="remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "GUID").ToString() %>' EnableViewState="true">
                            </asp:LinkButton>
                        </asp:Panel>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="TOTAL" HeaderStyle-HorizontalAlign="Left" meta:resourcekey="TemplateFieldResource6">
                    <ItemTemplate>
                        <div class="Price">
                            <%# DataBinder.Eval(Container.DataItem, "ExtendedPrice","{0:c}") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix()%>
                        </div>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle CssClass="total-price desktoponly" />
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="Footer" />
            <RowStyle CssClass="Row" />
            <HeaderStyle CssClass="Header" />
            <AlternatingRowStyle CssClass="AlternatingRow" />
        </asp:GridView>
    </div>
    <ZNode:CustomMessage ID="CMShipsMessage" MessageKey="ShoppingCartShippingMessage" runat="server" EnableViewState="false" />
    <div class="Form">
        <div class="Row Clear">
            <div align="right">
                <asp:Label ID="PaymentErrorMsg" EnableViewState="false" CssClass="Error" runat="server" Visible="False" meta:resourcekey="PaymentErrorMsgResource1" />
                <div>
                    <asp:Label ID="lblErrorMessage" CssClass="Error" runat="server" ForeColor="Green" EnableViewState="false"
                        meta:resourcekey="lblErrorMessageResource1"></asp:Label>
                </div>
            </div>
        </div>
        <div class="Row Clear">
            <div class="TotalBox">
                <div class="LeftContent">
                    <div class="ContinueShopping">
                        <a href="/" id="aContinueShopping" title="Continue Shopping">Continue Shopping</a>
                    </div>
                    <%-- <div class="updateQuantity">
                        <asp:LinkButton ID="lbtnUpdateCart" runat="server" AlternateText="Update Quantity" OnClick="lbtnUpdateCart_click"
                            ValidationGroup="groupCart" CssClass="Edit">UPDATE</asp:LinkButton>
                    </div>--%>
                    <div class="CustomMessage">
                        <ZNode:CustomMessage ID="ShoppingCartFooterTextCustomMessage" MessageKey="ShoppingCartFooterText" runat="server" EnableViewState="false" />
                    </div>
                </div>
                <div class="RightContent">
                    <div class="CartPricingSection">
                        <div class="Row Clear Right">
                            <div class="ShoppingTotalContent clearfix">
                                <asp:Localize ID="Localize3" meta:resourceKey="txtSubtotal" runat="server" EnableViewState="false" />
                            </div>
                            <div class="ShoppingTotalContent">
                                <asp:Label ID="SubTotal" EnableViewState="false" runat="server" meta:resourcekey="SubTotalResource1"></asp:Label>
                            </div>
                        </div>
                        <div class="Row Clear SubTotal Right" id="tblRowDiscount" runat="server" visible="true">
                            <div class="ShoppingTotalContent">
                                <asp:Localize ID="Localize1" meta:resourceKey="txtDiscount" runat="server" EnableViewState="false" />
                            </div>
                            <div class="ShoppingTotalContent">
                                <asp:Label ID="DiscountDisplay" EnableViewState="false" runat="server" meta:resourcekey="DiscountDisplayResource1"></asp:Label>
                            </div>
                        </div>
                        <div class="Row Clear Right" id="tblRowTax" runat="server" visible="false">
                            <div class="ShoppingTotalContent">
                                <asp:Localize ID="Localize2" meta:resourceKey="txtTax" runat="server" EnableViewState="false" />
                                <asp:Label ID="TaxPct" runat="server" EnableViewState="False" meta:resourcekey="TaxPctResource1"></asp:Label>
                            </div>
                            <div class="ShoppingTotalContent">
                                <asp:Label ID="Tax" runat="server" meta:resourcekey="TaxResource1"></asp:Label>
                            </div>
                        </div>
                        <div class="Row Clear" id="tblRowShipping" runat="server" visible="false"
                            style="float: right;">
                            <div class="ShoppingTotalContent">
                                <asp:Localize ID="Localize4" meta:resourceKey="txtShipping" runat="server" EnableViewState="false" />
                            </div>
                            <div class="ShoppingTotalContent">
                                <asp:Label ID="Shipping" runat="server"></asp:Label>
                            </div>
                        </div>

                        <div class="Row Clear Right TopBorder">
                            <div class="ShoppingTotalAmountContent">
                                <asp:Localize ID="Localize6" meta:resourceKey="txtTotal" EnableViewState="false" runat="server" />
                            </div>
                            <div class="ShoppingTotalAmountContent">
                                <asp:Label ID="Total" EnableViewState="false" runat="server" meta:resourcekey="TotalResource2"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="CheckoutBox Right">
                        <asp:LinkButton ID="Checkout1" runat="server" AlternateText="Check Out" OnClick="Checkout_Click" ToolTip="Check Out"
                            ValidationGroup="groupCart" CssClass="checkout" meta:resourcekey="Checkout1Resource1" ClientIDMode="Static">Proceed to Checkout</asp:LinkButton>
                        <br />
                    </div>
                </div>

                <asp:PlaceHolder runat="server" ID="PromotionPanel">
                    <div class="clearfix CouponContent">
                        <asp:Panel ID="pnlCouponCode" runat="server" DefaultButton="ApplyCouponGo" CssClass="CouponContentBox">
                            <b>
                                <asp:Localize ID="Localize5" EnableViewState="false" meta:resourceKey="txtEnterCouponCode" runat="server" /></b>
                            <p>Do you have a Coupon Code on this order? Enter it now.</p>
                            <asp:TextBox runat="server" ID="ecoupon" meta:resourcekey="ecouponResource1"></asp:TextBox>
                            <asp:LinkButton ID="ApplyCouponGo" runat="server" AlternateText="Apply" OnClick="Btnapply_click" ToolTip="Apply"
                                ValidationGroup="groupCart1" CssClass="Button" meta:resourcekey="ApplyCouponGoResource1">Apply</asp:LinkButton>
                            <div class="Clear PromotionText">
                                <asp:Label ID="lblPromoMessage" CssClass="Error" EnableViewState="false" runat="server" meta:resourcekey="lblPromoMessageResource1"></asp:Label>
                            </div>
                        </asp:Panel>
                        <div class="PromiseSection">
                            <ZNode:CustomMessage ID="ucHomeLeftSectionMessage" EnableViewState="false" MessageKey="HomeTopRightBlock" runat="server" />
                        </div>
                    </div>
                </asp:PlaceHolder>
            </div>
        </div>
    </div>
</asp:Panel>


