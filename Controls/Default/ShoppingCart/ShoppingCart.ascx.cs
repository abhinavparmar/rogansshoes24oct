using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.Promotions;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using System.Linq;
using Zeon.Libraries.Utilities;
using System.Data.Linq;
using System.Collections.Generic;
using WebApp.Controls.Default.Home;
namespace WebApp
{
    /// <summary>
    /// Represents the ShoppingCart user control class.
    /// </summary>
    public partial class Controls_Default_ShoppingCart_ShoppingCart : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeShoppingCart _ShoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
        private DateTime currentdate = System.DateTime.Now.Date;
        private ZNodeCoupon _coupon = new ZNodeCoupon();
        private ZNodeOrderFulfillment _order = new ZNodeOrderFulfillment();
        private int rowNumber = 0;
        private bool _ShowTaxShipping = false;
        //Zeon Custom Members
        private bool _NotesEnabled = false;
        bool allowOutOfStockToUser = false;
        #endregion

        #region delegate event

        public delegate void ProceedToCheckout(object sender, EventArgs e);
        public event ProceedToCheckout ProceedToCheckoutEvent;

        #endregion

        #region Public Events
        public event System.EventHandler CartItem_RemoveLinkClicked;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the Current shopping cart object
        /// </summary>
        public ZNodeShoppingCart ShoppingCartObject
        {
            get
            {
                return this._ShoppingCart;
            }

            set
            {
                this._ShoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the boolean value to display/hide the shipping and tax fields
        /// </summary>
        public bool ShowTaxShipping
        {
            get
            {
                return this._ShowTaxShipping;
            }

            set
            {
                this._ShowTaxShipping = value;
            }
        }

        /// <summary>
        /// Gets the coupon code
        /// </summary>
        public string CouponCode
        {
            get
            {
                return ecoupon.Text.Trim();
            }
        }


        #endregion

        #region Zeon Custom Properties

        /// <summary>
        /// get or se value of _NotesEnabled
        /// </summary>
        protected bool NotesEnabled
        {
            get { return _NotesEnabled; }
            set { _NotesEnabled = value; }
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Returns boolean value to enable Validator to check against the Quantity textbox
        /// It checks for AllowBackOrder and TrackInventory settings for this product,and returns the value
        /// </summary>
        /// <param name="value">The value to enable Stock Validator or not</param>
        /// <returns>Returns a bool value to enable Stock validator or not</returns>
        public bool EnableStockValidator(string value)
        {
            // Get Shopping Cart Item
            ZNodeShoppingCartItem item = this._ShoppingCart.GetItem(value);

            bool enablevalidator = true;

            if (item != null)
            {
                // Allow Back Order
                if (item.Product.AllowBackOrder && item.Product.TrackInventoryInd)
                {
                    enablevalidator = false;
                }
                else if (item.Product.AllowBackOrder == false && item.Product.TrackInventoryInd == false)
                {
                    // Don't track inventory
                    enablevalidator = false;
                }

            }

            return enablevalidator;
        }

        public bool EnableAddOnStockvalidator(string value)
        {
            // Get Shopping Cart Item
            ZNodeShoppingCartItem item = this._ShoppingCart.GetItem(value);

            bool enableaddonvalidator = false;
            if (item.Product.SelectedAddOns.AddOnCollection.Count > 0)
            {
                enableaddonvalidator = true;
                // Loop through the Selected addons for each Shopping cartItem
                foreach (
                    ZNode.Libraries.ECommerce.Entities.ZNodeAddOnEntity AddOn in
                        item.Product.SelectedAddOns.AddOnCollection)
                {
                    // Loop through the Add-on values for each Add-on
                    foreach (
                        ZNode.Libraries.ECommerce.Entities.ZNodeAddOnValueEntity AddOnValue in
                            AddOn.AddOnValueCollection)
                    {
                        // Check for quantity on hand and back-order,track inventory settings
                        if (AddOn.AllowBackOrder && AddOn.TrackInventoryInd)
                        {
                            enableaddonvalidator = false;
                        }
                        else if (AddOn.AllowBackOrder == false && AddOn.TrackInventoryInd == false)
                        {
                            // Don't track inventory
                            enableaddonvalidator = false;
                        }
                    }
                }
            }

            return enableaddonvalidator;
        }


        /// <summary>
        /// Returns Quantity on Hand (inventory stock value)
        /// </summary>
        /// <param name="value">The Item value </param>
        /// <returns>Returns the Quantity on Hand</returns>
        public int CheckInventory(string value)
        {
            // Get Shopping Cart Item
            ZNodeShoppingCartItem item = this._ShoppingCart.GetItem(value);

            if (item != null)
            {
                int? quantityAvailable = item.Product.MaxQty;

                //if (item.Product.ZNodeBundleProductCollection.Count == 0 && item.Product.AllowBackOrder == false && item.Product.TrackInventoryInd)
                //{
                //    quantityAvailable = item.Product.QuantityOnHand;
                //}

                foreach (ZNode.Libraries.ECommerce.Entities.ZNodeProductBaseEntity _bundleProduct in item.Product.ZNodeBundleProductCollection)
                {
                    if (quantityAvailable == null && _bundleProduct.AllowBackOrder == false && _bundleProduct.TrackInventoryInd)
                    {
                        quantityAvailable = _bundleProduct.QuantityOnHand;
                    }
                    else if (quantityAvailable > _bundleProduct.QuantityOnHand && _bundleProduct.AllowBackOrder == false && _bundleProduct.TrackInventoryInd)
                    {
                        quantityAvailable = _bundleProduct.QuantityOnHand;
                    }
                }

                if (item.Product.SelectedAddOns.SelectedAddOnValues.Length > 0 && item.Product.SelectedAddOns.AddOnCollection
                    .Cast<ZNodeAddOn>()
                    .Any(y => y.TrackInventoryInd))
                {

                    // Loop through the Selected addons for each Shopping cartItem
                    foreach (ZNode.Libraries.ECommerce.Entities.ZNodeAddOnEntity AddOn in item.Product.SelectedAddOns.AddOnCollection.Cast<ZNodeAddOn>()
                    .Where(y => y.AllowBackOrder == false && y.TrackInventoryInd))
                    {
                        // Loop through the Add-on values for each Add-on
                        foreach (ZNode.Libraries.ECommerce.Entities.ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                        {
                            if (quantityAvailable > AddOnValue.QuantityOnHand)
                                quantityAvailable = AddOnValue.QuantityOnHand;

                            // Check for quantity on hand and back-order,track inventory settings
                            if (AddOnValue.QuantityOnHand <= 0 && AddOn.AllowBackOrder == false && AddOn.TrackInventoryInd)
                            {
                                quantityAvailable = 0;
                            }
                        }
                    }
                }

                // Get Current Qunatity by Subtracting QunatityOrdered from Quantity On Hand(Inventory)
                if (item.Product.SelectedSKU != null && item.Product.SelectedSKU.QuantityOnHand < item.Product.MaxQty)
                {
                    quantityAvailable = item.Product.SelectedSKU.QuantityOnHand;
                }
                int CurrentQuantity = quantityAvailable.GetValueOrDefault(0) - this._ShoppingCart.GetQuantityOrdered(item);

                if (CurrentQuantity <= 0)
                {
                    return 0;
                }
                else
                {
                    return CurrentQuantity;
                }
            }
            else
            {
                return 1;
            }
        }

        /// <summary>
        /// Bind control display based on properties set
        /// </summary>
        public void Bind()
        {
            this.rowNumber = 0;

            //Zeon Customization :: Changes done on date  : 28/May/2012 :: Issue Fix :: Shopping object cart is null while adding item from quick order.
            if (this._ShoppingCart == null)
            {
                this._ShoppingCart = ZNodeShoppingCart.CurrentShoppingCart();
            }
            if (this._ShoppingCart != null)
            {
                this._ShoppingCart.ClearExistingShippingDetails();
                this._ShoppingCart.CalculateShoppingCart();

                ZNodeUserAccount _userAccount = ZNodeUserAccount.CurrentAccount();

                // Bind cart
                uxCart.DataSource = this._ShoppingCart.ShoppingCartItems;
                uxCart.DataBind();

                // Disable the view state for the selected columns only
                for (int rowIndex = 0; rowIndex <= uxCart.Rows.Count - 1; rowIndex++)
                {
                    // Do not disable viewstate for first column (index 0) and quantity column
                    for (int columnIndex = 1; columnIndex <= uxCart.Rows[rowIndex].Cells.Count - 1; columnIndex++)
                    {
                        if (columnIndex != 3)
                        {
                            //uxCart.Rows[rowIndex].Cells[columnIndex].EnableViewState = false;
                        }
                    }
                }
                string error = string.Empty;
                int rowNum = 1;
                // Set the value for the Dropdown list
                foreach (GridViewRow row in uxCart.Rows)
                {
                    HiddenField hdnGUID = row.FindControl("GUID") as HiddenField;
                    string GUID = hdnGUID.Value;

                    // Get Shopping cart item using GUID value
                    ZNodeShoppingCartItem item = this._ShoppingCart.GetItem(GUID);

                    item.Product.CheckSKUProfile();
                    // Bind the MaxQuantity value to the Dropdown list
                    // If Min quantity is not set in admin, set it to 1
                    int minQty = item.Product.MinQty == 0 ? 1 : item.Product.MinQty;

                    // If Max quantity is not set in admin , set it to 10
                    int maxQty = item.Product.MaxQty == 0 ? 10 : item.Product.MaxQty;

                    ArrayList quantityList = new ArrayList();

                    for (int itemIndex = minQty; itemIndex <= maxQty; itemIndex++)
                    {
                        quantityList.Add(itemIndex);
                    }

                    //Zeon Custom Code:- Start
                    TextBox txtqty = row.FindControl("txtQty") as TextBox;
                    if (txtqty != null)
                    {
                        txtqty.Text = item.Quantity.ToString();
                        if (rowNum == 1)
                        {
                            txtqty.Focus();
                            rowNum++;
                        }
                    }

                    if (Roles.IsUserInRole("ADMIN") || Roles.IsUserInRole("CUSTOMER SERVICE REP"))
                    {
                        Label lblRetailPrice = row.FindControl("lblRetailPrice") as Label;
                        Label lblTeamPrice = row.FindControl("lblTeamPrice") as Label;
                        if (lblRetailPrice != null)
                        {
                            lblRetailPrice.Text = "Retail Price: " + item.ShowRetailPrice.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                        }
                        if (lblTeamPrice != null)
                        {
                            if (item.TeamPrice > 0)
                            {
                                lblTeamPrice.Visible = true;
                                lblTeamPrice.Text = "<BR>Team Price: " + item.TeamPrice.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                            }
                            else
                            {
                                lblTeamPrice.Visible = false;
                            }
                        }
                        TextBox txtOverridePrice = row.FindControl("txtUnitPrice") as TextBox;
                        if (txtOverridePrice != null)
                        {
                            txtOverridePrice.Text = item.UnitPrice.ToString();
                        }
                    }
                    //Zeon Custom Code:- End
                }

                // Bind totals
                SubTotal.Text = this._ShoppingCart.SubTotal.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                DiscountDisplay.Text = "-" + this._ShoppingCart.Discount.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                tblRowDiscount.Visible = this._ShoppingCart.Discount > 0 ? true : false;
                if (this._ShowTaxShipping)
                {
                    // Shows the tax and shipping fields
                    tblRowTax.Visible = true;
                    tblRowShipping.Visible = true;
                    Tax.Text = this._ShoppingCart.OrderLevelTaxes.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                    Shipping.Text = this._ShoppingCart.ShippingCost.ToString("C") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                    Total.Text = this._ShoppingCart.Total.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();

                    // Check Sale Tax rate
                    if (this._ShoppingCart.TaxRate > 0)
                    {
                        TaxPct.Text = "(" + this._ShoppingCart.TaxRate + "%)";
                    }
                    else
                    {
                        TaxPct.Text = string.Empty;
                    }
                }
                else
                {
                    // Hides the tax and shipping fields.
                    tblRowTax.Visible = false;
                    tblRowShipping.Visible = false;
                    Total.Text = (this._ShoppingCart.SubTotal - this._ShoppingCart.Discount).ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                }

                // Show messages
                lblErrorMessage.Text = this._ShoppingCart.ErrorMessage;
                lblPromoMessage.Text = this._ShoppingCart.PromoDescription;

                if (error.Length > 0)
                {
                    if (System.Web.HttpContext.Current.Session["IsCartChanged"] == null && System.Web.HttpContext.Current.Session["RedirectToCart"] == null)
                        lblErrorMessage.Text = error;
                }

                //int shoppingCartItemCount = this._ShoppingCart.ShoppingCartItems.Count;
                //int cartItemCountforPromotionalProduct = this._ShoppingCart.ShoppingCartItems.OfType<ZNodeShoppingCartItem>().AsQueryable().Where(i => i.Product.IsCustomPromotionApplied == true).Count();
                //if (shoppingCartItemCount == cartItemCountforPromotionalProduct)
                //{
                //    pnlCouponCode.Visible = false;
                //}
            }
        }

        /// <summary>
        /// Represents the UpdateQuantity method
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        public void UpdateQuantity(object sender, EventArgs e)
        {
            DropDownList ddl = (DropDownList)sender;

            // Get the rowId for the Dropdown list
            int rowId = Convert.ToInt32(ddl.Attributes["rowid"]);

            HiddenField hdnGUID = uxCart.Rows[rowId].Cells[0].FindControl("GUID") as HiddenField;

            // Set the Dropdown list value   
            int Qty = Convert.ToInt32(ddl.Text);
            string GUID = hdnGUID.Value;

            //Perficient Customization to allow Role based order Qty:Starts
            bool isPrdMaxQtyAllowToUser = Roles.IsUserInRole("ADMIN") || Roles.IsUserInRole("CUSTOMER SERVICE REP") ? true : false;
            //Perficient Customization to allow Role based order Qty:Ends

            // Get Shopping cart item using GUID value
            ZNodeShoppingCartItem item = this._ShoppingCart.GetItem(GUID);

            // Update quantity in the shopping cart manager
            if (Qty == 0)
            {
                this._ShoppingCart.RemoveFromCart(GUID);

                // Remove the item from SavedCart Info
                ZNodeSavedCart savedCart = new ZNodeSavedCart();
                savedCart.RemoveSavedCartLineItem(item.Product.SelectedSKU.SKUID);
            }
            else
            {
                // If Product is set to Allow back order or Track inventory is not enabled
                // then Update the quantity  
                if (item.Product.AllowBackOrder)
                {
                    // Update Quantity for this product
                    this._ShoppingCart.UpdateItemQuantity(GUID, Qty);
                }
                else if (item.Product.AllowBackOrder == false && item.Product.TrackInventoryInd == false)
                {
                    // Update Quantity for this product
                    this._ShoppingCart.UpdateItemQuantity(GUID, Qty);
                }
                else
                {
                    // Check for available quantity (which included both Available + Quantity Ordered)
                    if (this.UpdateQuantity(item))
                    {
                        this._ShoppingCart.UpdateItemQuantity(GUID, Qty);
                    }
                }

                // Update quantity to SavedCart Info
                item = this._ShoppingCart.GetItem(GUID);
                ZNodeSavedCart.AddToSavedCart(item);
            }
        }

        #endregion

        #region Page Load

        /// <summary>
        ///  Page Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            if (this._ShoppingCart != null)
            {
                // Add the shopping cart page listrack tracking script.
                ZNodeAnalytics analytics = new ZNodeAnalytics();
                analytics.AnalyticsData.IsShoppingCartPage = true;
                analytics.AnalyticsData.ShoppingCart = this._ShoppingCart;
                analytics.Bind();
            }
        }

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get the user account from session
            ZNodeUserAccount _userAccount = ZNodeUserAccount.CurrentAccount();

            ZNode.Libraries.DataAccess.Service.PaymentSettingService _pmtServ = new ZNode.Libraries.DataAccess.Service.PaymentSettingService();
            ZNode.Libraries.DataAccess.Entities.TList<ZNode.Libraries.DataAccess.Entities.PaymentSetting> _pmtSetting = _pmtServ.GetAll();

            PaymentErrorMsg.Visible = false;

            // Check whether this profile has payment options
            if (Request.QueryString["ErrorMsg"] != null)
            {
                if (Request.QueryString["ErrorMsg"] == "1")
                {
                    PaymentErrorMsg.Text = this.GetLocalResourceObject("ProcessFailed").ToString();
                }

                PaymentErrorMsg.Visible = true;
            }

            PromotionPanel.Visible = this.GetShoppingCartVendors().Count == 1;

            //Genrate Image pixel tag for Abandon shopping cart : Trigger Email
            //GenerateTriggerMailHelperCartPixelTags();
            //Zeon custom Code to set Notes Visibility
            NotesEnabled = GetNotesVisibility();
            allowOutOfStockToUser = Roles.IsUserInRole("ADMIN") || Roles.IsUserInRole("CUSTOMER SERVICE REP") ? true : false;

        }
        #endregion

        #region General Events
        /// <summary>
        /// Event is raised when Apply button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Btnapply_click(object sender, EventArgs e)
        {
            if (this._ShoppingCart != null)
            {
                this._ShoppingCart.AddCouponCode(this.CouponCode);
            }
        }

        /// <summary>
        /// DropDown Index change event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Quantity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._ShoppingCart != null)
            {
                this.UpdateQuantity(sender, e);
            }
        }

        #endregion

        #region Grid Event

        /// <summary>
        /// Grid command event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Cart_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (this._ShoppingCart != null)
            {
                // The remove link was clicked
                if (e.CommandName.Equals("remove"))
                {
                    string GUID = e.CommandArgument.ToString();

                    // First locate the item in the collection
                    ZNodeShoppingCartItem itemToRemove = this._ShoppingCart.GetItem(GUID);

                    // Remove from the SavedCart table
                    if (itemToRemove != null)
                    {
                        ZNodeSavedCart savedCart = new ZNodeSavedCart();
                        savedCart.RemoveSavedCartLineItem(itemToRemove.Product.SelectedSKU.SKUID);
                    }

                    // Update in the shopping cart manager
                    this._ShoppingCart.RemoveFromCart(GUID);

                    if (this.CartItem_RemoveLinkClicked != null)
                    {
                        this.CartItem_RemoveLinkClicked(sender, e);
                    }
                    GetCartItemCount(); //Zeon Custom Code to update mini cart status
                }
                //Zeon Code to update single Item in cart
                if (e.CommandName.Equals("UpdateItem"))
                {
                    string GUID = e.CommandArgument.ToString();
                    string notes = string.Empty;
                    // First locate the item in the collection
                    ZNodeShoppingCartItem itemToUpdate = this._ShoppingCart.GetItem(GUID);
                    //get Updated  Quantity
                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
                    TextBox txt = row.FindControl("txtQty") as TextBox;
                    TextBox txtNotes = row.FindControl("txtItemNotes") as TextBox;

                    decimal unitPrice = 0;
                    if (Roles.IsUserInRole("ADMIN") || Roles.IsUserInRole("CUSTOMER SERVICE REP"))
                    {
                        TextBox txtUnitPrice = row.FindControl("txtUnitPrice") as TextBox;
                        if (txtUnitPrice != null)
                        {
                            decimal.TryParse(txtUnitPrice.Text, out unitPrice);
                        }
                    }

                    if (this._ShoppingCart != null && itemToUpdate != null && txt != null)
                    {
                        int newQty = 0;
                        int.TryParse(txt.Text, out newQty);
                        if (NotesEnabled && txtNotes != null && !string.IsNullOrEmpty(txtNotes.Text))
                        {
                            notes = txtNotes.Text;
                        }
                        UpdateItemQuantity(GUID, newQty, notes, unitPrice);
                        GetCartItemCount();
                    }
                }
            }
        }

        protected void UxCart_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (this._ShoppingCart != null)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Retrieve the dropdownlist control from the first column
                    TextBox qty = e.Row.Cells[0].FindControl("txtQty") as TextBox;
                    if (qty != null)
                    {
                        qty.Attributes.Add("rowid", this.rowNumber.ToString());
                    }

                    this.rowNumber++;
                }
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Represents the Shopping Cart Vendors
        /// </summary>
        /// <returns>Returns the list of shopping cart vendors</returns>
        private System.Collections.Generic.List<int> GetShoppingCartVendors()
        {
            System.Collections.Generic.List<int> list = new System.Collections.Generic.List<int>();

            if (this._ShoppingCart != null)
            {
                foreach (ZNodeShoppingCartItem cartItem in this._ShoppingCart.ShoppingCartItems)
                {
                    int portalId = 0;
                    if (cartItem.Product.PortalID > 0)
                    {
                        portalId = cartItem.Product.PortalID;
                    }

                    if (!list.Contains(portalId))
                    {
                        list.Add(portalId);
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Check for Quantity ordered against Quantity On Hand
        /// </summary>
        /// <param name="item">ShoppintCartItem instance</param>
        /// <returns>Returns the Quantity ordered against Quantity on Hand</returns>
        private bool UpdateQuantity(ZNodeShoppingCartItem item)
        {
            ZNodeShoppingCartItem _ShoppingCartItem = null;

            int QunatityOrdered = 0;

            // Loop through the grid rows
            foreach (GridViewRow row in uxCart.Rows)
            {
                TextBox txtQty = row.FindControl("txtQty") as TextBox;
                int Qty = int.Parse(txtQty.Text);

                // Find the hidden form field and get product GUID
                HiddenField hdnGUID = row.FindControl("GUID") as HiddenField;
                string GUID = hdnGUID.Value;

                // Get the item using GUID
                _ShoppingCartItem = this._ShoppingCart.GetItem(GUID);

                // If product is removed or update with 0,then this object is set to null.
                // (Above GetItem() method will return the value for this object)
                if (_ShoppingCartItem != null)
                {
                    // Match Product
                    if (item.Product.ProductID == _ShoppingCartItem.Product.ProductID)
                    {
                        // Check Product has attributes or not 
                        if (item.Product.SelectedSKU.SKUID > 0)
                        {
                            // If Product has attributes then check for SKUID
                            if (item.Product.SelectedSKU.SKUID == _ShoppingCartItem.Product.SelectedSKU.SKUID)
                            {
                                QunatityOrdered += Qty;
                            }
                        }
                        else
                        {
                            // Product has no attributes. Increment QuantityOrdered value
                            QunatityOrdered += Qty;
                        }
                    }
                }
            }

            // Check Qunatity on hand is greater than Qunatity Ordered value
            //Perficient Custom Code to allow Admin with Out of stock products:starts
            if (allowOutOfStockToUser)
            {
                return true;
            }
            //Perficient Custom Code to allow Admin with Out of stock products:Ends
            else if (item.Product.QuantityOnHand < QunatityOrdered)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion

        #region Zeon Custom Methods and events

        /// <summary>
        ///  Represents the UpdateQuantity method
        /// </summary>
        /// <param name="gridRow"></param>
        public void UpdateQuantity(GridViewRow gridRow)
        {
            //Zeon Custom Code -- Start
            TextBox txtQty = gridRow.FindControl("txtQty") as TextBox;
            int rowId = Convert.ToInt32(txtQty.Attributes["rowid"]);
            int qty = 1;
            if (!int.TryParse(txtQty.Text, out qty))
            {
                qty = 1;
            }
            //Zeon Custom Code -- End  

            HiddenField hdnGUID = uxCart.Rows[rowId].Cells[0].FindControl("GUID") as HiddenField;
            string GUID = hdnGUID.Value;

            // Get Shopping cart item using GUID value
            ZNodeShoppingCartItem item = this._ShoppingCart.GetItem(GUID);

            // Update quantity in the shopping cart manager
            if (qty.Equals(0))
            {
                this._ShoppingCart.RemoveFromCart(GUID);

                // Remove the item from SavedCart Info
                ZNodeSavedCart savedCart = new ZNodeSavedCart();
                savedCart.RemoveSavedCartLineItem(item.Product.SelectedSKU.SKUID);
            }
            else
            {
                // If Product is set to Allow back order or Track inventory is not enabled
                // then Update the quantity  
                if (allowOutOfStockToUser)
                {
                    // Update Quantity for this product
                    this._ShoppingCart.UpdateItemQuantity(GUID, qty);
                }
                else if (item.Product.AllowBackOrder)
                {
                    // Update Quantity for this product
                    this._ShoppingCart.UpdateItemQuantity(GUID, qty);
                }
                else if (item.Product.AllowBackOrder == false && item.Product.TrackInventoryInd == false)
                {
                    // Update Quantity for this product
                    this._ShoppingCart.UpdateItemQuantity(GUID, qty);
                }
                else
                {
                    // Check for available quantity (which included both Available + Quantity Ordered)
                    if (this.UpdateQuantity(item))
                    {
                        this._ShoppingCart.UpdateItemQuantity(GUID, qty);
                    }
                }

                // Update quantity to SavedCart Info
                item = this._ShoppingCart.GetItem(GUID);
                ZNodeSavedCart.AddToSavedCart(item);
            }
        }

        /// <summary>
        /// Handeles lbtnUpdateCart_click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbtnUpdateCart_click(object sender, EventArgs e)
        {
            foreach (GridViewRow grView in uxCart.Rows)
            {
                this.UpdateQuantity(grView);
            }
            //Zeon Custom Code To update item  Notes 
            if (GetNotesVisibility()) { this.UpdateCartItems(); };
            GetCartItemCount();
        }

        /// <summary>
        /// Update Shopping Cart Item Notes
        /// </summary>
        public void UpdateCartItems()
        {
            foreach (GridViewRow grView in uxCart.Rows)
            {
                TextBox txtNotes = grView.FindControl("txtItemNotes") as TextBox;
                HiddenField hdnGUID = grView.FindControl("hdnGUID") as HiddenField;
                if (txtNotes != null && hdnGUID != null)
                {
                    string GUID = hdnGUID.Value;
                    ZNodeShoppingCartItem itemToUpdate = this._ShoppingCart.GetItem(GUID);
                    if (itemToUpdate != null)
                    {
                        itemToUpdate = _ShoppingCart.GetItem(GUID);
                        itemToUpdate.Notes = txtNotes.Text;
                        ZNodeSavedCart.AddToSavedCart(itemToUpdate);
                    }
                }
            }
        }

        /// <summary>
        /// set Notes Visibility for Shopping Cart Item
        /// </summary>
        /// <returns></returns>
        public bool GetNotesVisibility()
        {
            bool isNotesEnabled = false;
            if (ConfigurationManager.AppSettings["ShoppingCartNotesPortal"] != null && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["ShoppingCartNotesPortal"]))
            {
                string portalList = ConfigurationManager.AppSettings["ShoppingCartNotesPortal"].ToString().Trim();
                string[] allPortals = null;
                if (portalList.IndexOf(',') > 0)
                {
                    allPortals = portalList.Split(',');
                    foreach (string strPortal in allPortals)
                    {
                        if (strPortal.Equals(ZNodeConfigManager.SiteConfig.PortalID.ToString()))
                        {
                            isNotesEnabled = true;
                            break;
                        }
                    }
                }
                else
                {
                    if (portalList.Equals(ZNodeConfigManager.SiteConfig.PortalID.ToString()))
                    {
                        isNotesEnabled = true;
                    }

                }
            }
            return isNotesEnabled;
        }


        /// <summary>
        /// set cart item count explicetely
        /// </summary>
        private string GetCartItemCount()
        {
            ZNodeShoppingCart ShoppingCart = (ZNodeShoppingCart)ZNodeCheckout.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
            string cartCount = "0", cartTotal = "$0.00";
            if (ShoppingCart != null)
            {
                cartCount = GetShoppingCartTotalItemCount(ShoppingCart.ShoppingCartItems);
                cartTotal = ShoppingCart.SubTotal.ToString("c");
            }
            ZHeader Zheader = (ZHeader)this.Page.Master.Master.FindControl("uxZHeader");
            if (Zheader != null)
            {
                Controls_Default_Common_CartItemCount cartItemCount = (Controls_Default_Common_CartItemCount)Zheader.FindControl("CART_ITEM_COUNT");
                if (cartItemCount != null)
                {
                    Label lblCartCount = (Label)cartItemCount.FindControl("lblCartItemCount");
                    Label lblCartSubTotal = (Label)cartItemCount.FindControl("lblCartSubTotal");
                    lblCartCount.Text = cartCount.ToString();
                    lblCartSubTotal.Text = cartTotal;
                }
            }
            return cartCount;
        }

        /// <summary>
        /// Handles Checkout_Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Checkout_Click(object sender, EventArgs e)
        {
            if (ProceedToCheckoutEvent != null)
            {
                ProceedToCheckoutEvent(sender, e);
            }
        }

        /// <summary>
        /// UpdateQuantity for item in shopping cart
        /// </summary>
        /// <param name="gridRow"></param> 
        public void UpdateItemQuantity(string GUID, int qty, string notes, decimal unitPrice)
        {
            // Get Shopping cart item using GUID value
            ZNodeShoppingCartItem item = this._ShoppingCart.GetItem(GUID);

            // Update quantity in the shopping cart manager
            if (qty.Equals(0))
            {
                this._ShoppingCart.RemoveFromCart(GUID);

                // Remove the item from SavedCart Info
                ZNodeSavedCart savedCart = new ZNodeSavedCart();
                savedCart.RemoveSavedCartLineItem(item.Product.SelectedSKU.SKUID);
            }
            else
            {

                // If Product is set to Allow back order or Track inventory is not enabled
                // then Update the quantity  

                if (item.Product.AllowBackOrder)
                {
                    // Update Quantity for this product
                    this._ShoppingCart.UpdateItemQuantity(GUID, qty);
                }
                else if (item.Product.AllowBackOrder == false && item.Product.TrackInventoryInd == false)
                {
                    // Update Quantity for this product
                    this._ShoppingCart.UpdateItemQuantity(GUID, qty);
                }
                else
                {
                    // Check for available quantity (which included both Available + Quantity Ordered)
                    if (this.UpdateQuantity(item))
                    {
                        this._ShoppingCart.UpdateItemQuantity(GUID, qty);
                    }
                }
                //Update Cart  Item Notes
                if (!string.IsNullOrEmpty(notes))
                {
                    item.Notes = notes;
                }

                if (Roles.IsUserInRole("ADMIN") || Roles.IsUserInRole("CUSTOMER SERVICE REP"))
                {
                    item.UnitPriceOverride = unitPrice;
                }
                // Update quantity to SavedCart Info
                item = this._ShoppingCart.GetItem(GUID);
                ZNodeSavedCart.AddToSavedCart(item);
            }
        }

        /// <summary>
        /// get Shopping cart item total quantity count
        /// </summary>
        /// <returns></returns>
        private string GetShoppingCartTotalItemCount(ZNodeGenericCollection<ZNodeShoppingCartItem> shoppingCartItems)
        {
            int totalQuantity = 0;
            foreach (ZNodeShoppingCartItem item in shoppingCartItems)
            {
                totalQuantity += item.Quantity;
            }
            return totalQuantity.ToString();
        }

        #endregion

    }
}