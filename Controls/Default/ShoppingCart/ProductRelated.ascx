<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_ShoppingCart_ProductRelated" CodeBehind="ProductRelated.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Default/Reviews/ProductAverageRating.ascx" TagName="ProductAverageRating" TagPrefix="ZNode" %>

<asp:Panel ID="pnlRelatedProduct" runat="server" Visible="false">
    <div id="CartItemRelatedProducts">
        <div class="CustomTitle">
            <uc1:CustomMessage ID="RelatedProductTitle" EnableViewState="<%# EnableViewstate %>" MessageKey="ProductRelatedProductsTitle" runat="server"></uc1:CustomMessage>
        </div>
        <div id="relProdShoppingCart" runat="server">
            <ul id="ProductRelatedShop" class="FlexiSliderImageShow" runat="server">
                <asp:Repeater ID="RelatedProductList" runat="server">
                    <ItemTemplate>
                        <li>
                            <div class="item">
                                <div class="Cart_CrossSellItem_element">
                                    <div class="CartRelatedItem">
                                        <div class="Image">
                                            <div class="ItemType">
                                                <%--<asp:Image ID="NewItemImage" EnableViewState="false" runat="server" ImageUrl='<%#ResolveUrl(ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("new.png")) %>'
                                                    meta:resourcekey="NewItemImageResource1" Visible='<%#DataBinder.Eval(Container.DataItem, "NewProductInd")%>' ClientIDMode="Static"  AlternateText="New" alt="New"/>--%>
                                                <span class="rs-new" id="spnRSNew" runat="server" visible='<%#DataBinder.Eval(Container.DataItem, "NewProductInd")%>'><span class="rs-new-text">New</span></span>
                                                &nbsp;<asp:Image EnableViewState="false" ID="FeaturedItemImage" runat="server" ImageUrl='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("sale.png") %>'
                                                    meta:resourcekey="FeaturedItemImageResource1" Visible='<%# DataBinder.Eval(Container.DataItem, "FeaturedInd") %>' ClientIDMode="Static" AlternateText="Featured" ToolTip="Featured"/>
                                            </div>
                                            <a id="A1" enableviewstate="<%# EnableViewstate %>" href='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()%>' runat="server">
                                                <img id="img1" enableviewstate="<%# EnableViewstate %>" name='<%# "img"+DataBinder.Eval(Container.DataItem, "ProductID")%>' border='0' alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag")%>' src='<%#  GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>' runat="server" title='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag")%>' />
                                            </a>
                                        </div>
                                        <div class="StarRating">
                                            <ZNode:ProductAverageRating ID="uxProductAverageRating" ProductName='<%# DataBinder.Eval(Container.DataItem, "Name").ToString()%>' TotalReviews='<%# DataBinder.Eval(Container.DataItem, "TotalReviews") %>' ViewProductLink='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink")%>' ReviewRating='<%# DataBinder.Eval(Container.DataItem, "ReviewRating")%>' runat="server" />
                                        </div>
                                        <div class="ShortDescription">
                                            <asp:Label ID="ShortDescription" runat="server" Text='<%# WebApp.ZCommonHelper.GetFormattedProductName(((ZNode.Libraries.ECommerce.Catalog.ZNodeCrossSellItem)Container.DataItem).ShortDescription.ToString(), System.Configuration.ConfigurationManager.AppSettings["ProductGridNameMaxLimit"].ToString())%>' meta:resourcekey="ShortDescriptionResource1" EnableViewState="false" />
                                        </div>
                                        <div class="DetailLink">
                                            <asp:HyperLink ID="hlName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Name").ToString()%>' NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()%>'></asp:HyperLink>
                                            <span class="Price">
                                                <asp:Label ID="Label1" EnableViewState="<%# EnableViewstate %>" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FormattedPrice") %>' Visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && RelatedProductProfile.ShowPrice %>'></asp:Label>
                                        </div>
                                        <div class="CallForPrice">
                                            <asp:Label EnableViewState="<%# EnableViewstate %>" ID="uxCallForPricing" runat="server" Text='<%# CheckForCallForPricing(DataBinder.Eval(Container.DataItem, "CallForPricing")) %>' CssClass="Price" />
                                        </div>
                                        <%--<asp:HyperLink ID="HyperLink1" CssClass="Button View"
                                            EnableViewState="<%# EnableViewstate %>"
                                            meta:resourcekey="btnbuyResource1"
                                            runat="server"
                                            Visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && RelatedProductProfile.ShowAddToCart %>'
                                            NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink")%>'>View �</asp:HyperLink>--%>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
    </div>
</asp:Panel>
