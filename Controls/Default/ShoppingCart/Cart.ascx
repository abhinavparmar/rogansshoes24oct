<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_ShoppingCart_Cart" CodeBehind="Cart.ascx.cs" %>
<%@ Register Src="~/Controls/Default/navigation/NavigationBreadCrumbs.ascx" TagName="StoreBreadCrumbs" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Common/CustomerServicePhone.ascx" TagName="CustomerServicePhoneNo" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Common/spacer.ascx" TagName="Spacer" TagPrefix="uc1" %>
<%@ Register Src="ShoppingCart.ascx" TagName="ShoppingCart" TagPrefix="ZNode" %>
<%@ Register Src="ProductRelated.ascx" TagName="ProductRelated" TagPrefix="ZNode" %>
<%@ Register Src="zQuickOrder.ascx" TagName="zQuickOrder" TagPrefix="Zeon" %>
<div class="ShoppingCart">
    <div class="BreadCrumb">
        <span class="PromoText">
            <ZNode:CustomMessage ID="StoreSpecialsHomeCustomMessage5" EnableViewState="false" MessageKey="StoreSpecials" runat="server" ClientIDMode="Static" Visible="false" />
        </span>
    </div>
    <%--<hr class="Horizontal" />--%>
</div>
<div class="ShoppingCart">
    <div class="CartTitleArea Clearfix">
        <div>
            <div class="PageTitle">
                <asp:Localize ID="Localize3" EnableViewState="false" meta:resourceKey="txtShoppingCart" runat="server" ClientIDMode="Static" />
            </div>
            <div class="CheckoutBox">
                <asp:LinkButton ID="lbtnApplyTeamPrice" runat="server" AlternateText="Apply Team Price" OnClick="ApplyTeamPrice_Click" Visible='<%# Roles.IsUserInRole("ADMIN") || Roles.IsUserInRole("CUSTOMER SERVICE REP")%>'
                    ValidationGroup="groupCart" CssClass="checkout ApplyTeamPrice" ClientIDMode="Static">Apply Team Price</asp:LinkButton>
              
                 <asp:LinkButton ID="lbtnApplyRetailPrice" runat="server" AlternateText="Apply Retail Price" OnClick="ApplyRetailPrice_Click" Visible='<%# Roles.IsUserInRole("ADMIN") || Roles.IsUserInRole("CUSTOMER SERVICE REP")%>'
                    ValidationGroup="groupCart" CssClass="checkout ApplyTeamPrice" ClientIDMode="Static">Apply Retail Price </asp:LinkButton>
                
                <asp:LinkButton ID="lnkProceeedToCheckout" runat="server" AlternateText="Check Out" OnClick="Checkout_Click"
                    ValidationGroup="groupCart" CssClass="checkout" meta:resourcekey="Checkout1Resource1" ClientIDMode="Static" ToolTip="Proceed to Checkout">Proceed to Checkout <i class="fa fa-arrow-circle-right"></i></asp:LinkButton>
            </div>
        </div>
        <div class="Error">
            <div class="uxMsg ErrorSection" runat="server" id="divErrorMessage" visible="false" enableviewstate="false">
                <em>
                    <asp:Label ID="uxMsg" EnableViewState="false" runat="server" meta:resourcekey="uxMsgResource1" ClientIDMode="Static"></asp:Label></em>
            </div>
        </div>
    </div>
    <asp:Panel ID="pnlShoppingCart" runat="server" meta:resourcekey="pnlShoppingCartResource1" ClientIDMode="Static">
        <div class="Error">
            <asp:Label ID="uxErrorMsg" EnableViewState="false" runat="server" meta:resourcekey="uxErrorMsgResource1" ClientIDMode="Static"></asp:Label>
        </div>
        <div>
            <ZNode:ShoppingCart ID="uxCart" runat="server" />
        </div>
        <%--<div class="CheckoutBox" style="float: right;">
            <asp:LinkButton ID="ContinueShopping1" runat="server" AlternateText="Continue Shopping"
                OnClick="ContinueShopping1_Click" ValidationGroup="groupCart" CssClass="Button GrayButton"
                meta:resourcekey="ContinueShopping1Resource1" CausesValidation="false" ClientIDMode="Static">Continue Shopping</asp:LinkButton>
            <asp:LinkButton ID="Checkout1" runat="server" AlternateText="Check Out" OnClick="Checkout_Click"
                ValidationGroup="groupCart" CssClass="checkout" meta:resourcekey="Checkout1Resource1" ClientIDMode="Static">Proceed to Checkout</asp:LinkButton>
            <br />
        </div>--%>
    </asp:Panel>
    <div class="CheckoutBox" runat="server" id="divContinueShopping">
        <asp:LinkButton ID="lnkContinueShopping" runat="server" AlternateText="Continue Shopping"
            OnClick="ContinueShopping1_Click" ValidationGroup="groupCart" CssClass="Button" ToolTip="Continue Shopping"
            meta:resourcekey="ContinueShopping1Resource1" CausesValidation="false" ClientIDMode="Static"><asp:Localize runat="server" meta:resourcekey="lnkContinueShopping"></asp:Localize></asp:LinkButton>
    </div>
</div>
<div class="UpSell">
    <ZNode:ProductRelated ID="uxProductRelatedItems" runat="server" />
</div>
<%--<div class="ShoppingCart">
    <div class="ShippingText">
        <ZNode:CustomMessage ID="ShoppingCartShippingTextMessage" EnableViewState="false" MessageKey="ShoppingCartShippingText" runat="server" ClientIDMode="Static" />
    </div>
</div>--%>
