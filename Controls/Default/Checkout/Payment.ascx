<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Checkout_Payment"
    CodeBehind="Payment.ascx.cs" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="spacer" TagPrefix="uc1" %>
<%@ Register TagPrefix="ZNode" Assembly="ZNode.Libraries.ECommerce.Catalog" Namespace="ZNode.Libraries.ECommerce.Catalog" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="Znode" %>
<%@ Register Src="~/Controls/Default/Checkout/PerftMultipleCardTender.ascx" TagName="MulitCreditCard" TagPrefix="Perficient" %>
<asp:PlaceHolder runat="server" ID="ErrorPanel" Visible="false">
    <div class="Form">
        <div class="HeaderStyle">
            <asp:Localize ID="Localize16" runat="server" meta:resourceKey="txtPaymentInformation"></asp:Localize>
        </div>
        <div class="clear">
            <asp:Label CssClass="Error" ID="lblError" runat="server" EnableViewState="False"
                meta:resourcekey="lblErrorResource1" ClientIDMode="Static"></asp:Label>
        </div>
    </div>
</asp:PlaceHolder>
<asp:Panel ID="pnlAddress" CssClass="PNLAddress" runat="server" meta:resourcekey="pnlAddressResource1" ClientIDMode="Static">
    <div>
        <%--<div class="HeaderStyle">
            <asp:Localize ID="Localize18" runat="server" meta:resourceKey="txtAddress"></asp:Localize>
        </div>--%>
        <table>
            <tr>
                <td class="BillingAddressContent">
                    <h3>
                        <asp:Localize ID="Localize2" runat="server" meta:resourceKey="txtBillingAddress"></asp:Localize>:
                    </h3>
                    <div class="AddressDetails">
                        <asp:Label ID="lblBillingAddress" runat="server" ClientIDMode="Static"></asp:Label>
                        <br />
                        <div class="Clear">
                            <asp:LinkButton ID="lnkEditBilling" runat="server" Text="Edit Address" OnClick="LnkEditBilling_Click"
                                CausesValidation="False" meta:resourcekey="txtEditBillingShippingAddress" ClientIDMode="Static"></asp:LinkButton>
                        </div>
                    </div>
                </td>
                <td class="ShippingAddressContent">
                    <h3>
                        <asp:Localize ID="Localize20" runat="server" meta:resourceKey="txtShippingAddress" ClientIDMode="Static"></asp:Localize>:
                    </h3>
                    <div class="AddressDetails">
                        <asp:Label ID="lblShippingAddress" runat="server" ClientIDMode="Static"></asp:Label><br />
                        <div class="Clear" style="display: block">
                            <asp:LinkButton ID="lnkEditShipping" runat="server" Text="Edit Address" OnClick="LnkEditShipping_Click"
                                CausesValidation="False" meta:resourcekey="txtEditBillingShippingAddress" ClientIDMode="Static"></asp:LinkButton>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Panel>

<asp:Panel ID="pnlPayment" runat="server" meta:resourcekey="pnlPaymentResource1" ClientIDMode="Static" CssClass="PaymentInformation">
    <div class="Form">
        <h3>
            <asp:Localize ID="Localize1" runat="server" meta:resourceKey="txtPaymentMethod"></asp:Localize>
        </h3>
        <ul id="PaymentTypeRadioButtonList" class="PaymentTypeRadioButtonList">
            <li>
                <asp:Panel ID="pnlPaymentType" runat="server" ClientIDMode="Static">
                    <asp:PlaceHolder runat="server" ID="PaymentTypePanel" ClientIDMode="Static">
                        <div>
                            <%-- <asp:DropDownList ID="lstPaymentType" runat="server" OnSelectedIndexChanged="LstPaymentType_SelectedIndexChanged"
                                        AutoPostBack="True" ClientIDMode="Static">
                                    </asp:DropDownList>--%>

                            <asp:RadioButtonList ID="rdolstPaymentType" runat="server" OnSelectedIndexChanged="LstPaymentType_SelectedIndexChanged" CausesValidation="false"
                                AutoPostBack="True" RepeatDirection="Vertical" RepeatLayout="UnorderedList" />
                        </div>
                        <div class="LeftContent ValueField">
                            <asp:LinkButton ID="lnkRemoveCardData" runat="server" Text="Remove this card" OnClick="LnkRemoveCardData_Click"
                                Visible="false" ClientIDMode="Static" ToolTip="Remove this card"></asp:LinkButton>
                        </div>
                    </asp:PlaceHolder>
                </asp:Panel>
            </li>
        </ul>
        <ul class="PaymentType">
            <li>
                <div>
                    <asp:Panel ID="pnlPurchaseOrder" CssClass="PNLCreditCard" runat="server" Visible="False" meta:resourcekey="pnlPurchaseOrderResource1" ClientIDMode="Static">
                        <div class="Row Clear">
                            <div class="FieldStyle LeftContent">
                                <label class="CardName">
                                    <asp:Localize ID="Localize7" runat="server" meta:resourceKey="txtPONumber"></asp:Localize>
                                </label>
                            </div>
                            <div class="LeftContent">
                                <asp:TextBox ID="txtPONumber" runat="server" Columns="30" MaxLength="100"
                                    AutoCompleteType="Disabled" meta:resourcekey="txtPONumberResource1" ClientIDMode="Static"></asp:TextBox>
                                <div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" ControlToValidate="txtPONumber"
                                        runat="server" Display="Dynamic" CssClass="Error" meta:resourcekey="Requiredfieldvalidator1Resource1"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>

                <asp:Panel ID="pnlCreditCard" CssClass="PNLCreditCard" runat="server" Visible="false">
                    <%--Additional Tender/Credit Card Section Starts--%>
                    <div class="MultiCardError Error" runat="server" id="div">
                        <asp:Label ID="lblMultiCardErr" runat="server" ClientIDMode="Static" meta:resourcekey="lblMultiCardErr" CssClass="CCError" EnableViewState="false" Style="display: none;"></asp:Label>
                        <asp:Label ID="lblCardAmtTotalErr" runat="server" ClientIDMode="Static" Style="display: none;" class="CCError"></asp:Label>
                        <span class="uxMsg ErrorSection" runat="server" id="spanCardErr" visible="false">
                            <em>
                                <asp:Label ID="lblCardPayStatus" runat="server" ClientIDMode="Static" Visible="false"></asp:Label>
                            </em>
                        </span>
                    </div>
                    <%--Additional Tender/Credit Card Section Ends--%>
                    <div class="CreditCardList">
                        <ul>
                            <li>
                                <label class="CardName">
                                    <asp:Localize ID="Localize3" runat="server" meta:resourceKey="txtCardNumber"></asp:Localize>
                                </label>
                            </li>
                            <li>
                                <div class="CreditCardNumber">
                                    <asp:TextBox ID="txtCreditCardNumber" runat="server" Columns="30" MaxLength="20"
                                        autocomplete="off" meta:resourcekey="txtCreditCardNumberResource1" ClientIDMode="Static" CssClass="CCError"></asp:TextBox>&nbsp;&nbsp;
                       
                               <div class="Req1">
                                   <asp:RequiredFieldValidator ID="req1" runat="server" ControlToValidate="txtCreditCardNumber"
                                       Display="Dynamic" meta:resourcekey="req1Resource1" CssClass="CCError"></asp:RequiredFieldValidator>
                               </div>
                                    <div class="RegularExpressionValidator">
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtCreditCardNumber"
                                            Display="Dynamic" meta:resourcekey="RegularExpressionValidator1Resource1" CssClass="CCError"></asp:RegularExpressionValidator>
                                        <asp:CustomValidator ID="cardNumValidator" ControlToValidate="txtCreditCardNumber" Display="Dynamic"
                                            runat="server" ClientValidationFunction="perfiPayment.ValidateAllCardNumber"
                                            ValidateEmptyText="false" EnableViewState="false"></asp:CustomValidator>
                                    </div>
                                    <div class="MyValidator">
                                        <Znode:ZNodeCreditCardValidator ID="MyValidator" runat="server" AcceptedCardTypes="All, Unknown"
                                            ControlToValidate="txtCreditCardNumber" Display="Dynamic" meta:resourcekey="MyValidatorResource1"
                                            ValidateCardType="True" CssClass="CCError">
                                        </Znode:ZNodeCreditCardValidator>
                                    </div>

                                </div>

                                <div class="CreditCardImg">
                                    <img id="imgVisa" src="~/Controls/Images/card_visa.gif" runat="server" alt="visa" title="Visa"
                                        align="absmiddle" clientidmode="Static" />
                                    <img id="imgMastercard" runat="server" align="absmiddle" alt="Mastercard" title="Mastercard"
                                        src="~/Controls/Images/card_mastercard.gif" clientidmode="Static" />
                                    <img id="imgAmex" runat="server" align="absmiddle" src="~/Controls/Images/card_amex.gif" clientidmode="Static" alt="Amex" title="Amex"/>
                                    <img id="imgDiscover" runat="server" align="absmiddle" src="~/Controls/Images/card_discover.gif" clientidmode="Static" alt="Discover" title="Discover" />
                                </div>
                            </li>

                            <li>
                                <label>
                                    <asp:Localize ID="Localize4" runat="server" meta:resourceKey="txtExpirationDate"></asp:Localize>
                                </label>
                            </li>

                            <li>
                                <div class="Month">
                                    <asp:DropDownList ID="lstMonth" runat="server" meta:resourcekey="lstMonthResource1" ClientIDMode="Static">
                                        <asp:ListItem Value="0" meta:resourcekey="ListItemResource1">-- Month --</asp:ListItem>
                                        <asp:ListItem Value="01" meta:resourcekey="ListItemResource2">Jan</asp:ListItem>
                                        <asp:ListItem Value="02" meta:resourcekey="ListItemResource3">Feb</asp:ListItem>
                                        <asp:ListItem Value="03" meta:resourcekey="ListItemResource4">Mar</asp:ListItem>
                                        <asp:ListItem Value="04" meta:resourcekey="ListItemResource5">Apr</asp:ListItem>
                                        <asp:ListItem Value="05" meta:resourcekey="ListItemResource6">May</asp:ListItem>
                                        <asp:ListItem Value="06" meta:resourcekey="ListItemResource7">Jun</asp:ListItem>
                                        <asp:ListItem Value="07" meta:resourcekey="ListItemResource8">Jul</asp:ListItem>
                                        <asp:ListItem Value="08" meta:resourcekey="ListItemResource9">Aug</asp:ListItem>
                                        <asp:ListItem Value="09" meta:resourcekey="ListItemResource10">Sep</asp:ListItem>
                                        <asp:ListItem Value="10" meta:resourcekey="ListItemResource11">Oct</asp:ListItem>
                                        <asp:ListItem Value="11" meta:resourcekey="ListItemResource12">Nov</asp:ListItem>
                                        <asp:ListItem Value="12" meta:resourcekey="ListItemResource13">Dec</asp:ListItem>
                                    </asp:DropDownList>
                                    <div>
                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" ControlToValidate="lstMonth"
                                            runat="server" Display="Dynamic" InitialValue="0" ErrorMessage="Select Month" CssClass="CCError"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="Year">
                                    <asp:DropDownList ID="lstYear" runat="server" meta:resourcekey="lstYearResource1" ClientIDMode="Static">
                                    </asp:DropDownList>

                                    <asp:CustomValidator ID="cstVMonthValidator" ControlToValidate="lstMonth" Display="Dynamic" runat="server"
                                        ClientValidationFunction="zeonCheckout.validateCardForExpiry"
                                        ValidateEmptyText="true" CssClass="CCError"></asp:CustomValidator>
                                    <asp:CustomValidator ID="cstVYearValidator" ControlToValidate="lstYear" Display="Dynamic"
                                        runat="server" ClientValidationFunction="zeonCheckout.validateCardForExpiry"
                                        ValidateEmptyText="true" CssClass="CCError"></asp:CustomValidator>
                                    <div>
                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" ControlToValidate="lstYear"
                                            runat="server" Display="Dynamic" InitialValue="0" ErrorMessage="Select Year" CssClass="CCError"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <asp:Label ID="lblCardExpiryMessage" runat="server" meta:resourceKey="lblCardExpiryMessage"
                                    CssClass="CCError" Style="display: none;" ClientIDMode="Static"></asp:Label>
                            </li>

                            <li>
                                <label>
                                    <asp:Localize ID="Localize6" runat="server" meta:resourceKey="txtSecurityCode"></asp:Localize></label>
                            </li>
                            <li>
                                <div class="SecurityCode">
                                    <asp:TextBox ID="txtCVV" runat="server" Columns="30" MaxLength="4" autocomplete="off"
                                        meta:resourcekey="txtCVVResource1" ClientIDMode="Static"></asp:TextBox>&nbsp;&nbsp;
                    <asp:HyperLink ID="CVVhyperlink" EnableViewState="false" runat="server"
                        ClientIDMode="Static"></asp:HyperLink>
                                    <div>
                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" ControlToValidate="txtCVV"
                                            runat="server" Display="Dynamic" meta:resourcekey="Requiredfieldvalidator2Resource1" CssClass="CCError"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtCVV"
                                            Display="Dynamic" meta:resourcekey="RegularExpressionValidator2Resource1" CssClass="CCError"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <%--Additional Tender/Credit Card Section Starts--%>
                        <div id="divDefaultCardTotal" class="PriceBox" style="display: none;">
                            <ul>
                                <li>
                                    <label>
                                        <asp:Localize ID="locAmount" runat="server" meta:resourceKey="locAmount"></asp:Localize>
                                    </label>
                                </li>
                                <li>
                                    <asp:TextBox ID="txtDefaultCCTotal" runat="server" onblur="perfiPayment.ManageCreditCardAmount(0,event)" CssClass="TenderTotal"></asp:TextBox>

                                </li>
                                <div>
                                    <asp:RequiredFieldValidator ID="revTotal" runat="server" ControlToValidate="txtDefaultCCTotal" meta:resourcekey="reqtxtDefaultCCTotal" Display="Dynamic" EnableViewState="false"></asp:RequiredFieldValidator>
                                    <asp:RequiredFieldValidator ID="revTotalMoreThanZero" runat="server" ControlToValidate="txtDefaultCCTotal" meta:resourcekey="reqtxtDefaultCCTotal" Display="Dynamic" EnableViewState="false" InitialValue="0"></asp:RequiredFieldValidator>
                                    <asp:RequiredFieldValidator ID="reqtxtDefaultCCTotal" runat="server" ControlToValidate="txtDefaultCCTotal" meta:resourcekey="reqtxtDefaultCCTotal" CssClass="CCError" InitialValue="0.00" Display="Dynamic" EnableViewState="false"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regtxtDefaultCCTotal" runat="server" ControlToValidate="txtDefaultCCTotal" meta:resourcekey="regtxtDefaultCCTotal" CssClass="CCError" Display="Dynamic" EnableViewState="false"></asp:RegularExpressionValidator>
                                </div>
                            </ul>
                        </div>
                    </div>
                    <div id="divMultipleTenders" runat="server" visible="false" class="MoreCreditCard">

                        <asp:CustomValidator ID="csvMultipleCardValidator" runat="server" ClientValidationFunction="perfiPayment.ValidateAllCardNumber" ValidateEmptyText="true" ControlToValidate="txtCreditCardNumber" EnableViewState="false" CssClass="CCError"></asp:CustomValidator>
                        <Perficient:MulitCreditCard ID="uxMulTender1" runat="server" ControlInd="1"></Perficient:MulitCreditCard>
                        <Perficient:MulitCreditCard ID="uxMulTender2" runat="server" ControlInd="2"></Perficient:MulitCreditCard>
                        <asp:HiddenField ID="hdnMoreCreditCardCount" runat="server" Value="2" ClientIDMode="Static" />
                    </div>
                    <div class="AddCart" runat="server" id="divAddMoreCardLink">
                        <a id="addMoreCardLnk" onclick="perfiPayment.AddCreditCardPanel();" style="cursor: pointer;" title="Use Additional Credit Card">Use Additional Credit Card</a>
                    </div>
                    <%--Additional Tender/Credit Card Section Ends--%>
                    <div class="FieldStyle LeftContent">
                        <asp:Localize ID="Localize11" runat="server" meta:resourceKey="rdbPaypal"></asp:Localize>
                    </div>

                </asp:Panel>
                <asp:Panel ID="pnlCOD" runat="server" Visible="False" meta:resourcekey="pnlCODResource1" ClientIDMode="Static">
                    <asp:Localize ID="Localize8" runat="server" meta:resourceKey="txtHint1"></asp:Localize>

                </asp:Panel>
                <div>
                    <asp:HiddenField ID="hfPaymentSettingID" Value="0" runat="server" ClientIDMode="Static" />

                    <%-- Google Checkout Payment Section --%>
                    <asp:Panel ID="pnlGoogleCheckout" runat="server" Visible="false" ClientIDMode="Static">
                        <div class="FieldStyle LeftContent">
                            <asp:Localize ID="Localize15" runat="server"></asp:Localize>
                        </div>
                        <div class="PaymentSection">
                            <asp:ImageButton EnableViewState="true" ID="GoogleSubmitButton" CausesValidation="false"
                                ImageUrl="https://checkout.google.com/buttons/checkout.gif?merchant_id={0}&w=160&h=43&style=white&variant=text&loc=en_US"
                                AlternateText="Google Submit Button" ToolTip="Google Submit Button"
                                runat="server" ImageAlign="AbsMiddle" OnClick="GoogleSubmitButton_Click" ClientIDMode="Static" />
                            <br />
                        </div>
                    </asp:Panel>
                    <div class="Clear">
                    </div>
                    <%-- 2Checkout Payment Section --%>
                    <asp:Panel ID="pnl2CO" runat="server" Visible="false" ClientIDMode="Static">
                        <div class="FieldStyle LeftContent">
                            <asp:Localize ID="Localize17" runat="server"></asp:Localize>
                        </div>
                        <div class="PaymentSection">
                            <asp:LinkButton EnableViewState="true" ID="TwoCheckoutSubmitButton" Text="Pay via 2CO"
                                OnClick="TwoCheckoutSubmitButton_Click" runat="server" CssClass="Button" ClientIDMode="Static" />
                            <br />
                        </div>
                    </asp:Panel>
                </div>
                <asp:Panel ID="pnlIPCCreditCard" runat="server" Visible="False" meta:resourcekey="pnlIPCCreditCardResource1" ClientIDMode="Static">
                    <asp:Localize ID="Localize9" runat="server" meta:resourceKey="txtTBD"></asp:Localize>
                </asp:Panel>
            </li>
        </ul>
        <ul class="PaypalCheckout">
            <li>
                <%--Paypal Payment Section--%>
                <asp:Panel ID="pnlPaypal" runat="server" Visible="false" ClientIDMode="Static">
                    <div class="FieldStyle LeftContent">
                        <asp:Localize ID="Localize12" runat="server"></asp:Localize>
                    </div>
                    <div class="PaymentSection">
                        <%--<asp:ImageButton runat="server" EnableViewState="false" CausesValidation="false"
                                    ID="Paypal" ImageUrl="https://cms.paypal.com/cms_content/US/en_US/images/developer/EC-button.gif"
                                    AlternateText="Acceptance Mark" ImageAlign="AbsMiddle" OnClick="Paypal_Click" />--%>
                        <asp:ImageButton runat="server" EnableViewState="false" CausesValidation="false"
                            ID="Paypal" ImageUrl="/Themes/Default/Images/btn_xpressCheckout.gif"
                            AlternateText="Acceptance Mark" ToolTip="Acceptance Mark" ImageAlign="AbsMiddle" OnClick="Paypal_Click" ClientIDMode="Static" />
                        <br />
                    </div>
                </asp:Panel>
            </li>
        </ul>

        <table>
            <tr>
                <td class="PaymentContent">


                    <%--Credit Card Payment Section--%>
                    
                </td>
                <td style="vertical-align: top;"></td>
            </tr>
        </table>


        <%--<asp:Panel ID="pnlGiftCard" runat="server" meta:resourcekey="pnlGiftCardResource1" ClientIDMode="Static">
            <div class="Form">
                <div class="HeaderStyle">
                    <asp:Localize ID="Localize19" runat="server" meta:resourceKey="txtGiftCard"></asp:Localize>
                </div>
                <div class="FieldStyle LeftContent">
                </div>
                <div class="LeftContent">
                    <uc1:spacer ID="Spacer3" EnableViewState="false" SpacerHeight="10" SpacerWidth="5"
                        runat="server" ClientIDMode="Static"></uc1:spacer>
                </div>
                <div style="clear: both;"></div>
            </div>
        </asp:Panel>--%>
        <asp:Panel ID="addtionalInstruction" runat="server">
            <div class="GiftcardSpace">
                <uc1:spacer ID="Spacer9" EnableViewState="false" SpacerHeight="2" SpacerWidth="5"
                    runat="server" ClientIDMode="Static" />
                <div class="HeaderStyle">
                    <asp:Localize ID="Localize10" runat="server" meta:resourceKey="txtHint2"></asp:Localize>
                </div>
                <div>
                    <p>
                        <asp:Localize ID="locShippingInstruction" runat="server" meta:resourceKey="txtHint2"></asp:Localize>
                    </p>
                    <asp:TextBox Columns="34" Rows="3" ID="txtAdditionalInstructions" runat="server"
                        TextMode="MultiLine" meta:resourcekey="txtAdditionalInstructionsResource1" ClientIDMode="Static"></asp:TextBox>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="TermandCondition" runat="server">
            <div class="TermsCond">
                <uc1:spacer ID="Spacer1" EnableViewState="false" SpacerHeight="2" SpacerWidth="5"
                    runat="server" ClientIDMode="Static" />
                <div class="HeaderStyle">
                    Accept Terms and Conditions
                </div>
                <div>
                    <asp:CheckBox ID="chkTermsandCondition" runat="server" CssClass="AcceptedAgreement" />
                    <Znode:CustomMessage ID="uxCustomMessage" runat="server" MessageKey="TermsAndConditions" />
                    <asp:CustomValidator runat="server" ID="CustomValidator1" EnableClientScript="true" Display="Dynamic"
                        ClientValidationFunction="CheckBoxRequired_ClientValidate">Please Accept Terms and Conditions to proceed.</asp:CustomValidator>
                </div>
            </div>
            <div class="TermsCond">
                <div>
                    <asp:CheckBox ID="chkSubscribe" runat="server" meta:resourcekey="ltrSubscriptionText" />
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Panel>
