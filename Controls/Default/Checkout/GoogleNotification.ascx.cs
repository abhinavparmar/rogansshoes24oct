using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.Suppliers;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.Google;
using ZNode.Libraries.Google.GoogleCallbackServices;
using ZNode.Libraries.Google.GoogleCheckoutServices;
using ZNode.Libraries.Paypal;
using ZNode.Libraries.ECommerce.ShoppingCart;

namespace WebApp
{
    /// <summary>
    /// Represents the Google Notification user control class.
    /// </summary>
    public partial class Controls_Default_Checkout_GoogleNotification : System.Web.UI.UserControl
    {
        private ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCart _shoppingCart = new ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCart();
        private ZNode.Libraries.ECommerce.ShoppingCart.ZNodeCheckout _checkout = new ZNode.Libraries.ECommerce.ShoppingCart.ZNodeCheckout();
        private ZNodeUserAccount _userAccount = null;

        #region Encoding helper Methods
        /// <summary>
        /// Converts a string to bytes in UTF-8 encoding.
        /// </summary>
        /// <param name="InString">The string to convert.</param>
        /// <returns>The UTF-8 bytes.</returns>
        public static byte[] StringToUtf8Bytes(string InString)
        {
            System.Text.UTF8Encoding utf8encoder = new System.Text.UTF8Encoding(false, true);
            return utf8encoder.GetBytes(InString);
        }

        /// <summary>
        /// Represents the GetTopElement method
        /// </summary>
        /// <param name="Xml">The string to convert. </param>
        /// <returns>The UTF-8 bytes</returns>
        public static string GetTopElement(string Xml)
        {
            return GetTopElement(StringToUtf8Bytes(Xml));
        }

        public static string GetTopElement(byte[] Xml)
        {
            using (MemoryStream ms = new MemoryStream(Xml))
            {
                return GetTopElement(ms);
            }
        }

        /// <summary>
        /// Returns the top element of the xml
        /// </summary>
        /// <param name="Xml">Xml represents the string.</param>
        /// <returns>The UTF-8 bytes.</returns>
        public static string GetTopElement(Stream Xml)
        {
            // Set the begin postion so we can set the stream back when we are done.
            long beginPos = Xml.Position;
            XmlTextReader XReader = new XmlTextReader(Xml);
            XReader.WhitespaceHandling = WhitespaceHandling.None;
            XReader.Read();
            XReader.Read();
            string RetVal = XReader.Name;

            // Do not close the stream, we will still need it for additional operations.
            // Reposition the stream to where it started
            Xml.Position = beginPos;
            return RetVal;
        }
        #endregion

        #region Page Load Event

        /// <summary>
        /// Page Load Event
        /// This will read the Response from  Google API notifications
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Extract the XML from the request.          
            Stream RequestStream = Request.InputStream;
            StreamReader RequestStreamReader = new StreamReader(RequestStream);
            string RequestXml = RequestStreamReader.ReadToEnd();
            bool subscriptionResult = true;
            string responseText = "This gateway does not support all of the billing periods that you have set on products. Please be sure to update the billing period on all your products.";
            int counter = 0;
            int index = 0;
            RequestStream.Close();
            
            //// For testing.
            //XmlTextReader xtr = new XmlTextReader(@"e:\googleorder.xml");
            //XmlDocument xDoc = new XmlDocument();
            //xDoc.Load(xtr);
            //string RequestXml = xDoc.InnerXml;
            //xtr.Close();

            // Redirect to home, if request stream comes with empty.
            if (string.IsNullOrEmpty(RequestXml))
            {
                Response.Redirect("~/default.aspx");
            }

            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Google Notification: " + RequestXml);

            // Serialize the object
            ZNodeSerializer ser = new ZNodeSerializer();

            // Act on the XML.
            switch (GetTopElement(RequestXml))
            {
                case "new-order-notification":
                    NewOrderNotification N1 = (NewOrderNotification)ser.GetContentFromString(RequestXml, typeof(NewOrderNotification));
                    string OrderNumber1 = N1.googleordernumber;
                    string ShipToName = N1.buyershippingaddress.contactname;
                    string ShipToAddress1 = N1.buyershippingaddress.address1;
                    string ShipToAddress2 = N1.buyershippingaddress.address2;
                    string ShipToCity = N1.buyershippingaddress.city;
                    string ShipToState = N1.buyershippingaddress.region;
                    string ShipToZip = N1.buyershippingaddress.postalcode;
                    string TaxCost = N1.orderadjustment.totaltax.Value.ToString();
                    
                    ZNodeSKU SKU = new ZNodeSKU();
                    this._shoppingCart = new ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCart();

                    try
                    {
                        NewOrderNotification Callback = N1;
                        XmlNode[] privatedata = N1.shoppingcart.merchantprivatedata.Any;
                        string serialNo = string.Empty;
                        if (privatedata.Length > 0)
                        {
                            serialNo = privatedata[0].InnerText;
                        }

                        if (HttpRuntime.Cache[serialNo + "cart"] != null)
                        {
                            this._shoppingCart = (ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCart)HttpRuntime.Cache[serialNo + "cart"];
                            this._shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
                            ZNodeUserAccount userAccount = (ZNodeUserAccount)HttpRuntime.Cache[serialNo + "user"];
                            int portalId = (int)HttpRuntime.Cache[serialNo + "portalid"];

                            if (userAccount == null)
                            {
                                userAccount = new ZNodeUserAccount();
                            }

                            // Update Address
                            AccountAdmin accountAdmin = new AccountAdmin();
                            AddressService addressService = new AddressService();
                            ZNode.Libraries.DataAccess.Entities.Address billingAddress = accountAdmin.GetDefaultBillingAddress(userAccount.AccountID);
                            if (billingAddress == null)
                            {
                                billingAddress = new ZNode.Libraries.DataAccess.Entities.Address();
                                billingAddress.IsDefaultBilling = true;
                                billingAddress.AccountID = userAccount.AccountID;
                            }

                            userAccount.BillingAddress = new ZNode.Libraries.DataAccess.Entities.Address();
                            if (N1.buyerbillingaddress.structuredname != null)
                            {
                                billingAddress.FirstName = N1.buyerbillingaddress.structuredname.firstname;
                                billingAddress.LastName = N1.buyerbillingaddress.structuredname.lastname;
                            }
                            else
                            {
                                billingAddress.LastName = N1.buyerbillingaddress.contactname;
                            }

                            billingAddress.Street = N1.buyerbillingaddress.address1;
                            billingAddress.Street1 = N1.buyerbillingaddress.address2;
                            billingAddress.City = N1.buyerbillingaddress.city;
                            billingAddress.StateCode = N1.buyerbillingaddress.region;
                            billingAddress.PostalCode = N1.buyerbillingaddress.postalcode;
                            billingAddress.PostalCode = N1.buyerbillingaddress.countrycode;
                            userAccount.EmailID = N1.buyerbillingaddress.email;
                            userAccount.BillingAddress = billingAddress;

                            ZNode.Libraries.DataAccess.Entities.Address shippingAddress = accountAdmin.GetDefaultBillingAddress(userAccount.AccountID);
                            if (shippingAddress == null)
                            {
                               shippingAddress = new ZNode.Libraries.DataAccess.Entities.Address();
                               shippingAddress.IsDefaultShipping = true;
                               shippingAddress.AccountID = userAccount.AccountID;
                            }

                            if (N1.buyershippingaddress.structuredname != null)
                            {
                                shippingAddress.FirstName = N1.buyershippingaddress.structuredname.firstname;
                                shippingAddress.LastName = N1.buyershippingaddress.structuredname.lastname;
                            }
                            else
                            {
                                shippingAddress.LastName = N1.buyershippingaddress.contactname;
                            }

                            shippingAddress.Street = N1.buyershippingaddress.address1;
                            shippingAddress.Street1 = N1.buyershippingaddress.address2;
                            shippingAddress.City = N1.buyershippingaddress.city;
                            shippingAddress.StateCode = N1.buyershippingaddress.region;
                            shippingAddress.PostalCode = N1.buyershippingaddress.postalcode;
                            shippingAddress.PostalCode = N1.buyershippingaddress.countrycode;
                            userAccount.ShippingAddress = shippingAddress;

                            userAccount.UpdateUserAccount();

                            if (billingAddress.AddressID > 0)
                            {
                                addressService.Update(billingAddress);
                            }
                            else
                            {
                                addressService.Insert(billingAddress);
                            }

                            if (shippingAddress.AddressID > 0)
                            {
                                addressService.Update(shippingAddress);
                            }
                            else
                            {
                                addressService.Insert(shippingAddress);
                            }
                            userAccount.AddToSession(ZNodeSessionKeyType.UserAccount);

                            ZNodeCheckout _checkout = new ZNodeCheckout();
                            _checkout.PaymentSettingID = this.GetByPaymentTypeId(3).PaymentSettingID;                            
                            _checkout.PortalID = portalId;
                            _checkout.ShoppingCart = _shoppingCart;
                            _checkout.ShoppingCart.Payment.BillingAddress = userAccount.BillingAddress;
                            _checkout.ShoppingCart.Payment.ShippingAddress = userAccount.ShippingAddress;
                            _checkout.ShoppingCart.Payment.TransactionID = N1.googleordernumber;                            
                            _checkout.UserAccount = userAccount;

                            ZNodeOrderFulfillment order = new ZNodeOrderFulfillment();
                            order = _checkout.SubmitOrder();
                           

                            ZNodePaymentResponse response = new ZNodePaymentResponse();
                            // Loop through the Order Line Items
                            foreach (OrderLineItem orderLineItem in order.OrderLineItems)
                            {
                                ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCartItem cartItem = _shoppingCart.ShoppingCartItems[counter++];

                                decimal productPrice = (cartItem.TieredPricing + cartItem.ShippingCost + cartItem.TaxCost) * cartItem.Quantity;

                                if (cartItem.Product.RecurringBillingInd && subscriptionResult)
                                {
                                    RecurringBillingInfo recurringBilling = new RecurringBillingInfo();
                                    recurringBilling.InitialAmount = Math.Round(cartItem.Product.RecurringBillingInitialAmount, 2);
                                    recurringBilling.InstallmentInd = cartItem.Product.RecurringBillingInstallmentInd;
                                    recurringBilling.Period = cartItem.Product.RecurringBillingPeriod;
                                    recurringBilling.TotalCycles = cartItem.Product.RecurringBillingTotalCycles;
                                    recurringBilling.Frequency = cartItem.Product.RecurringBillingFrequency;
                                    recurringBilling.Amount = Math.Round(productPrice, 2);
                                    recurringBilling.ProfileName = cartItem.Product.SKU + " - " + orderLineItem.OrderLineItemID.ToString();
                                    recurringBilling.InvoiceNo = orderLineItem.OrderID + " - " + cartItem.Product.SKU;
                                    
                                    ZNode.Libraries.ECommerce.Payment.ZNodePayment  _payment = new ZNode.Libraries.ECommerce.Payment.ZNodePayment();
                                    ZNodeSubscriptionResponse val = _payment.SubmitRecurringBilling(recurringBilling);

                                    if (N1.shoppingcart.merchantprivatedata != null && N1.shoppingcart.merchantprivatedata.Any != null && N1.shoppingcart.merchantprivatedata.Any.Length > 0)
                                    {
                                        if (val.IsSuccess)
                                        {
                                            foreach (XmlNode node in N1.shoppingcart.merchantprivatedata.Any)
                                            {
                                                if (node.LocalName.Trim() == "subscription-id")
                                                {
                                                    val.ReferenceId = orderLineItem.OrderLineItemID;
                                                    val.SubscriptionId = node.InnerText.ToString();
                                                }
                                            }
                                            response.RecurringBillingSubscriptionResponse.Add(val);
                                        }
                                        else
                                        {
                                            subscriptionResult &= false;
                                            responseText = val.ResponseText + val.ResponseCode;
                                            break;
                                        }
                                    }

                                }
                            }
                            // if any subscription fails, then revert all the transactions
                            if (!subscriptionResult)
                            {
                                foreach (ZNodeSubscriptionResponse subscriptionResp in response.RecurringBillingSubscriptionResponse)
                                {
                                   // this.CancelSubscription(subscriptionResp.SubscriptionId);
                                }

                                if (!string.IsNullOrEmpty(response.TransactionId))
                                {
                                    // Revert Value
                                   // this.RefundPayment(response.TransactionId, NonRecurringItemsTotalAmount);
                                }

                                response = new ZNodePaymentResponse();
                                response.ResponseCode = "-1";
                                response.ResponseText = responseText;
                            }
                            
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(string.Format("{0} - {1}", order.OrderID, order.OrderDate));

                            OrderAdmin AdminAccess = new OrderAdmin();
                            ZNode.Libraries.DataAccess.Entities.Order orderEntity = null;

                            orderEntity = AdminAccess.GetOrderByOrderID(order.OrderID);

                            if (orderEntity != null)
                            {
                                if (orderEntity.CardTransactionID == null || orderEntity.CardTransactionID.Length == 0)
                                {
                                    orderEntity.PaymentTypeId = 3;
                                    orderEntity.PaymentSettingID = this.GetByPaymentTypeId(3).PaymentSettingID;
                                    orderEntity.PaymentStatusID = (int)ZNodePaymentStatus.CREDIT_AUTHORIZED;
                                    orderEntity.CardTransactionID = N1.googleordernumber;
                                    orderEntity.Total = N1.ordertotal.Value;

                                    if (N1.orderadjustment.shipping != null)
                                    {
                                        object shipping = N1.orderadjustment.shipping.Item;
                                        if (shipping is ZNode.Libraries.Google.GoogleCheckoutServices.FlatRateShippingAdjustment)
                                        {
                                            FlatRateShippingAdjustment frs = (FlatRateShippingAdjustment)shipping;
                                            ShippingService shipService = new ShippingService();
                                            TList<Shipping> tss = shipService.Find("ShippingCode='" + frs.shippingname + "'");
                                            if (tss.Count > 0)
                                            {
                                                orderEntity.ShippingID = tss[0].ShippingID;
                                            }

                                            orderEntity.ShippingCost = frs.shippingcost.Value;
                                        }

                                        if (shipping is CarrierCalculatedShippingAdjustment)
                                        {
                                            CarrierCalculatedShippingAdjustment ccs = (CarrierCalculatedShippingAdjustment)shipping;
                                            ShippingService shipService = new ShippingService();

                                            string shipcode = string.Empty;
                                            if (ccs.shippingname == "Fedex 2Day")
                                            {
                                                shipcode = "FEDEX_2_DAY";
                                            }
                                            else if (ccs.shippingname.Contains("Fedex"))
                                            {
                                                shipcode = ccs.shippingname.Replace(" ", "_");
                                            }
                                            else if (ccs.shippingname.Contains("UPS"))
                                            {
                                                shipcode = ccs.shippingname.Replace("UPS", "UPS -");
                                            }

                                            TList<Shipping> tss = shipService.Find("ShippingCode='" + shipcode.ToUpper() + "' OR Description='" + shipcode.ToUpper() + "'");
                                            if (tss.Count > 0)
                                            {
                                                orderEntity.ShippingID = tss[0].ShippingID;
                                            }

                                            orderEntity.ShippingCost = ccs.shippingcost.Value;
                                        }
                                    }

                                    AdminAccess.Update(orderEntity);
                                    
                                    // Decrement inventory level according to the AddOn Inventory settings
                                    // UpdateValues(N1);
                                    if (this._shoppingCart != null)
                                    {
                                        this._shoppingCart.PostSubmitOrderProcess();
                                    }
                                }
                            }

                            // Remove ShoppingCart, User and Profile objects from cache.
                            HttpRuntime.Cache.Remove(serialNo + "cart");
                            HttpRuntime.Cache.Remove(serialNo + "user");
                            HttpRuntime.Cache.Remove(serialNo + "portalid");
                        }
                        else
                        {
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Error: Cache does not have for google order: " + N1.googleordernumber + " and order does not create in our database");
                        }
                    }
                    catch (Exception ex)
                    {
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Exception On Google Callback : " + ex.ToString());
                    }

                    break;
                case "risk-information-notification":
                    RiskInformationNotification N2 = (RiskInformationNotification)ser.GetContentFromString(RequestXml, typeof(RiskInformationNotification));
                    
                    // This notification tells us that Google has authorized the order and it has passed the fraud check.
                    // Use the data below to determine if you want to accept the order, then start processing it.
                    {
                        string OrderNumber2 = N2.googleordernumber;
                        string AVS = N2.riskinformation.avsresponse;
                        string CVN = N2.riskinformation.cvnresponse;
                        bool SellerProtection = N2.riskinformation.eligibleforprotection;

                        OrderService orderService = new OrderService();
                        ZNode.Libraries.DataAccess.Entities.Order orderEntity = null;

                        orderEntity = orderService.Find("CardTransactionID='" + OrderNumber2 + "'")[0];
                        orderEntity.PaymentStatusID = (int)ZNodePaymentStatus.CREDIT_AUTHORIZED;
                        orderService.Update(orderEntity);
                    }

                    break;

                case "order-state-change-notification":
                    OrderStateChangeNotification N3 = (OrderStateChangeNotification)ser.GetContentFromString(RequestXml, typeof(OrderStateChangeNotification));
                    {
                        // The order has changed either financial or fulfillment state in Google's system.
                        string OrderNumber3 = N3.googleordernumber;
                        string NewFinanceState = N3.newfinancialorderstate.ToString();
                        string NewFulfillmentState = N3.newfulfillmentorderstate.ToString();
                        string PrevFinanceState = N3.previousfinancialorderstate.ToString();
                        string PrevFulfillmentState = N3.previousfulfillmentorderstate.ToString();

                        if (FulfillmentOrderState.DELIVERED == N3.newfulfillmentorderstate)
                        {
                            OrderService orderService = new OrderService();
                            ZNode.Libraries.DataAccess.Entities.Order orderEntity = null;

                            orderEntity = orderService.Find("CardTransactionID='" + OrderNumber3 + "'")[0];
                            orderEntity.OrderStateID = (int)ZNodeOrderState.SHIPPED;
                            orderService.Update(orderEntity);
                        }
                    }

                    break;

                case "charge-amount-notification":
                    {
                        ChargeAmountNotification N4 = (ChargeAmountNotification)ser.GetContentFromString(RequestXml, typeof(ChargeAmountNotification));
                        
                        // Google has successfully charged the customer's credit card
                        string OrderNumber4 = N4.googleordernumber;
                        decimal ChargedAmount = N4.latestchargeamount.Value;

                        OrderService orderService = new OrderService();
                        ZNode.Libraries.DataAccess.Entities.Order orderEntity = null;

                        orderEntity = orderService.Find("CardTransactionID='" + OrderNumber4 + "'")[0];

                        if (orderEntity.Total == N4.totalchargeamount.Value)
                        {
                            orderEntity.PaymentStatusID = (int)ZNodePaymentStatus.CREDIT_CAPTURED;
                            orderService.Update(orderEntity);
                        }
                    }

                    break;

                case "refund-amount-notification":
                    RefundAmountNotification N5 = (RefundAmountNotification)ser.GetContentFromString(RequestXml, typeof(RefundAmountNotification));
                    
                    // Google has successfully refunded the customer's credit card.
                    string OrderNumber5 = N5.googleordernumber;
                    decimal RefundedAmount = N5.latestrefundamount.Value;
                    break;

                case "chargeback-amount-notification":
                    ChargebackAmountNotification N6 = (ChargebackAmountNotification)ser.GetContentFromString(RequestXml, typeof(ChargebackAmountNotification));
                    
                    // A customer initiated a chargeback with his credit card company to get her money back.
                    string OrderNumber6 = N6.googleordernumber;
                    decimal ChargebackAmount = N6.latestchargebackamount.Value;
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region Helper Methods

       /// <summary>
        /// This method will create objects for ZnodeProduct ZNodeSKu  ZnodeAddOn with the merchant private data info
       /// </summary>
       /// <param name="newOrder">New Order Notification</param>
        private void UpdateValues(NewOrderNotification newOrder)
        {
            ZNodeSKU SKU = new ZNodeSKU();
            foreach (Item thisItem in newOrder.shoppingcart.items)
            {
                int productId = 0;
                XmlNode[] _MerchantItemPrivateDataNodes = null;
                string SKUattributeList = string.Empty;
                string SkuId = string.Empty;
                string selectedAddOnValues = string.Empty;

                if (thisItem.merchantprivateitemdata != null && thisItem.merchantprivateitemdata != null)
                {
                    // Retreieve All Child Nodes - ProductId,SKUattributeList,AddonList
                    _MerchantItemPrivateDataNodes = thisItem.merchantprivateitemdata.Any;

                    productId = int.Parse(_MerchantItemPrivateDataNodes[0].InnerText);

                    // Retrieve SKUId 
                    SkuId = _MerchantItemPrivateDataNodes[1].InnerText;

                    // Retrieve AddOn Value Id List
                    selectedAddOnValues = _MerchantItemPrivateDataNodes[2].InnerText;
                }

                // Create Product Entity using ProductId
                ZNodeProduct _product = ZNodeProduct.Create(productId, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);

                if (_product != null)
                {
                    ZNodeAddOnList SelectedAddOn = new ZNodeAddOnList();
                    
                    // Get a sku based on Add-ons selected
                    SelectedAddOn = ZNodeAddOnList.CreateByProductAndAddOns(_product.ProductID, selectedAddOnValues);

                    // Set Selected Add-on 
                    _product.SelectedAddOnItems = SelectedAddOn;
                }

                if (SkuId.Length > 0)
                {
                    SKUAdmin _skuAdmin = new SKUAdmin();
                    int skuid;
                    int.TryParse(SkuId, out skuid);
                    DataSet ds = _skuAdmin.GetBySKUId(skuid);

                    // Loop through the items in the dataset
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        SKUattributeList += dr["AttributeId"].ToString() + ",";
                    }

                    // Get a sku based on attributes selected
                    SKU = ZNodeSKU.CreateByProductAndAttributes(_product.ProductID, SKUattributeList);
                }

                _product.SelectedSKU = SKU;

                // Create shopping cart item
                ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCartItem item = new ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCartItem();
                item.Product = new ZNodeProductBase(_product);
                item.Quantity = thisItem.quantity;

                // Add item to cart
                this._shoppingCart.AddToCart(item);
            }

            this._shoppingCart.PostSubmitOrderProcess();
        }

        /// <summary>
        /// Returns an instance of the PaymentSetting  class.
        /// </summary>
        /// <param name="paymentTypeID">Payment Type ID</param>
        /// <returns>Returns the PaymentSettting</returns>
        private PaymentSetting GetByPaymentTypeId(int paymentTypeID)
        {
            PaymentSettingService _pmtServ = new PaymentSettingService();
            int? profileID = 0;

            if (HttpContext.Current.Session[ZNodeSessionKeyType.UserAccount.ToString()] != null)
            {
                this._userAccount = ZNodeUserAccount.CurrentAccount();
            }

            if (this._userAccount != null)
            {
                // Get loggedIn user profile
                profileID = this._userAccount.ProfileID; 
            }

            TList<PaymentSetting> list = _pmtServ.GetByPaymentTypeID(paymentTypeID);
            list.Filter = "ActiveInd = true AND ProfileID = " + profileID;

            if (list.Count == 0)
            {
                // Get All Profiles payment setting
                list.Filter = "ActiveInd = true AND ProfileID = null";
            }

            return list[0];
        }

        #endregion
    }
}