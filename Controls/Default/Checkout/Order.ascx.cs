using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.Shipping;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.Shipping;
using System.Linq;

/// <summary>
/// Checkout order information
/// </summary>
namespace WebApp
{
    /// <summary>
    /// Represents the Order user control class.
    /// </summary>
    public partial class Controls_Default_Checkout_Order : System.Web.UI.UserControl
    {
        #region Private Variables

        private ZNodeShoppingCart _shoppingCart = (ZNodeShoppingCart)ZNodeCheckout.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
        private ZNodeShoppingCart _filteredShoppingCart;
        private int _PortalID = ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.PortalID;
        private List<zUPSRate> upsRates = null;
        private List<zUSPSRate> uspsRates = null;
        private bool _isShippingEditAllow = false;
        private bool _isTotalUpdated = true;
        //FedEx Integration
        private List<PeftFedEXRates> fedExRates = null;

        private string upsShippingTypeID = ConfigurationManager.AppSettings["UPSShippingTypeID"] != null ? ConfigurationManager.AppSettings["UPSShippingTypeID"].ToString() : string.Empty;
        private string uspsShippingTypeID = ConfigurationManager.AppSettings["USPSShippingTypeID"] != null ? ConfigurationManager.AppSettings["USPSShippingTypeID"].ToString() : string.Empty;
        private string upsFreeShippingServiceCode = ConfigurationManager.AppSettings["GroundFreeShippingCode"] != null ? ConfigurationManager.AppSettings["GroundFreeShippingCode"].ToString() : string.Empty;
        private string grounFlatRateShippingID = ConfigurationManager.AppSettings["GroundFlatRateShippingID"] != null ? ConfigurationManager.AppSettings["GroundFlatRateShippingID"].ToString() : string.Empty;
        private string roganShoesPortalID = ConfigurationManager.AppSettings["RoganShoesPortalID"] != null ? ConfigurationManager.AppSettings["RoganShoesPortalID"].ToString() : string.Empty;
        //FedEx Integration
        private bool isFedExEnable = ConfigurationManager.AppSettings["IsFedExEnabled"] != null && ConfigurationManager.AppSettings["IsFedExEnabled"].ToString() == "1" ? true : false;
        private string fedExShippingTypeID = ConfigurationManager.AppSettings["FedExShippingTypeID"] != null ? ConfigurationManager.AppSettings["FedExShippingTypeID"].ToString() : string.Empty;
        private string fedExFreeShippingServiceCode = ConfigurationManager.AppSettings["FedExGroundFreeShippingCode"] != null ? ConfigurationManager.AppSettings["FedExGroundFreeShippingCode"].ToString() : string.Empty;
        #endregion

        public event System.EventHandler ShippingSelectedIndexChanged;

        #region Public Properties
        public int PortalID
        {
            get { return this._PortalID; }
            set { this._PortalID = value; }
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Removes all the objects from the list
        /// </summary>
        public void ClearItems()
        {
            rdolstShipping.Items.Clear();
        }

        /// <summary>
        /// Represents the BindGrid method
        /// </summary>
        public void BindGrid()
        {
            this._filteredShoppingCart = this.FilterCartItemsByPortal(this._shoppingCart);
            Session[string.Format("{0}_{1}", ZNodeSessionKeyType.ShoppingCart.ToString(), this.PortalID)] = this._filteredShoppingCart;
            uxCart.DataSource = this._filteredShoppingCart.ShoppingCartItems;
            uxCart.DataBind();

            #region[Znode old Code:: Code Commneted as need to maintain the view state of order grid]
            //foreach (GridViewRow row in uxCart.Rows)
            //{
            //    foreach (TableCell cell in row.Cells)
            //    {
            //        cell.EnableViewState = false;
            //    }
            //}
            #endregion

            this.SetShippingSettings();
        }

        public void BindShipping()
        {
            ZNodeUserAccount userAccount = (ZNodeUserAccount)ZNodeUserAccount.CreateFromSession(ZNodeSessionKeyType.UserAccount);
            bool isPoBox = userAccount.ShippingAddressExtn != null && userAccount.ShippingAddressExtn.IsPOBox.HasValue ? userAccount.ShippingAddressExtn.IsPOBox.Value : false;
            DataSet ds = new DataSet();
            DataView dv = new DataView();
            divAllFreeShipping.Visible = false;
            if (userAccount != null)
            {
                // Find is default store, so we include all profiles.
                CategoryService categoryService = new CategoryService();
                int total;
                TList<Category> categoryList = categoryService.GetByPortalID(this.PortalID, 0, 1, out total);
                bool IsDefaultPortal = categoryList.Count == 0;

                ShippingService shipServ = new ShippingService();
                TList<Shipping> shippingList = shipServ.GetAll();
                shippingList.Sort("DisplayOrder Asc");

                //Zeon Customization :: Retrive Shipping Methods Based On ZeonCustomShippingRule
                shippingList = this.GetFilteredShippingList(shippingList, userAccount.ShippingAddress.CountryCode, userAccount.ShippingAddress.StateCode, isPoBox);


                int profileID = 0;
                if (userAccount != null)
                {
                    profileID = userAccount.ProfileID;
                }
                else
                {
                    // Get Default Registered profileId
                    profileID = ZNodeProfile.DefaultRegisteredProfileId;
                }

                if (!IsDefaultPortal)
                {
                    // If franchise store then hide the "All profiles" associated shipping option.
                    PortalProfileService ppservice = new PortalProfileService();
                    TList<PortalProfile> portalProfiles = ppservice.GetByPortalID(this.PortalID);
                    portalProfiles.ApplyFilter(delegate(PortalProfile pProfile) { return pProfile.ProfileID != profileID; });

                    if (portalProfiles.Count > 0)
                    {
                        profileID = portalProfiles[0].ProfileID;
                    }

                    shippingList.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.Shipping shipping) { return shipping.ActiveInd == true && shipping.ProfileID == profileID; });
                }
                else
                {
                    // If site admin store then show all profiles.

                    PortalProfileService ppservice = new PortalProfileService();
                    TList<PortalProfile> portalProfiles = ppservice.GetByPortalID(this.PortalID);
                    portalProfiles.ApplyFilter(delegate(PortalProfile pProfile) { return pProfile.ProfileID == profileID; });

                    if (portalProfiles.Count == 0)
                    {
                        profileID = 0;
                    }

                    // If site admin store then show all profiles.
                    shippingList.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.Shipping shipping) { return (shipping.ActiveInd == true) && (shipping.ProfileID == profileID || shipping.ProfileID == null); });

                }

                ds = shippingList.ToDataSet(false);
                dv = new DataView(ds.Tables[0]);

                if (userAccount.BillingAddress.CountryCode == userAccount.ShippingAddress.CountryCode)
                {
                    dv.RowFilter = "DestinationCountryCode = '" + userAccount.BillingAddress.CountryCode + "' or DestinationCountryCode is null";
                }
                else
                {
                    dv.RowFilter = "DestinationCountryCode = '" + userAccount.ShippingAddress.CountryCode + "' or DestinationCountryCode is null";
                }

                if (dv.ToTable().Rows.Count > 0)
                {
                    //Zeon Customization :: Get the list of UPS Rates
                    if (isFedExEnable)
                    {
                        this.InitiateALLFedExRates(shippingList);
                    }
                    else
                    {
                        this.InitiateALLUpsRates(shippingList);
                    }
                    //Zeon Customization :: For USPS ShippingRates
                    this.InitiateALLUSPSRates(shippingList);


                    foreach (DataRow dr in dv.ToTable().Rows)
                    {

                        //Zeon Customization :: Bind Ship Methods
                        this.BindShippingMethods(dr);

                        #region[ZNode Old Code]
                        //string description = dr["Description"].ToString();
                        //System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("&reg;");
                        //description = regex.Replace(description, "�");
                        //ListItem li = new ListItem(description, dr["ShippingID"].ToString());
                        //lstShipping.Items.Add(li);
                        #endregion
                    }
                    if (rdolstShipping.Items.Count > 0)
                    {
                        rdolstShipping.SelectedIndex = 0;
                    }
                }

                if (rdolstShipping.Items.Count == 0)
                {
                    ShipSection.Visible = false;

                    this.BindNoShippingRates();
                }
            }

            if (Request.Form[rdolstShipping.UniqueID] != null)
            {
                int shipId = 0;
                if (int.TryParse(Request.Form[rdolstShipping.UniqueID], out shipId))
                {
                    ListItem li = rdolstShipping.Items.FindByValue(shipId.ToString());
                    if (li != null)
                    {
                        rdolstShipping.SelectedIndex = rdolstShipping.Items.IndexOf(li);
                    }
                }
            }


        }
        #endregion

        #region Protected Methods and Events
        /// <summary>
        ///  Page Pre Render Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            ZNodeUserAccount userAccount = ZNodeUserAccount.CurrentAccount();
            if (userAccount != null)
            {
                #region[Znode Old Code :: Code Commented :: this.ShippingSelectedIndexChanged calling multiple times]
                //if (this.ShippingSelectedIndexChanged != null)
                //{
                //    // Triggers parent page event to hide or show the payment section
                //    this.ShippingSelectedIndexChanged(sender, e);
                //}
                #endregion

                this.BindTotals();
            }
        }

        /// <summary>
        /// Event triggered when shipping option is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstShipping_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.SetShippingSettings();

            if (this.ShippingSelectedIndexChanged != null)
            {
                // Triggers parent page event to hide or show the payment section
                this.ShippingSelectedIndexChanged(sender, e);
            }
        }
        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            ZNodeUserAccount userAccount = ZNodeUserAccount.CurrentAccount();

            if (userAccount != null)
            {
                this._shoppingCart.Payment.BillingAddress = userAccount.BillingAddress;

                //Zeon Custom Code :: Start
                //this.BindGrid();
                //Zeon Custom Code :: Start

                if (rdolstShipping.Items.Count == 0)
                {
                    //Zeon Custom Code :: Start
                    this.BindGrid();
                    //Zeon Custom Code :: Start

                    this.BindShipping();

                    this.SetShippingSettings();


                    #region[Znode Old Code :: Code Commented :: this.ShippingSelectedIndexChanged calling multiple times]
                    //if (this.ShippingSelectedIndexChanged != null)
                    //{
                    //    // Triggers parent page event to hide or show the payment section
                    //    this.ShippingSelectedIndexChanged(sender, e);
                    //}
                    #endregion
                }

                // Check Sale Tax rate
                if (this._shoppingCart.TaxRate > 0)
                {
                    TaxPct.Text = "(" + this._shoppingCart.TaxRate + "%)";
                }
                else
                {
                    TaxPct.Text = string.Empty;
                }
                //Check if user has right to edit shipping charges
                _isShippingEditAllow = Roles.IsUserInRole("ADMIN") || Roles.IsUserInRole("CUSTOMER SERVICE REP") ? true : false;
                Shipping.Visible = !_isShippingEditAllow;
                txtShipping.Visible = rgvShipping.Visible = rgvShipping.Enabled = copValShipping.Visible = copValShipping.Enabled = rangeValiShipping.Enabled = rangeValiShipping.Visible = lnkShippingUpdate.Visible = _isShippingEditAllow;
                if (this._shoppingCart.IsShipngChrgsExtended)
                {
                    this.BindRoleWiseShippingCharges(this._shoppingCart.ShippingCost.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix());
                }
            }
        }

        #endregion

        #region Private Methods

        private ZNodeShoppingCart FilterCartItemsByPortal(ZNodeShoppingCart _shopCart)
        {
            ZNodeShoppingCart cartItems = new ZNodeShoppingCart();
            if (_shopCart != null)
            {
                foreach (ZNodeShoppingCartItem cartItem in _shopCart.ShoppingCartItems)
                {
                    if (cartItem.Product.PortalID == this.PortalID)
                    {
                        ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
                        item.Product = cartItem.Product;
                        item.Quantity = cartItem.Quantity;
                        item.Notes = cartItem.Notes;
                        item.AddProductAsLineItem = ZCommonHelper.IsCustomMulitipleAttributePortal();
                        cartItems.AddToCart(item);
                    }
                }
            }

            if (_shopCart.ShoppingCartItems.Count == cartItems.ShoppingCartItems.Count)
            {
                cartItems = _shopCart;
            }

            return cartItems;
        }

        /// <summary>
        ///  Represents the BindTotals method
        /// </summary>
        private void BindTotals()
        {
            if (this._filteredShoppingCart == null)
                this._filteredShoppingCart = this.FilterCartItemsByPortal(this._shoppingCart);

            string shippingCharges = string.Empty;
            // Bind totals
            SubTotal.Text = this._filteredShoppingCart.SubTotal.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
            Discount.Text = "-" + this._filteredShoppingCart.Discount.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
            lblGiftCardAmount.Text = "-" + this._filteredShoppingCart.GiftCardAmount.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
            Tax.Text = this._filteredShoppingCart.OrderLevelTaxes.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
            shippingCharges = this._filteredShoppingCart.ShippingCost.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
            Total.Text = this._filteredShoppingCart.Total.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
            rowDiscount.Visible = this._filteredShoppingCart.Discount > 0 ? true : false;
            divGiftCardAmount.Visible = this._filteredShoppingCart.GiftCardAmount > 0 ? true : false;
            rowTax.Visible = this._filteredShoppingCart.OrderLevelTaxes > 0 ? true : false;
            ltshippingName.Text = "(" + this._filteredShoppingCart.Shipping.ShippingName + ")";
            if (this._filteredShoppingCart.ShippingCost == 0 && (this._filteredShoppingCart.Shipping.ShippingName.Equals("Ground Shipping", StringComparison.OrdinalIgnoreCase)))
            {
                shippingCharges = this.GetLocalResourceObject("GroundFreeShippingMessage").ToString();
                rowShipping.Visible = true;
            }
            else if (this._filteredShoppingCart.ShippingCost == 0 && (this._filteredShoppingCart.Shipping.ShippingName.Equals("Ground", StringComparison.OrdinalIgnoreCase)))
            {
                shippingCharges = this.GetLocalResourceObject("GroundFreeShippingHTML").ToString();
                rowShipping.Visible = true;
            }
            else
            {
                rowShipping.Visible = this._filteredShoppingCart.ShippingCost > 0 ? true : false;
            }
            BindRoleWiseShippingCharges(shippingCharges);
        }

        /// <summary>
        /// Re-calculate shipping cost
        /// </summary>
        private void SetShippingSettings()
        {
            object objShoppingCart = Session[string.Format("{0}_{1}", ZNodeSessionKeyType.ShoppingCart.ToString(), this.PortalID)];
            if (objShoppingCart != null)
            {
                this._filteredShoppingCart = (ZNodeShoppingCart)objShoppingCart;
            }

            if (rdolstShipping.Items.Count > 0)
            {
                int shippingID = int.Parse(rdolstShipping.SelectedValue);

                if (this._filteredShoppingCart != null)
                {
                    // Set shipping name in shopping cart object
                    this._filteredShoppingCart.Shipping.ShippingName = rdolstShipping.SelectedItem.Text;
                    this._filteredShoppingCart.Shipping.ShippingID = shippingID;

                    // ZNode Old Code ::Does not need to call  Calculate() as we have already loaded the price in drop down text.
                    //this._filteredShoppingCart.Calculate();

                    //Zeon Customization Start 
                    this.SetShippingCost(this._filteredShoppingCart.Shipping.ShippingName, this._filteredShoppingCart.Shipping.ShippingID);
                    //Zeon Customization End

                    Session[string.Format("{0}_{1}", ZNodeSessionKeyType.ShoppingCart.ToString(), this.PortalID)] = this._filteredShoppingCart;
                }
                else
                {
                    // Set shipping name in shopping cart object
                    this._shoppingCart.Shipping.ShippingName = rdolstShipping.SelectedItem.Text;
                    this._shoppingCart.Shipping.ShippingID = shippingID;
                    this._shoppingCart.Calculate();
                }
                //Reset Extended Shipping Flag
                _isTotalUpdated = false;
                this._shoppingCart.IsShipngChrgsExtended = false;
                this._shoppingCart.ExtendedShippingCost = 0;
            }
        }

        #endregion

        #region[Zeon Custom Code]

        /// <summary>
        /// Retive & Set ALL UPS Rates
        /// </summary>
        /// <param name="shippingList"></param>
        private void InitiateALLUpsRates(TList<Shipping> shippingList)
        {
            int upsCount = shippingList.OfType<ZNode.Libraries.DataAccess.Entities.Shipping>().AsQueryable().Where(i => i.ShippingTypeID.Equals(int.Parse(upsShippingTypeID))).Count();
            if (upsCount > 0)
            {
                upsRates = this.GetUpsShippingRate(shippingList);

                //Rule :: SHIPPING METHODS � Cheer and Pom ::48 states <=$100 :: Apply Ground Shipping - $9 � Flat Rate
                //So we have removed the Grond Shipping from the UPS Rates list 
                bool isExists = shippingList != null && shippingList.Exists(o => o.ShippingID == int.Parse(grounFlatRateShippingID));
                if (isExists)
                {
                    upsRates.RemoveAll(o => o.Code.Equals(upsFreeShippingServiceCode, StringComparison.OrdinalIgnoreCase));
                }
            }
        }

        /// <summary>
        /// Retrive & Set ALL USPS Rates 
        /// </summary>
        /// <param name="shippingList"></param>
        private void InitiateALLUSPSRates(TList<Shipping> shippingList)
        {

            int upsCount = shippingList.OfType<ZNode.Libraries.DataAccess.Entities.Shipping>().AsQueryable().Where(i => i.ShippingTypeID.Equals(int.Parse(uspsShippingTypeID))).Count();
            if (upsCount > 0)
            {
                ZNodeUSPS uspsObject = new ZNodeUSPS();
                uspsRates = uspsObject.GetUSPSShippingRate(_shoppingCart, GetShippingAddress(), GetShippingAddress().CountryCode, shippingList);

            }
        }

        /// <summary>
        /// Get USPS Rate List
        /// </summary>
        /// <param name="shippingList"></param>
        /// <returns></returns>
        private List<zUPSRate> GetUpsShippingRate(TList<Shipping> shippingList)
        {
            Address shippingAddress = GetShippingAddress();
            List<zUPSRate> upsrateList = new List<zUPSRate>();
            ZNodeUPS upsObject = new ZNodeUPS();
            upsrateList = upsObject.GetUPSShippingRate(_shoppingCart, shippingAddress, shippingList);
            return upsrateList;
        }

        /// <summary>
        /// Gets Shipping Address
        /// </summary>
        /// <returns></returns>
        private Address GetShippingAddress()
        {
            ZNodeUserAccount _userAccount = ZNodeUserAccount.CurrentAccount();
            Address shippingAddress = new Address();

            shippingAddress.AccountID = _userAccount.AccountID;
            shippingAddress.City = _userAccount.ShippingAddress.City;
            shippingAddress.CompanyName = _userAccount.ShippingAddress.CompanyName;
            shippingAddress.CountryCode = _userAccount.ShippingAddress.CountryCode;
            shippingAddress.FirstName = _userAccount.ShippingAddress.FirstName;
            shippingAddress.LastName = _userAccount.ShippingAddress.LastName;
            shippingAddress.MiddleName = string.Empty;
            shippingAddress.StateCode = _userAccount.ShippingAddress.StateCode;
            shippingAddress.Street = _userAccount.ShippingAddress.Street;
            shippingAddress.Street1 = _userAccount.ShippingAddress.Street1;
            shippingAddress.PostalCode = _userAccount.ShippingAddress.PostalCode;
            shippingAddress.IsDefaultBilling = false;
            shippingAddress.IsDefaultShipping = true;
            shippingAddress.Name = "Default Shipping";

            return shippingAddress;
        }

        /// <summary>
        /// Set Shipping Cost
        /// </summary>
        private void SetShippingCost(string selectedShippingText, int selectedShippingID)
        {
            string selectedShippingName = string.Empty;
            string shippingValue = string.Empty;
            this._shoppingCart.ClearExistingShippingDetails();
            int startingpostion = selectedShippingText.IndexOf('$');
            if (startingpostion != -1)
            {

                selectedShippingName = selectedShippingText.Substring(0, (startingpostion - 1));
                shippingValue = selectedShippingText.Substring(startingpostion + 1);

                if (shippingValue.Equals("Free", StringComparison.OrdinalIgnoreCase))
                {
                    shippingValue = "0";
                }

                this._shoppingCart.Shipping.ShippingName = selectedShippingName;
                this._shoppingCart.Shipping.ShippingID = selectedShippingID;


                decimal orderLevelShipping = 0;
                decimal.TryParse(shippingValue, out orderLevelShipping);
                this._shoppingCart.OrderLevelShipping = this._filteredShoppingCart.OrderLevelShipping = orderLevelShipping;
            }
            else if (selectedShippingText.ToLower().Contains("free"))
            {
                string[] shippingNameArray = selectedShippingText.Split('-');

                this._shoppingCart.Shipping.ShippingName = shippingNameArray != null && shippingNameArray.Length > 0 ? shippingNameArray[0].ToString() : "Ground Shipping";
                this._shoppingCart.Shipping.ShippingID = selectedShippingID;

                decimal orderLevelShipping = 0;
                this._shoppingCart.OrderLevelShipping = this._filteredShoppingCart.OrderLevelShipping = orderLevelShipping;
            }
            else
            {
                this._filteredShoppingCart.Shipping.ShippingName = selectedShippingText;
                this._filteredShoppingCart.Shipping.ShippingID = selectedShippingID;

                this._filteredShoppingCart.Calculate();
            }
        }

        /// <summary>
        /// Get filtered Shipping List based on the ZeonCustomShippingRules
        /// </summary>
        /// <param name="shippingList"></param>
        /// <param name="countryCode"></param>
        /// <param name="stateCode"></param>
        /// <returns></returns>
        private TList<Shipping> GetFilteredShippingList(TList<Shipping> shippingList, string countryCode, string stateCode, bool isPoBox)
        {
            try
            {
                TList<ZeonCustomShippingRules> customShippingRuleList = this.GetZeonCustomShippingRuleList(countryCode, stateCode, isPoBox);
                if (customShippingRuleList != null && customShippingRuleList.Count > 0)
                {
                    if (customShippingRuleList.Count > 1)
                    {
                        customShippingRuleList = SortShippingRuleList(customShippingRuleList);
                    }
                    shippingList = this.GetShippingList(shippingList, customShippingRuleList);
                }
                else
                {
                    customShippingRuleList = this.GetZeonCustomShippingRuleList(countryCode, "ALL", isPoBox);
                    if (customShippingRuleList != null && customShippingRuleList.Count > 0)
                    {
                        if (customShippingRuleList.Count > 1)
                        {
                            customShippingRuleList = SortShippingRuleList(customShippingRuleList);
                        }
                        shippingList = this.GetShippingList(shippingList, customShippingRuleList);
                    }
                    else if (!countryCode.Equals("US", StringComparison.OrdinalIgnoreCase))
                    {
                        customShippingRuleList = this.GetZeonCustomShippingRuleList("INTL", "ALL", isPoBox);
                        if (customShippingRuleList != null && customShippingRuleList.Count > 0)
                        {
                            shippingList = this.GetShippingList(shippingList, customShippingRuleList);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in Controls_Default_Checkout_Order>>GetFilteredShippingList!!" + ex.ToString());
            }

            return shippingList;
        }

        /// <summary>
        /// Retrive the Qualified Shipping Type based on countryCode,statecode,isPoBox
        /// </summary>
        /// <param name="countryCode"></param>
        /// <param name="stateCode"></param>
        /// <param name="isPoBox"></param>
        /// <returns></returns>
        private TList<ZeonCustomShippingRules> GetZeonCustomShippingRuleList(string countryCode, string stateCode, bool isPoBox)
        {

            TList<ZeonCustomShippingRules> customShippingRuleList = new TList<ZeonCustomShippingRules>();
            try
            {
                ZeonCustomShippingRulesQuery filter = new ZeonCustomShippingRulesQuery();
                if (isPoBox && stateCode.Equals("ALL") && countryCode.Equals("US", StringComparison.OrdinalIgnoreCase))
                {
                    //IF POX IS TRUE :: RETRIVE RULE BASED ON THE PBOX IS TRUE INSTEAD OF COUNTRY & STATE PARAMETER 
                    filter.Append(ZeonCustomShippingRulesColumn.IsPOBox, isPoBox.ToString());
                    filter.Append(ZeonCustomShippingRulesColumn.CountryCode, "ALL");
                }
                else
                {
                    filter.Append(ZeonCustomShippingRulesColumn.CountryCode, countryCode);
                    filter.Append(ZeonCustomShippingRulesColumn.StateCode, stateCode);
                }

                filter.Append(ZeonCustomShippingRulesColumn.ActiveInd, "true");
                filter.Append(ZeonCustomShippingRulesColumn.PortalID, ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.PortalID.ToString());

                ZeonCustomShippingRulesService service = new ZeonCustomShippingRulesService();
                customShippingRuleList = service.Find(filter.GetParameters());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return customShippingRuleList;
        }

        /// <summary>
        /// Get the filtered Shipping Types
        /// </summary>
        /// <param name="shippingList"></param>
        /// <param name="customShippingRuleList"></param>
        /// <returns></returns>
        private TList<Shipping> GetShippingList(TList<Shipping> shippingList, TList<ZeonCustomShippingRules> customShippingRuleList)
        {
            TList<Shipping> filteredShippingList = new TList<Shipping>();
            try
            {
                if (customShippingRuleList != null && customShippingRuleList.Count > 0)
                {
                    foreach (ZeonCustomShippingRules zeonCustomShippingRule in customShippingRuleList)
                    {
                        int shippingID = zeonCustomShippingRule.ShipingID.GetValueOrDefault(0);

                        foreach (Shipping shipping in shippingList)
                        {
                            if (shippingID > 0)
                            {
                                if (zeonCustomShippingRule.ShippingTypeID == shipping.ShippingTypeID && shipping.ShippingID == shippingID)
                                {
                                    filteredShippingList.Add(shipping);
                                }
                            }
                            else if (zeonCustomShippingRule.ShippingTypeID == shipping.ShippingTypeID)
                            {
                                filteredShippingList.Add(shipping);
                            }
                        }
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }

            return filteredShippingList;
        }

        /// <summary>
        /// Check is UPS Groud Shipping Free based on ShippingThreshholdOrderAmount
        /// </summary>
        /// <param name="currentTotal"></param>
        /// <returns></returns>
        private bool CheckGoundShippingFree(decimal currentTotal, out decimal shippingThreshHoldOrderAmount)
        {
            bool isGroundShippingFree = false;
            shippingThreshHoldOrderAmount = 0M;
            try
            {
                PortalExtnQuery filter = new PortalExtnQuery();
                filter.Append(PortalExtnColumn.PortalID, ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.PortalID.ToString());
                PortalExtnService portalExnService = new PortalExtnService();
                TList<PortalExtn> portalExtnList = portalExnService.Find(filter.GetParameters());
                if (portalExtnList != null && portalExtnList.Count > 0)
                {
                    var shippingThreshholdOrderAmount = shippingThreshHoldOrderAmount = portalExtnList[0].ShippingThreshholdOrderAmount.GetValueOrDefault(0);
                    if (currentTotal > shippingThreshholdOrderAmount)
                    {
                        isGroundShippingFree = true;
                    }


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isGroundShippingFree;
        }

        /// <summary>
        /// Retrive Flat Rate Shipping Cost 
        /// </summary>
        /// <param name="selectedShippingText"></param>
        /// <param name="selectedShippingID"></param>
        /// <returns></returns>
        private decimal GetFlatRateShippingCost(string selectedShippingText, int selectedShippingID)
        {
            decimal flatRateShippingCost = 0M;
            try
            {
                this._filteredShoppingCart.Shipping.ShippingName = selectedShippingText;
                this._filteredShoppingCart.Shipping.ShippingID = selectedShippingID;

                flatRateShippingCost = this._filteredShoppingCart.GetFlatRateShippingCost();

            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!GetFlatRateShippingCost!!Error!!" + ex.ToString());
            }

            return flatRateShippingCost;

        }

        /// <summary>
        /// Get USPS Shipping Name :: Remove HTML Character Code 
        /// </summary>
        /// <param name="shippingName"></param>
        /// <returns></returns>
        private string GetFormattedUSPSName(string shippingName)
        {
            string formattedShippingName = string.Empty;
            string shippingValue = string.Empty;
            int startingpostion = shippingName.IndexOf("<sup>");
            if (startingpostion != -1)
            {
                formattedShippingName = shippingName.Substring(0, (startingpostion));
            }

            return formattedShippingName;
        }

        /// <summary>
        /// Bind Shipping Method in Drop Down Control
        /// </summary>
        /// <param name="dr"></param>
        private void BindShippingMethods(DataRow dr)
        {
            string description = dr["Description"].ToString();

            //Zeon Customization :: Show the UPS Price of in Drop Down Text 
            if (dr["ShippingTypeID"] != null && dr["ShippingTypeID"].ToString().Equals(upsShippingTypeID, StringComparison.OrdinalIgnoreCase))
            {
                System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("&reg;");
                description = regex.Replace(description, "�").Replace("UPS - ", string.Empty);

                zUPSRate upsRate = new zUPSRate();
                upsRate = upsRates != null ? upsRates.SingleOrDefault(o => o.Code.Equals(dr["ShippingCode"].ToString(), StringComparison.OrdinalIgnoreCase)) : null;
                if (upsRate != null && upsRate.MonetaryValue > 0)
                {
                    if (upsRate.Code.Equals(upsFreeShippingServiceCode, StringComparison.OrdinalIgnoreCase))
                    {
                        decimal shippingThreshHoldOrderAmount = 0M;
                        bool checkIsGroundShippingFree = CheckGoundShippingFree(this._filteredShoppingCart.SubTotal, out shippingThreshHoldOrderAmount);
                        string monetaryValue = checkIsGroundShippingFree ? this.GetLocalResourceObject("GroundFreeShippingHTML").ToString() : upsRate.MonetaryValue.ToString("c");
                        description = description + "-" + monetaryValue + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                    }
                    else
                    {
                        description = description + "-" + upsRate.MonetaryValue.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                    }

                    ListItem li = new ListItem(description, dr["ShippingID"].ToString());
                    rdolstShipping.Items.Add(li);
                }
            }
            else if (uspsRates != null && uspsRates.Count > 0 && dr["ShippingTypeID"] != null && dr["ShippingTypeID"].ToString().Equals(uspsShippingTypeID, StringComparison.OrdinalIgnoreCase))
            {
                //Zeon Customization :: Show the USPS Price of in Drop Down Text 
                zUSPSRate uspsRate = new zUSPSRate();
                uspsRate = uspsRates[0];
                if (uspsRate != null && uspsRate.MonetaryValue > 0)
                {
                    description = GetFormattedUSPSName(uspsRate.ServiceName);
                    description = !string.IsNullOrEmpty(description) ? description : uspsRate.ServiceName;
                    System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("&reg;");
                    description = regex.Replace(description, "�").Replace("USPS", string.Empty).TrimStart('-');

                    description = description + "-" + uspsRate.MonetaryValue.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                    ListItem li = new ListItem(description, dr["ShippingID"].ToString());
                    rdolstShipping.Items.Add(li);
                }
            }
            else if (dr["ShippingID"] != null && int.Parse(dr["ShippingID"].ToString()).Equals(int.Parse(grounFlatRateShippingID)))
            {
                decimal shippingCost = 0;
                decimal shippingThreshHoldOrderAmount = 0M;

                bool checkIsGroundShippingFree = CheckGoundShippingFree(this._filteredShoppingCart.SubTotal, out shippingThreshHoldOrderAmount);
                //Rule :: Hide Ground Free When PO and Military checked and less than shippingThreshholdOrderAmount For Rogan Shoes Portal
                if (!HideGroundFreeShipping(shippingThreshHoldOrderAmount, this._filteredShoppingCart.SubTotal))
                {
                    shippingCost = !checkIsGroundShippingFree ? GetFlatRateShippingCost(description, int.Parse(dr["ShippingID"].ToString())) : 0M;
                    string montryValue = shippingCost > 0 ? shippingCost.ToString("c") : this.GetLocalResourceObject("GroundFreeShippingHTML").ToString();
                    description = description + "-" + montryValue + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                    ListItem li = new ListItem(description, dr["ShippingID"].ToString());
                    rdolstShipping.Items.Add(li);
                }

            }
            else if (dr["ShippingTypeID"] != null && dr["ShippingTypeID"].ToString().Equals(fedExShippingTypeID, StringComparison.OrdinalIgnoreCase))
            {
                System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("&reg;");
                description = regex.Replace(description, "�").Replace("FedEx-", string.Empty).Replace("�", string.Empty);

                PeftFedEXRates fedExRate = new PeftFedEXRates();
                fedExRate = fedExRates != null ? fedExRates.SingleOrDefault(o => o.Code.Equals(dr["ShippingCode"].ToString(), StringComparison.CurrentCultureIgnoreCase)) : null;
                if (fedExRate != null && fedExRate.MonetaryValue > 0)
                {
                    if (fedExRate.Code.Equals(fedExFreeShippingServiceCode, StringComparison.OrdinalIgnoreCase))
                    {
                        decimal shippingThreshHoldOrderAmount = 0M;
                        bool checkIsGroundShippingFree = CheckGoundShippingFree(this._filteredShoppingCart.SubTotal, out shippingThreshHoldOrderAmount);
                        string monetaryValue = checkIsGroundShippingFree ? this.GetLocalResourceObject("GroundFreeShippingHTML").ToString() : fedExRate.MonetaryValue.ToString("c");
                        description = description + "-" + monetaryValue + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                    }
                    else
                    {
                        description = description + "-" + fedExRate.MonetaryValue.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                    }

                    ListItem li = new ListItem(description, dr["ShippingID"].ToString());
                    rdolstShipping.Items.Add(li);
                }
            }
            //else
            //{
            //    System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("&reg;");
            //    description = regex.Replace(description, "�");
            //    ListItem li = new ListItem(description, dr["ShippingID"].ToString());
            //    rdolstShipping.Items.Add(li);
            //}


        }

        /// <summary>
        /// Rule :: Hide Ground Free When PO and Military checked and less than shippingThreshholdOrderAmount For Rogan Shoes Portal
        /// For Chear and Pom 
        /// </summary>
        /// <param name="shippingThreshholdOrderAmount"></param>
        /// <param name="currentTotal"></param>
        /// <returns></returns>
        private bool HideGroundFreeShipping(decimal shippingThreshholdOrderAmount, decimal currentTotal)
        {
            bool hideGroundFreeShipping = false;
            int raganPortalID = 0;
            int.TryParse(roganShoesPortalID, out raganPortalID);
            try
            {
                if (_PortalID == raganPortalID && currentTotal < shippingThreshholdOrderAmount)
                {
                    hideGroundFreeShipping = true;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return hideGroundFreeShipping;
        }

        /// <summary>
        /// Bind Custom Flat Rate Shipping when no shipping calculated
        /// </summary>
        private void BindNoShippingRates()
        {
            TList<ZeonCustomShippingRules> customShippingRuleList = new TList<ZeonCustomShippingRules>();
            ZeonCustomShippingRulesQuery filter = new ZeonCustomShippingRulesQuery();
            filter.Append(ZeonCustomShippingRulesColumn.CountryCode, "None");
            filter.Append(ZeonCustomShippingRulesColumn.StateCode, "None");
            filter.Append(ZeonCustomShippingRulesColumn.ActiveInd, "true");
            filter.Append(ZeonCustomShippingRulesColumn.PortalID, ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.PortalID.ToString());

            ZeonCustomShippingRulesService service = new ZeonCustomShippingRulesService();
            customShippingRuleList = service.Find(filter.GetParameters());
            if (customShippingRuleList != null && customShippingRuleList.Count > 0)
            {
                foreach (ZeonCustomShippingRules zeonCustomShippingRule in customShippingRuleList)
                {
                    int shippingID = zeonCustomShippingRule.ShipingID.GetValueOrDefault(0);
                    if (shippingID > 0)
                    {
                        ShippingService shippingService = new ShippingService();
                        Shipping shippingList = shippingService.GetByShippingID(shippingID);

                        decimal shippingCost = GetFlatRateShippingCost(shippingList.Description, shippingID);
                        if (shippingCost > 0)
                        {
                            string montryValue = shippingCost.ToString("c");
                            string description = shippingList.Description + "-" + montryValue + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                            ListItem li = new ListItem(description, shippingID.ToString());
                            rdolstShipping.Items.Add(li);
                            rdolstShipping.Items[0].Selected = true;
                            ShipSection.Visible = true;
                        }
                    }
                }
            }

        }

        /// <summary>
        /// Check All Free Shipping Product Exists
        /// </summary>
        /// <returns>bool</returns>
        private bool CheckAllFreeShippingProductExists()
        {
            bool isAllLineItemFreesShippingInd = false;
            try
            {
                int shoppingCartItemCount = this._shoppingCart != null && this._shoppingCart.ShoppingCartItems != null ? this._filteredShoppingCart.ShoppingCartItems.Count : 0;
                if (this._shoppingCart != null && this._shoppingCart.ShoppingCartItems != null)
                {
                    var freeShippingProductIndProductCount = this._shoppingCart.ShoppingCartItems.OfType<ZNodeShoppingCartItem>().AsQueryable().Where(i => i.Product.FreeShippingInd == true).Count();
                    if (freeShippingProductIndProductCount.Equals(shoppingCartItemCount))
                    {
                        isAllLineItemFreesShippingInd = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("Error in CheckAllFreeShippingProductExists::" + ex.ToString());
                isAllLineItemFreesShippingInd = false;
            }
            return isAllLineItemFreesShippingInd;
        }

        /// <summary>
        /// Set Shipping Charges to Control
        /// </summary>
        /// <param name="shippingCharges">string</param>
        private void BindRoleWiseShippingCharges(string shippingCharges)
        {
            if (this._filteredShoppingCart == null)
                this._filteredShoppingCart = this.FilterCartItemsByPortal(this._shoppingCart);

            if (!string.IsNullOrWhiteSpace(shippingCharges))
            {
                string updatedShipping = !string.IsNullOrWhiteSpace(txtShipping.Text) ? txtShipping.Text : string.Empty;
                bool isShippingUpdatd = !updatedShipping.Equals(this._shoppingCart.ShippingCost);

                if (_isShippingEditAllow)
                {
                    if (isShippingUpdatd)
                    {
                        decimal shippingCharge = this._filteredShoppingCart.ShippingCost;
                        this._shoppingCart.IsShipngChrgsExtended = this._filteredShoppingCart.IsShipngChrgsExtended = true;
                        this._filteredShoppingCart.ExtendedShippingCost = this._shoppingCart.ExtendedShippingCost = _isTotalUpdated && !string.IsNullOrWhiteSpace(updatedShipping) ? decimal.Parse(updatedShipping) : this._filteredShoppingCart.ShippingCost;
                        decimal updatedTotal = (this._filteredShoppingCart.Total - this._filteredShoppingCart.ShippingCost) + this._shoppingCart.ExtendedShippingCost;
                        Total.Text = updatedTotal.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                        txtShipping.Text = this._filteredShoppingCart.ExtendedShippingCost.ToString();
                    }
                    else
                    {
                        txtShipping.Text = shippingCharges.Equals(this.GetLocalResourceObject("GroundFreeShippingMessage").ToString()) || shippingCharges.Equals(this.GetLocalResourceObject("GroundFreeShippingHTML").ToString()) ? "0.00" : this._filteredShoppingCart.ShippingCost.ToString();
                    }
                }
                else
                {
                    Shipping.Text = shippingCharges;
                }
            }
        }

        /// <summary>
        /// Handles Button Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkShippingUpdate_Click(object sender, EventArgs e)
        {
            _isTotalUpdated = true;
            string updatedShipping = !string.IsNullOrWhiteSpace(txtShipping.Text) ? txtShipping.Text : this._shoppingCart.ShippingCost.ToString();
            this._shoppingCart.IsShipngChrgsExtended = true;
            this._shoppingCart.ExtendedShippingCost = decimal.Parse(updatedShipping);
        }

        /// <summary>
        /// Put Ground Shipping rule Up in filter list
        /// </summary>
        /// <param name="shippingRuleList">TList<ZeonCustomShippingRules></param>
        /// <returns>TList<ZeonCustomShippingRules></returns>
        private TList<ZeonCustomShippingRules> SortShippingRuleList(TList<ZeonCustomShippingRules> shippingRuleList)
        {
            TList<ZeonCustomShippingRules> filteredShippingRuleList = new TList<ZeonCustomShippingRules>();
            foreach (ZeonCustomShippingRules zeonCustomShippingRule in shippingRuleList)
            {
                int shippingID = zeonCustomShippingRule.ShipingID.GetValueOrDefault(0);
                if (shippingID == int.Parse(grounFlatRateShippingID))
                {
                    filteredShippingRuleList.Clear();
                    filteredShippingRuleList.Add(zeonCustomShippingRule);
                    break;
                }
            }
            filteredShippingRuleList.AddRange(shippingRuleList.FindAll(x => x.ShipingID != int.Parse(grounFlatRateShippingID)));

            return filteredShippingRuleList;
        }

        #region FedEx Shipping Integration
        /// <summary>
        /// Retrive & Set ALL FedEx Rates 
        /// </summary>
        /// <param name="shippingList">TList<Shipping></param>
        private void InitiateALLFedExRates(TList<Shipping> shippingList)
        {
            int fedExCount = shippingList.OfType<ZNode.Libraries.DataAccess.Entities.Shipping>().AsQueryable().Where(i => i.ShippingTypeID.Equals(int.Parse(fedExShippingTypeID))).Count();
            if (fedExCount > 0)
            {
                fedExRates = this.GetFedExShippingRate(shippingList);

                //Rule :: SHIPPING METHODS � Cheer and Pom ::48 states <=$100 :: Apply Ground Shipping - $9 � Flat Rate
                //So we have removed the Grond Shipping from the UPS Rates list 
                bool isExists = shippingList != null && shippingList.Exists(o => o.ShippingID == int.Parse(grounFlatRateShippingID));
                if (isExists)
                {
                    fedExRates.RemoveAll(o => o.Code.Equals(fedExFreeShippingServiceCode, StringComparison.OrdinalIgnoreCase));
                }
            }
        }

        /// <summary>
        /// Get USPS Rate List
        /// </summary>
        /// <param name="shippingList"></param>
        /// <returns></returns>
        private List<PeftFedEXRates> GetFedExShippingRate(TList<Shipping> shippingList)
        {
            Address shippingAddress = GetShippingAddress();
            List<PeftFedEXRates> fedExRateList = new List<PeftFedEXRates>();
            try
            {
                ZNodeFedEx fedExObject = new ZNodeFedEx();
                fedExRateList = fedExObject.GetFedExShippingRateList(_shoppingCart, shippingAddress, shippingList);
                if (fedExRateList == null || fedExRateList.Count == 0)
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in Fed Ex Shipping Rates Call -No Shipping Returned!!");
                }
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in Fed Ex Shipping Rates Call!!" + ex.ToString());
            }
            
            return fedExRateList;
        }

        #endregion
        #endregion
    }
}