using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net.Mail;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.Payment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.Suppliers;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the WorldPayNotification user control in marketplace.
    /// </summary>
    public partial class Controls_Default_Checkout_WorldPayNotification : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private string _paymentGateway = string.Empty;
        private ZNodeUserAccount _userAccount = null;
        private ZNodeShoppingCart _shoppingCart = null;
        private PaymentSetting _paymentSetting;
        private ZNodeCheckout _checkout;

        #endregion

        #region Protected Properties

        /// <summary>
        /// Gets the Worldpay OrderId
        /// </summary>
        protected string Id
        {
            get
            {
                return this.Request.QueryString.Get("Id");
            }
        }

        #endregion

        #region World Pay Submit Method

        protected void WorldpaySubmit()
        {
            this._checkout = new ZNodeCheckout();
            this._paymentSetting = this.GetByPaymentTypeId(0);
            this._userAccount = ZNodeUserAccount.CurrentAccount();
            this._checkout.IsWorldpaySecure = true;
            this._shoppingCart.Payment.WorldPayPostData = Request.Params["PaRes"].ToString();

            // Set payment data in checkout object
            this._checkout.ShoppingCart.Payment.CreditCard.CardNumber = this._shoppingCart.Payment.CreditCard.CardNumber;
            this._checkout.ShoppingCart.Payment.CreditCard.CreditCardExp = this._shoppingCart.Payment.CreditCard.CreditCardExp;
            this._checkout.ShoppingCart.Payment.CreditCard.CardSecurityCode = this._shoppingCart.Payment.CreditCard.CardSecurityCode;
            this._checkout.ShoppingCart.Payment = this._shoppingCart.Payment;
            this._checkout.ShoppingCart.Payment.BillingAddress = this._userAccount.BillingAddress;
            this._checkout.ShoppingCart = this._shoppingCart;
            this._checkout.UserAccount = this._userAccount;
            this._checkout.PaymentSettingID = this._paymentSetting.PaymentSettingID;
            this._checkout.ShoppingCart.Payment.PaymentSetting.PaymentTypeID = this._paymentSetting.PaymentTypeID;
            this._checkout.ShoppingCart.Payment.Is3DSecure = true;

            ZNodeOrderFulfillment order = this._checkout.SubmitOrder();

            if (this._checkout.IsSuccess)
            {
                ZNodeTracking tracker = new ZNodeTracking();
                if (tracker.AffiliateId.Length > 0)
                {
                    tracker.AccountID = order.AccountID;
                    tracker.OrderID = order.OrderID;
                    tracker.LogTrackingEvent("Placed an Order");
                }

                this.PostSubmitOrder(order);

                // Create session for Order and Shopping cart
                List<ZNodeOrderFulfillment> orders = new List<ZNodeOrderFulfillment>();
                orders.Add(order);
                ZNodeShoppingCart shopping = new ZNodeShoppingCart();
                Session.Add("OrderDetail", orders);

                ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

                // Cookie based persistent cart.
                ZNodeSavedCart savedCart = new ZNodeSavedCart();
                savedCart.RemoveSavedCart();

                // If shopping cart is null, create in session
                if (shoppingCart == null)
                {
                    shoppingCart = new ZNodeShoppingCart();
                    shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
                }

                Response.Redirect("~/orderreceipt.aspx");
            }
            else
            {
                pnlConfirmation.Visible = true;
                lblMsg.Text = Resources.CommonCaption.OrderSubmitFailed;

               
                //Restore Session values
                ZNodeUserAccount useracct = (ZNodeUserAccount)Session[ZNodeSessionKeyType.UserAccount.ToString()];
                Account account = (Account)Session["AccountObject"];
                Profile profile = (Profile)Session["ProfileCache"];

                // Empty cart 
                Session.Abandon();

                Session.Add(ZNodeSessionKeyType.UserAccount.ToString(), useracct);
                HttpContext.Current.Session["ProfileCache"] = profile;
                Session.Add("AccountObject", account);
            }
        }
        #endregion

        #region Page Load Event

        /// <summary>
        /// Page Load Event    
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.Session[ZNodeSessionKeyType.ShoppingCart.ToString()] != null)
            {
                this._shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();
            }

            if (this._shoppingCart == null)
            {
                Response.Redirect("~/");
            }

            if (!Page.IsPostBack)
            {
                this.WorldpaySubmit();
            }
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Returns an instance of the PaymentSetting  class.
        /// </summary>
        /// <param name="paymentTypeID">Payment Type ID</param>
        /// <returns>Returns the Payment Setting</returns>
        private PaymentSetting GetByPaymentTypeId(int paymentTypeID)
        {
            PaymentSettingService _pmtServ = new PaymentSettingService();
            int? profileID = 0;

            if (HttpContext.Current.Session[ZNodeSessionKeyType.UserAccount.ToString()] != null)
            {
                this._userAccount = ZNodeUserAccount.CurrentAccount();
            }

            if (this._userAccount != null)
            {
                // Get loggedIn user profile
                profileID = this._userAccount.ProfileID; 
            }

            TList<PaymentSetting> list = _pmtServ.GetByPaymentTypeID(paymentTypeID);
            list.Filter = "ActiveInd = true AND ProfileID = " + profileID;

            if (list.Count == 0)
            {
                // Get All Profiles payment setting
                list.Filter = "ActiveInd = true AND ProfileID = null";
            }

            return list[0];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="order"></param>
        #region Post Submit Order - Set Digital Asset

        /// <summary>
        /// Post Order Submission Method
        /// </summary>
        /// <param name="order">Order Information</param>
        private void PostSubmitOrder(ZNodeOrderFulfillment order)
        {
            this._checkout.ShoppingCart.PostSubmitOrderProcess();

            ZNodeSupplierOption supplierOption = new ZNodeSupplierOption();
            supplierOption.SubmitOrder(order, this._checkout.ShoppingCart);
            
            int Counter = 0;
            DigitalAssetService digitalAssetService = new DigitalAssetService();

            // Loop through the Order Line Items
            foreach (OrderLineItem orderLineItem in order.OrderLineItems)
            {
                ZNodeShoppingCartItem shoppingCartItem = this._checkout.ShoppingCart.ShoppingCartItems[Counter++];
                
                // Set quantity ordered
                int qty = shoppingCartItem.Quantity;
                
                // Set product id
                int productId = shoppingCartItem.Product.ProductID;
                ZNodeGenericCollection<ZNodeDigitalAsset> AssignedDigitalAssets = new ZNodeGenericCollection<ZNodeDigitalAsset>();
                
                // Set Digital assets for productid and quantity
                AssignedDigitalAssets = ZNodeDigitalAssetList.CreateByProductIdAndQuantity(productId, qty).DigitalAssetCollection;

                // Loop through the digital asset retrieved for this product
                foreach (ZNodeDigitalAsset digitalAsset in AssignedDigitalAssets)
                {
                    DigitalAsset entity = digitalAssetService.GetByDigitalAssetID(digitalAsset.DigitalAssetID);
                    
                    // Set OrderLineitemId property
                    entity.OrderLineItemID = orderLineItem.OrderLineItemID;
                    
                    // Update digital asset to the database
                    digitalAssetService.Update(entity);
                }

                // Set retrieved digital asset collection to shopping product object if product has digital assets, it will display it on the receipt page along with the product name
                shoppingCartItem.Product.ZNodeDigitalAssetCollection = AssignedDigitalAssets;
            }
        }

        #endregion

        #endregion
    }
}