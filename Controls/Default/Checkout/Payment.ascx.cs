using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.Payment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using EnumPaymentType = ZNode.Libraries.ECommerce.Entities.PaymentType;

/// <summary>
/// Checkout Payment
/// </summary>
namespace WebApp
{
    /// <summary>
    /// Represents the Payment user control class.
    /// </summary>
    public partial class Controls_Default_Checkout_Payment : System.Web.UI.UserControl
    {
        #region Member Variables
        private PaymentSetting _paymentSetting;
        private ZNodeUserAccount _userAccount;
        private ZNodeShoppingCart shoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
        private ZNodePayment _payment;
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        private bool _IsMultipleVendor = false;
        //Perficient Custom Varibles
        private bool _IsMultiCardAllow = false;
        #endregion

        public event System.EventHandler EditAddressClicked;

        public event System.EventHandler PayPalClicked;

        public event System.EventHandler GoogleCheckoutClicked;

        public event System.EventHandler TwoCheckoutClicked;

        #region Public Properties

        /// <summary>
        /// Gets or sets a value indicating whether to check the items in the shopping cart are from Multiple vendors. If multiple vendors, we will show only Credit Card payment type.
        /// </summary>
        public bool IsMultipleVendorShoppingCart
        {
            get
            {
                return this._IsMultipleVendor;
            }

            set
            {
                this._IsMultipleVendor = value;
                // pnlGiftCard.Visible = !this._IsMultipleVendor;
            }
        }

        /// <summary>
        /// Sets a value indicating whether the Payment section on the final checkout page
        /// </summary>
        public bool ShowPaymentSection
        {
            set { pnlPayment.Visible = value; }
        }

        /// <summary>
        /// Gets or sets the Payment Object
        /// </summary>
        public ZNodePayment Payment
        {
            get
            {
                // Code for Paypal payment
                if (this.PaymentType == ZNode.Libraries.ECommerce.Entities.PaymentType.PAYPAL || this.PaymentType == ZNode.Libraries.ECommerce.Entities.PaymentType.GOOGLE_CHECKOUT)
                {
                    ZNodePayment _paypalpayment = new ZNodePayment();
                    this._payment = _paypalpayment;
                }

                // Check payment type            
                if (this.PaymentType == EnumPaymentType.CREDIT_CARD)
                {
                    ZNodePayment _creditCardPayment = new ZNodePayment();
                    if ((!string.IsNullOrEmpty(rdolstPaymentType.SelectedValue)) && rdolstPaymentType.SelectedItem.Text.Contains("Use card ending in"))
                    {
                        _creditCardPayment.UseToken = true;
                        _creditCardPayment.TokenId = Convert.ToInt32(rdolstPaymentType.SelectedValue);
                    }
                    else
                    {
                        _creditCardPayment.UseToken = false;

                        _creditCardPayment.CreditCard.CardNumber = txtCreditCardNumber.Text.Trim();
                        _creditCardPayment.CreditCard.CreditCardExp = lstMonth.SelectedValue + "/" + lstYear.SelectedValue;
                        _creditCardPayment.CreditCard.CardSecurityCode = txtCVV.Text.Trim();
                        //Zeon Custom Code:Starts
                        //if (chkSaveCardData.Checked)
                        //{
                        //    _creditCardPayment.SaveCardData = true;
                        //}
                        //Zeon Custom Code:Ends
                    }

                    this._payment = _creditCardPayment;
                }
                else if (this.PaymentType == EnumPaymentType.PURCHASE_ORDER)
                {
                    ZNodePayment _purchaseOrderPayment = new ZNodePayment();
                    this._payment = _purchaseOrderPayment;
                }
                else if (this.PaymentType == EnumPaymentType.COD)
                {
                    ZNodePayment _chargeOnDeliveryPayment = new ZNodePayment();
                    this._payment = _chargeOnDeliveryPayment;
                }

                // Set payment Name to display it on the order receipt
                if (this.PaymentType == EnumPaymentType.PAYPAL || this.PaymentType == EnumPaymentType.GOOGLE_CHECKOUT)
                {
                    PaymentTypeService paymentTypeService = new PaymentTypeService();
                    ZNode.Libraries.DataAccess.Entities.PaymentType pt = paymentTypeService.GetByPaymentTypeID((int)this.PaymentType);
                    if (pt != null)
                    {
                        this._payment.PaymentName = pt.Name;
                    }
                }
                else if (rdolstPaymentType.Items.Count > 0)
                {
                    this._payment.PaymentName = rdolstPaymentType.SelectedItem.Text;
                }
                //Perficient Multiple Card Payment Custom Code:Starts
                LoadMultipleTenderSetting();
                if (this._payment.IsMultipleCardPayment)
                {
                    GetMoreCreditCardInfoList();
                }
                //Perficient Multiple Card Payment Custom Code:Ends
                return this._payment;
            }

            set
            {
                this._payment = value;

                lblBillingAddress.Text = ZCommonHelper.GetAddressString(this._payment.BillingAddress);//Zeon Custom Code

                lblShippingAddress.Text = ZCommonHelper.GetAddressString(this._payment.ShippingAddress);//Zeon Custom Code
            }
        }

        /// <summary>
        /// Gets the payment type selected
        /// </summary>
        public EnumPaymentType PaymentType
        {
            get
            {
                EnumPaymentType pt = EnumPaymentType.CREDIT_CARD;

                if (Session["PaymentType"] != null)
                {
                    pt = (EnumPaymentType)Enum.ToObject(typeof(EnumPaymentType), Convert.ToInt32(Session["PaymentType"]));
                }
                else if (rdolstPaymentType.Items.Count > 0 && !rdolstPaymentType.SelectedItem.Text.Contains("Use card ending in"))
                {
                    PaymentSettingService _pmtServ = new PaymentSettingService();
                    ZNode.Libraries.DataAccess.Entities.PaymentSetting pmtSetting = _pmtServ.DeepLoadByPaymentSettingID(int.Parse(rdolstPaymentType.SelectedValue), true, DeepLoadType.IncludeChildren, typeof(ZNode.Libraries.DataAccess.Entities.PaymentType));
                    int _paymentTypeID = pmtSetting.PaymentTypeID;
                    if (_paymentTypeID == (int)EnumPaymentType.CREDIT_CARD)
                    {
                        pt = EnumPaymentType.CREDIT_CARD;
                    }
                    else if (_paymentTypeID == (int)EnumPaymentType.PURCHASE_ORDER)
                    {
                        pt = EnumPaymentType.PURCHASE_ORDER;
                    }
                    else if (_paymentTypeID == (int)EnumPaymentType.COD)
                    {
                        pt = EnumPaymentType.COD;
                    }
                }

                return pt;
            }
        }

        /// <summary>
        /// Gets or sets the payment settingId associated with the payment type selected
        /// </summary>
        public int PaymentSettingID
        {
            get
            {
                int paymentSettingId = 0;

                if (!string.IsNullOrEmpty(rdolstPaymentType.SelectedValue) && rdolstPaymentType.SelectedItem.Text.Contains("Use card ending in"))
                {
                    paymentSettingId = this.Payment.GetPaymentSettingIdByTokenId(Convert.ToInt32(rdolstPaymentType.SelectedValue));
                }
                else if (this.PaymentType == ZNode.Libraries.ECommerce.Entities.PaymentType.PAYPAL || this.PaymentType == ZNode.Libraries.ECommerce.Entities.PaymentType.GOOGLE_CHECKOUT)
                {
                    // Paypal or Google checkout payment setting selected
                    if (!string.IsNullOrEmpty(hfPaymentSettingID.Value))
                    {
                        paymentSettingId = Convert.ToInt32(hfPaymentSettingID.Value);
                    }
                }
                else if (rdolstPaymentType.Items.Count > 0)
                {
                    paymentSettingId = int.Parse(rdolstPaymentType.SelectedValue);
                }

                return paymentSettingId;
            }

            set
            {
                rdolstPaymentType.SelectedValue = value.ToString();
            }
        }

        /// <summary>
        /// Gets the PO number if purchase Order payment CreateCustomerProfile selected
        /// </summary>
        public string PurchaseOrderNumber
        {
            get
            {
                return txtPONumber.Text.Trim();
            }
        }

        /// <summary>
        /// Gets the special additional instructions
        /// </summary>
        public string AdditionalInstructions
        {
            get
            {
                return Server.HtmlEncode(txtAdditionalInstructions.Text.Trim());
            }
        }

        /// <summary>
        /// Gets a value indicating whether Save Credit Card data checkbox is checked or not
        /// </summary>
        public bool SaveCardData
        {
            get
            {
                //Zeon Custom Code
                // return chkSaveCardData.Checked;
                return false;
            }
        }


        /// <summary>
        /// Gets the special additional instructions
        /// </summary>
        public bool IsMailSubscribe
        {
            get
            {
                return chkSubscribe.Checked;
            }
        }

        /// <summary>
        /// Set Value of Card Pay Status
        /// </summary>
        public string CardPayStatus
        {
            set
            {
                lblCardPayStatus.Text = value;
                lblCardPayStatus.Visible = true;
                spanCardErr.Visible = true;
            }
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Set Cvv File path dynamically
        /// </summary>
        /// <returns>Returns the Cvv Path</returns>
        public string GetCvvPath()
        {
            string JavaScript = "javascript:popupWin=window.open('Controls/default/Cvv/cvv.htm','EIN','scrollbars,resizable,width=515,height=300,left=50,top=50');popupWin.focus();";

            CVVhyperlink.NavigateUrl = JavaScript;

            return JavaScript;
        }

        /// <summary>
        /// Binds the payment types
        /// </summary>
        public void BindPaymentTypeData()
        {
            this._userAccount = (ZNodeUserAccount)ZNodeUserAccount.CreateFromSession(ZNodeSessionKeyType.UserAccount);

            // Find is default store, so we include all profiles.
            CategoryService categoryService = new CategoryService();
            int total;
            TList<Category> categoryList = categoryService.GetByPortalID(ZNodeConfigManager.SiteConfig.PortalID, 0, 1, out total);
            bool IsDefaultPortal = categoryList.Count == 0;

            int profileID = 0;
            int registerProfileID = ZNodeProfile.DefaultRegisteredProfileId; //Perfi Custom Code
            if (this._userAccount != null)
            {
                profileID = this._userAccount.ProfileID;
            }
            else
            {
                // Get Default Registered profileId
                profileID = ZNodeProfile.DefaultRegisteredProfileId;
            }

            PaymentSettingService _pmtServ = new PaymentSettingService();
            ZNode.Libraries.DataAccess.Entities.TList<PaymentSetting> _pmtSetting = _pmtServ.GetAll();

            // Show or hide google express checkout button
            if (!this.IsMultipleVendorShoppingCart)
            {
                PortalProfileService ps = new PortalProfileService();
                TList<PortalProfile> portalProfiles = ps.GetByProfileID(profileID);
                this._paymentSetting = _pmtSetting.Find(delegate(ZNode.Libraries.DataAccess.Entities.PaymentSetting pmt)
                {
                    return (pmt.ProfileID == profileID || pmt.ProfileID == registerProfileID || (pmt.ProfileID == null && IsDefaultPortal)) &&
                        pmt.PaymentTypeID == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.GOOGLE_CHECKOUT &&
                        pmt.ActiveInd == true;
                });

                if (this._paymentSetting != null)
                {
                    string merchantID = this._paymentSetting.GatewayUsername;
                    hfPaymentSettingID.Value = this._paymentSetting.PaymentSettingID.ToString();
                    ZNodeEncryption decrypt = new ZNodeEncryption();
                    GoogleSubmitButton.ImageUrl = string.Format(GoogleSubmitButton.ImageUrl, decrypt.DecryptData(merchantID));
                    pnlGoogleCheckout.Visible = true;
                }

                // Check paypal express configured.
                /*
                PaymentSetting paypalPaymentSetting = _pmtSetting.Find(delegate(ZNode.Libraries.DataAccess.Entities.PaymentSetting pmt)
                {
                    return (pmt.ProfileID == profileID || (pmt.ProfileID == null && IsDefaultPortal)) &&
                        pmt.PaymentTypeID == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.PAYPAL &&
                        pmt.ActiveInd == true;
                });

                if (paypalPaymentSetting != null && paypalPaymentSetting.ActiveInd)
                {
                    hfPaymentSettingID.Value = paypalPaymentSetting.PaymentSettingID.ToString();
                    pnlPaypal.Visible = true;
                }*/

                PaymentSetting _2COpaymentSetting = _pmtSetting.Find(delegate(ZNode.Libraries.DataAccess.Entities.PaymentSetting pmt)
                {
                    return (pmt.ProfileID == profileID || pmt.ProfileID == registerProfileID || (pmt.ProfileID == null && IsDefaultPortal)) &&
                    pmt.PaymentTypeID == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.TwoCO &&
                    pmt.ActiveInd == true;
                });
                // Show/hide 2CO checkout button                    
                if (_2COpaymentSetting != null)
                {
                    pnl2CO.Visible = true;
                }

            }

            if (rdolstPaymentType.Items.Count == 0)
            {
                profileID = ZNodeProfile.CurrentUserProfileId;
                PortalProfileService ps = new PortalProfileService();
                TList<PortalProfile> portalProfiles = ps.GetByProfileID(profileID);

                if (this.IsMultipleVendorShoppingCart)
                {
                    _pmtSetting.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.PaymentSetting pmt)
                    {
                        return (pmt.ProfileID == profileID) || (pmt.ProfileID == registerProfileID) || (pmt.ProfileID == null && IsDefaultPortal) &&
                                (pmt.PaymentTypeID == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.CREDIT_CARD) &&
                                pmt.ActiveInd == true;
                    });
                }
                else
                {
                    var _profilePayments = _pmtSetting.FindAll(delegate(ZNode.Libraries.DataAccess.Entities.PaymentSetting pmt)
                    {
                        return (pmt.ProfileID == profileID) &&
                            (

                            //pmt.PaymentTypeID != (int)ZNode.Libraries.ECommerce.Entities.PaymentType.PAYPAL && // Zeon Customization :: Bind Paypal payment option in radio button list
                            pmt.PaymentTypeID != (int)ZNode.Libraries.ECommerce.Entities.PaymentType.GOOGLE_CHECKOUT &&
                            pmt.PaymentTypeID != (int)ZNode.Libraries.ECommerce.Entities.PaymentType.TwoCO) &&
                            pmt.ActiveInd == true;
                    });

                    //Perfi Custom Code:Start                   
                    var _defaultProfilePayments = _pmtSetting.FindAll(delegate(ZNode.Libraries.DataAccess.Entities.PaymentSetting pmt)
                     {
                         return (pmt.ProfileID == registerProfileID) &&
                             (

                             //pmt.PaymentTypeID != (int)ZNode.Libraries.ECommerce.Entities.PaymentType.PAYPAL && // Zeon Customization :: Bind Paypal payment option in radio button list
                             pmt.PaymentTypeID != (int)ZNode.Libraries.ECommerce.Entities.PaymentType.GOOGLE_CHECKOUT &&
                             pmt.PaymentTypeID != (int)ZNode.Libraries.ECommerce.Entities.PaymentType.TwoCO) &&
                             pmt.ActiveInd == true;
                     });

                    if (_defaultProfilePayments.Any())
                    {
                        _profilePayments.AddRange(_defaultProfilePayments.Where(x => !_profilePayments.Any(y => y.PaymentTypeID == x.PaymentTypeID)).ToList());
                    }

                    //Perfi Custom Code:end

                    var allProfilePayments = _pmtSetting.FindAll(delegate(ZNode.Libraries.DataAccess.Entities.PaymentSetting pmt)
                    {
                        return (pmt.ProfileID == null && IsDefaultPortal) &&
                            (
                            //pmt.PaymentTypeID != (int)ZNode.Libraries.ECommerce.Entities.PaymentType.PAYPAL && // Zeon Customization :: Bind Paypal payment option in radio button list
                            pmt.PaymentTypeID != (int)ZNode.Libraries.ECommerce.Entities.PaymentType.GOOGLE_CHECKOUT &&
                            pmt.PaymentTypeID != (int)ZNode.Libraries.ECommerce.Entities.PaymentType.TwoCO) &&
                            pmt.ActiveInd == true;
                    });

                    if (allProfilePayments.Any())
                    {
                        _profilePayments.AddRange(allProfilePayments.Where(x => !_profilePayments.Any(y => y.PaymentTypeID == x.PaymentTypeID)).ToList());
                    }

                    _pmtSetting = _profilePayments;
                }

                _pmtServ.DeepLoad(_pmtSetting, true, DeepLoadType.IncludeChildren, typeof(ZNode.Libraries.DataAccess.Entities.PaymentType));
                _pmtSetting.Sort("DisplayOrder");

                // If the customer has choosen saved card option
                if (this._userAccount != null)
                {
                    PaymentTokenService serv = new PaymentTokenService();
                    TList<PaymentToken> list = serv.GetByAccountID(this._userAccount.AccountID);

                    foreach (PaymentToken token in list)
                    {
                        ListItem scardli = new ListItem();
                        scardli.Text = "Use card ending in " + token.CreditCardDescription;
                        scardli.Value = token.PaymentTokenID.ToString();

                        rdolstPaymentType.Items.Add(scardli);
                    }
                }

                foreach (PaymentSetting _pmt in _pmtSetting)
                {
                    ListItem li = new ListItem();

                    if (_pmt.PaymentTypeID == (int)EnumPaymentType.PAYPAL)
                    {
                        string paypalpaymentacceptanceimg = "themes/" + ZNodeCatalogManager.Theme + "/Images/btn_xpressCheckout.gif";
                        string str = "<p><img src='" + paypalpaymentacceptanceimg + "' alt='" + _pmt.PaymentTypeIDSource.Name + "' title='" + _pmt.PaymentTypeIDSource.Name + "'/></p>";
                        li.Text = str;
                        li.Value = _pmt.PaymentSettingID.ToString();
                        hfPaymentSettingID.Value = _pmt.PaymentSettingID.ToString();
                    }
                    else
                    {
                        li.Text = _pmt.PaymentTypeIDSource.Name;
                        li.Value = _pmt.PaymentSettingID.ToString();
                    }

                    rdolstPaymentType.Items.Add(li);

                    if (_pmt.PaymentTypeID == (int)EnumPaymentType.CREDIT_CARD)
                    {
                        imgAmex.Visible = (bool)_pmt.EnableAmex;
                        imgMastercard.Visible = (bool)_pmt.EnableMasterCard;
                        imgVisa.Visible = (bool)_pmt.EnableVisa;
                        imgDiscover.Visible = (bool)_pmt.EnableDiscover;
                    }
                }

                if (this._userAccount != null)
                {
                    lblBillingAddress.Text = ZCommonHelper.GetAddressString(this._userAccount.BillingAddress);//Zeon Custom Code
                    lblShippingAddress.Text = ZCommonHelper.GetAddressString(this._userAccount.ShippingAddress);
                }

                // Select first item
                if (rdolstPaymentType.Items.Count > 0)
                {
                    rdolstPaymentType.Items[0].Selected = true;

                    // Show appropriate payment control
                    this.SetPaymentControl();
                }
                else
                {
                    if (this.IsMultipleVendorShoppingCart)
                    {
                        pnlPayment.Visible = false;
                        ErrorPanel.Visible = true;

                        StringBuilder productName = new StringBuilder();
                        ZNodeShoppingCart cartItems = new ZNodeShoppingCart();
                        if (shoppingCart != null)
                        {
                            foreach (ZNodeShoppingCartItem cartItem in shoppingCart.ShoppingCartItems)
                            {
                                if (cartItem.Product.PortalID != ZNodeConfigManager.SiteConfig.PortalID)
                                {
                                    ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
                                    item.Product = cartItem.Product;
                                    productName.Append(item.Product.Name.ToString());
                                    productName.Append(" ,");
                                }
                            }
                            productName = productName.Remove(productName.Length - 1, 1);
                        }

                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("General Checkout Exception - UnableToProcessRequest : Users purchasing items from multiple vendors, but the site is not configured for Credit Card payment");

                        lblError.Text = "The following product above have not been set up correctly by the vendor : " + productName + "<br>" + this.GetLocalResourceObject("NotSupported").ToString();
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Shows the appropriate payment control based on the option selected
        /// </summary>
        public void SetPaymentControl()
        {
            PaymentSettingService _pmtServ = new PaymentSettingService();
            ZNode.Libraries.DataAccess.Entities.PaymentSetting pmtSetting = _pmtServ.DeepLoadByPaymentSettingID(int.Parse(rdolstPaymentType.SelectedValue), true, DeepLoadType.IncludeChildren, typeof(ZNode.Libraries.DataAccess.Entities.PaymentType));

            // Hide Check here to save... checkbox. This will be set to true if supported below.
            //chkSaveCardData.Visible = false;//Zeon Custom Code

            if (!string.IsNullOrEmpty(rdolstPaymentType.SelectedValue) && rdolstPaymentType.SelectedItem.Text.Contains("Use card ending in"))
            {
                // Hide purchase order panel
                pnlPurchaseOrder.Visible = false;

                // Show credit card panel
                pnlCreditCard.Visible = false;

                // Show Remove Credit Card link button
                lnkRemoveCardData.Visible = true;

                // Uncheck the Save Credit Card data
                //chkSaveCardData.Checked = false;//Zeon Custom Code

                // Remove the Purchase Order number
                txtPONumber.Text = string.Empty;

                return;
            }

            int paymentTypeId = pmtSetting.PaymentTypeID;

            if (paymentTypeId == (int)EnumPaymentType.CREDIT_CARD)
            {
                // Show credit card panel
                pnlCreditCard.Visible = true;
                txtCreditCardNumber.Focus();
                //Zeon Custom Code
                //if (pmtSetting.GatewayTypeID == (int)GatewayType.VERISIGN)
                //{
                //    chkSaveCardData.Visible = true;
                //}

                // Hide purchase order panel
                pnlPurchaseOrder.Visible = false;

                // Hide Remove Credit Card link button
                lnkRemoveCardData.Visible = false;

                // Uncheck the Save Credit Card data
                //chkSaveCardData.Checked = false;//Zeon Custom Code

                pnlCreditCard.Focus();
            }
            else if (paymentTypeId == (int)EnumPaymentType.PURCHASE_ORDER)
            {
                // Hide credit card panel
                pnlCreditCard.Visible = false;

                // Show purchase order panel
                pnlPurchaseOrder.Visible = true;
                txtPONumber.Focus();
                // Hide Remove Credit Card link button
                lnkRemoveCardData.Visible = false;

                // Uncheck the Save Credit Card data
                //chkSaveCardData.Checked = false;//Zeon Custom Code
            }
            else if (paymentTypeId == (int)EnumPaymentType.COD)
            {
                // Hide credit card panel
                pnlCreditCard.Visible = false;

                // Hide purchase order panel
                pnlPurchaseOrder.Visible = false;

                // Hide Remove Credit Card link button
                lnkRemoveCardData.Visible = false;

                // Uncheck the Save Credit Card data
                //chkSaveCardData.Checked = false;//Zeon Custom Code

                addtionalInstruction.Focus();
            }
            else if (paymentTypeId == (int)EnumPaymentType.PAYPAL)
            {
                pnlCreditCard.Visible = false;

                // Hide purchase order panel
                pnlPurchaseOrder.Visible = false;

                // Hide Remove Credit Card link button
                lnkRemoveCardData.Visible = false;

                if (this.PayPalClicked != null)
                {
                    Session["PaymentType"] = (int)ZNode.Libraries.ECommerce.Entities.PaymentType.PAYPAL;
                    this.PayPalClicked(null, null);
                    Session["PaymentType"] = null;
                }
            }
        }

        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Payment type changed event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstPaymentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.SetPaymentControl();
        }

        /// <summary>
        /// Raises the edit address event to the parent control
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LnkEditBilling_Click(object sender, EventArgs e)
        {
            if (this.EditAddressClicked != null)
            {
                this.EditAddressClicked(sender, e);
            }
        }

        /// <summary>
        /// Raises the edit address event to the parent control
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LnkEditShipping_Click(object sender, EventArgs e)
        {
            if (this.EditAddressClicked != null)
            {
                this.EditAddressClicked(sender, e);
            }
        }
        protected void Paypal_Click(object sender, ImageClickEventArgs e)
        {
            if (this.PayPalClicked != null)
            {
                Session["PaymentType"] = (int)ZNode.Libraries.ECommerce.Entities.PaymentType.PAYPAL;
                this.PayPalClicked(sender, e);
                Session["PaymentType"] = null;
            }
        }

        protected void GoogleSubmitButton_Click(object sender, ImageClickEventArgs e)
        {
            if (this.GoogleCheckoutClicked != null)
            {
                Session["PaymentType"] = (int)ZNode.Libraries.ECommerce.Entities.PaymentType.GOOGLE_CHECKOUT;
                this.GoogleCheckoutClicked(sender, e);

                Session["PaymentType"] = null;
            }
        }

        protected void TwoCheckoutSubmitButton_Click(object sender, EventArgs e)
        {
            if (this.TwoCheckoutClicked != null)
            {
                this.TwoCheckoutClicked(sender, e);
            }
        }

        /// <summary>
        /// To remove the saved credit card info
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LnkRemoveCardData_Click(object sender, EventArgs e)
        {
            this.Payment.DeleteGatewayToken(Convert.ToInt32(rdolstPaymentType.SelectedValue));
            rdolstPaymentType.Items.Clear();
            this.BindPaymentTypeData();
        }

        //protected void BtnApplyGiftCard_Click(object sender, EventArgs e)
        //{
        //    this.shoppingCart.AddGiftCard(txtGiftCardNumber.Text);

        //    txtGiftCardNumber.Text = this.shoppingCart.GiftCardNumber;
        //    lblGiftCardMessage.Text = this.shoppingCart.GiftCardMessage;
        //}

        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (rdolstPaymentType.Items.Count == 0)
            {
                this.BindPaymentTypeData();
            }

            if (lstYear.Items.Count < 1)
            {
                this.BindYearList();
            }

            // Set Image Path
            imgAmex.Src = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/card_amex.gif";
            imgDiscover.Src = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/card_discover.gif";
            imgMastercard.Src = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/card_mastercard.gif";
            imgVisa.Src = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/card_visa.gif";

            CVVhyperlink.Attributes.Add("OnClientClick", this.GetCvvPath() + "return false");

            if (IsPostBack)
            {
                if (this.PaymentType == EnumPaymentType.CREDIT_CARD)
                {
                    pnlCreditCard.Visible = true;
                    Localize3.Visible = true;
                    txtCreditCardNumber.Visible = true;
                    Localize4.Visible = true;
                    lstMonth.Visible = true;
                    lstYear.Visible = true;
                    Localize6.Visible = true;
                    txtCVV.Visible = true;
                    CVVhyperlink.Visible = true;
                }

                if (!string.IsNullOrEmpty(rdolstPaymentType.SelectedValue) && rdolstPaymentType.SelectedItem.Text.Contains("Use card ending in"))
                {
                    pnlPurchaseOrder.Visible = false;
                    pnlCreditCard.Visible = false;
                    lnkRemoveCardData.Visible = true;
                    //chkSaveCardData.Checked = false;//Zeon Custom Code
                    txtPONumber.Text = string.Empty;

                    return;
                }
                //Perft Custom Code
                lblCardPayStatus.Visible = false;
                spanCardErr.Visible = false;
            }


            if (!IsPostBack)
            {
                Session["PaymentType"] = null;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (this.shoppingCart.Total <= 0)
            {
                pnlCreditCard.Visible = false;
                pnlCOD.Visible = false;
                pnlIPCCreditCard.Visible = false;
                pnlPurchaseOrder.Visible = false;
                PaymentTypePanel.Visible = false;
                pnl2CO.Visible = false;
            }
            else
            {
                PaymentTypePanel.Visible = true;
                this.BindPaymentTypeData();
            }

            if (this.shoppingCart.IsGiftCardApplied && this.shoppingCart.Total == 0)
            {
                pnlPayment.Visible = true;
                pnlGoogleCheckout.Visible = false;
                pnlPaypal.Visible = false;
                pnl2CO.Visible = false;
                //pnlGiftCardNumber.Visible = true;
            }

            //Perficient Custom Code
            LoadMultipleTenderSetting();
            RegisterClientScriptClasses();
           
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Binds the expiration year list based on current year
        /// </summary>
        private void BindYearList()
        {
            ListItem defaultItem = new ListItem("-- Year --", "0");
            lstYear.Items.Add(defaultItem);
            defaultItem.Selected = true;

            int currentYear = System.DateTime.Now.Year;
            int counter = 25;

            do
            {
                string itemtext = currentYear.ToString();

                lstYear.Items.Add(new ListItem(itemtext));

                currentYear = currentYear + 1;

                counter = counter - 1;
            }
            while (counter > 0);
        }

        /// <summary>
        /// Returns an instance of the PaymentSetting  class.
        /// </summary>
        /// <param name="paymentTypeID">Payment Type ID</param>
        /// <returns>Returns the Payment Setting</returns>
        private PaymentSetting GetByPaymentTypeId(int paymentTypeID)
        {
            PaymentSettingService _pmtServ = new PaymentSettingService();
            int? profileID = 0;
            if (this._userAccount != null)
            {
                // Get loggedIn user profile
                profileID = this._userAccount.ProfileID;
            }

            TList<PaymentSetting> list = _pmtServ.GetByPaymentTypeID(paymentTypeID);
            list.Filter = "ActiveInd = true AND ProfileID = " + profileID;

            if (list.Count == 0)
            {
                // Get All Profiles payment setting
                list.Filter = "ActiveInd = true";
            }

            return list[0];
        }

        #endregion

        #region Zeon custom code
        /// <summary>
        /// Handeles CheckBoxRequired Server Validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CheckBoxRequired_ServerValidate(object sender, ServerValidateEventArgs e)
        {
            e.IsValid = chkTermsandCondition.Checked;
        }

        /// <summary>
        /// Set Check box Value based on Checkout senario
        /// </summary>
        /// <param name="isChecked">bool</param>
        public void SetSubscriptionCheckBox(bool isChecked)
        {
            chkSubscribe.Checked = isChecked;
        }

        #endregion

        #region Perficient Multiple Tenders/Card Custom Code

        /// <summary>
        /// Registers the Script
        /// </summary>
        private void RegisterClientScriptClasses()
        {
            StringBuilder script = new StringBuilder();
            script.Append("var perfiPayment = new Payments({");
            script.Append("\"Controls\":{");
            script.AppendFormat("\"{0}\":\"{1}\"", txtDefaultCCTotal.ID, txtDefaultCCTotal.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", hdnMoreCreditCardCount.ID, hdnMoreCreditCardCount.ClientID);
            script.Append("},\"Messages\":{");
            script.AppendFormat("\"OrderTotalErrMesg\":\"{0}\"", this.GetLocalResourceObject("InvalidOrderTotalErrorMessage").ToString());
            script.Append("}");
            script.Append("});perfiPayment.Init();");
            this.Page.ClientScript.RegisterStartupScript(GetType(), "Payments", script.ToString(), true);
        }

        /// <summary>
        /// Load Credit Card display Setting
        /// </summary>
        private void LoadMultipleTenderSetting()
        {
            if (ConfigurationManager.AppSettings["EnableMultipleCardPayment"] != null && ConfigurationManager.AppSettings["EnableMultipleCardPayment"].ToString().Length > 0)
            {
                bool isMutiCardEnable = false;
                string[] multiCardPayPortal = ConfigurationManager.AppSettings["EnableMultipleCardPayment"].ToString().Split(',');
                foreach (string portal in multiCardPayPortal)
                {
                    if (portal.Equals(ZNodeConfigManager.SiteConfig.PortalID.ToString(), StringComparison.CurrentCultureIgnoreCase))
                    {
                        isMutiCardEnable = true;
                        break;
                    }
                }
                this._payment.IsMultipleCardPayment = _IsMultiCardAllow = divMultipleTenders.Visible = divAddMoreCardLink.Visible = lblCardAmtTotalErr.Visible = lblMultiCardErr.Visible = isMutiCardEnable;
            }
        }

        /// <summary>
        /// Create All details list of credit card payments 
        /// </summary>
        private void GetMoreCreditCardInfoList()
        {
            ZNode.Libraries.ECommerce.Entities.CreditCard creditCard1;
            ZNode.Libraries.ECommerce.Entities.CreditCard creditCard2;
            if (!string.IsNullOrEmpty(txtDefaultCCTotal.Text))
            {
                this._payment.CreditCard.Amount = Decimal.Parse(txtDefaultCCTotal.Text);
            }
            if (uxMulTender1.IsTenderVisible)
            {
                creditCard1 = uxMulTender1.GetCreditCardDetails();
                if (!string.IsNullOrEmpty(creditCard1.CardNumber))
                {
                    this._payment.CreditCardList.Add(creditCard1);
                }
            }
            if (uxMulTender2.IsTenderVisible)
            {
                creditCard2 = uxMulTender2.GetCreditCardDetails();
                if (!string.IsNullOrEmpty(creditCard2.CardNumber))
                {
                    this._payment.CreditCardList.Add(creditCard2);
                }
            }
        }

        /// <summary>
        /// Comare Card Total with Cart Total
        /// </summary>
        /// <param name="orderTotal"></param>
        /// <returns></returns>
        public bool CompareCCTotalWithOrderTotal(decimal orderTotal)
        {
            decimal AllCardTotal = 0;
            bool isTotalValid = false;
            //Check if all Used Credit card has valid amounts
            bool isDefaultCardDataValid = string.IsNullOrWhiteSpace(txtCreditCardNumber.Text) || Decimal.Parse(txtCVV.Text) <= 0 || lstMonth.SelectedIndex == 0 || lstYear.SelectedIndex == 0 ? false : true;
            bool isAllCard1HasValidData = true;
            bool isAllCard2HasValidData = true;
            isAllCard1HasValidData = uxMulTender1.HasAllValidData();
            isAllCard2HasValidData = uxMulTender2.HasAllValidData();
            if (isDefaultCardDataValid && isAllCard1HasValidData && isAllCard2HasValidData)
            {
                AllCardTotal = decimal.Parse(txtDefaultCCTotal.Text) + uxMulTender1.GetCardTotal() + uxMulTender2.GetCardTotal();
                if (AllCardTotal == orderTotal)
                {
                    isTotalValid = true;
                }
            }
            return isTotalValid;
        }
        #endregion
    }
}