﻿using System;
using System.Web.Security;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    public partial class zPayPalOrderSummary : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeShoppingCart _shoppingCart = (ZNodeShoppingCart)ZNodeCheckout.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
        private ZNodeShoppingCart _filteredShoppingCart;
        private int _PortalID;
        private bool _isShippingEditAllow = false;
        #endregion

        #region Public Properties

        public int PortalID
        {
            get { return this._PortalID; }
            set { this._PortalID = value; }
        }

        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["mode"] == null)
            {
                Response.Redirect("checkout.aspx");
            }

        }

        /// <summary>
        ///  Page Pre Render Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            ZNodeUserAccount userAccount = ZNodeUserAccount.CurrentAccount();
            if (userAccount != null)
            {
                //Check if user has right to edit shipping charges
                _isShippingEditAllow = Roles.IsUserInRole("ADMIN") || Roles.IsUserInRole("CUSTOMER SERVICE REP") ? true : false;
                this.BindTotals();
            }
        }

        #endregion

        #region[Public Methods]

        /// <summary>
        /// Represents the BindGrid method
        /// </summary>
        public void BindGrid()
        {
            this._filteredShoppingCart = this.FilterCartItemsByPortal(this._shoppingCart);
            Session[string.Format("{0}_{1}", ZNodeSessionKeyType.ShoppingCart.ToString(), this.PortalID)] = this._filteredShoppingCart;
            uxCart.DataSource = this._filteredShoppingCart.ShoppingCartItems;
            uxCart.DataBind();
            foreach (GridViewRow row in uxCart.Rows)
            {
                foreach (TableCell cell in row.Cells)
                {
                    cell.EnableViewState = false;
                }
            }


        }

        #endregion

        #region[Private Methods]

        /// <summary>
        /// Bind Shopping cart details for review
        /// </summary>
        /// <param name="_shopCart"></param>
        /// <returns></returns>
        private ZNodeShoppingCart FilterCartItemsByPortal(ZNodeShoppingCart _shopCart)
        {
            ZNodeShoppingCart cartItems = new ZNodeShoppingCart();
            if (_shopCart != null)
            {
                foreach (ZNodeShoppingCartItem cartItem in _shopCart.ShoppingCartItems)
                {
                    if (cartItem.Product.PortalID == this.PortalID)
                    {
                        ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
                        item.Product = cartItem.Product;
                        item.Quantity = cartItem.Quantity;
                        cartItems.AddToCart(item);
                    }
                }
            }

            if (_shopCart.ShoppingCartItems.Count == cartItems.ShoppingCartItems.Count)
            {
                cartItems = _shopCart;
            }

            return cartItems;
        }

        /// <summary>
        ///  Represents the BindTotals method
        /// </summary>
        private void BindTotals()
        {
            // Bind totals
            if (_filteredShoppingCart == null)
            {
                this._filteredShoppingCart = this.FilterCartItemsByPortal(this._shoppingCart);
            }
            string shippingCharges = string.Empty;
            SubTotal.Text = this._filteredShoppingCart.SubTotal.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
            Discount.Text = "-" + this._filteredShoppingCart.Discount.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
            lblGiftCardAmount.Text = "-" + this._filteredShoppingCart.GiftCardAmount.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
            Tax.Text = this._filteredShoppingCart.OrderLevelTaxes.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
            shippingCharges = this._filteredShoppingCart.ShippingCost.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
            Total.Text = this._filteredShoppingCart.Total.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
            rowDiscount.Visible = this._filteredShoppingCart.Discount > 0 ? true : false;
            divGiftCardAmount.Visible = this._filteredShoppingCart.GiftCardAmount > 0 ? true : false;
            rowTax.Visible = this._filteredShoppingCart.OrderLevelTaxes > 0 ? true : false;
            ltshippingName.Text = "(" + this._filteredShoppingCart.Shipping.ShippingName + ")";
            if (this._filteredShoppingCart.ShippingCost == 0 && (this._filteredShoppingCart.Shipping.ShippingName.Equals("Ground Shipping", StringComparison.OrdinalIgnoreCase)))
            {
                shippingCharges = this.GetLocalResourceObject("GroundFreeShippingMessage").ToString();
                rowShipping.Visible = true;
            }
            else if (this._filteredShoppingCart.ShippingCost == 0 && (this._filteredShoppingCart.Shipping.ShippingName.Equals("Ground", StringComparison.OrdinalIgnoreCase)))
            {
                shippingCharges = this.GetLocalResourceObject("GroundFreeShippingHTML").ToString();
                rowShipping.Visible = true;
            }
            else
            {
                rowShipping.Visible = this._filteredShoppingCart.ShippingCost > 0 ? true : false;
            }

            if (this._filteredShoppingCart != null && this._filteredShoppingCart.Shipping != null && !string.IsNullOrEmpty(this._filteredShoppingCart.Shipping.ShippingName))
            {
                lblShippingName.Text = this._filteredShoppingCart.Shipping.ShippingName;
            }

            BindRoleWiseShippingCharges(shippingCharges);
        }


        /// <summary>
        /// Set Shipping Charges to Control
        /// </summary>
        /// <param name="shippingCharges">string</param>
        private void BindRoleWiseShippingCharges(string shippingCharges)
        {
            if (!string.IsNullOrWhiteSpace(shippingCharges))
            {
                if (_isShippingEditAllow && this._shoppingCart.IsShipngChrgsExtended && this._shoppingCart.ExtendedShippingCost != decimal.MinValue)
                {
                    decimal shippingCharge = this._filteredShoppingCart.ShippingCost;
                    this._filteredShoppingCart.IsShipngChrgsExtended = this._shoppingCart.IsShipngChrgsExtended;
                    this._filteredShoppingCart.ExtendedShippingCost = this._shoppingCart.ExtendedShippingCost;
                    decimal updatedTotal = (this._filteredShoppingCart.Total - this._filteredShoppingCart.ShippingCost) + this._shoppingCart.ExtendedShippingCost;
                    Total.Text = updatedTotal.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                    Shipping.Text = this._shoppingCart.ExtendedShippingCost.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                }
                else
                {
                    Shipping.Text = shippingCharges;
                }
            }
        }
        #endregion
    }
}