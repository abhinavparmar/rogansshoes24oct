<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Checkout_Order" CodeBehind="Order.ascx.cs" %>
<div class="ShoppingCart">
    <h3>
        <asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtOrderInformation"></asp:Localize>
    </h3>
    <div>
        <asp:GridView ID="uxCart" runat="server" AutoGenerateColumns="False" EmptyDataText="Shopping Cart Empty"
            GridLines="None" CellPadding="4" CssClass="Grid"
            meta:resourcekey="uxCartResource1" ClientIDMode="Static">
            <Columns>
                <asp:TemplateField HeaderText="PRODUCT DETAILS" HeaderStyle-HorizontalAlign="Left"
                    meta:resourcekey="TemplateFieldResource2">
                    <ItemTemplate>
                        <div class="ProductImage">
                            <a id="A1" href='<%# DataBinder.Eval(Container.DataItem, "Product.ViewProductLink").ToString() %>'
                                runat="server">
                                <img id="Img1" alt='<%# DataBinder.Eval(Container.DataItem, "Product.ImageAltTag") %>'
                                    border='0' src='<%# DataBinder.Eval(Container.DataItem, "Product.ThumbnailImageFilePath") %>'
                                    runat="server" title='<%# DataBinder.Eval(Container.DataItem, "Product.ImageAltTag") %>' />
                            </a>
                        </div>
                        <div class="ProductName">
                            <a runat="server" href='<%# DataBinder.Eval(Container.DataItem, "Product.ViewProductLink").ToString()%>' title='<%# DataBinder.Eval(Container.DataItem, "Product.Name").ToString()%>'><%# DataBinder.Eval(Container.DataItem, "Product.Name").ToString()%></a>
                        </div>
                        <div class="Description">
                            <div class="ProductDesc">
                                <p>
                                    <strong>Item#:</strong>
                                    <%# DataBinder.Eval(Container.DataItem, "Product.ProductNum").ToString()%>
                                </p>
                                <%# DataBinder.Eval(Container.DataItem, "Product.ShoppingCartDescription").ToString()%>
                            </div>
                            <div>
                            </div>
                            <div id="divNotes" runat="server" visible='<%#!string.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "Notes").ToString())%>'>
                                <asp:Literal ID="ltrNotes" runat="server" meta:resourcekey="ltrNotes" ClientIDMode="Static"></asp:Literal><%# DataBinder.Eval(Container.DataItem, "Notes").ToString()%>
                            </div>
                        </div>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SUBTOTAL" HeaderStyle-HorizontalAlign="Left"
                    meta:resourcekey="TemplateFieldResource3">
                    <ItemTemplate>
                        <div class="Price unite-price">
                            <%# DataBinder.Eval(Container.DataItem, "UnitPrice", "{0:c}") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix()%>
                        </div>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="QTY" HeaderStyle-HorizontalAlign="Left"
                    meta:resourcekey="TemplateFieldResource1">
                    <ItemTemplate>
                        <div class="Quantity">
                            <%# DataBinder.Eval(Container.DataItem, "Quantity").ToString()%>
                        </div>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="TOTAL" HeaderStyle-HorizontalAlign="Right"
                    ItemStyle-HorizontalAlign="Right" meta:resourcekey="TemplateFieldResource4">
                    <ItemTemplate>
                        <div class="Price">
                            <%# DataBinder.Eval(Container.DataItem, "ExtendedPrice", "{0:c}") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix()%>
                        </div>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="Footer" />
            <RowStyle CssClass="Row" />
            <HeaderStyle CssClass="Header" />
            <AlternatingRowStyle CssClass="AlternatingRow" />
        </asp:GridView>
    </div>
</div>
<div class="ShoppingCart Form">
    <div class="TotalBox">
        <div class="LeftContent">
        </div>
        <div class="ShipBy" id="ShipSection" runat="server">
            <h3>
                <asp:Localize ID="Localize1" runat="server" meta:resourceKey="txtShipBy"></asp:Localize></h3>
            <span>
                <%--  <asp:DropDownList ID="lstShipping" runat="server" AutoPostBack="True"
                        OnSelectedIndexChanged="LstShipping_SelectedIndexChanged"
                        meta:resourcekey="lstShippingResource1" ClientIDMode="Static">
                    </asp:DropDownList>--%>
                <p>
                    <asp:Localize ID="locShippingTitle" runat="server" meta:resourceKey="locShippingTitle"></asp:Localize>
                </p>
                <asp:RadioButtonList ID="rdolstShipping" runat="server" OnSelectedIndexChanged="LstShipping_SelectedIndexChanged" AutoPostBack="true">
                </asp:RadioButtonList>

            </span>
            <div class="Error">
                <asp:Literal ID="uxErrorMsg" EnableViewState="False" runat="server"
                    meta:resourcekey="uxErrorMsgResource1" ClientIDMode="Static"></asp:Literal>
            </div>
        </div>


        <div class="RightContent checkout-price-section">
            <div class="CartPricingSection"> 
              <table>
                <tr> 
                        
                    <td><div class="ShoppingTotalContent clearfix">
                        <asp:Localize ID="Localize2" runat="server" meta:resourceKey="txtsubtotal"></asp:Localize>
                    </div></td>
                    <td> <div class="ShoppingTotalContent">
                            <asp:Label ID="SubTotal" runat="server" EnableViewState="False"
                                meta:resourcekey="SubTotalResource1" ClientIDMode="Static"></asp:Label>
                        </div></td>
                </tr> 
                <tr class="SubTotal" runat="server" id="rowDiscount" visible="false"> 
                     <td><div class="ShoppingTotalContent">
                        <asp:Localize ID="Localize3" runat="server" meta:resourceKey="txtDiscount"></asp:Localize>
                    </div></td>
                    <td><div class="ShoppingTotalContent">
                        <asp:Label ID="Discount" runat="server" EnableViewState="False"
                            meta:resourcekey="DiscountResource1" ClientIDMode="Static"></asp:Label>
                    </div></td>
                     
                </tr> 
                <tr runat="server" id="rowTax" visible="false"> 
                    <td><div class="ShoppingTotalContent">
                        <asp:Localize ID="Localize4" runat="server" meta:resourceKey="txtTax"></asp:Localize>
                        <asp:Label ID="TaxPct" runat="server" EnableViewState="False"
                            meta:resourcekey="TaxPctResource1" ClientIDMode="Static"></asp:Label>
                    </div></td> 
                     <td><div class="ShoppingTotalContent">
                        <asp:Label ID="Tax" runat="server" EnableViewState="False"
                            meta:resourcekey="TaxResource1" ClientIDMode="Static"></asp:Label>
                    </div></td>
                </tr> 
                <tr runat="server" id="rowShipping" enableviewstate="false" visible="false"> 
                    <td><div class="ShoppingTotalContent">
                        <asp:Localize ID="Localize6" runat="server" meta:resourceKey="txtShipping">
                        </asp:Localize><span class="shipname">
                            <asp:Literal ID="ltshippingName" runat="server"></asp:Literal></span>:
                    </div></td> 
                      <td><div class="ShoppingTotalContent">
                     <asp:Label ID="Shipping" runat="server" EnableViewState="False"
                            meta:resourcekey="ShippingResource2" ClientIDMode="Static"></asp:Label>
                        <%--Admin and CSR Roles UI section Starts--%>
                        <asp:TextBox ID="txtShipping" runat="server" Width="20" ValidationGroup="ShippingCharges"></asp:TextBox>
                        <asp:LinkButton runat="server" CssClass="Updatelink" Text="Update" OnClick="lnkShippingUpdate_Click" ID="lnkShippingUpdate" Visible="false"  ValidationGroup="ShippingCharges"></asp:LinkButton>
                    </div></td>
                </tr> 
                <tr> 
                    <td colspan="2"> <asp:RequiredFieldValidator ID="rgvShipping" CssClass="Error righttxt" runat="server" ControlToValidate="txtShipping" SetFocusOnError="true" ErrorMessage="You must enter a valid Shipping Price" Visible="false"  ValidationGroup="ShippingCharges" Display="Dynamic"></asp:RequiredFieldValidator>
                     <asp:CompareValidator ID="copValShipping" runat="server" ControlToValidate="txtShipping"
                            Type="Currency" Operator="DataTypeCheck" ErrorMessage="You must enter a valid Shipping Price (ex: 123.45)"
                            CssClass="Error righttxt" Display="Dynamic" Visible="false"  ValidationGroup="ShippingCharges" />
                    <asp:RangeValidator ID="rangeValiShipping" runat="server" ControlToValidate="txtShipping"  ValidationGroup="ShippingCharges"
                            ErrorMessage="You must enter a Shipping Price value between $0 and $999,999.99"
                            MaximumValue="99999999" Type="Currency" MinimumValue="0" Display="Dynamic" CssClass="Error" Visible="false"></asp:RangeValidator>
                     <%--Admin and CSR Roles UI section Ends--%></td>
                </tr> 
                <tr runat="server" id="divAllFreeShipping" enableviewstate="false" visible="false"> 
                    <td> <div class="ShoppingTotalContent">
                        <asp:Localize ID="Localize8" runat="server" meta:resourceKey="txtShipping"></asp:Localize>
                    </div></td> 
                      <td><div class="ShoppingTotalContent">
                        <asp:Label ID="lblFreeShippingText" runat="server"></asp:Label>
                    </div></td>
                </tr> 
                 <tr runat="server" id="divGiftCardAmount" visible="false"> 
                    <td> <div class="ShoppingTotalContent">
                        <asp:Localize ID="Localize7" runat="server" meta:resourceKey="lblGiftCard"></asp:Localize>
                    </div></td> 
                      <td>   <div class="ShoppingTotalContent">
                        <asp:Label ID="lblGiftCardAmount" runat="server" EnableViewState="False" ClientIDMode="Static"></asp:Label>
                    </div></td>
                </tr> 
                 <tr class="TopBorder"> 
                    <td> <div class="ShoppingTotalAmountContent">
                        <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:CommonCaption, Total%>" ClientIDMode="Static" />
                    </div></td> 
                     <td> <div class="ShoppingTotalAmountContent">
                        <asp:Label ID="Total" runat="server" EnableViewState="False"
                            meta:resourcekey="TotalResource2" ClientIDMode="Static"></asp:Label>
                    </div></td>
                </tr>  
            </table> 
            </div>
            <div class="CheckoutShoppingCartLink">
                <asp:Localize ID="locShoppingCartLink" runat="server" meta:resourcekey="locShoppingCartLink"></asp:Localize>
                <a href="shoppingcart.aspx" title="Edit Shopping Cart">
                    <asp:Localize ID="locShoppingCartLinkText" runat="server" meta:resourcekey="locShoppingCartLinkText"></asp:Localize></a>
            </div>
        </div>
    </div>
    <div class="Clear"></div>
</div>

