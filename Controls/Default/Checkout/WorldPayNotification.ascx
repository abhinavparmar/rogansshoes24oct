<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Checkout_WorldPayNotification" Codebehind="WorldPayNotification.ascx.cs" %>
<%@ Register Src="~/Controls/Default/Common/spacer.ascx" TagName="spacer" TagPrefix="ZNode" %>

<div>
    <h1>
        <asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtSecureCheckout"></asp:Localize></h1>
</div>
<div>
    <!-- Confirmation panel -->
    <asp:Panel ID="pnlConfirmation" runat="server" Visible="False" 
        meta:resourcekey="pnlConfirmationResource1">
        <div class="Content">
            <ZNode:spacer SpacerWidth="15" SpacerHeight="15" ID="Spacer3" runat="server" />
            <div class="Text"><asp:Label id='lblMsg' runat="server" 
                    meta:resourcekey="lblMsgResource1"></asp:Label></div>        
            <ZNode:spacer ID="Spacer2" runat="server" SpacerWidth="1" SpacerHeight="10" />
            <div class="BackLink"><a href="~/" id="HomeLink" runat="server"><<
                <asp:Localize ID="Localize1" runat="server" meta:resourceKey="txtReturntoHome"></asp:Localize></a></div>
            <ZNode:spacer ID="Spacer1" runat="server" SpacerWidth="1" SpacerHeight="10" />
        </div>
    </asp:Panel>
    
</div>