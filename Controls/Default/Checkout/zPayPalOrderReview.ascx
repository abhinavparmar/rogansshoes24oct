﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="zPayPalOrderReview.ascx.cs" Inherits="WebApp.zPayPalOrderReview" %>
<%@ Register Src="StepTracker.ascx" TagName="StepTracker" TagPrefix="uc6" %>
<%@ Register Src="zPayPalOrderSummary.ascx" TagName="Order" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/Default/Common/spacer.ascx" TagName="spacer" TagPrefix="uc1" %>
<div class="Checkout OrderPayament Securecheckout">
    <div class="PageTitle">
        Secure Checkout: Order Review
    </div>
    <div class="Steps">
        <uc6:StepTracker ID="uxStepTracker" runat="server" Step="2" />
    </div>
    <div class="Error" runat="server" id="errorMsg" visible="false" enableviewstate="false">
        <span class="uxMsg ErrorSection">
            <em>
                <asp:Label ID="lblCustomError" runat="server" EnableViewState="False"
                    ClientIDMode="Static"></asp:Label>
            </em></span>
    </div>
    <p class="Review">
        Please review your order and provide payment information to complete your purchase.
    </p>
    <div id="OrderSummaryContainer">
        <asp:Repeater ID="rptCart" runat="server" OnItemDataBound="RptCart_ItemDataBound">
            <ItemTemplate>
                <div>
                    <uc4:Order ID="uxOrder" runat="server" PortalID='<%# DataBinder.Eval(Container.DataItem, "Product.PortalID") %>' />
                </div>
                <div>
                    <uc1:spacer ID="Spacer1" EnableViewState="false" SpacerHeight="10" SpacerWidth="10"
                        runat="server"></uc1:spacer>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <div>
            <uc1:spacer ID="Spacer1" EnableViewState="false" SpacerHeight="10" SpacerWidth="10"
                runat="server"></uc1:spacer>
        </div>
    </div>
    <div id="AddressContainer">
        <asp:PlaceHolder runat="server" ID="ErrorPanel" Visible="false">
            <div class="Form">
                <div class="HeaderStyle">
                    <asp:Localize ID="Localize16" runat="server" meta:resourceKey="txtPaymentInformation"></asp:Localize>
                </div>
                <div class="clear">
                    <asp:Label CssClass="Error" ID="lblError" runat="server" EnableViewState="False"
                        meta:resourcekey="lblErrorResource1"></asp:Label>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:Panel ID="pnlAddress" CssClass="PNLAddress" runat="server" meta:resourcekey="pnlAddressResource1">
            <div class="Form">
                <%--  <div class="HeaderStyle">
                    <asp:Localize ID="Localize18" runat="server" meta:resourceKey="txtAddress"></asp:Localize>
                </div>--%>
                <div>
                    <table style="width: 100%;">
                        <tr>
                            <td class="BillingAddressContent">
                                <h3>
                                    <asp:Localize ID="Localize2" runat="server" meta:resourceKey="txtBillingAddress"></asp:Localize>:
                                </h3>
                                <div class="AddressDetails">
                                    <asp:Label ID="lblBillingAddress" runat="server"></asp:Label><br />
                                </div>
                            </td>
                            <td class="ShippingAddressContent">
                                <div>
                                    <h3>
                                        <asp:Localize ID="Localize20" runat="server" meta:resourceKey="txtShippingAddress"></asp:Localize>:
                                    </h3>
                                    <div class="AddressDetails">
                                        <asp:Label ID="lblShippingAddress" runat="server"></asp:Label>

                                    </div>
                                </div>

                            </td>
                        </tr>
                    </table>
                </div>
                <div class="Clear"></div>
            </div>
        </asp:Panel>
        <div style="clear: both;"></div>
        <asp:Panel ID="pnlPayment" runat="server" meta:resourcekey="pnlPaymentResource1">
            <div class="Form">
                <h3>
                    <asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtPaymentInformation"></asp:Localize>
                </h3>
                <div class="Row Clear"></div>
                <table style="width: 100%;">
                    <tr>
                        <td class="PaymentContent">
                            <div>
                                <asp:Panel ID="pnlPaymentType" runat="server">
                                    <div class="LeftContent">
                                        <b>
                                            <img id="imgPayPalAcceptence" src="/Themes/Default/Images/AcceptanceMark_60x38.gif" alt="Paypal AcceptanceMark" title="Paypal AcceptanceMark" />
                                            <span class="PaypalUserEmail">
                                                <asp:Literal ID="ltPaypalUserEmail" runat="server" Text=""></asp:Literal></span>
                                        </b>
                                    </div>
                                </asp:Panel>
                            </div>
                        </td>
                    </tr>
                </table>
                <%--<div class="Row Clear">
                </div>--%>
                <div class="GiftcardSpace">
                    <%-- <uc1:spacer ID="Spacer9" EnableViewState="false" SpacerHeight="2" SpacerWidth="5"
                        runat="server" />--%>
                    <div class="HeaderStyle">
                        <asp:Localize ID="Localize10" runat="server" meta:resourceKey="txtHint2"></asp:Localize>
                    </div>
                    <div>
                        <p>
                            <asp:Localize ID="locShippingInstruction" runat="server" meta:resourceKey="txtHint2"></asp:Localize>
                        </p>
                        <asp:TextBox Columns="45" Rows="3" ID="txtAdditionalInstructions" runat="server"
                            TextMode="MultiLine" meta:resourcekey="txtAdditionalInstructionsResource1"></asp:TextBox>
                    </div>
                </div>
            </div>

        </asp:Panel>

    </div>
    <div id="SubmitOrderContainer">
        <table class="ProceedtoNextButton">
            <tr>
                <td>
                    <div class="ProceedtoNextButton">
                        <%--   <asp:LinkButton ID="SubmitButton" runat="server" CssClass="Button" OnClientClick="javascript:showWait();"
                            meta:resourcekey="SubmitButtonResource1" OnClick="OnSubmitOrder">Submit Order</asp:LinkButton>--%>
                        <asp:ImageButton ID="ibtnSubmitOrder" runat="server" AlternateText="Submit Order" ToolTip="Submit Order" ImageUrl="~/Themes/Default/Images/btn_paynow_LG.gif"
                            OnClick="OnSubmitOrder" />
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div>
                        <uc1:spacer ID="Spacer2" EnableViewState="false" SpacerHeight="1" SpacerWidth="10"
                            runat="server"></uc1:spacer>
                    </div>
                </td>
            </tr>
        </table>
        <%--Zeon Custom UI: Start--%>
        <div id="progressBarDiv" style="display: none; background-color: gray; filter: alpha(opacity=50); opacity: 0.5;"></div>
        <%--Zeon Custom UI: End--%>
    </div>
</div>

