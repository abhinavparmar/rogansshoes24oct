﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.Payment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.Suppliers;
using ZNode.Libraries.ECommerce.Taxes;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using EnumPaymentType = ZNode.Libraries.ECommerce.Entities.PaymentType;
using Zeon.Libraries.Avatax;
using ZNode.Libraries.DataAccess.Custom;
using Zeon.Libraries.Utilities;
using ZNode.Libraries.Paypal;

namespace WebApp
{
    public partial class zPayPalOrderReview : System.Web.UI.UserControl
    {

        #region[Private Variable]

        private ZNodeCheckout _checkout = null;
        private ZNodeUserAccount _userAccount = null;
        private ZNodeShoppingCart _shoppingCart = null;

        #endregion

        #region[public property]

        public string PayPalUserEmailID { get; set; }

        #endregion


        #region[Page Event]
        /// <summary>
        ///  Page Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            if (HttpContext.Current.Session[ZNodeSessionKeyType.ShoppingCart.ToString()] != null)
            {
                this._userAccount = ZNodeUserAccount.CreateFromSession(ZNodeSessionKeyType.UserAccount) as ZNodeUserAccount;
                this._shoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
            }
        }

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["mode"] != null && Request["mode"].ToUpper().IndexOf("PAYPAL") == 0)
                {
                    if (!IsPostBack)
                    {
                        this.GetExpressCheckoutDetails();
                    }
                }
            }
            this.BindCartItems();
            this.BindAddressInfo();
        }



        #endregion

        #region[Methods]
        /// <summary>
        /// Bind Cart Item 
        /// </summary>
        private void BindCartItems()
        {
            System.Collections.Generic.List<int> list = new System.Collections.Generic.List<int>();
            List<ZNodeShoppingCartItem> cartItems = new List<ZNodeShoppingCartItem>();
            if (this._shoppingCart != null)
            {
                foreach (ZNodeShoppingCartItem cartItem in this._shoppingCart.ShoppingCartItems)
                {
                    int portalId = 0;
                    if (cartItem.Product.PortalID > 0)
                    {
                        portalId = cartItem.Product.PortalID;
                    }

                    if (!list.Contains(portalId))
                    {
                        list.Add(portalId);
                        cartItems.Add(cartItem);
                    }
                }
            }

            rptCart.DataSource = cartItems;
            rptCart.DataBind();

        }

        /// <summary>
        /// Bind User Billing and Shipping Infomration
        /// </summary>
        private void BindAddressInfo()
        {
            if (this._userAccount == null)
            {
                if (HttpContext.Current.Session[ZNodeSessionKeyType.UserAccount.ToString()] != null)
                {
                    this._userAccount = ZNodeUserAccount.CurrentAccount();
                }
            }

            if (this._userAccount != null && this._userAccount.BillingAddress != null && this._userAccount.ShippingAddress != null)
            {
                lblBillingAddress.Text = ZCommonHelper.GetAddressString(this._userAccount.BillingAddress);//Zeon Custom Code          
                lblShippingAddress.Text = ZCommonHelper.GetAddressString(this._userAccount.ShippingAddress);//Zeon Custom Code
            }

            if (this._userAccount != null && this._userAccount.ShippingAddress != null)
            {
                if (string.IsNullOrWhiteSpace(this._userAccount.ShippingAddress.FirstName) || string.IsNullOrWhiteSpace(this._userAccount.ShippingAddress.LastName) || string.IsNullOrWhiteSpace(this._userAccount.ShippingAddress.PostalCode) || string.IsNullOrWhiteSpace(this._userAccount.ShippingAddress.StateCode) || string.IsNullOrWhiteSpace(this._userAccount.ShippingAddress.CountryCode) || string.IsNullOrWhiteSpace(this._userAccount.ShippingAddress.City) || string.IsNullOrWhiteSpace(this._userAccount.ShippingAddress.Street))
                {
                    ZNodeUserAccount _UserAccountForShippingEmpty = ZNodeUserAccount.CreateFromSession(ZNodeSessionKeyType.UserAccount) as ZNodeUserAccount;
                    if (_UserAccountForShippingEmpty != null)
                    {
                        _UserAccountForShippingEmpty.SetAccountAddress();
                        lblShippingAddress.Text = ZCommonHelper.GetAddressString(_UserAccountForShippingEmpty.ShippingAddress);//Zeon Custom Code
                    }
                }
            }

            ltPaypalUserEmail.Text = PayPalUserEmailID;//this._userAccount.ShippingAddress..EmailID;
        }

        /// <summary>
        /// Returns an instance of the PaymentSetting  class.
        /// </summary>
        /// <param name="paymentTypeID">Payment Type Id</param>
        /// <returns>Returns the PaymentSetting</returns>
        private PaymentSetting GetByPaymentTypeId(int paymentTypeID)
        {
            PaymentSettingService _pmtServ = new PaymentSettingService();
            int? profileID = 0;
            int registerProfileID = ZNodeProfile.DefaultRegisteredProfileId;
            if (HttpContext.Current.Session[ZNodeSessionKeyType.UserAccount.ToString()] != null)
            {
                this._userAccount = ZNodeUserAccount.CurrentAccount();
            }

            if (this._userAccount != null)
            {
                profileID = this._userAccount.ProfileID; // Get loggedIn user profile
            }
            else
            {
                profileID = ZNodeProfile.DefaultRegisteredProfileId;
            }

            TList<PaymentSetting> list = _pmtServ.GetByPaymentTypeID(paymentTypeID);
            PaymentSetting listResult = list.Find((e) => { return ((e.ProfileID == profileID || e.ProfileID == registerProfileID) && e.ActiveInd == true); });
            PaymentSetting listAllProfileResult = null;
            //list.Filter = "ActiveInd = true AND ProfileID = " + profileID;

            if (listResult != null)
            {
                return listResult;
            }

            if (listResult == null)
            {
                //Get All Profiles payment setting
                //listFilter.Filter = "ActiveInd = true AND ProfileID = null";
                listAllProfileResult = list.Find((e) => { return (e.ProfileID == null && e.ActiveInd == true); });
            }

            if (listAllProfileResult != null)
            {
                return listAllProfileResult;
            }
            return null;
        }

        /// <summary>
        /// Get Paypal Shipping Address
        /// </summary>
        private void GetExpressCheckoutDetails()
        {

            if (_shoppingCart != null)
            {
                string token = Request["token"];
                PaymentSetting paymentSetting = GetPaymentSettingByPaymentTypeId((int)EnumPaymentType.PAYPAL);



                var paypal = new PaypalGateway(paymentSetting, "", "", false, 0);


                PaypalResponse paypalResponse = paypal.GetExpressCheckoutDetailsExtn(token);
                SetPayPalShippingAddressInformation(paypalResponse);
            }
        }

        /// <summary>
        /// Get GetPaymentSetting details based on PaymentTypeId
        /// </summary>
        /// <param name="paymentTypeID"></param>
        /// <returns></returns>
        public PaymentSetting GetPaymentSettingByPaymentTypeId(int paymentTypeID)
        {
            PaymentSettingService pmtSettingService = new PaymentSettingService();
            int profileID = 0;
            int registerProfileID = ZNodeProfile.DefaultRegisteredProfileId;
            if (_userAccount != null)
            {
                profileID = _userAccount.ProfileID; //Get loggedIn user profile
            }
            else
            {
                profileID = ZNodeProfile.DefaultRegisteredProfileId;
            }
            TList<PaymentSetting> list = pmtSettingService.GetByPaymentTypeID(paymentTypeID);
            PaymentSetting listResult = list.Find((e) => { return ((e.ProfileID == profileID || e.ProfileID == registerProfileID) && e.ActiveInd == true); });
            PaymentSetting listAllProfileResult = null;
            //list.Filter = "ActiveInd = true AND ProfileID = " + profileID;

            if (listResult != null)
            {
                return listResult;
            }

            if (listResult == null)
            {
                //Get All Profiles payment setting
                //listFilter.Filter = "ActiveInd = true AND ProfileID = null";
                listAllProfileResult = list.Find((e) => { return (e.ProfileID == null && e.ActiveInd == true); });
            }

            if (listAllProfileResult != null)
            {
                return listAllProfileResult;
            }
            return null;
        }

        /// <summary>
        /// Set Shipping Address Retrun From Paypal
        /// </summary>
        /// <param name="paypalResponse"></param>
        protected void SetPayPalShippingAddressInformation(ZNode.Libraries.Paypal.PaypalResponse paypalResponse)
        {
            string EmailID = string.Empty;
            this._userAccount.ShippingAddress = paypalResponse != null ?
                GetPayPalShippingAddress(paypalResponse, this._userAccount.ShippingAddress, out EmailID) : this._userAccount.ShippingAddress;
            this._userAccount.ShippingAddress.Name = "Default Shipping";
            PayPalUserEmailID = EmailID;
        }

        /// <summary>
        /// Get Paypal Type 
        /// </summary>
        /// <returns></returns>
        private ZNodePayment GetPaymentType()
        {
            ZNodePayment payPalPayment = new ZNodePayment();
            payPalPayment.PaymentName = "PAYPAL";
            return payPalPayment;
        }

        /// <summary>
        /// Retrun PaypalSHipping Address Based on Paypal Response
        /// </summary>
        /// <param name="paypalResponse"></param>
        /// <param name="emailID"></param>
        /// <returns></returns>
        private Address GetPayPalShippingAddress(ZNode.Libraries.Paypal.PaypalResponse paypalResponse, Address shippingAddress, out string emailID)
        {
            Address address = new Address();
            emailID = string.Empty;
            if (paypalResponse != null && paypalResponse.ShippingAddress != null)
            {
                //**Assign Shipping Address
                address.FirstName = paypalResponse.ShippingAddress.FirstName;
                address.LastName = paypalResponse.ShippingAddress.LastName;
                address.MiddleName = paypalResponse.ShippingAddress.MiddleName;
                address.Street = paypalResponse.ShippingAddress.Street1;
                address.Street1 = paypalResponse.ShippingAddress.Street2;
                address.City = paypalResponse.ShippingAddress.City;
                address.StateCode = paypalResponse.ShippingAddress.StateCode;
                address.PostalCode = paypalResponse.ShippingAddress.PostalCode;
                address.PhoneNumber = !string.IsNullOrEmpty(paypalResponse.ShippingAddress.PhoneNumber) ? paypalResponse.ShippingAddress.PhoneNumber : shippingAddress.PhoneNumber;
                address.CompanyName = paypalResponse.ShippingAddress.CompanyName;
                address.CountryCode = paypalResponse.ShippingAddress.CountryCode;
                emailID = paypalResponse.ShippingAddress.EmailId;
            }
            return address;
        }

        /// <summary>
        /// Check is Order Placed 
        /// Before Submitting the Order Check is Order Submited,to prevent placed duplicate order in ie 11
        /// </summary>
        /// <returns></returns>
        private bool CheckIsOrderSubmittedSuccessfully()
        {
            bool isPlacedSuccessfully = false;
            try
            {
                ZNodeOrderFulfillment _Order = null;
                List<ZNodeOrderFulfillment> _Orders = new List<ZNodeOrderFulfillment>();
                if (Session["OrderDetail"] is List<ZNodeOrderFulfillment>)
                {
                    _Orders = Session["OrderDetail"] as List<ZNodeOrderFulfillment>;
                }
                else if (Session["OrderDetail"] is ZNodeOrderFulfillment)
                {
                    _Order = Session["OrderDetail"] as ZNodeOrderFulfillment;
                }

                if (_Order == null && _Orders.Count > 0)
                {
                    _Order = _Orders[0];
                }

                if (_Order != null && _Order.OrderID > 0)
                {
                    isPlacedSuccessfully = true;
                }
            }
            catch (Exception ex)
            {
                isPlacedSuccessfully = false;
            }
            return isPlacedSuccessfully;
        }
        #endregion

        #region[Genral Events]

        /// <summary>
        /// Submit Order
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnSubmitOrder(object sender, EventArgs e)
        {
            this.CreatePayPalExpressCheckoutOrder();
        }

        #endregion

        #region[Repeater Events]

        /// <summary>
        /// Repeater Control Item Data bound event.
        /// </summary>
        /// <param name="sender">repeater object</param>
        /// <param name="e">event args</param>
        protected void RptCart_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ZNodeShoppingCartItem item = (ZNodeShoppingCartItem)e.Item.DataItem;
                zPayPalOrderSummary ShopingCart = (zPayPalOrderSummary)e.Item.FindControl("uxOrder");
                ShopingCart.PortalID = item.Product.PortalID;
                ShopingCart.BindGrid();
            }
        }

        #endregion

        #region[Submit Order]
        /// <summary>
        /// Process Submit Order 
        /// </summary>
        private void CreatePayPalExpressCheckoutOrder()
        {
            if (CheckIsOrderSubmittedSuccessfully())
            {
                Response.Redirect("~/orderreceipt.aspx");
            }
            else
            {
                string redirectURL = string.Empty;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging log = new ZNode.Libraries.ECommerce.Utilities.ZNodeLogging();
                ZNodeOrderFulfillment order = new ZNodeOrderFulfillment();
                if (HttpContext.Current.Session[ZNodeSessionKeyType.ShoppingCart.ToString()] != null)
                {
                    this._userAccount = ZNodeUserAccount.CreateFromSession(ZNodeSessionKeyType.UserAccount) as ZNodeUserAccount;
                    this._shoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
                }

                if (this._shoppingCart == null)
                {
                    Response.Redirect("~/");
                }
                try
                {
                    log.LogActivityTimerStart();

                    // Get payment setting for Paypal Express Checkout payment type.
                    PaymentSetting _paymentSetting = this.GetByPaymentTypeId(2);
                    this._checkout = new ZNodeCheckout();
                    this._checkout.PaymentSettingID = _paymentSetting.PaymentSettingID;
                    this._checkout.AdditionalInstructions = txtAdditionalInstructions.Text;
                    order = (ZNodeOrderFulfillment)this._checkout.SubmitOrder();
                }
                catch (ZNode.Libraries.ECommerce.Entities.ZNodePaymentException ex)
                {
                    log.LogActivityTimerEnd(5003, Request.UserHostAddress.ToString(), null, null, null, "Paypal:" + ex.Message + " " + ex.StackTrace);

                    // Display payment error message
                    return;
                }
                catch (Exception exc)
                {
                    //log.LogActivityTimerEnd(5003, null);//Znode old code
                    log.LogActivityTimerEnd(5003, Request.UserHostAddress.ToString(), null, null, null, exc.StackTrace);

                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Paypal: General Checkout Exception - UnableToProcessRequest : " + exc.Message + " " + exc.StackTrace);

                    // Display error page
                    return;
                }
                // Post order submission
                if (this._checkout.IsSuccess)
                {
                    log.LogActivityTimerEnd(5002, order.OrderID.ToString());

                    // Worldpay
                    if (!string.IsNullOrEmpty(this._checkout.ECRedirectURL))
                    {
                        Response.Redirect(this._checkout.ECRedirectURL);
                    }

                    // Make an entry in tracking event for placing an Order 
                    ZNodeTracking tracker = new ZNodeTracking();
                    if (tracker.AffiliateId.Length > 0)
                    {
                        tracker.AccountID = order.AccountID;
                        tracker.OrderID = order.OrderID;
                        tracker.LogTrackingEvent("Placed an Order");
                    }

                    this.PostSubmitOrder(order);

                    List<ZNodeOrderFulfillment> orders = new List<ZNodeOrderFulfillment>();
                    orders.Add(order);
                    Session.Add("OrderDetail", orders);

                    // Create session for Order and Shopping cart               
                    ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

                    // Cookie based persistent cart.
                    ZNodeSavedCart savedCart = new ZNodeSavedCart();
                    savedCart.RemoveSavedCart();

                    shoppingCart.AddShoppingCartToSession(order.PortalId);

                    Response.Redirect("~/orderreceipt.aspx");
                }
                else
                {
                    log.LogActivityTimerEnd(5001, null, null, null, null, this._checkout.PaymentResponseText);
                    lblCustomError.Text = this.GetLocalResourceObject("CustomErrorMessage").ToString();
                    errorMsg.Visible = true;
                    return;
                }

                Response.Redirect(redirectURL);
            }
        }


        #region Post Submit Order - Set Digital Asset
        /// <summary>
        /// Post Order Submission Method
        /// </summary>
        /// <param name="order">Order information</param>
        private void PostSubmitOrder(ZNodeOrderFulfillment order)
        {
            ZNodeSupplierOption supplierOption = new ZNodeSupplierOption();
            supplierOption.SubmitOrder(order, this._checkout.ShoppingCart);

            // Set Digital Asset
            int counter = 0;
            DigitalAssetService digitalAssetService = new DigitalAssetService();

            // Loop through the Order Line Items
            foreach (OrderLineItem orderLineItem in order.OrderLineItems)
            {
                ZNodeShoppingCartItem shoppingCartItem = this._checkout.ShoppingCart.ShoppingCartItems[counter++];

                // Set quantity ordered
                int qty = shoppingCartItem.Quantity;

                // Set product id
                int productId = shoppingCartItem.Product.ProductID;
                ZNodeGenericCollection<ZNodeDigitalAsset> assignedDigitalAssets = new ZNodeGenericCollection<ZNodeDigitalAsset>();

                // Get Digital assets for productid and quantity
                assignedDigitalAssets = ZNodeDigitalAssetList.CreateByProductIdAndQuantity(productId, qty).DigitalAssetCollection;

                // Loop through the digital asset retrieved for this product
                foreach (ZNodeDigitalAsset digitalAsset in assignedDigitalAssets)
                {
                    DigitalAsset entity = digitalAssetService.GetByDigitalAssetID(digitalAsset.DigitalAssetID);
                    entity.OrderLineItemID = orderLineItem.OrderLineItemID; // Set OrderLineitemId property
                    digitalAssetService.Update(entity); // Update digital asset to the database
                }

                // Set retrieved digital asset collection to shopping product object
                // If product has digital assets, it will display it on the receipt page along with the product name
                shoppingCartItem.Product.ZNodeDigitalAssetCollection = assignedDigitalAssets;
            }
        }
        #endregion

        #endregion

    }
}