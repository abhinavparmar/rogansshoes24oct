﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="WebApp.Controls_Default_Checkout_PaymentThankyou" Codebehind="PaymentThankyou.ascx.cs" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="spacer" TagPrefix="ZNode" %>
    
<div>
    <!-- Confirmation panel -->
    <asp:Panel ID="pnlConfirmation" runat="server" Visible="False" meta:resourcekey="pnlConfirmationResource1">
        <div id="Notification">
            <znode:spacer spacerwidth="15" spacerheight="15" id="Spacer3" runat="server" />
            <div class="Text">
                <asp:Label ID='lblMsg' runat="server" meta:resourcekey="lblMsgResource1"></asp:Label></div>
            <znode:spacer id="Spacer2" runat="server" spacerwidth="1" spacerheight="10" />
            <div class="BackLink">
                <a href="~/" id="HomeLink" runat="server"><<
                    <asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtReturntoHome"></asp:Localize></a></div>
        </div>
    </asp:Panel>
</div>
