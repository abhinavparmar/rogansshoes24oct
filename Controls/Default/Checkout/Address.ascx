<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Checkout_Address"
    CodeBehind="Address.ascx.cs" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="Spacer" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Checkout/StepTracker.ascx" TagName="StepTracker" TagPrefix="uc6" %>
<asp:UpdatePanel runat="server" ID="upCheckoutAddress">
    <ContentTemplate>
        <asp:Panel ID="pnlSecureCheckOut" runat="server" DefaultButton="lbProceedToNext" ClientIDMode="Static">
            <div class="Checkout">
                <div class="PageTitle">
                    <asp:Localize ID="Localize17" runat="server" meta:resourceKey="txtSecureCheckout" EnableViewState="false"></asp:Localize>
                </div>
                <div class="Overview">
                    <asp:Localize ID="Localize27" runat="server" meta:resourceKey="txtOverView">
                    </asp:Localize>
                </div>
                <div class="Steps">
                    <uc6:StepTracker ID="uxStepTracker" runat="server" />
                </div>
                <div class="Error">
                    <asp:Localize ID="lblError" runat="server" EnableViewState="false" ClientIDMode="Static"> </asp:Localize>
                </div>
                <div class="Form Row Clear">
                    <div class="LeftContent BillingAddress CommonLableNone">
                        <div class="FormTitle">
                            <asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtBillingAddress" EnableViewState="false"></asp:Localize>
                        </div>
                        <div class="Row Clear" runat="server" id="divBillingAddressName">
                            <div class="FieldStyle">
                                <label>
                                    <asp:Localize ID="Localize25" runat="server" Text="<%$ Resources:CommonCaption, AddressName %>" EnableViewState="false"></asp:Localize></label>
                            </div>
                            <div class="Input-box">
                                <asp:DropDownList ID="ddlBillingAddressName" runat="server" AutoPostBack="True" ToolTip="<%$ Resources:CommonCaption, AddressName %>"
                                    OnSelectedIndexChanged="DdlAddressName_SelectedIndexChanged"  ClientIDMode="Static">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ErrorMessage="<%$ Resources:CommonCaption, FirstNameRequired %>" SetFocusOnError="true"
                                    ControlToValidate="ddlBillingAddressName" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="Row Clear">
                            <div class="FieldStyle">
                                <label class="Req">
                                    <asp:Localize ID="Localize1" runat="server" meta:resourceKey="txtFirstName"></asp:Localize></label>
                            </div>
                            <div class="Input-box">
                                <asp:TextBox ID="txtBillingFirstName" placeholder="First Name *" runat="server" Width="130px" Columns="30" MaxLength="100" EnableViewState="false" 
                                    meta:resourcekey="FirstName" ClientIDMode="Static"></asp:TextBox><div>
                                        <asp:RequiredFieldValidator ID="req1" ErrorMessage="<%$ Resources:CommonCaption, FirstNameRequired %>" SetFocusOnError="true"
                                            ControlToValidate="txtBillingFirstName" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                    </div>
                            </div>
                        </div>
                        <div class="Row Clear">
                            <div class="FieldStyle">
                                <label class="Req">
                                    <asp:Localize ID="Localize2" runat="server" meta:resourceKey="txtLastName"></asp:Localize></label>
                            </div>
                            <div class="Input-box">
                                <asp:TextBox ID="txtBillingLastName" placeholder="Last Name *" runat="server" Width="130px" Columns="30" MaxLength="100" EnableViewState="false" 
                                    meta:resourcekey="LastName" ClientIDMode="Static"></asp:TextBox><div>
                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" ErrorMessage="<%$ Resources:CommonCaption, LastNameRequired  %>" SetFocusOnError="true"
                                            ControlToValidate="txtBillingLastName" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                    </div>
                            </div>
                        </div>
                        <div class="Row Clear">
                            <div class="FieldStyle">
                                <label>
                                    <asp:Localize ID="Localize3" runat="server" meta:resourceKey="txtCompanyName" EnableViewState="true"></asp:Localize></label>
                            </div>
                            <div class="Input-box">
                                <asp:TextBox ID="txtBillingCompanyName" placeholder="Company Name" runat="server" Width="130px" Columns="30" EnableViewState="true" 
                                    MaxLength="100" meta:resourcekey="CompanyName" ClientIDMode="Static"></asp:TextBox>
                            </div>
                        </div>
                        <div class="Row Clear">
                            <div class="FieldStyle">
                                <label class="Req">
                                    <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:CommonCaption, Street1 %>"></asp:Localize></label>
                            </div>
                            <div class="Input-box">
                                <asp:TextBox ID="txtBillingStreet1" placeholder="Street 1 *" runat="server" Width="130px" Columns="30" MaxLength="100"
                                    meta:resourcekey="BillingStreet1" EnableViewState="true"  ClientIDMode="Static"></asp:TextBox><div>
                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" ErrorMessage="<%$ Resources:CommonCaption, Street1Required %>" SetFocusOnError="true"
                                            ControlToValidate="txtBillingStreet1" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                    </div>
                            </div>
                        </div>
                        <div class="Row Clear">
                            <div class="FieldStyle">
                                <label>
                                    <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:CommonCaption, Street2 %>" EnableViewState="false"></asp:Localize></label>
                            </div>
                            <div class="Input-box">
                                <asp:TextBox ID="txtBillingStreet2" placeholder="Street 2" runat="server" Width="130px" Columns="30" MaxLength="100"
                                    meta:resourcekey="BillingStreet2"  EnableViewState="false" ClientIDMode="Static"> </asp:TextBox>
                            </div>
                        </div>
                        <div class="Row Clear">
                            <div class="FieldStyle">
                                <label class="Req">
                                    <asp:Localize ID="Localize24" runat="server" meta:resourceKey="txtCity" EnableViewState="false"></asp:Localize></label>
                            </div>
                            <div class="Input-box">
                                <asp:TextBox ID="txtBillingCity"  placeholder="City *" runat="server" Width="130px" Columns="30" MaxLength="100"  EnableViewState="false"
                                    meta:resourcekey="BillingCity" ClientIDMode="Static"></asp:TextBox><div>
                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" ErrorMessage="<%$ Resources:CommonCaption, CityRequired %>" SetFocusOnError="true"
                                            ControlToValidate="txtBillingCity" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                    </div>
                            </div>
                        </div>
                        <div class="Row Clear">
                            <div class="FieldStyle">
                                <label class="Req">
                                    <asp:Localize ID="Localize11" runat="server" meta:resourceKey="txtCountry" EnableViewState="false"></asp:Localize></label>
                            </div>
                            <div class="Input-box CountryCode">
                                <asp:DropDownList ID="lstBillingCountryCode" runat="server"  meta:resourceKey="Country"
                                    AutoPostBack="true"
                                    OnTextChanged="lstBillingCountryCode_TextChanged" OnSelectedIndexChanged="lstCountry_OnSelectedIndexChanged" ClientIDMode="Static">
                                </asp:DropDownList>
                                <div>
                                    <asp:CustomValidator ID="valCountry" ControlToValidate="lstBillingCountryCode" OnServerValidate="validateCountry" SetFocusOnError="true"
                                        Display="Dynamic" CssClass="Error" ErrorMessage="<%$ Resources:CommonCaption, CountryCodeRequired %>"
                                        runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="Row Clear">
                            <div class="FieldStyle">
                                <label class="Req">
                                    <asp:Localize ID="Localize9" runat="server" meta:resourceKey="txtState" EnableViewState="false"></asp:Localize></label>
                            </div>
                            <div class="Input-box BillingState">
                                <asp:TextBox ID="txtBillingState" placeholder="State *" runat="server" Width="30px" Columns="10" EnableViewState="false" TabIndex="9"
                                    meta:resourcekey="BillingState" ClientIDMode="Static"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="revBillingStateCode" ErrorMessage="<%$ Resources:CommonCaption, StateRequired %>" SetFocusOnError="true"
                                    ControlToValidate="txtBillingState" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                <asp:DropDownList ID="lstBillingState" runat="server"  ClientIDMode="Static"></asp:DropDownList><div>
                                    <asp:RequiredFieldValidator ID="revBillingState" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, SelectStateRequired %>" ControlToValidate="lstBillingState" InitialValue="<%$ Resources:CommonCaption, DefaultSelectedState %>" CssClass="Error"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <div class="Row Clear">
                            <div class="FieldStyle">
                                <label class="Req">
                                    <asp:Localize ID="Localize10" runat="server" meta:resourceKey="txtPostalCode" EnableViewState="true"></asp:Localize></label>
                            </div>
                            <div class="Input-box">
                                <asp:TextBox ID="txtBillingPostalCode" placeholder="Postal Code *" runat="server" Width="130px" Columns="30" EnableTheming="false"
                                    MaxLength="20" meta:resourcekey="BillingPostalCode" ClientIDMode="Static"></asp:TextBox><div>
                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator12" ErrorMessage="<%$ Resources:CommonCaption, PostalCodeRequired  %>" SetFocusOnError="true"
                                            ControlToValidate="txtBillingPostalCode" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="rgxBillingPostalCodeValidation" runat="server" ControlToValidate="txtBillingPostalCode" Display="Dynamic"
                                            CssClass="Error" SetFocusOnError="true" ErrorMessage="Please enter valid zip code" ValidationExpression="^\d{5}(-\d{4})?$"></asp:RegularExpressionValidator>
                                    </div>
                            </div>
                        </div>
                        <%--  Zeon Custom Code :PO Box Implementation--%>
                        <div class="Row Clear">
                            <div class="FieldStyle None">
                                <label>&nbsp;</label>
                            </div>
                            <div class="Input-box ChkBillingPOBox checkbox-container">
                                <asp:CheckBox ID="chkBillingPOBox" runat="server" meta:resourceKey="chkBillingPOBox" ClientIDMode="Static" />
                            </div>
                            <asp:HiddenField ID="hdnBillingExtnID" runat="server" ClientIDMode="Static" />
                        </div>
                        <%--  Zeon Custom Code :PO Box Implementation--%>
                        <div class="Row Clear">
                            <div class="FieldStyle">
                                <label class="Req">
                                    <asp:Localize ID="Localize4" runat="server" meta:resourceKey="txtPhoneNumber" EnableViewState="false" ClientIDMode="Static"></asp:Localize></label>
                            </div>
                            <div class="Input-box">
                                <asp:TextBox ID="txtBillingPhoneNumber"  placeholder="Phone Number *" runat="server" Width="130px" Columns="30" EnableViewState="false" 
                                    MaxLength="100" meta:resourcekey="BillingPhoneNumber" ClientIDMode="Static"></asp:TextBox><div>
                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" ErrorMessage="<%$ Resources:CommonCaption, PhoneNoRequired  %>" SetFocusOnError="true"
                                            ControlToValidate="txtBillingPhoneNumber" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                    </div>
                            </div>
                        </div>
                        <div class="Row Clear" runat="server" id="divEmailAddress">
                            <div class="FieldStyle">
                                <label class="Req">
                                    <asp:Localize ID="Localize6" runat="server" meta:resourceKey="Email" EnableViewState="false"></asp:Localize>:</label>
                            </div>
                            <div class="Input-box EmailAddress">
                                <asp:TextBox ID="txtEmailAddress" placeholder="Email Address *" meta:resourceKey="EmailAddress" runat="server" Width="130px" Columns="30" MaxLength="100"  ClientIDMode="Static"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" SetFocusOnError="true"
                                        ControlToValidate="txtEmailAddress" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:CommonCaption, EmailAddressRequired%>'>
                                    </asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmailAddress" SetFocusOnError="true"
                                        ErrorMessage='<%$ Resources:CommonCaption, EmailValidErrorMessage %>' Display="Dynamic"
                                        ValidationExpression='<%$ Resources:CommonCaption, EmailValidationExpression%>'
                                        CssClass="Error">
                                    </asp:RegularExpressionValidator>
                                </div>
                            </div>
                        </div>
                        <div class="Row Clear">
                            <div class="FieldStyle None">
                                <label>&nbsp;</label>
                            </div>
                            <div class="Input-box SameBilling checkbox-container">
                                <asp:CheckBox ID="chkSameAsBilling" runat="server" Font-Bold="True" Text="<%$ Resources:CommonCaption, AddressAreSame %>" ToolTip="<%$ Resources:CommonCaption, AddressAreSame %>" 
                                    OnCheckedChanged="ChkSameAsBilling_CheckedChanged" AutoPostBack="True" ClientIDMode="Static" />
                            </div>
                        </div>
                    </div>
                    <%-- <div class="LeftContent" style="width: 10%">
                        &nbsp;
                    </div>--%>
                    <div class="LeftContent ShippingAddress CommonLableNone">
                        <asp:Panel ID="pnlShipping" runat="server" meta:resourcekey="pnlShippingResource1" ClientIDMode="Static">
                            <div class="FormTitle">
                                <asp:Localize ID="Localize12" runat="server" meta:resourceKey="txtShippingAddress"></asp:Localize>
                            </div>
                            <br />
                            <div class="Form">
                                <div class="Row Clear" runat="server" id="divShippingAddressName">
                                    <div class="FieldStyle">
                                        <label>
                                            <asp:Localize ID="Localize26" runat="server" Text="<%$ Resources:CommonCaption, AddressName %>"></asp:Localize></label>
                                    </div>
                                    <div class="Input-box ShippingAddressName">
                                        <asp:DropDownList ID="ddlShippingAddressName" runat="server" AutoPostBack="True"  ToolTip="<%$ Resources:CommonCaption, AddressName %>"
                                            OnSelectedIndexChanged="DdlAddressName_SelectedIndexChanged" ClientIDMode="Static">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvShippingAddress" ErrorMessage="<%$ Resources:CommonCaption, FirstNameRequired %>" SetFocusOnError="true"
                                            ControlToValidate="ddlShippingAddressName" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="Row Clear">
                                    <div class="FieldStyle">
                                        <label class="Req">
                                            <asp:Localize ID="Localize13" runat="server" meta:resourceKey="txtFirstName"></asp:Localize></label>
                                    </div>
                                    <div class="Input-box">
                                        <asp:TextBox ID="txtShippingFirstName" placeholder="First Name *" runat="server" Width="130px" Columns="30"
                                            MaxLength="100" meta:resourcekey="FirstName" ClientIDMode="Static"></asp:TextBox><div>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" ErrorMessage="<%$ Resources:CommonCaption, FirstNameRequired  %>" SetFocusOnError="true"
                                                    ControlToValidate="txtShippingFirstName" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                            </div>
                                    </div>
                                </div>
                                <div class="Row Clear">
                                    <div class="FieldStyle">
                                        <label class="Req">
                                            <asp:Localize ID="Localize14" runat="server" meta:resourceKey="txtLastName" EnableViewState="false"></asp:Localize></label>
                                    </div>
                                    <div class="Input-box">
                                        <asp:TextBox ID="txtShippingLastName" placeholder="Last Name *" runat="server" Width="130px" Columns="30" MaxLength="100" EnableViewState="false" 
                                            meta:resourcekey="LastName" ClientIDMode="Static"></asp:TextBox><div>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator7" ErrorMessage="<%$ Resources:CommonCaption, LastNameRequired  %>" SetFocusOnError="true"
                                                    ControlToValidate="txtShippingLastName" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                            </div>
                                    </div>
                                </div>
                                <div class="Row Clear">
                                    <div class="FieldStyle">
                                        <label>
                                            <asp:Localize ID="Localize15" runat="server" meta:resourceKey="txtCompanyName" EnableViewState="false"></asp:Localize></label>
                                    </div>
                                    <div class="Input-box">
                                        <asp:TextBox ID="txtShippingCompanyName" placeholder="Company Name" runat="server" Width="130px" Columns="30" EnableViewState="false"
                                            MaxLength="100" meta:resourcekey="CompanyName" ClientIDMode="Static"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="Row Clear">
                                    <div class="FieldStyle">
                                        <label class="Req">
                                            <asp:Localize ID="Localize18" runat="server" Text="<%$ Resources:CommonCaption, Street1 %>" EnableViewState="false"></asp:Localize></label>
                                    </div>
                                    <div class="Input-box">
                                        <asp:TextBox ID="txtShippingStreet1" placeholder="Street 1 *" runat="server" Width="130px" Columns="30" MaxLength="100" EnableViewState="false"  ToolTip="<%$ Resources:CommonCaption, Street1 %>" ClientIDMode="Static"></asp:TextBox><div>
                                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator9" ErrorMessage="<%$ Resources:CommonCaption, Street1Required %>" SetFocusOnError="true"
                                                ControlToValidate="txtShippingStreet1" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="Row Clear">
                                    <div class="FieldStyle">
                                        <label>
                                            <asp:Localize ID="Localize19" runat="server" Text="<%$ Resources:CommonCaption, Street2 %>" EnableViewState="false"></asp:Localize></label>
                                    </div>
                                    <div class="Input-box">
                                        <asp:TextBox ID="txtShippingStreet2" placeholder="Street 2" runat="server" Width="130px" Columns="30" MaxLength="100"
                                            ToolTip="<%$ Resources:CommonCaption, Street2 %>" EnableViewState="false"  ClientIDMode="Static"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="Row Clear">
                                    <div class="FieldStyle">
                                        <label class="Req">
                                            <asp:Localize ID="Localize20" runat="server" meta:resourceKey="txtCity" EnableViewState="false"></asp:Localize></label>
                                    </div>
                                    <div class="Input-box">
                                        <asp:TextBox ID="txtShippingCity" runat="server" Width="130px" Columns="30" MaxLength="100"
                                            meta:resourcekey="ShippingCity" EnableTheming="false" ClientIDMode="Static" placeholder="City *"></asp:TextBox><div>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator10" ErrorMessage="<%$ Resources:CommonCaption, CityRequired %>" SetFocusOnError="true"
                                                    ControlToValidate="txtShippingCity" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                            </div>
                                    </div>
                                </div>
                                <div class="Row Clear">
                                    <div class="FieldStyle">
                                        <label class="Req">
                                            <asp:Localize ID="Localize23" runat="server" meta:resourceKey="txtCountry" EnableViewState="false"></asp:Localize></label>
                                    </div>
                                    <div class="Input-box CountryCode">
                                        <asp:DropDownList ID="lstShippingCountryCode" runat="server" meta:resourcekey="Country" AutoPostBack="true" OnSelectedIndexChanged="lstCountry_OnSelectedIndexChanged" ClientIDMode="Static">
                                        </asp:DropDownList>
                                        <div>
                                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator15" ErrorMessage="<%$ Resources:CommonCaption, CountryCodeRequired %>" SetFocusOnError="true"
                                                ControlToValidate="lstShippingCountryCode" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="Row Clear">
                                    <div class="FieldStyle">
                                        <label class="Req">
                                            <asp:Localize ID="Localize21" runat="server" meta:resourceKey="txtState" EnableViewState="false"></asp:Localize></label>
                                    </div>
                                    <div class="Input-box BillingState">
                                        <asp:TextBox ID="txtShippingState" placeholder="State" runat="server" Width="30px" Columns="10" 
                                            meta:resourcekey="ShippingState" EnableViewState="false"  ClientIDMode="Static"></asp:TextBox>
                                        <asp:DropDownList ID="lstShippingState" runat="server"  ClientIDMode="Static"></asp:DropDownList>
                                        <div>
                                            <asp:RequiredFieldValidator ID="revShippingStateCode" ErrorMessage="<%$ Resources:CommonCaption, StateRequired %>" SetFocusOnError="true"
                                                ControlToValidate="txtShippingState" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                            <asp:RequiredFieldValidator ID="revShippingState" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, SelectStateRequired %>" ControlToValidate="lstShippingState" InitialValue="<%$ Resources:CommonCaption, DefaultSelectedState %>" CssClass="Error"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>

                                <div class="Row Clear">
                                    <div class="FieldStyle">
                                        <label class="Req">
                                            <asp:Localize ID="Localize22" runat="server" meta:resourceKey="txtPostalCode" EnableViewState="true"></asp:Localize></label>
                                    </div>
                                    <div class="Input-box">
                                        <asp:TextBox ID="txtShippingPostalCode" placeholder="Postal Code *" runat="server" Width="130px" Columns="30" EnableViewState="false" 
                                            MaxLength="20" meta:resourcekey="ShippingPostalCode" ClientIDMode="Static"></asp:TextBox><div>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator14" ErrorMessage="<%$ Resources:CommonCaption, PostalCodeRequired  %>"
                                                    SetFocusOnError="true" ControlToValidate="txtShippingPostalCode" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                                  <asp:RegularExpressionValidator ID="rgxShippingPostalCode" runat="server" ControlToValidate="txtShippingPostalCode" Display="Dynamic"
                                            CssClass="Error" SetFocusOnError="true" ErrorMessage="Please enter valid zip code" ValidationExpression="^\d{5}(-\d{4})?$"></asp:RegularExpressionValidator>
                                            </div>
                                    </div>
                                </div>
                                <%--  Zeon Custom Code :PO Box Implementation--%>
                                <div class="Row Clear">
                                    <div class="FieldStyle None">
                                        <label>&nbsp;</label>
                                    </div>
                                    <div class="Input-box ChkBillingPOBox checkbox-container">
                                        <asp:CheckBox ID="chkShippingPOBox" runat="server" meta:resourceKey="chkBillingPOBox" ClientIDMode="Static" />
                                    </div>
                                    <asp:HiddenField ID="hdnShippingExtnID" runat="server" ClientIDMode="Static" />
                                </div>
                                <%--  Zeon Custom Code :PO Box Implementation--%>
                                <div class="Row Clear">
                                    <div class="FieldStyle">
                                        <label class="Req">
                                            <asp:Localize ID="Localize16" runat="server" meta:resourceKey="txtPhoneNumber" EnableViewState="false"></asp:Localize></label>
                                    </div>
                                    <div class="Input-box">
                                        <asp:TextBox ID="txtShippingPhoneNumber" placeholder="Phone Number *" runat="server" Width="130px" Columns="30"
                                            MaxLength="100" meta:resourcekey="ShippingPhoneNumber"  EnableViewState="false" ClientIDMode="Static"></asp:TextBox><div>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator8" ErrorMessage="<%$ Resources:CommonCaption, PhoneNoRequired  %>" SetFocusOnError="true"
                                                    ControlToValidate="txtShippingPhoneNumber" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
                <%--<ZNode:Spacer ID="Spacer1" EnableViewState="false" SpacerHeight="5" SpacerWidth="5" runat="server" ClientIDMode="Static"></ZNode:Spacer>--%>
                <div class="ProceedtoNextButton">
                    <asp:LinkButton ID="lbProceedToNext" CssClass="Button Large" runat="server" EnableViewState="false"
                        OnClick="LbProceedToNext_Click" ClientIDMode="Static" ToolTip="Proceed to Order & Payment">Proceed to Order & Payment</asp:LinkButton>
                </div>
            </div> 
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
<div>
    <asp:Literal ID="TriggerMailCartImagesHolder" runat="server" ClientIDMode="Static"></asp:Literal>
</div>
