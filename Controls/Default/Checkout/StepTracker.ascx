<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Checkout_StepTracker" Codebehind="StepTracker.ascx.cs" %>
<a class='<% =Step1Css%>'>
    <span><span><i class="fa fa-map-marker"></i> <asp:Localize ID="locAddress" runat="server" meta:resourceKey="txtYourAddress"></asp:Localize></span></span>
    </a>
<a class='<% =Step2Css%>'>
     <span><span><i class="fa fa-usd"></i><asp:Localize ID="locOrderPayment" runat="server" meta:resourceKey="txtOrderandPayment"></asp:Localize></span></span>
</a>
<a class='<% =Step3Css%>'>
     <span><span><i class="fa fa-check-circle"></i> <span class="Text"><asp:Localize ID="locOrderConfirmation" runat="server" meta:resourceKey="locOrderConfirmation"></asp:Localize></span></span></span>
</a>

