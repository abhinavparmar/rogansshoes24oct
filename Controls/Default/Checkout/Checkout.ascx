<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Checkout_Checkout"
    CodeBehind="Checkout.ascx.cs" %>
<%@ Register Src="StepTracker.ascx" TagName="StepTracker" TagPrefix="uc6" %>
<%@ Register Src="Order.ascx" TagName="Order" TagPrefix="uc4" %>
<%@ Register Src="Payment.ascx" TagName="Payment" TagPrefix="uc5" %>
<%@ Register Src="Address.ascx" TagName="Address" TagPrefix="uc3" %>
<%@ Register Src="OrderReceipt.ascx" TagName="OrderReceipt" TagPrefix="Order" %>
<%@ Register Src="~/Controls/Default/Common/spacer.ascx" TagName="spacer" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="Znode" %>

<asp:Panel runat="server" ID="pnlCheckout" DefaultButton="SubmitButton">
    <div class="Checkout OrderPayament">
        <%--Disable the submit button and then call the server side button code.--%>
        <div class="PageTitle">
            <asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtSecureCheckout"></asp:Localize>
        </div>
        <div class="Steps">
            <uc6:StepTracker ID="uxStepTracker" runat="server" ClientIDMode="Static" />
        </div>
        <div class="Error" runat="server" id="errorMsg" visible="false" enableviewstate="false"><span class="uxMsg ErrorSection" >
           <em> <asp:Label ID="lblError" runat="server" EnableViewState="False"
                meta:resourcekey="lblErrorResource1" ClientIDMode="Static"></asp:Label> </em></span>
       </div>
        <p class="Review">
            <asp:Localize ID="Localize2" runat="server" meta:resourceKey="txtReview">
            </asp:Localize>
        </p>
        <asp:Repeater ID="rptCart" runat="server" OnItemDataBound="RptCart_ItemDataBound" ClientIDMode="Static">
            <ItemTemplate>
                <div>
                    <uc4:Order ID="uxOrder" runat="server" PortalID='<%# DataBinder.Eval(Container.DataItem, "Product.PortalID") %>' ClientIDMode="Static" />
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <div>
            <uc5:Payment ID="uxPayment" runat="server"/>
        </div> 
        <table>
            <tr>
                <td>
                    <div class="ProceedtoNextButton">
                        <%-- Zeon Custom Added- OnClientClick='javascript:showWait();' in SubmitButton control--%>
                        <asp:LinkButton ID="SubmitButton" OnClick="OnSubmitOrder" runat="server" CssClass="Button" OnClientClick="zeonCheckout.showWait();"
                            meta:resourcekey="SubmitButtonResource1" ClientIDMode="Static" CausesValidation="true" ToolTip="Place Order">Place Order</asp:LinkButton>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="divnotice" runat="server" class="RemoteSalesTax" visible="false">
                    <br />
                       <Znode:CustomMessage runat="server" MessageKey="RemoteSalesTaxNotice" />
                   </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div>
                        <uc1:spacer ID="Spacer2" EnableViewState="false" SpacerHeight="1" SpacerWidth="10"
                            runat="server" ClientIDMode="Static"></uc1:spacer>
                    </div>
                </td>
            </tr>
        </table>
        <%--Zeon Custom UI: Start--%>
        <div id="progressBarDiv" style="display: none; background-color: gray; filter: alpha(opacity=50); opacity: 0.5;"></div>
        <%--Zeon Custom UI: End--%>
    </div>   
</asp:Panel>
