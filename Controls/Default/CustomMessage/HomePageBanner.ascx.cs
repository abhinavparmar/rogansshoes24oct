﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.Framework.Business;

namespace WebApp.Controls.Default.CustomMessage
{
    public partial class HomePageBanner : System.Web.UI.UserControl
    {
        #region Page Events

        /// <summary>
        /// Handles Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               ltrBannerText.Text= BindHomePageBannerContent();
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Get Banner Content String
        /// </summary>
        /// <param name="portalID">int</param>
        /// <returns>string</returns>
        public string BindHomePageBannerContent()
        {
            string emptyBannerText = Resources.CommonCaption.HomeBannerNotFoundMessage;
            string bannerContent = string.Empty;
            PerfiBannerHelper perfibannerHelper = new PerfiBannerHelper();
            DataTable dtAvailableBanner = perfibannerHelper.GetHomePageBannerByDate(ZNodeConfigManager.SiteConfig.PortalID);
            if (dtAvailableBanner != null && dtAvailableBanner.Rows != null && dtAvailableBanner.Rows.Count > 0)
            {
                bannerContent = string.Format(Resources.CommonCaption.HomeBannerMainContainer, CreateBannerContent(dtAvailableBanner));
            }
            else
            {
                bannerContent = string.Format(Resources.CommonCaption.HomeBannerMainContainer, emptyBannerText);
            }
            return bannerContent;
        }

        /// <summary>
        /// Create multiple item banner 
        /// </summary>
        /// <param name="dtAvailableBanners">Datatable</param>
        /// <returns>string</returns>
        private string CreateBannerContent(DataTable dtAvailableBanners)
        {
            StringBuilder bannerContent = new StringBuilder(); 
            StringBuilder bannerItemContent = new StringBuilder();
            StringBuilder bannerText=new StringBuilder();
            int nonEmptyBanner = 0;
            foreach (DataRow row in dtAvailableBanners.Rows)
            {
                string bannerDiv = string.Empty;
                if(row!=null && row["BannerContent"]!=null && row["BannerContent"]!=string.Empty)
                {
                   bannerDiv = nonEmptyBanner == 0 ? string.Format(Resources.CommonCaption.HomeBannerItemContainerDivItem, "item active", row["BannerContent"].ToString()) : string.Format(Resources.CommonCaption.HomeBannerItemContainerDivItem, "item", row["BannerContent"].ToString());
                   nonEmptyBanner++;
                   bannerItemContent.AppendLine(bannerDiv);
                }
            }
            if (nonEmptyBanner > 1)
            {
                bannerText.AppendLine(BindBannerIndicator(nonEmptyBanner));
                bannerText.AppendLine(string.Format(Resources.CommonCaption.HomeBannerItemContainerDiv, bannerItemContent.ToString()));
                bannerText.AppendLine(Resources.CommonCaption.HomePageMultipleBannerNavigationControl);
            }
            else
            {
                 bannerText.AppendLine(string.Format(Resources.CommonCaption.HomeBannerItemContainerDiv, bannerItemContent.ToString()));
            }
            return bannerText.ToString();
        }

        /// <summary>
        /// Bind Banner Indiactor html
        /// </summary>
        /// <param name="bannerCount">INT</param>
        /// <returns>string</returns>
        private string BindBannerIndicator(int bannerCount)
        {
            string bannerIndicatorText = string.Empty;
            StringBuilder indicatorList =new StringBuilder();
            for (int count = 0; count < bannerCount; count++)
            {
                string classText=count==0?"class=active":string.Empty;
                string indiatorItem = string.Format(Resources.CommonCaption.HomeMultipleBannerIndicatorOrderedListItem, classText, count);
                indicatorList.AppendLine(indiatorItem);
                
            }
            bannerIndicatorText = string.Format(Resources.CommonCaption.HomeMultipleBannerIndicatorOrderedList,indicatorList.ToString());
            return bannerIndicatorText;
        }

        #endregion
    }
}