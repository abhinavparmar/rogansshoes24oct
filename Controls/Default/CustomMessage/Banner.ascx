﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Banner.ascx.cs" Inherits="WebApp.Controls_Default_CustomMessage_Banner" %>

<%--<div id="BannerItems" class="BannerItems">--%>
<div class="row SlideSection">
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div id="myCarousel" class="carousel slide">
            <!-- Carousel indicators -->
           <%-- <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
            </ol>--%>
            <asp:Repeater runat="server" ID="RptBanner" EnableViewState="false">
                <ItemTemplate>
                    <div class="carousel-inner">
                        <div class="item active">
                            <%--class="rotator"style="width:750px;">--%>
                            <asp:Literal runat="server" Text='<%# HttpUtility.HtmlDecode(DataBinder.Eval(Container.DataItem, "Value").ToString()) %>'></asp:Literal>

                        </div>
                    </div>

                </ItemTemplate>
            </asp:Repeater>
            <%--</div>--%>
            </div>
        </div>
    </div>
