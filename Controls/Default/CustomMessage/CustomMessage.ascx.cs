using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    public partial class Controls_Default_CustomMessage_CustomMessage : System.Web.UI.UserControl
    {
        public string MessageKey
        {
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    MessageConfig messageconfig = new MessageConfigAdmin().GetByKeyPortalIDLocaleID(value, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);

                    if (messageconfig != null)
                    {
                        lblMsg.Text = messageconfig.Value;
                    }
                    else
                    {
                        this.Visible = false;
                    }
                }
                else
                {
                    this.Visible = false;
                }
            }
        }
    }
}