﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the Banner user control class.
    /// </summary>
    public partial class Controls_Default_CustomMessage_Banner : System.Web.UI.UserControl
    {
        #region Properties
        private string _displayTime = "5000";
        private string _transitionTime = "1000";
        private string _bannerKey = string.Empty;
        private bool _IsHomePage = false; //Zeon Custom Code

        public string DisplayTime
        {
            get { return this._displayTime; }

            set { this._displayTime = value; }
        }

        public string TransitionTime
        {
            get { return this._transitionTime; }

            set { this._transitionTime = value; }
        }

        public string BannerKey
        {
            get { return this._bannerKey; }
            set { this._bannerKey = value; }
        }

        //Zeon Custom Property
        public bool IsHome
        {
            get { return this._IsHomePage; }
            set { this._IsHomePage = value; }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            string seoUrl;

            // Hide banner if no key provided.
            if (string.IsNullOrEmpty(this._bannerKey))
            {
                this.Visible = false;
            }

            MessageConfigAdmin messageadmin = new MessageConfigAdmin();
            if (!string.IsNullOrEmpty(this._bannerKey))
            {
                TList<MessageConfig> messageConfigs = messageadmin.GetBannersByKeyPortalIDLocaleID(this._bannerKey, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);

                seoUrl = this.IsSEOUrl();

                if (!IsHome) //Zeon Custom Code
                {
                    messageConfigs.ApplyFilter(delegate(MessageConfig msgConfig) { return msgConfig.PageSEOName.ToLower() == seoUrl.ToLower() || string.IsNullOrEmpty(msgConfig.PageSEOName); });
                }

                if (messageConfigs != null && messageConfigs.Count > 0)
                {
                    RptBanner.DataSource = messageConfigs;
                    RptBanner.DataBind();
                }
                else
                {
                    this.Visible = false;
                }
            }
        }

        /// <summary>
        /// Return the SEO Url
        /// </summary>
        /// <returns>Returns the SEO URL</returns>
        private string IsSEOUrl()
        {
            int zcid = 0;

            if (Request.QueryString["zcid"] != null)
            {
               zcid = Convert.ToInt32(Request.QueryString["zcid"].ToString());

               ZNode.Libraries.DataAccess.Service.CategoryService cservice = new ZNode.Libraries.DataAccess.Service.CategoryService();
               Category category = cservice.GetByCategoryID(zcid);
               if (category != null)
               {
                   string seourl = string.Empty;
                   if (category.SEOURL != null)
                   {
                       seourl = category.SEOURL;
                       return seourl;
                   }
                   else
                   {
                       return string.Empty;
                   }
               }
            }
            if (Request.QueryString["zpid"] != null)
            {
               int zpid = Convert.ToInt32(Request.QueryString["zpid"].ToString());

               ZNode.Libraries.DataAccess.Service.ProductService pservice = new ZNode.Libraries.DataAccess.Service.ProductService();
                Product product = pservice.GetByProductID(zpid);
                if (product != null)
                {
                    string seourl = string.Empty;
                    if (product.SEOURL != null)
                    {
                        seourl = product.SEOURL;
                        return seourl;
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
            }



            return string.Empty;
        }
    }
}