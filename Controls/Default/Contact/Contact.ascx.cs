using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the Contact user control class.
    /// </summary>
    public partial class Controls_Default_Contact_Contact : System.Web.UI.UserControl
    {
        #region Page Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            ZNodeResourceManager resourceManager = new ZNodeResourceManager();

            // Set SEO Properties
            ZNodeSEO seo = new ZNodeSEO();
            //PRFT Custom Code : Start
            seo.SEOTitle = resourceManager.GetLocalResourceObject(LocalizeTitle.TemplateControl.AppRelativeVirtualPath, "txtContactus.Text") + " | Rogan�s Shoes";
            seo.SEODescription = string.Format(GetLocalResourceObject("ContactusDescription").ToString(), "Rogan�s Shoes");
            //PRFT Custom Code : End
        }

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {

            //Zeon Custom Code: Start        
            FirstName.Focus();
            //Zeon Custom Code: End
        }
        #endregion

        #region General Events
        protected void ImgSubmit_Click(object sender, EventArgs e)
        {
            //this.SendEmail();//Old Code
            //Zeon custom code
            bool isValid = this.VerifyCaptachText();
            if (isValid)
            {
                this.SendEmail();
            }
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Send Email Receipt
        /// </summary>
        private void SendEmail()
        {
            //Zeon Custom Code: Start   
            ZeonMessageConfig zeonMessageConfig = new ZeonMessageConfig();
            string customerFeedbackTemplate = GetCustomerFeedbackEmailTemplate();
            pnlConfirm.Visible = true;
            pnlContact.Visible = false;

            try
            {
                if (!string.IsNullOrEmpty(customerFeedbackTemplate))
                {
                    ZNodeEmail.SendEmail(ZNodeConfigManager.SiteConfig.CustomerServiceEmail, Email.Text, string.Empty, zeonMessageConfig.GetMessageKey("ContactUsSubject"), customerFeedbackTemplate, true);
                    lblMessage.Text = this.GetLocalResourceObject("FeedbackSubmitted").ToString();
                }
            }
            //Zeon custom code: End
            catch (Exception)
            {
                lblMessage.Text = this.GetLocalResourceObject("RequestFailed").ToString();
                lblMessage.CssClass = "Error";
                return;
            }

            this.CreateCaseRecord();
        }

        #region Zeon Custom Code:Start

        /// <summary>
        /// Get Customer Feedback email template with body content
        /// </summary>
        /// <param name="content">body of email</param>
        /// <returns></returns>
        private string GetCustomerFeedbackEmailTemplate()
        {
            string defaultTemplatePath = string.Empty;
            string comments = string.Empty;
            string firstName = string.Empty;
            string lastName = string.Empty;
            string companyName = string.Empty;
            string phoneNumber;

            comments = Comments.Text;
            firstName = FirstName.Text;
            lastName = LastName.Text;
            phoneNumber = PhoneNumber.Text;
            companyName = CompanyName.Text;

            defaultTemplatePath = Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ZContactUs.html"));

            StreamReader streamReader = new StreamReader(defaultTemplatePath);
            string messageText = streamReader.ReadToEnd();

            string spacerImage = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/Themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/Images/space.png";
            string domainPath = Request.Url.GetLeftPart(UriPartial.Authority);
            int portalId = ZNodeConfigManager.SiteConfig.PortalID;

            ZEmailHelper zEmailHelper = new ZEmailHelper();
            ZeonMessageConfig zeonMessageConfig = new ZeonMessageConfig();
            string copyRightText = zeonMessageConfig.GetMessageKey("FooterCopyrightText");

            string messageTextHeader = zEmailHelper.EmailHeaderContent(domainPath, portalId, spacerImage);
            string messageTextFooter = zEmailHelper.EmailFooterContent(domainPath, portalId, copyRightText, spacerImage);

            //To add header to the template
            Regex rxHeaderContent = new Regex("#HeaderContent#", RegexOptions.IgnoreCase);
            messageText = rxHeaderContent.Replace(messageText, messageTextHeader);

            Regex rxComments = new Regex("#Comments#", RegexOptions.IgnoreCase);
            messageText = rxComments.Replace(messageText, comments);

            Regex rxFirstName = new Regex("#FirstName#", RegexOptions.IgnoreCase);
            messageText = rxFirstName.Replace(messageText, firstName);

            Regex rxLastName = new Regex("#LastName#", RegexOptions.IgnoreCase);
            messageText = rxLastName.Replace(messageText, lastName);

            Regex rxPhoneNumber = new Regex("#PhoneNumber#", RegexOptions.IgnoreCase);
            messageText = rxPhoneNumber.Replace(messageText, phoneNumber);

            Regex rxCompanyName = new Regex("#CompanyName#", RegexOptions.IgnoreCase);
            messageText = rxCompanyName.Replace(messageText, companyName);

            //To add footer to the template
            Regex rxFooterContent = new Regex("#FooterContent#", RegexOptions.IgnoreCase);
            messageText = rxFooterContent.Replace(messageText, messageTextFooter);


            return messageText;
        }
        #endregion

        /// <summary>
        /// Creates New Case Record
        /// </summary>
        private void CreateCaseRecord()
        {
            ZNode.Libraries.Admin.CaseAdmin _CaseAdmin = new ZNode.Libraries.Admin.CaseAdmin();
            ZNode.Libraries.DataAccess.Entities.CaseRequest CaseRecord = new ZNode.Libraries.DataAccess.Entities.CaseRequest();

            // Set Values 
            CaseRecord.Title = "Contact Form Submission";
            CaseRecord.FirstName = Server.HtmlEncode(FirstName.Text);
            CaseRecord.LastName = Server.HtmlEncode(LastName.Text);
            CaseRecord.CompanyName = Server.HtmlEncode(CompanyName.Text);
            CaseRecord.PhoneNumber = Server.HtmlEncode(PhoneNumber.Text);
            CaseRecord.EmailID = Email.Text;
            CaseRecord.Description = Server.HtmlEncode(Comments.Text);

            // Set Some Default Values
            CaseRecord.CasePriorityID = 3;
            CaseRecord.CaseStatusID = 1;
            CaseRecord.CreateDte = System.DateTime.Now;
            CaseRecord.PortalID = ZNodeConfigManager.SiteConfig.PortalID;
            CaseRecord.OwnerAccountID = null;
            CaseRecord.CaseOrigin = "Contact Us Form";
            CaseRecord.CreateUser = System.Web.HttpContext.Current.User.Identity.Name;

            if (Session[ZNodeSessionKeyType.UserAccount.ToString()] != null)
            {
                ZNodeUserAccount _usrAccount = Session[ZNodeSessionKeyType.UserAccount.ToString()] as ZNodeUserAccount;
                CaseRecord.AccountID = _usrAccount.AccountID;
            }
            else
            {
                CaseRecord.AccountID = null;
            }

            // Add New Case for this Email
            _CaseAdmin.Add(CaseRecord);
        }
        #endregion

        #region Zeon Custom Code
        /// <summary>
        /// Verify the Captach Text
        /// </summary>
        private bool VerifyCaptachText()
        {
            CaptchaID.ValidateCaptcha(txtCharacters.Text.Trim());
            if (CaptchaID.UserValidated)
            {
                return true;
            }
            lblErrorMsg.Text = this.GetLocalResourceObject("CaptchaErrorMessage").ToString();
            txtCharacters.Focus();
            return false;
        }

        /// <summary>
        /// Click Event to set Focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbtnRefresh_Click(object sender, EventArgs e)
        {
            txtCharacters.Text = string.Empty;
            lblErrorMsg.Text = string.Empty;
            txtCharacters.Focus();
        }
        #endregion
    }
}
