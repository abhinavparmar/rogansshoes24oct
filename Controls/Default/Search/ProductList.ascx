<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Search_ProductList" Codebehind="ProductList.ascx.cs" %>
<%@ Register Src="~/Controls/Default/Reviews/ProductAverageRating.ascx" TagName="ProductAverageRating"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Default/Product/SwatchImages.ascx" TagName="Swatches"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Common/spacer.ascx" TagName="Spacer" TagPrefix="ZNode" %>

<asp:Panel ID="pnlProductList" runat="server" Visible="False" meta:resourcekey="pnlProductListResource1">
    <div class="ProductList">
        <asp:Label ID="Label1" runat="server" meta:resourcekey="Label1Resource1"></asp:Label>
        <asp:Label ID="ErrorMsg" runat="server" CssClass="Error" meta:resourcekey="ErrorMsgResource1"></asp:Label>
        <div id="ProductListNavigation">
            <div class="TopPagingSection">
                <div class="Sorting">
                    <span class="Label">
                        <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:CommonCaption, Sort%>"></asp:Localize>
                        &raquo;</span>
                    <asp:DropDownList ID="lstFilter" runat="server" OnSelectedIndexChanged="LstFilter_SelectedIndexChanged"
                        AutoPostBack="True" title="SORT BY">
                        <asp:ListItem Value="0" meta:resourcekey="ListItemResource1">Popular Items</asp:ListItem>
                        <asp:ListItem Value="1" meta:resourcekey="ListItemResource2">Price Low to High</asp:ListItem>
                        <asp:ListItem Value="2" meta:resourcekey="ListItemResource3">Price High to Low</asp:ListItem>
                    </asp:DropDownList>
                </div>
                      <%--Zeon Custom Code: Start--%>
                <div>
                  
                    <asp:LinkButton ID="imgbtnGrid" CssClass="ActiveLink GridView" runat="server" Visible="False" Enabled="False" ToolTip="Grid" OnClick="imgbtnGrid_OnClick"></asp:LinkButton>
						<asp:ImageButton ID="imgGrid1" runat="server" AlternateText="Grid" CssClass="ActiveLink" ImageUrl="~/Themes/Default/Images/grid-active.gif" ToolTip="Grid" OnClick="imgbtnGrid_OnClick" />
						&nbsp;&nbsp;<asp:LinkButton ID="imgbtnList" CssClass="DeActiveLink ListView" runat="server" Visible="False" ToolTip="List" OnClick="imgbtnList_OnClick"	></asp:LinkButton>
						<asp:ImageButton ID="imgListActive1" AlternateText="List" ToolTip="List" ImageUrl="~/Themes/Default/Images/list.gif" runat="server"  CssClass="DeActiveLink" OnClick="imgbtnList_OnClick" />
                </div>
                <%--Zeon Custom Code: End--%>
                <div class="TopPaging">
                    <asp:HyperLink ID="hlTopPrevLink" CssClass="Button" Text="<<" runat="server"></asp:HyperLink>
                    <span>
                        <asp:Localize ID="Localize12" runat="server" meta:resourceKey="txtPage">
                        </asp:Localize>
                        <asp:Localize ID="Localize13" runat="server" meta:resourceKey="txtOF">
                        </asp:Localize>
                    </span>
                    <asp:HyperLink ID="hlTopNextLink" CssClass="Button" Text=">>" runat="server"></asp:HyperLink>
                    <span>|
                        <asp:DropDownList ID="ddlTopPaging" runat="server" AutoPostBack="True" CssClass="Pagingdropdown"
                            meta:resourcekey="ddlTopPagingResource1" OnSelectedIndexChanged="DdlTopPaging_SelectedIndexChanged" title="SORT BY">
                        </asp:DropDownList>
                    </span><span>
                        <asp:Localize ID="Localize9" runat="server" meta:resourceKey="txtProducts">
                        </asp:Localize><span class="SlashSeparator">/</span>
                        <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:CommonCaption, Page%>" />
                        </span>
                </div>
            </div>
        </div>
        <div class="CategoryProductlist">
            <asp:DataList ID="DataListProducts" runat="server" RepeatDirection="Horizontal" OnItemDataBound="DataListProducts_ItemDataBound"
                meta:resourcekey="DataListProductsResource1">
                <ItemStyle CssClass="ItemStyle" VerticalAlign="Top" />
                <ItemTemplate>
                    <div class="ProductListItem">
                        <div class="DetailLink">
                            <a id="hlName" class="boxed" href='<%# ResolveUrl("~/quickwatch.aspx?zpid=" + DataBinder.Eval(Container.DataItem, "ProductID")) %>'
                                rel="{handler:'iframe',size:{x:700,y:500}}">
                                <%# DataBinder.Eval(Container.DataItem, "Name").ToString() %></a>
                        </div>
                        <div class="Price">
                            <asp:Label ID="Label2" EnableViewState="false" runat="server" meta:resourcekey="Label2Resource1" Text='<%# DataBinder.Eval(Container.DataItem, "FormattedPrice") %>'
                                Visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && ProductListProfile.ShowPrice %>'></asp:Label>
                        </div>
                        <%--Zeon Custom HTML: Start--%>
                        <div class="ShortDescription">
                            <asp:Label ID="ShortDescription" runat="server" Text='<%# WebApp.ZCommonHelper.GetFormattedProductName(((ZNode.Libraries.ECommerce.Catalog.ZNodeProductBase)Container.DataItem).ShortDescription.ToString(), System.Configuration.ConfigurationManager.AppSettings["ProductGridNameMaxLimit"].ToString())%>' meta:resourcekey="ShortDescriptionResource1" EnableViewState="false" />
                        </div>
                        <br />
						  <%--Zeon Custom HTML: End--%>
                        <div class="Image">
                            <a id="imgProduct" class="boxed" href='<%# ResolveUrl("~/quickwatch.aspx?zpid=" + DataBinder.Eval(Container.DataItem, "ProductID")) %>'
                                ondblclick="javascript:return false;" rel="{handler:'iframe',size:{x:700,y:500}}">
                              
                                     <img id='<%# "Img" + DataBinder.Eval(Container.DataItem, "ProductID")%>' alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag") %>'
                                         title='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag") %>'
                            border="0" src='<%# ResolveUrl(DataBinder.Eval(Container.DataItem, "SmallImageFilePath").ToString()) %>' class="lazy"
                            style="vertical-align: bottom;"></img>

                            </a>
                        </div>
                        <div class="ProductSwatches" style='border-top: <%# GetColorCaption(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "AlternateProductImageCount"))) == "" ? "None":"solid 1px #D1D1D1" %>'>
                            <div class="ColorCaption">
                                <asp:Label ID="lblColor" EnableViewState="false" runat="server" meta:resourcekey="lblColorResource1" Text='<%# GetColorCaption(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "AlternateProductImageCount"))) %>'></asp:Label>
                            </div>
                            <ZNode:Swatches ID="uxProductSwatches" runat="server" ControlType="Swatches" ProductId='<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "AlternateProductImageCount")) > 0? DataBinder.Eval(Container.DataItem, "ProductID"):0 %>' IncludeProductImage="true" />
                        </div>   
                        <div class="StarRating">                            
                            <ZNode:ProductAverageRating ID="uxProductAverageRating"  ProductName='<%# DataBinder.Eval(Container.DataItem, "Name").ToString() %>' 
                                TotalReviews='<%# DataBinder.Eval(Container.DataItem, "TotalReviews") %>'
                                ViewProductLink='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink")%>'
                                ReviewRating='<%# DataBinder.Eval(Container.DataItem, "ReviewRating")%>' runat="server" />
                        </div>             
                        <div class="GrayBorder">
                            <ZNode:Spacer ID="Spacer1"  EnableViewState="false" runat="server" SpacerHeight="1" SpacerWidth="1" />
                        </div>
                        <div class="CallForPrice">
                            <asp:Label ID="uxCallForPricing" runat="server" CssClass="Price" meta:resourcekey="uxCallForPricingResource1"
                               Text='<%# CheckForCallForPricing(DataBinder.Eval(Container.DataItem, "CallForPricing"), DataBinder.Eval(Container.DataItem, "CallMessage")) %>'></asp:Label>
                       
                        </div>
                        <div style="width: 100%;">
                            <div style="width: 40%; float: left;">
                                    <asp:HyperLink ID="hlView" CssClass="Button View" 
                                        EnableViewState="false"
                                        meta:resourcekey="btnbuyResource1"
                                        runat="server" 
                                        Visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && ProductListProfile.ShowAddToCart %>'
                                        NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink") %>'>View �</asp:HyperLink>
                            </div>
                            <div style="width: 70%; float: right; text-align: right;">
                                <%--<asp:Image ID="NewItemImage" runat="server" ImageUrl='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("new.png") %>'
                                    meta:resourcekey="NewItemImageResource1" EnableTheming="false" Visible='<%# DataBinder.Eval(Container.DataItem, "NewProductInd") %>' AlternateText="New" alt="New" />--%>
                                    <span class="rs-new" id="spnRSNew" runat="server" visible='<%#DataBinder.Eval(Container.DataItem, "NewProductInd")%>'><span class="rs-new-text">New</span></span>
                                &nbsp;<asp:Image ID="FeaturedItemImage" runat="server" ImageUrl='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("sale.png") %>'
                                    meta:resourcekey="FeaturedItemImageResource1" EnableTheming="false" Visible='<%# DataBinder.Eval(Container.DataItem, "FeaturedInd") %>' AlternateText="Featured" ToolTip="Featured"/>
                            </div>
                            <div style="clear: both;">
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
               <%-- <SeparatorStyle CssClass="Separator" VerticalAlign="Top" Wrap="True" />
                <SeparatorTemplate>
                    <img id="imgSeparator" runat="server" src="<%# ProductListSeparatorImage %>"></img>
                </SeparatorTemplate>--%>
            </asp:DataList>
        </div>
        <div class="BottomPaging">
            <div class="Paging">
                <asp:HyperLink ID="hlBotPrevLink" CssClass="Button" Text="<<" runat="server"></asp:HyperLink>
                <span>
                    <asp:Localize ID="Localize3" runat="server" meta:resourceKey="txtPage"></asp:Localize>
                    <asp:Localize ID="Localize4" runat="server" meta:resourceKey="txtOF"></asp:Localize>
                </span>
                <asp:HyperLink ID="hlBotNextLink" Text=">>" CssClass="Button"  runat="server"></asp:HyperLink>
                <span>|<span class="ShowText">
                    <asp:DropDownList runat="server" ID="ddlBottomPaging" CssClass="Pagingdropdown" AutoPostBack="True"
                        OnSelectedIndexChanged="DdlBottomPaging_SelectedIndexChanged" meta:resourcekey="ddlBottomPagingResource1" title="Top Paging">
                    </asp:DropDownList>
                </span></span><span>
                    <asp:Localize ID="Localize1" runat="server" meta:resourceKey="txtProducts"></asp:Localize><span
                        class="SlashSeparator">/</span>
                    <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:CommonCaption, Page%>" /></span>
            </div>
        </div>
    </div>
</asp:Panel>