<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Search_Search" Codebehind="Search.ascx.cs" %>
<%@ Register Src="ProductList.ascx" TagName="ProductList" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Common/spacer.ascx" TagName="Spacer" TagPrefix="ZNode" %>
<asp:UpdatePanel runat="server" ID="upSearchPage">
    <ContentTemplate>
        <div id="ProductSearch">
            <div class="PageTitle" style="margin-top: 12px; margin-left: 15px;">
                <asp:Localize ID="Localize3" meta:resourceKey="txtSearchcatalog" runat="server" /></div>
            <ZNode:Spacer ID="Spacer2" spacerheight="10" spacerwidth="1" runat="server" />
            <div class="Description">
                <asp:Localize ID="Localize1" meta:resourceKey="txtSearchDescription" runat="server" /></div>
            <asp:Panel ID="pnlSearchCatalog" runat="server" DefaultButton="btnSearch" meta:resourcekey="pnlSearchCatalogResource1">
                <div class="SearchForm">
                    <div class="Row">
                        <div class="FieldStyle">
                            <asp:Localize ID="Localize13" meta:resourceKey="txtKeywordLabel" runat="server" />
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtKeyword" runat="server" meta:resourcekey="txtKeywordResource1"></asp:TextBox>&nbsp;&nbsp;
                            <asp:DropDownList ID="lstSearchOption" runat="server">
                                <asp:ListItem Selected="True" Value="0" meta:resourcekey="ListItemResource1" />
                                <asp:ListItem Value="1" meta:resourcekey="ListItemResource2" />
                                <asp:ListItem Value="2" meta:resourcekey="ListItemResource3" />
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="Row">
                        <div class="FieldStyle">
                            <asp:Localize ID="Localize2" meta:resourceKey="txtSearchinCategory" runat="server" />
                        </div>
                        <div class="ValueStyle">
                            <asp:DropDownList ID="lstCategories" runat="server" meta:resourcekey="lstCategoriesResource1">
                                <asp:ListItem Selected="True" Value="0" meta:resourcekey="ListItemResource4" />
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="Row">
                        <div class="FieldStyle">
                            <asp:Localize ID="Localize4" meta:resourceKey="txtSKUPart" runat="server" />
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtSKU" runat="server" meta:resourcekey="txtSKUResource1"></asp:TextBox></div>
                    </div>
                    <div class="Row">
                        <div class="FieldStyle">
                            <asp:Localize ID="Localize5" meta:resourceKey="txtProductNumber" runat="server" />
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtProductNum" runat="server" meta:resourcekey="txtProductNumResource1"></asp:TextBox></div>
                    </div>
                    <div class="Row">
                        <div class="FieldStyle">
                            &nbsp;
                        </div>
                        <div class="ValueStyle">
                            <asp:LinkButton ID="btnSearch" runat="server" OnClick="BtnSearch_Click" CssClass="Button"
                                Text="<%$ Resources:CommonCaption, Search %>" />&nbsp;&nbsp;<asp:LinkButton ID="btnClear"
                                    runat="server" CssClass="Button" OnClick="BtnClear_Click" Text="<%$ Resources:CommonCaption, Cancel %>" />
                        </div>
                    </div>
                    <div>
                        <div>
                            <ZNode:Spacer ID="Spacer1" spacerheight="20" spacerwidth="1" runat="server" />
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <div class="CategoryDetail">
                <ZNode:ProductList ID="uxProductList" runat="server" CssClass="ProductList" Title="Search Results">
                </ZNode:ProductList>
            </div>
            <div style="margin-left: 20px;" class="Error">
                <asp:Literal ID="FailureText" EnableViewState="False" runat="server" meta:resourcekey="FailureTextResource1" /></div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

<asp:UpdateProgress ID="uxSearchUpdateProgress" runat="server" AssociatedUpdatePanelID="upSearchPage" DisplayAfter="10">
    <ProgressTemplate>
        <div id="ajaxProgressBg">
        </div>
        <div id="ajaxProgress">
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
