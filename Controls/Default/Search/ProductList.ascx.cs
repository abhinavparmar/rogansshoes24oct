using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the Product List user control class.
    /// </summary>
    public partial class Controls_Default_Search_ProductList : System.Web.UI.UserControl
    {
        #region Private Variables
        private string _ProductListSeparatorImage = string.Empty;
        private string BuyImage = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/view_cart_bg.gif";
        private ZNodeProfile _ProductListProfile = new ZNodeProfile();
        private ZNodeProductList _ProductList;        
        private int RecCount = 0;
        private int Currentpage = 0;
        //Zeon Custom Code: Start
        private string _view = string.Empty;
        private string _listActiveURL = "~/Themes/" + ZNodeCatalogManager.Theme + "/Images/list-active.gif";
        private string _gridDeActiveURL = "~/Themes/" + ZNodeCatalogManager.Theme + "/Images/grid.gif";
        private string _gridActiveURL = "~/Themes/" + ZNodeCatalogManager.Theme + "/Images/grid-active.gif";
        private string _listDeActiveURL = "~/Themes/" + ZNodeCatalogManager.Theme + "/Images/list.gif";
        //Zeon Custom Code: End
        #endregion

        // Public Event Handler
        public event System.EventHandler PagingSelectedIndexChanged;

        public event System.EventHandler SortingSelectedIndexChanged;

        public event System.EventHandler PreviousButtonClicked;

        public event System.EventHandler NextButtonClicked;

        #region Public Properties

        /// <summary>
        /// Gets the product list seperator image
        /// </summary>
        public string ProductListSeparatorImage
        {
            get { return "~/themes/" + ZNodeCatalogManager.Theme + "/Images/line_seperator.gif"; }
        }

        /// <summary>
        /// Gets or sets the product list page profile.
        /// </summary>
        public ZNodeProfile ProductListProfile
        {
            get { return this._ProductListProfile; }
            set { this._ProductListProfile = value; }
        }

        /// <summary>
        /// Gets the current page
        /// </summary>
        public int CurrentPage
        {
            get
            {
                int currentPage = 1;

                if (Request.QueryString["page"] != null)
                {
                    currentPage = Convert.ToInt32(Request.QueryString["page"]);
                }

                return currentPage;
            }
        }

        /// <summary>
        /// Gets or sets the Total Records
        /// </summary>
        public int TotalRecords
        {
            get
            {
                int totalRecords = 0;

                if (ViewState["TotalRecords"] != null)
                {
                    totalRecords = (int)ViewState["TotalRecords"];
                }

                return totalRecords;
            }

            set
            {
                ViewState["TotalRecords"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the Total Pages
        /// </summary>
        public int TotalPages
        {
            get
            {
                int totalPages = 0;

                if (ViewState["TotalPages"] != null)
                {
                    totalPages = (int)ViewState["TotalPages"];
                }

                return totalPages;
            }

            set
            {
                ViewState["TotalPages"] = value;
            }
        }

        /// <summary>
        /// Gets the search list page pize
        /// </summary>
        public int PageSize
        {
            get
            {
                int pageSize = 8;

                string eventTarget = "__EVENTTARGET";
                if (Request.QueryString["size"] != null)
                {
                    pageSize = Convert.ToInt32(Request.QueryString["size"]);
                }
                else if (ddlTopPaging.SelectedIndex > 0 || ddlBottomPaging.SelectedIndex > 0)
                {
                    if (Request[eventTarget] != null && Request[eventTarget].Contains(ddlTopPaging.ID))
                    {
                        pageSize = Convert.ToInt32(ddlTopPaging.SelectedValue);
                    }
                    else
                    {
                        pageSize = Convert.ToInt32(ddlBottomPaging.SelectedValue);
                    }
                }
                else
                    pageSize = 8;

                return pageSize;
            }
        }

        /// <summary>
        /// Gets or sets the  product list passed in from the search page
        /// </summary>
        public ZNodeProductList ProductList
        {
            get
            {
                return this._ProductList;
            }

            set
            {
                this._ProductList = value;
            }
        }

        /// <summary>
        /// Gets the SoryBy value
        /// </summary>
        public string SortBy
        {
            get
            {
                // Apply Sorting Only Page gets Reload.
                if (!lstFilter.SelectedValue.Equals("0"))
                {
                    return "RetailPrice";
                }

                return "DisplayOrder";
            }
        }

        /// <summary>
        /// Gets the Sort Direction
        /// </summary>
        public string SortDirection
        {
            get
            {
                if (lstFilter.SelectedValue.Equals("2"))
                {
                    return "DESC";
                }

                return "ASC";
            }
        }

        /// <summary>
        /// Gets the sort option
        /// </summary>
        public int SortOption
        {
            get
            {
                if (lstFilter.SelectedValue.Equals("2"))
                {
                    return 2;
                }

                // Apply Sorting Only Page gets Reload.
                if (!lstFilter.SelectedValue.Equals("0"))
                {
                    return 1;
                }

                return 0;
            }
        }

        #endregion

        #region Private Properties
        /// <summary>
        /// Gets or sets the Last Index
        /// </summary>
        private int LastIndex
        {
            get
            {
                int lastIndex = 0;

                if (ViewState["LastIndex"] != null)
                {
                    lastIndex = Convert.ToInt32(ViewState["LastIndex"]);
                }

                return lastIndex;
            }

            set
            {
                ViewState["LastIndex"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the First Index
        /// </summary>
        private int FirstIndex
        {
            get
            {
                int firstIndex = 0;

                if (ViewState["FirstIndex"] != null)
                {
                    firstIndex = Convert.ToInt32(ViewState["FirstIndex"]);
                }

                return firstIndex;
            }

            set
            {
                ViewState["FirstIndex"] = value;
            }
        }
        #endregion

        #region Public Methods      

        /// <summary>
        /// Check Call For Pricing enabled for the product
        /// </summary>
        /// <param name="fieldValue">The Field Value</param>
        /// <returns>Returns the Call For Pricing message</returns>
        public string CheckForCallForPricing(object fieldValue, object callMessage)
        {
            MessageConfigAdmin mconfig = new MessageConfigAdmin();
            bool Status = bool.Parse(fieldValue.ToString());
            string message = mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, Convert.ToInt32(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0)), 43);

            if (callMessage != null && !string.IsNullOrEmpty(callMessage.ToString()))
            {
                message = callMessage.ToString();
            }  
            if (Status)
            {
                return message;
            }
            else if (!this._ProductListProfile.ShowPrice)
            {
                return message;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Represents the GetColorOption method
        /// </summary>
        /// <param name="AlternateProductImageCount">Alternate Product Image Count</param>
        /// <returns>Returns the Color option</returns>
        public string GetColorCaption(int AlternateProductImageCount)
        {
            string output = string.Empty;

            if (AlternateProductImageCount > 0)
            {
                output = "COLORS ";
            }

            return output;
        }

        /// <summary>
        /// Bind display based on a product list
        /// </summary>
        public void BindProducts()
        {
            if (this.Visible)
            {
                if (this._ProductList.ZNodeProductCollection.Count == 0)
                {
                    pnlProductList.Visible = false;
                    ErrorMsg.Visible = true;
                    ErrorMsg.Text = Resources.CommonCaption.NoDataFound;
                }
                else
                {
                    pnlProductList.Visible = true;
                    ErrorMsg.Visible = false;
                }

                if (this._ProductList.ZNodeProductCollection.Count > 0)
                {
                    this.BindDataListPagedDataSource();
                }
            }
        }

        #endregion

        #region Protected Methods and Events

        /// <summary>
        /// Set the page size to selected DropDownList value size. If "SHOW ALL" selected then display all products.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlTopPaging_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.PagingSelectedIndexChanged != null)
            {
                this.PagingSelectedIndexChanged(sender, e);
            }

            this.NavigationToUrl(1, Convert.ToInt32(ddlTopPaging.SelectedValue));
        }

        /// <summary>
        /// Set the page size to selected DropDownList value size. If "SHOW ALL" selected then display all products.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlBottomPaging_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.PagingSelectedIndexChanged != null)
            {
                this.PagingSelectedIndexChanged(sender, e);
            }
            
            this.NavigationToUrl(1, Convert.ToInt32(ddlBottomPaging.SelectedValue));
        }

        /// <summary>
        /// Event is raised when LstFilter control Selected Index is Changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
           if (this.SortingSelectedIndexChanged != null)
            {
                this.SortingSelectedIndexChanged(sender, e);
            }

           this.NavigationToUrl(1, Convert.ToInt32(ddlBottomPaging.SelectedValue));
           // this.BindDataListPagedDataSource();
        }

        /// <summary>
        /// Event is raised when DataListProducts Item Data Bound is called
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DataListProducts_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            // Find the seperator image and then remove for the last column
            if (e.Item.ItemType == ListItemType.Separator)
            {
                int lastColumnIndex = ZNodeConfigManager.SiteConfig.MaxCatalogDisplayColumns;

                if ((e.Item.ItemIndex + 1) % lastColumnIndex == 0 && e.Item.ItemIndex != 0)
                {
                    foreach (Control ctrl in e.Item.Controls)
                    {
                        if (ctrl.GetType().ToString() == "System.Web.UI.HtmlControls.HtmlImage")
                        {
                            e.Item.Controls.Remove(ctrl);
                        }
                    }
                }
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["sort"] != null)
                {
                    lstFilter.SelectedValue = Request.QueryString["sort"].ToString();
                }
                if (Request.QueryString["size"] != null)
                {
                    int size = Convert.ToInt32(Request.QueryString["size"].ToString());
                    ddlTopPaging.SelectedIndex = ddlTopPaging.Items.IndexOf(ddlTopPaging.Items.FindByValue(size.ToString()));
                    ddlBottomPaging.SelectedIndex = ddlTopPaging.SelectedIndex;
                }
            }
            //Zeon Custom Code: Start
            if (Request.Params["v"] != null)
            {
                _view = Request.Params["v"];
            }
            else
            {
                _view = "g";
                imgbtnGrid.Enabled = false;
                imgGrid1.Enabled = false;
            }

            // Set Repeat columns to the product Data list
            DataListProducts.RepeatColumns = ZNodeConfigManager.SiteConfig.MaxCatalogDisplayColumns;
            //Zeon Custom Code: End
           
            int previousPageIndex = this.Currentpage;
            
            if (previousPageIndex < this.CurrentPage)
            {
                if (this.NextButtonClicked != null)
                {
                    this.NextButtonClicked(sender, e);
                }
            }
            else
            {
                if (this.PreviousButtonClicked != null)
                {
                    this.PreviousButtonClicked(sender, e);
                }
            }

          

            string baseUrl = this.GetNavigationUrl();

            //// Do not add the & or ? in the following query string.
            //Zeon Custom Code: Start
            //hlTopPrevLink.NavigateUrl = hlBotPrevLink.NavigateUrl = string.Format("{0}page={1}&size={2}&sort={3}&v={4}", baseUrl, this.CurrentPage - 1, ddlTopPaging.SelectedValue,lstFilter.SelectedValue);
            //hlTopNextLink.NavigateUrl = hlBotNextLink.NavigateUrl = string.Format("{0}page={1}&size={2}&sort={3}&v={4}", baseUrl, this.CurrentPage + 1, ddlTopPaging.SelectedValue,lstFilter.SelectedValue);
            hlTopPrevLink.NavigateUrl = hlBotPrevLink.NavigateUrl = string.Format("{0}page={1}&size={2}&sort={3}&v={4}", baseUrl, this.CurrentPage - 1, ddlTopPaging.SelectedValue, lstFilter.SelectedValue, _view);
            hlTopNextLink.NavigateUrl = hlBotNextLink.NavigateUrl = string.Format("{0}page={1}&size={2}&sort={3}&v={4}", baseUrl, this.CurrentPage + 1, ddlTopPaging.SelectedValue, lstFilter.SelectedValue, _view);
            //Zeon Custom Code: End

            //hlTopPrevLink.NavigateUrl = hlBotPrevLink.NavigateUrl = string.Format("{0}page={1}&size={2}&sort={3}", baseUrl, this.CurrentPage - 1, ddlTopPaging.SelectedValue, lstFilter.SelectedValue);
           // hlTopNextLink.NavigateUrl = hlBotNextLink.NavigateUrl = string.Format("{0}page={1}&size={2}&sort={3}", baseUrl, this.CurrentPage + 1, ddlTopPaging.SelectedValue, lstFilter.SelectedValue);

            this.BindDataListPagedDataSource();
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Navigate to the category page with query string the specified current page and page size.
        /// </summary>
        /// <param name="currentPage">Current page index.</param>
        /// <param name="pageSize">Category page size. If Show All selected then -1 will be used.</param>
        private void NavigationToUrl(int currentPage, int pageSize)
        {
            string baseUrl = this.GetNavigationUrl();

            // Do not add the & or ? in the following query string.
            //Zeon Custom Code: Start
            string view = "g";
            if (imgGrid1.CssClass == "ActiveLink")
            {
                view = "g";
            }
            else
            {
                view = "l";
            }
            //string navigateUrl = string.Format("{0}page={1}&size={2}&sort={3}", baseUrl, currentPage, pageSize, lstFilter.SelectedValue);
            string navigateUrl = string.Format("{0}page={1}&size={2}&v={3}", baseUrl, currentPage, pageSize, view);
            //Zeon Custom Code: End
            Response.Redirect(navigateUrl);
        }

        /// <summary>
        /// Gets the navigation url for the previous/next navigation link
        /// </summary>
        /// <returns>Returns the navigation base url.</returns>
        private string GetNavigationUrl()
        {
            string baseUrl = string.Empty;
            if (this.Request.QueryString.Count > 0)
            {
                StringBuilder url = new StringBuilder();
                url.Append(Request.Url.GetLeftPart(UriPartial.Authority));
                url.Append(Request.Url.AbsolutePath);

                // Append the category Url with base url
                if (Request.QueryString["keyword"] != null)
                {
                    url.Append("?keyword=").Append(Request.QueryString["keyword"]).Append("&");
                }
                else
                {
                    url.Append("?");
                }

                baseUrl = url.ToString();
            }
            else
            {
                baseUrl = Request.Url.ToString()+"?";
            }

            return baseUrl;
        }

        /// <summary>
        /// Bind Datalist using Paged DataSource object
        /// </summary>
        private void BindDataListPagedDataSource()
        {
            //Zeon Custom Code : Start
            if (Request.QueryString["v"] != null)
            {
                if (Request.QueryString["v"] == "g")
                {
                    SetGridView();
                }
                else
                {
                    SetListView();
                }
            }
            //Zeon Custom Code : End
            // Retrieve collection object from Viewstate
            if (this._ProductList == null)
            {
                return;
            }

            // Assigning Datasource to the DataList.
            DataListProducts.DataSource = this._ProductList.ZNodeProductCollection;
            DataListProducts.DataBind();

            // Disable the view state for the data list item. 
            foreach (DataListItem item in DataListProducts.Items)
            {
                item.EnableViewState = false;
            }

            this.Currentpage = this.CurrentPage;
            this.RecCount = this.TotalPages = this._ProductList.TotalPageCount;
            this.TotalRecords = this._ProductList.TotalRecordCount;

            hlTopNextLink.Enabled = hlBotNextLink.Enabled = !(this.Currentpage == this.RecCount);
            hlTopPrevLink.Enabled = hlBotPrevLink.Enabled = !(this.Currentpage == 1);

            if (!this.IsPostBack)
            {
                this.DoPaging();
            }
        }

        /// <summary>
        /// Binding Paging List
        /// </summary>
        private void DoPaging()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("PageIndex");
            dt.Columns.Add("PageText");

            this.FirstIndex = this.CurrentPage - 5;

            if (this.CurrentPage > 5)
            {
                this.LastIndex = this.CurrentPage + 5;
            }
            else
            {
                this.LastIndex = 10;
            }

            if (this.LastIndex > this.TotalPages)
            {
                this.LastIndex = this.TotalPages;
                this.FirstIndex = this.LastIndex - 10;
            }

            if (this.FirstIndex < 0)
            {
                this.FirstIndex = 0;
            }

            ddlTopPaging.Items.Clear();
            ddlBottomPaging.Items.Clear();

            string pagingText = this.GetLocalResourceObject("PagingText").ToString();

            // Each items seperated with & symbol
            string[] pagingItems = pagingText.Split(new char[] { '&' });

            foreach (string pagingItem in pagingItems)
            {
                if (pagingItem.Contains("|") && pagingItem.Trim().Length > 0)
                {
                    // Key value pair seperated with | symbol
                    string[] itemText = pagingItem.Split(new char[] { '|' });
                    DataRow dr = null;
                    dr = dt.NewRow();
                    dr[0] = Convert.ToInt32(itemText[1].Trim());
                    dr[1] = itemText[0].Trim();
                    dt.Rows.Add(dr);
                }
            }

            this.ddlTopPaging.DataTextField = "PageText";
            this.ddlTopPaging.DataValueField = "PageIndex";
            this.ddlTopPaging.DataSource = dt;
            this.ddlTopPaging.DataBind();

            this.ddlBottomPaging.DataTextField = "PageText";
            this.ddlBottomPaging.DataValueField = "PageIndex";
            this.ddlBottomPaging.DataSource = dt;
            this.ddlBottomPaging.DataBind();

            if (Request.QueryString["size"] != null)
            {
                int size = Convert.ToInt32(Request.QueryString["size"].ToString());
                ddlTopPaging.SelectedIndex = ddlTopPaging.Items.IndexOf(ddlTopPaging.Items.FindByValue(size.ToString()));
                ddlBottomPaging.SelectedIndex = ddlTopPaging.SelectedIndex;
            }
        }

        /// <summary>
        /// Set the selected page size and navigate to that page.
        /// </summary>
        /// <param name="selectedPageSize">Selected Page Size</param>
        private void SetSelectedPageSize(int selectedPageSize)
        {
            // Point to the first page if user selects the page size.            
            this.NavigationToUrl(this.CurrentPage, selectedPageSize);
        }
        #endregion

        # region Zeon Methods
        protected void imgbtnList_OnClick(object sender, EventArgs e)
        {
            SetListView();
            NavigationToUrl(this.CurrentPage, this.PageSize);
        }

        /// <summary>
        /// Set List view of product list
        /// </summary>
        private void SetListView()
        {
            DataListProducts.RepeatColumns = 1;
            DataListProducts.RepeatDirection = RepeatDirection.Vertical;

            imgbtnGrid.Enabled = true;
            imgGrid1.Enabled = true;
            imgbtnGrid.CssClass = "DeActiveLink GridView";

            imgbtnList.Enabled = false;
            imgListActive1.Enabled = false;

            imgbtnList.CssClass = "ActiveLink ListView";

            imgListActive1.ImageUrl = _listActiveURL;

            imgGrid1.ImageUrl = _gridDeActiveURL;

            imgListActive1.CssClass = "ActiveLink";

            imgGrid1.CssClass = "DeActiveLink";

        }

        protected void imgbtnGrid_OnClick(object sender, EventArgs e)
        {

            SetGridView();
            NavigationToUrl(this.CurrentPage, this.PageSize);


        }

        /// <summary>
        /// Set Grid view of product list
        /// </summary>
        private void SetGridView()
        {
            DataListProducts.RepeatColumns = ZNodeConfigManager.SiteConfig.MaxCatalogDisplayColumns;
            DataListProducts.RepeatDirection = RepeatDirection.Horizontal;

            imgbtnGrid.CssClass = "ActiveLink GridView";
            imgbtnGrid.Enabled = false;
            imgGrid1.Enabled = false;

            imgbtnList.CssClass = "DeActiveLink ListView";
            imgbtnList.Enabled = true;
            imgListActive1.Enabled = true;

            imgListActive1.ImageUrl = _listDeActiveURL;

            imgGrid1.ImageUrl = _gridActiveURL;

            imgListActive1.CssClass = "DeActiveLink";

            imgGrid1.CssClass = "ActiveLink";
        }
        # endregion

    }
}