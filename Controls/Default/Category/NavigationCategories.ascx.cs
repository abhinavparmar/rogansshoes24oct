using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the Navigation Categories user control class.
    /// </summary>
    public partial class Controls_Default_Category_NavigationCategories : System.Web.UI.UserControl
    {
        #region Private Variables
        private string selectedCategoryId = string.Empty;
        private bool _ShowCategorySiblings = false;
        private bool _EnableNavigationViewState = true;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets a value indicating whether to show Category Siblings or not
        /// </summary>
        public bool ShowCategorySiblings
        {
            get
            {
                return this._ShowCategorySiblings;
            }

            set
            {
                this._ShowCategorySiblings = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the treeview control persists its view
        /// state, to the requesting client.
        /// <remarks>true if the server control maintains its view state; 
        /// otherwise false. The default is true.</remarks>
        /// </summary>
        public bool EnableNavigationViewState
        {
            get
            {
                return this._EnableNavigationViewState;
            }

            set
            {
                this._EnableNavigationViewState = value;
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["zcid"] != null)
            {
                this.selectedCategoryId = Request.Params["zcid"];
            }

            if (!Page.IsPostBack || !this._EnableNavigationViewState)
            {
                this.BindData();
            }
        }

        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the treeview
        /// </summary>
        private void BindData()
        {
            //Zeon Custom Code:Start
            int brandID = 0;


            if (Request.Params["mid"] != null)
            {
                brandID = int.Parse(Request.Params["mid"]);
            }
            if ((brandID > 0 || IsSubCategory() || IsShowAllCategories) || !IsProdLstEnableInSubCategory())
            {
                //Zeon Custom Code:end
                ZNodeNavigation navigation = new ZNodeNavigation();
                navigation.PopulateStoreTreeView(ctrlNavigation, this.selectedCategoryId, this._ShowCategorySiblings);
                divCategoryTreeView.Visible = ctrlNavigation.Nodes.Count <= 0 ? false : true;//Zeon Custom Code
            }
            else
            {
                divCategoryTreeView.Visible = false;
            }

        }
        #endregion

        #region Zeon Custom Methods

        /// <summary>
        /// Is sub category
        /// </summary>
        /// <returns></returns>
        private bool IsSubCategory()
        {
            // Retrieve category data from httpContext
            ZNodeCategory _category = (ZNodeCategory)HttpContext.Current.Items["Category"];
            if (_category != null && _category.SubCategoryGridVisibleInd)
            {
                if (_category.ZNodeCategoryCollection.Count == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }

        bool _isShowAllCategories = false;
        public bool IsShowAllCategories
        {
            get { return _isShowAllCategories; }
            set { _isShowAllCategories = value; }
        }

        /// <summary>
        /// Is sub category has Product List Enabled
        /// </summary>
        /// <returns>bool</returns>
        private bool IsProdLstEnableInSubCategory()
        {
            // Retrieve category data from httpContext
            int showSubCatPrdList = 0;
            bool isPrdListEnabled = false;
            ZNodeCategory _category = (ZNodeCategory)HttpContext.Current.Items["Category"];
            if (_category != null)
            {
                int.TryParse(_category.Custom2, out showSubCatPrdList);
                isPrdListEnabled = showSubCatPrdList == 1 ? true : false;
            }
            return isPrdListEnabled;
        }
        #endregion

    }
}