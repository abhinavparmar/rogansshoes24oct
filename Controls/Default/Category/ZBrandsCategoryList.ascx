﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ZBrandsCategoryList.ascx.cs" Inherits="WebApp.Controls.Default.Category.ZBrandsCategoryList" %>
<div class="ContentContainer">
    <h1 class="CategoryTitle">
        <asp:Literal ID="ltrAllBrandTitle" Text="Shop By Brand" runat="server" ClientIDMode="Static" EnableViewState="false"></asp:Literal></h1>
    <div class="AllBrands">
        <asp:Literal ID="ltlBrandLinkTab" runat="server" EnableViewState="false"></asp:Literal>
    </div>
    <br />

    <div class="Clear"></div>
    <div id="ShopByBrandsContainer">
        <div id="dvShopByBrandsList">
            <asp:Literal ID="ltlAllBrands" runat="server" EnableViewState="false"></asp:Literal>
        </div>
    </div>
    <div class="horizontalline">&nbsp;</div>
</div>
