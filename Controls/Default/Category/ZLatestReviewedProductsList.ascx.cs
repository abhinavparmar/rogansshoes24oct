﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.EntityFramework;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Catalog;
using System.Configuration;

namespace WebApp
{
    public partial class Controls_Default_Category_ZLatestReviewedProductsList : System.Web.UI.UserControl
    {
        #region  Private Member
        private ZNodeProfile _Profile = new ZNodeProfile();
        private ZNodeProductList _latestReviewedProductList;
        private int _categoryId;
        private ZNodeCategory _category;
        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Get category id from querystring  
                if (Request.Params["zcid"] != null)
                {
                    this._categoryId = int.Parse(Request.Params["zcid"]);
                }
                if (HttpContext.Current.Items["Category"] != null)
                {
                    this._category = (ZNodeCategory)HttpContext.Current.Items["Category"];
                }
                BindTopReviewedProductList();
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the brand profile.
        /// </summary>
        public ZNodeProfile BrandProfile
        {
            get
            {
                return this._Profile;
            }

            set
            {
                this._Profile = value;
            }
        }

        /// <summary>
        /// Sets the Product List
        /// </summary>
        public ZNodeProductList LatestReviewedProductList
        {
            get { return this._latestReviewedProductList; }
            set { this._latestReviewedProductList = value; }
        }

        #endregion

        #region Preotected Methods and Events

        /// <summary>
        /// Represents the CheckForCallFor Pricing method
        /// </summary>
        /// <param name="fieldValue">field value</param>
        /// <returns>Returns the Call For Pricing message</returns>
        protected string CheckForCallForPricing(object fieldValue, object callMessage)
        {
            MessageConfigAdmin mconfig = new MessageConfigAdmin();
            bool Status = bool.Parse(fieldValue.ToString());
            string message = mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, ZNodeConfigManager.SiteConfig.PortalID, ZNodeConfigManager.SiteConfig.LocaleID);

            //PRFT Custom Code : Start
            if (callMessage != null && !string.IsNullOrEmpty(callMessage.ToString()))
            {
                message = callMessage.ToString();
            }
            //PRFT Custom Code : End

            if (Status)
            {
                return message;
            }
            else if (!this._Profile.ShowPrice)
            {
                return message;
            }
            else
            {
                return string.Empty;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Bind Top Reviwed Product List of Brand
        /// </summary>
        private void BindTopReviewedProductList()
        {
            pnlLatestTopReviewedPrd.Visible = false;
            bool isShopByBrandCategory = false;
            isShopByBrandCategory = GetParentCategoryType();
            if (this._category != null && isShopByBrandCategory)
            {
                GetAllReviewList();
                if (this.LatestReviewedProductList != null && this.LatestReviewedProductList.ZNodeProductCollection.Count > 0)
                {
                    locTopReviewedTitle.Text = string.Format(Resources.CommonCaption.TopReviewdProductListTitle.ToString(), _category.Name);
                    if (this.LatestReviewedProductList != null && this.LatestReviewedProductList.ZNodeProductCollection.Count > 0)
                    {
                        pnlLatestTopReviewedPrd.Visible = true;
                        rptTopReviewedProdList.DataSource = this.LatestReviewedProductList.ZNodeProductCollection;
                        rptTopReviewedProdList.DataBind();
                    }
                }
            }
        }

        /// <summary>
        /// Get Review list from cache or database
        /// </summary>
        private void GetAllReviewList()
        {

            this.LatestReviewedProductList = (ZNodeProductList)System.Web.HttpRuntime.Cache["ShopByBrandsReviewList" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId + _category.CategoryID];
            if (this.LatestReviewedProductList == null)
            {
                int maxCount = int.Parse(Resources.CommonCaption.MaxBrandLatestReviewdListCount);
                this.LatestReviewedProductList = ZNodeProductList.GetCategoryTopReviewedProductList(ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId, _category.CategoryID, maxCount);
                //Insert into cache
                ZNodeCacheDependencyManager.Insert("ShopByBrandsReviewList" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId + _category.CategoryID,
                                             this.LatestReviewedProductList,
                                             DateTime.Now.AddHours(1),
                                             System.Web.Caching.Cache.NoSlidingExpiration,
                                             null);
            }

        }

        /// <summary>
        /// Get Parent Category of Current category
        /// </summary>
        /// <returns>bool</returns>
        protected bool GetParentCategoryType()
        {
            bool isShopByBrandCategory = false;
            if (this._category != null && !string.IsNullOrEmpty(this._category.CategoryPath.ToString()) && ConfigurationManager.AppSettings["ShopByBrandCategory"] != null)
            {
                string compareStr = System.Text.RegularExpressions.Regex.Replace(this._category.CategoryPath.ToLower(), "(<[a|A][^>]*>|)", "");
                compareStr = System.Text.RegularExpressions.Regex.Replace(compareStr, "(</[a|A]>|)", "");
                string[] shopByBrandCatName = ConfigurationManager.AppSettings["ShopByBrandCategory"].ToString().Split(',');
                foreach (string currentString in shopByBrandCatName)
                {
                    if (compareStr.ToLower().IndexOf(currentString.ToLower()) >= 0)
                    {
                        isShopByBrandCategory = true;
                        break;
                    }
                }

            }
            return isShopByBrandCategory;
        }
        #endregion
    }
}