﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace WebApp.Controls.Default.Category
{
    public partial class ZBrandsCategoryList : System.Web.UI.UserControl
    {
        #region Private Member
        decimal maxRowItem = 0;
        decimal maxColumnNo = 5;
        int currentRowCount = 0;
        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindBrandLinkTabs();
                BindAllBrandsAlphabetical();
            }

        }

        /// <summary>
        /// Page Prerender event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            RegisterClientScriptClasses();
        }

        /// <summary>
        /// Page Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            ZNodeResourceManager resourceManager = new ZNodeResourceManager();

            // Set SEO Properties
            ZNodeSEO seo = new ZNodeSEO();
            //seo.SEOTitle = resourceManager.GetGlobalResourceObject("SiteMap", "txtAllBrands");
            //PRFT Custom Code : Start
            seo.SEOTitle = resourceManager.GetGlobalResourceObject("SiteMap", "txtAllBrands");
            seo.SEODescription = resourceManager.GetGlobalResourceObject("SiteMap", "AllBrandsDescription").ToString();
            //PRFT Custom Code : End
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Bind All Brands Alphabetically
        /// </summary>
        private void BindAllBrandsAlphabetical()
        {
            StringBuilder sbAllBrands = new StringBuilder();
            DataTable dtBrands = new DataTable();
            string[] brandAlphabets = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z","#" };
            dtBrands = this.GetAllBrandAlphabetical();
            if (dtBrands != null && dtBrands.Rows != null && dtBrands.Rows.Count > 0)
            {
                maxRowItem = decimal.Round(dtBrands.Rows.Count / maxColumnNo) + dtBrands.Rows.Count % maxColumnNo;
                for (int count = 0; count < brandAlphabets.Length; count++)
                {
                    string brandsString = string.Empty;
                    brandsString = FormatBrandListColumnWise(dtBrands.Select("InitialChar ='" + brandAlphabets[count] + "'"), brandAlphabets[count]);
                    if (!string.IsNullOrEmpty(brandsString))
                    {
                        if (brandAlphabets[count].Equals("#")) { brandAlphabets[count] = "Other"; }
                        sbAllBrands.AppendLine(brandsString);
                    }
                }
                ltlAllBrands.Text = sbAllBrands.ToString();
            }
        }

        /// <summary>
        /// Bind All Brand Link Tabs in top of the page
        /// </summary>
        private void BindBrandLinkTabs()
        {
            ltlBrandLinkTab.Text = Resources.CommonCaption.ShopByBrandsTopLinks;
        }

        /// <summary>
        /// Get all brands category list
        /// </summary>
        private DataTable GetAllBrandAlphabetical()
        {
            DataTable dtCategoryList = (DataTable)System.Web.HttpRuntime.Cache["ShopByBrandsCategory" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId];
            string categoryName = ConfigurationManager.AppSettings["ShopByBrandCategory"]!=null?ConfigurationManager.AppSettings["ShopByBrandCategory"].ToString():"Shop by Brand";
            if (dtCategoryList == null)
            {
                CategoryHelper categoryHelper = new CategoryHelper();
                dtCategoryList = categoryHelper.GetBrandCategoryListByPortal(ZNodeConfigManager.SiteConfig.PortalID, ZNodeConfigManager.SiteConfig.LocaleID, categoryName);
                //Insert into cache
                ZNodeCacheDependencyManager.Insert("ShopByBrandsCategory" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId
                                             ,
                                             dtCategoryList,
                                             DateTime.Now.AddHours(1),
                                             System.Web.Caching.Cache.NoSlidingExpiration,
                                             null);
            }

            return dtCategoryList;

        }

        /// <summary>
        /// get brand URL
        /// </summary>
        /// <param name="brandID"></param>
        /// <returns></returns>
        private string GetRedirectURL(object brandID, object seoURL)
        {
            string currentId = brandID != null ? brandID.ToString() : string.Empty;
            string currentUrl = seoURL != null ? seoURL.ToString() : string.Empty;
            string defaultURL = "~/category.aspx?zcid=";
            if (!string.IsNullOrEmpty(currentUrl))
            {
                return currentUrl.ToLower();
            }
            else
            {
                return ResolveUrl(defaultURL + currentId).ToLower();
            }
        }

        /// <summary>
        /// Registers the Script for this page
        /// </summary>
        private void RegisterClientScriptClasses()
        {
            StringBuilder script = new StringBuilder();
            script.Append("var zeonAllBrands = new AllBrands({");
            script.Append("});zeonAllBrands.Init();");
            this.Page.ClientScript.RegisterStartupScript(GetType(), "AllBrands", script.ToString(), true);
        }

        /// <summary>
        /// Get DataRow and Format it as per requirement
        /// </summary>
        /// <param name="brandRows">DataRow[]</param>
        /// <param name="brandAlphabet">string</param>
        /// <returns>string</returns>
        private string FormatBrandListRowWise(DataTable dtBrands)
        {
            StringBuilder sbBrandNames = new StringBuilder();
            int totalChildCount = Convert.ToInt32(dtBrands.Rows.Count);
            int maxColumnNo = 4;
            int currentColNo = 0;
            int currentChildCount = 0;
            foreach (DataRow row in dtBrands.Rows)
            {
                if (currentColNo == 0)
                {
                    sbBrandNames.AppendLine(@"<ul>");
                }
                sbBrandNames.Append(string.Format("<li id={0}>", "lstAlpha-" + row["InitialChar"]));
                string seoURL = string.Empty;// GetRedirectURL(row["CategoryID"], row["SEOURL"]);
                string brandName = string.Empty;
                brandName = row["Name"].ToString();
                sbBrandNames.AppendLine(@"<a title= '" + row["Name"].ToString() + "' href='" + seoURL.Replace("~/", "") + "'>");
                if (!string.IsNullOrEmpty(brandName))
                {
                    sbBrandNames.Append("<div>" + brandName + "</div>");
                }
                sbBrandNames.Append(@"</a>");
                sbBrandNames.AppendLine(@"</li>");

                currentChildCount++;
                currentColNo++;
                if (maxColumnNo == currentColNo)
                {
                    currentColNo = 0;
                    sbBrandNames.AppendLine(@"</ul>");
                }
                else if (totalChildCount == currentChildCount)
                {
                    sbBrandNames.AppendLine(@"</ul>");
                }
            }
            return sbBrandNames.ToString();
        }

        /// <summary>
        /// Get DataRow and Format it as per requirement
        /// </summary>
        /// <param name="brandRows">DataRow[]</param>
        /// <param name="brandAlphabet">string</param>
        /// <returns>string</returns>
        private string FormatBrandListColumnWise(DataRow[] brandRows, string IntialChar)
        {
            StringBuilder sbBrandNames = new StringBuilder();
            int totalChildCount = Convert.ToInt32(brandRows.Count());

            for (int currentItemCount = 0; currentItemCount < totalChildCount; currentItemCount++)
            {
                if (currentRowCount == 0)
                {
                    sbBrandNames.AppendLine(@"<ul class='ShopByBrandColumn'>");
                    if (currentItemCount > 0)
                    {
                        sbBrandNames.AppendLine(@"<ul id='lstAlpha-" + IntialChar + "'>");
                    }
                }
                if (currentItemCount == 0)
                {
                    sbBrandNames.AppendLine(@"<ul id='lstAlpha-" + IntialChar + "'>");
                }

                sbBrandNames.Append("<li>");
                string seoURL = GetRedirectURL(brandRows[currentItemCount]["CategoryID"], brandRows[currentItemCount]["SEOURL"]);
                string brandName = string.Empty;
                brandName = brandRows[currentItemCount]["Name"].ToString();
                sbBrandNames.AppendLine(@"<a title= '" + brandRows[currentItemCount]["Name"].ToString() + "' href='" + seoURL.Replace("~/", "") + "'>");
                if (!string.IsNullOrEmpty(brandName))
                {
                    sbBrandNames.Append("<div>" + brandName + "</div>");
                }
                sbBrandNames.Append(@"</a>");
                sbBrandNames.AppendLine(@"</li>");

                currentRowCount++;
                if (currentRowCount == maxRowItem)
                {
                    currentRowCount = 0;
                    sbBrandNames.AppendLine(@"</ul>");
                    sbBrandNames.AppendLine(@"</ul>");
                }
                else if (currentItemCount == totalChildCount - 1)
                {
                    sbBrandNames.AppendLine(@"</ul>");
                }
            }
            return sbBrandNames.ToString();
        }

        #endregion

    }
}