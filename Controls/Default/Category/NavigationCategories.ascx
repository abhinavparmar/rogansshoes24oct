<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Category_NavigationCategories" CodeBehind="NavigationCategories.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="uc1" %>
<div class="CategoryTreeView" runat="server" id="divCategoryTreeView">
    <div class="Title" onclick="zeonTopSearch.ToggalCategory();">
        <uc1:CustomMessage ID="CustomMessage2" MessageKey="LeftNavigationShopByCategoryTitle" runat="server" ClientIDMode="Static"
            EnableViewState="false" />
    </div>
    <div class="SearchKeys">
        <asp:TreeView ID="ctrlNavigation" runat="server" ExpandDepth="0" CssClass="TreeView" NodeWrap="true" NodeIndent="15" ShowExpandCollapse="False" ClientIDMode="Static" EnableViewState="false">
            <ParentNodeStyle CssClass="ParentNodeStyle" />
            <HoverNodeStyle CssClass="HoverNodeStyle" />
            <SelectedNodeStyle CssClass="SelectedNodeStyle" />
            <RootNodeStyle CssClass="RootNodeStyle" />
        </asp:TreeView>
    </div>
</div>
