<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Category_ProductList"
    CodeBehind="ProductList.ascx.cs" %>
<%@ Register Src="~/Controls/Default/Reviews/ProductAverageRating.ascx" TagName="ProductAverageRating"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage"
    TagPrefix="uc1" %>

<asp:Panel ID="pnlProductList" runat="server" Visible="False" meta:resourcekey="pnlProductListResource1" ClientIDMode="Static">
   
    <div class="ProductList">
       
        <div class="CategorySpecificBanners">
            <asp:Literal ID="ltrCategorySpecificBanner" EnableViewState="false" runat="server" ClientIDMode="Static"></asp:Literal>
        </div>
        <%--<asp:Label ID="Label1" runat="server" meta:resourcekey="Label1Resource1" ClientIDMode="Static"></asp:Label>--%>
        <asp:Label ID="ErrorMsg" EnableViewState="false" runat="server" CssClass="Error" meta:resourcekey="ErrorMsgResource1" ClientIDMode="Static"></asp:Label>

        <div id="ProductListNavigation">
            <div class="TopPagingSection">
                <div class="NarrowResults">
                    <a href="#">
                        <asp:Literal ID="ltlNarrowTitle" EnableViewState="false" Text="NARROW RESULTS" runat="server"></asp:Literal></a>
                </div>
                <div class="Sorting">

                    <span class="PageResult">
                        <strong>
                            <asp:Literal ID="ltrTopTotalCounts" meta:resourcekey="ltrTotalCountresource" runat="server" EnableViewState="false"></asp:Literal></strong>
                        &nbsp;&nbsp;<asp:Literal ID="ltrTopTotalResults" meta:resourcekey="ltrTotalResultsresource" runat="server" EnableViewState="false"></asp:Literal>
                    </span>&nbsp;&nbsp; 
                    <div class="filter" id="NarrowResult"><a href="javascript:void(0)" onclick="objFacets.ShowFacets()">Narrow Results</a></div>
                    <div class="PageSort">
                        <span class="Label">
                            <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:CommonCaption, Sort%>" ClientIDMode="Static" EnableViewState="false"></asp:Localize>
                        </span>
                        <asp:DropDownList ID="lstFilter" runat="server" OnSelectedIndexChanged="LstFilter_SelectedIndexChanged"
                            AutoPostBack="false" ClientIDMode="Static" onchange="zeonCatProdList.ApplySortByPageNarrowResult(this);" title="SORT BY">
                            <asp:ListItem Value="1" meta:resourcekey="ListItemResource1">Relevance</asp:ListItem>
                            <asp:ListItem Value="4" meta:resourcekey="ListItemResource4">Product A-Z</asp:ListItem>
                            <asp:ListItem Value="5" meta:resourcekey="ListItemResource5">Product Z-A</asp:ListItem>
                            <asp:ListItem Value="6" meta:resourcekey="ListItemResource6">Highest Rated</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="PageShow">
                        <span class="label">
                            <asp:Literal ID="ltrTopShow" runat="server" EnableViewState="false" meta:resourceKey="ltrShowresource"></asp:Literal>
                        </span>
                        <asp:DropDownList ID="ddlTopPaging" runat="server" AutoPostBack="false" CssClass="Pagingdropdown" onchange="zeonCatProdList.ApplyShowNarrowResult(this);"
                            meta:resourcekey="ddlTopPagingResource1" OnSelectedIndexChanged="DdlTopPaging_SelectedIndexChanged" ClientIDMode="Static" title="Top Paging">
                        </asp:DropDownList>
                    </div>
                    <div class="PageNavArrow">
                        <span class="label">PAGE</span>
                        <asp:HyperLink ID="hlTopPrevLink" EnableViewState="false" CssClass="Button" meta:resourcekey="hlkPrviLink" runat="server" ClientIDMode="Static"></asp:HyperLink>
                        <span>
                            <asp:Localize ID="locTopPageNum" runat="server" ClientIDMode="Static" EnableViewState="false">
                            </asp:Localize>&nbsp;&nbsp;
                        </span>
                    </div>
                    <asp:HyperLink EnableViewState="false" ID="hlTopNextLink" CssClass="Button" meta:resourcekey="hlkNextiLink" runat="server" ClientIDMode="Static"></asp:HyperLink>
                    <span>
                        <asp:Localize ID="locTopPageTotal" runat="server" ClientIDMode="Static" EnableViewState="false">
                        </asp:Localize></span>

                </div>
                <div class="TopPaging" runat="server" visible="false" enableviewstate="false">
                    <div style="float: left">
                        <asp:LinkButton ID="imgbtnGrid" CssClass="ActiveLink GridView" runat="server" Visible="False" Enabled="False" ToolTip="Grid" OnClick="imgbtnGrid_OnClick" ClientIDMode="Static"></asp:LinkButton>
                        <asp:ImageButton ID="imgGrid1" runat="server" AlternateText="Grid" CssClass="ActiveLink" ImageUrl="/Themes/Default/Images/grid-active.gif" ToolTip="Grid" OnClick="imgbtnGrid_OnClick" ClientIDMode="Static" Visible="false" />
                        &nbsp;&nbsp;
                    <asp:LinkButton ID="imgbtnList" CssClass="DeActiveLink ListView" runat="server" Visible="False" ToolTip="List" OnClick="imgbtnList_OnClick" ClientIDMode="Static"></asp:LinkButton>
                        <asp:ImageButton ID="imgListActive1" AlternateText="List" ToolTip="List" ImageUrl="/Themes/Default/Images/list.gif" runat="server" CssClass="DeActiveLink" OnClick="imgbtnList_OnClick" ClientIDMode="Static" Visible="false" />
                    </div>
                </div>
                <div>
                    <asp:HiddenField ID="hdnCompareProductCount" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnTotalComparableProducts" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnPreviousCategory" runat="server" ClientIDMode="Static" />
                </div>
            </div>
        </div>
        <div class="CategoryProductlist">
            <asp:DataList ID="DataListProducts" runat="server" RepeatDirection="Horizontal"
                OnItemDataBound="DataListProducts_ItemDataBound" RepeatLayout="Flow"
                meta:resourcekey="DataListProductsResource1" EnableViewState="false">
                <ItemStyle CssClass="ItemStyle" />
                <ItemTemplate>
                    <div class="ProductListItem">
                        <asp:HiddenField ID="hdnPID" EnableViewState="false" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "ProductID") %>'></asp:HiddenField>
                        <div class="Image">
                            <div class="ItemType">
                                <%--<asp:Image ID="NewItemImage" EnableViewState="false" runat="server" ImageUrl='<%#ResolveUrl(ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("new.png")) %>'
                                    meta:resourcekey="NewItemImageResource1" Visible='<%#DataBinder.Eval(Container.DataItem, "NewProductInd")%>' ClientIDMode="Static" AlternateText="New" alt="New" />--%>
                                <span class="rs-new" id="spnRSNew" runat="server" Visible='<%#DataBinder.Eval(Container.DataItem, "NewProductInd")%>'><span class="rs-new-text">New</span></span>
                                &nbsp;<asp:Image EnableViewState="false" ID="FeaturedItemImage" runat="server" ImageUrl='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("sale.png") %>'
                                    meta:resourcekey="FeaturedItemImageResource1" Visible='<%# DataBinder.Eval(Container.DataItem, "FeaturedInd") %>' ClientIDMode="Static" AlternateText="Featured" ToolTip="Featured" />
                            </div>
                            <%--  <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#ResolveUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>' ClientIDMode="Static">
                                <img id='<%# "Img" + DataBinder.Eval(Container.DataItem, "ProductID")%>' alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag") %>'
                                    border="0" src='<%# ResolveUrl(DataBinder.Eval(Container.DataItem, "SmallImageFilePath").ToString()) %>' title='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag") %>'
                                    style="vertical-align: bottom;"></img>
                            </asp:HyperLink>--%>
                            <a href='<%# ResolveUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>'>
                                <img id='Img<%#((ZNode.Libraries.ECommerce.Catalog.ZNodeProductBase)Container.DataItem).ProductID %>'
                                    alt="<%# ((ZNode.Libraries.ECommerce.Catalog.ZNodeProductBase)Container.DataItem).ImageAltTag %>"
                                    title="<%# ((ZNode.Libraries.ECommerce.Catalog.ZNodeProductBase)Container.DataItem).ImageAltTag %>"
                                    src="images/grey1px.gif" class="lazy" data-original='<%# ResolveUrl(((ZNode.Libraries.ECommerce.Catalog.ZNodeProductBase)Container.DataItem).SmallImageFilePath) %>' />
                            </a>
                            <div class="AddViewProduct">
                                <div class="HoverBox"></div>
                            </div>
                            <div class="CompareBox">
                                <%--<asp:CheckBox ID="chkCompareProduct" data-pid='<%# DataBinder.Eval(Container.DataItem, "ProductID").ToString() %>'
                                data-catid='<%# this._categoryId %>' runat="server" EnableViewState="false" onchange="zeonCatProdList.CheckBoxChange(this);" />
                            <div class="SubCompare">
                                <asp:LinkButton ID="lbtnCompare" runat="server" meta:resourceKey="lbtnCompareResource1" EnableViewState="false"
                                    CssClass="CompareButton" OnClick="btnCompareProductList_Click" ClientIDMode="Static"></asp:LinkButton>
                            </div>--%>
                                <asp:LinkButton ID="btnCompareProduct" runat="server" ToolTip='<%# "Compare "+ DataBinder.Eval(Container.DataItem, "Name").ToString() %>' EnableViewState="false" data-catid='<%# this._categoryId %>'
                                    data-pid='<%# DataBinder.Eval(Container.DataItem, "ProductID").ToString() %>'
                                    OnClick="btnCompareProduct_Click"
                                    OnClientClick="return zeonCatProdList.AddProductToCompareList(this);" alt="Compare">Compare</asp:LinkButton>
                            </div>
                            <div class="QuickViewLink">
                                <div class="SubCompare">
                                    <%--<a href="javascript:void(0)" onclick="return quickViewHelper.SetSrcInframe(<%# DataBinder.Eval(Container.DataItem, "ProductID").ToString() %>)">Quick View</a>--%>
                                </div>
                            </div>
                        </div>
                        <div class="ProductSwatches">
                            <div class="ColorCaption">
                                <a href='<%# ResolveUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>' title='<%# "More Colors Available "+ DataBinder.Eval(Container.DataItem, "Name").ToString() %>' id="lnkColorCaption" class="ColorPicker" runat="server" visible='<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "ProductSKUCount")) > 1 %>'>MORE COLORS AVAILABLE</a>
                            </div>
                            <%-- <ZNode:Swatches ID="uxProductSwatches" runat="server" ControlType="Skus" IncludeProductImage="true" ClientIDMode="Static"
                                ProductId='<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "ProductSKUCount")) > 0? DataBinder.Eval(Container.DataItem, "ProductID"):0 %>'
                                ZNodeSKUImageCollection='<%#((ZNode.Libraries.ECommerce.Catalog.ZNodeProductBase)Container.DataItem).ZNodeSKUImageCollection %>' />--%>
                        </div>
                        <div class="StarRating">
                            <ZNode:ProductAverageRating ID="uxProductAverageRating" ProductName='<%# DataBinder.Eval(Container.DataItem, "Name").ToString() %>' TotalReviews='<%# DataBinder.Eval(Container.DataItem, "TotalReviews") %>'
                                ViewProductLink='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink")%>'
                                ReviewRating='<%# DataBinder.Eval(Container.DataItem, "ReviewRating")%>' runat="server" EnableViewState="false" />
                        </div>
                        <div class="DetailLink">
                            <asp:HyperLink ID="HyperLink2" runat="server" ToolTip='<%# DataBinder.Eval(Container.DataItem, "Name").ToString() %>' NavigateUrl='<%#ResolveUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>'>
                                <%# DataBinder.Eval(Container.DataItem, "Name").ToString() %></asp:HyperLink>
                        </div>
                        <div class="Price">
                            <asp:Label ID="Label2" EnableViewState="false" runat="server" meta:resourcekey="Label2Resource1"
                                Text='<%# DataBinder.Eval(Container.DataItem, "FormattedPrice") %>' Visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && ProductListProfile.ShowPrice %>'></asp:Label>
                            <%--<asp:Label ID="lblERPPrice" runat="server" EnableViewState="false" Text="Loading.."
                                Visible='<%# !(bool)((ZNode.Libraries.ECommerce.Catalog.ZNodeProductBase)Container.DataItem).CallForPricing && ProductListProfile.ShowPrice %>'></asp:Label>--%>
                        </div>
                        <%--   <div class="ShortDescription">
                            <asp:Label ID="ShortDescription" runat="server" Text='<%# WebApp.ZCommonHelper.GetFormattedProductName(((ZNode.Libraries.ECommerce.Catalog.ZNodeProductBase)Container.DataItem).ShortDescription.ToString(), System.Configuration.ConfigurationManager.AppSettings["ProductGridNameMaxLimit"].ToString())%>' meta:resourcekey="ShortDescriptionResource1" EnableViewState="false" ClientIDMode="Static" Visible='<%#SetShortDescriptionVisibility()%>' />
                        </div>--%>
                        <br />
                        <%--<div class="GrayBorder">
                            <ZNode:Spacer ID="Spacer1" EnableViewState="false" runat="server" SpacerHeight="1"
                                SpacerWidth="1" ClientIDMode="Static" />
                        </div>--%>
                        <div class="CallForPrice">
                            <asp:Label ID="uxCallForPricing" EnableViewState="false" runat="server" meta:resourcekey="uxCallForPricingResource1"
                                Text='<%# CheckForCallForPricing(DataBinder.Eval(Container.DataItem, "CallForPricing"), DataBinder.Eval(Container.DataItem, "CallMessage")) %>'></asp:Label>
                        </div>
                        <div class="ItemNumber" style="display: none">
                            <asp:Label ID="lblItemHash" runat="server" Text='<%# ((ZNode.Libraries.ECommerce.Catalog.ZNodeProductBase)Container.DataItem).ProductID %>'
                                EnableViewState="false" Style="font-weight: normal" />
                        </div>
                    </div>
                </ItemTemplate>
            </asp:DataList>
        </div>
        <div class="BottomPaging">
            <div class="Paging">
                <span class="Sorting">
                    <strong>
                        <asp:Literal ID="ltrBottomTotalCount" meta:resourcekey="ltrTotalCountresource" ClientIDMode="Static" runat="server" EnableViewState="false"></asp:Literal></strong>
                    &nbsp;&nbsp;<asp:Literal ID="ltrBottomTotalResult" ClientIDMode="Static" meta:resourcekey="ltrTotalResultsresource" runat="server" EnableViewState="false"></asp:Literal></span>
                <div class="PageSort">
                    <span class="Label">
                        <asp:Localize ID="locBottomSort" EnableViewState="false" runat="server" Text="<%$ Resources:CommonCaption, Sort%>" ClientIDMode="Static"></asp:Localize>
                    </span>
                    <asp:DropDownList ID="lstFilterBottom" runat="server" OnSelectedIndexChanged="LstFilterBottom_SelectedIndexChanged" onchange="zeonCatProdList.ApplySortByPageNarrowResult(this);"
                        AutoPostBack="false" ClientIDMode="Static" title="SORT BY">
                        <asp:ListItem Value="1" meta:resourcekey="ListItemResource1">Relevance</asp:ListItem>
                        <asp:ListItem Value="4" meta:resourcekey="ListItemResource4">Product A-Z</asp:ListItem>
                        <asp:ListItem Value="5" meta:resourcekey="ListItemResource5">Product Z-A</asp:ListItem>
                        <asp:ListItem Value="6" meta:resourcekey="ListItemResource6">Highest Rated</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="PageShow">
                    <span class="label">
                        <asp:Literal ID="ltrBottomShow" EnableViewState="false" meta:resourceKey="ltrShowresource" runat="server"></asp:Literal></span>
                    <asp:DropDownList runat="server" ID="ddlBottomPaging" CssClass="Pagingdropdown" AutoPostBack="false" onchange="zeonCatProdList.ApplyShowNarrowResult(this);"
                        OnSelectedIndexChanged="DdlBottomPaging_SelectedIndexChanged" meta:resourcekey="ddlBottomPagingResource1" ClientIDMode="Static" title="Bottom Pager">
                    </asp:DropDownList>
                </div>
                <div class="PageNavArrow">
                    <span class="label">PAGE</span>
                    <asp:HyperLink ID="hlBotPrevLink" CssClass="Button" meta:resourcekey="hlkPrviLink" runat="server" ClientIDMode="Static" EnableViewState="false"></asp:HyperLink>
                    <span class="PageShow" style="display: none;"></span>
                    <span>
                        <asp:Localize ID="locBottomPageNum" runat="server" ClientIDMode="Static" EnableViewState="false"></asp:Localize>&nbsp;&nbsp;</span>
                </div>
                <asp:HyperLink ID="hlBotNextLink" meta:resourcekey="hlkNextiLink" CssClass="Button" runat="server" ClientIDMode="Static" EnableViewState="false"></asp:HyperLink>
                <span>
                    <asp:Localize ID="locBottomPageTotal" runat="server" ClientIDMode="Static" EnableViewState="false"></asp:Localize></span>
            </div>
        </div>
    </div>
    <div id="divMaxProductLimitReached" runat="server" class="DilogModal" style="display: none;" enableviewstate="false">
        <asp:Label ID="lblMaxProductWarning" EnableViewState="false" runat="server" meta:resourceKey="lblMaxProductWarningResource1" ClientIDMode="Static"></asp:Label>
    </div>
    <div id="divAlreadyProductExist" runat="server" class="DilogModal" style="display: none;" enableviewstate="false">
        <asp:Label ID="lblAlreadyProductExist" EnableViewState="false" runat="server" meta:resourceKey="lblAlreadyProductExistResource1" ClientIDMode="Static"></asp:Label>
    </div>
    <div id="divCategoryChangedWarning" runat="server" class="DilogModal" style="display: none;" enableviewstate="false">
        <asp:Label ID="lblCategoryChangedWarning" EnableViewState="false" runat="server" meta:resourceKey="lblCategoryChangedWarningResource1" ClientIDMode="Static"></asp:Label>
    </div>
    <div id="divAddToCompareFailed" runat="server" class="DilogModal" style="display: none;" enableviewstate="false">
        <asp:Label ID="lblAddToCompareFailed" EnableViewState="false" runat="server" meta:resourceKey="lblAddToCompareFailedResource1" ClientIDMode="Static"></asp:Label>
    </div>
    <div id="divAddMoreItems" runat="server" class="DilogModal" style="display: none;" enableviewstate="false">
        <asp:Label ID="lblAddMoreItems" EnableViewState="false" runat="server" meta:resourceKey="lblProductAddedResource1" ClientIDMode="Static"></asp:Label>
        <div style="display: none;">
            <asp:Label ID="lblAddMoreItemsTemp" EnableViewState="false" runat="server" meta:resourceKey="lblProductAddedResource2" ClientIDMode="Static"></asp:Label>
        </div>
    </div>
    <div id="divQuickWatch" style="display: none;">
        <iframe id="ifzQuickView" title="quickview" runat="server" style="border-style: none; border: 0px; background-color: transparent; width: 100%; min-height: 100%; overflow: hidden;"
            ></iframe>
    </div>
</asp:Panel>

