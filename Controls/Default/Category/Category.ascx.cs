using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using System.Collections.Generic;
using ZNode.Libraries.Ecommerce.Entities;
using ZNode.Libraries.ECommerce.SEO;
using System.Text;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.DataAccess.Entities;
using System.Linq;

namespace WebApp
{
    /// <summary>
    /// Represents the Category user control class.
    /// </summary>
    public partial class Controls_Default_Category_Category : System.Web.UI.UserControl
    {
        #region Member Variables
        private int _categoryId;
        private ZNodeCategory _category;

        //Zeon Custom Code: Start
        //const string querystringparams = "category,text,page,size,sort";
        const string querystringparams = "category,text,page,s,sort,v";
        private string _viewtype = string.Empty;
        private int showSubCatPrdList = 0;
        //Zeon Custom COde: End
        #endregion

        #region Page Load

        protected void Page_Init(object sender, EventArgs e)
        {
            // Get category id from querystring  
            if (Request.Params["zcid"] != null)
            {
                this._categoryId = int.Parse(Request.Params["zcid"]);
            }
            else
            {
                throw new ApplicationException("Invalid Category Request");
            }
            //Zeon Custom Code: Start
            if (Request.Params["v"] != null)
            {
                _viewtype = Request.Params["v"].ToString();
            }
            else
            {
                _viewtype = "g";

            }
            //Zeon Custom Code: End

            // Retrieve category data from httpContext
            this._category = (ZNodeCategory)HttpContext.Current.Items["Category"];

            if (!Page.IsPostBack)
            {
                //Zeon Custom Code : Start
                if (this._category != null && this._category.CategoryID > 0)
                {
                    this.DisplayCategoryList();
                    ShowCategoryBanner(this._category.CategoryID);
                    int.TryParse(this._category.Custom2, out showSubCatPrdList);
                    if (ZCommonHelper.IsCustomMulitipleAttributePortal())
                    {
                        if (IsSubCategory() == false || showSubCatPrdList == 1)
                        {
                            CustomLoadIndex(HttpUtility.HtmlDecode(this._category.Name));
                        }
                    }
                    else
                    {
                        if (IsSubCategory() == false || showSubCatPrdList == 1 || Convert.ToBoolean(this._category.ISShowSubCategory) == false)
                        {
                            CustomLoadIndex(HttpUtility.HtmlDecode(this._category.Name));
                        }
                    }
                }
                //Zeon Custom Code : End
            }

            //Zeon Custom Code : Start
            //LoadIndex(HttpUtility.HtmlDecode(this._category.Name));

            // Facets.UpdatePageHandler += UpdatePage;          
            ucFacets.UpdatePageHandler += UpdatePage;
            ucLeftProductCompare.Visible = ucLeftProductCompare.NoProductsForComparison();
            //Zeon Custom Code : End

            // Code disables caching by browser.
            //ClearCacheFromTheBrowser();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Bind All Controls to Category Data
        /// </summary>
        protected void Bind()
        {
            // Bind title         
            CategoryTitle.Text = string.Format(CategoryTitle.Text, this._category.ShortDescription);
            if (!(string.IsNullOrEmpty(this._category.Description)) && this._category.Description.Length > 0)
            {
                pnlCategoryDescription.Visible = true;
                //CategoryDescription.Text = "<div class='CategoryInformation'>" + this._category.Description + "</div>"; 
                CategoryDescription.Text = this._category.Description;
                pnlMobileCategoryDesc.Visible = true;
                //lblMobileCategoryDesc.Text = "<div class='CategoryInformation'>" + this._category.Description + "</div>"; 
                lblMobileCategoryDesc.Text = this._category.Description;
            }
            else
            {
                pnlCategoryDescription.Visible = false;
                pnlMobileCategoryDesc.Visible = false;
            }
            //AdditionalDescription.Text = this._category.AlternateDescription;
        }

        public void UpdatePage(string facetquery)
        {
            string baseUrl = ZNodeSEOUrl.MakeURL(_category.CategoryID.ToString(), SEOUrlType.Category, _category.SEOURL).ToLower() + (string.IsNullOrEmpty(_category.SEOURL) ? "&" : "?");
            int sort = (!string.IsNullOrEmpty(Request.QueryString["sort"])) ? Convert.ToInt32(Request.QueryString["sort"]) : 1;
            // Do not add the & or ? in the following query string.
            string navigateUrl = string.Format("{0}category={1}&text={2}&page={3}&s={4}&sort={5}", baseUrl, HttpUtility.UrlEncode(this._category.Name), string.Empty, 1, uxCategoryProductList.PageSize, sort);
            //string navigateUrl = string.Format("{0}category={1}&text={2}&page={3}&size={4}&sort={5}&v={6}", baseUrl, HttpUtility.UrlEncode(this._category.Name), string.Empty, uxCategoryProductList.CurrentPage, uxCategoryProductList.PageSize, 1, _viewtype); //Zeon Custom Code
            Response.Redirect(navigateUrl + facetquery);

        }

        /// <summary>
        /// Loads the data from the search engine
        /// </summary>
        /// <param name="Category"></param>
        private void LoadIndex(string Category)
        {
            int sort = (!string.IsNullOrEmpty(Request.QueryString["sort"])) ? Convert.ToInt32(Request.QueryString["sort"]) : 0;
            //var cacheKey = string.Format("{0}_{1}_{2}_{3}_{4}_{5}", "CategoryPage", _category.CategoryID, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId, ZNodeProfile.CurrentUserProfileId, sort);
            var cacheKey = string.Format("{0}_{1}_{2}_{3}_{4}_{5}_{6}", "CategoryPage", _category.CategoryID, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId, ZNodeProfile.CurrentUserProfileId, sort, _viewtype);//Zeon Custom Code
            var result = HttpContext.Current.Cache[cacheKey] as ZNodeSearchEngineResult;

            string searchText = string.Empty;
            var RefinedFacets = new List<KeyValuePair<string, IEnumerable<string>>>();

            foreach (var item in Request.QueryString.Keys)
            {
                if (item.ToString().Contains("zcid"))
                {
                    continue;
                }
                string itemString = item.ToString();
                if (!querystringparams.Contains(item.ToString().ToLower()))
                {
                    RefinedFacets.Add(new KeyValuePair<string, IEnumerable<string>>(itemString, Request.QueryString.GetValues(itemString)));
                }
            }

            // if cache is empty or if facets selection, then do search.
            if (result == null || RefinedFacets.Count > 0)
            {
                ZNodeSearchEngine search = new ZNodeSearchEngine();
                result = search.Search(Category, searchText, RefinedFacets, sort);

                // update cache for category page without facets
                if (RefinedFacets.Count == 0)
                {
                    ZNodeCacheDependencyManager.Insert(cacheKey, result, "ZNodeCategoryNode", "ZNodeCategory", "ZNodePortalCatalog", "ZNodeProductCategory", "ZNodeLuceneIndexServerStatus");
                }
            }

            if (result != null)
            {
                var resultFacet = HttpContext.Current.Cache[cacheKey] as ZNodeSearchEngineResult;
                if (result.Facets != null && !IsSubCategory()) //Zeon Custom Code : Restricted facets to get bound on Subcategory
                {
                    ucFacets.Facetslist = result.Facets;
                    ucFacets.BindFacets();
                }
                if (result.CategoryNavigation != null)
                {
                    //Categories.CategoryNavigation = result.CategoryNavigation; //Zeon Custom Code
                }
                if (result.ProductIDs != null && !IsSubCategory()) //Zeon Custom Code : Restricted Products to get bound on Subcategory
                {
                    uxCategoryProductList.ProductID = result.ProductIDs;
                    uxCategoryProductList.DataBind();
                }
            }
        }
        #endregion

        #region Zeon Custom Methods and Events

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (IsSubCategory())
            {
                RegisterClientScriptClasses();
            }
        }

        /// <summary>
        /// To clear the cache  from the browser
        /// </summary>
        //public  void ClearCacheFromTheBrowser()
        //{
        //    Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
        //    Response.Cache.SetNoStore();
        //}

        /// <summary>
        /// Show Category Banner 
        /// </summary>
        /// <param name="catID"></param>
        private void ShowCategoryBanner(int catID)
        {
            //CategoryExtnService catService = new CategoryExtnService();
            //TList<CategoryExtn> categoryList = catService.GetByCategoryID(catID);

            //if (categoryList != null && categoryList.Count > 0)
            //{
            //    ltrCategorySpecificBanner.Text = categoryList[0].CategoryBanner;
            //}
            ltrCategorySpecificBanner.Text = this._category.CategoryBanner;
        }

        /// <summary>
        /// Display Sub Category List
        /// </summary>
        private void DisplayCategoryList()
        {
            int.TryParse(this._category.Custom2, out showSubCatPrdList);
            if (ZCommonHelper.IsCustomMulitipleAttributePortal())
            {
                if ((!IsSubCategory() || showSubCatPrdList == 1) )
                {
                   ShowCategoryProductList();
                }
                else
                {
                    uxSubCategoryList.Visible = true;
                    uxCategoryProductList.Visible = false;
                    ProductDetails.Visible = false;
                    ucFacets.Visible = false;
                }
            }
            else
            {
                if((!IsSubCategory() || showSubCatPrdList == 1) || Convert.ToBoolean(this._category.ISShowSubCategory) == false)
                {
                    ShowCategoryProductList();
                }
                else
                {
                    uxSubCategoryList.Visible = true;
                    uxCategoryProductList.Visible = false;
                    ProductDetails.Visible = false;
                    ucFacets.Visible = false;
                }
            }
            //if ((!IsSubCategory() || showSubCatPrdList == 1) || Convert.ToBoolean(this._category.ISShowSubCategory) == false)
            //{
            //    ShowCategoryProductList();
            //}
            //else
            //{
            //    uxSubCategoryList.Visible = true;
            //    uxCategoryProductList.Visible = false;
            //    ProductDetails.Visible = false;
            //    ucFacets.Visible = false;
            //}
        }

        /// <summary>
        /// Check wheather Sub Category Exits or not
        /// </summary>
        /// <returns>return true if exits else false</returns>ee
        private bool IsSubCategory()
        {
            if (this._category.SubCategoryGridVisibleInd)
            {
                if (this._category.ZNodeCategoryCollection.Count == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Get Refined facets Product List 
        /// </summary>
        /// <param name="selectedFacets">List<KeyValuePair<string, IEnumerable<string>>></param>
        /// <param name="facetList"> List<ZNodeFacet></param>
        /// <returns> List<int></returns>
        private List<int> GetSelectedFacetsProductList(List<KeyValuePair<string, IEnumerable<string>>> selectedFacets, List<ZNodeFacet> facetList)
        {
            List<int> selectedProducts = new List<int>();

            if (facetList != null && selectedFacets != null)
            {
                foreach (KeyValuePair<string, IEnumerable<string>> facet in selectedFacets)
                {
                    ZNodeFacet currentFacet = facetList.Find(x => x.AttributeName.Split('|')[0].ToLower().Equals(facet.Key.ToString().ToLower()));
                    if (currentFacet != null)
                    {
                        using (var refinedFacet = facet.Value.GetEnumerator())
                        {
                            while (refinedFacet.MoveNext())
                            {
                                if (refinedFacet.Current.ToString().IndexOf('^') > 0)
                                {
                                    string[] selFac = refinedFacet.Current.ToString().Split('^');

                                    foreach (string selAttr in selFac)
                                    {
                                        ZNodeFacetValue selectedAttribute = currentFacet.AttributeValues.Find(y => y.AttributeValue.Equals(selAttr));
                                        if (selectedAttribute != null && selAttr.Equals(selectedAttribute.AttributeValue))
                                        {
                                            selectedProducts.AddRange(selectedAttribute.ProductIDList);
                                        }
                                    }
                                }
                                else
                                {
                                    ZNodeFacetValue selectedAttribute = currentFacet.AttributeValues.Find(y => y.AttributeValue.Equals(refinedFacet.Current.ToString()));
                                    if (selectedAttribute != null && refinedFacet.Current.ToString().Equals(selectedAttribute.AttributeValue))
                                    {
                                        selectedProducts.AddRange(selectedAttribute.ProductIDList);
                                    }
                                }
                            }
                        }
                    }
                }

            }
            return selectedProducts.Distinct().ToList();
        }

        /// <summary>
        /// Loads the data from the search engine
        /// </summary>
        /// <param name="Category"></param>
        private void CustomLoadIndex(string Category)
        {
            int sort = (!string.IsNullOrEmpty(Request.QueryString["sort"])) ? Convert.ToInt32(Request.QueryString["sort"]) : 0;
            var cacheKey = string.Format("{0}_{1}_{2}_{3}", "CategoryPage", _category.CategoryID, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);//Zeon Custom Code
            string customQueryStringParam = ConfigurationManager.AppSettings["SEOQuerystringKeys"] != null ? querystringparams + ConfigurationManager.AppSettings["SEOQuerystringKeys"].ToString() : querystringparams;
            var result = HttpContext.Current.Cache[cacheKey] as ZNodeSearchEngineResult;
            string searchText = string.Empty;
            var RefinedFacets = new List<KeyValuePair<string, IEnumerable<string>>>();
            string RifinedProductList = string.Empty;

            foreach (var item in Request.QueryString.Keys)
            {
                if (item == null)
                {
                    continue;
                }
                else if (item.ToString().Contains("zcid"))
                {
                    continue;
                }
                string itemString = item.ToString();
                if (!customQueryStringParam.ToLower().Split(',').Contains(item.ToString().ToLower()))
                {
                    RefinedFacets.Add(new KeyValuePair<string, IEnumerable<string>>(itemString, Request.QueryString.GetValues(itemString)));
                }
            }
            // if cache is empty, then do search.
            if (result == null)
            {
                ZNodeSearchEngine search = new ZNodeSearchEngine();
                //Always send Empty Selected facet list to bring all facets
                var emptyList = new List<KeyValuePair<string, IEnumerable<string>>>();
                result = search.Search(Category, searchText, emptyList, sort);

                //ZNodeCacheDependencyManager.Insert(cacheKey,result, System.DateTime.UtcNow.AddHours(24), System.Web.Caching.Cache.NoSlidingExpiration, null);
                //// update cache for category page without facets
                //if (RefinedFacets.Count == 0)
                //{
                ZNodeCacheDependencyManager.Insert(cacheKey, result, "ZNodeCategoryNode", "ZNodeCategory", "ZNodePortalCatalog", "ZNodeProductCategory", "ZNodeLuceneIndexServerStatus");
                //}
            }

            if (result != null)
            {
                int.TryParse(this._category.Custom2, out showSubCatPrdList);
                var resultFacet = HttpContext.Current.Cache[cacheKey] as ZNodeSearchEngineResult;
                if (ZCommonHelper.IsCustomMulitipleAttributePortal())
                {
                    if (result.Facets != null && ((!IsSubCategory() || showSubCatPrdList == 1)))
                    {
                        ucFacets.Facetslist = result.Facets;
                        ucFacets.CurrentSelFacetProductList = RefinedFacets != null && RefinedFacets.Count > 0 ? GetCurrentFacetProductList(RefinedFacets, result.Facets, result.ProductIDs) : result.ProductIDs;
                        ucFacets.SelectedProductList = GetSelectedFacetsSearchProductList(RefinedFacets, result.Facets);//Assign Selected Product List
                        ucFacets.BindFacets();
                    }
                    if (result.ProductIDs != null && ((!IsSubCategory() || showSubCatPrdList == 1)))
                    {
                        ltrSearchResultMessage.Visible = false;
                        uxCategoryProductList.ProductID = RefinedFacets.Count == 0 ? result.ProductIDs : GetSelectedFacetsSearchProductList(RefinedFacets, result.Facets);//Display Selected Facets Product List;
                        if (uxCategoryProductList.ProductID.Count <= 0) { ltrSearchResultMessage.Text = this.GetLocalResourceObject("ltrSearchResultMessageResource").ToString(); ltrSearchResultMessage.Visible = true; }
                        uxCategoryProductList.DataBind();
                    }
                }
                else
                {
                    if (result.Facets != null && ((!IsSubCategory() || showSubCatPrdList == 1) || Convert.ToBoolean(this._category.ISShowSubCategory) == false))
                    {
                        ucFacets.Facetslist = result.Facets;
                        ucFacets.CurrentSelFacetProductList = RefinedFacets != null && RefinedFacets.Count > 0 ? GetCurrentFacetProductList(RefinedFacets, result.Facets, result.ProductIDs) : result.ProductIDs;
                        ucFacets.SelectedProductList = GetSelectedFacetsSearchProductList(RefinedFacets, result.Facets);//Assign Selected Product List
                        ucFacets.BindFacets();
                    }
                    if (result.ProductIDs != null && ((!IsSubCategory() || showSubCatPrdList == 1) || Convert.ToBoolean(this._category.ISShowSubCategory) == false))
                    {
                        ltrSearchResultMessage.Visible = false;
                        uxCategoryProductList.ProductID = RefinedFacets.Count == 0 ? result.ProductIDs : GetSelectedFacetsSearchProductList(RefinedFacets, result.Facets);//Display Selected Facets Product List;
                        if (uxCategoryProductList.ProductID.Count <= 0) { ltrSearchResultMessage.Text = this.GetLocalResourceObject("ltrSearchResultMessageResource").ToString(); ltrSearchResultMessage.Visible = true; }
                        uxCategoryProductList.DataBind();
                    }
                }
                //if (result.Facets != null && ((!IsSubCategory() || showSubCatPrdList == 1) || Convert.ToBoolean(this._category.ISShowSubCategory) == false))
                //{
                //    ucFacets.Facetslist = result.Facets;
                //    ucFacets.CurrentSelFacetProductList = RefinedFacets != null && RefinedFacets.Count > 0 ? GetCurrentFacetProductList(RefinedFacets, result.Facets, result.ProductIDs) : result.ProductIDs;
                //    ucFacets.SelectedProductList = GetSelectedFacetsSearchProductList(RefinedFacets, result.Facets);//Assign Selected Product List
                //    ucFacets.BindFacets();
                //}
                //if (result.ProductIDs != null && ((!IsSubCategory() || showSubCatPrdList == 1) || Convert.ToBoolean(this._category.ISShowSubCategory) == false))
                //{
                //    ltrSearchResultMessage.Visible = false;
                //    uxCategoryProductList.ProductID = RefinedFacets.Count == 0 ? result.ProductIDs : GetSelectedFacetsSearchProductList(RefinedFacets, result.Facets);//Display Selected Facets Product List;
                //    if (uxCategoryProductList.ProductID.Count <= 0) { ltrSearchResultMessage.Text = this.GetLocalResourceObject("ltrSearchResultMessageResource").ToString(); ltrSearchResultMessage.Visible = true; }
                //    uxCategoryProductList.DataBind();
                //}
            }
        }

        /// <summary>
        /// Registers the Script only if Subcategory is visiable
        /// </summary>
        private void RegisterClientScriptClasses()
        {
            StringBuilder script = new StringBuilder();
            script.Append("var zeonCatProdList = new CategoryProductList({");
            script.Append("\"Controls\":{");
            script.Append("},\"Messages\":{");
            script.Append("}");
            script.Append("});zeonCatProdList.Init();");
            this.Page.ClientScript.RegisterStartupScript(GetType(), "CategoryProductList", script.ToString(), true);

        }

        #region Search List Implementation (referd:http://www.onlineshoes.com/)

        /// <summary>
        /// Get Refined facets Product List 
        /// </summary>
        /// <param name="selectedFacets">List<KeyValuePair<string, IEnumerable<string>>></param>
        /// <param name="facetList"> List<ZNodeFacet></param>
        /// <returns> List<int></returns>
        private List<int> GetSelectedFacetsSearchProductList(List<KeyValuePair<string, IEnumerable<string>>> selectedFacets, List<ZNodeFacet> facetList)
        {
            List<int> selectedProductsList = new List<int>();
            if (facetList != null && selectedFacets != null)
            {
                List<int> currentSelectedFacetProducts = new List<int>();
                foreach (KeyValuePair<string, IEnumerable<string>> facet in selectedFacets)
                {
                    ZNodeFacet currentFacet = facetList.Find(x => x.AttributeName.Split('|')[0].ToLower().Equals(facet.Key.ToString().ToLower()));
                    if (currentFacet != null)
                    {
                        currentSelectedFacetProducts.Clear();
                        using (var refinedFacet = facet.Value.GetEnumerator())
                        {
                            while (refinedFacet.MoveNext())
                            {
                                if (refinedFacet.Current.ToString().IndexOf('^') > 0)
                                {
                                    string[] selFac = refinedFacet.Current.ToString().Split('^');

                                    foreach (string selAttr in selFac)
                                    {
                                        ZNodeFacetValue selectedAttribute = currentFacet.AttributeValues.Find(y => y.AttributeValue.Equals(selAttr));
                                        if (selectedAttribute != null && selAttr.Equals(selectedAttribute.AttributeValue))
                                        {
                                            currentSelectedFacetProducts.AddRange(selectedAttribute.ProductIDList);
                                        }
                                    }
                                }
                                else
                                {
                                    ZNodeFacetValue selectedAttribute = currentFacet.AttributeValues.Find(y => y.AttributeValue.Equals(refinedFacet.Current.ToString()));
                                    if (selectedAttribute != null && refinedFacet.Current.ToString().Equals(selectedAttribute.AttributeValue))
                                    {
                                        currentSelectedFacetProducts.AddRange(selectedAttribute.ProductIDList);
                                    }
                                }
                                //Add values in CurrentFacet list 
                                if (selectedProductsList.Count > 0)
                                {
                                    selectedProductsList = selectedProductsList.Intersect(currentSelectedFacetProducts).ToList();
                                }
                                else
                                {
                                    selectedProductsList.AddRange(currentSelectedFacetProducts);
                                }
                            }
                        }
                    }
                }

            }
            return selectedProductsList.Distinct().ToList();
        }

        /// <summary>
        /// Get Refined facets Product List to Manage Facet Count
        /// </summary>
        /// <param name="selectedFacets">List<KeyValuePair<string, IEnumerable<string>>></param>
        /// <param name="facetList"> List<ZNodeFacet></param>
        /// <returns> List<int></returns>
        private List<int> GetCurrentFacetProductList(List<KeyValuePair<string, IEnumerable<string>>> selectedFacets, List<ZNodeFacet> facetList, List<int> allProductList)
        {
            List<int> selectedProductsList = new List<int>();
            string currentFacetName = string.Empty, facetName = string.Empty, currentFacetValue = string.Empty;
            facetName = Session["CurrentUpdatedFacet"] != null ? GetCurrentUpdatedFacetFromSession() : GetCurrentFacetName(selectedFacets);
            currentFacetName = facetName.IndexOf(':') > 0 ? facetName.Split(':')[0] : facetName;
            currentFacetValue = facetName.IndexOf(':') > 0 ? facetName.Split(':')[1] : string.Empty;
            if (facetList != null && selectedFacets != null)
            {
                List<int> currentSelectedFacetProducts = new List<int>();
                foreach (KeyValuePair<string, IEnumerable<string>> facet in selectedFacets)
                {
                    if (!currentFacetName.ToLower().Equals(facet.Key.ToString().ToLower()) || !string.IsNullOrEmpty(currentFacetValue))
                    {
                        ZNodeFacet currentFacet = facetList.Find(x => x.AttributeName.Split('|')[0].ToLower().Equals(facet.Key.ToString().ToLower()));
                        if (currentFacet != null)
                        {
                            currentSelectedFacetProducts.Clear();
                            using (var refinedFacet = facet.Value.GetEnumerator())
                            {
                                while (refinedFacet.MoveNext())
                                {
                                    if (refinedFacet.Current.ToString().IndexOf('^') > 0)
                                    {
                                        string[] selFac = refinedFacet.Current.ToString().Split('^');

                                        foreach (string selAttr in selFac)
                                        {
                                            ZNodeFacetValue selectedAttribute = currentFacet.AttributeValues.Find(y => y.AttributeValue.Equals(selAttr));
                                            if (selectedAttribute != null && selAttr.Equals(selectedAttribute.AttributeValue))
                                            {
                                                if (currentFacetName.ToLower().Equals(facet.Key.ToString().ToLower()) && currentFacetValue.Split(',').Contains(selectedAttribute.AttributeValue) || (selectedAttribute.AttributeValue.Equals(currentFacetValue)))
                                                {
                                                    continue;
                                                }
                                                else
                                                {
                                                    currentSelectedFacetProducts.AddRange(selectedAttribute.ProductIDList);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ZNodeFacetValue selectedAttribute = currentFacet.AttributeValues.Find(y => y.AttributeValue.Equals(refinedFacet.Current.ToString()));
                                        if (selectedAttribute != null && refinedFacet.Current.ToString().Equals(selectedAttribute.AttributeValue))
                                        {
                                            if (currentFacetName.ToLower().Equals(facet.Key.ToString().ToLower()) && currentFacetValue.Split(',').Contains(selectedAttribute.AttributeValue) || (selectedAttribute.AttributeValue.Equals(currentFacetValue)))
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                currentSelectedFacetProducts.AddRange(selectedAttribute.ProductIDList);
                                            }
                                        }
                                    }
                                    //Add CurrentFacet list 
                                    if (selectedProductsList.Count > 0)
                                    {
                                        if (currentSelectedFacetProducts.Count > 0)
                                        {
                                            selectedProductsList = selectedProductsList.Intersect(currentSelectedFacetProducts).ToList();
                                        }
                                    }
                                    else
                                    {
                                        selectedProductsList.AddRange(currentSelectedFacetProducts);
                                    }
                                }
                            }
                        }

                    }
                }
            }
            if (selectedProductsList.Count <= 0) { selectedProductsList = allProductList; }
            return selectedProductsList.Distinct().ToList();
        }

        /// <summary>
        /// Get Current Selected Facet Name
        /// </summary>
        /// <returns>string</returns>
        private string GetCurrentFacetName(List<KeyValuePair<string, IEnumerable<string>>> selectedFacets)
        {
            string currentFacetSelected = string.Empty;
            if (selectedFacets != null && selectedFacets.Count > 0)
            {
                for (int facetIndex = selectedFacets.Count - 1; facetIndex >= 0; facetIndex--)
                {
                    if (selectedFacets.Count > 0)
                    {
                        currentFacetSelected = selectedFacets[facetIndex].Key + ":";
                        using (var refinedFacet = selectedFacets[facetIndex].Value.GetEnumerator())
                        {
                            while (refinedFacet.MoveNext())
                            {
                                if (refinedFacet.Current.ToString().IndexOf('^') > 0)
                                {
                                    string[] selFac = refinedFacet.Current.ToString().Split('^');
                                    foreach (string attrVal in selFac)
                                    {
                                        currentFacetSelected = currentFacetSelected + attrVal + ",";
                                    }
                                    currentFacetSelected.TrimEnd(',');
                                }
                                else
                                {
                                    currentFacetSelected = currentFacetSelected + refinedFacet.Current;
                                    break;
                                }
                            }
                        }
                        if (currentFacetSelected.IndexOf(':') > 0) { break; }
                    }
                }
            }
            return currentFacetSelected;
        }

        /// <summary>
        /// Get Current Selected Facet Name from Session
        /// </summary>
        /// <returns>string</returns>
        private string GetCurrentUpdatedFacetFromSession()
        {
            string currentFacetSelected = string.Empty;
            if (Session["CurrentUpdatedFacet"] != null)
            {
                currentFacetSelected = Session["CurrentUpdatedFacet"].ToString();
            }
            return currentFacetSelected;
        }

        /// <summary>
        /// Show Category Product List Setting
        /// </summary>
        private void ShowCategoryProductList()
        {
            uxSubCategoryList.Visible = false;
            uxCategoryProductList.Visible = true;
            ProductDetails.Visible = true;
            ucFacets.Visible = true;
            // Bind data to page
            this.Bind();
        }

        #endregion

        #endregion
    }
}