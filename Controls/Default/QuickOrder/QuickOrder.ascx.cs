using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the QuickOrder user control class.
    /// </summary>
    public partial class Controls_Default_QuickOrder_QuickOrder : System.Web.UI.UserControl
    {
        #region Public Event Handler
        public System.EventHandler AddToCartClicked;
        #endregion

        #region Protected Member variables
        private bool _RedirectToCartPage = true;
        private ZNodeShoppingCart shoppingCart = null;
        private ZNodeGenericCollection<ZNodeShoppingCartItem> ItemCollection = new ZNodeGenericCollection<ZNodeShoppingCartItem>();
        #endregion
        
        #region Public Properties
        /// <summary>
        /// Gets or sets a value indicating whether redirect to shopping cart page based on this porperties
        /// </summary>
        public bool RedirectToCartPage
        {
            get
            {
                return this._RedirectToCartPage;
            }

            set
            {
                this._RedirectToCartPage = value;
            }
        }
        #endregion

        #region Page Events
        /// <summary>
        /// Page Load Events
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            uxProductDetail.CancelButtonClicked += new System.EventHandler(this.Cancel_Click);
            uxProductDetail.AddProductClicked += new System.EventHandler(this.AddAnother_Click);

            // Set image path
            uxAddToCart.ImageUrl = "~/themes/" + ZNodeCatalogManager.Theme + "/images/addtocart.gif";

            if (!Page.IsPostBack)
            {
                // Initialize these items
                this.IntializeItemCollection();

                // On page load Show select products popup 
                uxProductDetail.Product = new ZNodeProduct();
                uxProductDetail.RowIndex = -1;
                uxProductDetail.BindEditData();

                // Hide Quick order if Show Price is False
                if (!this.DisplayQuickOrder())
                {
                    this.HideAddToCartControls();
                    mdlPopup.Hide();

                    return;
                }
            }
            else 
            { 
                this.ItemCollection = this.CurrentShoppingCartObject(); 
            }
        }

        /// <summary>
        /// Pre-Render event page
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            decimal subTotal = 0;

            // Loop through the quick Order Items
            foreach (ZNodeShoppingCartItem Item in this.ItemCollection)
            {
                subTotal += Item.ExtendedPrice;
            }

            // Set Total field
            Total.Text = subTotal.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();

            if (this.ItemCollection.Count > 0)
            {
                pnlAddToCart.Visible = true;
            }
            else
            {
                pnlAddToCart.Visible = false;
            }
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Event occurs when a data row is bound to data in a GridView control.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowEditing(object sender, GridViewEditEventArgs e)
        {
            int index = e.NewEditIndex;
            uxProductDetail.Product = (ZNodeProduct)this.ItemCollection[index].Product;
            uxProductDetail.ShoppingCartQuantity = this.ItemCollection[index].Quantity;
            uxProductDetail.RowIndex = index;
            uxProductDetail.BindEditData();
            UpdatePnlSelectProduct.Update();
            mdlPopup.Show();
        }

        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.ItemCollection.RemoveAt(e.RowIndex);
            this.BindGrid();
        }
        #endregion

        #region General Events

        /// <summary>
        /// Add Item button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddItem_Click(object sender, EventArgs e)
        {
            uxProductDetail.Product = new ZNodeProduct();
            uxProductDetail.RowIndex = -1;
            uxProductDetail.BindEditData();
            UpdatePnlSelectProduct.Update();
            mdlPopup.Show();
        }

        /// <summary>
        /// Event is raised when cancel button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            // Bind Grid
            this.BindGrid();
            UpdatePanel.Update();
            mdlPopup.Hide();
        }

        /// <summary>
        /// Event is raised when Add Another Product button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddAnother_Click(object sender, EventArgs e)
        {
            // Bind Grid
            this.BindGrid();
            UpdatePanel.Update();

            uxProductDetail.Product = new ZNodeProduct();
            uxProductDetail.RowIndex = -1;
            uxProductDetail.BindEditData();
            UpdatePnlSelectProduct.Update();

            mdlPopup.Show();
        }

        /// <summary>
        /// Checkout Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxAddToCart_Click(object sender, EventArgs e)
        {
            // Retreive shopping cart object from current session
            this.shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

            if (this.shoppingCart == null)
            {
                this.shoppingCart = new ZNodeShoppingCart();
                this.shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
            }

            // Loop through the quick Order Items
            foreach (ZNodeShoppingCartItem Item in this.ItemCollection)
            {
                ZNodeShoppingCartItem newItem = new ZNodeShoppingCartItem();
                newItem.Product = new ZNodeProductBase((ZNodeProduct)Item.Product);

                if (Item.Quantity > 0)
                {
                    newItem.Quantity = Item.Quantity;

                    // Add quick Order items into current shopping cart object
                    this.shoppingCart.AddToCart(newItem);
                }
            }

            if (this.RedirectToCartPage)
            {
                // Remove temporary shopping cart Object
                Session.Remove("CartItemCollectionObject");

                // Redirect to Shopping Cart Page
                Response.Redirect("~/shoppingCart.aspx");
            }
            else
            {
                if (this.AddToCartClicked != null)
                {
                    this.AddToCartClicked(sender, e);
                }
            }
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// check whether catalog image is exists or not
        /// If exists, then return true otherwise false
        /// </summary>
        /// <param name="filePath">Path of the File</param>
        /// <returns>Returns the bool value to show catalog image or not</returns>
        protected bool ShowCatalogImage(string filePath)
        {
            // FileInfo object
            System.IO.FileInfo Info = new System.IO.FileInfo(Server.MapPath(filePath));
            
            // If file exists
            if (Info.Exists) 
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Get Show Price value from Default Profile.
        /// </summary>
        /// <returns>Returns a bool value that represents Show Price value</returns>
        private bool DisplayQuickOrder()
        {
            ZNodeProfile profile = new ZNodeProfile();

            return profile.ShowPrice;
        }

        /// <summary>
        /// Represents the HideAddToCartControls method
        /// </summary>
        private void HideAddToCartControls()
        {
            pnlAddToCart.Visible = false;
            ibAddItem.Visible = false;
            ErrorMessage.Text = this.GetLocalResourceObject("PermissionDenied").ToString();
        }

        /// <summary>
        /// Represents the Bind Grid method
        /// </summary>
        private void BindGrid()
        {
            uxGrid.DataSource = this.ItemCollection;
            uxGrid.DataBind();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Creates a Item collection object and add it to Session object
        /// </summary>
        private void IntializeItemCollection()
        {
            this.ItemCollection.Clear();

            // Remove temporary shopping cart Object
            Session.Remove("CartItemCollectionObject");

            Total.Text = 0.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();

            // Bind Grid
            this.BindGrid();

            Session["CartItemCollectionObject"] = this.ItemCollection;
        }
        #endregion

        #region Methods related to Collection object
        /// <summary>
        /// Returns the current shopping cartItem collection
        /// </summary>
        /// <returns>Returns the Item Collection</returns>
        private ZNodeGenericCollection<ZNodeShoppingCartItem> CurrentShoppingCartObject()
        {
            // Get the user account from session
            this.ItemCollection = (ZNodeGenericCollection<ZNodeShoppingCartItem>)HttpContext.Current.Session["CartItemCollectionObject"];

            // Not in session
            if (this.ItemCollection == null) 
            {
                Session["CartItemCollectionObject"] = this.ItemCollection;

                return new ZNodeGenericCollection<ZNodeShoppingCartItem>();
            }
            else
            {
                // Return the value from session
                return this.ItemCollection;
            }
        }
        #endregion
    }
}