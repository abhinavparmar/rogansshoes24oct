<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_QuickOrder_QuickOrder" Codebehind="QuickOrder.ascx.cs" %>
<%@ Register Src="ProductDetail.ascx" TagName="ProductDetail" TagPrefix="ZNode" %>
<% Session.Timeout = 120; %>

<div class="QuickOrder">
    <h1>
        <asp:Localize ID="txtQuickOrder" runat="server" meta:resourceKey="txtQuickOrder">
        </asp:Localize></h1>   
    <div class="HintText">
        <asp:Localize ID="Localize1" runat="server" meta:resourceKey="txtHint1">
        </asp:Localize>
        <strong>
            <asp:Localize ID="Localize2" runat="server" meta:resourceKey="txtHint2">
            </asp:Localize></strong>
    </div>    
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div>
                <asp:Button ID="ibAddItem" runat="server" Text="Add New Item" 
                    OnClick="BtnAddItem_Click" CausesValidation="False" 
                    meta:resourcekey="ibAddItemResource1"/>
            </div>
            <!-- Ajax Modal Popup Box  -->            
            <asp:Button id="btnShowPopup" runat="server" style="display:none" 
                meta:resourcekey="btnShowPopupResource1" />            
            <ajaxToolkit:ModalPopupExtender ID="mdlPopup" runat="server" 
                TargetControlID="btnShowPopup" PopupControlID="pnlSelectProduct" 
                BackgroundCssClass="modalBackground" DynamicServicePath="" Enabled="True" />
             <!-- Select product Section -->            
            <asp:Panel ID="pnlSelectProduct" runat="server" CssClass="PopupStyle" 
                style="display:none;" meta:resourcekey="pnlSelectProductResource1">
                <asp:UpdatePanel ID="UpdatePnlSelectProduct" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <ZNode:ProductDetail ID="uxProductDetail" runat="server" />                    
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <!-- Select product Section End -->             
            
            <asp:Panel ID="pnlAddToCart" runat ="server" Visible="False" 
                meta:resourcekey="pnlAddToCartResource1">            
            <div>
                <asp:GridView ID="uxGrid" CssClass="Grid" runat="server" 
                    AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" 
                    GridLines="None" OnRowEditing="UxGrid_RowEditing" 
                    OnRowDeleting="UxGrid_RowDeleting" meta:resourcekey="uxGridResource1">
                    <Columns>
                        <asp:TemplateField meta:resourcekey="TemplateFieldResource1">
                            <ItemTemplate>
                                <div class="EditLink"><asp:LinkButton ID="EditLink" runat="server" Text="Edit" 
                                        CausesValidation="False" OnClientClick="_isset=-1;" CommandName="Edit" 
                                        meta:resourcekey="EditLinkResource1"></asp:LinkButton></div>
                            </ItemTemplate>       
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField> 
                        <asp:TemplateField meta:resourcekey="TemplateFieldResource2">
                            <ItemTemplate>
                                <div class="EditLink" ID="ibDelete">
                                    <asp:ImageButton ID="ImageButton1" runat="server" CommandName="Delete" 
                                        CausesValidation="False" AlternateText="Delete" ToolTip="Delete" 
                                        ImageUrl='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("RemoveLineItem.png") %>' 
                                        onclientclick="_isset=-1;" meta:resourcekey="ImageButton1Resource1" />
                                </div>
                            </ItemTemplate>       
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>                   
                        <asp:TemplateField meta:resourcekey="TemplateFieldResource3">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem,"Quantity") %>                            
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField meta:resourcekey="TemplateFieldResource4">
                            <ItemTemplate><asp:Image id="CatalogImage" AlternateText="Catalog Image" ToolTip="Catalog Image" border='0' 
                                    ImageUrl='<%# DataBinder.Eval(Container.DataItem, "Product.ThumbnailImageFilePath") %>' 
                                    visible='<%# ShowCatalogImage(DataBinder.Eval(Container.DataItem, "Product.ThumbnailImageFilePath").ToString()) %>' 
                                    runat="server" meta:resourcekey="CatalogImageResource1" /></ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField> 
                         <asp:TemplateField meta:resourcekey="TemplateFieldResource5">
                            <ItemTemplate>                            
                                <%# DataBinder.Eval(Container.DataItem,"Product.Name") %>
                            </ItemTemplate>
                             <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField meta:resourcekey="TemplateFieldResource6">
                            <ItemTemplate>                            
                                <%# DataBinder.Eval(Container.DataItem,"Product.ProductNum") %>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        
                        <asp:TemplateField meta:resourcekey="TemplateFieldResource7">
                            <ItemTemplate>                            
                                <%# DataBinder.Eval(Container.DataItem,"Product.ShoppingCartDescription") %>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField meta:resourcekey="TemplateFieldResource8">
                            <ItemTemplate>                    
                                <div class='Price'><asp:Label ID='UnitPrice' runat="server"  
                                        Text='<%# DataBinder.Eval(Container.DataItem,"UnitPrice","{0:c}") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix() %>' 
                                        meta:resourcekey="UnitPriceResource1"></asp:Label></div>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        
                        <asp:TemplateField meta:resourcekey="TemplateFieldResource9">
                            <ItemTemplate>                    
                                <div class='Price'><asp:Label ID='TotalPrice' runat="server"  
                                        Text='<%# DataBinder.Eval(Container.DataItem, "ExtendedPrice", "{0:c}") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix() %>' 
                                        meta:resourcekey="TotalPriceResource1"></asp:Label></div>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>                    
                    </Columns>
                    <FooterStyle CssClass="FooterStyle" />
                    <RowStyle CssClass="RowStyle" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                </asp:GridView>
            </div>
            <table width="100%" class="TotalBox">
                <tr>
                    <td>&nbsp;</td>
                    <td class="CartTotals">
                        <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:CommonCaption, Total%>">
                        </asp:Literal> &nbsp;<asp:Label ID="Total" runat="server" 
                            meta:resourcekey="TotalResource1"></asp:Label></td>
                </tr>
                <tr class="Row">
                    <td>&nbsp;</td>
                    <td class="AddToCartButton"><asp:ImageButton ID="uxAddToCart" runat="server" AlternateText="Add To Cart" ToolTip="Add To Cart"
                            OnClick="UxAddToCart_Click" ImageAlign="AbsMiddle" OnClientClick="_isset=1;" 
                            CausesValidation="False" meta:resourcekey="uxAddToCartResource1" /></td>
                </tr>
            </table>
            </asp:Panel>
            <div><asp:Label ID="ErrorMessage" CssClass="Error" runat="server" 
                    meta:resourcekey="ErrorMessageResource1"></asp:Label></div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div>&nbsp;</div>
</div>