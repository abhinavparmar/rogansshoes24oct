using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the ProductDetail user control class.
    /// </summary>
    public partial class Controls_Default_QuickOrder_ProductDetail : System.Web.UI.UserControl
    {
        #region Public Events
        public System.EventHandler AddProductClicked;
        public System.EventHandler CancelButtonClicked;
        #endregion

        #region Private Variables
        private ZNodeProduct _product;
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the shopping cart quantity
        /// </summary>
        public int ShoppingCartQuantity
        {
            get
            {
                if (ViewState["ShoppingCartQuantity"] != null)
                {
                    return int.Parse(ViewState["ShoppingCartQuantity"].ToString());
                }

                return 1;
            }

            set
            {
                ViewState["ShoppingCartQuantity"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the catalog item object. The control will bind to the data from this object
        /// </summary>
        public ZNodeProduct Product
        {
            get
            {
                if (ViewState["CatalogItem"] != null)
                {
                    return ViewState["CatalogItem"] as ZNodeProduct;
                }

                return new ZNodeProduct();
            }

            set
            {
                ViewState["CatalogItem"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the index of the row being Edited
        /// </summary>
        public int RowIndex
        {
            get
            {
                if (ViewState["RowIndex"] != null)
                {
                    return int.Parse(ViewState["RowIndex"].ToString());
                }

                return -1;
            }

            set
            {
                ViewState["RowIndex"] = value;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Bind edit data
        /// </summary>
        public void BindEditData()
        {
            this._product = this.Product;

            if (this._product.ProductID > 0)
            {
                pnlProductDetails.Visible = true;

                CatalogItemImage.ImageUrl = this._product.MediumImageFilePath;
                ProductDescription.Text = this._product.Description;

                // Bind product addons and attributes
                this.BindAddOnsAttributes();

                // Bind the MaxQuantity value to the Dropdown list
                this.BindQuantityList();

                // Show Price
                this.DisplayPrice();

                CatalogItemImage.Visible = this.ShowCatalogImage(this._product.MediumImageFilePath);

                tablerow.Visible = true;

                // Enable stock validator
                this.EnableStockValidator(this._product);

                // Display availability grid
                this.DisplayProductGrid(this._product);
            }
            else
            {
                this.ResetUI();
            }
        }

        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Validate selected addons and returns the Inventory related messages
        /// </summary>
        /// <param name="AddOn">AddOn Instance</param>
        /// <param name="AddOnValue">AddonValue Instance</param>
        /// <returns>Retunrs the Inventory related message</returns>
        protected string BindStatusMsg(ZNodeAddOn AddOn, ZNodeAddOnValue AddOnValue)
        {
            // If quantity available is less and track inventory is enabled
            if (AddOnValue.QuantityOnHand <= 0 && AddOn.AllowBackOrder == false && AddOn.TrackInventoryInd)
            {
                return AddOn.OutOfStockMsg;
            }
            else if (AddOnValue.QuantityOnHand <= 0 && AddOn.AllowBackOrder == true && AddOn.TrackInventoryInd)
            {
                return AddOn.BackOrderMsg;
            }
            else if (AddOn.TrackInventoryInd && AddOnValue.QuantityOnHand > 0)
            {
                return AddOn.InStockMsg;
            }
            else if (AddOn.AllowBackOrder == false && AddOn.TrackInventoryInd == false)
            {
                // Don't track Inventory
                // Items can always be added to the cart,TrackInventory is disabled
                return AddOn.InStockMsg;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the Display Price
        /// </summary>
        protected void DisplayPrice()
        {
            decimal unitPrice = 0;
            decimal extendedPrice = 0;

            if (this._product.ProductID > 0)
            {
                unitPrice = this._product.FinalPrice + this._product.AddOnPrice;
                extendedPrice = unitPrice * this.ShoppingCartQuantity;
            }

            lblUnitPrice.Text = unitPrice.ToString("c");
            lblTotalPrice.Text = extendedPrice.ToString("c");
        }

        /// <summary>
        /// Display product size grid
        /// </summary>
        /// <param name="product">Product Instance</param>
        protected void DisplayProductGrid(ZNodeProduct product)
        {
            if (product.ProductID > 0)
            {
                string[] attributeValues = product.SelectedSKU.AttributesValue.Split(new char[] { ',' });
                uxProductAttributeGrid.AttributeId = -1;
                uxProductAttributeGrid.Bind();

                if (attributeValues.Length > 1)
                {
                    int attributeId = int.Parse(attributeValues[0]);
                    uxProductAttributeGrid.AttributeId = attributeId;
                    uxProductAttributeGrid.ProductTypeID = product.ProductTypeID;
                    uxProductAttributeGrid.ProductID = product.ProductID;
                    uxProductAttributeGrid.Bind();
                    uxProductAttributeGrid.Visible = true;

                    string _imagePath = ZNodeConfigManager.EnvironmentConfig.MediumImagePath + uxProductAttributeGrid.Path;

                    if (this.ShowCatalogImage(_imagePath))
                    {
                        CatalogItemImage.ImageUrl = _imagePath;
                    }
                    else
                    {
                        CatalogItemImage.ImageUrl = this._product.MediumImageFilePath;
                    }
                }
                else
                {
                    uxProductAttributeGrid.Visible = false;
                }
            }
        }

        /// <summary>
        /// Reset the field values
        /// </summary>
        protected void ResetUI()
        {
            tablerow.Visible = false;

            CatalogItemImage.ImageUrl = string.Empty;
            ProductDescription.Text = string.Empty;
            lblUnitPrice.Text = 0.ToString("c");
            lblTotalPrice.Text = lblUnitPrice.Text;
            Qty.SelectedIndex = 0;
            ControlPlaceHolder.Controls.Clear();

            CatalogItemImage.Visible = false;
            pnlProductDetails.Visible = false;
            pnlProductList.Visible = false;
        }

        #endregion

        #region Page Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Gets the browser string
            if (Request.Browser.Browser.Equals("IE"))
            {
                UpdateProgressIE.Visible = true;
            }
            else if (Request.Browser.Browser.Equals("Firefox"))
            {
                UpdateProgressFirefox.Visible = true;
            }
            else
            {
                UpdateProgressFirefox.Visible = true;
            }

            if (!Page.IsPostBack)
            {
                // Hide product attribute grid
                uxProductAttributeGrid.Visible = false;

                if (this._product.ProductID == 0)
                {
                    int max = 10;
                    ArrayList Quantity = new ArrayList();

                    for (int i = 1; i <= max; i++)
                    {
                        Quantity.Add(i);
                    }

                    Qty.DataSource = Quantity;
                    Qty.DataBind();
                }
            }
            else
            {
                // Bind Dynamic controls
                this.BindAddOnsAttributes();
            }

            // Registers a client script block with the ScriptManager control for use with a control that is inside an UpdatePanel control
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Reset", "javascript:_isset=0;", true);
        }

        protected override void OnPreRender(EventArgs e)
        {
            ibSearch.ImageUrl = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/SearchButton.gif";
            ibSubmit.ImageUrl = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/Submit.gif";
        }

        #endregion

        #region Grid Events

        /// <summary>
        /// Event is raised when Select button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Select_Clicked(object sender, EventArgs e)
        {
            // Get productId from select button Argument property
            LinkButton linkButton = (LinkButton)sender;

            if (linkButton.CommandArgument != "0")
            {
                int productId = 0;
                int quantity = 0;

                int.TryParse(linkButton.CommandArgument, out productId);

                if (productId > 0)
                {
                    pnlProductDetails.Visible = true;
                    pnlProductList.Visible = false;
                    uxGrid.DataSource = null;
                    uxGrid.DataBind();
                    
                   // Clear resources
                    uxGrid.Dispose(); 

                    if (!int.TryParse(Qty.SelectedValue, out quantity))
                    {
                        quantity = 1;
                    }

                    if (this.Product.ProductID != productId)
                    {
                        // Get a product object using create static method
                        this._product = ZNodeProduct.Create(productId, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);
                        this.ShoppingCartQuantity = 1;
                        this.Product = this._product;
                    }
                    else
                    {
                        this._product = this.Product;
                    }

                    CatalogItemImage.ImageUrl = this._product.MediumImageFilePath;
                    CatalogItemImage.Visible = this.ShowCatalogImage(this._product.MediumImageFilePath);

                    this.BindAddOnsAttributes();

                    if (this._product.ZNodeAddOnCollection.Count > 0)
                    {
                        this.ValidateAddOns(sender, e);
                    }
                    else
                    {
                        this.Bind();
                    }

                    if (this._product.ZNodeAttributeTypeCollection.Count == 0)
                    {
                        // Enable stock validator
                        this.EnableStockValidator(this._product);
                    }
                    else
                    {
                        this.ValidateAttributes(sender, e);
                    }
                }
            }
        }

        /// <summary>
        /// Event is raised when Next link is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void NextRecord(object sender, EventArgs e)
        {
            if (!PageIndex.Value.Equals(TotalPages.Value))
            {
                PageIndex.Value = (int.Parse(PageIndex.Value) + 1).ToString();

                this.BindProducts();

                FirstPageLink.Enabled = true;
                PreviousPageLink.Enabled = true;
            }

            if (PageIndex.Value.Equals(TotalPages.Value))
            {
                LastPageLink.Enabled = false;
                NextPageLink.Enabled = false;
            }

            lblSearhError.Text = string.Empty;
        }

        /// <summary>
        /// Event is fired when Previous link is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void PrevRecord(object sender, EventArgs e)
        {
            if (!PageIndex.Value.Equals("1"))
            {
                PageIndex.Value = (int.Parse(PageIndex.Value) - 1).ToString();

                this.BindProducts();

                LastPageLink.Enabled = true;
                NextPageLink.Enabled = true;
            }

            if (PageIndex.Value.Equals("1"))
            {
                FirstPageLink.Enabled = false;
                PreviousPageLink.Enabled = false;
            }

            lblSearhError.Text = string.Empty;
        }

        /// <summary>
        /// Represents the method that Moves to last Page.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void MoveToLastPage(object sender, EventArgs e)
        {
            if (!PageIndex.Value.Equals(TotalPages.Value))
            {
                int pageLength = int.Parse(TotalPages.Value);

                PageIndex.Value = pageLength.ToString();
                this.BindProducts();

                FirstPageLink.Enabled = true;
                PreviousPageLink.Enabled = true;
            }

            if (PageIndex.Value.Equals(TotalPages.Value))
            {
                LastPageLink.Enabled = false;
                NextPageLink.Enabled = false;
            }

            lblSearhError.Text = string.Empty;
        }

        /// <summary>
        /// Event is raised when Move to First page link is clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void MoveToFirstPage(object sender, EventArgs e)
        {
            if (!PageIndex.Value.Equals("1"))
            {
                PageIndex.Value = "1";
                this.BindProducts();

                NextPageLink.Enabled = true;
                LastPageLink.Enabled = true;
            }

            if (PageIndex.Value.Equals("1"))
            {
                FirstPageLink.Enabled = false;
                PreviousPageLink.Enabled = false;
            }

            lblSearhError.Text = string.Empty;
        }
        #endregion

        #region Events
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            this.AddToList();

            if (this.CancelButtonClicked != null)
            {
                this.CancelButtonClicked(sender, e);
            }
        }

        /// <summary>
        /// Cancel Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (this.CancelButtonClicked != null)
            {
                this.CancelButtonClicked(sender, e);
            }
        }

        /// <summary>
        /// Validate selected addons
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ValidateAddOns(object sender, EventArgs e)
        {
            System.Text.StringBuilder _addonValues = new System.Text.StringBuilder();
            this._product = this.Product;

            foreach (ZNodeAddOn AddOn in this._product.ZNodeAddOnCollection)
            {
                System.Web.UI.WebControls.DropDownList lstCtrl = new DropDownList();

                lstCtrl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAddOn" + AddOn.AddOnID.ToString());

                if (_addonValues.Length > 0)
                {
                    _addonValues.Append(",");
                }

                if (lstCtrl.SelectedValue == "0" || lstCtrl.SelectedValue == "-1")
                {
                    _addonValues.Append(lstCtrl.SelectedValue);
                }
                else
                {
                    // Loop through the Add-on values for each Add-on
                    foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                    {
                        // Optional Addons are not selected,then leave those addons 
                        // If optinal Addons are selected, it should add with the Selected item 
                        // Check for Selected Addon value 
                        if (AddOnValue.AddOnValueID.ToString() == lstCtrl.SelectedValue) 
                        {
                            // Add to Selected Addon list for this product
                            _addonValues.Append(AddOnValue.AddOnValueID.ToString());
                        }
                    }
                }
            }

            ZNodeAddOnList SelectedAddOn = new ZNodeAddOnList();

            if (!_addonValues.ToString().Contains("0"))
            {
                // Get a sku based on Add-ons selected
                SelectedAddOn = ZNodeAddOnList.CreateByProductAndAddOns(this._product.ProductID, _addonValues.ToString());

                SelectedAddOn.SelectedAddOnValues = _addonValues.ToString();
            }
            else 
            { 
                SelectedAddOn.SelectedAddOnValues = _addonValues.ToString(); 
            }

            // Set Selected Add-on 
            this._product.SelectedAddOnItems = SelectedAddOn;
           
            this.Product = this._product;
            this.Bind();
        }

        /// <summary>
        /// Validate selected attributes
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ValidateAttributes(object sender, EventArgs e)
        {
            System.Text.StringBuilder _attributes = new System.Text.StringBuilder();
            System.Text.StringBuilder _description = new System.Text.StringBuilder();
            System.Text.StringBuilder _addonValues = new System.Text.StringBuilder();
            this._product = this.Product;

            // Loop through types to locate the controls
            foreach (ZNodeAttributeType AttributeType in this._product.ZNodeAttributeTypeCollection)
            {
                if (_attributes.Length > 0)
                {
                    _attributes.Append(",");
                }

                if (!AttributeType.IsPrivate)
                {
                    System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + AttributeType.AttributeTypeId.ToString());

                    int selValue = 0;
                    if (lstControl.SelectedIndex != -1)
                    {
                        selValue = int.Parse(lstControl.SelectedValue);
                    }

                    if (selValue > 0)
                    {
                        AttributeType.SelectedAttributeId = selValue;

                        _attributes.Append(selValue.ToString());

                        _description.Append(AttributeType.Name);
                        _description.Append(": ");
                        _description.Append(lstControl.SelectedItem.Text);
                        _description.Append("<br />");
                    }
                    else
                    {
                        _attributes.Append(selValue.ToString());
                    }
                }
            }

            ZNodeSKU SKU = new ZNodeSKU();

            if (_attributes.Length > 0)
            {
                SKU = ZNodeSKU.CreateByProductAndAttributes(this._product.ProductID, _attributes.ToString());

                SKU.AttributesDescription = _description.ToString();
                SKU.AttributesValue = _attributes.ToString();
            }

            this._product.SelectedSKU = SKU;
          
            this.Product = this._product;
            this.Bind();
            
            // Enable Stock validator
            this.EnableStockValidator(this._product);
            
            // Display Grid
            this.DisplayProductGrid(this._product);
        }

        /// <summary>
        /// Event is raised when Close Button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClose_Click(object sender, EventArgs e)
        {
            uxGrid.DataSource = null;
            uxGrid.DataBind();

            uxGrid.Dispose();

            if (this.CancelButtonClicked != null)
            {
                this.CancelButtonClicked(sender, e);
            }
        }

        /// <summary>
        /// Event is raised when Search button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Search_Click(object sender, EventArgs e)
        {
            pnlProductDetails.Visible = false;
            pnlProductList.Visible = false;

            NextPageLink.Enabled = false;
            PreviousPageLink.Enabled = false;
            FirstPageLink.Enabled = false;
            LastPageLink.Enabled = false;

            uxGrid.DataSource = null;
            uxGrid.DataBind();

            TotalPages.Value = "0";
            PageIndex.Value = "1";

            this.BindProducts();
        }

        /// <summary>
        /// Event is raised when Add Another Product button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmitAnother_Click(object sender, EventArgs e)
        {
            this.AddToList();
           
            if (this.AddProductClicked != null)
            {
                this.AddProductClicked(sender, e);
            }
        }

        /// <summary>
        /// Event is raised when Quantity Selected Index is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Qty_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ShoppingCartQuantity = int.Parse(Qty.SelectedValue);
            this.DisplayPrice();
        }
        #endregion

        #region Bind Dynamic Controls
        /// <summary>
        /// Creates attribute drop downlist controls dynamically for a product
        /// </summary>
        protected void BindAddOnsAttributes()
        {
            this._product = this.Product;

            if (this._product.ProductID > 0)
            {
                // Remove existing dynamic controls from the control place holder object
                ControlPlaceHolder.Controls.Clear();

                // Find the placeholder control and Add the literal control to the Controls collection of the PlaceHolder control.
                Literal literal = new Literal();
                literal.Text = "<div class='Attributes'>";
                ControlPlaceHolder.Controls.Add(literal);
                int counter = 0;
                
                if (this._product.ZNodeAttributeTypeCollection.Count > 0)
                {
                    foreach (ZNodeAttributeType AttributeType in this._product.ZNodeAttributeTypeCollection)
                    {
                        System.Web.UI.WebControls.DropDownList lstControl = new DropDownList();
                        lstControl.ID = "lstAttribute" + AttributeType.AttributeTypeId.ToString();
                        lstControl.Attributes.Add("class", "ValueStyle");
                        lstControl.AutoPostBack = true;
                        lstControl.SelectedIndexChanged += new System.EventHandler(this.ValidateAttributes);

                        foreach (ZNodeAttribute Attribute in AttributeType.ZNodeAttributeCollection)
                        {
                            ListItem li1 = new ListItem(Attribute.Name, Attribute.AttributeId.ToString());
                            lstControl.Items.Add(li1);
                        }

                        string attributeValues = this._product.SelectedSKU.AttributesValue;
                        string[] attributes = new string[this._product.ZNodeAttributeTypeCollection.Count];

                        if (attributeValues.Length > 0)
                        {
                            attributes = attributeValues.Split(new char[] { ',' }, StringSplitOptions.None);
                        }

                        if (lstControl.Items.Count > 0)
                        {
                            if (attributes != null)
                            {
                                lstControl.SelectedValue = attributes[counter++];
                            }
                        }

                        // Add the Attribute dropdownlist control to the Controls collection
                        // of the PlaceHolder control.                
                        ControlPlaceHolder.Controls.Add(lstControl);

                        Literal ltrlSpacer = new Literal();
                        ltrlSpacer.Text = "<br />";
                        ControlPlaceHolder.Controls.Add(ltrlSpacer);
                    }
                }
                
                string addonValues = this._product.SelectedAddOnItems.SelectedAddOnValues;

                string[] addonValueId = new string[this._product.ZNodeAddOnCollection.Count];
                if (addonValues.Length > 0)
                {
                    addonValueId = addonValues.Split(new string[] { "," }, StringSplitOptions.None);
                }

                if (this._product.ZNodeAddOnCollection.Count > 0)
                {
                    foreach (ZNodeAddOn AddOn in this._product.ZNodeAddOnCollection)
                    {
                        System.Web.UI.WebControls.DropDownList lstControl = new DropDownList();
                        lstControl.ID = "lstAddOn" + AddOn.AddOnID.ToString();
                        lstControl.AutoPostBack = true;
                        lstControl.SelectedIndexChanged += new System.EventHandler(this.ValidateAddOns);
                        lstControl.Attributes.Add("class", "ValueStyle");

                        // Don't display list box if there is no add-on values for AddOns
                        if (AddOn.ZNodeAddOnValueCollection.Count > 0)
                        {
                            foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                            {
                                string AddOnValueName = AddOnValue.Name;
                                decimal decRetailPrice = AddOnValue.FinalPrice;

                                if (decRetailPrice < 0)
                                {
                                    // Price format
                                    string priceformat = "-" + ZNodeCurrencyManager.GetCurrencyPrefix() + "{0:0.00}" + ZNodeCurrencyManager.GetCurrencySuffix();
                                    AddOnValueName += " : " + String.Format(priceformat, System.Math.Abs(decRetailPrice)) + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                                }
                                else if (decRetailPrice > 0)
                                {
                                    AddOnValueName += " : " + decRetailPrice.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                                }

                                // Added Inventory Message with the Addon Value Name in the dropdownlist
                                AddOnValueName += " " + this.BindStatusMsg(AddOn, AddOnValue);

                                ListItem li1 = new ListItem(AddOnValueName, AddOnValue.AddOnValueID.ToString());
                                lstControl.Items.Add(li1);

                                if (AddOnValue.IsDefault)
                                {
                                    lstControl.SelectedValue = AddOnValue.AddOnValueID.ToString();
                                }
                            }

                            // Check for Optional 
                            if (AddOn.OptionalInd)
                            {
                                ListItem OptionalItem = new ListItem("Optional", "-1");
                                lstControl.Items.Insert(0, OptionalItem);
                                lstControl.SelectedValue = "-1";
                            }

                            // Pre-select previous selected addon values in the list
                            for (int i = 0; i < addonValueId.Length; i++)
                            {
                                if (addonValueId[i] != null)
                                {
                                    lstControl.SelectedValue = addonValueId[i];
                                    if (lstControl.SelectedValue == addonValueId[i])
                                    {
                                        break;
                                    }
                                }
                            }

                            ControlPlaceHolder.Controls.Add(lstControl); // Dropdown list control

                            Literal literalSpacer = new Literal();
                            literalSpacer.Text = "<br />";
                            
                            // Add controls to the place holder
                            ControlPlaceHolder.Controls.Add(literalSpacer);
                        }
                    }
                }

                // Add the literal control to the Controls collection of the PlaceHolder control.
                literal = new Literal();
                literal.Text = "</div>";
                ControlPlaceHolder.Controls.Add(literal);
            }
        }

        /// <summary>
        /// Returns Quantity on Hand (inventory stock value)
        /// </summary>
        protected void CheckInventory()
        {
            this._product = this.Product;

            if (this._product.ProductID > 0)
            {
                if (this._product.QuantityOnHand == 0)
                {
                    QuantityRangeValidator.MaximumValue = "0";
                }
                else
                {
                    QuantityRangeValidator.MaximumValue = this._product.QuantityOnHand.ToString();

                    // Get Current Qunatity by Subtracting QunatityOrdered from Quantity On Hand(Inventory)
                    int CurrentQuantity = this._product.QuantityOnHand - this.GetQuantityOrdered(this._product);

                    if (CurrentQuantity <= 0)
                    {
                        CurrentQuantity = 0;
                    }

                    QuantityRangeValidator.MaximumValue = CurrentQuantity.ToString();
                }
            }
            else
            {
                QuantityRangeValidator.MaximumValue = "1";
            }
        }

        #endregion

        #region Helper Methods
        /// <summary>
        /// Check whether catalog image is exists or not
        /// If exists, then return true otherwise false
        /// </summary>
        /// <param name="filePath">Path of the File</param>
        /// <returns>Returns a bool value whether to show the catalod image</returns>
        protected bool ShowCatalogImage(string filePath)
        {
            // FileInfo object
            System.IO.FileInfo Info = new System.IO.FileInfo(Server.MapPath(filePath));
            
            // If file exists
            if (Info.Exists) 
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns boolean value to enable Validator to check against the Quantity textbox
        /// It checks for AllowBackOrder and TrackInventory settings for this product,and returns the value
        /// </summary>
        /// <param name="product">Product Instance</param>
        protected void EnableStockValidator(ZNodeProduct product)
        {
            this._product = product;

            if (this._product.ProductID > 0)
            {
                // Allow Back Order
                if (this._product.AllowBackOrder && this._product.TrackInventoryInd)
                {
                    QuantityRangeValidator.Enabled = false;
                    return;
                }
                else if ((!this._product.AllowBackOrder) && (!this._product.TrackInventoryInd))
                {
                    // Don't track inventory
                    QuantityRangeValidator.Enabled = false;
                    return;
                }

                // Enable validator
                this.CheckInventory();
                QuantityRangeValidator.Enabled = true;
            }
            else
            {
                QuantityRangeValidator.Enabled = false;
            }
        }

        /// <summary>
        /// Represents the BindProducts method
        /// </summary>
        private void BindProducts()
        {
            int currentPageIndex = int.Parse(PageIndex.Value);
            int totalPages = 0;

            if (!string.IsNullOrEmpty(txtProductName.Text.Trim()) || !string.IsNullOrEmpty(txtProductSku.Text.Trim()) || !string.IsNullOrEmpty(txtProductNum.Text.Trim()) || !string.IsNullOrEmpty(txtBrand.Text.Trim()) || !string.IsNullOrEmpty(txtCategory.Text.Trim()))
            {
                // Get Products            
                DataTable productList = ZNodeProductList.GetPagedProductListByPortalID(ZNodeConfigManager.SiteConfig.PortalID, txtProductName.Text.Trim(), txtProductNum.Text.Trim(), txtProductSku.Text.Trim(), txtBrand.Text.Trim(), txtCategory.Text.Trim(), currentPageIndex, uxGrid.PageSize, ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.CatalogConfig.CatalogID, out totalPages);

                TotalPages.Value = totalPages.ToString();

                if (productList.Rows.Count > 0)
                {
                    pnlProductList.Visible = true;

                    uxGrid.DataSource = productList;
                    uxGrid.DataBind();

                    if (totalPages > 1)
                    {
                        NextPageLink.Enabled = true;
                        LastPageLink.Enabled = true;
                    }
                }
                else
                {
                    lblSearhError.Text = Resources.CommonCaption.NoDataFound;
                }
            }
            else
            {
                lblSearhError.Text = this.GetLocalResourceObject("SearhError").ToString();
            }
        }

        /// <summary>
        /// Returns Total Qunatity ordered for each Shopping cart Item
        /// </summary>
        /// <param name="product">Product Instance</param>
        /// <returns>Returns the no of quantity ordered</returns>
        private int GetQuantityOrdered(ZNodeProduct product)
        {
            int QuantityOrdered = 0;

            if (Session["CartItemCollectionObject"] != null)
            {
                ZNodeGenericCollection<ZNodeShoppingCartItem> CartItems = (ZNodeGenericCollection<ZNodeShoppingCartItem>)HttpContext.Current.Session["CartItemCollectionObject"];

                // loop through Shopping cart items
                foreach (ZNodeShoppingCartItem item in CartItems)
                {
                    if (item.Product.ProductID == product.ProductID && product.ProductID > 0)
                    {
                        // Check Product has any attributes
                        if (item.Product.SelectedSKU.SKUID > 0)
                        {
                            if (item.Product.SelectedSKU.SKUID == product.SelectedSKU.SKUID && product.SelectedSKU.SKUID > 0)
                            {
                                if (this.RowIndex > -1)
                                {
                                    if (item.GUID != CartItems[this.RowIndex].GUID)
                                    {
                                        QuantityOrdered += item.Quantity;
                                    }
                                }
                                else
                                {
                                    QuantityOrdered += item.Quantity;
                                }
                            }
                        }
                        else
                        {
                            if (this.RowIndex > -1)
                            {
                                if (item.GUID != CartItems[this.RowIndex].GUID)
                                {
                                    QuantityOrdered += item.Quantity;
                                }
                            }
                            else
                            {
                                QuantityOrdered += item.Quantity;
                            }
                        }
                    }
                }
            }

            return QuantityOrdered;
        }

        /// <summary>
        /// Validate this product
        /// </summary>
        /// <param name="product">Product Instance</param>
        /// <returns>Returns bool value to validate Product or not</returns>
        private bool ValidateProduct(ZNodeProduct product)
        {
            if (product != null)
            {
                if (product.ZNodeAttributeTypeCollection.Count == 0)
                {
                    if (product.QuantityOnHand == 0 && product.ProductID > 0 && (!product.AllowBackOrder) && product.TrackInventoryInd)
                    {
                        uxStatus.Text = product.OutOfStockMsg;
                        return false;
                    }
                }
                else
                {
                    string attributeIds = string.Empty;

                    for (int i = 0; i < product.ZNodeAttributeTypeCollection.Count; i++)
                    {
                        if (attributeIds.Length > 0)
                        {
                            attributeIds += ",";
                        }

                        attributeIds += "0";
                    }

                    if (product.SelectedSKU.SKUID == 0 || product.SelectedSKU.QuantityOnHand == 0)
                    {
                        if (product.SelectedSKU.AttributesValue == attributeIds)
                        {
                            uxStatus.Text = "Please select another color, size or width.";
                        }
                        else if (product.SelectedSKU.AttributesValue.Contains("0"))
                        {
                            uxStatus.Text = "Please select another color, size or width.";
                        }
                        else
                        {
                            uxStatus.Text = product.OutOfStockMsg;
                        }

                        return false;
                    }
                }

                if (product.ZNodeAddOnCollection.Count > 0)
                {
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    System.Text.StringBuilder seletecAddOns = new System.Text.StringBuilder();
                    System.Text.StringBuilder optionalAddOns = new System.Text.StringBuilder();

                    foreach (ZNodeAddOn AddOn in product.ZNodeAddOnCollection)
                    {
                        if (sb.Length > 0)
                        {
                            if (!AddOn.OptionalInd)
                            {
                                sb.Append(",");
                            }
                        }

                        if (optionalAddOns.Length > 0)
                        {
                            optionalAddOns.Append(",");
                        }

                        if (AddOn.OptionalInd)
                        {
                            optionalAddOns.Append(AddOn.AddOnID);
                        }
                        else 
                        { 
                            sb.Append(AddOn.AddOnID); 
                            optionalAddOns.Append(AddOn.AddOnID); 
                        }
                    }

                    foreach (ZNodeAddOn AddOn in product.SelectedAddOnItems.ZNodeAddOnCollection)
                    {
                        if (seletecAddOns.Length > 0)
                        {
                            seletecAddOns.Append(",");
                        }

                        seletecAddOns.Append(AddOn.AddOnID);
                    }

                    if (sb.ToString() == seletecAddOns.ToString() || seletecAddOns.ToString() == optionalAddOns.ToString())
                    {
                    }
                    else
                    {
                        uxStatus.Text = "Select required Add-On" + "<br />";
                        return false;
                    }

                    foreach (ZNodeAddOn AddOn in product.SelectedAddOnItems.ZNodeAddOnCollection)
                    {
                        // Loop through the Add-on values for each Add-on
                        foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                        {
                            // Check for quantity on hand and back-order,track inventory settings
                            if (AddOnValue.QuantityOnHand <= 0 && (!AddOn.AllowBackOrder) && AddOn.TrackInventoryInd)
                            {
                                uxStatus.Text = "A Required Add-On \'" + AddOn.Name + "\' is out of stock" + "<br />";
                                return false;
                            }
                        }
                    }
                }

                if (product.ProductID > 0)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Represents the AddToList method
        /// </summary>
        private void AddToList()
        {
            // Get product object from Cache object
            this._product = this.Product;
            int Quantity = Convert.ToInt32(Qty.SelectedValue);

            if (!this.ValidateProduct(this._product) && Quantity > 0 && this._product.ProductID > 0)
            {
                this.DisplayProductGrid(this._product);
                return;
            }

            if (Session["CartItemCollectionObject"] != null)
            {
                ZNodeGenericCollection<ZNodeShoppingCartItem> CartItems = (ZNodeGenericCollection<ZNodeShoppingCartItem>)HttpContext.Current.Session["CartItemCollectionObject"];

                if (this._product.ProductID == 0)
                {
                    if (this.RowIndex > -1)
                    {
                        CartItems.RemoveAt(this.RowIndex);
                    }
                }
                else if (this.RowIndex == -1) 
                {
                    // If "Add item" is selected
                    // Add these item if Quantity is greater than zero
                    if (Quantity > 0)
                    {
                        ZNodeShoppingCartItem Item = new ZNodeShoppingCartItem();
                        Item.Quantity = Quantity;
                        Item.Product = this._product;
                        CartItems.Add(Item);
                    }
                }
                else 
                {
                    // Product edit mode
                    if (Quantity == 0)
                    {
                        // Remove item from shopping cart object
                        CartItems.RemoveAt(this.RowIndex);
                    }
                    else
                    {
                        // Update Product Quantity
                        CartItems[this.RowIndex].Quantity = Quantity;
                        CartItems[this.RowIndex].Product = this._product;
                    }
                }
            }
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind the MaxQuantity value to the Dropdown list
        /// </summary>
        private void BindQuantityList()
        {
            int max = 0;

            // If Max quantity is not set in admin , set it to 10
            if (this._product.MaxQty == 0)
            {
                max = 10;
            }
            else
            {
                max = this._product.MaxQty;
            }

            ArrayList Quantity = new ArrayList();

            for (int i = 1; i <= max; i++)
            {
                Quantity.Add(i);
            }

            Qty.DataSource = Quantity;
            Qty.DataBind();

            Qty.SelectedValue = this.ShoppingCartQuantity.ToString();
        }

        /// <summary>
        /// Represent the Bind data method
        /// </summary>
        private void Bind()
        {
            this._product = this.Product;

            if (this._product.ProductID > 0)
            {
                ProductDescription.Text = this._product.Description;

                this.BindQuantityList();

                // Show Price
                this.DisplayPrice();

                tablerow.Visible = true;
            }
            else
            {
                this.ResetUI();
            }
        }

        #endregion
    }
}