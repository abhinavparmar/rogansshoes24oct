<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="WebApp.Controls_Default_Reviews_ProductReviews" CodeBehind="ProductReviews.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="Znode" %>

<div id="Review">
    <asp:UpdatePanel ID="UpdatePnlProductReviewDetails" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="Sorting">
                <span class="TitleStyle">
                    <asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtReviewedBy" EnableViewState="false"></asp:Localize>
                    <asp:Label ID="CustomersCnt" runat="server" meta:resourcekey="CustomersCntResource1" EnableViewState="false"></asp:Label>
                    <asp:Localize ID="Localize1" runat="server" meta:resourceKey="txtcustomer" EnableViewState="false"></asp:Localize>
                </span>&nbsp; <span>
                    <asp:DropDownList ID="RateList" runat="server" OnSelectedIndexChanged="RateList_SelectedIndexChanged"
                        CssClass="SortByDropdown" AutoPostBack="True" meta:resourcekey="RateListResource1" title="RateLista">
                        <asp:ListItem Value="desc" Selected="True" meta:resourcekey="ListItemResource1"></asp:ListItem>
                        <asp:ListItem Text="Oldest First" Value="asc" meta:resourcekey="ListItemResource2"></asp:ListItem>
                        <asp:ListItem Text="Highest Rating First" Value="5" meta:resourcekey="ListItemResource3"></asp:ListItem>
                        <asp:ListItem Text="Lowest Rating First" Value="1" meta:resourcekey="ListItemResource4"></asp:ListItem>
                    </asp:DropDownList>
                </span>
            </div>
            <div>
                <asp:DataList ID="ReviewList" runat="server" CellPadding="-1" CellSpacing="-1" ClientIDMode="Static" RepeatLayout="Flow">
                    <ItemTemplate>
                        <div class="ItemStyle" itemprop="review" itemscope itemtype="http://schema.org/Review">
                            <div>
                                <span class="Subject">
                                    <%# DataBinder.Eval(Container.DataItem,"Subject") %></span>,<br />
                                <span itemprop="datePublished" content="<%# DataBinder.Eval(Container.DataItem, "CreateDate") %>">
                                    <%# GetFormattedDate(DataBinder.Eval(Container.DataItem, "CreateDate"))%></span>
                            </div>
                            <div class="Content">
                                <div class="StarRating">
                                    <asp:Literal ID="ltStartImage" runat="server" Text='<%# GetRatingImagePath(Eval("Rating").ToString()) %>' EnableViewState="false"></asp:Literal>
                                </div>
                                <div id="divPros" visible='<%# ShowProsField %>' runat="server" class="Row">
                                    <span class="FieldStyle">Pros:</span><span class="ValueStyle"><%# Eval("Pros") %></span></div>
                                <div id="divCons" visible='<%# ShowConsField %>' runat="server" class="Row">
                                    <span class="FieldStyle">Cons:</span><span class="ValueStyle"><%# DataBinder.Eval(Container.DataItem,"Cons")  %></span></div>
                                <div class="Row">
                                    <%# DataBinder.Eval(Container.DataItem,"Comments").ToString() %>
                                </div>
                                <div class="Row">
                                    By <span class="Text" itemprop="author">
                                        <%# DataBinder.Eval(Container.DataItem,"CreateUser")%></span> from <span class="Text">
                                            <%# DataBinder.Eval(Container.DataItem, "UserLocation")%></span>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                    <ItemStyle CssClass="ReviewItem" BorderStyle="None" />
                </asp:DataList>
                <%--<asp:Repeater ID="rptReviewList" runat="server" ClientIDMode="Static">
                    <HeaderTemplate>
                        <table id="ReviewList">
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <div class="ItemStyle" itemprop="review" itemscope itemtype="http://schema.org/Review">
                                    <div>
                                        <span class="Subject">
                                            <%# DataBinder.Eval(Container.DataItem,"Subject") %></span>,<br />
                                        <span itemprop="datePublished" content="<%# DataBinder.Eval(Container.DataItem, "CreateDate") %>">
                                            <%# GetFormattedDate(DataBinder.Eval(Container.DataItem, "CreateDate"))%></span>
                                    </div>
                                    <div class="Content">
                                        <div class="StarRating">
                                            <asp:Literal ID="ltStartImage" runat="server" Text='<%# GetRatingImagePath(Eval("Rating").ToString()) %>' EnableViewState="false"></asp:Literal>
                                        </div>
                                        <div id="divPros" visible='<%# ShowProsField %>' runat="server" class="Row">
                                            <span class="FieldStyle">Pros:</span><span class="ValueStyle"><%# Eval("Pros") %></span>
                                        </div>
                                        <div id="divCons" visible='<%# ShowConsField %>' runat="server" class="Row">
                                            <span class="FieldStyle">Cons:</span><span class="ValueStyle"><%# DataBinder.Eval(Container.DataItem,"Cons")  %></span>
                                        </div>
                                        <div class="Row">
                                            <%# DataBinder.Eval(Container.DataItem,"Comments").ToString() %>
                                        </div>
                                        <div class="Row">
                                            By <span class="Text" itemprop="author">
                                                <%# DataBinder.Eval(Container.DataItem,"CreateUser")%></span> from <span class="Text">
                                                    <%# DataBinder.Eval(Container.DataItem, "UserLocation")%></span>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>--%>
            </div>
            <div class="Paging">
                <div id="topNavigation" runat="server">
                    <asp:LinkButton ID="TopPrevLink" runat="server" OnClick="PrevRecord" Text="<%$ Resources:CommonCaption, Prev%>" EnableViewState="false"></asp:LinkButton>
                    <asp:Label ID='lblPagingInfo' runat='server' ClientIDMode="Static"></asp:Label>
                    <asp:LinkButton ID="TopNextLink" runat="server" OnClick="NextRecord" Text="<%$ Resources:CommonCaption, Next%>" ClientIDMode="Static" EnableViewState="false"></asp:LinkButton>
                </div>
            </div>
            <div class="Spacer">
                &nbsp;
            </div>
            <div>
                <asp:Label ID="lblErrorMsg" EnableViewState="false" runat="server" meta:resourcekey="lblErrorMsgResource1" ClientIDMode="Static"></asp:Label>
            </div>
            <div class="Spacer">
                &nbsp;
            </div>
            <br />
            <Znode:CustomMessage ID="ucProductReviewMessage" runat="server" MessageKey="ProductReviewInformation" Visible="false" />
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
