﻿<%@ Control Language="C#" EnableViewState="False" AutoEventWireup="true" CodeBehind="TopSearch.ascx.cs" Inherits="WebApp.Controls.Default.SearchEngine.TopSearch" %>
<div class="SearchSection">
    <div id="SearchEngine">
        <asp:Panel ID="pnlTopSearch" runat="server" DefaultButton="btnSearch">
            <script>
                var searchURL = '<%= Page.ResolveUrl("~/GetSuggestions.asmx/TypeAhead") %>';
            </script>
            <div class="RoundedBox">
                <div style="float: left; display: none;">
                    <select name="ddlCategory" id="ddlCategory" tabindex="0" height="20px" width="130px" title="Category">
                        <option value="<%=GetSearchCategory() %>" selected="selected"></option>
                    </select>
                </div>
                <asp:TextBox ID="SearchText" CssClass="TextBoxPosition" runat="server"
                    ClientIDMode="Static" Text="Search by SKU# or Keyword"
                    title="Search by SKU# or Keyword"
                    EnableViewState="false" alt="Search by SKU# or Keyword"></asp:TextBox>
            </div>
            <asp:ImageButton ID="btnSearch" runat="server" CssClass="Button" CausesValidation="false" EnableViewState="false" OnClientClick="return zeonTopSearch.btnSearch_Click();" ImageUrl="~/Themes/CheerAndPom/Images/transper-img.png" AlternateText="Search" ToolTip="Search" />
            <asp:Label ID="lblSearchTitle" CssClass="SearchTitle" Text="Search" runat="server"></asp:Label>
            <%--<asp:HiddenField ID="hdneditMode" runat="server" Value="" ClientIDMode="Static" />--%>
            <input type="hidden" value="" id="hdneditMode" />
        </asp:Panel>
    </div>
</div>
