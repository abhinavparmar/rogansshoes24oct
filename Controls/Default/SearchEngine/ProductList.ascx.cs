﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.ECommerce.Catalog;
using System.Data;
using ZNode.Libraries.Ecommerce.Entities;
using System.Text;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.Admin;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using System.Configuration;

namespace WebApp.Controls.Default.SearchEngine
{
    public partial class ProductList : System.Web.UI.UserControl
    {

        #region Private Variables
        private ZNodeProfile _ProductListProfile = new ZNodeProfile();
        private ZNodeProductList _ProductList;
        private List<int> _ProductId;
        private int RecCount = 0;
        private int Currentpage = 0;
        private string category = string.Empty;
        private string searchText = string.Empty;
        private string sortOrder = string.Empty;
        private string sortDirection = string.Empty;
        //Zeon Custom Code Start
        private string _view = string.Empty; //Zeon Custom Code
        private string _listActiveURL = "~/Themes/" + ZNodeCatalogManager.Theme + "/Images/list-active.gif";
        private string _gridDeActiveURL = "~/Themes/" + ZNodeCatalogManager.Theme + "/Images/grid.gif";
        private string _gridActiveURL = "~/Themes/" + ZNodeCatalogManager.Theme + "/Images/grid-active.gif";
        private string _listDeActiveURL = "~/Themes/" + ZNodeCatalogManager.Theme + "/Images/list.gif";
        //Zeon Custom Code End

        string sessionKey = string.Empty;
        string isAsyncLoad = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["EnableAsyncLoad"]) ? ConfigurationManager.AppSettings["EnableAsyncLoad"].ToString() : "0";
        #endregion

        // Public Event Handler
        public event System.EventHandler PagingSelectedIndexChanged;

        public event System.EventHandler SortingSelectedIndexChanged;

        public event System.EventHandler PreviousButtonClicked;

        public event System.EventHandler NextButtonClicked;

        #region Public Properties

        /// <summary>
        /// Gets or sets the product list profile
        /// </summary>
        public ZNodeProfile ProductListProfile
        {
            get { return this._ProductListProfile; }
            set { this._ProductListProfile = value; }
        }

        /// <summary>
        /// Gets the Category 
        /// </summary>
        public string Category
        {
            get
            {
                category = string.Empty;

                if (Request.QueryString["category"] != null)
                {
                    category = Request.QueryString["category"].ToString();
                }

                return category;
            }
        }


        /// <summary>
        /// Gets the SearchText 
        /// </summary>
        public string SearchText
        {
            get
            {
                searchText = string.Empty;

                if (Request.QueryString["text"] != null)
                {
                    searchText = HttpContext.Current.Server.UrlDecode(Request.QueryString["text"].ToString());
                }

                return searchText;
            }
        }

        /// <summary>
        /// Gets the current page
        /// </summary>
        public int CurrentPage
        {
            get
            {
                int currentPage = 1;

                if (Request.QueryString["page"] != null)
                {
                    currentPage = Convert.ToInt32(Request.QueryString["page"]);
                }

                return currentPage;
            }
        }

        /// <summary>
        /// Gets or sets the Total Records
        /// </summary>
        public int TotalRecords
        {
            get
            {
                int totalRecords = 0;

                if (ViewState["TotalRecords"] != null)
                {
                    totalRecords = (int)ViewState["TotalRecords"];
                }

                return totalRecords;
            }

            set
            {
                ViewState["TotalRecords"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the Total Pages
        /// </summary>
        public int TotalPages
        {
            get
            {
                int totalPages = 0;

                if (ViewState["TotalPages"] != null)
                {
                    totalPages = (int)ViewState["TotalPages"];
                }

                return totalPages;
            }

            set
            {
                ViewState["TotalPages"] = value;
            }
        }

        /// <summary>
        /// Gets the search list page pize
        /// </summary>
        public int PageSize
        {
            get
            {
                int pageSize = 48;

                string eventTarget = "__EVENTTARGET";
                if (Request.QueryString["s"] != null)
                {
                    pageSize = Convert.ToInt32(Request.QueryString["s"]);
                }
                else if (ddlTopPaging.SelectedIndex > 0 || ddlBottomPaging.SelectedIndex > 0)
                {
                    if (Request[eventTarget] != null && Request[eventTarget].Contains(ddlTopPaging.ID))
                    {
                        pageSize = Convert.ToInt32(ddlTopPaging.SelectedValue);
                    }
                    else
                    {
                        pageSize = Convert.ToInt32(ddlBottomPaging.SelectedValue);
                    }
                }
                else
                    pageSize = 48;

                return pageSize;
            }
        }

        /// <summary>
        /// Gets the sort option
        /// </summary>
        public int SortOption
        {
            get
            {
                if (lstFilter.SelectedValue.Equals("2"))
                {
                    return 2;
                }

                // Apply Sorting Only Page gets Reload.
                if (!lstFilter.SelectedValue.Equals("0"))
                {
                    return 1;
                }

                return 0;
            }
        }

        /// <summary>
        /// Gets or sets the  product list passed in from the search page
        /// </summary>
        public ZNodeProductList ProductSearchList
        {
            get
            {
                return this._ProductList;
            }

            set
            {
                this._ProductList = value;
            }
        }

        public List<int> ProductID
        {
            get
            {
                return this._ProductId;
            }

            set
            {
                this._ProductId = value;
            }
        }
        #endregion

        #region Private Properties
        /// <summary>
        /// Gets or sets the Last Index
        /// </summary>
        private int LastIndex
        {
            get
            {
                int lastIndex = 0;

                if (ViewState["LastIndex"] != null)
                {
                    lastIndex = Convert.ToInt32(ViewState["LastIndex"]);
                }

                return lastIndex;
            }

            set
            {
                ViewState["LastIndex"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the First Index
        /// </summary>
        private int FirstIndex
        {
            get
            {
                int firstIndex = 0;

                if (ViewState["FirstIndex"] != null)
                {
                    firstIndex = Convert.ToInt32(ViewState["FirstIndex"]);
                }

                return firstIndex;
            }

            set
            {
                ViewState["FirstIndex"] = value;
            }
        }
        #endregion

        #region Protected Methods and Events

        /// <summary>
        /// Set the page size to selected DropDownList value size. If "SHOW ALL" selected then display all products.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlTopPaging_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.NavigationToUrl(1, Convert.ToInt32(ddlTopPaging.SelectedValue), lstFilter);
        }

        /// <summary>
        /// Set the page size to selected DropDownList value size. If "SHOW ALL" selected then display all products.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlBottomPaging_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.NavigationToUrl(1, Convert.ToInt32(ddlBottomPaging.SelectedValue), LstFilterBottom);
        }

        /// <summary>
        /// Event is raised when LstFilter control Selected Index is Changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.NavigationToUrl(1, Convert.ToInt32(ddlBottomPaging.SelectedValue), lstFilter);
        }


        protected void LstFilterBottom_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.NavigationToUrl(1, Convert.ToInt32(ddlBottomPaging.SelectedValue), LstFilterBottom);
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["sort"] != null)
                {
                    lstFilter.SelectedValue = Request.QueryString["sort"].ToString();
                    LstFilterBottom.SelectedValue = Request.QueryString["sort"].ToString();
                }
                if (Request.QueryString["s"] != null)
                {
                    int size = Convert.ToInt32(Request.QueryString["s"].ToString());
                    ddlTopPaging.SelectedIndex = ddlTopPaging.Items.IndexOf(ddlTopPaging.Items.FindByValue(size.ToString()));
                    ddlBottomPaging.SelectedIndex = ddlTopPaging.SelectedIndex;
                }
            }
            //Zeon Custom Code: Start
            if (Request.Params["v"] != null)
            {
                _view = Request.Params["v"];
            }
            else
            {
                _view = "g";
                imgbtnGrid.Enabled = false;
                imgGrid1.Enabled = false;
            }
            this._ProductList = new ZNodeProductList();
            //this._ProductList = ZNodePaging.GetProducts(ProductID, GetSortField(Request.QueryString["sort"]), GetSortDirection(Request.QueryString["sort"]), this.CurrentPage, this.PageSize, 1);//Old Code
            this._ProductList = ZNodePaging.GetProductsExtn(ProductID, GetSortField(Request.QueryString["sort"]), GetSortDirection(Request.QueryString["sort"]), this.CurrentPage, this.PageSize, 1);//Zeon Custom function call with all sorting enabled.

            //PRFT Custom Code : Start
            if (this._ProductList != null && this._ProductList.ZNodeProductCollection.Count == 1)
            {
                if (this._ProductList.ZNodeProductCollection != null && this._ProductList.ZNodeProductCollection[0] != null)
                {
                    if (!string.IsNullOrEmpty(this._ProductList.ZNodeProductCollection[0].SEOURL))
                    {
                        Response.Redirect("~/" + this._ProductList.ZNodeProductCollection[0].SEOURL);
                    }
                    else
                    {
                        Response.Redirect("~/product.aspx?zpid=" + this._ProductList.ZNodeProductCollection[0].ProductID);
                    }
                }
            }
            //PRFT Custom Code : End


            int previousPageIndex = this.Currentpage;

            if (previousPageIndex < this.CurrentPage)
            {
                if (this.NextButtonClicked != null)
                {
                    this.NextButtonClicked(sender, e);
                }
            }
            else
            {
                if (this.PreviousButtonClicked != null)
                {
                    this.PreviousButtonClicked(sender, e);
                }
            }

            this.BindDataListPagedDataSource();

            string baseUrl = this.GetNavigationUrl();

            //// Do not add the & or ? in the following query string.//Znode Old Code
            //hlTopPrevLink.NavigateUrl = hlBotPrevLink.NavigateUrl = string.Format("{0}category={1}&text={2}&page={3}&size={4}&sort={5}&v={6}", baseUrl, Category, SearchText, this.CurrentPage - 1, ddlTopPaging.SelectedValue, lstFilter.SelectedValue,_view);
            //hlTopNextLink.NavigateUrl = hlBotNextLink.NavigateUrl = string.Format("{0}category={1}&text={2}&page={3}&size={4}&sort={5}&v={6}", baseUrl, Category, SearchText, this.CurrentPage + 1, ddlTopPaging.SelectedValue, lstFilter.SelectedValue,_view);
            //Zeon Custom Code :Starts
            hlTopPrevLink.NavigateUrl = hlBotPrevLink.NavigateUrl = NavigationToNextPageUrl(this.CurrentPage - 1, ddlTopPaging.SelectedValue, lstFilter.SelectedValue);
            hlTopNextLink.NavigateUrl = hlBotNextLink.NavigateUrl = NavigationToNextPageUrl(this.CurrentPage + 1, ddlTopPaging.SelectedValue, lstFilter.SelectedValue);
            //Zeon Custom Code :Ends

            this.Page.Title = GetLocalResourceObject("txtSearchResultTitle.Text").ToString();
        }

        /// <summary>
        /// Page Prerender event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            RegisterClientScriptClasses();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Navigate to the category page with query string the specified current page and page size.
        /// </summary>
        /// <param name="currentPage">Current page index.</param>
        /// <param name="pageSize">Category page size. If Show All selected then -1 will be used.</param>
        private void NavigationToUrl(int currentPage, int pageSize, DropDownList ddlSort)
        {
            string baseUrl = this.GetNavigationUrl();
            string faceturl = "";
            string customQueryStringParam = ConfigurationManager.AppSettings["SEOQuerystringKeys"] != null ? ConfigurationManager.AppSettings["SEOQuerystringKeys"].ToString() : string.Empty;
            foreach (String key in Request.QueryString.AllKeys)
            {
                if (key.ToLower() == "category" || key.ToLower() == "text" || key.ToLower() == "page" || key.ToLower() == "s" || key.ToLower() == "sort" || key.ToLower() == "v" || customQueryStringParam.Split(',').Contains(key.ToLower().ToString()))
                    continue;


                if (Request.QueryString[key].Contains(","))
                {
                    string[] facets = Request.QueryString[key].Split(',');
                    foreach (string facet in facets)
                    {
                        faceturl = faceturl + "&" + key + "=" + HttpUtility.UrlEncode(facet);
                    }
                }
                else
                {
                    faceturl = faceturl + "&" + key + "=" + HttpUtility.UrlEncode(Request.QueryString[key]);
                }
            }

            //Zeon Custom Code :Start
            string view = "g";
            if (imgGrid1.CssClass == "ActiveLink")
            {
                view = "g";
            }
            else
            {
                view = "l";
            }


            // Do not add the & or ? in the following query string.
            //string navigateUrl = string.Format("{0}category={1}&text={2}&page={3}&size={4}&sort={5}&v={6}", baseUrl, category, searchText, currentPage, pageSize, ddlSort.SelectedValue, view);//Zeon:Custom Removal of List view,grid view
            string navigateUrl = string.Format("{0}category={1}&text={2}&page={3}&s={4}&sort={5}", baseUrl, category, HttpContext.Current.Server.UrlEncode(searchText), currentPage, pageSize, ddlSort.SelectedValue);
            //Zeon Custom Code :End
            Response.Redirect(navigateUrl + faceturl);
        }

        /// <summary>
        /// Gets the navigation url for the previous/next navigation link
        /// </summary>
        /// <returns>Returns the navigation base url.</returns>
        private string GetNavigationUrl()
        {
            string baseUrl = string.Empty;
            if (this.Request.QueryString.Count > 0)
            {
                StringBuilder url = new StringBuilder();
                url.Append(Request.Url.GetLeftPart(UriPartial.Authority));
                url.Append(Request.Url.AbsolutePath);

                // Append the category Url with base url
                if (Request.QueryString["keyword"] != null)
                {
                    url.Append("?keyword=").Append(Request.QueryString["keyword"]).Append("&");
                }
                else
                {
                    url.Append("?");
                }

                baseUrl = url.ToString();
            }
            else
            {
                baseUrl = Request.Url.ToString() + "?";
            }

            return baseUrl;
        }

        /// <summary>
        /// Bind Datalist using Paged DataSource object
        /// </summary>
        private void BindDataListPagedDataSource()
        {
            //Zeon Custom Code : Start
            if (Request.QueryString["v"] != null)
            {
                if (Request.QueryString["v"] == "g")
                {
                    SetGridView();
                }
                else
                {
                    SetListView();
                }
            }
            //Zeon Custom Code : End

            // Retrieve collection object from Viewstate
            if (this._ProductList == null)
            {
                return;
            }

            // Assigning Datasource to the DataList.
            if (isAsyncLoad.Equals("1"))
            {
                sessionKey = "CategoryProductList" + Guid.NewGuid();
                HttpContext.Current.Session[sessionKey] = this._ProductList.ZNodeProductCollection;
            }

            // Assigning Datasource to the DataList.
            DataListProducts.DataSource = this._ProductList.ZNodeProductCollection;
            DataListProducts.DataBind();
            //Zeon Custom Code: Start
            if (this._ProductList.TotalRecordCount > 0)
            {
                ltrTopTotalCounts.Text = Convert.ToString(this._ProductList.TotalRecordCount);
                ltrBottomTotalCount.Text = Convert.ToString(this._ProductList.TotalRecordCount);
                locBottomPageNum.Text = locTopPageNum.Text = string.Format(this.GetLocalResourceObject("txtPage").ToString(), this.CurrentPage);
                locTopPageTotal.Text = locBottomPageTotal.Text = string.Format(this.GetLocalResourceObject("txtOF").ToString(), this._ProductList.TotalPageCount);
            }
            //Zeon Custom Code: End
            // Disable the view state for the data list item. 
            foreach (DataListItem item in DataListProducts.Items)
            {
                item.EnableViewState = false;
            }

            this.Currentpage = this.CurrentPage;
            this.RecCount = this.TotalPages = this._ProductList.TotalPageCount;
            this.TotalRecords = this._ProductList.TotalRecordCount;

            hlTopNextLink.Enabled = hlBotNextLink.Enabled = !(this.Currentpage == this.RecCount);
            hlTopPrevLink.Enabled = hlBotPrevLink.Enabled = !(this.Currentpage == 1);

            if (!this.IsPostBack)
            {
                this.DoPaging();
            }

            if (this._ProductList.ZNodeProductCollection.Count == 0)
            {
                pnlProductList.Visible = false;
                ErrorMsg.Visible = true;
                SearchMessage.Text = GetSearchResultMessage(Category, SearchText, ProductID);
            }
            else
            {
                pnlProductList.Visible = true;
                ErrorMsg.Visible = false;
                SearchMessage.Visible = true;
                divSearchResult.Attributes.Add("class", "uxMsg SuccessSection");
                SearchMessage.Text = GetSearchResultMessage(Category, SearchText, ProductID);
            }
        }

        private string GetSearchResultMessage(string category, string searchText, List<int> productID)
        {
            if (productID.Count() == 0 && !string.IsNullOrEmpty(SearchText))
            {
                return string.Format("We found 0 results for \"{0}\"", SearchText);
            }
            else if (ProductID.Count() > 0 && string.IsNullOrEmpty(category) && !string.IsNullOrEmpty(searchText))
            {
                return string.Format("We found {0} results for \"{1}\"", this.ProductID.Count(), searchText);
            }
            else if (ProductID.Count() > 0 && !string.IsNullOrEmpty(category) && !string.IsNullOrEmpty(searchText))
            {
                return string.Format("We found {0} results for \"{1}\" in {2} ", this.ProductID.Count(), searchText, category);
            }
            else if (ProductID.Count() > 0 && string.IsNullOrEmpty(category) || string.IsNullOrEmpty(searchText))
            {
                return string.Format("We found {0} results", this.ProductID.Count());
            }

            return null;
        }




        /// <summary>
        /// Binding Paging List
        /// </summary>
        private void DoPaging()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("PageIndex");
            dt.Columns.Add("PageText");

            this.FirstIndex = this.CurrentPage - 5;

            if (this.CurrentPage > 5)
            {
                this.LastIndex = this.CurrentPage + 5;
            }
            else
            {
                this.LastIndex = 10;
            }

            if (this.LastIndex > this.TotalPages)
            {
                this.LastIndex = this.TotalPages;
                this.FirstIndex = this.LastIndex - 10;
            }

            if (this.FirstIndex < 0)
            {
                this.FirstIndex = 0;
            }

            ddlTopPaging.Items.Clear();
            ddlBottomPaging.Items.Clear();

            string pagingText = this.GetLocalResourceObject("PagingText").ToString();

            // Each items seperated with & symbol
            string[] pagingItems = pagingText.Split(new char[] { '&' });

            foreach (string pagingItem in pagingItems)
            {
                if (pagingItem.Contains("|") && pagingItem.Trim().Length > 0)
                {
                    // Key value pair seperated with | symbol
                    string[] itemText = pagingItem.Split(new char[] { '|' });
                    DataRow dr = null;
                    dr = dt.NewRow();
                    dr[0] = Convert.ToInt32(itemText[1].Trim());
                    dr[1] = itemText[0].Trim();
                    dt.Rows.Add(dr);
                }
            }

            this.ddlTopPaging.DataTextField = "PageText";
            this.ddlTopPaging.DataValueField = "PageIndex";
            this.ddlTopPaging.DataSource = dt;
            this.ddlTopPaging.DataBind();

            this.ddlBottomPaging.DataTextField = "PageText";
            this.ddlBottomPaging.DataValueField = "PageIndex";
            this.ddlBottomPaging.DataSource = dt;
            this.ddlBottomPaging.DataBind();

            if (Request.QueryString["s"] != null)
            {
                int size = Convert.ToInt32(Request.QueryString["s"].ToString());
                ddlTopPaging.SelectedIndex = ddlTopPaging.Items.IndexOf(ddlTopPaging.Items.FindByValue(size.ToString()));
                ddlBottomPaging.SelectedIndex = ddlTopPaging.SelectedIndex;
            }
            else
            {
                string defaultPageSize = "48";
                ddlTopPaging.SelectedIndex = ddlTopPaging.Items.IndexOf(ddlTopPaging.Items.FindByValue(defaultPageSize));
                ddlBottomPaging.SelectedIndex = ddlTopPaging.SelectedIndex;
            }
        }

        /// <summary>
        /// Get Sort Field
        /// </summary>
        private string GetSortField(string sort)
        {
            string sortOrder = string.Empty;

            if (!string.IsNullOrEmpty(sort))
            {
                switch (sort)
                {
                    case "2": return "FinalPrice";
                    case "3": return "FinalPrice";
                    case "4": return "Name";
                    case "5": return "Name";
                    case "6": return "Rating";
                }
            }
            return null;
        }

        /// <summary>
        /// Get Sort Direction
        /// </summary>
        private string GetSortDirection(string sort)
        {
            if (!string.IsNullOrEmpty(sort))
            {
                switch (sort)
                {
                    case "2": return "ASC";
                    case "3": return "DESC";
                    case "4": return "ASC";
                    case "5": return "DESC";
                    case "6": return "DESC";
                }
            }
            return null;
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Represents the CheckFor Call for pricing message
        /// </summary>
        /// <param name="fieldValue">passed the field value </param>
        /// <returns>Returns the Call For pricing message </returns>
        public string CheckForCallForPricing(object fieldValue, object callMessage)
        {
            MessageConfigAdmin mconfig = new MessageConfigAdmin();
            bool Status = bool.Parse(fieldValue.ToString());
            //string message = mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, Convert.ToInt32(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0)), 43);
            string message = mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, ZNodeConfigManager.SiteConfig.PortalID, 43); //zeon custom code

            if (callMessage != null && !string.IsNullOrEmpty(callMessage.ToString()))
            {
                message = callMessage.ToString();
            }

            if (Status)
            {
                return message;
            }
            else if (!this._ProductListProfile.ShowPrice)
            {
                return message;
            }
            else
            {
                return string.Empty;
            }
        }

        #endregion

        #region Zeon Custom Methods

        /// <summary>
        /// Handles imgbtnList_OnClick Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgbtnList_OnClick(object sender, EventArgs e)
        {
            SetListView();
            NavigationToUrl(this.CurrentPage, this.PageSize, lstFilter);
        }

        /// <summary>
        /// Set List view of product list
        /// </summary>
        private void SetListView()
        {
            DataListProducts.RepeatColumns = 1;
            DataListProducts.RepeatDirection = RepeatDirection.Vertical;

            imgbtnGrid.Enabled = true;
            imgGrid1.Enabled = true;

            imgbtnGrid.CssClass = "DeActiveLink GridView";

            imgbtnList.Enabled = false;
            imgListActive1.Enabled = false;

            imgbtnList.CssClass = "ActiveLink ListView";

            imgListActive1.ImageUrl = _listActiveURL;
            imgGrid1.ImageUrl = _gridDeActiveURL;

            imgListActive1.CssClass = "ActiveLink";
            imgGrid1.CssClass = "DeActiveLink";

        }

        /// <summary>
        /// Handles imgbtnGrid_OnClick Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgbtnGrid_OnClick(object sender, EventArgs e)
        {
            SetGridView();
            NavigationToUrl(this.CurrentPage, this.PageSize, lstFilter);
        }

        /// <summary>
        /// Set Grid view of product list
        /// </summary>
        private void SetGridView()
        {
            DataListProducts.RepeatColumns = ZNodeConfigManager.SiteConfig.MaxCatalogDisplayColumns;
            DataListProducts.RepeatDirection = RepeatDirection.Horizontal;

            imgbtnGrid.CssClass = "ActiveLink GridView";
            imgbtnGrid.Enabled = false;
            imgGrid1.Enabled = false;

            imgbtnList.CssClass = "DeActiveLink ListView";
            imgbtnList.Enabled = true;
            imgListActive1.Enabled = true;

            imgListActive1.ImageUrl = _listDeActiveURL;
            imgGrid1.ImageUrl = _gridActiveURL;

            imgListActive1.CssClass = "DeActiveLink";
            imgGrid1.CssClass = "ActiveLink";
        }

        /// <summary>
        /// Represents the GetColorOption method
        /// </summary>
        /// <param name="alternateProductImageCount">Alternate Product Image Count</param>
        /// <returns>Returns the Color option</returns>
        public string GetColorCaption(int alternateProductImageCount)
        {
            if (alternateProductImageCount > 0)
            {
                return "MORE COLORS AVAILABLE ";
            }
            return string.Empty;
        }

        /// <summary>
        /// Get Navigation url with facet for paging Buttons
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <param name="sort"></param>
        /// <returns></returns>
        private string NavigationToNextPageUrl(int currentPage, string pageSize, string sort)
        {
            string baseUrl = this.GetNavigationUrl();
            string faceturl = "";
            int nextPageSize = 0;
            int.TryParse(pageSize, out nextPageSize);
            foreach (String key in Request.QueryString.AllKeys)
            {
                if (key.ToLower() == "category" || key.ToLower() == "text" || key.ToLower() == "page" || key.ToLower() == "s" || key.ToLower() == "sort" || key.ToLower() == "v")
                    continue;
                if (Request.QueryString[key].Contains(","))
                {
                    string[] facets = Request.QueryString[key].Split(',');
                    foreach (string facet in facets)
                    {
                        faceturl = faceturl + "&" + key + "=" + HttpUtility.UrlEncode(facet);
                    }
                }
                else
                {
                    faceturl = faceturl + "&" + key + "=" + HttpUtility.UrlEncode(Request.QueryString[key]);
                }
            }
            string view = "g";
            if (imgGrid1.CssClass == "ActiveLink")
            {
                view = "g";
            }
            else
            {
                view = "l";
            }
            // Do not add the & or ? in the following query string.
            //string navigateUrl = string.Format("{0}category={1}&text={2}&page={3}&s={4}&sort={5}&v={6}", baseUrl, category, searchText, currentPage, nextPageSize, sort, view);
            string navigateUrl = string.Format("{0}category={1}&text={2}&page={3}&s={4}&sort={5}", baseUrl, category, HttpContext.Current.Server.UrlEncode(searchText), currentPage, nextPageSize, sort);

            return navigateUrl + faceturl;
        }

        /// <summary>
        /// set shortdescription visibility
        /// </summary>
        /// <returns>bool</returns>
        protected bool SetShortDescriptionVisibility()
        {
            bool isVisible = false;
            if (Request.Params["v"] != null && !string.IsNullOrEmpty(Request.Params["v"].ToString()))
            {
                if (Request.Params["v"].ToString().Equals("l")) { isVisible = true; }
            }
            return isVisible;
        }

        /// <summary>
        /// Register Client Script
        /// </summary>
        private void RegisterClientScriptClasses()
        {
            StringBuilder quckwatchscript = new StringBuilder();
            quckwatchscript.Append("var quickViewHelper = new QuickViewHelper({");
            quckwatchscript.Append("\"Controls\":{");
            quckwatchscript.AppendFormat("\"{0}\":\"{1}\"", ifzQuickView.ID, ifzQuickView.ClientID);
            quckwatchscript.Append("},\"Messages\":{");
            quckwatchscript.Append("}");
            quckwatchscript.Append("});quickViewHelper.Init();");

            StringBuilder script = new StringBuilder();
            script.Append("var zeonCatProdList = new CategoryProductList({");
            script.Append("\"Controls\":{");
            script.AppendFormat("\"{0}\":\"{1}\"", null, null);
            script.AppendFormat(",\"{0}\":\"{1}\"", null, null);
            script.AppendFormat(",\"{0}\":\"{1}\"", null, null);
            script.AppendFormat(",\"{0}\":\"{1}\"", null, null);
            script.AppendFormat(",\"{0}\":\"{1}\"", null, null);
            script.AppendFormat(",\"{0}\":\"{1}\"", null, null);

            script.AppendFormat(",\"{0}\":\"{1}\"", null, null);
            script.AppendFormat(",\"{0}\":\"{1}\"", null, null);

            script.AppendFormat(",\"{0}\":\"{1}\"", null, null);
            script.AppendFormat(",\"{0}\":\"{1}\"", null, null);
            script.AppendFormat(",\"{0}\":\"{1}\"", null, null);
            script.Append("},\"Messages\":{");
            script.AppendFormat("\"WarningTitle\":\"{0}\"", string.Empty);
            script.AppendFormat(",\"ProdctAddedTitle\":\"{0}\"", string.Empty);
            script.AppendFormat(",\"AddToCompareFailedTitle\":\"{0}\"", string.Empty);
            script.AppendFormat(",\"ProductListSessionKey\":\"{0}\"", sessionKey);
            script.Append("}");
            script.Append("});zeonCatProdList.Init();");

            this.Page.ClientScript.RegisterStartupScript(GetType(), "CategoryProductList", script.ToString(), true);
            this.Page.ClientScript.RegisterStartupScript(GetType(), "QuickwatchScript", quckwatchscript.ToString(), true);
        }

        #endregion

    }
}