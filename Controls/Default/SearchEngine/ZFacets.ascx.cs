﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Ecommerce.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.Framework.Business;


namespace WebApp.Controls.Default.SearchEngine
{
    public partial class ZFacets : System.Web.UI.UserControl
    {
        #region Public Member
        List<ZNodeFacet> facets;
        public delegate void UpdatePageDelegate(string facetquery);
        public event UpdatePageDelegate UpdatePageHandler;
        //const string querystringDefaultParams = "zcid,category,text,page,size,sort,v";//Customize to remove list view, grid view 
        const string querystringDefaultParams = "zcid,category,text,page,s,sort";
        public List<KeyValuePair<string, IEnumerable<string>>> RefinedFacets = new List<KeyValuePair<string, IEnumerable<string>>>();
        string currentSelectedFacet = string.Empty;
        #endregion

        #region Page Events

        /// <summary>
        /// Handeles Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindAllSelectedItems();
            }
        }

        #endregion

        #region Public Properties

        public List<ZNodeFacet> Facetslist
        {
            get
            {
                return this.facets;
            }

            set
            {
                this.facets = value;
            }
        }

        /// <summary>
        /// get or set selected Product List
        /// </summary>
        public List<int> SelectedProductList { get; set; }

        /// <summary>
        /// get or set current selected facet product count
        /// </summary>
        public List<int> CurrentSelFacetProductList { get; set; }

        /// <summary>
        /// get or set current selected facet value
        /// </summary>
        public string CurrentSelFacet
        {
            get { return !string.IsNullOrEmpty(hdnCurrentSelFacet.Value) ? hdnCurrentSelFacet.Value : string.Empty; }
            set { hdnCurrentSelFacet.Value = value; }
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Bind all Facets on page
        /// </summary>
        public void BindFacets()
        {
            //Create Refined Facet
            string customQueryStringParam = ConfigurationManager.AppSettings["SEOQuerystringKeys"] != null ? querystringDefaultParams + ConfigurationManager.AppSettings["SEOQuerystringKeys"].ToString() : querystringDefaultParams;
            foreach (var item in Request.QueryString.Keys)
            {
                if (item == null)
                {
                    continue;
                }
                else if (item.ToString().Contains("zcid"))
                {
                    continue;
                }
                string itemString = item.ToString();
                if (!customQueryStringParam.Split(',').Contains(item.ToString().ToLower()))
                {
                    RefinedFacets.Add(new KeyValuePair<string, IEnumerable<string>>(itemString, Request.QueryString.GetValues(itemString)));
                }
            }
            currentSelectedFacet = GetCurrentFacetName();
            FacetRepeater.DataSource = facets;
            FacetRepeater.DataBind();
            FacetRepeater.Visible = facets.Any();
        }

        /// <summary>
        /// Get Facet removal url
        /// </summary>
        /// <param name="facetName">string</param>
        /// <returns>string</returns>
        private string GetRemoveUrlForFacet(string facetName)
        {
            string query = string.Empty;
            foreach (string key in Request.QueryString.Keys)
            {
                if (key != null && key != facetName && !key.Contains("zcid"))
                {
                    Request.QueryString[key].Split(',').ToList().ForEach(x =>
                    {
                        query += string.Format("&{0}={1}", key, HttpUtility.UrlEncode(x));
                    });
                }
            }

            if (query.Length > 0)
                query = query.Substring(1);


            string currentURL = Request.Path;
            if (currentURL.Contains("searchengine.aspx"))
            {
                return Request.Url.GetLeftPart(UriPartial.Path) + "?" + query;
            }
            else
            {
                string url = this.GetCurrentPageURL();
                return url + "?" + query;
            }
            //return Request.Url.GetLeftPart(UriPartial.Path) + "?" + query;


        }

        /// <summary>
        /// Get clear all facet URL
        /// </summary>
        /// <returns>string</returns>
        public string GetClearAllUrlForFacet()
        {
            string query = string.Empty;
            string customQueryStringParam = ConfigurationManager.AppSettings["SEOQuerystringKeys"] != null ? querystringDefaultParams + ConfigurationManager.AppSettings["SEOQuerystringKeys"].ToString() : querystringDefaultParams;
            string[] facetName = customQueryStringParam.Split(',');
            foreach (string key in Request.QueryString.Keys)
            {
                if (key != null && !key.Contains("zcid") && facetName.Contains(key))
                {
                    Request.QueryString[key].Split(',').ToList().ForEach(x =>
                    {
                        query += string.Format("&{0}={1}", key, HttpUtility.UrlEncode(x));
                    });
                }
            }
            if (query.Length > 0)
                query = query.Substring(1);


            string currentURL = Request.Path;
            if (currentURL.ToLower().Contains("searchengine.aspx"))
            {
                return Request.Url.GetLeftPart(UriPartial.Path) + "?" + query;
            }
            else
            {
                string url = this.GetCurrentPageURL();
                return url + "?" + query;
            }
            //return Request.Url.GetLeftPart(UriPartial.Path) + "?" + query;

        }

        #endregion

        #region Protected Methods Or Events

        /// <summary>
        /// Handles FacetRepeater item data bound
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">RepeaterItemEventArgs</param>
        protected void FacetRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ZNodeFacet facet = (ZNodeFacet)e.Item.DataItem;
                if (facet != null)
                {
                    switch ((ZNodeFacetControlType)(facet.ControlTypeID))
                    {
                        case ZNodeFacetControlType.LABEL:
                            Repeater links = e.Item.FindControl("FacetLinks") as Repeater;
                            if (links != null)
                            {
                                List<ZNodeFacetValue> lstFiltered = new List<ZNodeFacetValue>();
                                links.DataSource = facet.AttributeValues;
                                links.DataBind();

                            }
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Handles Repeater ItemBound Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void FacetSelectedRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, string>>> selFacet = (System.Collections.Generic.KeyValuePair<string, System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, string>>>)(e.Item.DataItem);
                Repeater links = e.Item.FindControl("rptSelectedAttribute") as Repeater;
                if (selFacet.Value.Count > 0 && links != null)
                {
                    if (selFacet.Key.ToString().ToLower().Equals("price"))
                    {
                        System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<string, string>> updatedValue = new List<KeyValuePair<string, string>>();
                        foreach (KeyValuePair<string, string> selTag in selFacet.Value)
                        {
                            updatedValue.Add(new KeyValuePair<String, String>(FormatPriceFacetValue(selTag.Key), selTag.Value));
                        }
                        links.DataSource = updatedValue;
                        links.DataBind();
                    }
                    else
                    {
                        links.DataSource = selFacet.Value;
                        links.DataBind();
                    }

                }
            }
        }

        /// <summary>
        /// Handles Checkbox Check Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkFacets_CheckedChanged(object sender, EventArgs e)
        {
            List<KeyValuePair<string, IEnumerable<string>>> selectedItems = new List<KeyValuePair<string, IEnumerable<string>>>();
            CheckBox cb = sender as CheckBox;
            if (cb != null)
            {
                string query = string.Empty;
                string UpdatedQuery = string.Empty, facetQuery = string.Empty;
                string currentFacetQuery = string.Empty;
                bool isKeyAdded = false;
                int keyIndex = 1;
                bool isMiddleFacetSel = false;
                string[] qparams = querystringDefaultParams.Split(',');
                var currentQuery = cb.Attributes["Value"] != null ? cb.Attributes["Value"].ToString().Split(':') : null;

                if (currentQuery != null && currentQuery.Length > 0)
                {
                    var currentkey = currentQuery[0].Split('|')[0];
                    if (Session["CurrentUpdatedFacet"] != null && !Session["CurrentUpdatedFacet"].ToString().Split(':').Contains(currentkey.ToString()) && cb.Checked) { Session["CurrentUpdatedFacet"] = null; isMiddleFacetSel = true; }; //Search Implementation Code
                    foreach (string key in Request.QueryString.Keys)
                    {
                        if (currentkey != null && !qparams.Contains(key.ToLower()) && !currentkey.Equals(key))
                            Request.QueryString[key].Split(',').ToList().ForEach(x =>
                            {
                                query += string.Format("&{0}={1}", key, HttpUtility.UrlEncode(x));//Old Code
                            });

                        if (currentkey != null && key.Equals(currentkey, StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (cb.Checked)
                            {
                                string concatenatedValue = Request.QueryString.GetValues(currentkey)[0] + "^" + currentQuery[1];
                                CurrentSelFacet = cb.Attributes["Value"] != null ? cb.Attributes["Value"].ToString() : string.Empty;//set current selected facet 
                                query = query + string.Format("&{0}={1}", currentkey, HttpUtility.UrlEncode(concatenatedValue));//Old Code before Search Implemented
                                //Search Implementation Code:Starts
                                //currentFacetQuery = string.Format("&{0}={1}", currentkey, HttpUtility.UrlEncode(concatenatedValue));
                                if (keyIndex < Request.QueryString.Keys.Count || (keyIndex == Request.QueryString.Keys.Count && isMiddleFacetSel))
                                {
                                    AddupdatedFacetIntoSession(key, currentQuery[1]);//Search Implemented Code
                                }
                                //Search Implementation Code:Ends
                                isKeyAdded = true;
                            }
                            else
                            {
                                //Retrieve Key Value and Remove unchecked facet from URL
                                if (currentkey != null && Request.QueryString.GetValues(currentkey)[0].ToString().ToLower().Contains(currentQuery[1].ToLower()))
                                {
                                    Request.QueryString[key].Split(',').ToList().ForEach(x =>
                                    {
                                        facetQuery = x;
                                    });
                                    UpdatedQuery = facetQuery.Replace(currentQuery[1], "");
                                    if (Request.QueryString[key].IndexOf('^') > 0)
                                    {
                                        UpdatedQuery = FormatAttributeURLString(UpdatedQuery, facetQuery);
                                        query += string.Format("&{0}={1}", key, HttpUtility.UrlEncode(UpdatedQuery.Trim('^')));
                                    }
                                    isKeyAdded = true;
                                }
                                Session["CurrentUpdatedFacet"] = null;
                            }
                        }
                        keyIndex++;
                    }
                    ///Search Implementation Code:Starts
                    // query = query + currentFacetQuery;
                    ///Search Implementation Code:Ends
                    if (!isKeyAdded)
                    {
                        query += string.Format("&{0}={1}", currentkey, HttpUtility.UrlEncode(currentQuery[1]));
                    }

                    if (UpdatePageHandler != null)
                        UpdatePageHandler(query);
                }
                cb.Focus();
            }
        }

        /// <summary>
        /// Format Price Facet and Return string
        /// </summary>
        /// <param name="facetValue">object</param>
        /// <returns>string</returns>
        protected string FormatPriceFacetValue(object facetValue)
        {
            string formattedFacetVal = string.Empty;
            if (facetValue != null)
            {
                string[] facetVal = facetValue.ToString().Split(' ');
                foreach (string splitStr in facetVal)
                {
                    int strNum = 0;
                    bool isNumeric = int.TryParse(splitStr, out strNum);
                    formattedFacetVal += isNumeric ? string.Format("{0} ", ZNodeCurrencyManager.GetCurrencyPrefix() + splitStr) : string.Format("{0} ", splitStr);
                }

            }
            return formattedFacetVal.Trim(); ;
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Bind Selected Facet Items
        /// </summary>
        private void BindAllSelectedItems()
        {
            Dictionary<string, List<KeyValuePair<string, string>>> allSelectedItems = new Dictionary<string, List<KeyValuePair<string, string>>>();

            if (FacetRepeater.Visible)
            {
                foreach (RepeaterItem item in FacetRepeater.Items)
                {
                    var repeater = item.FindControl("FacetLinks") as Repeater;

                    foreach (RepeaterItem rptitem in repeater.Items)
                    {
                        //Zeon Custom Code:Start
                        List<KeyValuePair<string, string>> selFacetList = new List<KeyValuePair<string, string>>();
                        var chkBox = rptitem.FindControl("chkFacets") as CheckBox;
                        if (chkBox != null)
                        {
                            var currentItem = chkBox.Attributes["Value"] != null ? chkBox.Attributes["Value"].ToString().Split(':') : null;
                            if (currentItem != null && currentItem.Length > 0)
                            {
                                var key = currentItem[0].Split('|')[0];
                                if (Request.QueryString[key] != null)
                                {
                                    if (chkBox.Font.Bold = Request.QueryString[key].Split('^').Contains(currentItem[1]))
                                    {
                                        chkBox.Checked = true;
                                        chkBox.Visible = true;
                                        //chkBox.Focus();
                                    }
                                    if (!allSelectedItems.Keys.Contains(key) && chkBox.Checked)
                                    {
                                        string currntKey = string.Empty;
                                        currntKey = currentItem[1];
                                        KeyValuePair<string, string> listItem = new KeyValuePair<string, string>(currntKey, GetRemoveUrlForFacetAttributes(key, currentItem[1]));
                                        string updtatedURL = GetRemoveUrlForFacetAttributes(key, currentItem[1]);
                                        selFacetList.Add(listItem);
                                        allSelectedItems.Add(key, (selFacetList));

                                    }
                                    else if (chkBox.Checked)
                                    {
                                        KeyValuePair<string, string> listItem = new KeyValuePair<string, string>(currentItem[1], GetRemoveUrlForFacetAttributes(key, currentItem[1]));
                                        allSelectedItems.TryGetValue(key, out selFacetList);
                                        selFacetList.Add(listItem);
                                        allSelectedItems[key] = selFacetList;
                                    }
                                }
                            }
                        }

                    }
                    item.Visible = repeater.Items.Count > 0 && repeater.Items.Cast<RepeaterItem>().Any(x => (x.FindControl("chkFacets") as CheckBox).Visible);
                }

                FacetRepeater.Visible = FacetRepeater.Items.Cast<RepeaterItem>().Any(x => x.Visible);
            }
            else
            {
                // show selected facets when no facets found from search engine result.  
                string customQueryStringParam = ConfigurationManager.AppSettings["SEOQuerystringKeys"] != null ? querystringDefaultParams + ConfigurationManager.AppSettings["SEOQuerystringKeys"].ToString() : querystringDefaultParams;
                string[] qparams = customQueryStringParam.Split(',');
                foreach (string key in Request.QueryString.Keys)
                {
                    if (!qparams.Contains(key))
                    {
                        // Implements removing each facets attribute
                        List<KeyValuePair<string, string>> selFacetList = new List<KeyValuePair<string, string>>();
                        if (Request.QueryString[key].IndexOf('^') > 0)
                        {
                            string[] selFacets = Request.QueryString[key].Split('^');
                            foreach (string strfacet in selFacets)
                            {
                                KeyValuePair<string, string> listItem = new KeyValuePair<string, string>(Request.QueryString[strfacet], GetRemoveUrlForFacetAttributes(key, strfacet));
                                allSelectedItems.TryGetValue(key, out selFacetList);
                                selFacetList.Add(listItem);
                                allSelectedItems[key] = selFacetList;
                            }
                        }
                    }
                }
            }

            FacetSelectedRepeater.DataSource = allSelectedItems;
            FacetSelectedRepeater.DataBind();
            FacetSelectedRepeater.Visible = allSelectedItems.Any();
        }

        /// <summary>
        /// Get Seo Url of current page
        /// </summary>
        /// <returns>string</returns>
        private string IsSEOUrl()
        {
            string zcid = string.Empty;

            if (Request.QueryString["zcid"] != null)
            {
                Session["BreadCrumzcid"] = Request.QueryString["zcid"];
                zcid = Request.QueryString["zcid"].ToString();
            }
            else if (Session["BreadCrumzcid"] != null)
            {
                zcid = Session["BreadCrumzcid"].ToString();
            }
            else if (Request.QueryString["zpid"] != null)
            {
                ZNode.Libraries.DataAccess.Service.ProductCategoryService service = new ZNode.Libraries.DataAccess.Service.ProductCategoryService();
                TList<ProductCategory> pcategories = service.GetByProductID(Convert.ToInt32(Request.QueryString["zpid"]));
                if (pcategories.Count > 0)
                {
                    zcid = pcategories[0].CategoryID.ToString();
                }
            }
            else
            {
                zcid = "0";
            }

            ZNode.Libraries.DataAccess.Service.CategoryService cservice = new ZNode.Libraries.DataAccess.Service.CategoryService();
            ZNode.Libraries.DataAccess.Entities.Category category = cservice.GetByCategoryID(Convert.ToInt32(zcid));
            if (category != null)
            {
                string seourl = string.Empty;
                if (category.SEOURL != null)
                {
                    seourl = category.SEOURL;
                }

                return ZNodeSEOUrl.MakeURL(category.CategoryID.ToString(), SEOUrlType.Category, seourl);
            }

            return ZNodeSEOUrl.MakeURL("0", SEOUrlType.Category, string.Empty);
        }

        /// <summary>
        /// Gets the Current Page URL
        /// </summary>
        /// <returns>Returns the Current Page URL</returns>
        private string GetCurrentPageURL()
        {
            return this.IsSEOUrl();
        }

        /// <summary>
        /// Create selected facet value  removal string 
        /// </summary>
        /// <param name="facetName">string</param>
        /// <param name="facetAttribute">string</param>
        /// <returns>string</returns>
        private string GetRemoveUrlForFacetAttributes(string facetName, string facetAttribute)
        {
            string query = string.Empty;
            string facetQuery = string.Empty;
            string UpdatedQuery = string.Empty;

            foreach (string key in Request.QueryString.Keys)
            {
                if (key != null && !key.Contains("zcid"))
                {
                    if (key != facetName)
                    {
                        Request.QueryString[key].Split(',').ToList().ForEach(x =>
                        {
                            query += string.Format("&{0}={1}", key, HttpUtility.UrlEncode(x));
                        });
                    }
                    else
                    {
                        Request.QueryString[key].Split(',').ToList().ForEach(x =>
                        {
                            facetQuery = x;
                        });
                        if (!string.IsNullOrEmpty(facetAttribute))
                        {
                            UpdatedQuery = facetQuery.Replace(facetAttribute, "");
                        }
                        if (Request.QueryString[key].IndexOf('^') > 0)
                        {
                            UpdatedQuery = FormatAttributeURLString(UpdatedQuery, facetQuery);
                            query += string.Format("&{0}={1}", key, HttpUtility.UrlEncode(UpdatedQuery.Trim('^')));
                        }
                    }
                }

            }

            if (query.Length > 0)
                query = query.Substring(1);


            string currentURL = Request.Path;
            if (currentURL.ToLower().Contains("searchengine.aspx"))
            {
                return Request.Url.GetLeftPart(UriPartial.Path) + "?" + query;
            }
            else
            {
                string url = this.GetCurrentPageURL();
                return url + "?" + query;
            }
        }

        /// <summary>
        /// Format Attribute URL 
        /// </summary>
        /// <param name="attributeURL">string</param>
        /// <param name="facet">string</param>
        /// <returns>string</returns>
        private string FormatAttributeURLString(string attributeURL, string facet)
        {
            string formattedStr = string.Empty;
            string[] attrArry = attributeURL.Split('^');
            foreach (string strAttr in attrArry)
            {
                if (!string.IsNullOrEmpty(strAttr) && !facet.Equals(strAttr))
                {
                    formattedStr = formattedStr + '^' + strAttr;
                }
            }

            return formattedStr;
        }

        /// <summary>
        /// Page Prerender event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            RegisterClientScriptClasses();
        }

        /// <summary>
        /// Registers the Script
        /// </summary>
        private void RegisterClientScriptClasses()
        {
            StringBuilder script = new StringBuilder();
            script.Append("var objFacets = new Facets({");
            script.Append("});objFacets.Init();");
            this.Page.ClientScript.RegisterStartupScript(GetType(), "ScriptFacets", script.ToString(), true);
        }

        #endregion

        #region Search Implementatiom Methods

        /// <summary>
        /// Handels Visibility of Facets Based on its name and value
        /// </summary>
        /// <param name="attributeName">object</param>
        /// <param name="attributeValue">object</param>
        /// <returns>bool</returns>
        protected bool SetCurrentFacetVisibility(object attributeName, object attributeValue)
        {
            bool isVisible = false;
            if (attributeName != null && attributeValue != null && !string.IsNullOrEmpty(attributeName.ToString()) && !string.IsNullOrEmpty(attributeValue.ToString()))
            {
                string attributeValueCollection = GetCurrentFacetAttributeValues(attributeName.ToString().Split('|')[0].ToLower());
                if (SelectedProductList.Count() > 0 && CurrentSelFacetProductList.Count > 0)
                {
                    if (attributeValueCollection.Equals(attributeValue) || attributeValueCollection.Split(',').Contains(attributeValue.ToString().Trim()))
                    {
                        isVisible = true;
                    }
                    else
                    {
                        ZNodeFacet currentFacet = Facetslist.Find(x => x.AttributeName.Split('|')[0].ToLower().Equals(attributeName.ToString().Split('|')[0].ToLower()));
                        if (currentFacet != null)
                        {
                            ZNodeFacetValue selectedAttribute = currentFacet.AttributeValues.Find(x => x.AttributeValue.Equals(attributeValue));
                            if (selectedAttribute != null)
                            {
                                if (attributeName.ToString().Split('|')[0].ToLower().Equals(currentSelectedFacet.ToLower()))
                                {
                                    isVisible = selectedAttribute.ProductIDList.Intersect(CurrentSelFacetProductList).Distinct().Count() > 0;
                                }
                                else
                                {
                                    isVisible = selectedAttribute.ProductIDList.Intersect(SelectedProductList).Distinct().Count() > 0;
                                }
                            }
                        }
                    }
                }
                else
                {
                    isVisible = true;
                }
            }
            return isVisible;
        }

        /// <summary>
        /// Update Facet Count by attribute name value and facet count
        /// </summary>
        /// <param name="attributeName">object</param>
        /// <param name="attributeValue">object</param>
        /// <param name="facetValue">object</param>
        /// <returns>string</returns>
        protected string GetUpdatedFacetCount(object attributeName, object attributeValue, object facetValue)
        {
            int UpdatedFacetCount = 0;
            if (SelectedProductList.Count() > 0 && CurrentSelFacetProductList.Count > 0)
            {
                ZNodeFacet currentFacet = Facetslist.Find(x => x.AttributeName.Split('|')[0].ToLower().Equals(attributeName.ToString().Split('|')[0].ToLower()));
                ZNodeFacetValue selectedAttribute = currentFacet.AttributeValues.Find(x => x.AttributeValue.Equals(attributeValue));
                if (selectedAttribute != null)
                {
                    if (attributeName.ToString().Split('|')[0].ToLower().Equals(currentSelectedFacet.ToLower()))
                    {
                        UpdatedFacetCount = selectedAttribute.ProductIDList.Intersect(CurrentSelFacetProductList).Distinct().Count();
                    }
                    else
                    {
                        UpdatedFacetCount = selectedAttribute.ProductIDList.Intersect(SelectedProductList).Distinct().Count();
                    }

                }
            }
            else
            {
                UpdatedFacetCount = int.Parse(facetValue.ToString());
            }
            return UpdatedFacetCount.ToString();
        }

        /// <summary>
        /// Get Current Selected Facet Name
        /// </summary>
        /// <returns>string</returns>
        public string GetCurrentFacetName()
        {
            string currentFacetSelected = string.Empty;
            if (RefinedFacets != null && RefinedFacets.Count > 0)
            {
                if (Session["CurrentUpdatedFacet"] != null)
                {
                    currentFacetSelected = Session["CurrentUpdatedFacet"].ToString().Split(':')[0];
                }
                else
                {
                    currentFacetSelected = RefinedFacets[RefinedFacets.Count - 1].Key;
                }
            }
            return currentFacetSelected;
        }

        /// <summary>
        /// Get Current Selected Facet Name
        /// </summary>
        /// <returns>string</returns>
        private string GetCurrentFacetAttributeValues(string attributeName)
        {
            string currentFacetSelected = string.Empty;
            if (RefinedFacets != null && RefinedFacets.Count > 0)
            {
                for (int facetIndex = RefinedFacets.Count - 1; facetIndex >= 0; facetIndex--)
                {
                    if (RefinedFacets.Count > 0)
                    {
                        if (attributeName.Equals(RefinedFacets[facetIndex].Key.ToLower()))
                        {
                            using (var refinedFacet = RefinedFacets[facetIndex].Value.GetEnumerator())
                            {
                                while (refinedFacet.MoveNext())
                                {
                                    if (refinedFacet.Current.ToString().IndexOf('^') > 0)
                                    {
                                        string[] selFac = refinedFacet.Current.ToString().Split('^');
                                        foreach (string attrVal in selFac)
                                        {
                                            currentFacetSelected = currentFacetSelected + attrVal + ",";
                                        }
                                        currentFacetSelected = currentFacetSelected.TrimEnd(',');
                                    }
                                    else
                                    {
                                        currentFacetSelected = currentFacetSelected + refinedFacet.Current;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return currentFacetSelected;
        }

        /// <summary>
        /// Update session value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        private void AddupdatedFacetIntoSession(string key, string value)
        {
            if (Session["CurrentUpdatedFacet"] != null)
            {
                string preFacet = string.Empty, facetValue = Session["CurrentUpdatedFacet"].ToString().Split(':')[1];
                preFacet = Session["CurrentUpdatedFacet"].ToString().Split(':')[0];
                if (preFacet.Equals(key))
                {
                    Session["CurrentUpdatedFacet"] = string.Format("{0}:{1}", preFacet, facetValue + ',' + value);
                }
            }
            else
            {
                Session["CurrentUpdatedFacet"] = string.Format("{0}:{1}", key, value);
            }
        }

        /// <summary>
        /// Handeles Text Formatting For Facet Value and count
        /// </summary>
        /// <param name="facetName">object</param>
        /// <param name="facetValue">object</param>
        /// <param name="facetCount">object</param>
        /// <returns>string</returns>
        protected string GetFacetValueText(object facetName, object facetValue, object facetCount)
        {
            string facetText = string.Empty, facetNameText = string.Empty, updatedfacetCount = string.Empty;
            if (facetName != null && facetValue != null && facetCount != null)
            {
                facetNameText = facetName.ToString().Split('|')[0].Replace("_", " ").ToLower().Equals("price") ? FormatPriceFacetValue(facetValue) : facetValue.ToString();
                updatedfacetCount = GetUpdatedFacetCount(facetName, facetValue, facetCount);
                facetText = string.Format(" {0} ({1})", facetNameText, updatedfacetCount);
            }
            return facetText;
        }
        #endregion
    }
}