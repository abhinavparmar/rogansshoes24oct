﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Ecommerce.Entities;


namespace WebApp.Controls.Default.SearchEngine
{
    public partial class LeftNavigation : System.Web.UI.UserControl
    {
        public bool IsCategoryPage { get; set; }

        # region Private Variables
        private IEnumerable<ZNodeCategoryNavigation> _CategoryNavigation;
        private ZNodeCategory _category;
        private string searchText = string.Empty;
        private string category = string.Empty;
        private bool _showSubCategory = false;
        private string _view = string.Empty; //Zeon Custom Code: Start
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            //Zeon Custom Code: Start
            if (Request.QueryString["v"] != null)
            {
                _view = Request.QueryString["v"].ToString();
            }
            else
            {

                _view = "g";
            }
            //Zeon Cutom Code: End

            if (Request.QueryString["text"] != null)
            {
                searchText = Request.QueryString["text"].ToString();
            }
            category = Request.QueryString["category"];

            if (!Page.IsPostBack)
            {
                if (CategoryNavigation != null)
                {
                    CategoryList.CollapseImageUrl = string.Empty;
                    CategoryList.ExpandImageUrl = string.Empty;
                    CategoryList.NoExpandImageUrl = string.Empty;
                    CategoryList.ExpandAll();
                    divCategoryTreeTitle.Visible = true;//Zeon Custom Code
                    if (CategoryNavigation.Count() > 0)
                    {
                        // Retrieve category data from httpContext (set previously in the page_preinit event)
                        this._category = (ZNodeCategory)HttpContext.Current.Items["Category"];
                        var displayedCategories = CategoryNavigation.ToList();
                        if (!string.IsNullOrEmpty(category))
                        {
                            displayedCategories = FilterCategoryNavigation(category);
                        }
                        else if (this._category != null && this._category.Name != null)
                        {
                            displayedCategories = FilterCategoryNavigation(HttpUtility.HtmlDecode(this._category.Name));
                        }

                        //Zeon Custom Code: Start
                        if (displayedCategories.Count > 0)
                        {
                            PopulateTreeView(displayedCategories, null);
                        }
                        else
                        {
                            divCategoryTreeTitle.Visible = false;
                        }
                        //Zeon Custom Code: End
                    }
                    else if (!string.IsNullOrEmpty(Request.QueryString["category"]))
                    {
                        var rootItems = new List<ZNodeCategoryNavigation>() { new ZNodeCategoryNavigation() { Name = Request.QueryString["category"], NumberOfProducts = 0 } };
                        PopulateTreeView(rootItems, null);
                    }
                    else
                    {
                        CategoryList.Visible = false;
                        divCategoryTreeTitle.Visible = false;//Zeon Custom Code
                    }
                }
            }
        }

        public IEnumerable<ZNodeCategoryNavigation> CategoryNavigation
        {
            get
            {
                return this._CategoryNavigation;
            }

            set
            {
                this._CategoryNavigation = value;
            }
        }

        public bool ShowSubCategory { get { return _showSubCategory; } set { _showSubCategory = value; } }

        private List<ZNodeCategoryNavigation> FilterCategoryNavigation(string category)
        {
            var displayCategories = CategoryNavigation.Where(x => x.Name == category).ToList();

            if (!displayCategories.Any())
            {
                var rootCategories = CategoryNavigation.Where(x => x.CategoryHierarchy.Any(y => y.Name == category)).ToList();
                rootCategories.ForEach(x => x.CategoryHierarchy = x.CategoryHierarchy.Where(y => y.Name == category).ToList());
                displayCategories = rootCategories;
            }

            return displayCategories;
        }

        public string GetNavigationURL(string categoryName)
        {
            if (IsCategoryPage)
            {
                var url = new ZNodeNavigation().GetCategoryUrl(categoryName);

                if (!string.IsNullOrEmpty(url))
                {
                    return ResolveUrl(url);
                }
            }

            return "~/SearchEngine.aspx?category=" + HttpUtility.UrlEncode(categoryName) + "&text=" + HttpUtility.UrlEncode(searchText);
            //return "~/searchengine.aspx?category=" + HttpUtility.UrlEncode(categoryName) + "&text=" + HttpUtility.UrlEncode(searchText) + "&v=" + HttpUtility.UrlEncode(_view); //Zeon Custom Code for list view grid view
        } 
         
        private void PopulateTreeView(IEnumerable<ZNodeCategoryNavigation> displayedCategories, TreeNode parentNode)
        {
            foreach (var node in displayedCategories)
            {
                var treeNode = new TreeNode();
                string nodeText = string.Empty;
                if (node.CategoryHierarchy != null && node.CategoryHierarchy.Any())
                {
                    nodeText = string.Format("<span class=\"Rightarrow\">»</span><span>" + node.Name + "</span>");
                    treeNode.SelectAction = TreeNodeSelectAction.None;
                }
                else
                {
                    nodeText = string.Format("<span class=\"Rightarrow\">»</span><a class=\"UnderlineText\" href=\"{0}\">{1} ({2})</a>", Page.ResolveClientUrl(GetNavigationURL(node.Name)), node.Name, node.NumberOfProducts);
                }
                treeNode.Text = nodeText;
                treeNode.Value = node.Name;
                if (parentNode == null)
                {
                    CategoryList.Nodes.Add(treeNode);
                }
                else
                {
                    parentNode.ChildNodes.Add(treeNode);
                }

                if (node.CategoryHierarchy != null && node.CategoryHierarchy.Any())
                {
                    PopulateTreeView(node.CategoryHierarchy, treeNode);
                }
            }
        }
    }
}