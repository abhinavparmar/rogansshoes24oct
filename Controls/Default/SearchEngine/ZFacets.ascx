﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ZFacets.ascx.cs" Inherits="WebApp.Controls.Default.SearchEngine.ZFacets" %>
<%--<div class="filter"><a href="javascript:void(0)" onclick="objFacets.ShowFacets()">Filter</a></div>--%>
<div class="ProductTagging">
    <asp:Repeater runat="server" ID="FacetSelectedRepeater" OnItemDataBound="FacetSelectedRepeater_ItemDataBound">
        <HeaderTemplate>
            <div class="Title">
                <asp:Literal ID="ltrNarrowResult" runat="server" Text="<%$ Resources:CommonCaption,NarrowResultTitle %>" EnableViewState="false"></asp:Literal>
            </div>

            <div>
                <h3>
                    <asp:Literal ID="itrCurrSelText" Text="<%$ Resources:CommonCaption,NarrowResultCurrentSelection %>" runat="server" EnableViewState="false"></asp:Literal></h3>
                <div class="ClearAll">
                    <asp:HyperLink runat="server" ID="ClearAllSelection" CssClass="linkBtnTagText" NavigateUrl='<%#GetClearAllUrlForFacet()%>' EnableViewState="false">
                        <asp:Literal ID="ltrClearText" Text="<%$ Resources:CommonCaption,NarrowResultClearAll %>" runat="server" EnableViewState="false"></asp:Literal>
                    </asp:HyperLink>
                </div>
            </div>
        </HeaderTemplate>
        <ItemTemplate>
            <ul class="ProductTagged">
                <span class="SelectedTagText"><%# Eval("Key").ToString().Replace("_"," ")%></span>
                <li>
                    <asp:Repeater ID="rptSelectedAttribute" runat="server" EnableViewState="false" ClientIDMode="Static">
                        <ItemTemplate>
                            <span class="TagText">
                                <asp:HyperLink runat="server" EnableViewState="false" NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"Value")%>' ID="remove" CssClass="linkBtnTagText"></asp:HyperLink>
                                <span class="SelectedTagValue"><%# Eval("Key").ToString().Replace("_"," ").ToString().ToLower().Equals("price")?FormatPriceFacetValue(DataBinder.Eval(Container.DataItem,"Key")):DataBinder.Eval(Container.DataItem,"Key")%></span>
                            </span>
                        </ItemTemplate>
                    </asp:Repeater>
                </li>
            </ul>
        </ItemTemplate>
    </asp:Repeater>
    <asp:Repeater ID="FacetRepeater" runat="server" OnItemDataBound="FacetRepeater_ItemDataBound">
        <HeaderTemplate>
            <div class="menu">
                <ul id="FacetTreeMenu">
        </HeaderTemplate>
        <ItemTemplate>
            <li><span class="facetName"><em>
                <%# DataBinder.Eval(Container.DataItem,"AttributeName").ToString().Split('|')[0].Replace("_"," ") %></em></span>
                <ul data-facetname="<%# DataBinder.Eval(Container.DataItem,"AttributeName").ToString().Split('|')[0].Replace("_"," ") %>">
                    <li>
                        <asp:Repeater ID="FacetLinks" runat="server">
                            <ItemTemplate>
                                <div class="facetValue" id="divFacetValueSection" runat="server" visible='<%# SetCurrentFacetVisibility( DataBinder.Eval(Container.NamingContainer.NamingContainer, "DataItem.AttributeName"),DataBinder.Eval(Container.DataItem, "AttributeValue")) %>'>
                                    <asp:CheckBox ID="chkFacets" runat="server" CssClass="SelectedTagValue" AutoPostBack="true" onclick="zeonCatProdList.setCookie('plpfocus','Checkbox')"
                                        Value='<%# DataBinder.Eval(Container.NamingContainer.NamingContainer, "DataItem.AttributeName") +":" + DataBinder.Eval(Container.DataItem, "AttributeValue") %>'
                                        OnCheckedChanged="chkFacets_CheckedChanged" Text='<%# GetFacetValueText( DataBinder.Eval(Container.NamingContainer.NamingContainer, "DataItem.AttributeName"),DataBinder.Eval(Container.DataItem, "AttributeValue"),DataBinder.Eval(Container.DataItem, "FacetCount")) %>' />
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </li>
                </ul>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul> </div>
        </FooterTemplate>
    </asp:Repeater>
    <asp:HiddenField ID="hdnCurrentSelFacet" runat="server" />
</div>

