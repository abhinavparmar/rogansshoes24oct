﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Facets.ascx.cs" Inherits="WebApp.Controls.Default.SearchEngine.Facets" %>

<div class="ProductTagging">
    <asp:Repeater runat="server" ID="FacetSelectedRepeater">
        <HeaderTemplate>
            <div class="Title">
                YOU'VE SELECTED:
            </div>
        </HeaderTemplate>
        <ItemTemplate>
            <div>
                <span class="SelectedTagText"><%# Eval("Key").ToString().Replace("_"," ")%>:</span>
                <span class="SelectedTagValue"><%# Eval("Value.Key") %></span>
                <br>
                <asp:HyperLink runat="server" NavigateUrl='<%# Eval("Value.Value") %>' ID="remove" CssClass="linkBtnTagText" Style="font-size: 9px">[Remove]</asp:HyperLink>
            </div>
        </ItemTemplate>
        <FooterTemplate>
            <div class="ClearAll">
                <asp:HyperLink runat="server" ID="ClearAllSelection" CssClass="linkBtnTagText" NavigateUrl='<%#GetClearAllUrlForFacet()%>' Style="font-size: 9px">Clear all selections</asp:HyperLink>
            </div>
        </FooterTemplate>
    </asp:Repeater>
    <asp:Repeater ID="FacetRepeater" runat="server" OnItemDataBound="FacetRepeater_ItemDataBound">
        <HeaderTemplate>
            <div class="Title">
                Refine By
            </div>
            <div class="menu">
                <ul id="FacetTreeMenu">
        </HeaderTemplate>
        <ItemTemplate>
            <li><span class="facetName"><b>
                <%# DataBinder.Eval(Container.DataItem,"AttributeName").ToString().Split('|')[0].Replace("_"," ") %></b></span>
                <ul>
                    <li>
                        <asp:Repeater ID="FacetLinks" runat="server" OnItemCommand="FacetLinks_ItemCommand">
                            <ItemTemplate>

                                <div class="facetValue">
                                    <asp:LinkButton runat="server" CommandName="UpdateFacet" CssClass="SelectedTagValue"
                                        ID="facetLink" CommandArgument='<%# DataBinder.Eval(Container.NamingContainer.NamingContainer, "DataItem.AttributeName") +":" + DataBinder.Eval(Container.DataItem, "AttributeValue") %>'
                                        Text='<%# DataBinder.Eval(Container.DataItem, "AttributeValue").ToString()+"(" + DataBinder.Eval(Container.DataItem, "FacetCount").ToString()+")" %>'></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </li>
                </ul>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul> </div>
        </FooterTemplate>
    </asp:Repeater>
</div>

