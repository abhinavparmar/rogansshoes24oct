using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApp.Controls.Default.Home;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using Category = ZNode.Libraries.DataAccess.Entities.Category;

namespace WebApp
{
    /// <summary>
    /// Represents the Product user control class.
    /// </summary>
    public partial class Controls_Default_Product_Product : System.Web.UI.UserControl
    {
        #region Constant Variables

        private const string StoreLocatorPageLink = "~/storelocator.aspx";
        private const string URLStartString = "http";
        private const string SecuredURLString = "https://";
        private const string NonSecuredURLString = "http://";
        private const int YoutubeEmbdeTagLength = 11;

        #endregion

        #region Private Variables

        private const string PostBackEventTarget = "__EVENTTARGET";
        private int _productId;
        private ZNodeShoppingCart _shoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
        private ZNodeProduct Product;
        private ZNodeProfile Profile = new ZNodeProfile();
        private string zcid = string.Empty;
        private string _ComparePageURL = "/productcomparison.aspx";
        private string inStocklink = string.Empty;
        private string outOfStocklink = string.Empty;
        //Perficient Custom Member
        bool allowOutOfStockToUser = false;


        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets Shopping cart quantity
        /// </summary>
        public int ShoppingCartQuantity
        {
            get
            {
                if (ViewState["Quantity"] != null)
                {
                    return (int)ViewState["Quantity"];
                }

                return 1;
            }

            set
            {
                ViewState["Quantity"] = value;
            }
        }

        public string ProductIDVal
        {
            get
            {
                if (this.Product != null)
                {
                    return this.Product.ProductID.ToString();
                }
                ZNodeProduct dummyProduct = new ZNodeProduct();
                return dummyProduct.ProductID.ToString();
            }
        }

        /// <summary>
        /// get or set Selected Store ID
        /// </summary>
        public int SelectedStoreID { get; set; }


        #endregion

        #region  Public Methods

        /// <summary>
        /// Represents the CheckInventory Method
        /// </summary>
        /// <param name="Product">Product Instance</param>
        /// <returns>Returns the Inventory stock message</returns>
        public bool CheckInventory(ZNodeProduct product)
        {
            this.DisableAddToCartButton(false);
            lblstockmessage.CssClass = "ProductStock";
            // Don't track Inventory - Items can always be added to the cart,TrackInventory is disabled
            if (allowOutOfStockToUser)
            {
                lblstockmessage.Text = inStocklink + product.InStockMsg;
            }
            else if (!product.TrackInventoryInd && !product.AllowBackOrder && product.QuantityOnHand > 0)
            {
                lblstockmessage.Text = inStocklink + product.InStockMsg;
            }
            else if (!product.TrackInventoryInd && !product.AllowBackOrder && product.QuantityOnHand <= 0)
            {
                lblstockmessage.Text = string.Empty;
                lblstockmessage.CssClass = string.Empty;
            }
            else if (product.TrackInventoryInd && !product.AllowBackOrder && product.QuantityOnHand >= this.ShoppingCartQuantity)
            {
                lblstockmessage.Text = inStocklink + product.InStockMsg;//+ ItemsLeftInInventory(product.QuantityOnHand);
            }
            else if (product.AllowBackOrder && product.QuantityOnHand >= this.ShoppingCartQuantity)
            {
                // Set Inventory stock message
                lblstockmessage.Text = inStocklink + product.InStockMsg;
            }
            else if (product.AllowBackOrder && product.QuantityOnHand < this.ShoppingCartQuantity)
            {
                lblstockmessage.Text = product.BackOrderMsg;
            }
            else
            {
                // Display Out of stock message
                lblstockmessage.Text = outOfStocklink + product.OutOfStockMsg;
                lblstockmessage.CssClass = "OutOfStockMsg";

                // Hide AddToCart button
                //uxAddToCart.Visible = false;

               

                return false;
            }
            upnlStock.Update();//Zeon custom Code:Start    
            return true;
        }


        /// <summary>
        /// Update and Bind inventory status message and product price
        /// </summary>
        /// <returns>Returns the boolean to update the product status or not</returns>
        public bool UpdateProductStatus()
        {
            // Update the panels
            UpdPnlOrderingOptions.Update();

            this.DisableAddToCartButton(false);

            // Hide Product price
            uxProductPrice.Visible = this.Profile.ShowPrice && !this.Product.CallForPricing;
            uxAddToCart.Visible = this.Profile.ShowPrice && !this.Product.CallForPricing;
            int qty = 0;
            int.TryParse(txtQuantity.Text, out qty);
            this.ShoppingCartQuantity = qty;

            bool status = true;

            ZNodeSKU SKU = ZNodeSKU.CreateByProductDefault(this.Product.ProductID);

            // Check if product has add-ons
            if (this.Product.ZNodeAddOnCollection.Count > 0)
            {
                // If product has Add-Ons, then validate that all Add-Ons Values have been selected by the customer
                string addOnMessage = string.Empty;
                ZNodeAddOnList selectedAddOn = null;

                status = uxProductAddOns.ValidateAddOns(out addOnMessage, this.ShoppingCartQuantity, out selectedAddOn);

                if (!status)
                {
                    uxStatus.Visible = true;

                    // Error Message
                    uxStatus.Text = addOnMessage;

                    // Hide AddToCart button
                    //uxAddToCart.Visible = false;

                    this.DisableAddToCartButton(true);

                    return status;
                }

                // Set Selected Add-on
                this.Product.SelectedAddOnItems = selectedAddOn;
            }

            // Check if product has attributes
            if (this.Product.ZNodeAttributeTypeCollection.Count > 0)
            {
                // If product has attributes then validate that all attributes have been selected by
                // the customer validate attributes first - This is to verify that all required attributes have a valid selection
                // if a valid selection is not found then an error message is displayed
                string attributeMessage = string.Empty;
                string attributeList = string.Empty;
                string selectedAttributesDescription = string.Empty;

                status = uxProductAttributes.ValidateAttributes(out attributeMessage, out attributeList, out selectedAttributesDescription);

                if (!status)
                {
                    //uxStatus.Visible = true;

                    //uxStatus.Text = attributeMessage;

                    // Hide AddToCart button
                    //uxAddToCart.Visible = false;

                    this.DisableAddToCartButton(true);

                    return status;
                }

                // Get a sku based on attributes selected
                SKU = ZNodeSKU.CreateByProductAndAttributes(this.Product.ProductID, attributeList);

                // Set Attributes Description to Description property
                SKU.AttributesDescription = selectedAttributesDescription;

                if (SKU.SKUID == 0)
                {
                    lblstockmessage.Text = outOfStocklink + "Selected options is not available, please try different options";
                    lblstockmessage.CssClass = "OutOfStockMsg";

                    // Hide AddToCart button
                    //uxAddToCart.Visible = false;

                    this.DisableAddToCartButton(true);

                    status = false;
                }
                else // Check stock    
                    if (!allowOutOfStockToUser && SKU.QuantityOnHand < this.ShoppingCartQuantity && (!this.Product.AllowBackOrder) && this.Product.TrackInventoryInd)
                    {
                        // Display Out of stock message
                        lblstockmessage.Text = outOfStocklink + this.Product.OutOfStockMsg;
                        lblstockmessage.CssClass = "OutOfStockMsg";

                        uxStatus.Text = this.Product.OutOfStockMsg;
                        // Hide AddToCart button
                        //uxAddToCart.Visible = false;
                        this.DisableAddToCartButton(true);
                        status = false;
                    }
                    //Zeon Custom Code:Start
                    else
                    {
                        lblstockmessage.Text = inStocklink + this.Product.InStockMsg;// + qtyMessage;
                        lblstockmessage.CssClass = "ProductStock";
                    }
                //Zeon Custom Code:End
            }
            else
            {
                // If Product has no SKUs associated with this product.           
                SKU.AttributesDescription = string.Empty;

                // Check stock    
                if (this.Product.QuantityOnHand < this.ShoppingCartQuantity && (this.Product.AllowBackOrder == false) && this.Product.TrackInventoryInd)
                {
                    lblstockmessage.Text = outOfStocklink + this.Product.OutOfStockMsg; // Display Out of stock message
                    lblstockmessage.CssClass = "OutOfStockMsg";

                    // Hide AddToCart button
                    //uxAddToCart.Visible = false;

                    this.DisableAddToCartButton(true);

                    status = false;
                }
            }

            this.Product.SelectedSKU = SKU;
            this.Product.CheckSKUProfile();
            this.Product.IsPromotionApplied = false;
            this.Product.ApplyPromotion();

            // Check if product has child product (Bundle product)
            if (this.Product.ZNodeBundleProductCollection.Count > 0)
            {
                string addOnMessage = string.Empty;

                status = uxBundleProduct.ValidateAddons(out addOnMessage);

                if (!status)
                {
                    uxStatus.Visible = true;

                    // Error Message
                    uxStatus.Text = addOnMessage;

                    // Hide AddToCart button
                    //uxAddToCart.Visible = false;

                    this.DisableAddToCartButton(true);

                    return status;
                }

                string attributeMessage = string.Empty;

                status = uxBundleProduct.ValidateAttributes(out attributeMessage);

                if (!status)
                {
                    uxStatus.Visible = true;

                    uxStatus.Text = attributeMessage;

                    // Hide AddToCart button
                    //uxAddToCart.Visible = false;

                    this.DisableAddToCartButton(true);

                    return status;
                }
            }

            if (status)
            {
                status = this.CheckInventory(this.Product);

                if (this.Product.ZNodeBundleProductCollection.Count > 0 && status)
                {
                    bool applypromotion = false;
                    foreach (ZNode.Libraries.ECommerce.Entities.ZNodeProductBaseEntity _bundleProduct in this.Product.ZNodeBundleProductCollection)
                    {
                        ZNodeProduct _znodeBundleProduct = ZNodeProduct.Create(_bundleProduct.ProductID, applypromotion);

                        _znodeBundleProduct.SelectedSKUvalue = _bundleProduct.SelectedSKUvalue;

                        status = status && this.CheckInventory(_znodeBundleProduct);
                    }
                }
            }

            // Check for 'Call for Pricing'
            if (this.Product.CallForPricing)
            {
                lblstockmessage.Visible = false;
                uxAddToCart.Visible = false;

                uxCallForPricing.Visible = true;
                uxStatus.Visible = false;

                if (!string.IsNullOrEmpty(this.Product.CallMessage))
                {
                    uxCallForPricing.Text = this.Product.CallMessage;
                }
            }
            else
            {
                lblstockmessage.Visible = true;
            }

            // Bind selected sku product price if one exists
            uxProductPrice.Quantity = this.ShoppingCartQuantity;
            uxProductPrice.Product = this.Product;
            uxProductPrice.Bind();

            // Set product properties       
            ProductTitle.Text = string.Format(ProductTitle.Text, this.Product.Name);
            ProductID.Text = this.Product.ProductNum;

            // Set Manufacturer Name
            //if (this.Product.ManufacturerName != string.Empty)
            //{               
            //    //Zeon custom Code:Start    
            //    BrandLabel.Visible = true;
            //    SetBrandLinkAttribute();
            //    //Zeon custom Code:End
            //}
            upnlStock.Update();  //Zeon custom Code:Start
            return status;
        }

        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Adds Item to Shopping Cart
        /// </summary>

        public bool AddtoCart(object sender, EventArgs e)
        {
            string strlog = "\nAdd to Cart Button Click Details. \nIP : {0} \nBrowser: {1}, \nVersion : {2}, \nType : {3}, \nProductNum : {4}, \nQty : {5}\n\n";
            strlog = string.Format(strlog, Request.UserHostAddress, Request.Browser.Browser, Request.Browser.Version, Request.UserAgent, this.Product.ProductNum, txtQuantity.Text);
            Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(strlog);

            // Reset status
            uxStatus.Text = string.Empty;

            try
            {
                if (this.UpdateProductStatus())
                {
                    // Create shopping cart item
                    ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
                    item.Product = new ZNodeProductBase(this.Product);
                    item.Quantity = this.ShoppingCartQuantity;

                    // Add product to cart
                    ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

                    // If shopping cart is null, create in session
                    if (shoppingCart == null)
                    {
                        shoppingCart = new ZNodeShoppingCart();
                        shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
                    }

                    // Add item to cart
                    if (shoppingCart.AddToCart(item))
                    {
                        //Update Display Order
                        ZNodeDisplayOrder displayOrder = new ZNodeDisplayOrder();
                        displayOrder.SetDisplayOrder(this.Product);

                        ZNodeSavedCart.AddToSavedCart(item);
                        //Zeon custom Code:Starts
                        //string link = "~/shoppingcart.aspx";
                        //Response.Redirect(link);
                        LoadCustomAddtoCartSetting();

                        strlog = "\nItem added into cart Details. \nIP : {0} \nBrowser: {1}, \nVersion : {2}, \nType : {3}, \nProductNum : {4}, \nQty : {5}\n\n";
                        strlog = string.Format(strlog, Request.UserHostAddress, Request.Browser.Browser, Request.Browser.Version, Request.UserAgent, this.Product.ProductNum, txtQuantity.Text);
                        Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(strlog);

                        return true;

                        //Zeon custom Code:Ends
                    }
                    else
                    {
                        strlog = "\nItem not Added into Cart Details due to {6}. \nIP : {0} \nBrowser: {1}, \nVersion : {2}, \nType : {3}, \nProductNum : {4}, \nQty : {5}\n\n";
                        strlog = string.Format(strlog, Request.UserHostAddress, Request.Browser.Browser, Request.Browser.Version, Request.UserAgent, this.Product.ProductNum, txtQuantity.Text, this.Product.OutOfStockMsg);
                        Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(strlog);

                        // Display Out of Stock message
                        uxStatus.Visible = true;
                        uxStatus.Text = this.Product.OutOfStockMsg;
                        lblstockmessage.Visible = false;
                        return false;
                    }
                }
                else
                {
                    // Display Out of Stock message
                    if (!string.IsNullOrEmpty(uxStatus.Text))
                        uxStatus.Visible = true;
                    strlog = "\nItem validation in Cart Details due to {6}. \nIP : {0} \nBrowser: {1}, \nVersion : {2}, \nType : {3}, \nProductNum : {4}, \nQty : {5}\n\n";
                    strlog = string.Format(strlog, Request.UserHostAddress, Request.Browser.Browser, Request.Browser.Version, Request.UserAgent, this.Product.ProductNum, txtQuantity.Text, uxStatus.Text);
                    Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(strlog);
                    return false;
                }
            }
            catch (Exception ex)
            {
                strlog = "\nItem not Added into Cart Details. \nIP : {0} \nBrowser: {1}, \nVersion : {2}, \nType : {3}, \nProductNum : {4}, \nQty : {5}\n\n";
                strlog = string.Format(strlog, Request.UserHostAddress, Request.Browser.Browser, Request.Browser.Version, Request.UserAgent, this.Product.ProductNum, txtQuantity.Text);
                Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(strlog);
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return false;
            }
        }

        /// <summary>
        /// Triggered when addtoCart is fired on the product listing page using query string
        /// </summary>
        protected void Buy_Click()
        {
            // Reset status
            uxStatus.Text = string.Empty;

            // Check product attributes count
            if (this.Product.ZNodeAttributeTypeCollection.Count > 0)
            {
                return;
            }

            // Loop through the product addons
            foreach (ZNodeAddOn _addOn in this.Product.ZNodeAddOnCollection)
            {
                if (!_addOn.OptionalInd)
                {
                    return;
                }
            }

            // Create shopping cart item
            ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
            item.Product = new ZNodeProductBase(this.Product);
            item.Quantity = 1;

            // Add product to cart
            ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

            // If shopping cart is null, create in session
            if (shoppingCart == null)
            {
                shoppingCart = new ZNodeShoppingCart();
                shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
            }

            // Add item to cart
            if (shoppingCart.AddToCart(item))
            {
                // Update SavedCart items
                ZNodeSavedCart.AddToSavedCart(item);

                string link = "~/shoppingcart.aspx";
                Response.Redirect(link);
            }
            else
            {
                // Display Out of Stock message
                uxStatus.Visible = true;
                uxStatus.Text = this.Product.OutOfStockMsg;
                return;
            }
        }

        /// <summary>
        /// Represents the AddToViewList Method
        /// </summary>
        protected void AddToViewedList()
        {
            ZNodeProductList productList = null;
            if (Session["ProductViewedList" + ZNodeCatalogManager.LocaleId.ToString()] != null)
            {
                productList = Session["ProductViewedList" + ZNodeCatalogManager.LocaleId.ToString()] as ZNodeProductList;
                ZNodeProductBase objectToAdd = new ZNodeProductBase(this.Product);
                objectToAdd.NewProductInd = this.Product.NewProductInd;
                objectToAdd.FeaturedInd = this.Product.FeaturedInd;
                objectToAdd.CallForPricing = this.Product.CallForPricing;
                bool isItemExists = false;
                int index = 0;

                foreach (ZNodeProductBase product in productList.ZNodeProductCollection)
                {
                    if (product.ProductID == objectToAdd.ProductID)
                    {
                        isItemExists = true;
                        objectToAdd.DisplayOrder = Convert.ToInt32(Session["DisplayOrder"]) + 1;
                        productList.ZNodeProductCollection[index] = objectToAdd; // Update product details                    
                        break;
                    }

                    index++;
                }

                if (!isItemExists)
                {
                    objectToAdd.DisplayOrder = Convert.ToInt32(Session["DisplayOrder"]) + 1;
                    productList.ZNodeProductCollection.Add(objectToAdd);
                }

                Session["DisplayOrder"] = objectToAdd.DisplayOrder.ToString();
            }
            else
            {
                productList = new ZNodeProductList();

                Session["DisplayOrder"] = "1";

                ZNodeProductBase addProduct = new ZNodeProductBase(this.Product);
                addProduct.NewProductInd = this.Product.NewProductInd;
                addProduct.FeaturedInd = this.Product.FeaturedInd;
                addProduct.CallForPricing = this.Product.CallForPricing;
                addProduct.DisplayOrder = Convert.ToInt32(Session["DisplayOrder"]);
                productList.ZNodeProductCollection.Add(addProduct);

                Session["ProductViewedList" + ZNodeCatalogManager.LocaleId.ToString()] = productList;
            }
        }

        /// <summary>
        /// Add to cart button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxAddToCart_Click(object sender, EventArgs e)
        {
            //this.AddtoCart();
            bool isAddToCartSuccess = this.AddtoCart(sender, e);
            if (isAddToCartSuccess)
            {
                Response.Redirect("shoppingcart.aspx");
            }
            uxStatus.Focus();
        }

        /// <summary>
        /// Quantity Drop down list index changed event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Quantity_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.UpdateProductStatus();
        }

        /// <summary>
        /// Buy from Vendor site.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxBuyDirectFromVendor_Click(object sender, EventArgs e)
        {
            decimal price = uxProductPrice.ProductPrice;
            ZNode.Libraries.ECommerce.Analytics.ZNodeOutboundTracking outboundTracking = new ZNode.Libraries.ECommerce.Analytics.ZNodeOutboundTracking();
            outboundTracking.LogTrackingEvent("Product Referral", HttpContext.Current.Request.Url.ToString(), this.Product.AffiliateUrl, this.Product.ProductID, price);
        }

        /// <summary>
        /// Send a Email Link Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void EmailLink_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/emailfriend.aspx?zpid=" + this._productId.ToString());
        }

        /// <summary>
        /// Add to Wish List link button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddToWishList_Click(object sender, EventArgs e)
        {
            ZNodeUserAccount _userAccount = ZNodeUserAccount.CurrentAccount();
            string redirectURL = "~/account.aspx";
            if (_userAccount != null)
            {
                WishListService service = new WishListService();
                WishListQuery query = new WishListQuery();
                query.AppendEquals(WishListColumn.ProductID, this._productId.ToString());
                query.AppendEquals(WishListColumn.AccountID, _userAccount.AccountID.ToString());
                TList<WishList> wishList = service.Find(query.GetParameters());

                if (wishList.Count == 0)
                {
                    WishList wishListEntity = new WishList();
                    wishListEntity.AccountID = _userAccount.AccountID;
                    wishListEntity.ProductID = this._productId;
                    wishListEntity.CreateDte = System.DateTime.Today;
                    service.Insert(wishListEntity);
                }
            }
            else
            {
                redirectURL += "?zpid=" + this._productId;
            }

            Response.Redirect(redirectURL);
        }

        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {


            this.BindDefaultValues();

            this.BindProduct();

            // Bind reference user controls
            this.BindControls();

            uxStatus.Visible = false;

            // Check if add to cart action was requested from a different page
            if (Request.Params["action"] != null)
            {
                if (Request.Params["action"].Equals("addtocart"))
                {
                    if (!Page.IsPostBack)
                    {
                        this.Buy_Click();
                    }
                }
            }

            //Zeon Custom Code: Start
            if (!IsPostBack)
            {
                // BindTierPriceCollection();
                BindProductSchemaMetaTag();
            }
            this.GetSizeOfImage();
            //Assign ProductId to control
            uxAternateImages.ProductID = this._productId;
            ManageAddToCartText();
            BindVideoURLAndThumbnailImage();
            lblStoreAvailErrorMsg.Visible = false;
            this.ShowMultipleProductAttributeGrid();
            this.HideControlByCurrentPortal();
            //Disable Store Locator Link
            SetDefaultStoreLocatorSetting();
            allowOutOfStockToUser = Roles.IsUserInRole("ADMIN") || Roles.IsUserInRole("CUSTOMER SERVICE REP") ? true : false;
            if (!IsPostBack)
            {
                this.DisableAddToCartButton(true);
            }
            txtQuantity.Focus();           
            //Zeon Custom Code: Ends

        }

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {

            if (Session["BreadCrumzcid"] != null)
            {
                this.zcid = Session["BreadCrumzcid"].ToString();
            }

            #region[Code Commented]
            /*
            else
            {
                ZNode.Libraries.DataAccess.Service.ProductCategoryService service = new ZNode.Libraries.DataAccess.Service.ProductCategoryService();
                TList<ProductCategory> pcategories = service.GetByProductID(Convert.ToInt32(Request.QueryString["zpid"]));
                if (pcategories.Count > 0)
                {
                    this.zcid = pcategories[0].CategoryID.ToString();
                }
            }

            // Retrieve category data from httpContext (set previously in the page_preinit event)
            ZNode.Libraries.DataAccess.Service.CategoryService cservice = new ZNode.Libraries.DataAccess.Service.CategoryService();
            Category category = cservice.GetByCategoryID(Convert.ToInt32(zcid));

            if (category != null)
            {
                //CategoryTitle.Text = category.Name;
            }
            */
            #endregion

            //hdnCurrentCategory.Value = this.zcid;

            //Zeon Custom Code : Start
            RegisterClientScriptClasses();
            if (this.Product != null && !this.Product.CallForPricing)
            {
                bool isCustomPortal = ZCommonHelper.IsCustomMulitipleAttributePortal();
                if (!isCustomPortal)
                {
                    BindTierPriceCollection();
                    RegisterSideCarScript();
                }
            }
            this.BindLastAttributeStoreLocatorSetting();
            //Zeon Custom Code : Start
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Bind all the controls to the data
        /// </summary>
        private void BindControls()
        {
            uxReviewRating.ProductName = this.Product.Name;
            // Review star rating        
            uxReviewRating.TotalReviews = this.Product.TotalReviews;
            uxReviewRating.ReviewRating = this.Product.ReviewRating;
            uxReviewRating.ViewProductLink = this.Product.ViewProductLink;


            Hyperlink.HRef = "~/customerreview.aspx?zpid=" + this.Product.ProductID;

            // Related products        
            //uxProductRelated.Product = this.Product;
            //uxProductRelated.Bind();

            // Best Sellers
            //uxBestSellers.BindProducts();

            // Set visible for best sellers and related products
            //pnlBestSellers.Visible = uxBestSellers.HasItems;
            //pnlProductRelated.Visible = uxProductRelated.HasItems;

            //if (!uxProductRelated.HasItems)
            //{
            //    ProductTabs.Visible = false;
            //}
            //else
            //{
            //    ProductTabs.Visible = true;
            //}

            // Registers the event for the payment (child) control
            this.uxProductAttributes.SelectedIndexChanged += new EventHandler(this.UxProductAttributes_SelectedIndexChanged);
            this.uxProductAddOns.SelectedIndexChanged += new EventHandler(this.UxProductAddOns_SelectedIndexChanged);
            this.uxBundleProduct.SelectedIndexChanged += new EventHandler(this.UxBundleProduct_SelectedIndexChanged);
            this.uxProductAttributes.ColorSelectedIndexChanged += new EventHandler(this.uxProductAttributes_ColorSelectedIndexChanged);


            // Highlights         
            uxHighlights.Product = this.Product;
            uxHighlights.Bind();
            if (uxHighlights.Product != null)
            {
                uxHighlights.Visible = true;
            }


            // Price
            uxProductPrice.Visible = this.Profile.ShowPrice && !this.Product.CallForPricing;
            pnlQty.Visible = this.Profile.ShowPrice;
        }

        /// <summary>
        /// Bind a product
        /// </summary>
        private void BindProduct()
        {
            // Get product id from querystring  
            if (Request.Params["zpid"] != null)
            {
                this._productId = int.Parse(Request.Params["zpid"]);
            }
            else
            {
                throw new ApplicationException("Invalid Product Id");
            }

            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this.Product = (ZNodeProduct)HttpContext.Current.Items["Product"];

            if (!Page.IsPostBack)
            {
                this.AddToViewedList();
                ZNodeDisplayOrder displayOrder = new ZNodeDisplayOrder();
                displayOrder.SetDisplayOrder(this.Product);
            }

            // Set Visible true for Affilate Url
            //if (!string.IsNullOrEmpty(this.Product.AffiliateUrl))
            //{
            //    uxBuyDirectFromVendor.Visible = true;
            //    uxBuyDirectFromVendor.Attributes.Add("onclick", "return affiliateurl_click('" + this.Product.AffiliateUrl + "');" + Page.ClientScript.GetPostBackEventReference(uxBuyDirectFromVendor, string.Empty).ToString());
            //}
            //else
            //{
            //    uxBuyDirectFromVendor.Visible = false;
            //}

            this.BindProductData();

            //Zeon Custom Code:Start
            if (this.Product != null && this.Product.DropShipInd == true)
            {
                lblstockmessage.Visible = false;
                upnlStock.Update();
            }
            //Zeon Custom Code:End
        }

        /// <summary>
        /// Represents the BindDefaultValues Method
        /// </summary>
        private void BindDefaultValues()
        {
            // This code must be placed here for SEO Url Redirect to work and fix postback problem with URL rewriting
            if (this.Page.Form != null)
            {
                this.Page.Form.Action = Request.RawUrl;
            }

            // Set button image
            uxAddToCart.Attributes.Add("onclick", "return detect()");
            FeaturedItemImage.ImageUrl = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/sale.png";
            //NewItemImage.ImageUrl = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/new.png";
            BindStockAvailabilityMetaTag();//Zeon Custom Code to move meta tag to resource File
        }

        /// <summary>
        /// Represents the BindProductData method
        /// </summary>
        private void BindProductData()
        {
            // set product properties       
            ProductTitle.Text = string.Format(ProductTitle.Text, this.Product.Name);
            ProductID.Text = this.Product.ProductNum;
            //ProductDescription.Text = Server.HtmlDecode(this.Product.Description);

            // Set Manufacturer Name
            //if (this.Product.ManufacturerName != string.Empty)
            //{
            //    BrandLabel.Visible = true;
            //    //Zeon Custom Code:Start
            //    if (!Page.IsPostBack)
            //    {
            //        SetBrandLinkAttribute();
            //    }
            //    //Zeon Custom Code:End
            //}

            CheckInventory(this.Product);
            if (!Page.IsPostBack)
            {
                this.BindStockData();
            }
        }

        /// <summary>
        /// Bind all the controls to the data
        /// </summary>
        private void BindStockData()
        {
            //uxProductSwatches.ProductId = this._productId;
            // pnlSwatches.Visible = uxProductSwatches.HasImages;

            // Product highlights         
            uxHighlights.Product = this.Product;
            uxHighlights.Bind();

            uxProductPrice.Visible = this.Profile.ShowPrice && !this.Product.CallForPricing;
            pnlQty.Visible = this.Profile.ShowPrice;

            bool showCartButton = this.Profile.ShowAddToCart;

            // If out of stock and backorder disabled then hide the AddToCart button
            if (this.Product.ZNodeAttributeTypeCollection.Count > 0)
            {
                // If product has attributes,then display the AddToCart button
                showCartButton &= true;
            }
            else if (this.Product.AllowBackOrder && this.Product.QuantityOnHand <= 0)
            {
                // Allow back Order
                showCartButton &= true;

                // Set back order message
                lblstockmessage.Text = this.Product.BackOrderMsg;
            }
            else if (this.Product.AllowBackOrder && this.Product.QuantityOnHand > 0)
            {
                showCartButton &= true;

                // Set Item In-Stock message
                lblstockmessage.Text = inStocklink + this.Product.InStockMsg;
            }
            else if (this.Product.QuantityOnHand > 0 && this.Product.TrackInventoryInd == true && !this.Product.AllowBackOrder)
            {
                // Track Inventory
                showCartButton &= true;

                // Set In stock message
                lblstockmessage.Text = inStocklink + this.Product.InStockMsg;
            }
            else if (this.Product.AllowBackOrder == false && this.Product.TrackInventoryInd == false && this.Product.QuantityOnHand > 0)
            {
                // Don't track Inventory Items can always be added to the cart,TrackInventory is disabled
                showCartButton &= true;

                // Set In stock message
                lblstockmessage.Text = inStocklink + this.Product.InStockMsg;
            }
            else if (this.Product.AllowBackOrder == false && this.Product.TrackInventoryInd == false && this.Product.QuantityOnHand <= 0)
            {
                // Don't track Inventory. Items can always be added to the cart,TrackInventory is disabled
                showCartButton &= true;

                // Set OutOf Stock message
                lblstockmessage.Text = string.Empty;
            }
            else
            {
                // Set OutOf Stock message
                lblstockmessage.Text = outOfStocklink + this.Product.OutOfStockMsg;
                lblstockmessage.CssClass = "OutOfStockMsg";
                showCartButton &= false;
            }

            // Set Visible true for NewItem.
            if (this.Product.NewProductInd)
            {
                //NewItemImage.Visible = true;
                divProdPageNewItem.Visible = true;
                //divProdPageNewItem.Style.Add("display","block");
            }

            //if (this.Product.InventoryDisplay == 1)
            //{
            //    uxBuyDirectFromVendor.Visible = true;
            //}

            // Set Visible true for FeaturedItem.
            if (this.Product.FeaturedInd)
            {
                FeaturedItemImage.Visible = true;
            }

            // Add to cart button
            if (this.Product.CallForPricing)
            {
                // If call for pricing is enabled,then hide AddToCart Button
                uxCallForPricing.Text = ZNodeCatalogManager.MessageConfig.GetMessage("ProductCallForPricing");
                showCartButton &= false;
                FeaturedItemImage.Visible = false;
                lblstockmessage.Text = string.Empty;
                lblstockmessage.CssClass = string.Empty;
                uxCallForPricing.Visible = !showCartButton;

                if (!string.IsNullOrEmpty(this.Product.CallMessage))
                {
                    uxCallForPricing.Text = this.Product.CallMessage;
                }
            }

            uxAddToCart.Visible = showCartButton;
        }

        /// <summary>
        /// Product Addons selected Index Changed Event raised by the Product Addon control
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        private void UxProductAddOns_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region Zeon Custom Code
            if (string.IsNullOrEmpty(txtQuantity.Text))
            {
                return;
            }
            #endregion Zeon Custom Code
            this.UpdateProductStatus();
        }

        /// <summary>
        /// Product Attributes selected Index Changed Event raised by the Product Attributes control
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        private void UxProductAttributes_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strlog = "\nAttribute Event (Size/width). \nIP : {0} \nBrowser: {1}, \nVersion : {2}, \nType : {3}, \nProductNum : {4}, \nQty : {5}\n\n";
            strlog = string.Format(strlog, Request.UserHostAddress, Request.Browser.Browser, Request.Browser.Version, Request.UserAgent, this.Product.ProductNum, txtQuantity.Text);
            Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(strlog);

            #region Zeon Custom Code
            if (string.IsNullOrEmpty(txtQuantity.Text))
            {
                return;
            }
            #endregion Zeon Custom Code
            DropDownList ddl = (DropDownList)sender;
            this.Product.SelectedSKU = uxProductAttributes.SelectedSKU;

            if (this.Product.SelectedSKU.SKUID > 0)
            {
                ScriptManager.RegisterClientScriptBlock(UpdPnlOrderingOptions, sender.GetType(), "AttributeImage", "document.getElementById('CatalogItemImage').src='" + ResolveUrl(this.Product.MediumImageFilePath) + "';", true);
                UpdPnlOrderingOptions.Update();
            }

            //Zeon Customization ::
            //this.UpdateProductStatus();
            this.UpdateStatusOnAttributeSelectedIndexChanged();
            uxStatus.Focus();
        }

        /// <summary>
        /// Event is raised when BundleProduct Selected Index Changed event is called
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        private void UxBundleProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdPnlOrderingOptions.Update();
            this.UpdateProductStatus();
        }

        protected void uxProductAttributes_ColorSelectedIndexChanged(object sender, EventArgs e)
        {
            string strlog = "\n Color Change Event. \nIP : {0} \nBrowser: {1}, \nVersion : {2}, \nType : {3}, \nProductNum : {4}, \nQty : {5}\n\n";
            strlog = string.Format(strlog, Request.UserHostAddress, Request.Browser.Browser, Request.Browser.Version, Request.UserAgent, this.Product.ProductNum, txtQuantity.Text);
            Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(strlog);

            if (!uxProductAttributes.IsLastAttributeSelectedIndexChanged)
            {
                this.DisableAddToCartButton(true);
            }
            else
            {
                this.UpdateStatusOnAttributeSelectedIndexChanged();
            }

        }

        #endregion

        #region Zeon Custom Methods

        /// <summary>
        /// Set the Message for the remaining Quantity in Hand
        /// </summary>
        /// <param name="productQtyinHand"></param>
        /// <returns></returns>
        private string ItemsLeftInInventory(int productQtyinHand)
        {
            int qtyInHand = 5;
            int.TryParse(ConfigurationManager.AppSettings["MinimumQuantity"].ToString(), out qtyInHand);
            if (productQtyinHand < qtyInHand)
            {
                return string.Format(this.GetLocalResourceObject("QuantityLeft").ToString(), qtyInHand);
            }
            return string.Empty;
        }

        /// <summary>
        /// load custom setting on addto cart 
        /// </summary>
        private void LoadCustomAddtoCartSetting()
        {
            uxStatus.Text = (string)GetLocalResourceObject("SuccessMessage");
            uxStatus.Visible = true;
            uxStatus.CssClass = "InStockMsg";
            UpdateMiniCart();
        }

        /// <summary>
        /// Update Mini cart for flyout
        /// </summary>
        protected void UpdateMiniCart()
        {
            ZNodeShoppingCart ShoppingCart = (ZNodeShoppingCart)ZNodeCheckout.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
            string cartCount = "0", cartTotal = "$0.00";
            if (ShoppingCart != null)
            {
                cartCount = GetShoppingCartTotalItemCount(ShoppingCart.ShoppingCartItems);
                cartTotal = ShoppingCart.SubTotal.ToString("c");
                ScriptManager.RegisterClientScriptBlock(Page, GetType(), "Script", "objProduct.UpdateMiniCart('" + cartCount + "', '" + cartTotal + "');", true);
            }
        }

        /// <summary>
        /// Size of Product Image
        /// </summary>
        private void GetSizeOfImage()
        {
            try
            {
                string createLargeImage = this.Product.LargeImageFilePath;
                hdnfSmallImagename.Value = ZNodeConfigManager.SiteConfig.MaxCatalogItemMediumWidth.ToString();
                hdnfLargeImagename.Value = ZNodeConfigManager.SiteConfig.MaxCatalogItemLargeWidth.ToString();
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Set Brand link control properties
        /// </summary>
        private void SetBrandLinkAttribute()
        {
            string imgTag = "<img src={0} alt={1} title={2} />";
            ZNodeImage znodeImage = new ZNodeImage();
            ManufacturerExtnService service = new ManufacturerExtnService();
            ManufacturerExtn manufacaturerExtn = this.Product.ZNodeManufacturerExtnList != null && this.Product.ZNodeManufacturerExtnList.Count > 0
                ? this.Product.ZNodeManufacturerExtnList[0] : service.GetByManufacturerID(this.Product.ManufacturerID)[0];
            if (manufacaturerExtn != null)
            {
                //BrandLink.NavigateUrl = !string.IsNullOrEmpty(manufacaturerExtn.SEOURL) ? "~/" + manufacaturerExtn.SEOURL : "~/brand.aspx?mid=" + this.Product.ManufacturerID;
                BrandLink.Text = !string.IsNullOrEmpty(manufacaturerExtn.Image) ? this.Product.ManufacturerName + string.Format(imgTag, znodeImage.GetImageBySize(ZNodeConfigManager.SiteConfig.MaxCatalogItemCrossSellWidth, manufacaturerExtn.Image).Replace("~/", ""), this.Product.ManufacturerName, this.Product.ManufacturerName) : this.Product.ManufacturerName;
            }
            else
            {
                //BrandLink.NavigateUrl = "~/brand.aspx?mid=" + this.Product.ManufacturerID;
                BrandLink.Text = this.Product.ManufacturerName;
            }
        }

        /// <summary>
        /// Set text of  uxAddToCart control
        /// </summary>
        private void ManageAddToCartText()
        {
            try
            {
                if (this.Product != null && this.Product.PreOrderFlag)
                {
                    uxAddToCart.Text = this.GetLocalResourceObject("uxAddToCartPreOrderText").ToString();
                }
                else
                {
                    uxAddToCart.Text = this.GetLocalResourceObject("uxAddToCartText").ToString();
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Bind Tier Price Collection
        /// </summary>
        private void BindTierPriceCollection()
        {
            bool isProductHasCustomPromotion = false;
            int tierIndex = 0;
            StringBuilder htmlBuilder = new StringBuilder();
            StringBuilder priceRow = new StringBuilder();
            StringBuilder qtyRow = new StringBuilder();
            if (this.Product != null && this.Product.ZNodeTieredPriceCollection != null && this.Product.ZNodeTieredPriceCollection.Count > 0)
            {
                decimal price = 0;
                foreach (ZNode.Libraries.ECommerce.Entities.ZNodeProductTierEntity productTier in this.Product.ZNodeTieredPriceCollection)
                {
                    //Create Qty Row
                    qtyRow.Append("<td>");
                    if (tierIndex == 0)
                    {
                        qtyRow.Append(productTier.TierStart + " - " + productTier.TierEnd);
                    }
                    else
                    {
                        qtyRow.Append((Convert.ToInt32(productTier.TierStart.ToString())) + " - " + productTier.TierEnd.ToString());
                    }
                    qtyRow.Append("</td>");

                    //Create Price Row
                    string priceText = string.Empty;
                    if (tierIndex == 0 || isProductHasCustomPromotion)
                    {
                        ZNode.Libraries.ECommerce.Promotions.ZNodePromotionPricingOption pricing = new ZNode.Libraries.ECommerce.Promotions.ZNodePromotionPricingOption();
                        price = pricing.PromotionalPrice(this.Product, productTier.Price);
                        isProductHasCustomPromotion = this.Product.IsCustomPromotionApplied;
                    }
                    string htmText = this.Product.IsCustomPromotionApplied ? "<td><span class='RegularPrice'>" : "<td>";
                    priceRow.Append(htmText);
                    if (!this.Product.IsCustomPromotionApplied)
                    {
                        if (price == productTier.Price || price == 0)
                        {
                            priceText = string.Format(this.GetLocalResourceObject("TierRegulerPriceFormat").ToString(), productTier.Price.ToString("C"));
                        }
                        else
                        {
                            priceText = string.Format(this.GetLocalResourceObject("TierRegulerPriceFormat").ToString(), productTier.Price.ToString("C"));
                        }
                    }
                    else
                    {
                        priceText = productTier.Price.ToString("C") + string.Format(this.GetLocalResourceObject("TierSalesPriceFormat").ToString(), price.ToString("C")); ;
                    }
                    priceRow.Append(priceText);
                    priceRow.Append("</td>");
                    tierIndex++;

                }
                htmlBuilder.Append(string.Format(this.GetLocalResourceObject("TierPriceTableStructureFormat").ToString(), qtyRow.ToString(), priceRow.ToString()));
                //Create Proper HTML and Assign it to below variable.
                ltrProductTierPrice.Text = htmlBuilder.ToString();
            }
        }

        /// <summary>
        /// Get the cookies for the store locator
        /// </summary>
        /// <returns>Returns the HttpCookie</returns>
        private HttpCookie GetCookie()
        {
            HttpCookie storeCookie = null;
            int accID = ZNodeUserAccount.CurrentAccount() == null ? 0 : ZNodeUserAccount.CurrentAccount().AccountID;

            if (accID > 0)
            {
                string cookieName = Convert.ToString(accID) + "_StoreCookie";
                if (Request.Cookies[cookieName] != null)
                {
                    storeCookie = Request.Cookies[cookieName];
                }
            }
            else
            {
                if (Request.Cookies["StoreCookie"] != null)
                {
                    storeCookie = Request.Cookies["StoreCookie"];
                }
            }

            return storeCookie;
        }

        /// <summary>
        /// Assign the store wise availablity message
        /// </summary>
        private void AssignStoreWiseInventory()
        {
            ZNodeProduct prod = (ZNodeProduct)HttpContext.Current.Items["Product"];
            bool dropShipInd = prod != null ? prod.DropShipInd.GetValueOrDefault(false) : false;
            if (!dropShipInd)
            {
                SetCheckInStoreMessage();
            }
            else
            {
                SetDefaultStoreLocatorSetting();
            }
            upnlStoreInventory.Update();
        }

        /// <summary>
        /// Show the store wise inventory or show the check in store availability option
        /// </summary>
        /// <param name="product"></param>
        /// <param name="storeID"></param>
        /// <param name="storeCookie"></param>
        private void ShowStoreWiseInventory(ZNodeProduct product, int storeID, HttpCookie storeCookie)
        {
            string whereClause = storeID > 0 ? "StoreId=" + storeID + " AND SKUID= " + product.SelectedSKU.SKUID : "SKUID= " + product.SelectedSKU.SKUID;
            ZeonStoreWiseInventoryService service = new ZeonStoreWiseInventoryService();
            TList<ZeonStoreWiseInventory> selectedStoreList = service.Find(whereClause);
            if (selectedStoreList != null && selectedStoreList.Count > 0)
            {
                ZeonStoreWiseInventory selectedStore = selectedStoreList[0];
                if (selectedStore != null)
                {
                    SetCheckInStoreMessage();
                }
            }

        }

        /// <summary>
        /// Set the Out of store with Check in availability
        /// </summary>
        private void SetCheckInStoreMessage()
        {
            lnkInStoreAvailability.Style["display"] = "block";
            infoIcon.Style["display"] = "block";
        }

        /// <summary>
        /// Bind the video url. If present then show the control else hide the iframe control
        /// </summary>
        private void BindVideoURLAndThumbnailImage()
        {
            try
            {
                ProductExtnService prodExtService = new ProductExtnService();

                ifrmYoutubeVideo.Visible = false;
                imgYouTubeVideo.Visible = false;
                string videoUrl = string.Empty;
                if (this.Product != null && !string.IsNullOrEmpty(this.Product.VideoUrl))
                {
                    videoUrl = this.Product.VideoUrl;
                }

                #region [Code commented]

                //    TList<ProductExtn> prodList = prodExtService.GetByProductID(this.Product.ProductID);
                //    if (prodList != null && prodList.Count > 0)
                //    {
                //        videoUrl = prodList[0].VideoUrl;
                //    }

                #endregion

                if (!string.IsNullOrEmpty(videoUrl))
                {
                    ifrmYoutubeVideo.Visible = true;
                    ifrmYoutubeVideo.Attributes.Add("title", this.Product.Name + "video");//PRFT ADA Task Changes
                    if (!string.IsNullOrEmpty(videoUrl) && !videoUrl.StartsWith(URLStartString, StringComparison.CurrentCultureIgnoreCase))
                    {
                        videoUrl = HttpContext.Current.Request.IsSecureConnection ? SecuredURLString + videoUrl : NonSecuredURLString + videoUrl;
                    }

                    if (string.IsNullOrEmpty(videoUrl))
                    {
                        ifrmYoutubeVideo.Visible = false;
                    }
                    else
                    {
                        ShowHideVideoThumbnail(videoUrl);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Show Hide Video Thumbnail
        /// </summary>
        /// <param name="videoUrl"></param>
        private void ShowHideVideoThumbnail(string videoUrl)
        {
            string curVideoUrl = string.Empty;
            if (videoUrl.IndexOf('?') > 0 && videoUrl.Split('?').Length > 1) { curVideoUrl = videoUrl.Split('?')[0]; }
            string[] splittedURL = !string.IsNullOrWhiteSpace(curVideoUrl) ? curVideoUrl.Split('/') : videoUrl.Split('/');
            int youtubEmbedTagLen = YoutubeEmbdeTagLength;
            if (ConfigurationManager.AppSettings["YoutubeEmbedURLLength"] != null && !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["YoutubeEmbedURLLength"]))
            {
                int.TryParse(ConfigurationManager.AppSettings["YoutubeEmbedURLLength"].ToString(), out youtubEmbedTagLen);
            }
            if (splittedURL != null && splittedURL.Length > 0)
            {
                string videoID = splittedURL[splittedURL.Length - 1];
                if (!string.IsNullOrEmpty(videoID) && videoID.Length.Equals(youtubEmbedTagLen))
                {
                    imgYouTubeVideo.Visible = true;
                }
                else
                {
                    imgYouTubeVideo.Visible = false;
                }
            }
            else
            {
                imgYouTubeVideo.Visible = false;
            }
            ifrmYoutubeVideo.Attributes["src"] = videoUrl;
        }

        /// <summary>
        ///  Update Status On Attribute select event
        /// </summary>
        private void UpdateStatusOnAttributeSelectedIndexChanged()
        {
            // Update the panels
            UpdPnlOrderingOptions.Update();

            if (!uxProductAttributes.IsLastAttributeSelectedIndexChanged)
            {
                this.DisableAddToCartButton(true);
            }
            else
            {
                this.DisableAddToCartButton(false);
            }

            // Hide Product price
            uxProductPrice.Visible = this.Profile.ShowPrice && !this.Product.CallForPricing;
            uxAddToCart.Visible = this.Profile.ShowPrice && !this.Product.CallForPricing;
            uxProductAttributes.ResetAttributeErrorMessage(); //PRFT Custom Code

            // Check if product has attributes
            if (uxProductAttributes.IsLastAttributeSelectedIndexChanged && this.Product.ZNodeAttributeTypeCollection.Count > 0)
            {
                // If product has attributes then validate that all attributes have been selected by
                // the customer validate attributes first - This is to verify that all required attributes have a valid selection
                // if a valid selection is not found then an error message is displayed
                string attributeMessage = string.Empty;
                string attributeList = string.Empty;
                string selectedAttributesDescription = string.Empty;

                bool status = uxProductAttributes.ValidateAttributes(out attributeMessage, out attributeList, out selectedAttributesDescription);
                if (!status)
                {
                    if (!string.IsNullOrEmpty(attributeMessage))
                    {
                        uxStatus.Visible = true;
                        uxStatus.Text = attributeMessage;
                    }
                    // Hide AddToCart button
                    //uxAddToCart.Visible = false;

                    this.DisableAddToCartButton(true);
                }
                else
                {
                    uxAddToCart.Visible = true;
                    //Bind SKU Price on last attribute selected index changes
                    ZNodeSKU SKU = ZNodeSKU.CreateByProductAndAttributes(this.Product.ProductID, attributeList);
                    // Set Attributes Description to Description property
                    SKU.AttributesDescription = selectedAttributesDescription;
                    this.Product.SelectedSKU = SKU;
                    this.Product.CheckSKUProfile();
                    this.Product.IsPromotionApplied = false;
                    this.Product.ApplyPromotion();

                    // Check for 'Call for Pricing'
                    if (this.Product.CallForPricing)
                    {
                        lblstockmessage.Visible = false;
                        uxAddToCart.Visible = false;
                        uxCallForPricing.Visible = true;
                        uxStatus.Visible = false;

                        if (!string.IsNullOrEmpty(this.Product.CallMessage))
                        {
                            uxCallForPricing.Text = this.Product.CallMessage;
                        }
                    }
                    else
                    {
                        if (SKU.QuantityOnHand < this.ShoppingCartQuantity && (!this.Product.AllowBackOrder) && this.Product.TrackInventoryInd)
                        {
                            // Display Out of stock message
                            lblstockmessage.Text = this.Product.OutOfStockMsg;
                            lblstockmessage.CssClass = "OutOfStockMsg";
                        }
                        else
                        {
                            lblstockmessage.Text = inStocklink + this.Product.InStockMsg;
                            lblstockmessage.CssClass = "ProductStock";
                        }
                        lblstockmessage.Visible = true;
                    }
                    upnlStock.Update();

                    // Bind selected sku product price if one exists
                    uxProductPrice.Quantity = this.ShoppingCartQuantity;
                    uxProductPrice.Product = this.Product;
                    uxProductPrice.Bind();
                    //Show Store Locator Link
                    AssignStoreWiseInventory();
                }
            }
        }

        /// <summary>
        /// Registers the Script
        /// </summary>
        private void RegisterClientScriptClasses()
        {
            StringBuilder script = new StringBuilder();
            script.Append("var objProduct = new Product({");
            script.Append("\"Controls\":{");
            script.AppendFormat("\"{0}\":\"{1}\"", hdnfLargeImagename.ID, hdnfLargeImagename.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", hdnfSmallImagename.ID, hdnfSmallImagename.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", divEnlargedImage.ID, divEnlargedImage.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", inStoreAvailabilityListId.ID, inStoreAvailabilityListId.ClientID);
            script.Append("}});objProduct.Init();");

            this.Page.ClientScript.RegisterStartupScript(GetType(), "ProductScript", script.ToString(), true);

            if (ScriptManager.GetCurrent(Page).IsInAsyncPostBack)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, GetType(), "AttributeImageZoom", "InitializeZoom();", true);
            }
        }

        /// <summary>
        /// Show the other available store
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkInStoreAvailability_Click(object sender, EventArgs e)
        {
            HttpCookie storeCookie = GetCookie();
            int skuID = GetSelectedSKUID();
            if (skuID > 0)
            {
                int storeID = storeCookie != null ? int.Parse(storeCookie["StoreCookieID"].ToString()) : 0;
                SelectedStoreID = storeID;
                DataTable dtStoreInvData = ZNodeStoreList.GetListOfStorewiseAvalabilityOfSku(storeID, skuID, ZNodeConfigManager.SiteConfig.PortalID);
                if (dtStoreInvData != null && dtStoreInvData.Rows.Count > 0)
                {
                    rptInStoreAvailability.DataSource = dtStoreInvData;
                    rptInStoreAvailability.DataBind();

                }
                upnlStoreInv.Update();
                SetCheckInStoreMessage();
                upnlStoreInventory.Update();
            }
            else
            {
                rptInStoreAvailability.Visible = false;
                lblStoreAvailErrorMsg.Visible = true;
            }
        }

        /// <summary>
        /// Show Multiple Product AttributeGrid
        /// </summary>
        public void ShowMultipleProductAttributeGrid()
        {
            try
            {
                if (this.Product != null && this.Product.ZNodeAttributeTypeCollection != null && this.Product.ZNodeAttributeTypeCollection.Count > 0)
                {
                    string portalIDs = ConfigurationManager.AppSettings["ShowMultipleAttributeGridPortalIDs"] != null ? ConfigurationManager.AppSettings["ShowMultipleAttributeGridPortalIDs"].ToString() : string.Empty;
                    if (!string.IsNullOrEmpty(portalIDs))
                    {
                        string[] portalIDArray = portalIDs.Split(',');
                        if (portalIDArray != null && portalIDArray.Length > 0)
                        {
                            foreach (string portalIdStr in portalIDArray)
                            {
                                if (!string.IsNullOrEmpty(portalIdStr))
                                {
                                    int portalID = 0;
                                    int.TryParse(portalIdStr, out portalID);
                                    if (ZNodeConfigManager.SiteConfig.PortalID.Equals(portalID))
                                    {
                                        pnlGroupProductAttribute.Visible = true;
                                        uxProductSkuImages.Visible = this.Product.ProductSKUCount > 1 ? true : false;
                                        break;
                                    }
                                }
                            }
                        }
                        else if (portalIDs.Equals(ZNodeConfigManager.SiteConfig.PortalID.ToString()))
                        {
                            pnlGroupProductAttribute.Visible = true;
                            uxProductSkuImages.Visible = this.Product.ProductSKUCount > 1 ? true : false;
                        }
                    }
                }
                else
                {
                    pnlGroupProductAttribute.Visible = false;
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Disable Add To Cart Button
        /// </summary>
        /// <param name="isDisable"></param>
        public void DisableAddToCartButton(bool isDisable)
        {
            if (isDisable)
            {
                uxAddToCart.Enabled = true;
                divAddToCart.Attributes.Remove("class");
            }
            else
            {
                uxAddToCart.Enabled = true;
                divAddToCart.Attributes.Remove("class");
            }
        }

        /// <summary>
        /// get Shopping cart item total quantity count
        /// </summary>
        /// <returns></returns>
        private string GetShoppingCartTotalItemCount(ZNodeGenericCollection<ZNodeShoppingCartItem> shoppingCartItems)
        {
            int totalQuantity = 0;
            foreach (ZNodeShoppingCartItem item in shoppingCartItems)
            {
                totalQuantity += item.Quantity;
            }
            return totalQuantity.ToString();
        }

        /// <summary>
        /// Add Product Schema Meta Tags
        /// </summary>
        private void BindProductSchemaMetaTag()
        {
            try
            {
                if (this.Product != null && this.Product.ProductID > 0)
                {
                    string siteUrl = "https://" + ZNodeConfigManager.DomainConfig.DomainName;
                    string ImageMetaTag = !string.IsNullOrWhiteSpace(this.Product.MediumImageFilePath) ? string.Format(this.GetLocalResourceObject("ProductImageSchemaMetaTag").ToString(), siteUrl + this.Product.MediumImageFilePath.Replace("~", "")) : string.Empty;
                    string urlMetaTag = !string.IsNullOrWhiteSpace(this.Product.ViewProductLink) ? string.Format(this.GetLocalResourceObject("ProductURLSchemaMetaTag").ToString(), siteUrl + this.Product.ViewProductLink.Replace("~", "")) : string.Empty;
                    string aggrRatingMetaText = string.Format(this.GetLocalResourceObject("ProductAggregateRatingMetaTag").ToString(), this.Product.ReviewRating, this.Product.TotalReviews);
                    if (this.Product.ReviewRating != null && this.Product.ReviewRating > 0)
                    {
                        ltrProductMetaTag.Text = !string.IsNullOrWhiteSpace(urlMetaTag) && !string.IsNullOrWhiteSpace(ImageMetaTag) && !string.IsNullOrWhiteSpace(aggrRatingMetaText) ? urlMetaTag + ImageMetaTag + aggrRatingMetaText : string.Empty;
                    }
                    else
                    {
                        ltrProductMetaTag.Text = !string.IsNullOrWhiteSpace(urlMetaTag) && !string.IsNullOrWhiteSpace(ImageMetaTag) ? urlMetaTag + ImageMetaTag : string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Bind Meta Tags Format
        /// </summary>
        private void BindStockAvailabilityMetaTag()
        {
            inStocklink = this.GetLocalResourceObject("InstockLinkFormat").ToString();
            outOfStocklink = this.GetLocalResourceObject("OutOfStockstockLinkFormat").ToString();
        }

        /// <summary>
        /// Find If Current portal is Cheer and Pom
        /// </summary>
        private void HideControlByCurrentPortal()
        {
            bool isCustomPortal = ZCommonHelper.IsCustomMulitipleAttributePortal();
            inStoreAvailabilityListId.Visible = divStoreInventory.Visible = pnlDefaultButton.Visible = !isCustomPortal;
        }

        /// <summary>
        /// Set Default Store Setting 
        /// </summary>
        private void SetDefaultStoreLocatorSetting()
        {
            lnkInStoreAvailability.Style["display"] = "none";
            infoIcon.Style["display"] = "none";
            upnlStoreInventory.Update();
        }

        /// <summary>
        /// Set Selected Store Highlighted Class
        /// </summary>
        /// <param name="storeID">stroreID</param>
        protected string SetSelStoreHighlightedClass(object storeID)
        {
            string cssClass = string.Empty;
            int strID = 0;
            if (storeID != null && SelectedStoreID > 0)
            {
                int.TryParse(storeID.ToString(), out strID);
                if (strID == SelectedStoreID)
                {
                    cssClass = "SelStore";
                }
            }
            return cssClass;
        }

        /// <summary>
        /// If Last attribute is selected bind Store Setting
        /// </summary>
        private void BindLastAttributeStoreLocatorSetting()
        {
            if (!IsPostBack && uxProductAttributes.IsColorIsLastAttribute && !uxProductAttributes.IsLastAttributeSelectedIndexChanged)
            {
                AssignStoreWiseInventory();
            }
        }

        /// <summary>
        /// Get selected SKU ID
        /// </summary>
        /// <returns></returns>
        private int GetSelectedSKUID()
        {
            int skuID = 0;
            try
            {
                if (this.Product.ZNodeAttributeTypeCollection.Count > 0)
                {
                    // If product has attributes then validate that all attributes have been selected by
                    // the customer validate attributes first - This is to verify that all required attributes have a valid selection
                    // if a valid selection is not found then an error message is displayed
                    string attributeMessage = string.Empty;
                    string attributeList = string.Empty;
                    string selectedAttributesDescription = string.Empty;

                    bool status = uxProductAttributes.ValidateAttributes(out attributeMessage, out attributeList, out selectedAttributesDescription);
                    if (status)
                    {
                        //Bind SKU Price on last attribute selected index changes
                        ZNodeSKU SKU = ZNodeSKU.CreateByProductAndAttributes(this.Product.ProductID, attributeList);
                        return SKU.SKUID;
                    }
                }
            }
            catch (Exception ex)
            {
                return skuID;
            }
            return skuID;
        }

        private void RegisterSideCarScript()
        {
            StringBuilder script = new StringBuilder();
            if (Product != null && Product.ProductNum != null)
            {
                script.Append("var sidecar = sidecar || {};");
                script.Append(" sidecar.product_info = {group_id:");
                script.AppendFormat("'{0}'}};", Product.ProductNum);
                this.Page.ClientScript.RegisterStartupScript(GetType(), "SideCarScript", script.ToString(), true);
            }
        }
        #endregion;
    }
}