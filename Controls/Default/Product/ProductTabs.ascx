<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Product_ProductTabs" CodeBehind="ProductTabs.ascx.cs" %>
<%@ Register Src="~/Controls/Default/Reviews/ProductReviews.ascx" TagName="ProductReviews"
    TagPrefix="ZNode" %>
<%@ Register Src="ProductRelated.ascx" TagName="ProductRelated" TagPrefix="uc8" %>
<%@ Register Src="ProductAdditionalImages.ascx" TagName="ProductAdditionalImages"
    TagPrefix="uc9" %>
<%--<style>
    .morecontent span {display: none;}
</style>--%>
<div id="Tab" class="TabSection">
    <ul class="tabStructure">
        <li runat="server" id="liFeatureHeader"><a href="#tabsFeatures" title="DETAILS">
            <asp:Localize ID="locFeaturesTabs" runat="server" meta:resourcekey="pnlFeatures"></asp:Localize></a></li>
        <li><a href="#tabsCustomerReviews" title="REVIEWS">
            <asp:Localize ID="locCustomerReviewsTabs" runat="server" meta:resourcekey="pnlCustomerReviews"></asp:Localize></a></li>
        <li runat="server" id="liInformationHeader"><a href="#tabsAdditionalInformations" title="SHIPPING">
            <asp:Localize ID="locAdditionalInformationTabs" runat="server" meta:resourcekey="pnlAdditionalInformation"></asp:Localize></a></li>
        <li runat="server" id="liProductSpecs"><a href="#tabsProductSpecs" title="SPECIFICATIONS">
            <asp:Localize ID="locProductSpecsTabs" runat="server" meta:resourcekey="pnlProductSpecs"></asp:Localize></a></li>
    </ul>

    <h3 class="accordionStructure" runat="server" id="h3FeatureHeader"><a href="#tabsFeatures">
        <asp:Localize ID="locFeaturesAccordion" runat="server" meta:resourcekey="pnlFeatures"></asp:Localize></a></h3>

    <div id="tabsFeatures" runat="server" class="ProductPanel" clientidmode="Static">

        <asp:Label ID="ProductFeatureDesc" EnableViewState="false" runat="server" ClientIDMode="Static" itemprop="description"></asp:Label>
    </div>

    <h3 class="accordionStructure"><a href="#tabsCustomerReviews">
        <asp:Localize ID="locCustomerReviewsAccordion" runat="server" meta:resourcekey="pnlCustomerReviews"></asp:Localize></a></h3>

    <div id="tabsCustomerReviews" class="ProductPanel">
        <div class="Reviews">
            <ZNode:ProductReviews ID="uxProductReviews2" runat="server" PageSize="3" />
        </div>
    </div>

    <h3 class="accordionStructure" runat="server" id="h3InformationHeader"><a href="#tabsAdditionalInformations">
        <asp:Localize ID="locAdditionalInformationAccordion" runat="server" meta:resourcekey="pnlAdditionalInformation"></asp:Localize></a></h3>

    <div id="tabsAdditionalInformations" runat="server" clientidmode="Static" class="ProductPanel">
        <asp:Label ID="ProductAdditionalInformation" EnableViewState="false" runat="server" meta:resourcekey="ProductAdditionalInformationResource1" ClientIDMode="Static"></asp:Label>
    </div>

    <h3 class="accordionStructure" runat="server" id="h3ProductSpecs"><a href="#tabsProductSpecs">
        <asp:Localize ID="locProductSpecsAccordion" runat="server" meta:resourcekey="pnlProductSpecs"></asp:Localize></a></h3>

    <div id="tabsProductSpecs" runat="server" clientidmode="Static" class="ProductPanel">
        <div id="dvSpecification" runat="server" enableviewstate="false" class="CommonTable">
        </div>
    </div>
</div>


