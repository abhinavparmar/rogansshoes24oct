﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApp.Controls.Default.Product;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using System.Linq;
using System.Data.Linq;
using System.Text;
using System.Configuration;

namespace WebApp.Controls.Default.Product
{
    public partial class ZGroupProductAttributes : System.Web.UI.UserControl
    {
        #region[Global Varibale]

        private ZNodeProduct _product;

        #endregion

        #region[Public Property]

        public int ProductID { get; set; }

        #endregion

        #region[Page Events]

        protected void Page_Load(object sender, EventArgs e)
        {
            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this._product = (ZNodeProduct)HttpContext.Current.Items["Product"];
            if (this._product != null && this._product.ProductID > 0)
            {
                ProductID = this._product.ProductID;
                // Check for 'Call for Pricing'
                ManageAddToCartVisibility();
            }
            if (ShowMultipleProductAttributeGrid())
            {
                BindProductAttribute();
            }

        }

        /// <summary>
        /// Page PreRender Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            RegisterClientScriptClasses();
        }
        #endregion

        #region[Private Methods]

        /// <summary>
        /// Bind Product Attribute
        /// </summary>
        private void BindProductAttribute()
        {
            try
            {
                if (this._product != null && this._product.ZNodeAttributeTypeCollection != null && _product.ZNodeAttributeTypeCollection.Count > 0)
                {
                    this.CreateAttributeDomElement();
                }
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in ZGroupProductAttributes.ascx.cs!!Method>>BindProductAttribute!!Error>>" + ex.ToString());
            }

        }

        /// <summary>
        /// Create Attribute HTML Structure 
        /// </summary>
        /// <returns></returns>
        private string CreateAttributeDomElement()
        {
            string attributeDomElement = string.Empty;
            try
            {
                attributeDomElement = attributeDomElement + "<table  id=\"table-data\" class=\"table\">";
                attributeDomElement = attributeDomElement + this.GetAttributeDomHeader();
                attributeDomElement = attributeDomElement + this.GetAttributeControlAndValues();
                attributeDomElement = attributeDomElement + "</table>";

                ltlProductAttribute.Text = attributeDomElement;
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in ZGroupProductAttributes.ascx.cs!!Method>>CreateAttributeDomElement!!Error>>" + ex.ToString());
            }

            return attributeDomElement;
        }

        /// <summary>
        /// Retruns Attribute HTML Header
        /// </summary>
        /// <returns></returns>
        private string GetAttributeDomHeader()
        {
            string header = string.Empty;
            try
            {
                header = header + "<tr class>";

                foreach (ZNodeAttributeType attributeType in this._product.ZNodeAttributeTypeCollection)
                {
                    if (attributeType.ZNodeAttributeCollection != null && attributeType.ZNodeAttributeCollection.Count > 0)
                    {
                        header = header + "<th class='" + attributeType.Name + "'>" + attributeType.Name + "</th>";
                    }
                }
                header = header + this.GetLocalResourceObject("NameHeaderTextFormat").ToString();
                header = header + this.GetLocalResourceObject("QuantityHeaderTextFormat").ToString();
                header = header + this.GetLocalResourceObject("ItemCloseFormat").ToString();
                header = header + "</tr>";

            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in ZGroupProductAttributes.ascx.cs!!Method>>GetAttributeDomHeader!!Error>>" + ex.ToString());
            }

            return header;
        }

        /// <summary>
        /// Retruns HTML Drop Down Controls Structures
        /// </summary>
        /// <returns></returns>
        private string GetAttributeControlAndValues()
        {
            string attributeControlHtml = string.Empty;
            try
            {
                attributeControlHtml = attributeControlHtml + "<tr class=\"tr_clone\">";
                //int counter = this._product.ZNodeAttributeTypeCollection.Count;
                int count = 1;
                foreach (ZNodeAttributeType attributeType in this._product.ZNodeAttributeTypeCollection)
                {
                    if (attributeType.ZNodeAttributeCollection != null &&
                        attributeType.ZNodeAttributeCollection.Count > 0)
                    {
                        if (count == this._product.ZNodeAttributeTypeCollection.Count)
                        {
                            attributeControlHtml = attributeControlHtml + "<td> <select id=\"Select_" +
                                                   attributeType.AttributeTypeId + "\" name=" + attributeType.Name +
                                                   "  onchange=\"attributehelper.ValidateAttribute(this)\" data-typeid=" +
                                                   attributeType.AttributeTypeId + " data-display-order=" +
                                                   attributeType.DisplayOrder + ">";
                                attributeControlHtml = attributeControlHtml +
                                                       "<option id=\"opt\" value='0'>-Select-</option>";
                        }
                        else
                        {
                            attributeControlHtml = attributeControlHtml + "<td> <select id=\"Select_" +
                                                   attributeType.AttributeTypeId + "\" name=" + attributeType.Name +
                                                   " onchange=\"attributehelper.LoadNextLevelAttributeValues(this)\"  data-typeid=" +
                                                   attributeType.AttributeTypeId + " data-display-order=" +
                                                   attributeType.DisplayOrder + ">";
                                attributeControlHtml = attributeControlHtml +
                                                       "<option id=\"opt\" value='0'>-Select-</option>";
                        
                        }
                        foreach (ZNodeAttribute attribute in attributeType.ZNodeAttributeCollection)
                        {
                            attributeControlHtml = attributeControlHtml + "<option id=\"opt_" + attributeType.Name +
                                                   "\" value=" + attribute.AttributeId + ">" + attribute.Name +
                                                   "</option>";
                        }
                        attributeControlHtml = attributeControlHtml + "</select></td>";
                        count++;
                    }
                }
                attributeControlHtml = attributeControlHtml + "<td> <input type=\"text\" class=\"text\" id=\"txtComment\" style=\"width: 100px\" /></td>";
                attributeControlHtml = attributeControlHtml + "<td> <input type=\"text\" class=\"numeric\" id=\"txtQty\" style=\"width: 40px\" value=\"1\" /></td>";
                attributeControlHtml = attributeControlHtml + "<td> <input type=\"button\" name=\"add\" value=\"X\" class=\"close tr_clone_remove\"></td>";
                attributeControlHtml = attributeControlHtml + "</tr>";
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in ZGroupProductAttributes.ascx.cs!!Method>>GetAttributeControlAndValues!!Error>>" + ex.ToString());
            }
            return attributeControlHtml;
        }

        /// <summary>
        /// Registers the Script
        /// </summary>
        private void RegisterClientScriptClasses()
        {
            StringBuilder script = new StringBuilder();
            script.Append("var attributehelper = new AttributeHelper({");
            script.Append("\"Controls\":{");
            script.Append("},\"Messages\":{");
            script.AppendFormat("\"ItemInstockMessage\":\"{0}\"", this.GetLocalResourceObject("ItemInstockMessage").ToString());
            script.AppendFormat(",\"ItemOutOfstockMessage\":\"{0}\"", this.GetLocalResourceObject("ItemOutOfstockMessage").ToString());
            script.AppendFormat(",\"SKUNotAvailableMessage\":\"{0}\"", this.GetLocalResourceObject("SKUNotAvailableMessage").ToString());
            script.Append("}");
            script.Append("});attributehelper.Init();");
            this.Page.ClientScript.RegisterStartupScript(GetType(), "GroupProductAttributeScript", script.ToString(), true);
        }

        /// <summary>
        /// Show Multiple Product AttributeGrid
        /// </summary>
        public bool ShowMultipleProductAttributeGrid()
        {
            bool result = false;

            if (this._product != null && this._product.ZNodeAttributeTypeCollection != null && this._product.ZNodeAttributeTypeCollection.Count > 0)
            {
                string portalIDs = ConfigurationManager.AppSettings["ShowMultipleAttributeGridPortalIDs"] != null ? ConfigurationManager.AppSettings["ShowMultipleAttributeGridPortalIDs"].ToString() : string.Empty;
                if (!string.IsNullOrEmpty(portalIDs))
                {
                    string[] portalIDArray = portalIDs.Split(',');
                    if (portalIDArray != null && portalIDArray.Length > 0)
                    {
                        foreach (string portalIdStr in portalIDArray)
                        {
                            if (!string.IsNullOrEmpty(portalIdStr))
                            {
                                int portalID = 0;
                                int.TryParse(portalIdStr, out portalID);
                                if (ZNodeConfigManager.SiteConfig.PortalID.Equals(portalID))
                                {
                                    result = true;
                                    break;
                                }
                            }
                        }
                    }
                    else if (portalIDs.Equals(ZNodeConfigManager.SiteConfig.PortalID.ToString()))
                    {
                        result = true;
                    }
                }
            }
            else
            {
                result = false;
            }

            return result;

        }

        /// <summary>
        /// Manage Add To cart Link Visibility 
        /// </summary>
        private void ManageAddToCartVisibility()
        {
            ltrBotmCallForPrice.Text = ltrTopCallForPrice.Text = !string.IsNullOrWhiteSpace(this._product.CallMessage) ? string.Format(this.GetLocalResourceObject("CallForPriceMessageFormat").ToString(), this._product.CallMessage) : string.Format(this.GetLocalResourceObject("CallForPriceMessageFormat").ToString(), ZNodeCatalogManager.MessageConfig.GetMessage("ProductCallForPricing"));
            lnkBtmGrpAddToCart.Visible = lnkTopGrpAddToCart.Visible = !this._product.CallForPricing;
            ltrBotmCallForPrice.Visible = ltrTopCallForPrice.Visible = this._product.CallForPricing;
        }
        #endregion

    }
}