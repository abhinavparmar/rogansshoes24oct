﻿using System;
using System.Text;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.DataAccess.Custom;
using System.Collections.Generic;

namespace WebApp.Controls.Default.Product
{
    public partial class ZProductAlternateImages : System.Web.UI.UserControl
    {
        #region Private Members
        private int productID = 0;
        private ZNodeImage znodeImage = new ZNodeImage();
        private int maxDisplayColumns = ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.MaxCatalogCategoryDisplayThumbnails;
        private bool isProductPage = false;
        private int skuId = 0;
        private int defaultSKUId = 0;
        private bool isSKUList = false;

        #endregion

        #region Public Properties

        /// <summary>
        /// get or set productID value 
        /// </summary>
        public int ProductID
        {
            get { return productID; }
            set { productID = value; }
        }

        /// <summary>
        /// Gets or sets the maximum number of columns used to display the images. Defaults to Max Catalog Display Columns in General Settings.
        /// </summary>m
        public int MaxDisplayColumns
        {
            get
            {
                return this.maxDisplayColumns;
            }

            set
            {
                this.maxDisplayColumns = value;
            }
        }

        /// <summary>
        /// get or set value of _skuId
        /// </summary>
        public int SKUID
        {
            get
            {
                return this.skuId;
            }

            set
            {
                this.skuId = value;

                //BindAlternateImage();
            }
        }

        /// <summary>
        /// get or set default SKU
        /// </summary>
        public int DefaultSKU
        {
            set { defaultSKUId = value; }
            get { return defaultSKUId; }
        }

        public bool IsQuickWatch
        {
            get;
            set;
        }

        #endregion

        #region[Page Events]

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (SKUID > 0)
            {
                this.BindSkuAlternateImages();
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Bind All Alternate Image of Product
        /// </summary>
        private void BindSkuAlternateImages()
        {
            relAlternateImageList.Visible = true;
            if (SKUID > 0)
            {
                try
                {
                    ZeonSKUImageHelper zeonSkuImageHelper = new ZeonSKUImageHelper();
                    List<ZeonSKUImage> zeonSkuImageList = zeonSkuImageHelper.GetSkuImagesBySkuID(SKUID);
                    if (zeonSkuImageList != null && zeonSkuImageList.Count > 0)
                    {
                        this.isProductPage = true;
                        isSKUList = true;
                        RepeaterAlternateImage.DataSource = zeonSkuImageList;
                        RepeaterAlternateImage.DataBind();
                    }
                    else
                    {
                        relAlternateImageList.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    relAlternateImageList.Visible = false;
                    ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "Image File Currupted", "Alternate Image", "In ZProductAlternateImages.BindAlternateImage", "Fail", ex.StackTrace.ToString());
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log(ex.ToString());
                }
            }
            else
            {
                relAlternateImageList.Visible = false;
            }

        }

        #endregion

        #region Protected Methods and Events

        /// <summary>
        /// Get catalog product image Name
        /// </summary>
        /// <param name="title">The value of Title</param>
        /// <param name="imageFileName">Image File Name</param>
        /// <returns>Returns the Image name</returns>
        protected string GetImageName(string title, string imageFileName)
        {
            int idx = imageFileName.IndexOf('.');
            if (title.Trim().Length > 0)
            {
                return title;
            }
            else
            {
                if (imageFileName.Length > 0 && idx > 0 && idx < imageFileName.Length)
                {
                    return imageFileName.Remove(imageFileName.IndexOf('.'));
                }
                else
                {
                    return imageFileName;
                }
            }
        }

        /// <summary>
        /// Represents the ShowImage method
        /// </summary>
        /// <param name="ShowOnCategoryPage">Show on Category Page</param>
        /// <returns>Returns the bool value whether to show the Image on Category Page</returns>
        protected bool ShowImage(object ShowOnCategoryPage)
        {
            return this.isProductPage || bool.Parse(ShowOnCategoryPage.ToString());
        }

        /// <summary>
        /// Get product image alternative text
        /// </summary>
        /// <param name="altTagText">Alternate Tag text</param>
        /// <param name="productName">Product Name</param>
        /// <returns>Returns the Alternative Image Text</returns>
        protected string GetImageAltTagText(object altTagText, string productName)
        {
            if (altTagText == null)
            {
                return productName;
            }
            else if (altTagText.ToString().Length == 0)
            {
                return productName;
            }
            else
            {
                return altTagText.ToString();
            }
        }

        /// <summary>
        /// get image path
        /// </summary>
        /// <param name="fileName">string</param>
        /// <param name="useSwatch">string</param>
        /// <returns>string</returns>
        protected string GetImagePath(string fileName)
        {
            string sourceFileName = string.Empty;
            sourceFileName = this.znodeImage.GetImageHttpPathThumbnail(fileName);
            string largeImage = this.znodeImage.GetImageHttpPathLarge(fileName);
            return sourceFileName;
        }

        /// <summary>
        /// Event that is raised when Databind Method is called
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RepeaterAlternateImage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            string clickImg = string.Empty;
            try
            {
                if (isSKUList)
                {
                    //ListItemType type = e.Item.ItemType;
                    ZeonSKUImage img = (ZeonSKUImage)e.Item.DataItem;
                    clickImg = img.AlternateThumbnailImageFile;
                    if (string.IsNullOrEmpty(clickImg))
                    {
                        clickImg = img.ImageFile;
                    }
                }
                else
                {
                    ProductImage img = (ProductImage)e.Item.DataItem;
                    clickImg = img.AlternateThumbnailImageFile;
                    if (string.IsNullOrEmpty(clickImg))
                    {
                        clickImg = img.ImageFile;
                    }

                }
                if (this.isProductPage)
                {
                    clickImg = this.znodeImage.GetImageHttpPathMedium(clickImg);
                }
                else
                {
                    clickImg = this.znodeImage.GetImageHttpPathSmall(clickImg);
                }

                HtmlImage imgSwatch = (HtmlImage)e.Item.FindControl("imgSwatch");

                if (this.isProductPage)
                {
                    //imgSwatch.Attributes.Add("onclick", "document.getElementById('CatalogItemImage').src='" + ResolveUrl(clickImg) + "';");
                    if (IsQuickWatch)
                    {
                        imgSwatch.Attributes.Add("onclick", "document.getElementById('CatalogItemImage').src='" + ResolveUrl(clickImg) + "';");
                    }
                    else
                    {

                        imgSwatch.Attributes.Add("onclick", "objProduct.UpdateAlternateImage('" + ResolveUrl(clickImg) + "')");
                    }

                }
                else if (IsQuickWatch)
                {
                    imgSwatch.Attributes.Add("onclick", "document.getElementById('CatalogItemImage').src='" + ResolveUrl(clickImg) + "';");
                }
                else
                {
                    imgSwatch.Attributes.Add("onclick", "document.getElementsByName('img" + this.ProductID + "')[0].src='" + ResolveUrl(clickImg) + "';");
                }
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
            }
        }

        #endregion

        #region[Code Commented: As not int use]
        /*
        /// <summary>
        /// get all alternate images of product
        /// </summary>
        /// <returns>TList<ProductImage></returns>
        private void BindProductAlternateImages()
        { // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            ZNode.Libraries.ECommerce.Catalog.ZNodeProduct Product = (ZNode.Libraries.ECommerce.Catalog.ZNodeProduct)HttpContext.Current.Items["Product"];
            if (this.ProductID > 0)
            {
                // Bind AlternateImages
                //ZNode.Libraries.DataAccess.Service.ProductImageService service = new ZNode.Libraries.DataAccess.Service.ProductImageService();
                //var productImages = service.GetByProductIDActiveIndProductImageTypeID(this.ProductID, true, 1);
                if (Product != null && Product.ZNodeProductImage != null && Product.ZNodeProductImage.Count > 0)
                {
                    var productImages = Product.ZNodeProductImage;
                    this.isProductPage = true;
                    //RepeaterAlternateImage.RepeatColumns = this.MaxDisplayColumns;
                    isSKUList = false;
                    productImages.Sort("DisplayOrder");
                    RepeaterAlternateImage.DataSource = productImages;
                    RepeaterAlternateImage.DataBind();
                }
            }
        }
        */

        /*
        /// <summary>
        /// Bind All Alternate Image of Product
        /// </summary>
        private void BindAlternateImage()
        {
            relAlternateImageList.Visible = true;
            if (this.SKUID > 0)
            {
                try
                {
                    //Bind SkuImages
                    ZNode.Libraries.DataAccess.Service.ZeonSKUImageService service = new ZNode.Libraries.DataAccess.Service.ZeonSKUImageService();
                    ZeonSKUImageQuery query = new ZeonSKUImageQuery();
                    query.Append(ZeonSKUImageColumn.SKUID, this.SKUID.ToString());
                    query.Append(ZeonSKUImageColumn.ProductImageTypeID, "1,2");
                    TList<ZeonSKUImage> skuImages = service.Find(query);
                    if (skuImages != null && skuImages.Count > 0)
                    {
                        skuImages = skuImages.FindAll(x => x.ActiveInd == true);
                        this.isProductPage = true;
                        skuImages.Sort("DisplayOrder");
                        // Set up our grid.
                        //RepeaterAlternateImage.RepeatColumns = this.MaxDisplayColumns;
                        isSKUList = true;
                        RepeaterAlternateImage.DataSource = skuImages;
                        RepeaterAlternateImage.DataBind();
                    }
                    else
                    {
                        relAlternateImageList.Visible = false;

                        #region[Code Commented]
                        //if (this.DefaultSKU == this.SKUID)
                        //{
                        //    this.BindProductAlternateImages();
                        //}
                        //else
                        //{
                        //    relAlternateImageList.Visible = false;
                        //}
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    relAlternateImageList.Visible = false;
                    ZNodeLogging.LogMessage(ex.StackTrace.ToString());
                    ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "Image File Currupted", "Alternate Image", "In ZProductAlternateImages.BindAlternateImage", "Fail", ex.StackTrace.ToString());
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log(ex.ToString());

                }
            }
            else
            {
                relAlternateImageList.Visible = false;
            }
            //disable viewstate
            foreach (RepeaterItem item in RepeaterAlternateImage.Items)
            {
                item.EnableViewState = false;
            }
        }

        */
        #endregion

    }
}