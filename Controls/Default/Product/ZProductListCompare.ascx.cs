﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Utilities;

namespace WebApp.Controls.Default.Product
{
    public partial class ZProductListCompare : System.Web.UI.UserControl
    {
        #region Private Members

        private ZNodeProfile _ProductListProfile = new ZNodeProfile();
        private string _productIDs = string.Empty;
        private ZNodeProductList _productList = null;
        private string upcSpecificationText = System.Configuration.ConfigurationManager.AppSettings["UpcSpecificationText"].ToString();
        private string alternatePartNumberSpecificationText = System.Configuration.ConfigurationManager.AppSettings["AlternatePartNumberSpecificationText"].ToString();
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the product list profile
        /// </summary>
        public ZNodeProfile ProductListProfile
        {
            get { return this._ProductListProfile; }
            set { this._ProductListProfile = value; }
        }

        /// <summary>
        /// String Product IDS
        /// </summary>
        public string ProductIDs
        {
            get { return this._productIDs; }
            set { this._productIDs = value; }
        }

        /// <summary>
        /// Sets the Product List
        /// </summary>
        public ZNodeProductList ProductList
        {
            get { return this._productList; }
            set { this._productList = value; }
        }

        #endregion

        #region Page Events

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (NoProductsForComparison())
            {
                BindComparisonData();
            }
        }

        #region Private Methods


        /// <summary>
        /// Binds the product data for comparison
        /// </summary>
        private void BindComparisonData()
        {
            if (string.IsNullOrEmpty(this._productIDs))
            {
                List<CompareProducts> comparableProducts = Session["CompareProductIDs"] as List<CompareProducts>;
                foreach (CompareProducts prods in comparableProducts)
                {
                    this._productIDs = this._productIDs + Convert.ToString(prods.ProductID) + ",";
                }
                if (this._productIDs.LastIndexOf(',') > 0)
                {
                    this._productIDs = this._productIDs.Substring(0, this._productIDs.Length - 1);
                }
            }

            if (ProductList == null)
            {
                ProductList = ZNodeProductList.GetCompareProducts(this._productIDs,upcSpecificationText,alternatePartNumberSpecificationText);
            }
            rptlistPageCompare.DataSource = ProductList.ZNodeProductCollection;
            rptlistPageCompare.DataBind();
        }
        #endregion

        #region Public Method
        /// <summary>
        /// Check for the products.
        /// </summary>
        /// <returns>Returns true if there is not items in session</returns>
        public bool NoProductsForComparison()
        {
            bool noProductForComparison = false;
            if (Session["CompareProductIDs"] == null)
            {
                noProductForComparison = false;
            }
            else
            {
                List<CompareProducts> compProducts = Session["CompareProductIDs"] as List<CompareProducts>;

                if (compProducts != null && compProducts.Count > 0)
                {
                    noProductForComparison = true;
                }
            }
            return noProductForComparison;
        }

        #endregion

        #endregion

        #region Protected Member
        /// <summary>
        /// Get the image url
        /// </summary>
        /// <param name="imageName"></param>
        /// <returns></returns>
        protected string GetImageURL(object imageName)
        {
            string imageUrl = string.Empty;

            if (imageName != null)
            {
                ZNodeImage znodeImage = new ZNodeImage();
                imageUrl = znodeImage.GetImageHttpPathSmallThumbnail(Convert.ToString(imageName));
            }

            return imageUrl;
        }

        /// <summary>
        /// Get the Product url
        /// </summary>
        /// <param name="drr"></param>
        /// <returns></returns>
        protected string GetProductNavigationURL(object seoUrl, object productID)
        {
            string navigateUrl = string.Empty;

            if (seoUrl == null)
            {
                navigateUrl = "product.aspx?zpid=" + Convert.ToString(productID);
            }
            else
            {
                navigateUrl = Convert.ToString(seoUrl);
            }

            return navigateUrl.ToLower();
        }
        #endregion
    }
}