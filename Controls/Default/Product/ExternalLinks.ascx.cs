﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the External Links user control class.
    /// </summary>
    public partial class Controls_Default_Product_ExternalLinks : System.Web.UI.UserControl
    {
        #region Private Member Variables
        
        private int _ProductId;
        private bool _IsQuickWatch = false;
        private ZNodeProduct _Product;
      
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets a value indicating whether QuickWatch is enabled or not
        /// </summary>
        public bool IsQuickWatch
        {
            get
            {
                return this._IsQuickWatch;
            }

            set
            {
                this._IsQuickWatch = value;
            }
        }

        #endregion

        #region Page Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get product id from querystring  
            if (Request.Params["zpid"] != null)
            {
                this._ProductId = int.Parse(Request.Params["zpid"]);
            }
            else
            {
                throw new ApplicationException("Invalid Product Id");
            }

            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this._Product = (ZNodeProduct)HttpContext.Current.Items["Product"];

            EmailImage.ImageUrl = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/friend.png";
            WishListImage.ImageUrl = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/wish_list.gif";
            PrintImage.ImageUrl = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/Print.gif";

            if (this._IsQuickWatch)
            {
                EmailFriendLink.NavigateUrl = "javascript:self.parent.location ='" + ResolveUrl("~/emailfriend.aspx?zpid=" + this._ProductId.ToString()) + "'";
            }
            else
            {
                EmailFriendLink.NavigateUrl = "~/emailfriend.aspx?zpid=" + this._ProductId.ToString();
            }
        }
        public string AddThisScriptURL
        {
            get
            {
                return ConfigurationManager.AppSettings["SocialSharingURL"];
            }
        }

        public string AddThisLanguageTwoLetterCode
        {
            get
            {
                string retValue = "en";//Default to English
                //var locale = AppContext.Portal.Locale.LocaleCode;
                //if (locale != null)
                //{
                //    retValue = locale.Substring(0, 2);
                //}
                return retValue;
            }
        }
        #endregion

        #region General Events
        /// <summary>
        ///  Event is raised when WishListLink button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void WishListLink_Click(object sender, EventArgs e)
        {
            this.AddToWishList(sender);
        }

        /// <summary>
        /// Event is raised when WishListImage is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void WishListImage_Click(object sender, ImageClickEventArgs e)
        {
            this.AddToWishList(sender);
        }

        /// <summary>
        /// Event is raised when EmailImage button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void EmailImage_Click(object sender, ImageClickEventArgs e)
        {
            if (this._IsQuickWatch)
            {
                ScriptManager.RegisterClientScriptBlock(this, sender.GetType(), "BuyClick", "javascript:self.parent.location ='" + ResolveUrl("~/emailfriend.aspx?zpid=" + this._ProductId.ToString()) + "'", true);
            }
            else
            {
                Response.Redirect("~/emailfriend.aspx?zpid=" + this._ProductId.ToString());
            } 
        }

        #endregion

        #region Helper Methods
        /// <summary>
        /// Represents the AddToWishList method
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        protected void AddToWishList(object sender)
        {
            ZNodeUserAccount _userAccount = ZNodeUserAccount.CurrentAccount();
            string redirectURL = "~/account.aspx";

            if (_userAccount != null)
            {
                WishListService service = new WishListService();
                WishListQuery query = new WishListQuery();
                query.AppendEquals(WishListColumn.ProductID, this._ProductId.ToString());
                query.AppendEquals(WishListColumn.AccountID, _userAccount.AccountID.ToString());
                TList<WishList> wishList = service.Find(query.GetParameters());

                if (wishList.Count == 0)
                {
                    WishList wishListEntity = new WishList();
                    wishListEntity.AccountID = _userAccount.AccountID;
                    wishListEntity.ProductID = this._ProductId;
                    wishListEntity.CreateDte = System.DateTime.Today;
                    service.Insert(wishListEntity);
                }
            }
            else
            {
                redirectURL += "?zpid=" + this._ProductId;
            }

            if (this._IsQuickWatch)
            {
                ScriptManager.RegisterClientScriptBlock(this, sender.GetType(), "BuyClick", "javascript:self.parent.location ='" + ResolveUrl(redirectURL) + "'", true);
            }
            else
            {
                Response.Redirect(redirectURL);
            }
        }
        #endregion
    }
}