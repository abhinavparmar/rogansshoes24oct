﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Bundleproducts.ascx.cs"
    Inherits="WebApp.Controls_Default_Product_Bundleproducts" %>
<%@ Register Src="ProductAttributes.ascx" TagName="ProductAttributes" TagPrefix="ZNode" %>
<%@ Register Src="ProductAddOns.ascx" TagName="ProductAddOns" TagPrefix="ZNode" %>
<asp:Panel ID="pnlBundle" runat="server" ClientIDMode="Static">
    <div class="PageTitle">
        <asp:Label ID="CategoryTitle" Text="Items included" EnableViewState="false" runat="server" ClientIDMode="Static"></asp:Label></div>
    <div class="BundleProductlist">
        <asp:UpdatePanel ID="pnlBundleproductlist" runat="server" UpdateMode="Conditional" ClientIDMode="Static">
            <ContentTemplate>
                <asp:DataList ID="bundleProductList" RepeatDirection="Vertical" runat="server" OnItemDataBound="BundleProductList_ItemDataBound" ClientIDMode="Static">
                    <ItemTemplate>
                     <div class="BundleProductlistItem">
                        <div class="Title">
                            <asp:Label ID="ProductTitle" Text='<%# DataBinder.Eval(Container.DataItem, "Name").ToString() %>'
                                EnableViewState="false" runat="server" ClientIDMode="Static"></asp:Label>
                        </div>
                        <div class="ProductImage">
                            <img id='<%# "Img"  + DataBinder.Eval(Container.DataItem, "ProductID")%>' alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag") %>'
                                title='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag") %>'
                                border="0" src='<%# ResolveUrl(DataBinder.Eval(Container.DataItem, GetImageFileName()).ToString()) %>'
                                style="vertical-align: bottom;"></img>
                        </div>
                        <div class="Description">
                            <asp:Label ID="ProductDescription" Text='<%# DataBinder.Eval(Container.DataItem, "ShortDescription").ToString() %>'
                                EnableViewState="false" runat="server" Visible='<%# !IsQuickWatch %>' ClientIDMode="Static"></asp:Label></div>
                        <ZNode:ProductAttributes ID="uxBundleProductAttributes" BundleProductID='<%# DataBinder.Eval(Container.DataItem, "ProductID")%>'
                            runat="server" />
                        <ZNode:ProductAddOns ID="uxBundleProductAddOns" runat="server" ShowCheckBoxesVertically="true"
                            ShowRadioButtonsVerically="true" BundleProductID='<%# DataBinder.Eval(Container.DataItem, "ProductID")%>'>
                        </ZNode:ProductAddOns>
                     </div>
                    </ItemTemplate>
                    <SeparatorStyle CssClass="SeperatorBundleProduct" />
                </asp:DataList>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Panel>
 