using System;
using System.Data;
using System.Text;
using System.Web;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Catalog;

namespace WebApp
{
    /// <summary>
    /// Represents the ProductTabs user control class.
    /// </summary>
    public partial class Controls_Default_Product_ProductTabs : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProduct _product = new ZNodeProduct();
        private string Mode = string.Empty;
        #endregion

        #region Bind Method

        /// <summary>
        /// Represents the Bind Method
        /// </summary>
        public void Bind()
        {
            string prodInformation = this._product.AdditionalInformation;
            string prodFeatureDesc = this._product.Description.Trim();

            ShowSKUSpecification();

            if (string.IsNullOrEmpty(prodInformation))
            {
                HideProductInfoTab();
            }
            else
            {
                ProductAdditionalInformation.Text = Server.HtmlDecode(prodInformation);
            }

            if (string.IsNullOrEmpty(prodFeatureDesc))
            {
                HideProductFeatureTab();
            }
            else
            {
                ProductFeatureDesc.Text = Server.HtmlDecode(prodFeatureDesc);
            }
        }

        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get product id from querystring  
            if (Request.Params["mode"] != null)
            {
                this.Mode = Request.Params["mode"];
            }
            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this._product = (ZNodeProduct)HttpContext.Current.Items["Product"];
            this.Bind();
        }

        #endregion

        #region Zeon Custom Code

        /// <summary>
        /// Method for hiding Product Information panel and its header
        /// </summary>
        private void HideProductInfoTab()
        {
            liInformationHeader.Visible = false;
            h3InformationHeader.Visible = false;
            tabsAdditionalInformations.Visible = false;
        }

        /// <summary>
        /// Method for hiding Product Details panel and its header
        /// </summary>
        private void HideProductFeatureTab()
        {
            liFeatureHeader.Visible = false;
            h3FeatureHeader.Visible = false;
            tabsFeatures.Visible = false;
        }

        /// <summary>
        /// Show the Product Specifications
        /// </summary>
        public void ShowSKUSpecification()
        {
            int skuID = 0;
            if (Request.Params["sku"] != null)
            {
                skuID = _product.SelectedSKU.SKUID;
            }

            StringBuilder sbSpecification = new StringBuilder();
            if (skuID > 0)
            {
                sbSpecification.Append(GetProductSpecification(_product.ProductID, skuID));
                dvSpecification.InnerHtml = string.Empty;
            }
            else
            {
                sbSpecification.Append(GetProductSpecification());
                dvSpecification.InnerHtml = string.Empty;
            }
            if (!string.IsNullOrEmpty(sbSpecification.ToString()))
            {
                dvSpecification.InnerHtml = Server.HtmlDecode(sbSpecification.ToString());
            }
            else
            {
                liProductSpecs.Visible = false;
                tabsProductSpecs.Visible = false;
                h3ProductSpecs.Visible = false;
            }
        }

        /// <summary>
        /// retrive product specification
        /// </summary>
        /// <param name="productID"></param>
        /// <param name="skuID"></param>
        /// <returns></returns>
        private string GetProductSpecification(int productID, int skuID)
        {
            DataSet dsSpecs = new DataSet();
            StringBuilder sbSpec = new StringBuilder();

            ProductSpecificationsHelper SpecHelper = new ProductSpecificationsHelper();
            try
            {
                dsSpecs = SpecHelper.GetProductSpecifications_ByProductIdSkuId(productID, skuID);
                if (dsSpecs != null && dsSpecs.Tables.Count > 0)
                {
                    if (dsSpecs.Tables[0].Rows.Count > 0)
                    {
                        sbSpec.Append(this.GetLocalResourceObject("SKUSpecificationHeaderHTML").ToString());
                        for (int i = 0; i < dsSpecs.Tables[0].Rows.Count; i++)
                        {
                            string specificationRow = string.Format(this.GetLocalResourceObject("SKUSpecificationRowHTML").ToString(), dsSpecs.Tables[0].Rows[i]["SpecificationName"], dsSpecs.Tables[0].Rows[i]["SpecificationValue"]);
                            sbSpec.Append(specificationRow);
                        }
                        sbSpec.Append(this.GetLocalResourceObject("SKUSpecificationFooterHTML").ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                sbError.Append("Error on ProductSpecification => GetProductSpecification => ");
                sbError.Append(ex.Message.ToString());
                sbError.Append(" ProductId =");
                sbError.Append(productID.ToString());
                sbError.Append(" => ");
                sbError.Append(" SkuId =");
                sbError.Append(skuID.ToString());
                sbError.Append(" => ");
            }
            return sbSpec.ToString();
        }

        /// <summary>
        /// Get Product Specification
        /// </summary>
        /// <returns>string</returns>
        private string GetProductSpecification()
        {
            StringBuilder sbSpec = new StringBuilder();
            ZNode.Libraries.ECommerce.Catalog.ZNodeProduct Product = (ZNode.Libraries.ECommerce.Catalog.ZNodeProduct)HttpContext.Current.Items["Product"];
            string upsSpecificationName = System.Configuration.ConfigurationManager.AppSettings["UpcSpecificationText"];
            string alternatePartNumberSpecificationText = System.Configuration.ConfigurationManager.AppSettings["AlternatePartNumberSpecificationText"];
            try 
            {

                if (Product != null && Product.ZeonProductSpecifications != null && Product.ZeonProductSpecifications.Count > 0)
                {
                    var specList = Product.ZeonProductSpecifications;
                    sbSpec.Append(this.GetLocalResourceObject("SKUSpecificationHeaderHTML").ToString());
                    foreach (ZNode.Libraries.DataAccess.Entities.ZeonProductSpecifications zeonProductSpecification in specList)
                    {
                        string specificationRow = string.Empty;
                        if (zeonProductSpecification.SpecificationName.Equals(upsSpecificationName, StringComparison.OrdinalIgnoreCase)
                            || zeonProductSpecification.SpecificationName.Equals(alternatePartNumberSpecificationText, StringComparison.OrdinalIgnoreCase))
                        {
                            string specificationValue = "<span class=\"more\">" + zeonProductSpecification.SpecificationValue + "</span>";
                            specificationRow = string.Format(this.GetLocalResourceObject("SKUSpecificationRowHTML").ToString(), zeonProductSpecification.SpecificationName, specificationValue);
                        }
                        else
                        {
                            specificationRow = string.Format(this.GetLocalResourceObject("SKUSpecificationRowHTML").ToString(), zeonProductSpecification.SpecificationName, zeonProductSpecification.SpecificationValue);
                        }
                        sbSpec.Append(specificationRow);
                    }
                    sbSpec.Append(this.GetLocalResourceObject("SKUSpecificationFooterHTML").ToString());

                }
            }
            catch (Exception ex)
            {
                ZNode.Libraries.Framework.Business.ZNodeLoggingBase.LogMessage("Error in GetProductSpecification>>" + ex.ToString());
                sbSpec.Append(string.Empty);
            }
            return sbSpec.ToString();
        }

        #endregion
    }
}