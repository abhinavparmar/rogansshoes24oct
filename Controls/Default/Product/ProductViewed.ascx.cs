using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the ProductViewed user control class.
    /// </summary>
    public partial class Controls_Default_Product_ProductViewed : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProductList _ProductList;
        private ZNodeProfile _ProductProfile = new ZNodeProfile();
        private string _Title = string.Empty;
        private string BuyImage = "~/themes/" + ZNodeCatalogManager.Theme + "/images/buynow.gif";
        private int _MaxItemsToDisplay = 3;
        private bool _ShowReviews = false;
        private bool _ShowDescription = false;
        private bool _ShowAddToCart = false;
        private bool _ShowPrice = false;
        private bool _ShowName = false;
        private bool _ShowImage = false;
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the product profile.
        /// </summary>
        public ZNodeProfile ProductProfile
        {
            get { return this._ProductProfile; }
            set { this._ProductProfile = value; }
        }

        /// <summary>
        /// Gets or sets the Product list
        /// </summary>
        public ZNodeProductList ProductList
        {
            get
            {
                return this._ProductList;
            }

            set
            {
                this._ProductList = value;
            }
        }

        /// <summary>
        /// Gets or sets the title for this control
        /// </summary>
        public string Title
        {
            get
            {
                return this._Title;
            }

            set
            {
                this._Title = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of items to be displayed
        /// </summary>
        public int MaxItemsToDisplay
        {
            get
            {
                return this._MaxItemsToDisplay;
            }

            set
            {
                this._MaxItemsToDisplay = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to Show Name
        /// </summary>
        public bool ShowName
        {
            get
            {
                return this._ShowName;
            }

            set
            {
                this._ShowName = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to Show Reviews
        /// </summary>
        public bool ShowReviews
        {
            get
            {
                return this._ShowReviews;
            }

            set
            {
                this._ShowReviews = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to Show Image
        /// </summary>
        public bool ShowImage
        {
            get
            {
                return this._ShowImage;
            }

            set
            {
                this._ShowImage = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to Show Description
        /// </summary>
        public bool ShowDescription
        {
            get
            {
                return this._ShowDescription;
            }

            set
            {
                this._ShowDescription = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to Show Price
        /// </summary>
        public bool ShowPrice
        {
            get
            {
                return this._ShowPrice;
            }

            set
            {
                this._ShowPrice = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to Show AddToCart
        /// </summary>
        public bool ShowAddToCart
        {
            get
            {
                return this._ShowAddToCart;
            }

            set
            {
                this._ShowAddToCart = value;
            }
        }
        #endregion

        #region Properties

        #endregion

        #region Bind Data
        /// <summary>
        /// Bind display based on a product list
        /// </summary>
        public void BindProducts()
        {
            if (this._ProductList.ZNodeProductCollection.Count == 0)
            {
                pnlRecentlyViewedProducts.Visible = false;
                return;
            }
            else
            {
                ProductService productService = new ProductService();
                StringBuilder productIds = new StringBuilder();
                string productId = string.Empty;
                foreach (ZNodeProductBase productBase in this._ProductList.ZNodeProductCollection)
                {
                    productId = productBase.ProductID.ToString();
                    productIds.Append(productIds.Length == 0 ? productId : ", " + productId);
                }

                ProductQuery filter = new ProductQuery();
                filter.AppendInQuery(ProductColumn.ProductID, productIds.ToString());

                TList<ZNode.Libraries.DataAccess.Entities.Product> recentlyViewedProductList = productService.Find(filter.GetParameters());

                ZNodeProductList activeProductBaseList = new ZNodeProductList();
                foreach (ZNodeProductBase productBase in this._ProductList.ZNodeProductCollection)
                {
                    foreach (ZNode.Libraries.DataAccess.Entities.Product currentItem in recentlyViewedProductList)
                    {
                        if (currentItem.ProductID == productBase.ProductID && currentItem.ActiveInd == true)
                        {
                            activeProductBaseList.ZNodeProductCollection.Add(productBase);
                        }
                    }
                }

                // Set the active product only in session list
                Session["ProductViewedList" + ZNodeCatalogManager.LocaleId.ToString()] = activeProductBaseList;

                if (Session["ProductViewedList" + ZNodeCatalogManager.LocaleId.ToString()] != null)
                {
                    this._ProductList = Session["ProductViewedList" + ZNodeCatalogManager.LocaleId.ToString()] as ZNodeProductList;
                }

                pnlRecentlyViewedProducts.Visible = true;
            }

            //Zeon Custom Code: Start
            //PagedDataSource pagedDataSource = new PagedDataSource();
            //pagedDataSource.AllowPaging = true;
            //pagedDataSource.PageSize = this._MaxItemsToDisplay;
            //pagedDataSource.DataSource = this._ProductList.ZNodeProductCollection.Sort("DisplayOrder", SortDirection.Descending);
            //Zeon Custom Code: End

            RecentlyViewedItems.DataSource = this._ProductList.ZNodeProductCollection.Sort("DisplayOrder", SortDirection.Descending);
            RecentlyViewedItems.DataBind();
        }
        #endregion

        #region Helper Functions

        /// <summary>
        ///  Check for Call for Pricing value
        /// </summary>
        /// <param name="fieldValue">The Field Value</param>
        /// <returns>Returns the Hahstable Collection</returns>
        public string CheckForCallForPricing(object fieldValue, object callMessage)
        {
            MessageConfigAdmin mconfig = new MessageConfigAdmin();
            bool Status = bool.Parse(fieldValue.ToString());

            if (Status && callMessage != null && !string.IsNullOrEmpty(callMessage.ToString()))
            {
                return callMessage.ToString();
            }

            if (Status)
            {
                return mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, Convert.ToInt32(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0)), 43);
            }
            else if (!this._ProductProfile.ShowPrice)
            {
                return mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, Convert.ToInt32(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0)), 43);
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Returns properties value for PlugIn Control
        /// </summary>
        /// <param name="reviewRating">Review Rating</param>
        /// <param name="totalReviews">Total Reviews</param>
        /// <param name="viewProductLink">View Product Link</param>
        /// <returns>Returns the Hastable Collection</returns>
        protected Hashtable GetParameterList(object reviewRating, object totalReviews, object viewProductLink)
        {
            // Create Instance for hashtable
            Hashtable collection = new Hashtable();

            collection.Add("ViewProductLink", viewProductLink);
            collection.Add("ReviewRating", reviewRating);
            collection.Add("TotalReviews", totalReviews);

            return collection;
        }

        /// <summary>
        /// Add To cart button is triggered
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Buy_Click(object sender, ImageClickEventArgs e)
        {
            string link = "~/product.aspx?zpid=";

            // Getting ProductID from the image button
            ImageButton but_buy = sender as ImageButton;
            int zpid = int.Parse(but_buy.CommandArgument);

            Response.Redirect(link + zpid + "&action=addtocart");
        }


        /// <summary>
        /// Registers the Script
        /// </summary>
        private void RegisterClientScriptClasses()
        {
            StringBuilder script = new StringBuilder();

            script.Append("var zeonProductViewed = new ZeonSlideProducts({");
            script.Append("'sliderID':'" + relRecentlyViewedProduct.ClientID + "',");
            script.Append("'MaxItemCount':'5',");
            script.Append("'MobPortraitItemCount':'3',");
            script.Append("'MobLandItemCount':'3',");
            script.Append("'TabletItemCount':'3',");
            script.Append("'MinimumItemsShown':'2',");
            script.Append("});zeonProductViewed.Init();");
            this.Page.ClientScript.RegisterStartupScript(GetType(), "zeonProductViewedSlider", script.ToString(), true);
        }

        #endregion

        #region Page Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ProductViewedList" + ZNodeCatalogManager.LocaleId.ToString()] != null)
            {
                this._ProductList = Session["ProductViewedList" + ZNodeCatalogManager.LocaleId.ToString()] as ZNodeProductList;

                this.BindProducts();
            }
        }
        /// <summary>
        /// Page Prerender event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            RegisterClientScriptClasses();
        }
        #endregion
    }
}