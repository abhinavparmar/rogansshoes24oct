﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ZProductComparison.ascx.cs" Inherits="WebApp.Controls.Default.Product.ZProductComparison" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/common/spacer.ascx" %>
<div class="ContentContainer">
    <asp:UpdatePanel ID="uplProductComparison" runat="server" UpdateMode="Conditional" ClientIDMode="Static">
        <ContentTemplate>
            <h1><asp:Literal ID="ltrPageTitle" runat="server" meta:resourceKey="ltrPageTitleResource"></asp:Literal></h1>
            <asp:Literal ID="ltrNoProductErrorMsg" runat="server" Visible="false" meta:resourceKey="ltrNoProductErrorMsgResource1"></asp:Literal>

            <asp:Literal ID="lblMessages" runat="server" ClientIDMode="Static"></asp:Literal>
            <asp:Literal ID="ltrerroMsg" runat="server" EnableViewState="false"></asp:Literal>
            <div class="ProductComparison">

                <div class="IconLink" runat="server" enableviewstate="false" id="iconDiv">

                    <div class="email-icon">
                        <asp:LinkButton ID="lbtnEmailFriend" runat="server" CssClass="Button" EnableViewState="false" meta:resourceKey="btnEmailProducts" ClientIDMode="Static" OnClientClick="return zeonCompareProduct.ProdCompPageLoadedEventHandler();"  alt="Email Friend" title="Email Friend"></asp:LinkButton>
                    </div>
                    <div class="Spacer"></div>
                    <div class="print-icon">
                        <a onclick="window.print();return false;" title="Print" style="cursor: pointer" class="Button" alt="Print">
                            <asp:Localize ID="Localize2" runat="server" meta:resourceKey="lblPrint" EnableViewState="false" ClientIDMode="Static" >Print</asp:Localize></a>
                    </div>
                    <div class="Spacer"></div>
                    <div class="hd-removeallbtn">
                        <asp:LinkButton ID="lbtnRemoveAll" runat="server" meta:resourceKey="lnkRemoveAll" OnClick="lbtnRemoveAll_Click" CssClass="Button" EnableViewState="false" ClientIDMode="Static" alt="Remove All"></asp:LinkButton>
                    </div>
                </div>
                <%--<uc1:Spacer ID="Spacer2" SpacerHeight="40" EnableViewState="false" SpacerWidth="2" runat="server" ClientIDMode="Static"></uc1:Spacer>--%>
                <div>
                </div>
                <%--<uc1:Spacer ID="Spacer1" SpacerHeight="40" EnableViewState="false" SpacerWidth="2" runat="server" ClientIDMode="Static"></uc1:Spacer>--%>
                <div id="CompareScroll" runat="server" class="CompareSection">
                    <asp:Literal ID="ltrProductComparison" runat="server" EnableViewState="false" ClientIDMode="Static"></asp:Literal>
                    <asp:Repeater ID="rptCompareProducts" runat="server" OnItemCommand="rptCompareProducts_ItemCommand" >
                        <ItemTemplate>
                            <asp:HiddenField ID="hdnProdID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "ProductID") %>' />
                            <div class="CompareProductWrapper">
                                <asp:LinkButton ID="lbtnRemove" runat="server" meta:resourceKey="lnkRemove" CommandName="RemoveProduct" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ProductID") %>' CssClass="Button Remove" alt="remove" title="remove"></asp:LinkButton>
                                <div class="ProductImage">
                                    <img id="imgProduct" border="0" src='<%# GetImageURL(DataBinder.Eval(Container.DataItem, "ImageFile")) %>'
                                        alt='<%# DataBinder.Eval(Container.DataItem, "ProductNum") %>' title='<%# DataBinder.Eval(Container.DataItem, "ProductNum") %>' runat="server">
                                </div>
                                <div class="rs-compare">
                                    <span class="rs-new" id="spnRSNew" runat="server" visible='<%#DataBinder.Eval(Container.DataItem, "NewProductInd")%>'><span class="rs-new-text">New</span></span>
                                    <%--<asp:Image ID="NewItemImage" runat="server" ImageUrl='<%#ResolveUrl(ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("new.png")) %>'
                                        meta:resourcekey="NewItemImageResource1" EnableViewState="false" Visible='<%#DataBinder.Eval(Container.DataItem, "NewProductInd")%>' ClientIDMode="Static" AlternateText="New" alt="New"/>--%>&nbsp;
                                    <asp:Image ID="FeaturedItemImage" runat="server" EnableViewState="false" ImageUrl='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("sale.png") %>'
                                        meta:resourcekey="FeaturedItemImageResource1" Visible='<%# DataBinder.Eval(Container.DataItem, "FeaturedInd") %>' ClientIDMode="Static" AlternateText="Featured" ToolTip="Featured"/>
                                </div>
                                <div class="ProductName">
                                    <a href='<%# GetProductNavigationURL(DataBinder.Eval(Container.DataItem, "SEOURL"), DataBinder.Eval(Container.DataItem, "ProductID")) %>'><%#DataBinder.Eval(Container.DataItem, "Name") %> </a>
                                </div>
                                <div class="SKU">
                                    <asp:Literal ID="ltrSKU" runat="server" meta:resourceKey="ltrSKUResource1" ></asp:Literal>
                                    <asp:Label ID="lblSKU" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ProductNum") %>'></asp:Label>
                                </div>
                                <div class="BrandName" runat="server" visible='<%# DataBinder.Eval(Container.DataItem, "Custom3")!= null  && !DataBinder.Eval(Container.DataItem, "Custom3").ToString().Equals("0") %>'>
                                    <asp:Literal ID="ltrBrand" runat="server" meta:resourceKey="ltrBrandResource1" ClientIDMode="Static"></asp:Literal>
                                    <asp:Label ID="lblBrandName" runat="server"
                                        Text='<%# DataBinder.Eval(Container.DataItem, "Custom3") %>' ></asp:Label>
                                </div>
                                <div class="ProductDescription">
                                    <asp:Label ID="lblProductDescription" runat="server"
                                        Text='<%#DataBinder.Eval(Container.DataItem, "ShortDescription") %>' ></asp:Label>
                                </div>
                               <%-- <div class="Availability"><%# DataBinder.Eval(Container.DataItem, "Custom1") %></div>--%>
                                <div class="Price">
                                    <asp:Label ID="Label2" runat="server" meta:resourcekey="Label2Resource1"
                                        Text='<%# DataBinder.Eval(Container.DataItem, "FormattedPrice") %>' Visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && ProductListProfile.ShowPrice %>'></asp:Label>
                                </div>
                                <div class="CallForPrice">
                                    <asp:Label ID="uxCallForPricing" runat="server" meta:resourcekey="uxCallForPricingResource1"
                                        Text='<%# CheckForCallForPricing(DataBinder.Eval(Container.DataItem, "CallForPricing"), DataBinder.Eval(Container.DataItem, "CallMessage")) %>'></asp:Label>
                                </div>
                                <div class="Specification">
                                    <asp:Label ID="lblSpecs" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Custom2") %>' ClientIDMode="Static"></asp:Label>
                                </div>
                                <div class="Specification ">
                                    <asp:Literal ID="ltrTierPrice" runat="server" Text='<%# GetTierPriceCollectionTable((ZNode.Libraries.ECommerce.Catalog.ZNodeProductBase)Container.DataItem)%>'></asp:Literal>
                                </div>
                                <div class="Navigation">
                                    <a href='<%# GetProductNavigationURL(DataBinder.Eval(Container.DataItem, "SEOURL"), DataBinder.Eval(Container.DataItem, "ProductID")) %>' class="Button" alt="view details">
                                        <asp:Localize ID="Localize2" runat="server" meta:resourceKey="lblViewDetails"></asp:Localize></a>
                                </div>
                            </div>
                            <div class="Spacer"></div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <asp:HiddenField ID="hdnEmailFrom" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="hdnEmailTo" runat="server" ClientIDMode="Static" />
            <div class="EmailFriendSection" style="display: none;" id="divCompareEmailToFriend">
                <asp:Panel ID="pnlEmailToFriend" runat="server" DefaultButton="btnEmailProducts" ClientIDMode="Static">
                    <ul>
                        <li>
                            <div>
                                <asp:Label ID="lblErrors" runat="server" CssClass="Error" ClientIDMode="Static"></asp:Label>
                            </div>
                        </li>
                        <li>
                            <div>
                                <asp:Label ID="lblEmailFrom" runat="server" meta:resourceKey="lblEmailFromResource1" ClientIDMode="Static"></asp:Label>
                                <asp:TextBox ID="txtEmailFrom"  ToolTip="Email From" runat="server" ValidationGroup="ProductCompareEmail" ClientIDMode="Static"></asp:TextBox>&nbsp;
                            </div>
                        </li>
                        <li>
                            <div>
                                <asp:Label ID="lblEmailTo" runat="server" meta:resourceKey="lblEmailToResource1" ClientIDMode="Static"></asp:Label>
                                <asp:TextBox ID="txtEmailTo" ToolTip="Email To" runat="server" ValidationGroup="ProductCompareEmail" ClientIDMode="Static"></asp:TextBox>&nbsp;
                            </div>
                        </li>
                        <li>
                            <div class="Buttons">
                                <asp:LinkButton ID="btnEmailProducts" runat="server" CssClass="Button" meta:resourceKey="btnEmailProducts" ValidationGroup="ProductCompareEmail" ClientIDMode="Static" OnClientClick="return zeonCompareProduct.SendMailToFriend();" OnClick="btnEmailProducts_Click">
                                </asp:LinkButton>
                            </div>
                        </li>
                    </ul>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
