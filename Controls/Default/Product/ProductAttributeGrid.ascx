<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Product_ProductAttributeGrid" Codebehind="ProductAttributeGrid.ascx.cs" %>

<div class="AttributeGrid" id="AttributeGrid" runat="server">
    <b><asp:Label id="lbltext" runat="server" visible="false"></asp:label></b><br />
    <asp:Literal ID="ui_Grid" Text="&nbsp" runat="server" ></asp:Literal>
    <asp:PlaceHolder ID="ControlPlaceHolder" runat="server" />
</div>
