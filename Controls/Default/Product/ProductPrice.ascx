<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="WebApp.Controls_Default_Product_ProductTitle" CodeBehind="ProductPrice.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="Znode" %>

<%-- <asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtPrice"></asp:Localize>:--%>
<asp:Label ID="Price" runat="server" EnableViewState="false" meta:resourcekey="PriceResource1"></asp:Label>
<asp:Label ID="TeamPrice" runat="server" EnableViewState="true" Visible="false"></asp:Label>
<div id="divTeamPriceDescripion"  style="display:none">
   <Znode:CustomMessage ID="ucTeamDescription" runat="server"  MessageKey="TeamPriceDetailMessage" EnableViewState="false"/>
</div>