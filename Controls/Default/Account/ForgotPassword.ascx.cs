using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the Forgot Password user control class.
    /// </summary>
    public partial class Controls_Default_Account_ForgotPassword : System.Web.UI.UserControl
    {
        #region Helper Methods
        /// <summary>
        /// Represents the Verifyuser method 
        /// </summary>
        protected void VerifyUser()
        {
            string loginName = (PasswordRecoveryWizard.WizardSteps[0].Controls[0].FindControl("UserName") as TextBox).Text.Trim();
            string emailID = loginName;
            loginName = loginName + "_" + ZNodeConfigManager.SiteConfig.PortalID.ToString();//Zeon Custom Code
            //string emailID = (PasswordRecoveryWizard.WizardSteps[0].Controls[0].FindControl("Email") as TextBox).Text.Trim();

            MembershipUser user = Membership.GetUser(loginName);

            if (user == null)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.PasswordResetFailed, loginName, Request.UserHostAddress.ToString(), null, "Invalid user name", null);

                (PasswordRecoveryWizard.WizardSteps[0].Controls[0].FindControl("FailureText") as Literal).Text = GetLocalResourceObject("InvalidUsername").ToString();
                PasswordRecoveryWizard.WizardSteps[0].Controls[0].FindControl("errorMsg").Visible = true;
                PasswordRecoveryWizard.ActiveStepIndex = 0;
                return;
            }
            else
            {
                ZNodeUserAccount userAcct = new ZNodeUserAccount();
                Account acc = userAcct.GetByUserID(new Guid(user.ProviderUserKey.ToString()));

                if (!user.IsApproved || user.IsLockedOut)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.PasswordResetFailed, loginName, Request.UserHostAddress.ToString(), null, "Account is Inactive", null);
                    (PasswordRecoveryWizard.WizardSteps[0].Controls[0].FindControl("FailureText") as Literal).Text = GetLocalResourceObject("InvalidUsername").ToString() + "<br>" + "Contact site administrator for further assistance.";
                    PasswordRecoveryWizard.WizardSteps[0].Controls[0].FindControl("errorMsg").Visible = true;
                    PasswordRecoveryWizard.MoveTo(PasswordRecoveryWizard.WizardSteps[0]);
                    return;
                }
                else
                {
                    PasswordRecoveryWizard.MoveTo(PasswordRecoveryWizard.WizardSteps[2]);
                }

                // Verify user email id
                //if (user.Email != emailID)
                //{
                //    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.PasswordResetFailed, loginName, Request.UserHostAddress.ToString(), null, "Invalid email", null);

                //    (PasswordRecoveryWizard.WizardSteps[0].Controls[0].FindControl("FailureText") as Literal).Text = GetLocalResourceObject("InvalidUsername").ToString();
                //    PasswordRecoveryWizard.MoveTo(PasswordRecoveryWizard.WizardSteps[0]);
                //    return;
                //}
                //Zeon Custom Code:Commented as part of removing security Question and answer:Starts
                //(PasswordRecoveryWizard.WizardSteps[1].Controls[0].FindControl("Question") as Literal).Text = user.PasswordQuestion;
                //PasswordRecoveryWizard.MoveTo(PasswordRecoveryWizard.WizardSteps[1]);
                //Zeon Custom Code:Commented as part of removing security Question and answer:Ends
            }
        }

        /// <summary>
        /// Send mail to user with 
        /// </summary>
        protected void SendMail()
        {
            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            string subject = ZNodeConfigManager.SiteConfig.StoreName;
            string loginName = (PasswordRecoveryWizard.WizardSteps[0].Controls[0].FindControl("UserName") as TextBox).Text.Trim();
            //Zeon Custom Code:Starts
            string mailLoginName = loginName;
            string portalLogo = ZCommonHelper.GetLogoHTML();
            loginName = loginName + "_" + ZNodeConfigManager.SiteConfig.PortalID.ToString();
            //Zeon Custom Code:Ends
            string passwordAnswer = (PasswordRecoveryWizard.WizardSteps[1].Controls[0].FindControl("Answer") as TextBox).Text.Trim();
            string CurrentCulture = string.Empty;
            string templatePath = string.Empty;
            string defaultTemplatePath = string.Empty;

            MembershipUser user = Membership.GetUser(loginName);
            string LoginPassword = string.Empty;

            try
            {
                // Resets a user's password to a new one,automatically generated password

                //Generating Alphnumeric Password Only 2015 June:Starts
                // LoginPassword = user.ResetPassword(passwordAnswer);//Old Code
                string resetPsd = user.ResetPassword(passwordAnswer);
                string newPwd = Guid.NewGuid().ToString().Substring(0, Membership.MinRequiredPasswordLength);
                LoginPassword = user.ChangePassword(resetPsd, newPwd) ? newPwd : resetPsd;
                //Generating Alphnumeric Password Only 2015 June:Ends

                // Log password for further debugging
                // ZNodeUserAccount.LogPassword((Guid)user.ProviderUserKey, LoginPassword);
                AccountHelper helperAccess = new AccountHelper();
                helperAccess.DeletePasswordLogByUserId(((Guid)user.ProviderUserKey).ToString());
            }
            catch (MembershipPasswordException ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.PasswordResetFailed, loginName, Request.UserHostAddress.ToString(), null, "Invalid secret question/answer", ex.Message);

                (PasswordRecoveryWizard.WizardSteps[1].Controls[0].FindControl("FailureText") as Literal).Text = GetLocalResourceObject("InvalidSecurityQuestion").ToString();
                PasswordRecoveryWizard.ActiveStepIndex = 1;
                PasswordRecoveryWizard.WizardSteps[0].Controls[0].FindControl("errorMsg").Visible = true;
                return;
            }

            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.PasswordResetSuccess, loginName, Request.UserHostAddress.ToString(), null, "Invalid secret question/answer", null);
            CurrentCulture = ZNodeCatalogManager.CultureInfo;
            defaultTemplatePath = Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ResetPassword_en.htm"));
            if (CurrentCulture != string.Empty)
            {
                templatePath = Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ResetPassword_" + CurrentCulture + ".htm");
            }
            else
            {
                templatePath = Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ResetPassword_en.htm"));
            }

            // Check the language specific file existence.
            if (!File.Exists(templatePath))
            {
                // Check the default template file existence.
                if (!File.Exists(defaultTemplatePath))
                {
                    (PasswordRecoveryWizard.WizardSteps[1].Controls[0].FindControl("FailureText") as Literal).Text = Resources.CommonCaption.GeneralError;
                    PasswordRecoveryWizard.WizardSteps[0].Controls[0].FindControl("errorMsg").Visible = true;
                    PasswordRecoveryWizard.ActiveStepIndex = 1;
                    return;
                }
                else
                {
                    // If language specific template not available then load default template.
                    templatePath = defaultTemplatePath;
                }
            }

            StreamReader rw = new StreamReader(templatePath);
            string messageText = rw.ReadToEnd();

            string spacerImage = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/Themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/Images/space.png";
            string domainPath = Request.Url.GetLeftPart(UriPartial.Authority);
            int portalId = ZNodeConfigManager.SiteConfig.PortalID;

            ZEmailHelper zEmailHelper = new ZEmailHelper();
            ZeonMessageConfig zeonMessageConfig = new ZeonMessageConfig();
            string copyRightText = zeonMessageConfig.GetMessageKey("FooterCopyrightText");

            string messageTextHeader = zEmailHelper.EmailHeaderContent(domainPath, portalId, spacerImage);
            string messageTextFooter = zEmailHelper.EmailFooterContent(domainPath, portalId, copyRightText, spacerImage);

            //To add header to the template
            Regex rxHeaderContent = new Regex("#HeaderContent#", RegexOptions.IgnoreCase);
            messageText = rxHeaderContent.Replace(messageText, messageTextHeader);

            //To add footer to the template
            Regex rxFooterContent = new Regex("#FooterContent#", RegexOptions.IgnoreCase);
            messageText = rxFooterContent.Replace(messageText, messageTextFooter);

            Regex rx1 = new Regex("#BillingFirstName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(messageText, mailLoginName);

            Regex rx2 = new Regex("#Password#", RegexOptions.IgnoreCase);
            messageText = rx2.Replace(messageText, LoginPassword);


            try
            {
                // Send mail
                //ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(user.Email, senderEmail, String.Empty, subject, messageText, true);
                string userMail = string.Empty;
                ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(mailLoginName, senderEmail, String.Empty, subject, messageText, true);
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.PasswordResetSuccess, loginName, Request.UserHostAddress.ToString(), null, ex.Message, null);
                (PasswordRecoveryWizard.WizardSteps[1].Controls[0].FindControl("FailureText") as Literal).Text = Resources.CommonCaption.GeneralError;
                PasswordRecoveryWizard.WizardSteps[1].Controls[0].FindControl("errorMsg").Visible = true;
                PasswordRecoveryWizard.ActiveStepIndex = 1;
            }
        }

        #endregion

        #region Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //Zeon Custom Code: Start
            if (this.PasswordRecoveryWizard.ActiveStep == this.step1)
            {
                Panel ctrlPanel = PasswordRecoveryWizard.WizardSteps[0].Controls[0].FindControl("UserNamePanel") as Panel;

                foreach (Control ctrl in ctrlPanel.Controls)
                {
                    if (ctrl.GetType().Equals(typeof(TextBox)))
                    {
                        if (ctrl.ID.ToLower().Equals("username"))
                        {
                            TextBox txtUsername = ctrl as TextBox;
                            if (txtUsername != null)
                            {
                                txtUsername.Focus();
                                break;
                            }
                        }
                    }
                }

            }
            //Zeon Custom Code: End
            if (this.Page != null)
            {
                ZNodeResourceManager resourceManager = new ZNodeResourceManager();
                //PRFT Custom Code : Start
                this.Page.Title = string.Format(resourceManager.GetLocalResourceObject(this.TemplateControl.AppRelativeVirtualPath, "txtPasswordTitle.Text"), "Rogan�s Shoes");
                this.Page.MetaDescription = string.Format(GetLocalResourceObject("PasswordDescription").ToString(), "Rogan�s Shoes");
                //PRFT Custom Code : End
            }
        }

        /// <summary>
        /// This event raised When PasswordRecovery Wizard step is chenged
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void PasswordRecoveryWizard_ActiveStepChanged(object sender, EventArgs e)
        {
            // Check active wizard step index
            if (PasswordRecoveryWizard.ActiveStepIndex == 1)
            {
                //Zeon Custom Code : Start
                TextBox txtAnswer = PasswordRecoveryWizard.WizardSteps[1].Controls[0].FindControl("Answer") as TextBox;
                txtAnswer.Focus();
                //Zeon Custom Code: End
                this.VerifyUser();
            }
            else if (PasswordRecoveryWizard.ActiveStepIndex == 2)
            {
                this.SendMail();
            }
        }

        #endregion
    }
}