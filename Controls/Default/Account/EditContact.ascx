﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditContact.ascx.cs" Inherits="WebApp.Controls_Default_Account_EditContact" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/common/spacer.ascx" %>
<div class="Form EditContact">
    <asp:Panel ID="pnlEditContact" runat="server" DefaultButton ="ibSubmit" CssClass="CommonLableNone">
    <h1 id="Title">
        <asp:Localize ID="ContactInformation" meta:resourceKey="ContactInformation" runat="server" /></h1>
    <br />
    <div class="Row Clear">
        <div class="FieldStyle LeftContent">
            <asp:Localize ID="Localize1" meta:resourceKey="EmailTitle" runat="server" /></div>
        <div class="LeftContent">
            <asp:TextBox ID="txtEmail" runat="server" meta:resourceKey="EmailAddress" EnableViewState="false"  ClientIDMode="Static"  alt="Email" placeholder="Subscription Email Address *"></asp:TextBox>
            <div>
            <asp:RequiredFieldValidator 
                ID="RequiredFieldValidator1" 
                 SetFocusOnError="true"
                runat="server" 
                Width="450px"
                ValidationGroup="EditContact"
                ControlToValidate="txtEmail"
                CssClass="Error"
                Display="Dynamic"
                ErrorMessage='<%$ Resources:CommonCaption, EmailAddressRequired%>'>
                </asp:RequiredFieldValidator>            
            <asp:RegularExpressionValidator 
                ID="revEmail"        
                SetFocusOnError="true"
                runat="server"
                Width="10px" 
                ControlToValidate="txtEmail"                
                ErrorMessage='<%$ Resources:CommonCaption, EmailValidErrorMessage %>'
                Display="Dynamic" 
                ValidationExpression='<%$ Resources:CommonCaption, EmailValidationExpression%>'
                ValidationGroup="EditContact"
                CssClass="Error">
           </asp:RegularExpressionValidator>
           </div>
        </div>
    </div>
    
    
    <div class="Row Clear">
        <div class="FieldStyle LeftContent NotMandatory">
        </div>
        <div class="LeftContent">
            <asp:CheckBox ID="chkEmailOptIn" meta:resourceKey="EmailOptIn" Checked="true" EnableViewState="false" 
                runat="server" ClientIDMode="Static"/>
        </div>
    </div>    
    <div class="Row Clear">
        <div class="FieldStyle LeftContent NotMandatory">
        </div>
        <div class="LeftContent Buttons" >
            <asp:LinkButton ID="ibSubmit" runat="server" CssClass="Button"   ToolTip="<%$ Resources:CommonCaption, Submit %>"
                ValidationGroup="EditContact" CausesValidation="true" Text="<%$ Resources:CommonCaption, Submit %>" 
                onclick="LbSubmit_Click"  ClientIDMode="Static"></asp:LinkButton>
                
            <asp:LinkButton ID="lbCancel" runat="server" CssClass="ClearButton"  ToolTip="<%$ Resources:CommonCaption, Cancel %>"
                ClientIDMode="Static"
                Text="<%$ Resources:CommonCaption, Cancel %>" onclick="LbCancel_Click"></asp:LinkButton>
        </div>
    </div>
    <div class="Clear">
        <uc1:Spacer ID="Spacer4" EnableViewState="false" SpacerHeight="10" SpacerWidth="10"
            runat="server" ClientIDMode="Static"></uc1:Spacer>
    </div></asp:Panel>
</div>
