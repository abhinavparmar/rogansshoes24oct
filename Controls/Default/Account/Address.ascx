<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Account_Address" CodeBehind="Address.ascx.cs" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="spacer" TagPrefix="uc1" %>
<asp:UpdatePanel runat="server" ID="upAddress">
    <ContentTemplate>
        <asp:Panel ID="pnlAddress" DefaultButton="btnUpdate" runat="server" ClientIDMode="Static" CssClass="CommonLableNone">
            <div class="Form Search EditAddress">
                <h1>
                    <asp:Label ID="lblAddressTitle" runat="server" EnableViewState="false" ClientIDMode="Static"></asp:Label></h1>
                <div>
                    <%--<uc1:spacer ID="Spacer1" EnableViewState="false" SpacerHeight="10" SpacerWidth="10"
                        runat="server" ClientIDMode="Static"></uc1:spacer>--%>
                    <asp:Label ID="lblError" CssClass="Error" runat="server" EnableViewState="false" ClientIDMode="Static"></asp:Label>
                </div>
                <div class="Row Clear">
                    <div class="LeftContent">
                        <!-- valign="top">-->
                        <div class="Form">
                            <div class="Row Clear">
                                <div class="FieldStyle LeftContent Req" style="text-align: left">
                                    <asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:CommonCaption, AddressName %>" EnableViewState="false" />
                                    <%-- <small>Enter a name for this address entry so that you can easily select it later :</small>--%>
                                </div>
                                <div class="LeftContent">
                                    <asp:TextBox ID="txtAddressName" placeholder="Address Name *" runat="server" Width="" Columns="30" MaxLength="100" ToolTip="<%$ Resources:CommonCaption, AddressName %>"  EnableViewState="false" ClientIDMode="Static" alt="AddressName"></asp:TextBox>
                                    <div>
                                        <asp:RequiredFieldValidator ID="rfvAdressName" ErrorMessage="<%$ Resources:CommonCaption, AddressNameRequired %>" SetFocusOnError="true"
                                            ControlToValidate="txtAddressName" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="Row Clear">
                                <div class="FieldStyle LeftContent Req" style="text-align: left">
                                    <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:CommonCaption, FirstName %>" EnableViewState="false" />
                                </div>
                                <div class="LeftContent">
                                    <asp:TextBox ID="txtBillingFirstName" placeholder="First Name *" runat="server" Width="" Columns="30" MaxLength="100" EnableViewState="false" ToolTip="<%$ Resources:CommonCaption, FirstName %>" ClientIDMode="Static" alt="FirstName"></asp:TextBox><div>
                                        <asp:RequiredFieldValidator ID="req1" ErrorMessage="<%$ Resources:CommonCaption, FirstNameRequired %>" SetFocusOnError="true"
                                            ControlToValidate="txtBillingFirstName" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="Row Clear">
                                <div class="FieldStyle LeftContent Req" style="text-align: left">
                                    <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:CommonCaption, LastName %>" EnableViewState="false" />
                                </div>
                                <div class="LeftContent">
                                    <asp:TextBox ID="txtBillingLastName" placeholder="Last Name *" runat="server" Width=" " Columns="30" MaxLength="100" ToolTip="<%$ Resources:CommonCaption, LastName %>"  EnableViewState="false" ClientIDMode="Static" alt="LastName"></asp:TextBox><div>
                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" ErrorMessage="<%$ Resources:CommonCaption, LastNameRequired %>"
                                            SetFocusOnError="true" ControlToValidate="txtBillingLastName" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="Row Clear">
                                <div class="FieldStyle LeftContent ReqNo" style="text-align: left">
                                    <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:CommonCaption, CompanyName %>" EnableViewState="false" />
                                </div>
                                <div class="LeftContent">
                                    <asp:TextBox ID="txtBillingCompanyName" placeholder="Company Name" runat="server" Width=" " Columns="30" MaxLength="100" ToolTip="<%$ Resources:CommonCaption, CompanyName %>"  EnableViewState="false" ClientIDMode="Static" alt="CompanyName"></asp:TextBox>
                                </div>
                            </div>

                            <div class="Row Clear">
                                <div class="FieldStyle LeftContent Req" style="text-align: left">
                                    <asp:Localize ID="Localize7" meta:resourceKey="txtBStreet1" runat="server" EnableViewState="false" />
                                </div>
                                <div class="LeftContent">
                                    <asp:TextBox ID="txtBillingStreet1" runat="server" placeholder="Billing Street1 *" Width=" " Columns="30" MaxLength="100" meta:resourceKey="BStreet1"  EnableViewState="false" ClientIDMode="Static" alt="Billing Street1"></asp:TextBox><div>
                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" ErrorMessage="<%$ Resources:CommonCaption, Street1Required %>" SetFocusOnError="true"
                                            ControlToValidate="txtBillingStreet1" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="Row Clear">
                                <div class="FieldStyle LeftContent ReqNo" style="text-align: left">
                                    <asp:Localize ID="Localize9" meta:resourceKey="txtBStreet2" runat="server" EnableViewState="false" />
                                </div>
                                <div class="LeftContent">
                                    <asp:TextBox ID="txtBillingStreet2" placeholder="Billing Street2" runat="server" Width=" " Columns="30" MaxLength="100" meta:resourceKey="BStreet2" EnableViewState="false"  ClientIDMode="Static" alt="Billing Street2"></asp:TextBox>
                                </div>
                            </div>
                            <div class="Row Clear">
                                <div class="FieldStyle LeftContent Req" style="text-align: left">
                                    <asp:Localize ID="txLiveChat" runat="server" Text="<%$ Resources:CommonCaption, City %>" EnableViewState="false" />
                                </div>
                                <div class="LeftContent">
                                    <asp:TextBox ID="txtBillingCity" placeholder="Billing City *" runat="server" Width=" " Columns="30" MaxLength="100"  EnableViewState="false" ToolTip="<%$ Resources:CommonCaption, City %>" ClientIDMode="Static" alt="Billing City"></asp:TextBox><div>
                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" ErrorMessage="<%$ Resources:CommonCaption, CityRequired %>" SetFocusOnError="true"
                                            ControlToValidate="txtBillingCity" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="Row Clear">
                                <div class="FieldStyle LeftContent Req" style="text-align: left">
                                    <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:CommonCaption, Country %>" EnableViewState="false" />
                                </div>
                                <div class="LeftContent">
                                    <asp:DropDownList ID="lstBillingCountryCode" Width=" " runat="server"  ToolTip="<%$ Resources:CommonCaption, Country %>" OnSelectedIndexChanged="lstBillingCountryCode_OnSelectedIndexChanged" AutoPostBack="true" ClientIDMode="Static" alt="BillingCountry">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator13" ErrorMessage="<%$ Resources:CommonCaption, CountryCodeRequired %>" SetFocusOnError="true"
                                        ControlToValidate="lstBillingCountryCode" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="Row Clear">
                                <div class="FieldStyle LeftContent Req" style="text-align: left">
                                    <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:CommonCaption, State %>" EnableViewState="false" />
                                </div>
                                <div class="LeftContent">
                                    <asp:TextBox ID="txtBillingState" runat="server" Width=" " Columns="10" EnableTheming="false"  ToolTip="<%$ Resources:CommonCaption, State %>" ClientIDMode="Static"></asp:TextBox><div>
                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" ErrorMessage="<%$ Resources:CommonCaption, StateRequired %>" SetFocusOnError="true"
                                            ControlToValidate="txtBillingState" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                    </div>
                                    <asp:DropDownList ID="lstBillingStateCode" runat="server" Visible="false" Width=" "  ClientIDMode="Static"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="reqBillingStateCode" runat="server" SetFocusOnError="true" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, SelectStateRequired %>" ControlToValidate="lstBillingStateCode" InitialValue="<%$ Resources:CommonCaption, DefaultSelectedState %>" CssClass="Error"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="Row Clear">
                                <div class="FieldStyle LeftContent Req" style="text-align: left">
                                    <asp:Localize ID="Localize12" runat="server" Text="<%$ Resources:CommonCaption, PostalCode %>" EnableViewState="false" />
                                </div>
                                <div class="LeftContent">
                                    <asp:TextBox ID="txtBillingPostalCode" placeholder="Postal Code *" runat="server" Width=" " Columns="30" MaxLength="10" ToolTip="<%$ Resources:CommonCaption, PostalCode %>"  EnableViewState="false" ClientIDMode="Static" alt="PostalCode"></asp:TextBox><div>
                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator12" ErrorMessage="<%$ Resources:CommonCaption, PostalCodeRequired %>" SetFocusOnError="true"
                                            ControlToValidate="txtBillingPostalCode" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="rgxBillingPostalCodeValidation" runat="server" ControlToValidate="txtBillingPostalCode" Display="Dynamic"
                                            CssClass="Error" SetFocusOnError="true" ErrorMessage="Please enter valid zip code" ValidationExpression="^\d{5}(-\d{4})?$"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                            </div>
                            <%--  Zeon Custom Code :PO Box Implementation--%>

                            <div class="Clear">
                                <div class="FieldStyle LeftContent NotMandatory" style="text-align: left">
                                </div>
                                <div class="LeftContent checkbox-container" style="width: auto!important; padding-bottom: 7px;">
                                    <asp:CheckBox ID="chkBillingPOBox" placeholder="Static" AutoPostBack="false" runat="server" meta:resourceKey="chkBillingPOBox" ClientIDMode="Static" />
                                </div>
                                <asp:HiddenField ID="hdnBillingExtnID" runat="server" />
                            </div>
                            <%--  Zeon Custom Code :PO Box Implementation--%>

                            <div class="Row Clear">
                                <div class="FieldStyle LeftContent Req" style="text-align: left">
                                    <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:CommonCaption, PhoneNumber %>" EnableViewState="false" />
                                </div>
                                <div class="LeftContent">
                                    <asp:TextBox ID="txtBillingPhoneNumber" placeholder="Phone Number *" runat="server" Width=" " Columns="30"  ToolTip="<%$ Resources:CommonCaption, PhoneNumber %>" EnableViewState="false" MaxLength="100" ClientIDMode="Static" alt="PhoneNumber"></asp:TextBox><div>
                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" ErrorMessage="<%$ Resources:CommonCaption, PhoneNoRequired %>" SetFocusOnError="true"
                                            ControlToValidate="txtBillingPhoneNumber" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="Clear">
                                <div class="FieldStyle LeftContent NotMandatory">
                                </div>
                                <div class="LeftContent checkbox-container">
                                    <asp:CheckBox ID="chkUseAsDefaultBillingAddress" placeholder="Billing Address" Width="300px" TextAlign="Right" EnableViewState="false" 
                                        runat="server" meta:resourceKey="txtUseAsDefaultBillingAddress" Checked="false" ClientIDMode="Static" />
                                </div>
                            </div>
                            <div class="Clear">
                                <div class="FieldStyle LeftContent NotMandatory">
                                </div>
                                <div class="LeftContent checkbox-container">
                                    <asp:CheckBox ID="chkUseAsDefaultShippingAddress" placeholder="Shipping Address" Width="300px" TextAlign="Right"  EnableViewState="false"
                                        runat="server" meta:resourceKey="txtUseAsDefaultShippingAddress" Checked="true" ClientIDMode="Static" />
                                </div>
                            </div>
                        </div>
                        <div class="Row Clear">
                            <%--<div class="FieldStyle LeftContent">
                            </div>--%>
                            <div class="Buttons">
                                <asp:LinkButton ID="btnUpdate" runat="server" Text="<%$ Resources:CommonCaption, Submit %>" ToolTip="<%$ Resources:CommonCaption, Submit %>"
                                    OnClick="BtnUpdate_Click" CssClass="Button" ClientIDMode="Static" />
                                <asp:LinkButton ID="btnCancel" runat="server" OnClick="BtnCancel_Click" CssClass="ClearButton"  ToolTip="<%$ Resources:CommonCaption, Cancel %>"
                                    CausesValidation="false" Text="<%$ Resources:CommonCaption, Cancel %>" ClientIDMode="Static" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="LeftContent" style="width: 10%">
                    &nbsp;
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
