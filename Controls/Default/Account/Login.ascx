<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Account_Login" CodeBehind="Login.ascx.cs" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="Spacer" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/navigation/NavigationBreadCrumbs.ascx" TagName="StoreBreadCrumbs" TagPrefix="ZNode" %>
<%--<%@ Register Src="~/Controls/Default/common/HomeQuickSearch.ascx" TagName="QuickSearch" TagPrefix="ZNode" %>--%>
<%@ Register Src="Register.ascx" TagName="Register" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="ZNode" %>
<div class="Loginpage">
    <div class="QuickSearchfield">
        <%-- <ZNode:QuickSearch ID="uxQuickSearch" runat="server"></ZNode:QuickSearch>--%>
    </div>
</div>
<div class="Form MemberLogin">
    <div class="PageTitle">
        <asp:Localize ID="Localize8" meta:resourceKey="txtSignIn" runat="server" /></div>
    <div class="AccessDenied">
        <asp:Label ID="lblaccess" runat="server" Visible="False" meta:resourceKey="lblaccess" CssClass="Error" ClientIDMode="Static"></asp:Label></div>
    <div class="Row Clear">
        <div class="AlreadyUser">
            <asp:Login ID="uxLogin" runat="server" MembershipProvider="ZNodeMembershipProvider"
                PasswordRecoveryText="Forgot Password?" PasswordRecoveryUrl="~/forgotpassword.aspx" 
                OnAuthenticate="UxLogin_Authenticate" DisplayRememberMe="False" FailureText="<%$ Resources:CommonCaption, LoginFailed%>"
                Width="100%" OnLoggedIn="UxLogin_LoggedIn" ClientIDMode="Static">
                <LayoutTemplate>
                    <asp:Panel ID="CheckoutPanel" DefaultButton="ibLogin" runat="server" ClientIDMode="Static">
                        <div class="FormTitle">
                            <asp:Localize ID="Localize8" meta:resourceKey="txtUser" runat="server"  EnableViewState="false"/>
                        </div>
                        <div class="Form">
                            <div class="Row Clear"></div>
                            <div class="Error" id="errorMsg" runat="server" enableviewstate="false" visible="false"><span class="uxMsg ErrorSection" ><em>
                                <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                         </em> </span>  </div>
                            <div class="Row Clear">
                                <div class="FieldStyle">
                                    <asp:Label ID="UserNameLabel" CssClass="Req" runat="server" AssociatedControlID="UserName" EnableViewState="false"  ClientIDMode="Static">
                                        <asp:Localize ID="Localize1" meta:resourceKey="txtUserName" runat="server" EnableViewState="false"/> :</asp:Label>
                                </div>
                                <div class="Input-box">
                                    <asp:TextBox ID="UserName" runat="server" autocomplete="off" meta:resourceKey="UserName" EnableViewState="false" CssClass="TextField"   ClientIDMode="Static" placeholder="Email *" alt="User Name"></asp:TextBox><div>
                                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" SetFocusOnError="true"
                                            ValidationGroup="uxLogin" meta:resourceKey="UserNameRequired" Display="Dynamic"
                                            CssClass="Error"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="UserName" SetFocusOnError="true"
                                            ValidationGroup="uxLogin" CssClass="Error" ErrorMessage="<%$ Resources:CommonCaption, ValidEmailRequired %>" ToolTip="<%$ Resources:CommonCaption, ValidEmailRequired %>"
                                            ValidationExpression='<%$ Resources:CommonCaption, EmailValidationExpression %>'
                                            Display="Dynamic"></asp:RegularExpressionValidator>
                                    </div>
                                </div> 
                            </div>
                            <div class="Row Clear">
                                <div class="FieldStyle">
                                    <asp:Label ID="PasswordLabel" CssClass="Req" runat="server" AssociatedControlID="Password" EnableViewState="false"  ClientIDMode="Static">
                                        <asp:Localize ID="Localize4" meta:resourceKey="txtPassword" runat="server" EnableViewState="false"/> :</asp:Label>
                                </div>
                                <div class="Input-box">
                                    <asp:TextBox ID="Password" runat="server" meta:resourceKey="Password" autocomplete="off" CssClass="TextField" EnableViewState="false" TextMode="Password" placeholder="Password *"  ClientIDMode="Static"  alt="Password"></asp:TextBox><div>
                                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" SetFocusOnError="true"
                                            CssClass="Error" Display="Dynamic" meta:resourceKey="PasswordRequired" ValidationGroup="uxLogin"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                              <div class="Row Clear">
                                      <div class="FieldStyle TextNone">&nbsp;</div>
                                     <div class="Input-box CheckBox">
                                    <div class="left-section"><asp:CheckBox ID="chkRememberMe" runat="server" Text="Remember me on this computer" 
                                    TextAlign="Right"  ClientIDMode="Static" />
                                        </div>
                                         <div class="NewButton">
                                    <asp:LinkButton ID="ibLogin" runat="server" ValidationGroup="uxLogin" CommandName="Login" meta:resourceKey="Login" EnableViewState="false" a Font-Overline="False" Font-Underline="False" ClientIDMode="Static">
                                        <asp:Localize ID="Localize2" meta:resourceKey="txtLogin" runat="server" EnableViewState="false" />
                                    </asp:LinkButton>
                                </div>

                                    </div>
                              </div> 
                              <%-- <div class="Row Clear">
                                <div class="FieldStyle TextNone">
                                    &nbsp;
                                </div>
                                
                            </div>--%>
                            <div class="Row Clear">
                               <div class="FieldStyle TextNone">&nbsp;</div>
                                <div class="ForgetLink Input-box">
                                    <asp:CheckBox ID="RememberMe" runat="server" Text="Remember me next time." meta:resourceKey="txtRemember"  EnableViewState="false"  ClientIDMode="Static"/>
                                    <div>
                                        <asp:LinkButton ID="ForgotPassword" runat="Server" meta:resourceKey="txtForgot" CssClass="TextField"  EnableViewState="false"
                                            OnClick="ForgotPassword_Click"  ClientIDMode="Static"> SSS</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                         
                            <div class="Row Clear img-clear">
                                <ZNode:Spacer ID="Spacer2" EnableViewState="false" SpacerHeight="10" SpacerWidth="3" runat="server" ClientIDMode="Static"></ZNode:Spacer>
                            </div>
                        </div>
                        </div>
                    </asp:Panel>
                </LayoutTemplate>
            </asp:Login>
        </div>
        <div class="NewUser">
            <div class="FormTitle">
                <asp:Localize ID="Localize5" meta:resourceKey="txtNewUser" runat="server"  EnableViewState="false"/></div>
            <div class="Form">
                <div class="CreateAccount">
                    <asp:LinkButton ID="uxCreate" EnableViewState="false" runat="server"  
                        CssClass="Button" meta:resourcekey="txtCreateAcc" OnClick="uxCreate_Click"  ClientIDMode="Static"></asp:LinkButton> 
                </div>
                <div class="RightTextalign">
                    <ZNode:Spacer ID="Spacer8" EnableViewState="false" SpacerHeight="2" SpacerWidth="3" runat="server"></ZNode:Spacer>
                    <asp:Localize ID="Localize6" meta:resourceKey="txtAccountInfo" runat="server" EnableViewState="false" />
                </div>
                <asp:Panel Visible="false" ID="uxExpressCheckout" runat="server" CssClass="guest-button-section">
                    <div><span>OR</span></div>
                    <div class="AddToCartButton">
                        <asp:LinkButton ID="uxCheckout" EnableViewState="false" runat="server" 
                            CssClass="Button" meta:resourcekey="txtCheckout" OnClick="uxCheckout_Click"></asp:LinkButton>
                    </div>
                    <div class="RightTextalign">
                        <ZNode:Spacer ID="Spacer3" EnableViewState="false" SpacerHeight="2" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        <asp:Localize ID="Localize9" meta:resourceKey="txtCheck" runat="server" />
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</div>
