﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PUnsubscribe.ascx.cs" Inherits="WebApp.Controls.Default.Account.PUnsubscribe" %>
<div class="Form Unsubscribe">
    <div class="PageTitle">
        <asp:Localize ID="locPageTitle" meta:resourceKey="txtSignIn" runat="server" />
    </div>
    <asp:Localize ID="locMessage" runat="server" EnableViewState="false" meta:resourceKey="locMessage"></asp:Localize>
    <div class="SuccessMsg" runat="server" visible="false" id="divStatusMessage">
        <div class="uxMsg SuccessSection">
            <em>
                <span id="lblMessage">
                    <asp:Localize ID="locStatusMessage" runat="server"></asp:Localize></span>
            </em>
        </div>
    </div>
    <div class="Row Clear">
        <div id="divChangeContact" class="AlreadyUser" runat="server">
            <div class="FormTitle">
                <asp:Localize EnableViewState="false" runat="server" ID="locManageSubMsg" meta:resourceKey="locManageSubMsg"></asp:Localize></div>
            <div class="Row Clear">
                <div class="FieldStyle">
                    <asp:Label ID="lblUserNameTitle" runat="server" EnableViewState="false" meta:resourceKey="lblUserNameTitle">
                    </asp:Label>
                </div>
                <div class="Input-box">
                    <asp:Label ID="lblUserName" runat="server" EnableViewState="false">
                    </asp:Label>
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle">
                    <asp:Label ID="lblNewEmailTitle" runat="server" EnableViewState="false" meta:resourceKey="lblNewEmailTitle"></asp:Label>
                </div>
                <div class="Input-box">
                    <asp:TextBox runat="server" ID="txtNewEmail" EnableViewState="false"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="emailRequired" runat="server" ControlToValidate="txtNewEmail" SetFocusOnError="true"
                        ValidationGroup="UpdateEmailInfo" meta:resourceKey="emailRequired" Display="Dynamic"
                        CssClass="Error"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtNewEmail" SetFocusOnError="true"
                        ValidationGroup="UpdateEmailInfo" CssClass="Error" ErrorMessage="<%$ Resources:CommonCaption, ValidEmailRequired %>" ToolTip="<%$ Resources:CommonCaption, ValidEmailRequired %>"
                        ValidationExpression='<%$ Resources:CommonCaption, EmailValidationExpression %>'
                        Display="Dynamic" EnableViewState="false"></asp:RegularExpressionValidator>
                </div>
                <div class="Row Clear">
                    <div class="FieldStyle TextNone">&nbsp;</div>
                    <div class="NewButton">
                        <asp:LinkButton ID="ibUpdateInfo" runat="server" meta:resourceKey="Unsubscribe" EnableViewState="false"  Font-Overline="False" Font-Underline="False" ClientIDMode="Static" OnClick="ibUpdateInfo_Click" ValidationGroup="UpdateEmailInfo">
                            <asp:Localize ID="locUpdateInfo" meta:resourceKey="ibUpdateInfo" runat="server" EnableViewState="false" />
                        </asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <div id="divUnsubscribe" class="NewUser" runat="server">
            <div class="FormTitle">
                <asp:Localize EnableViewState="false" runat="server" ID="locCurrentSetting" meta:resourceKey="locCurrentSetting"></asp:Localize></div>
            <div class="Row Clear">
                <div class="FieldStyle TextNone">
                    <asp:Localize ID="locUnsubscribeLabel" runat="server" meta:resourceKey="locUnsubscribeLabel"></asp:Localize>
                </div>
                <div class="NewButton">
                    <asp:LinkButton ID="ibUnSubscribe" runat="server" meta:resourceKey="Unsubscribe" EnableViewState="false" Font-Overline="False" Font-Underline="False" ClientIDMode="Static" OnClick="ibUnSubscribe_Click" CssClass="NewButton">
                        <asp:Localize ID="locUnSubscribe" meta:resourceKey="ibUnSubscribe" runat="server" EnableViewState="false" />
                    </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</div>
