using System;
using System.Data;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the Account user control class.
    /// </summary>
    public partial class Controls_Default_Account_Account : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeUserAccount userAccount;
        private ZNodeProfile _AccountProfile = new ZNodeProfile();
        private int productId = 0;
        private string billingAddress = string.Empty;
        private string shippingAddress = string.Empty;
        private string referralCommision = string.Empty;
        private string taxId = string.Empty;
        private string referralCommisionType = string.Empty;
        private int accountId = 0;
        private string buyImage = "~/themes/" + ZNodeCatalogManager.Theme + "/images/buynow.gif";
        //Zeon Custom Code
        private int addressPageSize = 10, orderPageSize = 10, wishlistpageSize = 10;
        #endregion

        /// <summary>
        /// Gets or sets the account profile
        /// </summary>
        public ZNodeProfile AccountProfile
        {
            get { return this._AccountProfile; }
            set { this._AccountProfile = value; }
        }

        /// <summary>
        /// Gets or sets the referral commission.
        /// </summary>
        public string ReferralCommision
        {
            get { return referralCommision; }
            set { referralCommision = value; }
        }

        /// <summary>
        /// Gets or sets the Tax Id
        /// </summary>
        public string TaxId
        {
            get { return taxId; }
            set { taxId = value; }
        }

        /// <summary>
        /// Gets or sets the referral commission type.
        /// </summary>
        public string ReferralCommisionType
        {
            get { return referralCommisionType; }
            set { referralCommisionType = value; }
        }

        #region Public methods

        /// <summary>
        /// Method Checks Call For Pricing message
        /// </summary>
        /// <param name="fieldValue">Field Value</param>
        /// <returns>Returns Call For Pricing message</returns>
        public string CheckForCallForPricing(object fieldValue, object callMessage)
        {
            MessageConfigAdmin mconfig = new MessageConfigAdmin();
            bool status = bool.Parse(fieldValue.ToString());

            if (status || !this.AccountProfile.ShowPrice)
            {
                if (callMessage != null && !string.IsNullOrEmpty(callMessage.ToString()))
                {
                    return callMessage.ToString();
                }
                return mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, Convert.ToInt32(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0)), 43);
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Get Gift Card Number 
        /// </summary>
        /// <param name="giftCardId">Gift Card ID</param>
        /// <returns>Returns gift card number.</returns>
        public string GetGiftCardNumber(object giftCardId)
        {
            int id = Convert.ToInt32(giftCardId.ToString());
            if (id > 0)
            {
                GiftCardService giftCardService = new GiftCardService();
                return giftCardService.GetByGiftCardId(id).CardNumber;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Bind the user account list.
        /// </summary>
        /// <param name="addressId">Address ID</param>
        /// <returns>Returns the Address Text</returns>
        public string GetAddress(string addressId)
        {
            string addressText = string.Empty;

            AddressService addressService = new AddressService();
            Address address = addressService.GetByAddressID(Convert.ToInt32(addressId));
            if (address != null)
            {
                addressText = ZCommonHelper.GetAddressString(address);//Zeon Custom Code
            }

            return addressText;
        }
        #endregion

        #region Protected Methods and Events


        /// <summary>
        /// Bind field data
        /// </summary>
        protected void BindOrders()
        {
            // Current Portal 
            int? portalId = ZNodeConfigManager.SiteConfig.PortalID;
            DataSet ds;
            if (ViewState["OrderList"] == null)
            {
                ZNode.Libraries.Admin.OrderAdmin orderAdmin = new ZNode.Libraries.Admin.OrderAdmin();
                ds = orderAdmin.FindOrders(null, null, null, null, this.userAccount.AccountID.ToString(), null, null, null, null, null);
                ViewState["OrderList"] = ds;
            }
            else
            {
                ds = (DataSet)ViewState["OrderList"];
            }
            if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
            {
                //Paging Implementation
                PagedDataSource objPage = new PagedDataSource();
                objPage.DataSource = ds.Tables[0].DefaultView;
                objPage.AllowPaging = true;
                objPage.PageSize = orderPageSize;
                if (objPage.PageCount > 1)
                {
                    objPage.CurrentPageIndex = !string.IsNullOrEmpty(hdnOrderCurrentPage.Value) && int.Parse(hdnOrderCurrentPage.Value) <= objPage.PageCount ? int.Parse(hdnOrderCurrentPage.Value) - 1 : 0;
                    CreatePagingControl(dlOrderPager, objPage.PageCount, "Order", objPage.CurrentPageIndex + 1);
                    divOrderPager.Visible = true;
                }
                else
                {
                    divOrderPager.Visible = false;
                }
                dlOrder.DataSource = objPage;
                dlOrder.DataBind();
                dlOrder.Visible = true;
            }
            else
            {
                ltrEmptyOrderMessage.Text = string.Empty;
                ltrEmptyOrderMessage.Text = HttpContext.GetGlobalResourceObject("CommonCaption", "NoOrder").ToString();
                dlOrder.Visible = false;
                divOrderPager.Visible = false;
            }

        }

        /// <summary>
        /// Bind Affiliate data
        /// </summary>
        protected void BindAffiliateDetails()
        {
            if (this.userAccount.ReferralStatus == "A" || this.userAccount.ReferralStatus == "N")
            {
                pnlAffiliate.Visible = true;
                this.ReferralCommision = this.userAccount.ReferralCommission.ToString("N");

                this.TaxId = this.userAccount.TaxId;

                if (this.userAccount.ReferralCommissionTypeId == 1)
                {
                    this.ReferralCommisionType = "Percentage";
                }
                else if (this.userAccount.ReferralCommissionTypeId == 2)
                {
                    this.ReferralCommisionType = "Amount";
                }
            }

            if (this.userAccount.ReferralStatus == "A")
            {
                string affiliateLink = "http://" + UserStoreAccess.DomainName + "/default.aspx?affiliate_id=" + this.userAccount.AccountID;
                hlAffiliateLink.Text = affiliateLink;
                hlAffiliateLink.NavigateUrl = affiliateLink;
            }
        }

        /// <summary>
        /// Bind wish list grid
        /// </summary>
        protected void BindWishList()
        {
            ZNodeProductList _productList = new ZNodeProductList();
            _productList = ViewState["WishList"] == null ? ZNodeProductList.GetWishListByAccountID(this.userAccount.AccountID) : (ZNodeProductList)ViewState["WishList"];
            PagedDataSource objPage = new PagedDataSource();
            objPage.DataSource = _productList.ZNodeProductCollection;
            objPage.AllowPaging = true;
            objPage.PageSize = wishlistpageSize;
            if (objPage.PageCount > 1)
            {
                objPage.CurrentPageIndex = !string.IsNullOrEmpty(hdnWishListCurrentPage.Value) && int.Parse(hdnWishListCurrentPage.Value) <= objPage.PageCount ? int.Parse(hdnWishListCurrentPage.Value) - 1 : 0;
                CreatePagingControl(dlWishListPager, objPage.PageCount, "WishList", objPage.CurrentPageIndex + 1);
                divWishlistPager.Visible = true;
            }
            else
            {
                divWishlistPager.Visible = false;
            }
            dlWishList.DataSource = objPage;
            dlWishList.DataBind();
            dlWishList.Visible = _productList.ZNodeProductCollection.Count > 0;
            if (_productList.ZNodeProductCollection.Count <= 0)
            {
                lblEmptyMessage.Text = string.Empty;
                lblEmptyMessage.Text = HttpContext.GetGlobalResourceObject("CommonCaption", "NoWishList").ToString();
            }
        }

        /// <summary>
        ///  Event triggered when a Edit Address button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnEditAddress_Click(object sender, EventArgs e)
        {
            // Redirect to account page
            ZNodeUrl url = new ZNodeUrl();
            Response.Redirect("~/editcontact.aspx");
        }

        protected void LbAddNewAddress_Click(object sender, EventArgs e)
        {
            Response.Redirect("address.aspx");
        }


        //protected void GvGiftCardHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    if (string.Compare(e.CommandName, "View", true) == 0)
        //    {
        //        GridViewRow selectedRow = (e.CommandSource as LinkButton).NamingContainer as GridViewRow;

        //        TableCell idcell = selectedRow.Cells[4];
        //        string id = idcell.Text;
        //        ZNodeUrl url = new ZNodeUrl();
        //        string viewLink = "~/order.aspx?itemid=" + id;
        //        Response.Redirect(viewLink);
        //    }
        //}


        /// <summary>
        /// This event raised on clicking the button btnChangePassword
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnChangePassword_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/password.aspx");
        }

        //protected void GvGiftCardHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    gvGiftCardHistory.PageIndex = e.NewPageIndex;
        //    this.BindGiftCardHistory(this.accountId);
        //}

        /// <summary>
        /// Add To Cart button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Buy_Click(object sender, EventArgs e)
        {
            string link = "~/product.aspx?zpid=";

            // Getting ProductID from the Link button
            LinkButton but_buy = sender as LinkButton;
            int productId = int.Parse(but_buy.CommandArgument);

            Response.Redirect(link + productId + "&action=addtocart");
        }

        /// <summary>
        /// Remove link button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LnkRemove_Click(object sender, EventArgs e)
        {
            LinkButton btn = sender as LinkButton;
            int productId = int.Parse(btn.CommandArgument);

            WishListService service = new WishListService();
            WishList wishListEntity = service.GetByProductID(productId)[0];
            service.Delete(wishListEntity);

            ViewState["WishList"] = null;
            this.BindWishList();
        }

        #endregion

        #region Page Events

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // If user is an admin, then redirect to the admin dashboard
            if (Roles.IsUserInRole("ADMIN") || Roles.IsUserInRole("CUSTOMER SERVICE REP")) //zeon custom code
            {
                Response.Redirect("~/siteadmin/secure/default.aspx");
            }
            else if (Roles.IsUserInRole("FRANCHISE"))
            {
                Response.Redirect("~/franchiseAdmin/secure/default.aspx");
            }
            else if (Roles.IsUserInRole("VENDOR"))
            {
                Response.Redirect("~/MallAdmin/secure/product/list.aspx");
            }


            // Get the user account from session
            this.userAccount = ZNodeUserAccount.CurrentAccount();
            if (this.userAccount != null)
            {
                AccountService accountService = new AccountService();
                Account account = accountService.GetByAccountID(this.userAccount.AccountID);
                if (account != null)
                {
                    lblEmail.Text = account.Email;

                    // Email Opt-In
                    if (account.EmailOptIn)
                    {
                        EmailOptin.ImageUrl = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
                    }
                    else
                    {
                        EmailOptin.ImageUrl = ZNode.Libraries.Admin.Helper.GetCheckMark(false);
                    }
                    //Zeon Custom Code
                    SetUserName();
                    SetPagingCount();
                }
            }

            this.accountId = this.userAccount.AccountID;
            this.BindAffiliateDetails();

            //this.BindAddresses();
            if (Request.Params["zpid"] != null)
            {
                int.TryParse(Request.Params["zpid"], out this.productId);
            }

            if (this.productId > 0)
            {
                WishListService service = new WishListService();
                WishListQuery query = new WishListQuery();
                query.AppendEquals(WishListColumn.ProductID, this.productId.ToString());
                query.AppendEquals(WishListColumn.AccountID, this.userAccount.AccountID.ToString());
                TList<WishList> wishList = service.Find(query.GetParameters());

                if (wishList.Count == 0)
                {
                    WishList wishListEntity = new WishList();
                    wishListEntity.AccountID = this.userAccount.AccountID;
                    wishListEntity.ProductID = this.productId;
                    wishListEntity.CreateDte = System.DateTime.Today;
                    service.Insert(wishListEntity);
                }

                Response.Redirect("~/account.aspx");
            }

            this.BindAddresses();
            this.BindOrders();
            this.BindWishList();

            //Zeon Custom Code:Starts
            if (!Page.IsPostBack)
            {
                ViewState["OrderList"] = null;
                ViewState["WishList"] = null;
                ViewState["AddressList"] = null;
            }
            //Zeon Custom Code:Ends
            this.accountId = this.userAccount.AccountID;

            if (this.Page != null)
            {
                ZNodeResourceManager resourceManager = new ZNodeResourceManager();
                this.Page.Title = resourceManager.GetLocalResourceObject(this.TemplateControl.AppRelativeVirtualPath, "txtAccountTitle.Text");
            }
        }

        /// <summary>
        /// Page Prerender event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            // this.BindGiftCardHistory(this.accountId);
            RegisterClientScriptClasses();
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Bind the user account list.
        /// </summary>
        private void BindAddresses()
        {
            TList<Address> addressList;
            if (ViewState["AddressList"] == null)
            {
                AddressService addressService = new AddressService();
                addressList = addressService.GetByAccountID(this.accountId);
                addressList.Sort("Name ASC");
                if (addressList.Count > 0)
                {
                    // Set Viewstate object with address collection object
                    ViewState["AddressList"] = addressList;
                }
            }
            else
            {
                addressList = (TList<Address>)ViewState["AddressList"];
            }
            //Bind DataList Paging Control
            PagedDataSource objPage = new PagedDataSource();
            if (addressList.Count > 0)
            {
                objPage.DataSource = addressList;
                objPage.AllowPaging = true;
                objPage.PageSize = addressPageSize;
                if (objPage.PageCount > 1)
                {
                    objPage.CurrentPageIndex = !string.IsNullOrEmpty(hdnAddressCurrrentPage.Value) && int.Parse(hdnAddressCurrrentPage.Value) <= objPage.PageCount ? int.Parse(hdnAddressCurrrentPage.Value) - 1 : 0;
                    CreatePagingControl(dlAddressPaging, objPage.PageCount, "Address", objPage.CurrentPageIndex + 1);
                    divAddressPager.Visible = true;
                }
                else
                {
                    divAddressPager.Visible = false;
                }
                dlAddress.DataSource = objPage;
                dlAddress.DataBind();
                dlAddress.Visible = addressList.Count > 0;
            }
        }

        /// <summary>
        /// Bind Gift Card History
        /// </summary>
        /// <param name="accountId">accountId to bind GiftCard History</param>
        //private void BindGiftCardHistory(int accountId)
        //{
        //    GiftCardAdmin giftCardAdmin = new GiftCardAdmin();
        //    TList<GiftCardHistory> giftCardHistoryList = giftCardAdmin.GetGiftCardHistoryByAccountID(accountId);
        //    if (giftCardHistoryList != null)
        //    {
        //        giftCardHistoryList.Sort("OrderID DESC");
        //    }

        //    gvGiftCardHistory.DataSource = giftCardHistoryList;
        //    gvGiftCardHistory.DataBind();
        //}

        /// <summary>
        /// Delete the selected address.
        /// </summary>
        /// <param name="addressId">Address Id</param>
        private void DeleteAddress(int addressId)
        {
            AddressService addressService = new AddressService();
            AccountAdmin accountAdmin = new AccountAdmin();
            TList<Address> addressList = addressService.GetByAccountID(this.userAccount.AccountID);
            int addressCount = addressList.Count;
            lblError.Text = string.Empty;
            DivAddressErr.Visible = false;

            // If account has more than one address then validate for default address.
            if (addressCount > 1)
            {
                // Validate default shipping address.
                addressList.ApplyFilter(delegate(Address a) { return a.IsDefaultBilling == true && a.AddressID == addressId; });
                if (addressList.Count == 1)
                {
                    lblError.Text = "Default billing address could not be deleted. Set some other address as default billing address and then try again.";
                    DivAddressErr.Visible = true;
                    return;
                }

                addressList.RemoveFilter();

                // Validate default shipping address.
                addressList.ApplyFilter(delegate(Address a) { return a.IsDefaultShipping == true && a.AddressID == addressId; });
                if (addressList.Count == 1)
                {
                    lblError.Text = "Default shipping address could not be deleted. Set some other address as default shipping address and then try again.";
                    DivAddressErr.Visible = true;
                    return;
                }
            }
            addressService.Delete(addressId);
        }

        #endregion

        #region Zeon Custom Code
        /// <summary>
        /// Send Email Button Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSendWishListEmail_Click(object sender, EventArgs e)
        {
            lblMessage.Text = string.Empty;
            string wishListMessgaeTemplate = string.Empty;
            string emailAddress = hdnEmailAddress.Value;
            int productId = int.Parse(hdnProductID.Value);
            wishListMessgaeTemplate = this.GenerateWishListEmailTemplate(productId);

            if (!string.IsNullOrEmpty(wishListMessgaeTemplate))
            {
                SendWishListEmail(wishListMessgaeTemplate, emailAddress);
            }

            hdnEmailAddress.Value = string.Empty;
            hdnName.Value = string.Empty;
            hdnProductID.Value = string.Empty;

        }

        /// <summary>
        /// Send Wish List Email
        /// </summary>
        private void SendWishListEmail(string wishlistMsgTemplate, string emailAddress)
        {
            ZeonMessageConfig zeonMessageconfig = new ZeonMessageConfig();
            try
            {
                ZNodeEmail.SendEmail(emailAddress, userAccount.EmailID, string.Empty, zeonMessageconfig.GetMessageKey("WishListEmailSubject"), wishlistMsgTemplate, true);
                lblMessage.Text = this.GetLocalResourceObject("MailSent").ToString();
                lblMessage.Focus();
                lblMessage.CssClass = "SuccessMsg";
            }
            catch (Exception ex)
            {
                lblMessage.Text = this.GetLocalResourceObject("MailSendFailed").ToString();
                lblMessage.CssClass = "Error";
                lblMessage.Focus();
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "Wish List user email template sending problem(Mehtod name: SendWishListEmail()):" + ex.StackTrace);
            }
        }

        /// <summary>
        /// Sending the Template
        /// </summary>
        private string GenerateWishListEmailTemplate(int productId)
        {
            string htmlTemplatePath = string.Empty;
            string CurrentCulture = string.Empty;
            string defaultHtmlTemplatePath = string.Empty;
            string productNum = string.Empty;
            string productShortDescription = string.Empty;
            string productPrice = string.Empty;
            string productName = string.Empty;
            string friendName = string.Empty;
            ZNodeProduct _whishListproduct = ZNodeProduct.Create(productId);
            string domainPath = Request.Url.GetLeftPart(UriPartial.Authority);
            string productURL = domainPath + Response.ApplyAppPathModifier(_whishListproduct.ViewProductLink);
            string imageURL = domainPath + ResolveUrl(_whishListproduct.MediumImageFilePath);

            // TemplatePath
            CurrentCulture = ZNodeCatalogManager.CultureInfo;

            defaultHtmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ZWishList.htm");

            // modified receipt file selection as we are using in-line styles for displaying template on mail content.
            if (CurrentCulture != string.Empty)
            {
                htmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ZWishList_" + CurrentCulture + ".htm");
            }
            else
            {
                htmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ZWishList.htm");
            }
            // Check the receipt template exists, if not exists then set default receipt template
            FileInfo fileinfo = new FileInfo(htmlTemplatePath);

            if (!fileinfo.Exists)
            {
                htmlTemplatePath = defaultHtmlTemplatePath;
            }

            productPrice = _whishListproduct.ProductPrice.ToString();
            productNum = _whishListproduct.ProductNum;
            productShortDescription = _whishListproduct.ShortDescription;
            productName = _whishListproduct.Name;
            friendName = hdnName.Value;

            StreamReader streamReader = new StreamReader(htmlTemplatePath);
            string messageText = streamReader.ReadToEnd();

            string spacerImage = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/Themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/Images/space.png";
            int portalId = ZNodeConfigManager.SiteConfig.PortalID;

            ZEmailHelper zEmailHelper = new ZEmailHelper();
            ZeonMessageConfig zeonMessageConfig = new ZeonMessageConfig();
            string copyRightText = zeonMessageConfig.GetMessageKey("FooterCopyrightText");

            string messageTextHeader = zEmailHelper.EmailHeaderContent(domainPath, portalId, spacerImage);
            string messageTextFooter = zEmailHelper.EmailFooterContent(domainPath, portalId, copyRightText, spacerImage);

            //To add header to the template
            Regex rxHeaderContent = new Regex("#HeaderContent#", RegexOptions.IgnoreCase);
            messageText = rxHeaderContent.Replace(messageText, messageTextHeader);

            //To add footer to the template
            Regex rxFooterContent = new Regex("#FooterContent#", RegexOptions.IgnoreCase);
            messageText = rxFooterContent.Replace(messageText, messageTextFooter);

            Regex rxFriendName = new Regex("#FriendName#", RegexOptions.IgnoreCase);
            messageText = rxFriendName.Replace(messageText, friendName);

            Regex rxProductURL = new Regex("#ProductUrl#", RegexOptions.IgnoreCase);
            messageText = rxProductURL.Replace(messageText, productURL);


            Regex rxProductImage = new Regex("<#ProductImageSrc#>", RegexOptions.IgnoreCase);
            messageText = rxProductImage.Replace(messageText, imageURL);

            Regex rxProductNum = new Regex("#ProductNum#", RegexOptions.IgnoreCase);
            messageText = rxProductNum.Replace(messageText, productNum);

            Regex rxProductName = new Regex("#ProductName#", RegexOptions.IgnoreCase);
            messageText = rxProductName.Replace(messageText, productName);

            Regex rxShortDescription = new Regex("#ShortDescription#", RegexOptions.IgnoreCase);
            messageText = rxShortDescription.Replace(messageText, productShortDescription);

            Regex rxProductPrice = new Regex("#ProductPrice#", RegexOptions.IgnoreCase);
            messageText = rxProductPrice.Replace(messageText, productPrice);

            Regex rxFromSignature = new Regex("#FromSignature#", RegexOptions.IgnoreCase);
            messageText = rxFromSignature.Replace(messageText, userAccount.FirstName);

            return messageText;

        }

        /// <summary>
        /// Set new user name
        /// </summary>
        private void SetUserName()
        {
            if (this.Page.User.Identity.IsAuthenticated)
            {
                if (!string.IsNullOrEmpty(this.Page.User.Identity.Name))
                {
                    LoginName1.FormatString = string.Format("<b> {0}</b>", this.Page.User.Identity.Name.Replace("_" + ZNodeConfigManager.SiteConfig.PortalID.ToString(), ""));
                }
            }
        }

        /// <summary>
        /// Registers the Script
        /// </summary>
        private void RegisterClientScriptClasses()
        {
            StringBuilder script = new StringBuilder();
            script.Append("var zeonAccount = new Account({");
            script.Append("\"Controls\":{");
            script.AppendFormat("\"{0}\":\"{1}\"", lblProductName.ID, lblProductName.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", hdnProductID.ID, hdnProductID.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", txtEmailId.ID, txtEmailId.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", txtFirstName.ID, txtFirstName.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", hdnName.ID, hdnName.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", hdnEmailAddress.ID, hdnEmailAddress.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", lblErrors.ID, lblErrors.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", hdnName.ID, hdnName.ClientID);
            script.Append("},\"Messages\":{");
            script.AppendFormat("\"EmailValidationErrorMessage\":\"{0}\"", GetLocalResourceObject("EmailValidationErrorMessage").ToString());
            script.AppendFormat(",\"NameRequiredErrorMessage\":\"{0}\"", GetLocalResourceObject("NameRequiredErrorMessage").ToString());
            script.Append("}");
            script.Append("});zeonAccount.Init();");
            this.Page.ClientScript.RegisterStartupScript(GetType(), "Account", script.ToString(), true);
        }

        /// <summary>
        /// Format order Status
        /// </summary>
        /// <param name="orderStatus">string</param>
        /// <returns>string</returns>
        protected string FormatOrderStatus(string orderStatus)
        {
            string strFormat = string.Empty;
            strFormat = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(orderStatus.ToLower());
            if (strFormat.Contains("Returned") || strFormat.Contains("Cancelled"))
            {
                strFormat = string.Format("<span class='RejectStatus'>{0}</span>", strFormat);
            }
            return strFormat;
        }

        #region Paging Implementation

        #region Private Methods

        /// <summary>
        /// Bind Pager Control for list on the page
        /// </summary>
        /// <param name="containerControl">PlaceHolder</param>
        /// <param name="listPageCount">int</param>
        /// <param name="listName">string</param>
        private void CreatePagingControl(PlaceHolder containerControl, int listPageCount, string listName, int currentPage)
        {
            StringBuilder pagerList = new StringBuilder();
            containerControl.Controls.Clear();
            for (int pagecount = 1; pagecount <= listPageCount; pagecount++)
            {
                LinkButton lnkPager = new LinkButton();
                lnkPager.Text = pagecount.ToString();
                if (currentPage == pagecount)
                {
                    lnkPager.Enabled = false;
                    lnkPager.CssClass = "GridPaging";
                }
                else
                {
                    lnkPager.CssClass = "GridPaging Active";
                }
                lnkPager.CommandArgument = pagecount.ToString();
                lnkPager.Click += new EventHandler(GetListEventHandeler(listName));
                pagerList.Append(lnkPager);
                containerControl.Controls.Add(lnkPager);
            }
        }

        /// <summary>
        /// Return Event Handeler For list paging click based on its name
        /// </summary>
        /// <param name="listControlName">string</param>
        /// <returns>EventHandler</returns>
        private EventHandler GetListEventHandeler(string listControlName)
        {
            switch (listControlName)
            {
                case "Order": return this.lnkOrderPager_Click;
                case "WishList": return this.lnkWishListPager_Click;
                case "Address": return this.lnkPager_Click;
            }
            return this.lnkPager_Click;
        }

        /// <summary>
        /// set Address,Wish List, Order Paging Count
        /// </summary>
        private void SetPagingCount()
        {
            addressPageSize = int.Parse(this.GetLocalResourceObject("AddressPageSize").ToString());
            wishlistpageSize = int.Parse(this.GetLocalResourceObject("WishListPageSize").ToString());
            orderPageSize = int.Parse(this.GetLocalResourceObject("OrderPageSize").ToString());
        }
        #endregion

        #region Protected Methods

        /// <summary>
        /// Handles Address Pager click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkPager_Click(object sender, EventArgs e)
        {
            LinkButton btn = sender as LinkButton;
            hdnAddressCurrrentPage.Value = btn.CommandArgument;
            BindAddresses();
            dlAddress.Focus();
        }

        /// <summary>
        /// Handeles order pager click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkOrderPager_Click(object sender, EventArgs e)
        {
            LinkButton btn = sender as LinkButton;
            hdnOrderCurrentPage.Value = btn.CommandArgument;
            BindOrders();
            dlOrder.Focus();
        }

        /// <summary>
        /// Handeles Wish list pager Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkWishListPager_Click(object sender, EventArgs e)
        {
            LinkButton btn = sender as LinkButton;
            hdnWishListCurrentPage.Value = btn.CommandArgument;
            BindWishList();
            dlWishList.Focus();
        }

        /// <summary>
        /// Handeles Datalist Item Command
        /// </summary>
        /// <param name="source">object</param>
        /// <param name="e">DataListCommandEventArgs</param>
        protected void dlAddress_OnDeleteCommand(object source, DataListCommandEventArgs e)
        {
            ViewState["AddressList"] = null;
            this.BindAddresses();
        }

        /// <summary>
        /// Handeles Datalist Item Command
        /// </summary>
        /// <param name="source">object</param>
        /// <param name="e">DataListCommandEventArgs</param>
        protected void dlAddress_OnItemCommand(object source, DataListCommandEventArgs e)
        {
            int addressId = Convert.ToInt32(e.CommandArgument);
            if (string.Compare(e.CommandName, "Delete", true) == 0)
            {
                this.DeleteAddress(addressId);
            }
        }


        #endregion

        #endregion

        #endregion
    }
}