﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Zeon.Libraries.Elmah;
using Zeon.Libraries.Utilities;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the Edit Contact user control class.
    /// </summary>
    public partial class Controls_Default_Account_EditContact : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeUserAccount _userAccount;
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get the user account from session
            this._userAccount = ZNodeUserAccount.CurrentAccount();

            if (!this.IsPostBack)
            {
                this.BindContact();
            }
        }
        #endregion

        #region Event Methods
        protected void LbCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("account.aspx");
        }

        protected void LbSubmit_Click(object sender, EventArgs e)
        {
            this.SubmitPage();
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Bind the user contact information
        /// </summary>
        private void BindContact()
        {
            if (this._userAccount == null)
            {
                Response.Redirect("~/default.aspx");
            }
            else
            {
                AccountService accountService = new AccountService();
                Account account = accountService.GetByAccountID(this._userAccount.AccountID);
                if (account != null)
                {
                    txtEmail.Text = account.Email;
                    chkEmailOptIn.Checked = account.EmailOptIn;
                }
            }
        }

        /// <summary>
        /// Submit the contact information
        /// </summary>
        private void SubmitPage()
        {
            AccountService accountService = new AccountService();
            Account account = accountService.GetByAccountID(this._userAccount.AccountID);
            if (account != null)
            {
                account.Email = txtEmail.Text;
                account.EmailOptIn = chkEmailOptIn.Checked;
              
                bool isSuccess = accountService.Update(account);

                //Zeon Custom Code:Starts
                if (chkEmailOptIn.Checked)
                {
                    string email = account.UserID.ToString().IndexOf('_') > 0 ? account.UserID.ToString().Split('_')[0] : account.UserID.ToString();
                    SubscribeEmailUser(email);
                }
                else if (!string.IsNullOrEmpty(account.Custom2))
                {
                    UnSubscribeEmailUser(account.Custom2);
                }
                //Zeon Custom Code:Ends
                // Update online account email address
                if (this._userAccount.UserID.HasValue)
                {
                    MembershipUser user = Membership.GetUser(this._userAccount.UserID.Value);

                    if (user != null)
                    {
                        user.Email = this._userAccount.EmailID;
                        Membership.UpdateUser(user);
                    }
                }
                // If successfully updated then redirect to account home page.
                if (isSuccess)
                {
                    Response.Redirect("account.aspx");
                }
            }
        }
        #endregion

        #region Zeon Custom Code
        /// <summary>
        /// Subscribe User For Email Service
        /// </summary>
        /// <param name="userEmail">string</param>
        private void SubscribeEmailUser(string userEmail)
        {
            string responseContactID = string.Empty;
            try
            {
                if (ConfigurationManager.AppSettings["GetResponseAPIKey"] != null && ConfigurationManager.AppSettings["GetResponseAPIURL"] != null)
                {
                    string newsLetterCampaign = GetCompaignNameByPortal();
                    if (!string.IsNullOrEmpty(newsLetterCampaign))
                    {
                        GetResponseEmailSubscriber emailSubscriber = new GetResponseEmailSubscriber();
                        emailSubscriber.AddContactEmailSubcriber(ConfigurationManager.AppSettings["GetResponseAPIKey"].ToString(), ConfigurationManager.AppSettings["GetResponseAPIURL"].ToString(), newsLetterCampaign, userEmail, userEmail);

                    }
                }
            }
            catch (Exception ex)
            {
                ElmahErrorManager.Log(ex.StackTrace);
            }
        }

        /// <summary>
        /// Get Newsletter Compaign Name by Portal
        /// </summary>
        /// <returns></returns>
        private string GetCompaignNameByPortal()
        {
            string compaignName = string.Empty;
            PortalExtnService portalExtnService = new PortalExtnService();
            TList<PortalExtn> currentPortal = portalExtnService.GetByPortalID(ZNodeConfigManager.SiteConfig.PortalID);
            if (currentPortal != null && currentPortal.Count > 0)
            {
                compaignName = currentPortal[0].NewsLetterCampaignName;
            }

            return compaignName;
        }


        /// <summary>
        /// Subscribe User For Email Service
        /// </summary>
        /// <param name="contactId">string</param>
        private void UnSubscribeEmailUser(string contactId)
        {
            string responseContactID = string.Empty;
            try
            {
                if (ConfigurationManager.AppSettings["GetResponseAPIKey"] != null && ConfigurationManager.AppSettings["GetResponseAPIURL"] != null)
                {
                    string newsLetterCampaign = GetCompaignNameByPortal();
                    if (!string.IsNullOrEmpty(newsLetterCampaign))
                    {
                        GetResponseEmailSubscriber emailSubscriber = new GetResponseEmailSubscriber();
                        emailSubscriber.UnsubscribeContactEmail(ConfigurationManager.AppSettings["GetResponseAPIKey"].ToString(), ConfigurationManager.AppSettings["GetResponseAPIURL"].ToString(), contactId);

                    }
                }
            }
            catch (Exception ex)
            {
                ElmahErrorManager.Log(ex.StackTrace);
            }
        }
        #endregion
    }
}