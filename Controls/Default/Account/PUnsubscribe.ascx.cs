﻿using System;
using ZNode.Libraries.DataAccess.Service;
using Zeon.Libraries.Elmah;
using Zeon.Libraries.Utilities;
using System.Configuration;
using ZNode.Libraries.Framework.Business;
using System.Text;

namespace WebApp.Controls.Default.Account
{
    public partial class PUnsubscribe : System.Web.UI.UserControl
    {
        #region Private Member
        private int _accountID = 0;
        #endregion

        #region Public Properties

        /// <summary>
        /// set or get _accountID value
        /// </summary>
        public int AccountID
        {
            get
            {
                return _accountID;
            }
            set
            {
                _accountID = value;
            }
        }

        #endregion

        #region Page Events

        /// <summary>
        /// Handeles Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get Account ID  from querystring  
            if (Request.Params["aid"] != null)
            {
                //Decrypt querystring 
                GetDecryptedAccountID(Request.Params["aid"]);
                if (this.AccountID > 0)
                {
                    LoadAccountData();
                    ShowDetailsControl(true);
                }
                else
                {
                    locMessage.Text = string.Format(this.GetLocalResourceObject("UnableToFindAccount").ToString(), ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber, "mailto:" + ZNodeConfigManager.SiteConfig.CustomerServiceEmail);
                    ShowDetailsControl(false);
                    divStatusMessage.Visible = false;
                    
                    locMessage.Visible = true;
                }
            }
        }

        #endregion

        #region Protected Events

        /// <summary>
        /// Handeles Button Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ibUpdateInfo_Click(object sender, EventArgs e)
        {
            if (this.AccountID > 0)
            {
                SaveAccountInfo(false, true);
                locStatusMessage.Text = this.GetLocalResourceObject("ChangeEmailMessage").ToString();
                ShowDetailsControl(false);
            }
        }

        /// <summary>
        /// Handeles Button Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ibUnSubscribe_Click(object sender, EventArgs e)
        {
            if (this.AccountID > 0)
            {
                SaveAccountInfo(true, false);
                locStatusMessage.Text = this.GetLocalResourceObject("UnsubscribeSuccessMessage").ToString();
                ShowDetailsControl(false);
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Save Email Opt In if Check box is check
        /// </summary>
        private void SaveAccountInfo(bool isUnSubscribe, bool isUpdateInfo)
        {
            AccountService accountService = new AccountService();
            ZNode.Libraries.DataAccess.Entities.Account account = accountService.GetByAccountID(this.AccountID);
            try
            {
                if (account != null)
                {
                    if (isUnSubscribe) { account.EmailOptIn = !isUnSubscribe; }
                    if (isUpdateInfo) { account.Email = txtNewEmail.Text.Trim(); }
                    accountService.Update(account);
                }
            }
            catch (Exception ex)
            {
                ElmahErrorManager.Log(ex.StackTrace);
            }
        }

        /// <summary>
        /// Use To  Decrypt Account ID
        /// </summary>
        /// <param name="creditCard"></param>
        /// <returns></returns>
        private void GetDecryptedAccountID(string encryptedAcctID)
        {
            try
            {
                string decryptedAcctID = string.Empty;
                string encryptionPassKey = ConfigurationManager.AppSettings["EncryptionPassKey"] != null && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["EncryptionPassKey"].ToString()) ? ConfigurationManager.AppSettings["EncryptionPassKey"].ToString() : string.Empty;
                if (!string.IsNullOrEmpty(encryptionPassKey))
                {
                    decryptedAcctID = ZEncryption.DecryptString(encryptedAcctID, encryptionPassKey);
                    if (!string.IsNullOrEmpty(decryptedAcctID))
                    {
                        int.TryParse(decryptedAcctID, out _accountID);
                    }
                }
            }
            catch (Exception ex)
            {
                ElmahErrorManager.Log(ex.StackTrace);
            }

        }

        /// <summary>
        /// Load Currunt Account Data
        /// </summary>
        private void LoadAccountData()
        {
            if (this.AccountID > 0)
            {
                AccountService accountService = new AccountService();
                ZNode.Libraries.DataAccess.Entities.Account account = accountService.GetByAccountID(this.AccountID);
                if (account != null)
                {
                    if (!account.EmailOptIn)
                    {
                        locStatusMessage.Text = this.GetLocalResourceObject("UnsubscribeSuccessMessage").ToString();
                        ShowDetailsControl(false);
                    }
                    else
                    {
                        lblUserName.Text = account.Email;
                        ShowDetailsControl(true);
                    }
                }
            }
        }

        /// <summary>
        /// set control visbility 
        /// </summary>
        /// <param name="isVisible"></param>
        private void ShowDetailsControl(bool isVisible)
        {
            divUnsubscribe.Visible = divChangeContact.Visible = locMessage.Visible = isVisible;
            divStatusMessage.Visible = !isVisible;
        }
        #endregion
    }
}