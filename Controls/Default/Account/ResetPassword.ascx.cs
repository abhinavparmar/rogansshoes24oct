using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using Account = ZNode.Libraries.DataAccess.Entities.Account;

namespace WebApp
{
    /// <summary>
    /// Represents the Reset Password user control class.
    /// </summary>
    public partial class Controls_Default_Account_ResetPassword : System.Web.UI.UserControl
    {
        #region Protected member variables
        private string _errorCode = string.Empty;
        private Account account = null;
        #endregion

        #region Events
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Check for account object value in session state
            if (Session["AccountObject"] == null)
            {
                Response.Redirect("~/account.aspx");
            }

            // Get User Account object from session object
            this.account = Session["AccountObject"] as Account;

            // Check for error code value in session state
            if (Session["ErrorCode"] != null)
            {
                // Get Error Code from session object
                this._errorCode = Session["ErrorCode"].ToString();
            }

            if (!Page.IsPostBack)
            {
                bool adminUserInd = false;

                if (this.account != null)
                {
                    MembershipUser user = Membership.GetUser(this.account.UserID);

                    if (user != null)
                    {
                        adminUserInd = Roles.IsUserInRole(user.UserName, "admin");

                        // Show email field if logged-on user is in the 'admin' role
                        if (adminUserInd)
                        {
                            tblRowEmail.Visible = true;
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/account.aspx");
                }

                if (this._errorCode.Equals("1"))
                {
                    lblIntroMessage.Text = this.GetLocalResourceObject("RequestResetPassword").ToString();

                    // Show UserID,passwordQuestion & answer fields 
                    tblRowUserId.Visible = true;
                    //tblRowSecurityQuestion.Visible = true;
                    //tblRowPasswordAnswer.Visible = true;
                    pnlCurrentpassword.Visible = false;
                }
                else if (this._errorCode.Equals("2"))
                {
                    lblIntroMessage.Text = this.GetLocalResourceObject("PasswordExpired").ToString();

                    // Hide User id field
                    tblRowUserId.Visible = false;
                    //tblRowSecurityQuestion.Visible = false;
                    //tblRowPasswordAnswer.Visible = false;
                }
                else
                {
                    Response.Redirect("~/account.aspx");
                }
            }
            UserName.Focus();//PEFT Custom Code
        }

        /// <summary>
        /// Continue button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ContinuePushButton_Click(object sender, EventArgs e)
        {
            // Redirect to account page
            Response.Redirect("~/account.aspx");
        }

        /// <summary>
        /// Reset password button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ResetPasswordPushButton_Click(object sender, EventArgs e)
        {
            bool status = false;
            string userName = string.Empty;
            AccountService acctService = new AccountService();
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging log = new ZNode.Libraries.ECommerce.Utilities.ZNodeLogging();

            if (this.account == null)
            {
                PasswordFailureText.Text = Resources.CommonCaption.UnableToProcessRequest;
                errorMsg.Visible = true;
                return;
            }

            log.LogActivityTimerStart();

            // Current User
            MembershipUser currentUser = Membership.GetUser(this.account.UserID.Value);

            if (currentUser == null)
            {
                log.LogActivityTimerEnd(1107, UserName.Text.Trim(), null, null, "User not found", null);
                PasswordFailureText.Text = Resources.CommonCaption.UnableToProcessRequest;
                errorMsg.Visible = true;
                return;
            }

            // First time log in
            if (this._errorCode.Equals("1"))
            {
                userName = UserName.Text.Trim() + "_" + ZNodeConfigManager.SiteConfig.PortalID.ToString();//Zeon Custom Code;
                string emailId = currentUser.Email;

                if (System.Web.Security.Roles.IsUserInRole(currentUser.UserName, "admin"))
                {
                    emailId = Email.Text.Trim();

                    // Set Email address
                    this.account.Email = emailId;
                }

                // Create user membership
                //Zeon Custom Code:Commented as part of removing security Question and answer:Starts
                ZNodeUserAccount userAccount = new ZNodeUserAccount();
                //Zeon Custom Code:Replace UserName with Email in all Places
                if (!userAccount.IsLoginNameAvailable(ZNodeConfigManager.SiteConfig.PortalID, userName))
                {
                    log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateFailed, userName, null, null, "User name already exists", null);
                    PasswordFailureText.Text = Resources.CommonCaption.AlreadyExist;
                    errorMsg.Visible = true;
                    return;
                }
                //MembershipCreateStatus memStatus;
                //MembershipUser newuser = Membership.CreateUser(userName, NewPassword.Text.Trim(), emailId, ddlSecretQuestions.SelectedItem.Value, Answer.Text.Trim(), true, out memStatus);
                MembershipUser newuser = Membership.CreateUser(userName, NewPassword.Text.Trim(), emailId);
                //if (memStatus == MembershipCreateStatus.DuplicateUserName)
                //{
                //    log.LogActivityTimerEnd(1107, UserName.Text.Trim(), null, null, "User name already exists", null);
                //    PasswordFailureText.Text = Resources.CommonCaption.AlreadyExist;
                //    return;
                //}
                if (!newuser.IsApproved)
                {
                    log.LogActivityTimerEnd(1107, UserName.Text.Trim());
                    PasswordFailureText.Text = Resources.CommonCaption.CreateFailed;
                    errorMsg.Visible = true;
                    return;
                }
                //Zeon Custom Code:Commented as part of removing security Question and answer:Ends
                else
                {
                    log.LogActivityTimerEnd(1106, UserName.Text.Trim());
                }

                // Get current roles for current user
                string[] roles = Roles.GetRolesForUser(currentUser.UserName);
                if (roles.Length > 0)
                {
                    // Associate the new user with the roles list
                    Roles.AddUsersToRoles(new string[] { newuser.UserName }, roles);
                }

                if (Roles.IsUserInRole(newuser.UserName, "admin"))
                {
                    // Create an Profile for the selected user
                    ProfileCommon newProfile = (ProfileCommon)ProfileCommon.Create(newuser.UserName, true);

                    // Properties Value
                    newProfile.StoreAccess = "AllStores";

                    // Save profile - must be done since we explicitly created it 
                    newProfile.Save();
                }

                // Log password
                ZNodeUserAccount.LogPassword((Guid)newuser.ProviderUserKey, NewPassword.Text.Trim());

                // Update latest userId
                this.account.UserID = (Guid)newuser.ProviderUserKey;
                acctService.Update(this.account);

                // Delete already existing user
                Membership.DeleteUser(currentUser.UserName);
            }
            else if (this._errorCode.Equals("2"))
            {
                // Password Expires
                UserName.Text = currentUser.UserName;
                if (!Membership.ValidateUser(currentUser.UserName, CurrentPassword.Text.Trim()))
                {
                    log.LogActivityTimerEnd(1107, UserName.Text.Trim(), null, null, "Password mismatch", null);
                    PasswordFailureText.Text = Resources.CommonCaption.LoginFailed;
                    errorMsg.Visible = true;
                    return;
                }

                // Verify if the new password specified by the user is in the list of the last 4 passwords used.
                status = ZNodeUserAccount.VerifyNewPassword((Guid)currentUser.ProviderUserKey, NewPassword.Text.Trim());

                if (status)
                {
                    userName = currentUser.UserName;

                    // Updates the password for this user
                    if (currentUser.ChangePassword(CurrentPassword.Text.Trim(), NewPassword.Text.Trim()))
                    {
                        // Log password
                        ZNodeUserAccount.LogPassword((Guid)currentUser.ProviderUserKey, NewPassword.Text.Trim());
                    }
                    else
                    {
                        log.LogActivityTimerEnd(1107, UserName.Text.Trim());
                        PasswordFailureText.Text = Resources.CommonCaption.UnableToProcessRequest;
                        errorMsg.Visible = true;
                        return;
                    }
                }
                else
                {
                    PasswordFailureText.Text = Resources.CommonCaption.RequestResetPassword;
                    log.LogActivityTimerEnd(1107, UserName.Text.Trim(), null, null, "Old password reused", null);
                    errorMsg.Visible = true;
                    return;
                }
            }
            else
            {
                PasswordFailureText.Text = Resources.CommonCaption.UnableToProcessRequest;
                errorMsg.Visible = true;
                return;
            }

            // If we reached this point then everything should have worked.
            log.LogActivityTimerEnd(1106, UserName.Text.Trim());

            // Login Process
            ZNodeUserAccount userAcct = new ZNodeUserAccount();
            bool loginSuccess = userAcct.Login(ZNodeConfigManager.SiteConfig.PortalID, UserName.Text.Trim(), NewPassword.Text.Trim());

            if (loginSuccess)
            {
                // Delete an Account object & ErrorCode from session
                // If the session-state collection does not contain an element with the specified name,
                // the session-state ollection remains unchanged. No exception is thrown.
                Session.Remove("AccountObject");
                Session.Remove("ErrorCode");

                // Set current user Profile.
                userAcct.ProfileID = ZNodeProfile.CurrentUserProfileId;

                // Get account and set to session
                Session.Add(ZNodeSessionKeyType.UserAccount.ToString(), userAcct);

                // Creates an authentication ticket for this user and adds it to the cookies collection of the response or the URL.
                FormsAuthentication.SetAuthCookie(UserName.Text.Trim(), false);

                // Get Profile entity for logged in user profileId
                ZNode.Libraries.DataAccess.Service.ProfileService _ProfileService = new ZNode.Libraries.DataAccess.Service.ProfileService();
                ZNode.Libraries.DataAccess.Entities.Profile _Profile = _ProfileService.GetByProfileID(userAcct.ProfileID);

                // Hold this profile object in the session state
                HttpContext.Current.Session["ProfileCache"] = _Profile;

                if (!Roles.IsUserInRole(UserName.Text.ToString(), "ADMIN"))
                {
                    // If Login success, merge the persistent cart items.
                    ZNode.Libraries.ECommerce.ShoppingCart.ZNodeSavedCart savedCart = new ZNode.Libraries.ECommerce.ShoppingCart.ZNodeSavedCart();
                    savedCart.Merge();
                }
            }

            // Redirect to account page
            Response.Redirect("~/account.aspx");
        }
        #endregion
    }
}