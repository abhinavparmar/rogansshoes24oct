﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="WebApp.Controls_Default_EmailFriend_SignupNewsletter" EnableViewState="false" CodeBehind="SignupNewsletter.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="Znode" %>
<asp:Panel ID="pnlNewsLetterSignup" runat="server" EnableViewState="false" DefaultButton="lnkNewsLetter"
    meta:resourcekey="pnlNewsLetterSignupResource1" ClientIDMode="Static">
    <div class="NewsLetterSignUp">
        <h2>
            <Znode:CustomMessage ID="ucSignUpHeader" runat="server" MessageKey="EmailSubscribeTitle" EnableViewState="False" />
        </h2>
        <div class="ValueStyle">
            <asp:TextBox ID="txtSignUpFirstName" ValidationGroup="SignUp" EnableViewState="true" runat="server" title="First Name" meta:resourceKey="txtFirstName" ClientIDMode="Static" CssClass="FirstName"  alt="First Name"></asp:TextBox> <%--onblur="objsignUpNewsLetterr.HideErrorMessage('FirstName')"--%>
            <div>
                <span class="Error" style="display: none" id="FirstNameErr"></span>
            </div>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtNewsLetter" Text="Email" runat="server" ValidationGroup="SignUp" EnableViewState="true" ClientIDMode="Static" title="Email" CssClass="NewsLetterEmail"
                alt="Email"></asp:TextBox> <%--onblur="objsignUpNewsLetter.HideErrorMessage('email')" --%>
            <div>
                <span class="Error" style="display: none" id="email"></span>
            </div>

        </div>
        <asp:LinkButton EnableViewState="false" ID="lnkNewsLetter" ValidationGroup="SignUp" runat="server" meta:resourcekey="newsletterImageResource1" OnClientClick="return objsignUpNewsLetter.lnkNewsLetter_Click();"></asp:LinkButton>
    </div>
</asp:Panel>
