﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the SignupNewsletter user control class.
    /// </summary>
    public partial class Controls_Default_EmailFriend_SignupNewsletter : System.Web.UI.UserControl
    {
        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            string theme = ZNodeCatalogManager.Theme;

            //newsletterImage.ImageUrl = ResolveUrl("~/Themes/" + ZNodeCatalogManager.Theme + "/Images/sign-me-up.png");
            
            // If category theme wise theme selected then apply the current category theme.
            if (Session["CurrentCategoryTheme"] != null)
            {
                theme = Session["CurrentCategoryTheme"].ToString();
            }

        }
        # endregion

        #region Private Method
        /// <summary>
        /// Link Button Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkNewsLetter_Click(object sender, EventArgs e)
        {
            Context.Items["Email"] = txtNewsLetter.Text;
            Context.Items["FirstName"] = txtSignUpFirstName.Text.Trim().Equals(this.GetLocalResourceObject("txtFirstName").ToString()) ? string.Empty : txtSignUpFirstName.Text;
            Server.Transfer("emailsignup.aspx");
        }
        #endregion

        #region Zeon Custom Code
        /// <summary>
        /// Page Prerender event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            RegisterClientScriptClasses();
        }

        /// <summary>
        /// Registers the Script
        /// </summary>
        private void RegisterClientScriptClasses()
        {
            StringBuilder script = new StringBuilder();
            script.Append("var objsignUpNewsLetter = new SignupNewsLetter({");
            script.Append("\"Controls\":{");
            script.AppendFormat("\"{0}\":\"{1}\"", txtSignUpFirstName.ID, txtSignUpFirstName.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", txtNewsLetter.ID, txtNewsLetter.ClientID);
            script.Append("}});objsignUpNewsLetter.Init();");

            this.Page.ClientScript.RegisterStartupScript(GetType(), "signUpNewsLetter", script.ToString(), true);
        }
        #endregion
    }
}