<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_EmailFriend_EmailSignup" CodeBehind="EmailSignup.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="Spacer" TagPrefix="ZNode" %>
<div class="NewsletterSignup">
    <div class="PageTitle">
        <uc1:CustomMessage ID="CustomMessage1" MessageKey="NewsletterSignupTitle" runat="server" ClientIDMode="Static" />
    </div>
    <asp:Panel ID="pnlContact" runat="server" meta:resourcekey="pnlContactResource1" ClientIDMode="Static">
        <p class="FormTitle">
            <uc1:CustomMessage ID="CustomMessage2" MessageKey="NewsletterSignupIntroText" runat="server" ClientIDMode="Static" />
        </p>
        <div class="Form Left">

            <asp:Panel ID="pnlNewsLetterSignup" runat="server" DefaultButton="lnkEmailSignUp" ClientIDMode="Static">

                <div class="Row">
                    <div class="FieldStyle LeftContent">
                        <label class="Req">
                            <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:CommonCaption, FirstName%>" ClientIDMode="Static"></asp:Localize></label>
                    </div>
                    <div class="FieldStyleRight">
                        <asp:TextBox ID="txtFirstname" runat="server" ClientIDMode="Static"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstname" SetFocusOnError="true"
                            CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, FirstNameRequired%>"
                            ToolTip="<%$ Resources:CommonCaption, PhoneNoRequired%>"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="Clear">
                    <ZNode:Spacer ID="Spacer6" EnableViewState="false" SpacerHeight="1" SpacerWidth="10" runat="server" ClientIDMode="Static"></ZNode:Spacer>
                </div>
                <div class="Row">
                    <div class="FieldStyle LeftContent">
                        <label class="ReqNo">
                            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:CommonCaption, LastName%>" ClientIDMode="Static"></asp:Localize></label>
                    </div>
                    <div class="FieldStyleRight">
                        <asp:TextBox ID="txtLastname" runat="server" meta:resourcekey="txtLastnameResource1" ClientIDMode="Static"></asp:TextBox>
                    </div>
                </div>
                <div class="Clear">
                    <ZNode:Spacer ID="Spacer5" EnableViewState="false" SpacerHeight="1" SpacerWidth="10" runat="server" ClientIDMode="Static"></ZNode:Spacer>
                </div>
                <div class="Row">
                    <div class="FieldStyle LeftContent">
                        <label class="Req">
                            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:CommonCaption, EmailID%>" ClientIDMode="Static"></asp:Localize></label>
                    </div>
                    <div class="FieldStyleRight">
                        <asp:TextBox ID="txtEmail" runat="server" meta:resourcekey="txtEmailResource1" ClientIDMode="Static"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmail"
                            CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, EmailAddressRequired%>"
                            ToolTip="<%$ Resources:CommonCaption, EmailAddressRequired%>"></asp:RequiredFieldValidator>

                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
                            CssClass="Error" ErrorMessage="<%$ Resources:CommonCaption, ValidEmailRequired%>"
                            ToolTip="<%$ Resources:CommonCaption, ValidEmailRequired%>" ValidationExpression="<%$ Resources:CommonCaption, EmailValidationExpression%>"
                            Display="Dynamic"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div>
                    <div class="FieldStyle LeftContent TopSpacer">
                        &nbsp;
                    </div>
                    <div class="ButtonsTxt Left">
                        <asp:LinkButton ID="lnkEmailSignUp" runat="server" OnClick="Btnsignup_Click" Text="<%$ Resources:CommonCaption, Submit %>"
                            CssClass="Button" meta:resourcekey="Button1Resource1" ClientIDMode="Static" /><br />
                    </div>
                </div>
            </asp:Panel>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlConfirm" runat="server" Visible="False" meta:resourcekey="pnlConfirmResource1" ClientIDMode="Static">
        <div class="SuccessMsg" runat="server" id="successMsg" enableviewstate="false" visible="false">
            <span class="uxMsg SuccessSection"><em>
                <uc1:CustomMessage ID="CustomMessage3" MessageKey="NewsletterSignupConfirmationIntroText"
                    runat="server" ClientIDMode="Static" /></em>
            </span>
        </div>
        <div class="Error " runat="server" id="errorMsg" enableviewstate="false" visible="false">
            <span class="uxMsg ErrorSection"><em>
                <asp:Label ID="ErrorMessage" runat="server" Visible="False" meta:resourcekey="ErrorMessageResource1" ClientIDMode="Static"></asp:Label>
            </em></span></div>
            <ZNode:Spacer ID="Spacer1" EnableViewState="false"  SpacerHeight="20" SpacerWidth="10" runat="server" ClientIDMode="Static" />
    </asp:Panel>
</div>
