using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the Email Friend user control class.
    /// </summary>
    public partial class Controls_Default_EmailFriend_EmailFriend : System.Web.UI.UserControl
    {
        #region Protected Variables
        private int ProductID = 0;
        private ZNodeProduct _product;
        #endregion

        #region Helper Methods
        /// <summary>
        /// Gets product link
        /// </summary>
        /// <returns></returns>
        public string ProductUrl
        {
            get
            {
                if (this._product != null)
                {
                    return this._product.ViewProductLink;
                }

                return "/product.aspx?zpid=" + this.ProductID;
            }
        }

        /// <summary>
        /// Get product Name
        /// </summary>
        /// <returns>Returns the Product Name</returns>
        public string GetProductName()
        {
            if (this._product != null)
            {
                return this._product.Name;
            }

            return string.Empty;
        }
        #endregion

        #region General Events
        /// <summary>
        /// Email Send Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void But_Send_Click(object sender, EventArgs e)
        {            
            //Zeon custom Code :Start
            SendEmail();
            //Zeon Custom Code: End
        }

        /// <summary>
        /// Back Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Back_Click(object sender, EventArgs e)
        {
            Response.Redirect(this._product.ViewProductLink);
        }

        #endregion

        #region Page Events

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["zpid"] != null)
            {
                this.ProductID = int.Parse(Request.QueryString["zpid"]);
            }
            else
            {
                throw new ApplicationException("Invalid Product Id");
            }

            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this._product = (ZNodeProduct)HttpContext.Current.Items["Product"];
            lblProductName.Text = this.GetProductName();

            if (this.Page.Title != null)
            {
                ZNodeResourceManager resourceManager = new ZNodeResourceManager();
                this.Page.Title = resourceManager.GetLocalResourceObject(this.TemplateControl.AppRelativeVirtualPath, "txtEmailaFriend.Text");
            }
        }
        #endregion

        #region PrivateMethods
        /// <summary>
        /// ESending the Template
        /// </summary>
        private void SendEmail()
        {
            //Zeon Custom Code:Start
            ZeonMessageConfig zeonMessageConfig = new ZeonMessageConfig();
            string defaultPath = string.Empty;
            string productPath = string.Empty;
            string companyPath = string.Empty;
            string productName = string.Empty;
            string companyName = string.Empty;
            //Zeon Custom Code:End
            try
            {
                string domainPath = Request.Url.GetLeftPart(UriPartial.Authority);
                //Zeon Custom Code:Start
                defaultPath = Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ZEmailFriend.html"));
                productPath = domainPath + Response.ApplyAppPathModifier(this.ProductUrl);
                productName = this.GetProductName();
                companyPath = domainPath + Response.ApplyAppPathModifier("default.aspx");
                companyName = ZNodeConfigManager.SiteConfig.CompanyName;

                StreamReader streamReader = new StreamReader(defaultPath);
                string messageText = streamReader.ReadToEnd();


                string spacerImage = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/Themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/Images/space.png";
                int portalId = ZNodeConfigManager.SiteConfig.PortalID;

                ZEmailHelper zEmailHelper = new ZEmailHelper();
                string copyRightText = zeonMessageConfig.GetMessageKey("FooterCopyrightText");

                string messageTextHeader = zEmailHelper.EmailHeaderContent(domainPath, portalId, spacerImage);
                string messageTextFooter = zEmailHelper.EmailFooterContent(domainPath, portalId, copyRightText, spacerImage);

                //To add header to the template
                Regex rxHeaderContent = new Regex("#HeaderContent#", RegexOptions.IgnoreCase);
                messageText = rxHeaderContent.Replace(messageText, messageTextHeader);

                //To add footer to the template
                Regex rxFooterContent = new Regex("#FooterContent#", RegexOptions.IgnoreCase);
                messageText = rxFooterContent.Replace(messageText, messageTextFooter);


                Regex rxProductUrl = new Regex("#ProductUrl#", RegexOptions.IgnoreCase);
                messageText = rxProductUrl.Replace(messageText, productPath);

                Regex rxProductName = new Regex("#ProductName#", RegexOptions.IgnoreCase);
                messageText = rxProductName.Replace(messageText, productName);

                Regex rxCompanyURL = new Regex("#CompanyURL#", RegexOptions.IgnoreCase);
                messageText = rxCompanyURL.Replace(messageText, companyPath);

                Regex rxCompanyName = new Regex("#CompanyName#", RegexOptions.IgnoreCase);
                messageText = rxCompanyName.Replace(messageText, companyName);
               
                ZNodeEmail.SendEmail(Email.Text.Trim(), FromEmailID.Text.Trim(), string.Empty,zeonMessageConfig.GetMessageKey("ProductLinkSubject"), messageText, true);

                //Zeon Custom Code:End
                pnlConfirm.Visible = true;
                pnlEmailFriend.Visible = false;
                lblMessage.Text = this.GetLocalResourceObject("MailSent").ToString();
                lblMessage.CssClass = "SuccessMsg";
            }
            catch (Exception ex)
            {
                pnlConfirm.Visible = true;
                lblMessage.Text = this.GetLocalResourceObject("MailSendFailed").ToString();
                lblMessage.CssClass = "Error";
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "Email Friend  template sending problem(Mehtod name: SendEmail())=> EmailFriend.ascx" + ex.StackTrace);
            }
        }

        #endregion
    }
}