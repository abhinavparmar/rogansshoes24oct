<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_EmailFriend_EmailFriend" CodeBehind="EmailFriend.ascx.cs" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="Spacer" TagPrefix="ZNode" %>
<div class="PageTitle">
    <asp:Localize ID="Localize4" runat="server" meta:resourceKey="txtEmailaFriend" ClientIDMode="Static"></asp:Localize>
</div>
<div class="Form EmailFriend">
    <asp:Panel ID="pnlEmailFriend" runat="server" meta:resourcekey="pnlEmailFriendResource1" DefaultButton="but_Send" ClientIDMode="Static" CssClass="CommonLableNone">
        <div class="Form">
            <div class="Row">
                <div class="FieldStyle LeftContent">
                    <asp:Localize ID="Localize3" runat="server" meta:resourceKey="txtProduct">:</asp:Localize>
                </div>
                <div class="Input-box">
                    <asp:Label ID="lblProductName" EnableViewState="false" runat="server" ClientIDMode="Static"></asp:Label>
                </div>
            </div>
            <div class="Clear">
                <ZNode:Spacer ID="Spacer6" EnableViewState="false" SpacerHeight="1" SpacerWidth="10" runat="server" ClientIDMode="Static"></ZNode:Spacer>
            </div>

            <div class="Row">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="FromEmailLabel" runat="server" AssociatedControlID="FromEmailID" meta:resourceKey="txtYourEmail" ClientIDMode="Static">
                    :</asp:Label>
                </div>
                <div class="Input-box">
                    <asp:TextBox ID="FromEmailID" runat="server" ClientIDMode="Static" placeholder="Your Email *"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="FromEmailID"
                        ErrorMessage="<%$ Resources:CommonCaption, EmailAddressRequired%>" ToolTip="<%$ Resources:CommonCaption, EmailAddressRequired%>"
                        CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="FromEmailID"
                        CssClass="Error" ErrorMessage="<%$ Resources:CommonCaption, ValidEmailRequired%>"
                        ToolTip="<%$ Resources:CommonCaption, ValidEmailRequired%>" ValidationExpression="<%$ Resources:CommonCaption, EmailValidationExpression%>"
                        Display="Dynamic"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="Clear">
                <ZNode:Spacer ID="Spacer3" EnableViewState="false" SpacerHeight="1" SpacerWidth="10" runat="server" ClientIDMode="Static"></ZNode:Spacer>
            </div>

            <div class="Row">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email" meta:resourceKey="txtFriendsEmail" ClientIDMode="Static">:</asp:Label>
                </div>
                <div class="Input-box">
                    <asp:TextBox ID="Email" runat="server" meta:resourceKey="EmailResource1" ClientIDMode="Static" placeholder="Friend's Email *"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="Email"
                        ErrorMessage="<%$ Resources:CommonCaption, EmailAddressRequired%>" ToolTip="<%$ Resources:CommonCaption, EmailAddressRequired%>"
                        CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="Email"
                        CssClass="Error" ErrorMessage="<%$ Resources:CommonCaption, ValidEmailRequired%>"
                        ToolTip="<%$ Resources:CommonCaption, ValidEmailRequired%>" ValidationExpression="<%$ Resources:CommonCaption, EmailValidationExpression%>"
                        Display="Dynamic"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="Row">
                <div class="FieldStyle LeftContent No-Text">
                    &nbsp;
                </div>
                <div class="Input-box">
                    <asp:LinkButton ID="but_Send" runat="server" CssClass="Button View" OnClick="But_Send_Click"
                        Text="Send" meta:resourceKey="but_SendResource1" ClientIDMode="Static" Style="color:white;"/>
                </div>
            </div>
        </div>

    </asp:Panel>
    <asp:Panel ID="pnlConfirm" runat="server" Visible="False" meta:resourcekey="pnlConfirmResource1" ClientIDMode="Static">
        <div class="SuccessMsg">
            <span class="uxMsg SuccessSection"><em>
                <asp:Label ID="lblMessage" runat="server" ClientIDMode="Static"></asp:Label>
           </em> </span>
        </div>
        <ZNode:Spacer ID="Spacer2" EnableViewState="false" SpacerHeight="20" SpacerWidth="10"
            runat="server" ClientIDMode="Static" />
       
        <asp:LinkButton ID="BackLink" CssClass="BackLink" runat="server" OnClick="Back_Click"
            CausesValidation="False" meta:resourceKey="txtBackToProduct" ClientIDMode="Static">
            <ZNode:Spacer ID="Spacer1" SpacerHeight="10" EnableViewState="false" SpacerWidth="10"
                runat="server" ClientIDMode="Static" />
        </asp:LinkButton>
    </asp:Panel>
</div>
<ZNode:Spacer ID="Spacer" SpacerHeight="1" EnableViewState="false" SpacerWidth="10"
    runat="server" ClientIDMode="Static" />
