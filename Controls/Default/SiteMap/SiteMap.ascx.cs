using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the SiteMap user control class.
    /// </summary>
    public partial class Controls_Default_SiteMap_SiteMap : System.Web.UI.UserControl
    {
        #region Page Events

        /// <summary>
        /// Page Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            ZNodeResourceManager resourceManager = new ZNodeResourceManager();

            // Set SEO Properties
            ZNodeSEO seo = new ZNodeSEO();
            //seo.SEOTitle = resourceManager.GetGlobalResourceObject("SiteMap", "txtSiteMap");
            //PRFT Custom Code : Start
            seo.SEOTitle = resourceManager.GetGlobalResourceObject("SiteMap", "txtSiteMap");
            seo.SEODescription = resourceManager.GetGlobalResourceObject("SiteMap", "SiteMapDescription").ToString();
            //PRFT Custom Code : End
            if (!IsPostBack)
            {
                CreateCategoryTree();
               // BindAllBrandAlphabetical();
            }

        }

        #endregion

        #region Zeon Custom Methods

        #region Protected Methods

        /// <summary>
        /// Get Title Case of the Menu Name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        protected string GetTitleCaseName(string name)
        {
            string titleCaseName = name;
            name = name.ToLower();
            TextInfo info = CultureInfo.CurrentCulture.TextInfo;
            titleCaseName = info.ToTitleCase(Server.HtmlDecode(name));
            return titleCaseName;
        }

        /// <summary>
        /// Repeater Item Data Bound Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void menuRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType.Equals(ListItemType.AlternatingItem) || e.Item.ItemType.Equals(ListItemType.Item))
            {
                DataRowView dbRowView = e.Item.DataItem as DataRowView;

                if (null == dbRowView) return;

                DataRow dbRow = dbRowView.Row;

                if (dbRow.IsNull("ParentCategoryNodeID"))
                {
                    if ((bool)dbRow["VisibleInd"])
                    {
                        Repeater rptLevel1Menu = e.Item.FindControl("subMenuRepeater") as Repeater;
                        if (null != rptLevel1Menu && dbRow.GetChildRows("NodeRelation").Length > 0)
                        {
                            rptLevel1Menu.Visible = true;
                            rptLevel1Menu.DataSource = dbRow.GetChildRows("NodeRelation");
                            rptLevel1Menu.DataBind();
                        }
                    }
                    else
                    {
                        e.Item.Visible = false;
                    }
                }
                else
                {
                    e.Item.Visible = false;
                }

            }
        }

        /// <summary>
        /// Repeater Item Data Bound Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void subMenuRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType.Equals(ListItemType.AlternatingItem) || e.Item.ItemType.Equals(ListItemType.Item))
            {
                DataRow dbRow = e.Item.DataItem as DataRow;

                if (null == dbRow) return;

                if ((bool)dbRow["VisibleInd"])
                {
                    string categoryId = dbRow["CategoryId"].ToString();
                    string seoURL = dbRow["SEOURL"].ToString();

                    seoURL = ZNodeSEOUrl.MakeURL(categoryId, SEOUrlType.Category, seoURL);
                    seoURL = seoURL.Replace("~/", "/");
                    HtmlAnchor aSub = e.Item.FindControl("a") as HtmlAnchor;
                    if (null != aSub)
                    {
                        TextInfo info = CultureInfo.CurrentCulture.TextInfo;

                        aSub.HRef = (string.IsNullOrEmpty(seoURL)) ? "#" : seoURL;
                        //string name = info.ToTitleCase(Server.HtmlDecode(dbRow["Title"].ToString()));
                        string name = info.ToTitleCase(Server.HtmlDecode(dbRow["Name"].ToString()));
                        aSub.InnerText = aSub.Title = name;
                    }
                    if (!string.IsNullOrEmpty(dbRow["ParentCategoryNodeID"].ToString()))
                    {
                        Repeater rptLevel2Menu = e.Item.FindControl("subMenuSubRepeater") as Repeater;
                        if (null != rptLevel2Menu && dbRow.GetChildRows("NodeRelation").Length > 0)
                        {
                            rptLevel2Menu.Visible = true;
                            rptLevel2Menu.DataSource = dbRow.GetChildRows("NodeRelation");
                            rptLevel2Menu.DataBind();
                            aSub.Attributes.Add("class", "SubCategoryMenu");
                        }
                    }
                }
                else
                {
                    e.Item.Visible = false;
                }
            }
        }

        /// <summary>
        /// Repeater Item Data Bound Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void subMenuSubRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType.Equals(ListItemType.AlternatingItem) || e.Item.ItemType.Equals(ListItemType.Item))
            {
                DataRow dbRow = e.Item.DataItem as DataRow;

                if (null == dbRow) return;

                if ((bool)dbRow["VisibleInd"])
                {
                    string categoryId = dbRow["CategoryId"].ToString();
                    string seoURL = dbRow["SEOURL"].ToString();

                    seoURL = ZNodeSEOUrl.MakeURL(categoryId, SEOUrlType.Category, seoURL);
                    seoURL = seoURL.Replace("~/", "/");
                    HtmlAnchor aSub = e.Item.FindControl("a") as HtmlAnchor;
                    if (null != aSub)
                    {
                        TextInfo info = CultureInfo.CurrentCulture.TextInfo;

                        aSub.HRef = (string.IsNullOrEmpty(seoURL)) ? "#" : seoURL;
                        //string name = info.ToTitleCase(Server.HtmlDecode(dbRow["Title"].ToString()));
                        string name = info.ToTitleCase(Server.HtmlDecode(dbRow["Name"].ToString()));
                        aSub.InnerText = aSub.Title = name;
                    }
                }
                else
                {
                    e.Item.Visible = false;
                }
            }
        }

        /// <summary>
        /// get brand URL
        /// </summary>
        /// <param name="brandID"></param>
        /// <returns></returns>
        protected string GetBrandURL(object brandID, object seoURL)
        {
            string currentId = brandID != null ? brandID.ToString() : string.Empty;
            string currentUrl = seoURL != null ? seoURL.ToString() : string.Empty;
            string defaultURL = "~/brand.aspx?mid=";
            if (!string.IsNullOrEmpty(currentUrl))
            {
                return currentUrl.ToLower();
            }
            else
            {
                return ResolveUrl(defaultURL + currentId).ToLower();
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Create Tree Menu
        /// </summary>
        private void CreateCategoryTree()
        {
            System.Data.DataSet ds = (System.Data.DataSet)HttpContext.Current.Cache["CategoryNavigationSitemap" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId];

            if (ds == null) 
            {
                CategoryHelper categoryHelper = new CategoryHelper();
                ds = categoryHelper.GetSitemapCategoryList(ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId, ZNodeProfile.CurrentUserProfileId, ConfigurationManager.AppSettings["HideCategoriesFromMenu"].ToString() + "," + ConfigurationManager.AppSettings["ShopByBrandCategory"].ToString());

                ds.Tables[0].Constraints.Add("PK_CategoryNodeID_FK_ParentCategoryNodeID", ds.Tables[0].Columns["CategoryNodeId"], ds.Tables[0].Columns["ParentCategoryNodeID"]);

                // add the hierarchical relationship to the dataset
                ds.Relations.Add("NodeRelation", ds.Tables[0].Columns["CategoryNodeId"], ds.Tables[0].Columns["ParentCategoryNodeId"]);

                // Create two sql Cache Dependency objects for categorynode and product table
                System.Web.Caching.AggregateCacheDependency aggregateDependency = new System.Web.Caching.AggregateCacheDependency();
                aggregateDependency.Add(new System.Web.Caching.SqlCacheDependency("ZNodeMultifront", "ZNodeCategoryNode"));
                aggregateDependency.Add(new System.Web.Caching.SqlCacheDependency("ZNodeMultifront", "ZNodePortalCatalog"));
                aggregateDependency.Add(new System.Web.Caching.SqlCacheDependency("ZNodeMultifront", "ZNodeProductCategory"));
                aggregateDependency.Add(new System.Web.Caching.SqlCacheDependency("ZNodeMultifront", "ZNodeCategory"));
                aggregateDependency.Add(new System.Web.Caching.SqlCacheDependency("ZNodeMultifront", "ZNodeProduct"));

                System.Web.HttpContext.Current.Cache.Insert("CategoryNavigationSitemap" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId, ds, aggregateDependency, System.Web.Caching.Cache.NoAbsoluteExpiration,
                          System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.NotRemovable, null);
            }

            menuRepeater.DataSource = ds;
            menuRepeater.DataBind();
        }

        /// <summary>
        /// Bind all brands alphabetically 
        /// </summary>
        private void BindAllBrandAlphabetical()
        {
            DataTable dtManufacturer = (DataTable)System.Web.HttpRuntime.Cache["AllBrands" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId + HttpContext.Current.Session.SessionID];

            if (dtManufacturer == null)
            {
                ManufacturerHelper manuHelper = new ManufacturerHelper();
                dtManufacturer = manuHelper.GetActiveBrandsByPortal(ZNodeConfigManager.SiteConfig.PortalID, ZNodeConfigManager.SiteConfig.LocaleID, 0);
                //Insert into cache
                ZNodeCacheDependencyManager.Insert("AllBrands" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId
                                             + HttpContext.Current.Session.SessionID,
                                             dtManufacturer,
                                             DateTime.Now.AddHours(1),
                                             System.Web.Caching.Cache.NoSlidingExpiration,
                                             null);
            }

            if (dtManufacturer != null)
            {
                //rptBrandList.DataSource = dtManufacturer;
                //rptBrandList.DataBind();
            }
             

        }

        #endregion

        #endregion
    }
}