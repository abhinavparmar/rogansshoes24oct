<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_SiteMap_SiteMap" CodeBehind="SiteMap.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="ZNode" %>
<%@ Import Namespace="ZNode.Libraries.Framework.Business" %>


<div class="SiteMap">
    <div class="PageTitle">
        <h1><asp:Localize ID="Localize1" Text="<%$ Resources:SiteMap, txtSiteMap %>" runat="server" /></h1>
    </div>
    <div id="LeftColumn">
        <div class="ShoppingCartNavigation">
            <asp:TreeView ID="uxTreeView" runat="server" DataSourceID="SiteMapDataSource1"
                NodeIndent="0" ShowExpandCollapse="False"
                meta:resourcekey="uxTreeViewResource1">
                <RootNodeStyle CssClass="Title" />
                <ParentNodeStyle CssClass="Title" />
                <HoverNodeStyle CssClass="HoverNodeStyle" />
                <SelectedNodeStyle CssClass="SelectedNodeStyle" HorizontalPadding="0px" VerticalPadding="0px" />
                <LeafNodeStyle CssClass="LeafNodeStyle" />
                <NodeStyle CssClass="NodeStyle" />
            </asp:TreeView>
            <asp:SiteMapDataSource ID="SiteMapDataSource1" runat="server" />
        </div>
    </div>
    <div id="MiddleColumn">
        <div class="CatalogMap">
            <div class="SiteMapNav">
                <div class="Title">
                    <asp:Localize ID="locCategoryMap" EnableViewState="false" Text="<%$ Resources:SiteMap, txtCatalogMap %>" runat="server" />
                </div>
                <div class="CategoryNavigation">
                    <asp:Repeater ID="menuRepeater" runat="server" OnItemDataBound="menuRepeater_ItemDataBound" EnableViewState="false">
                        <ItemTemplate>
                            <li class="dropdown">
                                <a title='<%# Server.HtmlDecode(GetTitleCaseName(Eval("Name").ToString()))%>'
                                    class="dropdown-toggle" href='<%#ResolveUrl(ZNode.Libraries.ECommerce.SEO.ZNodeSEOUrl.MakeURL(Eval("CategoryId").ToString(),  SEOUrlType.Category,Eval("SEOURL").ToString())) %>'>
                                    <%# Server.HtmlDecode(GetTitleCaseName(Eval("Name").ToString()))%>                            
                                </a>
                                <asp:Repeater ID="subMenuRepeater" OnItemDataBound="subMenuRepeater_ItemDataBound" Visible="false"
                                    runat="server">
                                    <HeaderTemplate>
                                        <ul>
                                            <li>
                                                <div>
                                                    <div class="row">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <ul>
                                            <li>
                                                <a id="a" runat="server"></a>
                                                <span></span>
                                                <asp:Repeater ID="subMenuSubRepeater" runat="server" OnItemDataBound="subMenuSubRepeater_ItemDataBound" Visible="false">
                                                    <HeaderTemplate>
                                                        <ul class="CatLevelMenu">
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <li>
                                                            <a id="a" runat="server" class="LastCategory"></a>
                                                        </li>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </ul>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </li>
                                        </ul>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </div>
                                            </div>
                                        </li>
                                    </ul>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <%-- <div class="SiteMapNav" style="display:none;">--%>
            <%--<div class="Title">
                    <ZNode:CustomMessage ID="ucBrandListTitle" runat="server" MessageKey="SitemapBrandTitle" />
                </div>--%>
            <%-- <div class="CategoryNavigation">
                    <ul>
                        <asp:Repeater ID="rptBrandList" runat="server">
                            <ItemTemplate>
                                <li>
                                    <a href='<%# GetBrandURL(DataBinder.Eval(Container.DataItem, "ManufacturerID"),DataBinder.Eval(Container.DataItem, "SEOURL"))%>'>
                                        <asp:Localize ID="locBrandName" Text='<%#DataBinder.Eval(Container.DataItem, "Name")%>' runat="server"></asp:Localize></a>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>--%>
            <%--</div>--%>
        </div>
    </div>
</div>
