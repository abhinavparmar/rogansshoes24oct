using System;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using WebApp.Controls.Default.Home;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the Store Locator user control class
    /// </summary>
    public partial class Controls_Default_StoreLocator_StoreLocator : System.Web.UI.UserControl
    {
        #region Constant Variables

        private const string ProductPageLink = "~/Product.aspx";

        #endregion

        #region Private Variables
        private static int TotalRecords = 0;
        private static int RecCount = 0;
        private static int Currentpage = 0;
        private string _noRecordsText = string.Empty;
        private int CurrentPage = 0;
        private ZNodeImage znodeImage = new ZNodeImage();
        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Search_Click(object sender, EventArgs e)
        {
            this.BindData();
        }

        /// <summary>
        /// Clear button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Clear_Click(object sender, EventArgs e)
        {
            txtZipcode.Text = string.Empty;
            ddlRadiusinMiles.SelectedIndex = 0;
            lblMsg.Text = string.Empty;
            lblMsgError.Text = string.Empty;
            //txtCity.Text = string.Empty;
            //txtState.Text = string.Empty;
            // txtAreaCode.Text = string.Empty;
            PnlStorelist.Visible = false;
            txtStoreFoundMessage.Text = string.Empty;
             //txtLinkStore.Text =
        }
        #endregion

        #region Page Events
        /// <summary>
        /// Page Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            ZNodeResourceManager resourceManager = new ZNodeResourceManager();

            // Set SEO Properties
            ZNodeSEO seo = new ZNodeSEO();
            //PRFT Custom Code : Start
            seo.SEOTitle = resourceManager.GetLocalResourceObject(txtStoreLocatorTitle.TemplateControl.AppRelativeVirtualPath, "txtStoreLocatorTitle.Text")+" | "+GetLocalResourceObject("StoreName").ToString();
            seo.SEODescription = GetLocalResourceObject("StoreLocatorDescription").ToString();
            //PRFT Custom Code : End
           
        }

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ZCommonHelper.IsCustomMulitipleAttributePortal())
            {
                if (!IsPostBack)
                {
                    lblMsg.Text = string.Empty;
                    lblStoreName.Text = string.Empty;
                    lblMsgError.Text = string.Empty;
                    txtZipcode.Focus();
                    PnlStorelist.Visible = false;
                }
                SetStoreName(GetCookie());
            }
            else
            {
                Response.Redirect("~/default.aspx");
            }
        }

        #endregion

        #region Bind
        /// <summary>
        /// Bind Search stores
        /// </summary>
        private void BindData()
        {
            string Zipcode = string.Empty;
            int InRadius = 0;
            string StateAbbr = string.Empty;
            string City = string.Empty;
            string Areacode = string.Empty;
            bool IsValue = false;
            errorMsg.Visible = false;
            //successMsg.Visible = false;

            Zipcode = txtZipcode.Text;
            //if (ddlRadiusinMiles.SelectedIndex != 0)
            //{
            //    InRadius = Convert.ToInt32(ddlRadiusinMiles.SelectedItem.Value);
            //}

            InRadius = Convert.ToInt32(ddlRadiusinMiles.SelectedItem.Value);
            City = string.Empty;//txtCity.Text;
            StateAbbr = string.Empty;// ddlState.SelectedValue;  //txtState.Text;

            // Areacode = txtAreaCode.Text;

            // Set Validation for Miles calculation.
            if (ddlRadiusinMiles.SelectedItem.Value != "0")
            {
                if (txtZipcode.Text.Length != 0)
                {
                    IsValue = true;
                }
                else
                {
                    IsValue = false;
                }
            }
            else
            {
                IsValue = true;
            }

            if (true)
            {
                ZNodeStoreList _store = new ZNodeStoreList();

                int count = _store.GetZipCodeCount();

                if (count > 0)
                {
                    ZNodeStoreList storesList = ZNodeStoreList.SearchByZipCodeAndRadius(Zipcode, InRadius, City, StateAbbr, Areacode, ZNodeConfigManager.SiteConfig.PortalID);

                    if (storesList.StoreCollection.Count == 0)
                    {
                        lblMsgError.Text = this.GetLocalResourceObject("NotFound").ToString();
                        errorMsg.Visible = true;
                        PnlStorelist.Visible = false;
                    }
                    else
                    {
                        string milesFormat = InRadius > 0 ? string.Format(this.GetLocalResourceObject("MilesFormat").ToString(), InRadius) : string.Empty;
                        string zipFormat = !string.IsNullOrEmpty(Zipcode) ? string.Format(this.GetLocalResourceObject("zipFormat").ToString(), Zipcode) : string.Empty;
                        string cityFormat = !string.IsNullOrEmpty(City) ? string.Format(this.GetLocalResourceObject("CityFormat").ToString(), City) : string.Empty;
                        string stateFormat = !string.IsNullOrEmpty(StateAbbr) ? string.Format(this.GetLocalResourceObject("StateFormat").ToString(), StateAbbr) : string.Empty;
                        if (!string.IsNullOrEmpty(Zipcode))
                        {
                            lblMsg.Text = string.Format(this.GetLocalResourceObject("StoreSuccessMessage").ToString(), storesList.StoreCollection.Count.ToString(), milesFormat + zipFormat);
                        }
                        else
                        {
                            lblMsg.Text = string.Format(this.GetLocalResourceObject("StoreSuccessMessage").ToString(), storesList.StoreCollection.Count.ToString(), cityFormat + stateFormat);
                        }
                        txtStoreFoundMessage.Text =  string.Format(this.GetLocalResourceObject("txtStoreFoundMessage").ToString(), storesList.StoreCollection.Count.ToString());
                        //txtLinkStore.Text =
                        // Set Dataset object to Viewstate
                        ViewState["StoreList"] = storesList;

                        // Set initial page value
                        ViewState["CurrentPage"] = 0;
                        this.BindDataListPagedDataSource();
                        PnlStorelist.Visible = true;
                        //successMsg.Visible = true;
                    }
                }
                else
                {
                    lblMsgError.Text = this.GetLocalResourceObject("ZipCodeNotUploaded").ToString();
                    errorMsg.Visible = true;

                }
            }
            else
            {
                lblMsgError.Text = this.GetLocalResourceObject("ZipCodeRequired").ToString();
                errorMsg.Visible = true;
                PnlStorelist.Visible = false;
            }
        }

        #endregion

        #region Bind Paging Methods

        private void BindDataListPagedDataSource()
        {
            // Declare dataset
            ZNodeStoreList StoreList = new ZNodeStoreList();

            // Retrieve dataset from Viewstate
            if (ViewState["StoreList"] != null)
            {
                StoreList = (ZNodeStoreList)ViewState["StoreList"];
            }

            // Creating an object for the 'PagedDataSource' for holding the data.
            PagedDataSource objPage = new PagedDataSource();

            // Assigning the datasource to the 'objPage' object.
            objPage.DataSource = StoreList.StoreCollection;

            // Enable paging
            objPage.AllowPaging = true;

            // Set Paging Size.
            objPage.PageSize = 10;

            // "CurrentPage" is public static variable to hold the current page index value declared in the global section.
            objPage.CurrentPageIndex = int.Parse(ViewState["CurrentPage"].ToString());

            // Checking for enabling/disabling next/prev buttons.
            // Next/prev buton will be disabled when is the last/first page of the pageobject.
            BotNextLink.Enabled = !objPage.IsLastPage;
            TopNextLink.Enabled = !objPage.IsLastPage;
            TopPrevLink.Enabled = !objPage.IsFirstPage;
            BotPrevLink.Enabled = !objPage.IsFirstPage;

            // Set current page index
            Currentpage = objPage.CurrentPageIndex + 1;
            RecCount = objPage.PageCount;
            TotalRecords = objPage.DataSourceCount;

            // Assigning Datasource to the DataList.
            DataListStores.DataSource = objPage;
            DataListStores.DataBind();

            this.DisplayCount();
        }

        /// <summary>
        /// Previous Link Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void PrevRecord(object sender, EventArgs e)
        {
            this.CurrentPage = int.Parse(ViewState["CurrentPage"].ToString());
            ViewState["CurrentPage"] = this.CurrentPage -= 1;

            this.BindDataListPagedDataSource();
            PnlStorelist.Visible = true;
            this.DisplayCount();
        }

        /// <summary>
        /// Next Link Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void NextRecord(object sender, EventArgs e)
        {
            this.CurrentPage = int.Parse(ViewState["CurrentPage"].ToString());
            ViewState["CurrentPage"] = this.CurrentPage += 1;

            this.BindDataListPagedDataSource();
            PnlStorelist.Visible = true;
            this.DisplayCount();
        }

        /// <summary>
        /// Get http image path.
        /// </summary>
        /// <param name="imageFileName">Image file name.</param>
        /// <returns>Returns the image file path with name.</returns>
        protected string GetImagePath(object imageFileName)
        {
            if (imageFileName != null && imageFileName.ToString().Length > 0)
            {
                ZNodeImage znodeImage = new ZNodeImage();
                return znodeImage.GetImageHttpPathSmall(imageFileName.ToString());
            }
            else
                return string.Empty;
        }

        /// <summary>
        /// Checks the imagefile exists or not
        /// </summary>
        /// <param name="Imagefile">Image File</param>
        /// <returns>Returns true if Image Exists otherwise false</returns>
        protected bool IsImageExists(object Imagefile)
        {
            if (Imagefile == null)
            {
                return false;
            }

            return true;
        }

        private void DisplayCount()
        {
            lblTopPaging.Text = string.Format("{0} {1} {2} (Total {3} {4})", Currentpage, Resources.CommonCaption.of, RecCount, TotalRecords, Resources.CommonCaption.Item);
            lblBottomPaging.Text = lblTopPaging.Text;
        }

        #endregion

        #region Zeon Custom Code


        /// <summary>
        /// Get the cookies for the store locator
        /// </summary>
        /// <returns>Returns the HttpCookie</returns>
        private HttpCookie GetCookie()
        {
            HttpCookie storeCookie = null;
            int accID = ZNodeUserAccount.CurrentAccount() == null ? 0 : ZNodeUserAccount.CurrentAccount().AccountID;

            if (accID > 0)
            {
                string cookieName = Convert.ToString(accID) + "_StoreCookie";
                if (Request.Cookies[cookieName] != null)
                {
                    storeCookie = Request.Cookies[cookieName];
                }
            }
            else
            {
                if (Request.Cookies["StoreCookie"] != null)
                {
                    storeCookie = Request.Cookies["StoreCookie"];
                }
            }

            return storeCookie;
        }


        /// <summary>
        /// To Set Tooltip to control
        /// </summary>
        /// <param name="storeCookie"></param>
        private void SetToolTipOfStoreLocator(HttpCookie storeCookie)
        {
            ZHeader zheader = (ZHeader)this.Page.Master.Master.FindControl("uxZHeader");
            if (zheader != null)
            {
                ZMyStore zMyStore = (ZMyStore)zheader.FindControl("uxMyStore");
                if (zMyStore != null)
                {
                    HyperLink zhlnkStoreLocator = zMyStore.FindControl("hlnkStoreLocator") as HyperLink;
                    if (zhlnkStoreLocator != null)
                    {
                        zhlnkStoreLocator.ToolTip = "Store Name: " + HttpContext.Current.Server.UrlDecode(storeCookie["StoreCookieName"].ToString()) + "\nPhone Number: " + HttpContext.Current.Server.UrlDecode(storeCookie["StoreCookiePhone"].ToString()) + "\nZip Code: " + HttpContext.Current.Server.UrlDecode(storeCookie["StoreCookieZipCode"].ToString());
                    }
                }
            }
        }

        /// <summary>
        /// Item command event of DataList
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void DataListStores_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName.Equals("SetStore"))
            {
                HttpCookie storeCookie = null;
                int accID = ZNodeUserAccount.CurrentAccount() == null ? 0 : ZNodeUserAccount.CurrentAccount().AccountID;
                string cookieName = accID > 0 ? Convert.ToString(accID) + "_StoreCookie" : "StoreCookie";
                storeCookie = Request.Cookies[cookieName] != null ? Request.Cookies[cookieName] : new HttpCookie(cookieName);

                if (storeCookie != null)
                {
                    storeCookie["StoreCookieID"] = HttpContext.Current.Server.UrlEncode(e.CommandArgument.ToString().Split('|')[0]);
                    storeCookie["StoreCookieName"] = HttpContext.Current.Server.UrlEncode(e.CommandArgument.ToString().Split('|')[1]);
                    storeCookie["StoreCookieAddress1"] = HttpContext.Current.Server.UrlEncode(e.CommandArgument.ToString().Split('|')[2]); // +HttpContext.Current.Server.UrlEncode(e.CommandArgument.ToString().Split('|')[3]) + HttpContext.Current.Server.UrlEncode(e.CommandArgument.ToString().Split('|')[4]) + HttpContext.Current.Server.UrlEncode(e.CommandArgument.ToString().Split('|')[5]);
                    storeCookie["StoreCookieCity"] = HttpContext.Current.Server.UrlEncode(e.CommandArgument.ToString().Split('|')[3]);
                    storeCookie["StoreCookieState"] = HttpContext.Current.Server.UrlEncode(e.CommandArgument.ToString().Split('|')[4]);
                    storeCookie["StoreCookiePhone"] = HttpContext.Current.Server.UrlEncode(e.CommandArgument.ToString().Split('|')[5]);
                    storeCookie["StoreCookieZipCode"] = HttpContext.Current.Server.UrlEncode(e.CommandArgument.ToString().Split('|')[6]);

                    storeCookie.Expires = DateTime.MaxValue;
                    Response.Cookies.Add(storeCookie);

                    if (storeCookie != null && !string.IsNullOrEmpty(storeCookie.Value))
                    {
                        SetToolTipOfStoreLocator(storeCookie);
                        SetStoreName(storeCookie);
                    }

                    lblMsg.Text = this.GetLocalResourceObject("StoreSelectedSuccessfully").ToString();

                    if (Request.QueryString["zpid"] != null)
                    {
                        Response.Redirect(ProductPageLink + "?zpid=" + Request.QueryString["zpid"]);
                    }
                    else if (Request.QueryString["seourl"] != null)
                    {
                        Response.Redirect(Request.QueryString["seourl"]);
                    }
                }
            }
        }

        /// <summary>
        /// To set store name
        /// </summary>
        /// <param name="storeCookie"></param>
        private void SetStoreName(HttpCookie storeCookie)
        {
            if (storeCookie != null && !string.IsNullOrEmpty(storeCookie.Value))
            {
                lblStoreName.Text = "<span class='title'>" + HttpContext.Current.Server.UrlDecode(storeCookie["StoreCookieName"].ToString()) + "</span> <span>" + HttpContext.Current.Server.UrlDecode(storeCookie["StoreCookieAddress1"]) + "</span><span>" + HttpContext.Current.Server.UrlDecode(storeCookie["StoreCookieCity"]) + ", " + HttpContext.Current.Server.UrlDecode(storeCookie["StoreCookieState"]) + "-" + HttpContext.Current.Server.UrlDecode(storeCookie["StoreCookieZipCode"].ToString()) + "</span><b>Phone: </b>" + HttpContext.Current.Server.UrlDecode(storeCookie["StoreCookiePhone"].ToString());
                //lblStoreTitile.Visible = this.Page.User.Identity.IsAuthenticated;
                lblStoreTitile.Visible = true;
            }
            //lblStoreName.Visible = this.Page.User.Identity.IsAuthenticated;
        }

        protected string GetGoogleMapURL(string querystring)
        {
            string url = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            url = url + "/googlemaps.aspx";

            url = url + "?" + querystring.Replace("'", string.Empty);

            return url;

        }

        protected string GetStaticUrl(string url)
        {
            if (!string.IsNullOrEmpty(url))
            {
                return url;
            }
            else
            {
                return "javascript:void(0)";
            }
        }
        #endregion
    }
}