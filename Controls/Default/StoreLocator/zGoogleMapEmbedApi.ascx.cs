﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.Controls.Default.StoreLocator
{
    public partial class zGoogleMapEmbedApi : System.Web.UI.UserControl
    {

        #region[Private Variable]
        string baseURL = "https://www.google.com/maps/embed/v1/place";
        #endregion

        #region[Public Property]

        public string Address
        {
            get
            {
                string address = string.Empty;
                if (Request["address"] != null && !string.IsNullOrEmpty(Request["address"].ToString()))
                {
                    address = Request["address"].ToString();
                }

                return address;
            }
        }


        public string City
        {
            get
            {
                string city = string.Empty;
                if (Request["city"] != null && !string.IsNullOrEmpty(Request["city"].ToString()))
                {
                    city = Request["city"].ToString();
                }

                return city;
            }
        }


        public string State
        {
            get
            {
                string state = string.Empty;
                if (Request["state"] != null && !string.IsNullOrEmpty(Request["state"].ToString()))
                {
                    state = Request["city"].ToString();
                }

                return state;
            }
        }

        public string Zipcode
        {
            get
            {
                string zipcode = string.Empty;
                if (Request["zipcode"] != null && !string.IsNullOrEmpty(Request["zipcode"].ToString()))
                {
                    zipcode = Request["zipcode"].ToString();
                }

                return zipcode;
            }
        }

        public string StoreName
        {
            get
            {
                string storename = string.Empty;
                if (Request["storename"] != null && !string.IsNullOrEmpty(Request["storename"].ToString()))
                {
                    storename = Request["storename"].ToString();
                }

                return storename;

            }
        }
        #endregion

        #region Page Event

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #endregion

        #region [Methods]

        protected string GetGoogleMapSrc()
        {
            string API_KEY =  System.Configuration.ConfigurationManager.AppSettings["GoogleMapApiKey"].ToString();
            string storeName = ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.StoreName;
            string queryParameter = storeName + "+" + Address;

            if (!string.IsNullOrEmpty(State))
            {
                queryParameter = queryParameter + "+" + State;
            }

            if (!string.IsNullOrEmpty(City))
            {
                queryParameter = queryParameter + "+" + City;
            }

            if (!string.IsNullOrEmpty(Zipcode))
            {
                queryParameter = queryParameter + "+" + Zipcode;
            }

            string googleMapURL = baseURL + "?" + "key=" + API_KEY + "&q=" + queryParameter;

            //ifGoogleMap.Attributes.Add("src", googleMapURL);

            return googleMapURL;

        }

        #endregion
    }
}