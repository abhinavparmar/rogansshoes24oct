<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_StoreLocator_StoreLocator"
    CodeBehind="StoreLocator.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="uc1" %>
<div class="StoreLocator">
    <%--<div id="BreadCrumb">
        <div class="BreadCrumbLabel">
            <span id="lblPath">
                <asp:Localize ID="txtStoreLocatorLnk" meta:resourceKey="StoreLocatorTitle" runat="server" />
                <asp:Localize ID="txtLinkStore" runat="server"></asp:Localize>
            </span>
        </div>
    </div>--%>
    <div class="PageTitle">
        <h1>
        <asp:Localize ID="txtStoreLocatorTitle" meta:resourceKey="txtStoreLocatorTitle" runat="server" EnableViewState="false" /><span class="fa fa-map-marker"></span></h1>
        <span>
            <asp:Localize ID="txtStoreFoundMessage" runat="server" EnableViewState="false"></asp:Localize>
        </span>
        <%-- <div class="StoreLocation">
            <asp:Localize ID="locOtherMessage" runat="server" meta:resourceKey="locOtherMessage"></asp:Localize>
        </div>--%>
        <p class="FormContent">
            <uc1:CustomMessage ID="uxCustomMessage" runat="server" MessageKey="StoreLocatorIntroText"></uc1:CustomMessage>
        </p>
    </div>
    <div class="SelectedStore">
        <asp:Label ID="lblStoreTitile" runat="server" CssClass="StoreTitle" EnableViewState="false" meta:resourceKey="lblStoreTitileresourcekey" Visible="false"></asp:Label>
        <asp:Label ID="lblStoreName" runat="server" EnableViewState="false"></asp:Label>
    </div>
    <%--<p>
        <asp:Localize ID="txtStoreLocatorHintText" meta:resourceKey="txtStoreLocatorHintText"
            runat="server" />
    </p>--%>
    <asp:Panel ID="pnlStoreLocator" runat="server" DefaultButton="ibSearch">
        <div class="Form">
            <div class="StoreLeftSection">
                <div class="StoreLocatorField">
                    <div class="FieldStyle LeftContent">
                        <label>
                            <asp:Localize ID="lblZip" runat="server" meta:resourceKey="lblZip" EnableViewState="false" /></label>
                    </div>
                    <div class="FieldStyle">
                        <asp:TextBox ID="txtZipcode" runat="server" Width="140px" MaxLength="6" ValidationGroup="dropdown" EnableViewState="false" meta:resourceKey="Zip" alt="Zip Code"/>
                        <div class="Validate">
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator" runat="server" ValidationGroup="dropdown" SetFocusOnError="true"
                                meta:resourcekey="RegularExpressionValidator" ControlToValidate="txtZipcode"
                                ValidationExpression="\d+" Display="Dynamic"></asp:RegularExpressionValidator>
                            <%-- <asp:RequiredFieldValidator ID="rfvZipCode" runat="server" ValidationGroup="dropdown" SetFocusOnError="true"
                            meta:resourcekey="RequiredFiledValidator" ControlToValidate="txtZipcode"
                            Display="Dynamic"></asp:RequiredFieldValidator>--%>
                        </div>
                    </div>
                </div>
                <div class="StoreLocatorField">
                    <div class="FieldStyle LeftContent">
                        <asp:Localize ID="tdSearchMessage" runat="server" meta:resourcekey="tdSearch" EnableViewState="false" />
                    </div>
                    <div class="FieldStyle">
                        <asp:DropDownList ID="ddlRadiusinMiles" runat="server" Font-Size="Small" CausesValidation="True" EnableViewState="false" meta:resourcekey="Search"
                            ValidationGroup="dropdown" Width="145px">
                            <%--<asp:ListItem Value="0" meta:resourceKey="liOne" />--%>
                            <%--  <asp:ListItem Value="5" meta:resourceKey="liTwo" />--%>
                            <asp:ListItem Value="10" meta:resourceKey="liThree" Selected="True" />
                            <asp:ListItem Value="25" meta:resourceKey="liFour" />
                            <asp:ListItem Value="50" meta:resourceKey="liFive" />
                            <asp:ListItem Value="75" meta:resourceKey="liSix" />
                            <asp:ListItem Value="100" meta:resourceKey="liSeven" />
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <%-- <div class="OrSection">
                -OR-
            </div>--%>
            <%--<div class="StoreRightSection">
                <div class="FieldStyle LeftContent">
                    <asp:Localize ID="lblCity" runat="server" meta:resourceKey="lblCity" EnableViewState="false" />
                </div>
                <div class="FieldStyle">
                    <asp:TextBox ID="txtCity" Width="140px" runat="server" TabIndex="3" EnableViewState="false" meta:resourceKey="City" />
                </div>
                <div class="Clear">
                </div>
                <div class="FieldStyle LeftContent">
                    <asp:Localize ID="lblState" runat="server" meta:resourceKey="lblState" />
                </div>
                <div class="FieldStyle">
                    <%--<asp:TextBox ID="txtState" runat="server" Width="140px" MaxLength="2" TabIndex="4" EnableViewState="false" meta:resourceKey="State" />--%>

            <%-- <asp:DropDownList ID="ddlState" runat="server" Width="145px">
                        <asp:ListItem Text="WI" Value="WI"></asp:ListItem>
                        <asp:ListItem Text="IL" Value="IL"></asp:ListItem>
                        <asp:ListItem Text="MN" Value="MN"></asp:ListItem>
                        <asp:ListItem Text="IA" Value="IA"></asp:ListItem>
                    </asp:DropDownList>--%>
            <%-- </div>--%>
            <%--<div class="FieldStyle LeftContent">
                <asp:Localize ID="lblAreaCode" runat="server" meta:resourceKey="lblAreaCode" EnableViewState="false" />
            </div>
            <div class="FieldStyle">
                <asp:TextBox ID="txtAreaCode" runat="server" Width="140px" MaxLength="3" TabIndex="5" EnableViewState="false" meta:resourceKey="AreaCode" />
             </div>--%>
            <%--</div>--%>
            <div class="StoreButtonSection">

                <div class="FieldStyle LeftContent">
                    <asp:LinkButton ID="ibSearch" runat="server" OnClick="Search_Click" ValidationGroup="dropdown"
                        CssClass="Button" meta:resourceKey="btnSubmit" />
                </div>
                <div class="FieldStyle" style="text-align: left;">
                    <asp:LinkButton ID="ibClear" runat="server" CausesValidation="False" OnClick="Clear_Click"
                        CssClass="ClearButton" meta:resourceKey="btnCancel" />
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="Row Error">
        <div class="Error" runat="server" id="errorMsg" visible="false" enableviewstate="false">
            <span class="uxMsg ErrorSection" runat="server" id="spnNoProductErrorMsg" enableviewstate="false">
                <em>
                    <asp:Label ID="lblMsgError" runat="server" EnableViewState="false"></asp:Label></em></span>
        </div>
        <asp:Label ID="lblMsg" runat="server" EnableViewState="false"></asp:Label>
    </div>
    <br />
    <asp:Panel ID="PnlStorelist" runat="server">
        <div class="StoreList">
            <div id="TopLink" runat="server">
                <asp:LinkButton ID="TopPrevLink" Text="<%$ Resources:CommonCaption, Prev%>" runat="server" CssClass="prev"
                    OnClick="PrevRecord"></asp:LinkButton>
                <asp:Localize ID="lblTopPaging" runat="server" Text="| <%$ Resources:CommonCaption, Page%>
                <%= ncurrentpage.ToString() %>
                <%$ Resources:CommonCaption, of%>
                <%= nRecCount.ToString() %>
                ( <%$ Resources:CommonCaption, Total%>
                <%=TotalRecords.ToString() %>
                <%$ Resources:CommonCaption, Item%> ) |" />
                <asp:LinkButton ID="TopNextLink" Text="<%$ Resources:CommonCaption, Next%>" OnClick="NextRecord" CssClass="next"
                    runat="server"></asp:LinkButton>
            </div>
            <br />
            <asp:DataList ID="DataListStores" EnableViewState="true" runat="server"
                OnItemCommand="DataListStores_ItemCommand"
                CellPadding="5" CellSpacing="10" RepeatColumns="1">
                <ItemTemplate>
                    <div class="TextContent">
                        <div class="Title">
                            <%# "<a target='_blank' href='" + GetStaticUrl(DataBinder.Eval(Container.DataItem, "Custom1").ToString()) + "'>"%>
                            <%# DataBinder.Eval(Container.DataItem, "Name")%></a>
                        </div>
                        <div class="Address">
                            <%# DataBinder.Eval(Container.DataItem, "Address1")%><br>
                            <%# DataBinder.Eval(Container.DataItem, "Address2")%>
                        </div>
                        <div class="Address">
                            <%# DataBinder.Eval(Container.DataItem, "Address3")%>
                        </div>
                        <div class="Address">
                            <%# DataBinder.Eval(Container.DataItem, "City")%>,&nbsp;<%# DataBinder.Eval(Container.DataItem, "State")%>&nbsp;-&nbsp;<%# DataBinder.Eval(Container.DataItem, "Zip")%><br />
                            <b>Phone: </b>
                            <%# DataBinder.Eval(Container.DataItem, "Phone")%><br />
                            <%--  <b>Fax: </b>
                            <%# DataBinder.Eval(Container.DataItem, "Fax")%>--%>
                        </div>
                        <div class="MapLink">
                            <%# "<a target='_blank' href='" + GetGoogleMapURL(DataBinder.Eval(Container.DataItem, "MapQuestURL").ToString()) +  "'>"%>
                            map and directions
                            <%--<img src='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogHelper.GetCatalogImagePath("bullet") %>'
                                align="absbottom" runat="server" border="0" alt="" />--%></a>
                        </div>
                    </div>
                    <div class="SetMyStore">
                        <asp:LinkButton ID="lbtnSetMyStore" runat="server" CssClass="Button" meta:resourceKey="lbtnSetMyStoreResource1"
                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "StoreID").ToString() + "|" + DataBinder.Eval(Container.DataItem, "Name").ToString() + "|"+  DataBinder.Eval(Container.DataItem, "Address1") + DataBinder.Eval(Container.DataItem, "Address2")  +  DataBinder.Eval(Container.DataItem, "Address3") + "|" + DataBinder.Eval(Container.DataItem, "City")+ "|" + DataBinder.Eval(Container.DataItem, "State") + "|" + DataBinder.Eval(Container.DataItem, "Phone").ToString() + "|" + DataBinder.Eval(Container.DataItem, "Zip").ToString()%>'
                            CommandName="SetStore"></asp:LinkButton>
                    </div>
                    <div class="ImageContent">
                        <img id="Img1" alt="IMG1" title="IMG1" border='0' src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile"))%> '
                            visible='<%# IsImageExists(DataBinder.Eval(Container.DataItem, "ImageFile")) %>'
                            runat="server" />
                    </div>
                </ItemTemplate>
                <ItemStyle BackColor="White" VerticalAlign="Top" />
            </asp:DataList>
            <br />
            <div id="BottomLink" runat="server">
                <asp:LinkButton ID="BotPrevLink" runat="server" CssClass="prev" OnClick="PrevRecord" Text="<%$ Resources:CommonCaption, Prev%>"></asp:LinkButton>
                <asp:Localize ID="lblBottomPaging" runat="server" Text="| <%$ Resources:CommonCaption, Page%> 
                <%= ncurrentpage.ToString() %>
                <%$ Resources:CommonCaption, of%>
                <%= nRecCount.ToString() %>
                (<%$ Resources:CommonCaption, Total%>
                <%=TotalRecords.ToString() %>
                <%$ Resources:CommonCaption, Item%> ) |" />
                <asp:LinkButton ID="BotNextLink" OnClick="NextRecord" CssClass="next" runat="server" Text="<%$ Resources:CommonCaption, Next%>"></asp:LinkButton>
            </div>
        </div>
    </asp:Panel>
</div>
