﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="zGoogleMapEmbedApi.ascx.cs" Inherits="WebApp.Controls.Default.StoreLocator.zGoogleMapEmbedApi" %>

<div class="google-maps">
    <iframe src="<%=GetGoogleMapSrc() %>"  title="Google Map" width="600"
        height="450" frameborder="0" style="border: 0" id="ifGoogleMap"></iframe>
</div>
