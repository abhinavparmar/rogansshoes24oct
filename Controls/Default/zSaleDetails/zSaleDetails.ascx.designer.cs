﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApp.Controls.Default.zSaleDetails {
    
    
    public partial class zSaleDetails {
        
        /// <summary>
        /// uxCustomMessage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WebApp.Controls_Default_CustomMessage_CustomMessage uxCustomMessage;
        
        /// <summary>
        /// ltlAllBrands control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltlAllBrands;
        
        /// <summary>
        /// CustomMessage1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WebApp.Controls_Default_CustomMessage_CustomMessage CustomMessage1;
    }
}
