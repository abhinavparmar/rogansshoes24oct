﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
namespace WebApp.Controls.Default.zSaleDetails
{
    public partial class zSaleDetails : System.Web.UI.UserControl
    {
        #region[Page Event]

        /// <summary>
        /// Page Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            ZNodeResourceManager resourceManager = new ZNodeResourceManager();

            // Set SEO Properties
            ZNodeSEO seo = new ZNodeSEO();
            //PRFT Custom Code : Start
            seo.SEOTitle = resourceManager.GetGlobalResourceObject("SiteMap", "txtSalesDetails");
            seo.SEODescription = resourceManager.GetGlobalResourceObject("SiteMap", "SalesDetailDescription").ToString();
            //PRFT Custom Code : End
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindManufacture();
            }
        }

        #endregion

        #region[Methods]

        /// <summary>
        /// Bind Manufactures list with product number for map price true
        /// </summary>
        private void BindManufacture()
        {
            ZNodeManufactureList znodeMaufactureList = new ZNodeManufactureList();
            try
            {
                znodeMaufactureList = ZNodeManufactureList.Create(ZNodeConfigManager.SiteConfig.PortalID, ZNodeConfigManager.SiteConfig.LocaleID);
                if (znodeMaufactureList != null && znodeMaufactureList.ZNodeMaufactureCollection != null && znodeMaufactureList.ZNodeMaufactureCollection.Count > 0)
                {
                    StringBuilder strManufacturer = new StringBuilder();
                    int count = 1;
                    foreach (ZNodeMaufactureCollection znodeMaufactureCollection in znodeMaufactureList.ZNodeMaufactureCollection)
                    {
                        strManufacturer.Append("<div class='Brand Sec" + count + "'>");
                        strManufacturer.Append("<span class='BrandName'><h4>" + znodeMaufactureCollection.ManufacturerName + "</h4></span>");
                        if (znodeMaufactureCollection.ZNodeMaufactureProductCollection != null && znodeMaufactureCollection.ZNodeMaufactureProductCollection.Count > 0)
                        {
                            strManufacturer.Append("<ul>");
                            foreach (ZNodeMaufactureProductCollection zNodeMaufactureProductCollection in znodeMaufactureCollection.ZNodeMaufactureProductCollection)
                            {
                                if (!string.IsNullOrEmpty(zNodeMaufactureProductCollection.ProductNum))
                                {
                                    strManufacturer.Append("<li><a href='" + zNodeMaufactureProductCollection.ProductSEOURL + "'>" + zNodeMaufactureProductCollection.ProductNum + "</a></li>");
                                }
                            }
                            strManufacturer.Append("</ul>");
                        }
                        strManufacturer.Append("</div>");
                        //if (count != 1 && count % 4 == 0)
                        //{
                        //    strManufacturer.Append("<br/>");
                        //}
                        count++;
                    }
                    ltlAllBrands.Text = strManufacturer.ToString();
                }


            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("Error in zSaleDetail.ascx.cs>>" + ex.ToString());
            }
        }

        #endregion
    }
}