<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_ProductTagging_ProductTagging" Codebehind="ProductTagging.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="ZNode" %>

<div class="ProductTagging">   
     <div class="Title" runat="server" id="divSelectedTitle"><ZNode:CustomMessage id="CustomMessage2" MessageKey="ProductTaggingSelectedTitle" runat="server" ClientIDMode="Static"></ZNode:CustomMessage></div>  
     <asp:PlaceHolder runat="server" ID="phSelectedTags"></asp:PlaceHolder> 
     <asp:Panel ID="pnlProductTaggingTitle" runat="server" Visible="false" EnableViewState="false" ClientIDMode="Static">
         <div class="Title"><ZNode:CustomMessage id="CustomMessage1" EnableViewState="false" MessageKey="ProductTaggingTitle" runat="server" ClientIDMode="Static"></ZNode:CustomMessage></div>
     </asp:Panel>
     <asp:PlaceHolder runat="server" ID="ControlsPlaceHolder"></asp:PlaceHolder> 
</div> 