using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.ECommerce.Tagging;
using ZNode.Libraries.Framework.Business;
using Category = ZNode.Libraries.DataAccess.Entities.Category;
using ZNode.Libraries.ECommerce.Utilities;

namespace WebApp
{
    /// <summary>
    /// Represents the Product Tagging user control class.
    /// </summary>
    public partial class Controls_Default_ProductTagging_ProductTagging : System.Web.UI.UserControl
    {
        #region Member Variables

        private ZNodeSEOUrl _SeoUrl = new ZNodeSEOUrl();
        private int _CategoryId = 0;
        private int ControlTypeID = 0;
        private string _SelectedTagValues = string.Empty;

        // Global Tagging Class declartion
        private ZNodeTag znodeTag = new ZNodeTag();
        private ZNodeCategory category = new ZNodeCategory();

        // Global dataset declaration
        private DataSet taggedDataSet = new DataSet();

        #endregion

        // Public Event Handler
        public event System.EventHandler SelectedIndexChanged;

        #region Properties
        /// <summary>
        /// Gets or sets the Selected tag values
        /// </summary>
        public string SelectedTagValues
        {
            get
            {
                return this._SelectedTagValues;
            }

            set
            {
                this._SelectedTagValues = value;
            }
        }

        /// <summary>
        /// Gets or sets the Category Id
        /// </summary>
        public int CategoryId
        {
            get
            {
                return this._CategoryId;
            }

            set
            {
                this._CategoryId = value;
            }
        }

        /// <summary>
        ///  Gets a Tagged DataSet
        /// </summary>
        public DataSet TaggedDataSet
        {
            get
            {
                return this.taggedDataSet;
            }
        }

        #endregion

        #region Bind Tags
        /// <summary>
        /// Bind the Product Tags in the leftside page.
        /// </summary>
        public void BindTags()
        {
            // Retrieve the tags details based on specific category
            this.taggedDataSet = this.znodeTag.RetrieveTagsByCategoryID(this._CategoryId, this.SelectedTagValues, ZNodeCatalogManager.CatalogConfig.CatalogID);
            
            // Add the hierarchical relationship to the dataset for TagGroup tables.        
            this.taggedDataSet.Relations.Add("TagGroupRelation", this.taggedDataSet.Tables[0].Columns["TagGroupId"], this.taggedDataSet.Tables[1].Columns["TagGroupId"]);

            // Start to add the dynamic controls here
            ControlsPlaceHolder.Controls.Add(new LiteralControl("<div class='ProductTagging'>"));

            pnlProductTaggingTitle.Visible = false;
           
            if (this.taggedDataSet.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow tagGroup in this.taggedDataSet.Tables[0].Rows)
                {
                    // TAG GROUP                
                    if (tagGroup["ControlTypeID"].ToString() != string.Empty)
                    {
                        this.ControlTypeID = int.Parse(tagGroup["ControlTypeID"].ToString());
                    }

                    DataRow[] childrows = tagGroup.GetChildRows("TagGroupRelation");

                    if (childrows != null && childrows.Length > 0)
                    {
                        pnlProductTaggingTitle.Visible = true;

                        switch ((ZNodeTagControlType)this.ControlTypeID)
                        {
                            case ZNodeTagControlType.LABEL:
                                // Label method here
                                this.BindLabelControl(tagGroup);
                                break;
                            case ZNodeTagControlType.RADIOBUTTON:
                                // RadioButton method here
                                this.BindRadioButton(tagGroup);
                                break;
                            case ZNodeTagControlType.ICONS:
                                this.BindICons(tagGroup);
                                break;
                            default:
                                // Dropdown method here
                                this.BindDropdown(tagGroup);
                                break;
                        }
                    }
                }
            }

            ControlsPlaceHolder.Controls.Add(new LiteralControl("</div>"));

            if (!string.IsNullOrEmpty(this.SelectedTagValues))
            {
                this.BindSelectedTags(this.SelectedTagValues);
            }
        }

        /// <summary>
        /// Get the list of the Products
        /// </summary>
        public void GetProductList()
        {
            // Serialize the object
            if (this.SelectedTagValues.Length > 0)
            {
                Session.Add("ProductList", this.znodeTag.RetrieveProductsByTagIds(this.SelectedTagValues, this._CategoryId));
            }
        }

        #endregion
        #region Page_Load

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get categoryId from querystring  
            if (Request.Params["zcid"] != null)
            {
                this._CategoryId = int.Parse(Request.Params["zcid"]);
            }

            if (!Page.IsPostBack)
            {
                divSelectedTitle.Visible = false;
            }

            if (this._CategoryId != 0)
            {
                if (this.taggedDataSet != null && Page.IsPostBack)
                {
                    if (Session["TaggedValues"] != null)
                    {
                        this.SelectedTagValues = Session["TaggedValues"].ToString();
                    }
                }
              
                ControlsPlaceHolder.Controls.Clear();
                this.BindTags();
            }
        }
        #endregion

        #region Events

        /// <summary>
        /// Event Handler for the dynamic control DropDownList
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Controls_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Get Selected Tag Values
            this.SelectedTagValues = this.GetSelectedTags();

            if (this.SelectedIndexChanged != null)
            {
                this.SelectedIndexChanged(sender, e);
            }
        }

        /// <summary>
        /// Event Handler for the dynamic control LinkButton
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void OnClick_linkbuttonChanged(object sender, EventArgs e)
        {
            LinkButton lnkBtnTagControl = sender as LinkButton;
            StringBuilder tagLinkValues = new StringBuilder();

            if (Session["TaggedValues"] != null)
            {
                this.SelectedTagValues = Session["TaggedValues"].ToString() + "," + lnkBtnTagControl.CommandArgument.ToString();
            }
            else
            {
                this.SelectedTagValues = lnkBtnTagControl.CommandArgument.ToString();
            }

            Session["TaggedValues"] = this.SelectedTagValues;

            string qstr = this.ConstructQueryStringFromSessionValues(Session["TaggedValues"].ToString());

            if (!string.IsNullOrEmpty(qstr))
            {
                Response.Redirect(qstr, true);
            }

            ControlsPlaceHolder.Controls.Clear();

            this.BindTags();

            if (this.SelectedIndexChanged != null)
            {
                this.SelectedIndexChanged(sender, e);
            }
        }

        /// <summary>
        /// Returns current selected addons from the list
        /// </summary>
        /// <returns>Returns the selected addons</returns>
        private string GetSelectedTags()
        {
            StringBuilder tagValues = new StringBuilder();

            // Dynamic Controls 
            DropDownList ddlistControl = new DropDownList();
            TreeView treeviewTagControl = new TreeView();
            RadioButtonList rbtnlistTagControl = new RadioButtonList();
            CheckBoxList chkBoxListTagControl = new CheckBoxList();

            if (this.taggedDataSet.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow tagGroup in this.taggedDataSet.Tables[0].Rows)
                {
                    if (tagGroup["ControlTypeID"].ToString() != string.Empty)
                    {
                        this.ControlTypeID = int.Parse(tagGroup["ControlTypeID"].ToString());
                    }

                    switch ((ZNodeTagControlType)this.ControlTypeID)
                    {
                        case ZNodeTagControlType.RADIOBUTTON: // RadioButtonList Control 

                            rbtnlistTagControl = (RadioButtonList)ControlsPlaceHolder.FindControl("ctrlRadionButtonTag" + tagGroup["TagGroupId"].ToString());

                            if (tagValues.Length > 0)
                            {
                                tagValues.Append(",");
                            }

                            if (rbtnlistTagControl != null)
                            {
                                if (rbtnlistTagControl.SelectedIndex > -1)
                                {
                                    int selvalue = int.Parse(rbtnlistTagControl.SelectedValue);

                                    if (selvalue > 0)
                                    {
                                        if (tagValues.Length > 0)
                                        {
                                            tagValues.Append(",");
                                        }

                                        rbtnlistTagControl.SelectedItem.Selected = true;

                                        tagValues.Append(selvalue.ToString());
                                    }
                                }
                            }

                            break;

                        case ZNodeTagControlType.ICONS: // CheckBoxList Control
                        default: // Dropdown Method here

                            ddlistControl = (DropDownList)ControlsPlaceHolder.FindControl("ctrldropdownTag" + tagGroup["TagGroupId"].ToString());

                            if (ddlistControl != null)
                            {
                                if (ddlistControl.SelectedIndex != 0)
                                {
                                    int selvalue = int.Parse(ddlistControl.SelectedValue);

                                    if (selvalue > 0)
                                    {
                                        if (tagValues.Length > 0)
                                        {
                                            tagValues.Append(",");
                                        }

                                        tagValues.Append(selvalue.ToString());
                                    }
                                }
                            }

                            break;
                    }  // End Switch      
                }
            }

            this.SelectedTagValues = tagValues.ToString();

            if (Session["TaggedValues"] != null)
            {
                this.SelectedTagValues = Session["TaggedValues"].ToString() + "," + tagValues;
            }

            Session["TaggedValues"] = this.SelectedTagValues;

            string qstr = this.ConstructQueryStringFromSessionValues(Session["TaggedValues"].ToString());

            if (!string.IsNullOrEmpty(qstr))
            {
                Response.Redirect(qstr, true);
            }

            ControlsPlaceHolder.Controls.Clear();

            // return the seleceted Tag Values.
            return this._SelectedTagValues;
        }

        #endregion

        #region Bind Methods

        private void ClearControls()
        {
            ControlsPlaceHolder.Controls.Clear();
        }

        #endregion

        #region Dynamic Control Bind Methods

        /// <summary>
        /// Represents the BindIcons Method
        /// </summary>
        /// <param name="tagGroupRow">The DataRow of Tag Group</param>
        private void BindICons(DataRow tagGroupRow)
        {
            // Start to add the dynamic controls here
            ControlsPlaceHolder.Controls.Add(new LiteralControl("<div>"));

            // String Builder
            StringBuilder labelTagValues = new StringBuilder();

            // Add controls to the place holder
            Literal litrl = new Literal();
            litrl.Text = "<span class='IconsTitle'>" + tagGroupRow["TagGroupLabel"].ToString() + "</span>";
            ControlsPlaceHolder.Controls.Add(litrl);
            ZNodeImage znodeImage = new ZNodeImage();
            foreach (DataRow tag in tagGroupRow.GetChildRows("TagGroupRelation"))
            {
                int TagProductCount = int.Parse(tag["TagProductCount"].ToString());

                if (TagProductCount > 0)
                {
                    LinkButton linkBtnTagName = new LinkButton();
                    linkBtnTagName.ID = "ctrllnkBtnTag" + tag["TagId"].ToString();
                    linkBtnTagName.Click += new EventHandler(this.OnClick_linkbuttonChanged);

                    if (tag["IconPath"] != System.DBNull.Value && tag["IconPath"].ToString() != string.Empty)
                    {
                        linkBtnTagName.Text = "<img style=\"border:none; vertical-align: middle;\" src=\"" + znodeImage.GetImageHttpPathSmallThumbnail(tag["IconPath"].ToString()).Replace("~/", string.Empty) + "\" />" + "(" + tag["TagProductCount"].ToString() + ")";
                    }
                    else
                    {
                        linkBtnTagName.Text = tag["TagName"].ToString() + " (" + tag["TagProductCount"].ToString() + ")";
                    }

                    linkBtnTagName.CommandArgument = tag["TagId"].ToString();
                    linkBtnTagName.CssClass = "linkBtnTagText";

                    ControlsPlaceHolder.Controls.Add(new LiteralControl("<div class='ValueStyle'>"));
                    ControlsPlaceHolder.Controls.Add(linkBtnTagName);
                    ControlsPlaceHolder.Controls.Add(new LiteralControl("</div>"));
                }
            }

            ControlsPlaceHolder.Controls.Add(new LiteralControl("</div>"));
        }

        /// <summary>
        /// Bind the Dropdown control for tags
        /// </summary>
        /// <param name="tagGroupRow">Tag Group Data Row</param>
        private void BindDropdown(DataRow tagGroupRow)
        {
            // Dropdownlist
            DropDownList ddlTaglist = new DropDownList();
            ddlTaglist.Items.Add(new ListItem(tagGroupRow["TagGroupLabel"].ToString(), "0"));
            ddlTaglist.AppendDataBoundItems = true;
            ddlTaglist.ID = "ctrldropdownTag" + tagGroupRow["TagGroupId"].ToString();
            ddlTaglist.AutoPostBack = true;
            ddlTaglist.CssClass = "dropdownText";
            ddlTaglist.SelectedIndexChanged += new EventHandler(this.Controls_SelectedIndexChanged);
            ddlTaglist.SelectedIndex = 0;

            foreach (DataRow tag in tagGroupRow.GetChildRows("TagGroupRelation"))
            {
                int TagProductCount = int.Parse(tag["TagProductCount"].ToString());

                if (TagProductCount > 0)
                {
                    // TAG                              
                    ListItem li1 = new ListItem(tag["TagName"].ToString() + " (" + tag["TagProductCount"].ToString() + ")", tag["TagId"].ToString());
                    ddlTaglist.Items.Add(li1);
                }
            }

            ControlsPlaceHolder.Controls.Add(new LiteralControl("<div class='ValueStyle'>"));
            ControlsPlaceHolder.Controls.Add(ddlTaglist);
            ControlsPlaceHolder.Controls.Add(new LiteralControl("</div>"));
        }

        /// <summary>
        /// Bind the Label control
        /// </summary>
        /// <param name="tagGroupRow">Tag Group Data Row</param>
        private void BindLabelControl(DataRow tagGroupRow)
        {
            // Start to add the dynamic controls here
            ControlsPlaceHolder.Controls.Add(new LiteralControl("<div>"));

            // String Builder
            StringBuilder labelTagValues = new StringBuilder();

            // Add controls to the place holder
            Literal litrl = new Literal();
            litrl.Text = "<span class='LinksTitle'>" + tagGroupRow["TagGroupLabel"].ToString() + "</span>";
            ControlsPlaceHolder.Controls.Add(litrl);

            foreach (DataRow tag in tagGroupRow.GetChildRows("TagGroupRelation"))
            {
                int TagProductCount = int.Parse(tag["TagProductCount"].ToString());

                if (TagProductCount > 0)
                {
                    LinkButton linkBtnTagName = new LinkButton();
                    linkBtnTagName.ID = "ctrllnkBtnTag" + tag["TagId"].ToString();
                    linkBtnTagName.Click += new EventHandler(this.OnClick_linkbuttonChanged);

                    linkBtnTagName.Text = tag["TagName"].ToString() + " (" + tag["TagProductCount"].ToString() + ")";
                    linkBtnTagName.CommandArgument = tag["TagId"].ToString();
                    linkBtnTagName.CssClass = "linkBtnTagText";

                    ControlsPlaceHolder.Controls.Add(new LiteralControl("<div class='ValueStyle'>"));
                    ControlsPlaceHolder.Controls.Add(linkBtnTagName);
                    ControlsPlaceHolder.Controls.Add(new LiteralControl("</div>"));
                }
            }

            ControlsPlaceHolder.Controls.Add(new LiteralControl("</div>"));
        }

        /// <summary>
        /// Bind the RadioButtonList control
        /// </summary>
        /// <param name="tagGroupRow">Tag Group Data Row</param>
        private void BindRadioButton(DataRow tagGroupRow)
        {
            // Start to add the dynamic controls here
            ControlsPlaceHolder.Controls.Add(new LiteralControl("<div>"));

            // RadioButtonList
            RadioButtonList rbtnlistControl = new RadioButtonList();
            rbtnlistControl.ID = "ctrlRadionButtonTag" + tagGroupRow["TagGroupId"].ToString();
            rbtnlistControl.AutoPostBack = true;
            rbtnlistControl.SelectedIndexChanged += new EventHandler(this.Controls_SelectedIndexChanged);
            rbtnlistControl.CssClass = "RadioButton";

            // Set Vertical view
            rbtnlistControl.RepeatDirection = RepeatDirection.Vertical;

            // Add controls to the place holder
            Literal litrl = new Literal();
            litrl.Text = "<span class='RadioButtonListTitle'>" + tagGroupRow["TagGroupLabel"].ToString() + "</span>";
            ControlsPlaceHolder.Controls.Add(litrl);

            foreach (DataRow tag in tagGroupRow.GetChildRows("TagGroupRelation"))
            {
                int TagProductCount = int.Parse(tag["TagProductCount"].ToString());

                if (TagProductCount > 0)
                {
                    // TAG                              
                    ListItem li1 = new ListItem(tag["TagName"].ToString() + " (" + tag["TagProductCount"].ToString() + ")", tag["TagId"].ToString());
                    rbtnlistControl.Items.Add(li1);
                }
            }

            ControlsPlaceHolder.Controls.Add(new LiteralControl("<div class='ValueStyle'>"));
            ControlsPlaceHolder.Controls.Add(rbtnlistControl); // radioButton list control
            ControlsPlaceHolder.Controls.Add(new LiteralControl("</div></div>"));
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Construct QueryString key name.
        /// </summary>
        /// <param name="squeryname">The Query name</param>
        /// <returns>Returns the Query name</returns>
        private string QKeyName(string squeryname)
        {
            return squeryname.Replace(" ", "_") + "=";
        }

        /// <summary>
        /// Construct QueryString value.
        /// </summary>
        /// <param name="squeryvalue">Query value</param>
        /// <returns>Returns the Query value</returns>
        private string QKeyValue(string squeryvalue)
        {
            return Server.UrlEncode(squeryvalue).Replace("%e2%80%99", "'");
        }

        /// <summary>
        /// Construct the query string from session values
        /// </summary>
        /// <param name="tagValues">Tag Values</param>
        /// <returns>Returns the querystring values</returns>
        private string ConstructQueryStringFromSessionValues(string tagValues)
        {
            string qstring = string.Empty;

            ZNode.Libraries.DataAccess.Service.TagService tagService = new ZNode.Libraries.DataAccess.Service.TagService();
            ZNode.Libraries.DataAccess.Service.TagGroupService tagGroupService = new ZNode.Libraries.DataAccess.Service.TagGroupService();

            string[] keyValues = tagValues.ToString().Split(',');

            foreach (string sessionkey in keyValues)
            {
                if (!string.IsNullOrEmpty(sessionkey))
                {
                    ZNode.Libraries.DataAccess.Entities.Tag tag = tagService.GetByTagID(Convert.ToInt32(sessionkey));

                    if (tag != null)
                    {
                        qstring = qstring + (qstring.Trim().Length == 0 ? string.Empty : "&") + this.QKeyName(tagGroupService.GetByTagGroupID(tag.TagGroupID.Value).TagGroupLabel) + this.QKeyValue(tag.TagName);
                    }
                }
            }

            string qurl = string.Empty;

            if (qstring.Trim().Length > 0)
            {
                string url = this.GetPageURL();
                qurl = url + ((url.IndexOf('?') > 0) ? "&" : "?") + qstring.Trim();
            }

            return qurl;
        }

        /// <summary>
        /// Gets the Page URL
        /// </summary>
        /// <returns>Returns the Page Url</returns>
        private string GetPageURL()
        {
            string url;
            string query;
            string zcid = string.Empty;

            if (Request.UrlReferrer == null)
            {
                url = Request.Url.ToString();
                query = Request.Url.Query;
                zcid = "?zcid=" + Request.QueryString["zcid"];
            }
            else
            {
                url = Request.UrlReferrer.ToString();
                query = Request.UrlReferrer.Query;
            }

            if (HttpContext.Current.Items["SeoUrlFound"] == null)
            {
                zcid = "?zcid=" + Request.QueryString["zcid"];
            }

            if (query.Length > 0)
            {
                url = url.Substring(0, url.IndexOf('?'));
            }

            return url + zcid.Trim();
        }

        /// <summary>
        /// Bind the Label control
        /// </summary>
        /// <param name="selectedTagValues">Selected Tag Values</param>
        private void BindSelectedTags(string selectedTagValues)
        {
            ZNodeTag Tagging = new ZNodeTag();
            DataSet tagNameDataset = new DataSet();
            bool IsClearNeed = false;

            // Clear the controls
            phSelectedTags.Controls.Clear();

            // String Builder
            StringBuilder labelTagValues = new StringBuilder();

            tagNameDataset = Tagging.GetBreadCrumbsByTagIds(this._SelectedTagValues);
            tagNameDataset.Tables[0].PrimaryKey = new DataColumn[] { tagNameDataset.Tables[0].Columns["TagId"] };

            ZNode.Libraries.DataAccess.Entities.TagGroup tg;
            ZNode.Libraries.DataAccess.Service.TagGroupService tgs = new ZNode.Libraries.DataAccess.Service.TagGroupService();
            Literal literal1 = null;
            divSelectedTitle.Visible = false;

            foreach (string tags in this._SelectedTagValues.Split(','))
            {
                if (!string.IsNullOrEmpty(tags))
                {
                    DataRow tag = tagNameDataset.Tables[0].Rows.Find(tags);

                    if (tag != null)
                    {
                        divSelectedTitle.Visible = true;
                        IsClearNeed = true;

                        // Start to add the dynamic controls here
                        phSelectedTags.Controls.Add(new LiteralControl("<div>"));

                        tg = tgs.GetByTagGroupID(int.Parse(Convert.ToString(tag["TagGroupId"])));

                        literal1 = new Literal();
                        literal1.Text = "<span class=\"SelectedTagText\">" + tg.TagGroupLabel + ":</span><span class=\"SelectedTagValue\">" + tag["TagName"].ToString() + "</span><br />";

                        HyperLink linkBtnTagName = new HyperLink();
                        linkBtnTagName.ID = "breadcrumblnkBtnTag" + tag["TagId"].ToString();
                        linkBtnTagName.Text = String.Format("[{0}]", this.GetLocalResourceObject("Remove").ToString());
                        linkBtnTagName.CssClass = "linkBtnTagText";
                        linkBtnTagName.Attributes["style"] = "font-size:7pt;";
                        linkBtnTagName.NavigateUrl = this.ConstructQueryStringFromSessionValues(Convert.ToInt32(tag["TagId"]));

                        phSelectedTags.Controls.Add(literal1);
                        phSelectedTags.Controls.Add(linkBtnTagName);
                        phSelectedTags.Controls.Add(new LiteralControl("</div><br />"));
                    }
                }
            }

            if (IsClearNeed)
            {
                // Start to add the dynamic controls here
                phSelectedTags.Controls.Add(new LiteralControl("<div>"));

                HyperLink linkBtnTagName = new HyperLink();
                linkBtnTagName.ID = "breadcrumblnkBtnTag0";
                linkBtnTagName.Text = this.GetLocalResourceObject("ClearAllSelections").ToString();
                linkBtnTagName.CssClass = "linkBtnTagText";
                linkBtnTagName.Style.Add("font-size", "7pt");
                linkBtnTagName.NavigateUrl = this.ConstructQueryStringFromSessionValues(0);

                phSelectedTags.Controls.Add(linkBtnTagName);
                phSelectedTags.Controls.Add(new LiteralControl("</div>"));
            }
        }

        #region Selected Tags

        /// <summary>
        /// Construct the query string from session values
        /// </summary>
        /// <param name="tagId">The value of tag Id</param>
        /// <returns>Returns the Query String From Session Values</returns>
        private string ConstructQueryStringFromSessionValues(int tagId)
        {
            string qstring = string.Empty;

            ZNode.Libraries.DataAccess.Service.TagService tagService = new ZNode.Libraries.DataAccess.Service.TagService();
            ZNode.Libraries.DataAccess.Service.TagGroupService tagGroupService = new ZNode.Libraries.DataAccess.Service.TagGroupService();

                string[] keyValues = Session["TaggedValues"].ToString().Split(',');

                foreach (string sessionkey in keyValues)
                {
                    if (!string.IsNullOrEmpty(sessionkey))
                    {
                        if (sessionkey == tagId.ToString() || tagId == 0)
                        {
                            continue;
                        }

                        Tag tag = tagService.GetByTagID(Convert.ToInt32(sessionkey));

                        if (tag != null)
                        {
                            qstring = qstring + (qstring.Trim().Length == 0 ? string.Empty : "&") + this.QKeyName(tagGroupService.GetByTagGroupID(tag.TagGroupID.Value).TagGroupLabel) + Server.UrlEncode(tag.TagName);
                        }
                    }
                }

            string url = this.GetCurrentPageURL();

            string qurl = url;

            if (qstring.Trim().Length > 0)
            {
                qurl = url + ((url.IndexOf('?') > 0) ? "&" : "?") + qstring.Trim();
            }

            return qurl;
        }

        private string IsSEOUrl()
        {
            string zcid = string.Empty;

            if (Request.QueryString["zcid"] != null)
            {
                Session["BreadCrumzcid"] = Request.QueryString["zcid"];
                zcid = Request.QueryString["zcid"].ToString();
            }
            else if (Session["BreadCrumzcid"] != null)
            {
                zcid = Session["BreadCrumzcid"].ToString();
            }
            else if (Request.QueryString["zpid"] != null)
            {
                ZNode.Libraries.DataAccess.Service.ProductCategoryService service = new ZNode.Libraries.DataAccess.Service.ProductCategoryService();
                TList<ProductCategory> pcategories = service.GetByProductID(Convert.ToInt32(Request.QueryString["zpid"]));
                if (pcategories.Count > 0)
                {
                    zcid = pcategories[0].CategoryID.ToString();
                }
            }
            else
            {
                zcid = "0";
            }

            ZNode.Libraries.DataAccess.Service.CategoryService cservice = new ZNode.Libraries.DataAccess.Service.CategoryService();
            Category category = cservice.GetByCategoryID(Convert.ToInt32(zcid));
            if (category != null)
            {
                string seourl = string.Empty;
                if (category.SEOURL != null)
                {
                    seourl = category.SEOURL;
                }

                return ZNodeSEOUrl.MakeURL(category.CategoryID.ToString(), SEOUrlType.Category, seourl);
            }

            return ZNodeSEOUrl.MakeURL("0", SEOUrlType.Category, string.Empty);
        }

        /// <summary>
        /// Gets the Current Page URL
        /// </summary>
        /// <returns>Returns the Current Page URL</returns>
        private string GetCurrentPageURL()
        {
            return this.IsSEOUrl();
        }

        #endregion

        #endregion
    }
}