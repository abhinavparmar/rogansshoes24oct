﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Zeon.Libraries.Elmah;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace WebApp.Controls.Default.zLandingPages
{
    public partial class ZShoeFinder : System.Web.UI.UserControl
    {
        #region Private Variables
        private string genders = string.Empty;
        private string widths = string.Empty;
        private string likeOurSite = string.Empty;
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            txtFirstNameShoeFinder.Focus();
            if (!Page.IsPostBack)
            {
                BindData();
            }
        }

        /// <summary>
        /// Page Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            ZNodeResourceManager resourceManager = new ZNodeResourceManager();

            // Set SEO Properties
            ZNodeSEO seo = new ZNodeSEO();
            //seo.SEOTitle = resourceManager.GetGlobalResourceObject("SiteMap", "txtShoeFinder");
            //PRFT Custom Code : Start
            seo.SEOTitle = resourceManager.GetGlobalResourceObject("SiteMap", "txtShoeFinder");
            seo.SEODescription = resourceManager.GetGlobalResourceObject("SiteMap", "ShoeFinderDescription").ToString();
            //PRFT Custom Code : End
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// To Bind data to controls
        /// </summary>
        private void BindData()
        {
            this.BindLikeOurSite();
            this.BindGender();
            this.BindWidth();
            this.BindBrands();
            this.BindStoreLocation();
        }

        /// <summary>
        /// To bind the store locations
        /// </summary>
        private void BindStoreLocation()
        {
            StoreService storeService = new StoreService();
            ZNode.Libraries.DataAccess.Entities.TList<ZNode.Libraries.DataAccess.Entities.Store> storeList = storeService.Find("PortalID = " + Convert.ToInt32(ZNodeConfigManager.SiteConfig.PortalID) + " AND " + "ActiveInd = True");
            if (storeList != null && storeList.Count > 0)
            {
                ddlStoreLocation.DataSource = storeList;
                ddlStoreLocation.DataValueField = "StoreID";
                ddlStoreLocation.DataTextField = "City";
                ddlStoreLocation.DataBind();
                ddlStoreLocation.Items.Insert(0, new ListItem(this.GetLocalResourceObject("DefaultSelectLocation").ToString(), this.GetLocalResourceObject("DefaultSelectLocation").ToString()));
                ddlStoreLocation.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// To bind the brands
        /// </summary>
        private void BindBrands()
        {
            DataTable dtManufacturerList = GetAllBrandAlphabetical();
            if (dtManufacturerList != null && dtManufacturerList.Rows.Count > 0)
            {
                ddlBrand.DataSource = dtManufacturerList;
                ddlBrand.DataValueField = "ManufacturerID";
                ddlBrand.DataTextField = "Name";
                ddlBrand.DataBind();
                ddlBrand.Items.Insert(0, new ListItem(this.GetLocalResourceObject("DefaultSelectBrand").ToString(), this.GetLocalResourceObject("DefaultSelectBrand").ToString()));
                ddlBrand.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Get all brands 
        /// </summary>
        private DataTable GetAllBrandAlphabetical()
        {
            DataTable dtManufacturer = (DataTable)System.Web.HttpRuntime.Cache["AllBrands" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId + HttpContext.Current.Session.SessionID];

            if (dtManufacturer == null)
            {
                ManufacturerHelper manuHelper = new ManufacturerHelper();
                dtManufacturer = manuHelper.GetActiveBrandsByPortal(ZNodeConfigManager.SiteConfig.PortalID, ZNodeConfigManager.SiteConfig.LocaleID, 0);
                //Insert into cache
                ZNodeCacheDependencyManager.Insert("AllBrands" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId
                                             + HttpContext.Current.Session.SessionID,
                                             dtManufacturer,
                                             DateTime.Now.AddHours(1),
                                             System.Web.Caching.Cache.NoSlidingExpiration,
                                             null);
            }

            return dtManufacturer;

        }

        /// <summary>
        /// To bind the width 
        /// </summary>
        private void BindWidth()
        {
            widths = this.GetLocalResourceObject("WidthValues").ToString();
            if (!string.IsNullOrEmpty(widths))
            {
                string[] arrwidth = widths.Split(',');
                ddlWidth.DataSource = arrwidth;
                ddlWidth.DataBind();
            }
        }

        /// <summary>
        /// To bind genders
        /// </summary>
        private void BindGender()
        {
            genders = this.GetLocalResourceObject("GenderValues").ToString();
            if (!string.IsNullOrEmpty(genders))
            {
                string[] arrgenders = genders.Split(',');
                ddlGender.DataSource = arrgenders;
                ddlGender.DataBind();
            }

        }

        /// <summary>
        /// To bind Like our site ratings
        /// </summary>
        private void BindLikeOurSite()
        {
            likeOurSite = this.GetLocalResourceObject("LikeOurSiteValues").ToString();
            if (!string.IsNullOrEmpty(likeOurSite))
            {
                string[] arrLikeOurSite = likeOurSite.Split(',');
                drpLikeOurSite.DataSource = arrLikeOurSite;
                drpLikeOurSite.DataBind();
            }
        }

        /// <summary>
        /// Verify the Captach Text
        /// </summary>
        private bool VerifyCaptachText()
        {
            CaptchaID.ValidateCaptcha(txtCharacters.Text.Trim());
            if (CaptchaID.UserValidated)
            {
                return true;
            }
            lblErrorMsg.Text = this.GetLocalResourceObject("CaptchaErrorMessage").ToString();
            txtCharacters.Focus();
            return false;
        }

        /// <summary>
        /// Send Shoe Finder Email
        /// </summary>
        private void SendShoeFinderEmail()
        {
            string shoeFinderEmailTemplate = string.Empty;
            shoeFinderEmailTemplate = CreateShoeFinderEmailTemplate();
            ZeonMessageConfig zeonMessageconfig = new ZeonMessageConfig();

            if (!string.IsNullOrEmpty(shoeFinderEmailTemplate))
            {
                try
                {
                    ZNodeEmail.SendEmail(ZNodeConfigManager.SiteConfig.CustomerServiceEmail, txtEmail.Text.Trim(), string.Empty, zeonMessageconfig.GetMessageKey("ShoeFinderSubject"), shoeFinderEmailTemplate, true);
                    pnlShoeFinder.Visible = false;
                    pnlConfirm.Visible = true;
                }
                catch (Exception ex)
                {
                    ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "Shoe Finder email template sending problem(Mehtod name: SendShoeFinderEmail())=>ZShoeFinder Page:" + ex.StackTrace);
                }

            }
        }

        /// <summary>
        /// To create Shoe Finder Emial Template
        /// </summary>
        /// <returns></returns>
        private string CreateShoeFinderEmailTemplate()
        {
            string defaultHtmlTemplatePath = string.Empty;
            string currentCulture = string.Empty;
            string htmlTemplatePath = string.Empty;

            currentCulture = ZNodeCatalogManager.CultureInfo;

            defaultHtmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ZShoeFinder.htm");

            // modified receipt file selection as we are using in-line styles for displaying template on mail content.
            if (currentCulture != string.Empty)
            {
                htmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ZShoeFinder_" + currentCulture + ".htm");
            }
            else
            {
                htmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ZShoeFinder.htm");
            }
            // Check the receipt template exists, if not exists then set default receipt template
            FileInfo fileinfo = new FileInfo(htmlTemplatePath);

            if (!fileinfo.Exists)
            {
                htmlTemplatePath = defaultHtmlTemplatePath;
            }

            StreamReader streamReader = new StreamReader(htmlTemplatePath);
            string messageText = streamReader.ReadToEnd();

            string spacerImage = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/Themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/Images/space.png";
            string domainPath = Request.Url.GetLeftPart(UriPartial.Authority);
            int portalId = ZNodeConfigManager.SiteConfig.PortalID;

            ZEmailHelper zEmailHelper = new ZEmailHelper();
            ZeonMessageConfig zeonMessageConfig = new ZeonMessageConfig();
            string copyRightText = zeonMessageConfig.GetMessageKey("FooterCopyrightText");

            string messageTextHeader = zEmailHelper.EmailHeaderContent(domainPath, portalId, spacerImage);
            string messageTextFooter = zEmailHelper.EmailFooterContent(domainPath, portalId, copyRightText, spacerImage);

            //To add header to the template
            Regex rxHeaderContent = new Regex("#HeaderContent#", RegexOptions.IgnoreCase);
            messageText = rxHeaderContent.Replace(messageText, messageTextHeader);

            //To add footer to the template
            Regex rxFooterContent = new Regex("#FooterContent#", RegexOptions.IgnoreCase);
            messageText = rxFooterContent.Replace(messageText, messageTextFooter);

            Regex rxFirstName = new Regex("#FirstName#", RegexOptions.IgnoreCase);
            messageText = rxFirstName.Replace(messageText, txtFirstNameShoeFinder.Text.Trim());

            Regex rxLastName = new Regex("#LastName#", RegexOptions.IgnoreCase);
            messageText = rxLastName.Replace(messageText, txtLastName.Text.Trim());

            Regex rxBrand = new Regex("#Brand#", RegexOptions.IgnoreCase);
            messageText = rxBrand.Replace(messageText, ddlBrand.SelectedItem.Text.Trim());

            Regex rxShoeName = new Regex("#ShoeName#", RegexOptions.IgnoreCase);
            messageText = rxShoeName.Replace(messageText, txtShoeName.Text.Trim());

            Regex rxColor = new Regex("#Color#", RegexOptions.IgnoreCase);
            messageText = rxColor.Replace(messageText, txtColor.Text.Trim());

            Regex rxSize = new Regex("#Size#", RegexOptions.IgnoreCase);
            messageText = rxSize.Replace(messageText, txtSize.Text.Trim());

            Regex rxWidth = new Regex("#Width#", RegexOptions.IgnoreCase);
            messageText = rxWidth.Replace(messageText, ddlWidth.SelectedItem.Text.Trim());

            Regex rxStoreLocation = new Regex("#StoreLocation#", RegexOptions.IgnoreCase);
            messageText = rxStoreLocation.Replace(messageText, ddlStoreLocation.SelectedItem.Text.Trim());

            Regex rxType = new Regex("#Type#", RegexOptions.IgnoreCase);
            messageText = rxType.Replace(messageText, txtType.Text.Trim());

            Regex rxStockNumber = new Regex("#StockNumber#", RegexOptions.IgnoreCase);
            messageText = rxStockNumber.Replace(messageText, txtStockNumber.Text.Trim());

            Regex rxLikeOurSite = new Regex("#LikeOurSite#", RegexOptions.IgnoreCase);
            messageText = rxLikeOurSite.Replace(messageText, drpLikeOurSite.SelectedItem.Text.Trim());

            Regex rxComments = new Regex("#Comments#", RegexOptions.IgnoreCase);
            messageText = rxComments.Replace(messageText, txtComments.Text.Trim());

            Regex rxGender = new Regex("#Gender#", RegexOptions.IgnoreCase);
            messageText = rxGender.Replace(messageText, ddlGender.SelectedItem.Text.Trim());

            Regex rxConfirmationText = new Regex("#EmailConfirmationText#", RegexOptions.IgnoreCase);
            messageText = rxConfirmationText.Replace(messageText, ZNodeCatalogManager.MessageConfig.GetMessage("ShoeFinderEmailConfirmationText", ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.CatalogConfig.LocaleID));
            return messageText;
        }

        /// <summary>
        /// Insert form Data Into ZShoeFinder Table
        /// </summary>
        /// <returns></returns>
        private bool InsertIntoZShoeFinder()
        {
            ZeonShoeFinder zeonShoeFinder = this.CreateZeonShoeFinderObject();
            if (zeonShoeFinder != null)
            {
                ZeonShoeFinderService zeonShoeFinderService = new ZeonShoeFinderService();
                zeonShoeFinderService.Insert(zeonShoeFinder);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Create ZeonShoeFinder Object
        /// </summary>
        /// <returns>Boolean Value</returns>
        private ZeonShoeFinder CreateZeonShoeFinderObject()
        {
            ZeonShoeFinder zeonShoeFinder = new ZeonShoeFinder();

            zeonShoeFinder.FirstName = txtFirstNameShoeFinder.Text.Trim();
            zeonShoeFinder.LastName = txtLastName.Text.Trim();
            zeonShoeFinder.EmailAddress = txtEmail.Text.Trim();
            zeonShoeFinder.TypeOfShoe = txtType.Text.Trim();
            zeonShoeFinder.StockNumber = txtStockNumber.Text.Trim();
            zeonShoeFinder.Comments = txtComments.Text.Trim();
            zeonShoeFinder.ShoeName = txtShoeName.Text.Trim();
            zeonShoeFinder.Color = txtColor.Text.Trim();
            zeonShoeFinder.SizeOfShoe = txtSize.Text.Trim();
            zeonShoeFinder.LikeOurSite = !drpLikeOurSite.SelectedItem.Text.ToLower().Equals(this.GetLocalResourceObject("DefaultSelectText").ToString().ToLower())?drpLikeOurSite.SelectedItem.Text.Trim():null;
            zeonShoeFinder.StoreLocation = !ddlStoreLocation.SelectedItem.Text.ToLower().Equals(this.GetLocalResourceObject("DefaultSelectText").ToString().ToLower())?ddlStoreLocation.SelectedItem.Text.Trim():null;
            zeonShoeFinder.Gender = !ddlGender.SelectedItem.Text.ToLower().Equals(this.GetLocalResourceObject("DefaultSelectText").ToString().ToLower())?ddlGender.SelectedItem.Text.Trim():null;
            zeonShoeFinder.Width = !ddlWidth.SelectedItem.Text.ToLower().Equals(this.GetLocalResourceObject("DefaultSelectText").ToString().ToLower())?ddlWidth.SelectedItem.Text.Trim():null;
            zeonShoeFinder.Brand = !ddlBrand.SelectedItem.Text.ToLower().Equals(this.GetLocalResourceObject("DefaultSelectText").ToString().ToLower()) ? ddlBrand.SelectedItem.Text.Trim() : null;
            zeonShoeFinder.CreateDate = DateTime.Now;

            return zeonShoeFinder;
        }
        #endregion

        #region Protected Methods
        /// <summary>
        /// Click Event to set Focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbtnRefresh_Click(object sender, EventArgs e)
        {
            txtCharacters.Text = string.Empty;
            lblErrorMsg.Text = string.Empty;
            txtCharacters.Focus();
        }

        /// <summary>
        /// Sumit button of the from
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgSubmit_Click(object sender, EventArgs e)
        {
            bool isValid = this.VerifyCaptachText();
            if (isValid)
            {
                try
                {
                    if (this.InsertIntoZShoeFinder())
                    {
                        this.SendShoeFinderEmail();
                    }
                }
                catch (Exception ex) { ElmahErrorManager.Log(ex); }
            }
        }
        #endregion
    }
}