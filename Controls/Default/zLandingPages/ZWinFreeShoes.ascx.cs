﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Zeon.Libraries.Elmah;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace WebApp.Controls.Default.zLandingPages
{
    public partial class ZWinFreeShoes : System.Web.UI.UserControl
    {
        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            txtFirstNameWinFreeShoes.Focus();
        }

        /// <summary>
        /// Page Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            ZNodeResourceManager resourceManager = new ZNodeResourceManager();

            // Set SEO Properties
            ZNodeSEO seo = new ZNodeSEO();
            seo.SEOTitle = resourceManager.GetGlobalResourceObject("SiteMap", "txtWinFreeShoes");
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Verify the Captach Text
        /// </summary>
        private bool VerifyCaptachText()
        {
            CaptchaID.ValidateCaptcha(txtCharacters.Text.Trim());
            if (CaptchaID.UserValidated)
            {
                return true;
            }
            lblErrorMsg.Text = this.GetLocalResourceObject("CaptchaErrorMessage").ToString();
            txtCharacters.Focus();
            return false;
        }

        /// <summary>
        /// To Send the Email
        /// </summary>
        private void SendWinFreeShoeEmail()
        {
            string winFreeShoeEmailTemplate = string.Empty;
            winFreeShoeEmailTemplate = CreateWinFreeShoeEmailTemplate();
            ZeonMessageConfig zeonMessageconfig = new ZeonMessageConfig();

            if (!string.IsNullOrEmpty(winFreeShoeEmailTemplate))
            {
                try
                {
                    ZNodeEmail.SendEmail(ZNodeConfigManager.SiteConfig.CustomerServiceEmail,txtEmail.Text.Trim(), string.Empty, zeonMessageconfig.GetMessageKey("WinFreeShoesSubject"), winFreeShoeEmailTemplate, true);
                    pnlWinFreeShoesTitle.Visible = false;
                    pnlConfirm.Visible = true;
                }
                catch (Exception ex)
                {
                    ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "Win Free Shoes  email template sending problem(Mehtod name: SendWinFreeShoeEmail())=>ZWinFreeShoes Page:" + ex.StackTrace);
                }
                
            } 
        }

        /// <summary>
        /// To create the Email Template
        /// </summary>
        private string CreateWinFreeShoeEmailTemplate()
        {
            string defaultHtmlTemplatePath = string.Empty;
            string currentCulture = string.Empty;
            string htmlTemplatePath = string.Empty;

            currentCulture = ZNodeCatalogManager.CultureInfo;

            defaultHtmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ZWinFreeShoes.htm");

            // modified receipt file selection as we are using in-line styles for displaying template on mail content.
            if (currentCulture != string.Empty)
            {
                htmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ZWinFreeShoes_" + currentCulture + ".htm");
            }
            else
            {
                htmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ZWinFreeShoes.htm");
            }
            // Check the receipt template exists, if not exists then set default receipt template
            FileInfo fileinfo = new FileInfo(htmlTemplatePath);

            if (!fileinfo.Exists)
            {
                htmlTemplatePath = defaultHtmlTemplatePath;
            }

            StreamReader streamReader = new StreamReader(htmlTemplatePath);
            string messageText = streamReader.ReadToEnd();

            string spacerImage = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/Themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/Images/space.png";
            string domainPath = Request.Url.GetLeftPart(UriPartial.Authority);
            int portalId = ZNodeConfigManager.SiteConfig.PortalID;

            ZEmailHelper zEmailHelper = new ZEmailHelper();
            ZeonMessageConfig zeonMessageConfig = new ZeonMessageConfig();
            string copyRightText = zeonMessageConfig.GetMessageKey("FooterCopyrightText");

            string messageTextHeader = zEmailHelper.EmailHeaderContent(domainPath, portalId, spacerImage);
            string messageTextFooter = zEmailHelper.EmailFooterContent(domainPath, portalId, copyRightText, spacerImage);

            //To add header to the template
            Regex rxHeaderContent = new Regex("#HeaderContent#", RegexOptions.IgnoreCase);
            messageText = rxHeaderContent.Replace(messageText, messageTextHeader);

            //To add footer to the template
            Regex rxFooterContent = new Regex("#FooterContent#", RegexOptions.IgnoreCase);
            messageText = rxFooterContent.Replace(messageText, messageTextFooter);

            Regex rxFirstName = new Regex("#FirstName#", RegexOptions.IgnoreCase);
            messageText = rxFirstName.Replace(messageText, txtFirstNameWinFreeShoes.Text.Trim());

            Regex rxLastName = new Regex("#LastName#", RegexOptions.IgnoreCase);
            messageText = rxLastName.Replace(messageText, txtLastName.Text.Trim());

            return messageText;
        }

        /// <summary>
        ///Insert the values into Table
        /// </summary>
        private bool InsertIntoWinFreeShoes()
        {
             ZeonWinFreeShoes zeonWinFreeShoes = CreateWinFreeShoesObject();
            if(zeonWinFreeShoes != null)
            {
                ZeonWinFreeShoesService zeonWinFreeShoesService = new ZeonWinFreeShoesService();
                zeonWinFreeShoesService.Insert(zeonWinFreeShoes);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Saved the Form Fields to the ZeonWinFreeShoes object
        /// </summary>
        /// <returns></returns>
        private ZeonWinFreeShoes CreateWinFreeShoesObject()
        {
            ZeonWinFreeShoes zeonWinFreeShoes = new ZeonWinFreeShoes();
            zeonWinFreeShoes.FirstName = txtFirstNameWinFreeShoes.Text.Trim();
            zeonWinFreeShoes.LastName = txtLastName.Text.Trim();
            zeonWinFreeShoes.EmailAddress = txtEmail.Text.Trim();
            zeonWinFreeShoes.IsWinner = false;
            zeonWinFreeShoes.CreateDate = DateTime.Now;

            return zeonWinFreeShoes;
        }
        #endregion

        #region Protected Methods
        /// <summary>
        /// Sumit button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgSubmit_Click(object sender, EventArgs e)
        {
            bool isValid = this.VerifyCaptachText();
            if (isValid)
            {
                try
                {
                    if (this.InsertIntoWinFreeShoes())
                    {
                        this.SendWinFreeShoeEmail();
                    }
                }
                catch (Exception ex) { ElmahErrorManager.Log(ex); }
            }
        }
        /// <summary>
        /// To Set Focus of the refersh button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbtnRefresh_Click(object sender, EventArgs e)
        {
            txtCharacters.Text = string.Empty;
            lblErrorMsg.Text = string.Empty;
            txtCharacters.Focus();
        }
        #endregion

    }
}