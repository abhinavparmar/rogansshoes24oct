﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Zeon.Libraries.Elmah;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace WebApp.Controls.Default.zLandingPages
{
    public partial class ZScheduleATruck : System.Web.UI.UserControl
    {

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            cmpDatesRequested.ValueToCompare = DateTime.Now.ToShortDateString();
            lblErrorMsg.Text = string.Empty;
            txtCompanyName.Focus();
        }

        /// <summary>
        /// Page Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            ZNodeResourceManager resourceManager = new ZNodeResourceManager();

            // Set SEO Properties
            ZNodeSEO seo = new ZNodeSEO();
            //seo.SEOTitle = resourceManager.GetGlobalResourceObject("SiteMap", "txtScheduleATruck");
            //PRFT Custom Code : Start
            seo.SEOTitle = resourceManager.GetGlobalResourceObject("SiteMap", "txtScheduleATruck");
            seo.SEODescription = resourceManager.GetGlobalResourceObject("SiteMap", "ScheduleATruckDescription").ToString();
            //PRFT Custom Code : End
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Verify the Captach Text
        /// </summary>
        private bool VerifyCaptachText()
        {
            CaptchaID.ValidateCaptcha(txtCharacters.Text.Trim());
            if (CaptchaID.UserValidated)
            {
                return true;
            }
            lblErrorMsg.Text = this.GetLocalResourceObject("CaptchaErrorMessage").ToString();
            txtCharacters.Focus();
            return false;
        }

        /// <summary>
        /// Send Schedule A Truck Email
        /// </summary>
        private void SendScheduleATruckEmail()
        {
            string scheduleATruckEmailTemplate = string.Empty;
            scheduleATruckEmailTemplate = CreateScheduleATruckEmailTemplate();
            ZeonMessageConfig zeonMessageconfig = new ZeonMessageConfig();

            if (!string.IsNullOrEmpty(scheduleATruckEmailTemplate))
            {
                try
                {
                    ZNodeEmail.SendEmail(ZNodeConfigManager.SiteConfig.CustomerServiceEmail, txtEmail.Text.Trim(), string.Empty, zeonMessageconfig.GetMessageKey("ScheduleATruckSubject"), scheduleATruckEmailTemplate, true);
                    pnlScheduleATruck.Visible = false;
                    pnlConfirm.Visible = true;
                }
                catch (Exception ex)
                {
                    ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "Schedule A Truck Email Template sending problem(Mehtod name: SendScheduleATruckEmail())=>ZScheduleATruck Page:" + ex.StackTrace);
                }

            }
        }

        /// <summary>
        /// Create Schedule Truck Template
        /// </summary>
        /// <returns></returns>
        private string CreateScheduleATruckEmailTemplate()
        {

            string defaultHtmlTemplatePath = string.Empty;
            string currentCulture = string.Empty;
            string htmlTemplatePath = string.Empty;

            currentCulture = ZNodeCatalogManager.CultureInfo;

            defaultHtmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ZScheduleATruck.htm");

            // modified receipt file selection as we are using in-line styles for displaying template on mail content.
            if (currentCulture != string.Empty)
            {
                htmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ZScheduleATruck_" + currentCulture + ".htm");
            }
            else
            {
                htmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ZScheduleATruck.htm");
            }
            // Check the receipt template exists, if not exists then set default receipt template
            FileInfo fileinfo = new FileInfo(htmlTemplatePath);

            if (!fileinfo.Exists)
            {
                htmlTemplatePath = defaultHtmlTemplatePath;
            }

            StreamReader streamReader = new StreamReader(htmlTemplatePath);
            string messageText = streamReader.ReadToEnd();

            string spacerImage = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/Themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/Images/space.png";
            string domainPath = Request.Url.GetLeftPart(UriPartial.Authority);
            int portalId = ZNodeConfigManager.SiteConfig.PortalID;

            ZEmailHelper zEmailHelper = new ZEmailHelper();
            ZeonMessageConfig zeonMessageConfig = new ZeonMessageConfig();
            string copyRightText = zeonMessageConfig.GetMessageKey("FooterCopyrightText");

            string messageTextHeader = zEmailHelper.EmailHeaderContent(domainPath, portalId, spacerImage);
            string messageTextFooter = zEmailHelper.EmailFooterContent(domainPath, portalId, copyRightText, spacerImage);

            //To add header to the template
            Regex rxHeaderContent = new Regex("#HeaderContent#", RegexOptions.IgnoreCase);
            messageText = rxHeaderContent.Replace(messageText, messageTextHeader);

            //To add footer to the template
            Regex rxFooterContent = new Regex("#FooterContent#", RegexOptions.IgnoreCase);
            messageText = rxFooterContent.Replace(messageText, messageTextFooter);

            Regex rxDatesRequested = new Regex("#DatesRequested#", RegexOptions.IgnoreCase);
            messageText = rxDatesRequested.Replace(messageText, txtDatesRequested.Text.Trim());

            Regex rxTimesRequested = new Regex("#TimesRequested#", RegexOptions.IgnoreCase);
            messageText = rxTimesRequested.Replace(messageText, txtTimesRequested.Text.Trim());

            Regex rxCompanyName = new Regex("#CompanyName#", RegexOptions.IgnoreCase);
            messageText = rxCompanyName.Replace(messageText, txtCompanyName.Text.Trim());

            Regex rxAddress = new Regex("#Address#", RegexOptions.IgnoreCase);
            messageText = rxAddress.Replace(messageText, txtAddress.Text.Trim());

            Regex rxPhone = new Regex("#Phone#", RegexOptions.IgnoreCase);
            messageText = rxPhone.Replace(messageText, txtPhoneNumber.Text.Trim());

            Regex rxFirstName = new Regex("#FirstName#", RegexOptions.IgnoreCase);
            messageText = rxFirstName.Replace(messageText, txtFirstNameScheduleTruck.Text.Trim());

            Regex rxLastName = new Regex("#LastName#", RegexOptions.IgnoreCase);
            messageText = rxLastName.Replace(messageText, txtLastName.Text.Trim());

            Regex rxRationMaleFemale = new Regex("#RationMaleFemale#", RegexOptions.IgnoreCase);
            messageText = rxRationMaleFemale.Replace(messageText, txtRationMaleFemale.Text.Trim());

            Regex rxNoOfEmp = new Regex("#NoOfEmp#", RegexOptions.IgnoreCase);
            messageText = rxNoOfEmp.Replace(messageText, txtNumOfEmployees.Text.Trim());

            Regex rxSteelToe = new Regex("#SteelToe#", RegexOptions.IgnoreCase);
            messageText = rxSteelToe.Replace(messageText, txtSteelToeReq.Text.Trim());

            Regex rxContributionAmount = new Regex("#ContributionAmount#", RegexOptions.IgnoreCase);
            messageText = rxContributionAmount.Replace(messageText, txtContributionAmount.Text.Trim());

            Regex rxPayrollDeduction = new Regex("#PayrollDeduction#", RegexOptions.IgnoreCase);
            messageText = rxPayrollDeduction.Replace(messageText, txtPayrollDeduction.Text.Trim());

            Regex rxPostersNeeded = new Regex("#PostersNeeded#", RegexOptions.IgnoreCase);
            messageText = rxPostersNeeded.Replace(messageText, txtPostersNeeded.Text.Trim());

            Regex rxNotes = new Regex("#Notes#", RegexOptions.IgnoreCase);
            messageText = rxNotes.Replace(messageText, txtNotes.Text.Trim());

            return messageText;
        }

        /// <summary>
        /// Insert into the Table
        /// </summary>
        /// <returns>Bool value</returns>
        private bool InsertIntoScheduleATruck()
        {
            ZeonScheduleATruck zeonScheduleATruck = this.CreateScheduleATruckObject();
            if (zeonScheduleATruck != null)
            {
                ZeonScheduleATruckService zeonScheduleATruckService = new ZeonScheduleATruckService();
                zeonScheduleATruckService.Insert(zeonScheduleATruck);
                return true;
            }
            return false;
        }

        /// <summary>
        /// To Create Schedule A Truck Object
        /// </summary>
        /// <returns></returns>
        private ZeonScheduleATruck CreateScheduleATruckObject()
        {
            ZeonScheduleATruck zeonScheduleAtruck = new ZeonScheduleATruck();

            zeonScheduleAtruck.CompanyName = txtCompanyName.Text.Trim();
            zeonScheduleAtruck.DatesRequested = Convert.ToDateTime(txtDatesRequested.Text.Trim());
            zeonScheduleAtruck.TimesRequested = txtTimesRequested.Text.Trim();
            zeonScheduleAtruck.Address = txtAddress.Text.Trim();
            zeonScheduleAtruck.Phone = txtPhoneNumber.Text.Trim();
            zeonScheduleAtruck.FirstName = txtFirstNameScheduleTruck.Text.Trim();
            zeonScheduleAtruck.LastName = txtLastName.Text.Trim();
            zeonScheduleAtruck.NumberOfEmployees = txtNumOfEmployees.Text.Trim();
            zeonScheduleAtruck.RationMaleFemale = txtRationMaleFemale.Text.Trim();
            zeonScheduleAtruck.SteelToeReq = txtSteelToeReq.Text.Trim();
            zeonScheduleAtruck.ContributionAmount = txtContributionAmount.Text.Trim();
            zeonScheduleAtruck.PayrollDeduction = txtPayrollDeduction.Text.Trim();
            zeonScheduleAtruck.EmailAddress = txtEmail.Text.Trim();
            zeonScheduleAtruck.PostersNeeded = txtPostersNeeded.Text.Trim();
            zeonScheduleAtruck.Notes = txtNotes.Text.Trim();
            zeonScheduleAtruck.CreateDate = DateTime.Now;

            return zeonScheduleAtruck;
        }
        #endregion

        #region Protected Methods
        /// <summary>
        /// To Set Focus of the refersh button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbtnRefresh_Click(object sender, EventArgs e)
        {
            txtCharacters.Text = string.Empty;
            lblErrorMsg.Text = string.Empty;
            txtCharacters.Focus();
        }

        /// <summary>
        /// Submit From Buton
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgSubmit_Click(object sender, EventArgs e)
        {
            bool isValid = this.VerifyCaptachText();
            if (isValid)
            {
                try
                {
                    if (this.InsertIntoScheduleATruck())
                    {
                        this.SendScheduleATruckEmail();
                    }
                }
                catch (Exception ex)
                {
                    ElmahErrorManager.Log(ex);
                }
            }
        }
        #endregion
    }
}