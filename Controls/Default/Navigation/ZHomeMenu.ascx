﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ZHomeMenu.ascx.cs" Inherits="WebApp.Controls.Default.Navigation.ZHomeMenu" EnableViewState="false" %>
<%@ Import Namespace="ZNode.Libraries.Framework.Business" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="Znode" %>

<div class="navbar yamm navbar-default">
    <div id="myNavmenu" class="navmenu navmenu-default navmenu-fixed-left offcanvas" role="navigation">
        <ul class=" nav navbar-nav">
            <li class="deliveryInfo">
                <Znode:CustomMessage ID="ucHeaderTopMessage" runat="server" MessageKey="HeaderTopMessage" />
            </li>
            <li class="dropdown highlight-rgtmenu hidden-lg hidden-md hidden-sm" id="liMyAccount">
                <a title="Clearance" class="dropdown-toggle" href="/account.aspx">My Account <span class="fa fa-lock"></span></a>
            </li>
            <asp:Repeater ID="menuRepeater" runat="server" OnItemDataBound="menuRepeater_ItemDataBound">
                <ItemTemplate>
                    <li class="dropdown">
                        <a title='<%# Server.HtmlDecode(GetTitleCaseName(Eval("Title").ToString()))%>'
                            class="dropdown-toggle" href='<%#ResolveUrl(ZNode.Libraries.ECommerce.SEO.ZNodeSEOUrl.MakeURL(Eval("CategoryId").ToString(),  SEOUrlType.Category,Eval("SEOURL").ToString())) %>'>
                            <%# Server.HtmlDecode(GetTitleCaseName(Eval("Title").ToString()))%>                            
                        </a>
                        <a href="javascript:void(0);" class="dropdown-link" data-toggle="dropdown"><span class="fa fa-plus-circle"></span></a>
                        <asp:Repeater ID="subMenuRepeater" OnItemDataBound="subMenuRepeater_ItemDataBound" Visible="false"
                            runat="server">
                            <HeaderTemplate>
                                <ul class="dropdown-menu">
                                    <li>
                                        <div class="yamm-content">
                                            <div class="row">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <ul class="col-sm-3 list-unstyled">
                                    <li>
                                        <a id="a" runat="server"></a>
                                        <span class="fa fa-minus"></span>
                                        <asp:Repeater ID="subMenuSubRepeater" runat="server" OnItemDataBound="subMenuSubRepeater_ItemDataBound" Visible="false">
                                            <HeaderTemplate>
                                                <ul class="CatLevelMenu">
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <li>
                                                    <a id="a" runat="server" class="LastCategory"></a>
                                                    <%--<a class="dropdown-link" href="javascript:void(0);"><span class="fa fa-plus-circle"></span></a>  --%>                                                  
                                                </li>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </ul>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </li>
                                </ul>
                            </ItemTemplate>
                            <FooterTemplate>
                                </div>
                                        </div>
                                    </li>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
            <li class="dropdown">
                <asp:Literal ID="ltrOtherMenus" runat="server"></asp:Literal>
            </li>
            <li class="dropdown  highlight-rgtmenu">
                <asp:Literal ID="ltrSecondMenu" runat="server"></asp:Literal>
            </li>
        </ul>
    </div>
    <div class="navbar navbar-default navbar-fixed-top">
        <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target="#myNavmenu" data-canvas="body">
            <div class="Btn"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></div>
            <em>Menu</em> </button>
    </div>
</div>
