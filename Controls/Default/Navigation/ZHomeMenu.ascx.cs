﻿using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Text;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.Framework.Business;

namespace WebApp.Controls.Default.Navigation
{
    public partial class ZHomeMenu : System.Web.UI.UserControl
    {
        # region Private Member Variables

        bool _IsVisible = true;

        # endregion

        # region Public Properties

        public bool IsVisible
        {
            get
            {
                return _IsVisible;
            }
            set
            {
                _IsVisible = value;
            }
        }

        # endregion

        #region Page Events

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (_IsVisible)
            {
                CreateTree();
                BindOtherMenus();
            }
        }

        /// <summary>
        /// Page Prerender event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            RegisterClientScriptClasses();
        }

        #endregion

        #region Control Events

        /// <summary>
        /// Repeater Item Data Bound Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void menuRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType.Equals(ListItemType.AlternatingItem) || e.Item.ItemType.Equals(ListItemType.Item))
            {
                DataRowView dbRowView = e.Item.DataItem as DataRowView;

                if (null == dbRowView) return;

                DataRow dbRow = dbRowView.Row;

                if (dbRow.IsNull("ParentCategoryNodeID"))
                {
                    if ((bool)dbRow["VisibleInd"])
                    {
                        Repeater rptLevel1Menu = e.Item.FindControl("subMenuRepeater") as Repeater;
                        if (null != rptLevel1Menu && dbRow.GetChildRows("NodeRelation").Length > 0)
                        {
                            rptLevel1Menu.Visible = true;
                            rptLevel1Menu.DataSource = dbRow.GetChildRows("NodeRelation");
                            rptLevel1Menu.DataBind();
                        }
                    }
                    else
                    {
                        e.Item.Visible = false;
                    }
                }
                else
                {
                    e.Item.Visible = false;
                }

            }
        }

        /// <summary>
        /// Repeater Item Data Bound Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void subMenuRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType.Equals(ListItemType.AlternatingItem) || e.Item.ItemType.Equals(ListItemType.Item))
            {
                DataRow dbRow = e.Item.DataItem as DataRow;

                if (null == dbRow) return;

                if ((bool)dbRow["VisibleInd"])
                {
                    string categoryId = dbRow["CategoryId"].ToString();
                    string seoURL = dbRow["SEOURL"].ToString();

                    seoURL = ZNodeSEOUrl.MakeURL(categoryId, SEOUrlType.Category, seoURL);
                    seoURL = seoURL.Replace("~/", "/");
                    HtmlAnchor aSub = e.Item.FindControl("a") as HtmlAnchor;
                    if (null != aSub)
                    {
                        TextInfo info = CultureInfo.CurrentCulture.TextInfo;

                        aSub.HRef = (string.IsNullOrEmpty(seoURL)) ? "#" : seoURL;
                        string name = info.ToTitleCase(Server.HtmlDecode(dbRow["Title"].ToString()));
                        string title = info.ToTitleCase(Server.HtmlDecode(dbRow["AlternateDescription"].ToString()));
                        aSub.InnerText= name;
                        aSub.Title = title;
                    }
                    if (!string.IsNullOrEmpty(dbRow["ParentCategoryNodeID"].ToString()))
                    {
                        Repeater rptLevel2Menu = e.Item.FindControl("subMenuSubRepeater") as Repeater;
                        if (null != rptLevel2Menu && dbRow.GetChildRows("NodeRelation").Length > 0)
                        {
                            rptLevel2Menu.Visible = true;
                            rptLevel2Menu.DataSource = dbRow.GetChildRows("NodeRelation");
                            rptLevel2Menu.DataBind();
                        }
                    }
                }
                else
                {
                    e.Item.Visible = false;
                }
            }
        }

        /// <summary>
        /// Repeater Item Data Bound Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void subMenuSubRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType.Equals(ListItemType.AlternatingItem) || e.Item.ItemType.Equals(ListItemType.Item))
            {
                DataRow dbRow = e.Item.DataItem as DataRow;

                if (null == dbRow) return;

                if ((bool)dbRow["VisibleInd"])
                {
                    string categoryId = dbRow["CategoryId"].ToString();
                    string seoURL = dbRow["SEOURL"].ToString();

                    seoURL = ZNodeSEOUrl.MakeURL(categoryId, SEOUrlType.Category, seoURL);
                    seoURL = seoURL.Replace("~/", "/");
                    HtmlAnchor aSub = e.Item.FindControl("a") as HtmlAnchor;
                    if (null != aSub)
                    {
                        TextInfo info = CultureInfo.CurrentCulture.TextInfo;

                        aSub.HRef = (string.IsNullOrEmpty(seoURL)) ? "#" : seoURL;
                        string name = info.ToTitleCase(Server.HtmlDecode(dbRow["Title"].ToString()));
                        string title = info.ToTitleCase(Server.HtmlDecode(dbRow["AlternateDescription"].ToString()));
                        aSub.InnerText = name;
                        aSub.Title = title;
                    }
                }
                else
                {
                    e.Item.Visible = false;
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Create Tree Menu
        /// </summary>
        private void CreateTree()
        {
            System.Data.DataSet ds = (System.Data.DataSet)HttpContext.Current.Cache["CategoryNavigation" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId];

            if (ds == null)
            {
                CategoryHelper categoryHelper = new CategoryHelper();
                ds = categoryHelper.GetNavigationItemsExtn(ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId, ZNodeProfile.CurrentUserProfileId, ConfigurationManager.AppSettings["HideCategoriesFromMenu"].ToString());

                ds.Tables[0].Constraints.Add("PK_CategoryNodeID_FK_ParentCategoryNodeID", ds.Tables[0].Columns["CategoryNodeId"], ds.Tables[0].Columns["ParentCategoryNodeID"]);

                // add the hierarchical relationship to the dataset
                ds.Relations.Add("NodeRelation", ds.Tables[0].Columns["CategoryNodeId"], ds.Tables[0].Columns["ParentCategoryNodeId"]);

                // Create two sql Cache Dependency objects for categorynode and product table
                System.Web.Caching.AggregateCacheDependency aggregateDependency = new System.Web.Caching.AggregateCacheDependency();
                aggregateDependency.Add(new System.Web.Caching.SqlCacheDependency("ZNodeMultifront", "ZNodeCategoryNode"));
                aggregateDependency.Add(new System.Web.Caching.SqlCacheDependency("ZNodeMultifront", "ZNodePortalCatalog"));
                aggregateDependency.Add(new System.Web.Caching.SqlCacheDependency("ZNodeMultifront", "ZNodeProductCategory"));
                aggregateDependency.Add(new System.Web.Caching.SqlCacheDependency("ZNodeMultifront", "ZNodeCategory"));
                aggregateDependency.Add(new System.Web.Caching.SqlCacheDependency("ZNodeMultifront", "ZNodeProduct"));

                System.Web.HttpContext.Current.Cache.Insert("CategoryNavigation" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId, ds, aggregateDependency, System.Web.Caching.Cache.NoAbsoluteExpiration,
                          System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.NotRemovable, null);
            }

            menuRepeater.DataSource = ds;
            menuRepeater.DataBind();
        }

        /// <summary>
        /// Get Home Menu Class Name
        /// </summary>
        /// <returns></returns>
        private string GetHomeMenuClassName()
        {
            string page = this.Page.Request.Url.ToString().ToLower();
            if (!page.Contains(".aspx"))
            {
                return "HomeMenuContainer";
            }
            else
            {
                if (page.Contains("default.aspx") || page.Contains("home.aspx"))
                {
                    return "HomeMenuContainer";
                }
            }
            return "";
        }

        /// <summary>
        /// Get Title Case of the Menu Name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        protected string GetTitleCaseName(string name)
        {
            string titleCaseName = name;
            name = name.ToLower();
            TextInfo info = CultureInfo.CurrentCulture.TextInfo;
            titleCaseName = info.ToTitleCase(Server.HtmlDecode(name));
            return titleCaseName;
        }

        /// <summary>
        /// Registers the Script
        /// </summary>
        private void RegisterClientScriptClasses()
        {
            StringBuilder script = new StringBuilder();
            script.Append("var zeonHomeMenu = new Menu({");
            script.Append("});zeonHomeMenu.Init();");
            this.Page.ClientScript.RegisterStartupScript(GetType(), "ZeonHomeMenu", script.ToString(), true);
        }

        /// <summary>
        /// Bind Non categorial Menus
        /// </summary>
        private void BindOtherMenus()
        {
            MessageConfigAdmin messageadmin = new MessageConfigAdmin();
            MessageConfig messageconfig = new MessageConfig();
            messageconfig = messageadmin.GetByKeyPortalIDLocaleID("OtherMenuContent", ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);
            if (messageconfig != null)
            {
                ltrOtherMenus.Text = messageconfig.Value;
            }
            messageconfig = messageadmin.GetByKeyPortalIDLocaleID("OtherSecondMenuContent", ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);
            if (messageconfig != null)
            {
                ltrSecondMenu.Text = messageconfig.Value;
            }
        }

        #endregion
    }
}