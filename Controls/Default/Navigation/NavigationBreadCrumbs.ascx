<%@ Control Language="C#" AutoEventWireup="true" EnableViewState="false" Inherits="WebApp.Controls_Default_Navigation_NavigationBreadCrumbs" Codebehind="NavigationBreadCrumbs.ascx.cs" %>

<div class="BreadCrumbLabel"><asp:Label ID="lblPath" runat="server" EnableViewState="false"></asp:Label><asp:PlaceHolder runat="server" ID="ControlsPlaceHolder" EnableViewState="false"></asp:PlaceHolder></div>
