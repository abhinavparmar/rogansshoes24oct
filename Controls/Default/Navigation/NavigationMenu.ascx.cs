using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the NavigationMenu user control class.
    /// </summary>
    public partial class Controls_Default_Navigation_NavigationMenu : System.Web.UI.UserControl
    {
        #region Private Member Variables
        private bool _EnableMenuViewState = true;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets a value indicating whether the menu control persists its view
        /// state, to the requesting client.
        /// <remarks>true if the server control maintains its view state; otherwise false. The default is true.</remarks>
        /// </summary>
        public bool EnableMenuViewState
        {
            get
            {
                return this._EnableMenuViewState;
            }

            set
            {
                this._EnableMenuViewState = value;
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (!Page.IsPostBack || !this._EnableMenuViewState)
            {
                this.ctrlMenu.EnableViewState = this._EnableMenuViewState;

                this.BindData();
            }
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the treeview
        /// </summary>
        private void BindData()
        {
            // Clear the exisitng menu items
            ctrlMenu.Items.Clear();

            // Menu properties
            ctrlMenu.Orientation = Orientation.Horizontal;

            string theme = ZNodeCatalogManager.Theme;
            
            // If category theme wise theme selected then apply the current category theme.
            if (Session["CurrentCategoryTheme"] != null)
            {
                theme = Session["CurrentCategoryTheme"].ToString();
            }

            // Add default item.
            MenuItem menuItemDefault = new MenuItem();
            menuItemDefault.ImageUrl = "~/themes/" + theme + "/Images/HomeIcon.gif";
            menuItemDefault.ToolTip = Resources.CommonCaption.HomeToolTip;

            menuItemDefault.SeparatorImageUrl = "~/themes/" + theme + "/Images/menu_seperator.gif";

            menuItemDefault.NavigateUrl = "~/";
            ctrlMenu.Items.Add(menuItemDefault);

            string zcid = "0";

            // Get category id from querystring  
            if (Request.Params["zcid"] != null)
            {
                zcid = Request.Params["zcid"];
            }
            else if (Request.QueryString["zpid"] != null)
            {
                if (Session["BreadCrumzcid"] != null)
                {
                    // Get categoryid from session
                    zcid = Session["BreadCrumzcid"].ToString();
                }                
            }

            ZNodeNavigation navigation = new ZNodeNavigation();
            navigation.PopulateStoreMenu(ctrlMenu, zcid);
        }
        #endregion
    }
}