﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrftNavigationMenu.ascx.cs" Inherits="WebApp.Controls.Default.Navigation.PrftNavigationMenu" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="Znode" %>
<%@ Register Src="~/Controls/Default/Common/LoginStatus.ascx" TagName="logout" TagPrefix="uc1" %>

<div class="navbar yamm navbar-default megamenu-custom">
    <div id="myNavmenu" class="navmenu navmenu-default navmenu-fixed-left offcanvas" role="navigation">
        <ul class=" nav navbar-nav">
            <li class="deliveryInfo">
                <Znode:CustomMessage ID="ucHeaderTopMessage" runat="server" MessageKey="HeaderTopMessage" />
            </li>
            <li class="dropdown highlight-rgtmenu hidden-lg hidden-md hidden-sm" id="liStoreLocation">
                <a title="Clearance" class="dropdown-toggle" href="/storelocator.aspx">Store Locations <span class="fa fa-map-marker"></span></a>
            </li>
            <li class="dropdown highlight-rgtmenu hidden-lg hidden-md hidden-sm" id="liMyAccount">
                <a title="Clearance" class="dropdown-toggle" href="/account.aspx">My Account <span class="fa fa-lock"></span></a>
            </li>
            <li class="dropdown SubMenu shop-by-brand-menu">
                <a title="Shop By Brand" class="dropdown-toggle" href="/shopbybrand.aspx">Shop By Brand</a>
                <a href="javascript:void(0);" class="dropdown-link" data-toggle="dropdown"><span class="fa fa-plus-circle"></span></a>
                <ul class="dropdown-menu">
                    <li>
                        <Znode:CustomMessage ID="uxMegaMenuBrand" runat="server" MessageKey="MegaMenuBrand" />
                    </li>
                </ul>
            </li>
            <li class="dropdown SubMenu womens-shoes-menu">
                <a title="Womens" class="dropdown-toggle" href="/womens-shoes">Womens</a>
                <a href="javascript:void(0);" class="dropdown-link" data-toggle="dropdown"><span class="fa fa-plus-circle"></span></a>
                <ul class="dropdown-menu">
                    <li>
                        <Znode:CustomMessage ID="ucWomensShoes" runat="server" MessageKey="MegaMenuWomenShoes" />
                    </li>
                </ul>
            </li>
            <li class="dropdown SubMenu mens-shoes-menu">
                <a title="Mens" class="dropdown-toggle" href="/mens-shoes">Mens</a>
                <a href="javascript:void(0);" class="dropdown-link" data-toggle="dropdown"><span class="fa fa-plus-circle"></span></a>
                <ul class="dropdown-menu">
                    <li>
                        <Znode:CustomMessage ID="uxMegaMenuMensShoes" runat="server" MessageKey="MegaMenuMensShoes" />
                    </li>
                </ul>
            </li>
        <li class="dropdown SubMenu kids-shoes-menu">
                <a title="Kids" class="dropdown-toggle" href="/kids-shoes">Kids</a>
                <a href="javascript:void(0);" class="dropdown-link" data-toggle="dropdown"><span class="fa fa-plus-circle"></span></a>
                <ul class="dropdown-menu">
                    <li>
                        <Znode:CustomMessage ID="uxMegaMenuKidsShoes" runat="server" MessageKey="MegaMenuKidsShoes" />
                    </li>
                </ul>
            </li>
            <li class="dropdown SubMenu clothing-and-accessories-menu">
                <a title="Clothing &amp; Accessories" class="dropdown-toggle" href="/clothing-and-accessories">Clothing &amp; Accessories</a>
                <a href="javascript:void(0);" class="dropdown-link" data-toggle="dropdown"><span class="fa fa-plus-circle"></span></a>
                <ul class="dropdown-menu">
                    <li>
                        <Znode:CustomMessage ID="uxMegaMenuClothingAndAccessories" runat="server" MessageKey="MegaMenuClothingAndAccessories" />
                    </li>
                </ul>
            </li>
            <li class="dropdown highlight-rgtmenu">
                <a title="Clearance" class="dropdown-toggle" href="/clearance-shoes">Clearance</a>
            </li>

            <%-- <Znode:CustomMessage ID="uxMegaMenuClearance" runat="server" MessageKey="MegaMenuClearance" />--%>
           
            
            <%--<li class="dropdown highlight-rgtmenu" id="liLogOut">
                <uc1:logout ID="logout" runat="server" />
            </li>--%>
        </ul>
    </div>
    <div class="navbar navbar-default navbar-fixed-top">
        <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target="#myNavmenu" data-canvas="body">
            <div class="Btn"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></div>
            <em>Menu</em>
        </button>
    </div>
</div>
