<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Navigation_NavigationBrand" Codebehind="NavigationBrand.ascx.cs" %>
<div class="BrandNavigation">
    <asp:Panel ID="pnlTreeView" runat="server">
        <div class="BrandTreeView">
            <asp:TreeView ID="ctrlNavigation" runat="server" ExpandDepth="2" CssClass="TreeView" NodeIndent="14" ShowExpandCollapse="False" ClientIDMode="Static">
                 <ParentNodeStyle CssClass="ParentNodeStyle" />
                 <HoverNodeStyle CssClass="HoverNodeStyle"/>
                 <SelectedNodeStyle CssClass="SelectedNodeStyle" />
                 <RootNodeStyle CssClass="RootNodeStyle"/>
                 <LeafNodeStyle CssClass="LeafNodeStyle"/>
                 <NodeStyle CssClass="NodeStyle" />
            </asp:TreeView>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <asp:DropDownList ID="ctrlBrandList" CssClass="DropDownList" runat="server" OnSelectedIndexChanged="BrandList_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></asp:Panel>      
</div>
