﻿using System;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
namespace WebApp.Controls.Default.QuickWatch
{
    public partial class ZQuickWatch : System.Web.UI.UserControl
    {
        #region Constant Variables

        private const string _StoreLocatorPageLink = "~/storelocator.aspx";
        private const string _URLStartString = "http";
        private const string _SecuredURLString = "https://";
        private const string _NonSecuredURLString = "http://";
        private const int _YoutubeEmbdeTagLength = 11;

        #endregion

        #region Private Variables

        private const string PostBackEventTarget = "__EVENTTARGET";
        private int _productId;
        private ZNodeShoppingCart _shoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
        //private ZNodeProduct Product;
        private ZNodeProfile Profile = new ZNodeProfile();
        private string zcid = string.Empty;
        private ZNodeProduct _Product;
        private string comparePageURL = "/productcomparison.aspx";
        //Perficient Custom Member
        bool allowOutOfStockToUser = false;

        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets Shopping cart quantity
        /// </summary>
        public int ShoppingCartQuantity
        {
            get
            {
                if (ViewState["Quantity"] != null)
                {
                    return (int)ViewState["Quantity"];
                }

                return 1;
            }

            set
            {
                ViewState["Quantity"] = value;
            }
        }

        public string ProductIDVal
        {
            get
            {
                if (this.Product != null)
                {
                    return this.Product.ProductID.ToString();
                }
                ZNodeProduct dummyProduct = new ZNodeProduct();
                return dummyProduct.ProductID.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the ZNodeProduct
        /// </summary>
        public ZNodeProduct Product
        {
            get { return _Product; }
            set { _Product = value; }
        }

        #endregion

        #region  Public Methods

        /// <summary>
        /// Represents the CheckInventory Method
        /// </summary>
        /// <param name="Product">Product Instance</param>
        /// <returns>Returns the Inventory stock message</returns>
        public bool CheckInventory(ZNodeProduct product)
        {
            this.DisableAddToCartButton(true);

            lblstockmessage.CssClass = "ProductStock";
            // Don't track Inventory - Items can always be added to the cart,TrackInventory is disabled
            if (allowOutOfStockToUser)
            {
                lblstockmessage.Text = product.InStockMsg;
            }
            if (!product.TrackInventoryInd && !product.AllowBackOrder && product.QuantityOnHand > 0)
            {
                lblstockmessage.Text = product.InStockMsg;
            }
            else if (!product.TrackInventoryInd && !product.AllowBackOrder && product.QuantityOnHand <= 0)
            {
                lblstockmessage.Text = string.Empty;
                lblstockmessage.CssClass = string.Empty; ;
            }
            else if (product.TrackInventoryInd && !product.AllowBackOrder && product.QuantityOnHand >= this.ShoppingCartQuantity)
            {
                lblstockmessage.Text = product.InStockMsg;
            }
            else if (product.AllowBackOrder && product.QuantityOnHand >= this.ShoppingCartQuantity)
            {
                // Set Inventory stock message
                lblstockmessage.Text = product.InStockMsg;
            }
            else if (product.AllowBackOrder && product.QuantityOnHand < this.ShoppingCartQuantity)
            {
                lblstockmessage.Text = product.BackOrderMsg;
            }
            else
            {
                // Display Out of stock message
                lblstockmessage.Text = product.OutOfStockMsg;
                lblstockmessage.CssClass = "OutOfStockMsg";

                // Hide AddToCart button
                //uxAddToCart.Visible = false;

                this.DisableAddToCartButton(true);

                return false;
            }
            upnlStockMessage.Update();
            return true;
        }

        /// <summary>
        /// Update and Bind inventory status message and product price
        /// </summary>
        /// <returns>Returns the boolean to update the product status or not</returns>
        public bool UpdateProductStatus()
        {
            //Enable Add To Cart Button
            this.DisableAddToCartButton(false);

            // Update the panels
            UpdPnlOrderingOptions.Update();

            // Hide Product price
            uxProductPrice.Visible = this.Profile.ShowPrice && !this.Product.CallForPricing;
            uxAddToCart.Visible = this.Profile.ShowPrice && !this.Product.CallForPricing;

            int qty = 0;
            int.TryParse(txtQuantity.Text, out qty);
            this.ShoppingCartQuantity = qty;

            bool status = true;

            ZNodeSKU SKU = ZNodeSKU.CreateByProductDefault(this.Product.ProductID);

            // Check if product has add-ons
            if (this.Product.ZNodeAddOnCollection.Count > 0)
            {
                // If product has Add-Ons, then validate that all Add-Ons Values have been selected by the customer
                string addOnMessage = string.Empty;
                ZNodeAddOnList selectedAddOn = null;

                status = uxProductAddOns.ValidateAddOns(out addOnMessage, this.ShoppingCartQuantity, out selectedAddOn);

                if (!status)
                {
                    uxStatus.Visible = true;

                    uxStatus.Text = addOnMessage;

                    // Hide AddToCart button
                    //uxAddToCart.Visible = false;

                    this.DisableAddToCartButton(true);

                    return status;
                }

                // Set Selected Add-on
                this.Product.SelectedAddOnItems = selectedAddOn;
            }

            // Check if product has attributes
            if (this.Product.ZNodeAttributeTypeCollection.Count > 0)
            {
                // If product has attributes then validate that all attributes have been selected by
                // the customer validate attributes first - This is to verify that all required attributes have a valid selection
                // if a valid selection is not found then an error message is displayed
                string attributeMessage = string.Empty;
                string attributeList = string.Empty;
                string selectedAttributesDescription = string.Empty;

                status = uxProductAttributes.ValidateAttributes(out attributeMessage, out attributeList, out selectedAttributesDescription);

                if (!status)
                {
                    //if(!string.IsNullOrEmpty(attributeMessage))
                    //    uxStatus.Visible = true;

                    //uxStatus.Text = attributeMessage;

                    // Hide AddToCart button
                    //uxAddToCart.Visible = false;

                    this.DisableAddToCartButton(true);

                    return status;
                }

                // Get a sku based on attributes selected
                SKU = ZNodeSKU.CreateByProductAndAttributes(this.Product.ProductID, attributeList);

                // Set Attributes Description to Description property
                SKU.AttributesDescription = selectedAttributesDescription;

                if (SKU.SKUID == 0)
                {
                    lblstockmessage.Text = "Selected options is not available, please try different options";
                    lblstockmessage.CssClass = "OutOfStockMsg";

                    // Hide AddToCart button
                    //uxAddToCart.Visible = false;

                    this.DisableAddToCartButton(true);

                    status = false;
                }
                else // Check stock    
                    if (!allowOutOfStockToUser && SKU.QuantityOnHand < this.ShoppingCartQuantity && (!this.Product.AllowBackOrder) && this.Product.TrackInventoryInd)
                    {
                        // Display Out of stock message
                        lblstockmessage.Text = this.Product.OutOfStockMsg;
                        lblstockmessage.CssClass = "OutOfStockMsg";
                        uxStatus.Text = this.Product.OutOfStockMsg;

                        // Hide AddToCart button
                        //uxAddToCart.Visible = false;

                        this.DisableAddToCartButton(true);

                        status = false;
                    }
                    else
                    {
                        lblstockmessage.Text = this.Product.InStockMsg;// + qtyMessage;
                        lblstockmessage.CssClass = "ProductStock";
                    }
            }
            else
            {
                // If Product has no SKUs associated with this product.           
                SKU.AttributesDescription = string.Empty;

                // Check stock    
                if (this.Product.QuantityOnHand < this.ShoppingCartQuantity && (this.Product.AllowBackOrder == false) && this.Product.TrackInventoryInd)
                {
                    lblstockmessage.Text = this.Product.OutOfStockMsg; // Display Out of stock message

                    lblstockmessage.CssClass = "OutOfStockMsg";
                    uxStatus.Text = this.Product.OutOfStockMsg;

                    // Hide AddToCart button
                    //uxAddToCart.Visible = false;

                    this.DisableAddToCartButton(true);

                    status = false;
                }
            }

            this.Product.SelectedSKU = SKU;
            this.Product.CheckSKUProfile();
            this.Product.IsPromotionApplied = false;
            this.Product.ApplyPromotion();

            // Check if product has child product (Bundle product)
            if (this.Product.ZNodeBundleProductCollection.Count > 0)
            {
                string addOnMessage = string.Empty;

                status = uxBundleProduct.ValidateAddons(out addOnMessage);

                if (!status)
                {
                    uxStatus.Visible = true;

                    // Error Message
                    uxStatus.Text = addOnMessage;

                    // Hide AddToCart button
                    //uxAddToCart.Visible = false;

                    this.DisableAddToCartButton(true);

                    return status;
                }

                string attributeMessage = string.Empty;

                status = uxBundleProduct.ValidateAttributes(out attributeMessage);

                if (!status)
                {
                    uxStatus.Visible = true;

                    uxStatus.Text = attributeMessage;

                    // Hide AddToCart button
                    //uxAddToCart.Visible = false;

                    this.DisableAddToCartButton(true);

                    return status;
                }
            }

            if (status)
            {
                status = this.CheckInventory(this.Product);

                if (this.Product.ZNodeBundleProductCollection.Count > 0 && status)
                {
                    bool applypromotion = false;
                    foreach (ZNode.Libraries.ECommerce.Entities.ZNodeProductBaseEntity _bundleProduct in this.Product.ZNodeBundleProductCollection)
                    {
                        ZNodeProduct _znodeBundleProduct = ZNodeProduct.Create(_bundleProduct.ProductID, applypromotion);

                        _znodeBundleProduct.SelectedSKUvalue = _bundleProduct.SelectedSKUvalue;

                        status = status && this.CheckInventory(_znodeBundleProduct);
                    }
                }
            }

            // Check for 'Call for Pricing'
            if (this.Product.CallForPricing)
            {
                lblstockmessage.Visible = false;
                uxAddToCart.Visible = false;
                uxCallForPricing.Visible = true;
                uxStatus.Visible = false;

                if (!string.IsNullOrEmpty(this.Product.CallMessage))
                {
                    uxCallForPricing.Text = this.Product.CallMessage;
                }
            }
            else
            {
                lblstockmessage.Visible = true;
            }

            // Bind selected sku product price if one exists
            uxProductPrice.Quantity = this.ShoppingCartQuantity;
            uxProductPrice.Product = this.Product;
            uxProductPrice.Bind();

            // Set product properties       
            ProductTitle.Text = string.Format(ProductTitle.Text, this.Product.Name);
            ProductID.Text = this.Product.ProductNum;

            // Set Manufacturer Name
            //if (this.Product.ManufacturerName != string.Empty)
            //{
            //    BrandLabel.Visible = true;

            //    //Zeon custom Code:Start
            //    SetBrandLinkAttribute();
            //    //Zeon custom Code:End
            //}
            upnlStockMessage.Update();
            return status;
        }

        /// <summary>
        /// Disable Add To Cart Button
        /// </summary>
        /// <param name="isDisable"></param>
        /// <summary>
        /// Disable Add To Cart Button
        /// </summary>
        /// <param name="isDisable"></param>
        public void DisableAddToCartButton(bool isDisable)
        {
            if (isDisable)
            {
                uxAddToCart.Enabled = true;
                divAddToCart.Attributes.Remove("class");
            }
            else
            {
                uxAddToCart.Enabled = true;
                divAddToCart.Attributes.Remove("class");
            }
        }

        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Adds Item to Shopping Cart
        /// </summary>

        public void AddtoCart(object sender, EventArgs e)
        {
            string strlog = "\nAdd to Cart Button Click Details : Quick View. \nIP : {0} \nBrowser: {1}, \nVersion : {2}, \nType : {3}, \nProductNum : {4}, \nQty : {5}\n\n";
            strlog = string.Format(strlog, Request.UserHostAddress, Request.Browser.Browser, Request.Browser.Version, Request.UserAgent, this.Product.ProductNum, txtQuantity.Text);
            Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(strlog);


            // Reset status
            uxStatus.Text = string.Empty;
            try
            {
                if (this.UpdateProductStatus())
                {
                    // Create shopping cart item
                    ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
                    item.Product = new ZNodeProductBase(this.Product);
                    item.Quantity = this.ShoppingCartQuantity;

                    // Add product to cart
                    ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

                    // If shopping cart is null, create in session
                    if (shoppingCart == null)
                    {
                        shoppingCart = new ZNodeShoppingCart();
                        shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
                    }

                    // Add item to cart
                    if (shoppingCart.AddToCart(item))
                    {
                        //Update Display Order
                        ZNodeDisplayOrder displayOrder = new ZNodeDisplayOrder();
                        displayOrder.SetDisplayOrder(this.Product);

                        ZNodeSavedCart.AddToSavedCart(item);
                        //Zeon custom Code:Starts
                        //string link = "~/shoppingcart.aspx";
                        //Response.Redirect(link);
                        LoadCustomAddtoCartSetting();

                        strlog = "\nItem added into cart Details : Quick View. \nIP : {0} \nBrowser: {1}, \nVersion : {2}, \nType : {3}, \nProductNum : {4}, \nQty : {5}\n\n";
                        strlog = string.Format(strlog, Request.UserHostAddress, Request.Browser.Browser, Request.Browser.Version, Request.UserAgent, this.Product.ProductNum, txtQuantity.Text);
                        Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(strlog);

                        //Zeon custom Code:Ends
                    }
                    else
                    {
                        strlog = "\nItem not Added into Cart Details due to {6} : Quick View. \nIP : {0} \nBrowser: {1}, \nVersion : {2}, \nType : {3}, \nProductNum : {4}, \nQty : {5}\n\n";
                        strlog = string.Format(strlog, Request.UserHostAddress, Request.Browser.Browser, Request.Browser.Version, Request.UserAgent, this.Product.ProductNum, txtQuantity.Text, this.Product.OutOfStockMsg);
                        Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(strlog);

                        // Display Out of Stock message
                        if (!string.IsNullOrEmpty(uxStatus.Text))
                            uxStatus.Visible = true;
                        uxStatus.Text = this.Product.OutOfStockMsg;
                        lblstockmessage.Visible = false;
                        return;
                    }
                }
                else
                {
                    // Display Out of Stock message
                    if (!string.IsNullOrEmpty(uxStatus.Text))
                        uxStatus.Visible = true;

                    strlog = "\nItem validation in Cart Details due to {6} : Quick View. \nIP : {0} \nBrowser: {1}, \nVersion : {2}, \nType : {3}, \nProductNum : {4}, \nQty : {5}\n\n";
                    strlog = string.Format(strlog, Request.UserHostAddress, Request.Browser.Browser, Request.Browser.Version, Request.UserAgent, this.Product.ProductNum, txtQuantity.Text, uxStatus.Text);
                    Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(strlog);

                    return;
                }
            }
            catch (Exception ex)
            {
                strlog = "\nItem not Added into Cart Details. \nIP : {0} \nBrowser: {1}, \nVersion : {2}, \nType : {3}, \nProductNum : {4}, \nQty : {5}\n\n";
                strlog = string.Format(strlog, Request.UserHostAddress, Request.Browser.Browser, Request.Browser.Version, Request.UserAgent, this.Product.ProductNum, txtQuantity.Text);
                Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(strlog);
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return;
            }
        }

        /// <summary>
        /// Triggered when addtoCart is fired on the product listing page using query string
        /// </summary>
        protected void Buy_Click()
        {
            // Reset status
            uxStatus.Text = string.Empty;

            // Check product attributes count
            if (this.Product.ZNodeAttributeTypeCollection.Count > 0)
            {
                return;
            }

            // Loop through the product addons
            foreach (ZNodeAddOn _addOn in this.Product.ZNodeAddOnCollection)
            {
                if (!_addOn.OptionalInd)
                {
                    return;
                }
            }

            // Create shopping cart item
            ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
            item.Product = new ZNodeProductBase(this.Product);
            item.Quantity = 1;

            // Add product to cart
            ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

            // If shopping cart is null, create in session
            if (shoppingCart == null)
            {
                shoppingCart = new ZNodeShoppingCart();
                shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
            }

            // Add item to cart
            if (shoppingCart.AddToCart(item))
            {
                // Update SavedCart items
                ZNodeSavedCart.AddToSavedCart(item);

                string link = "~/shoppingcart.aspx";
                Response.Redirect(link);
            }
            else
            {
                // Display Out of Stock message
                uxStatus.Visible = true;
                uxStatus.Text = this.Product.OutOfStockMsg;
                return;
            }
        }

        /// <summary>
        /// Represents the AddToViewList Method
        /// </summary>
        protected void AddToViewedList()
        {
            ZNodeProductList productList = null;
            if (Session["ProductViewedList" + ZNodeCatalogManager.LocaleId.ToString()] != null)
            {
                productList = Session["ProductViewedList" + ZNodeCatalogManager.LocaleId.ToString()] as ZNodeProductList;
                ZNodeProductBase objectToAdd = new ZNodeProductBase(this.Product);
                objectToAdd.NewProductInd = this.Product.NewProductInd;
                objectToAdd.FeaturedInd = this.Product.FeaturedInd;
                objectToAdd.CallForPricing = this.Product.CallForPricing;
                bool isItemExists = false;
                int index = 0;

                foreach (ZNodeProductBase product in productList.ZNodeProductCollection)
                {
                    if (product.ProductID == objectToAdd.ProductID)
                    {
                        isItemExists = true;
                        objectToAdd.DisplayOrder = Convert.ToInt32(Session["DisplayOrder"]) + 1;
                        productList.ZNodeProductCollection[index] = objectToAdd; // Update product details                    
                        break;
                    }

                    index++;
                }

                if (!isItemExists)
                {
                    objectToAdd.DisplayOrder = Convert.ToInt32(Session["DisplayOrder"]) + 1;
                    productList.ZNodeProductCollection.Add(objectToAdd);
                }

                Session["DisplayOrder"] = objectToAdd.DisplayOrder.ToString();
            }
            else
            {
                productList = new ZNodeProductList();

                Session["DisplayOrder"] = "1";

                ZNodeProductBase addProduct = new ZNodeProductBase(this.Product);
                addProduct.NewProductInd = this.Product.NewProductInd;
                addProduct.FeaturedInd = this.Product.FeaturedInd;
                addProduct.CallForPricing = this.Product.CallForPricing;
                addProduct.DisplayOrder = Convert.ToInt32(Session["DisplayOrder"]);
                productList.ZNodeProductCollection.Add(addProduct);

                Session["ProductViewedList" + ZNodeCatalogManager.LocaleId.ToString()] = productList;
            }
        }

        /// <summary>
        /// Add to cart button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxAddToCart_Click(object sender, EventArgs e)
        {
            //this.AddtoCart();
            this.AddtoCart(sender, e);
        }

        /// <summary>
        /// Quantity Drop down list index changed event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Quantity_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.UpdateProductStatus();
        }

        /// <summary>
        /// Buy from Vendor site.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxBuyDirectFromVendor_Click(object sender, EventArgs e)
        {
            decimal price = uxProductPrice.ProductPrice;
            ZNode.Libraries.ECommerce.Analytics.ZNodeOutboundTracking outboundTracking = new ZNode.Libraries.ECommerce.Analytics.ZNodeOutboundTracking();
            outboundTracking.LogTrackingEvent("Product Referral", HttpContext.Current.Request.Url.ToString(), this.Product.AffiliateUrl, this.Product.ProductID, price);
        }

        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.BindDefaultValues();

            this.BindProduct();

            // Bind reference user controls
            this.BindControls();

            uxStatus.Visible = false;

            // Check if add to cart action was requested from a different page
            if (Request.Params["action"] != null)
            {
                if (Request.Params["action"].Equals("addtocart"))
                {
                    if (!Page.IsPostBack)
                    {
                        this.Buy_Click();
                    }
                }
            }

            //Zeon Custom Code: Start
            if (!IsPostBack && this.Product != null && !this.Product.CallForPricing != null)
            {
                bool isCustomPortal = ZCommonHelper.IsCustomMulitipleAttributePortal();
                if (!isCustomPortal)
                {
                    BindTierPriceCollection();
                }
            }
            this.GetSizeOfImage();
            //Assign ProductId to control
            uxAternateImages.ProductID = this._productId;
            ManageAddToCartText();
            //BindVideoURLAndThumbnailImage();
            AssignStoreLocatorNavigationURL();
            lblStoreAvailErrorMsg.Visible = false;
            hlnkStoreLocator.Visible = false;

            if (!IsPostBack)
            {
                this.DisableAddToCartButton(true);
            }
            HideControlByCurrentPortals();
            allowOutOfStockToUser = Roles.IsUserInRole("ADMIN") || Roles.IsUserInRole("CUSTOMER SERVICE REP") ? true : false;

            //Zeon Custom Code: Ends
        }

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {

            if (Session["BreadCrumzcid"] != null)
            {
                this.zcid = Session["BreadCrumzcid"].ToString();
            }
            else
            {
                ZNode.Libraries.DataAccess.Service.ProductCategoryService service = new ZNode.Libraries.DataAccess.Service.ProductCategoryService();
                TList<ProductCategory> pcategories = service.GetByProductID(Convert.ToInt32(Request.QueryString["zpid"]));
                if (pcategories.Count > 0)
                {
                    this.zcid = pcategories[0].CategoryID.ToString();
                }
            }

            // Retrieve category data from httpContext (set previously in the page_preinit event)
            ZNode.Libraries.DataAccess.Service.CategoryService cservice = new ZNode.Libraries.DataAccess.Service.CategoryService();
            ZNode.Libraries.DataAccess.Entities.Category category = cservice.GetByCategoryID(Convert.ToInt32(zcid));
            AssignStoreWiseInventory();
            RegisterClientScriptClasses();
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Bind all the controls to the data
        /// </summary>
        private void BindControls()
        {
            // Review star rating 
            uxReviewRating.ProductName = this.Product.Name;
            uxReviewRating.TotalReviews = this.Product.TotalReviews;
            uxReviewRating.ReviewRating = this.Product.ReviewRating;
            uxReviewRating.ViewProductLink = this.Product.ViewProductLink;
            ExternalLinks1.IsQuickWatch = true;
            string url = "~/customerreview.aspx?zpid=" + this.Product.ProductID;
            Hyperlink.HRef = "javascript:self.parent.location ='" + ResolveUrl(url) + "'";
            // Registers the event for the payment (child) control
            this.uxProductAttributes.SelectedIndexChanged += new EventHandler(this.UxProductAttributes_SelectedIndexChanged);
            this.uxProductAddOns.SelectedIndexChanged += new EventHandler(this.UxProductAddOns_SelectedIndexChanged);
            this.uxBundleProduct.SelectedIndexChanged += new EventHandler(this.UxBundleProduct_SelectedIndexChanged);
            this.uxProductAttributes.ColorSelectedIndexChanged += new EventHandler(this.uxProductAttributes_ColorSelectedIndexChanged);


            // Highlights         
            uxHighlights.Product = this.Product;
            uxHighlights.Bind();
            if (uxHighlights.Product != null)
            {
                uxHighlights.Visible = true;
            }


            // Price
            uxProductPrice.Visible = this.Profile.ShowPrice && !this.Product.CallForPricing;
            pnlQty.Visible = this.Profile.ShowPrice;
            //Zeon Custom Code
            uxProductPrice.IsQuickWatch = true;
        }

        /// <summary>
        /// Bind a product
        /// </summary>
        private void BindProduct()
        {
            // Get product id from querystring  
            if (Request.Params["zpid"] != null)
            {
                this._productId = int.Parse(Request.Params["zpid"]);
                if (this.Product == null)
                {
                    // Retrieve product data
                    this.Product = ZNodeProduct.Create(this._productId, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);
                    if (this.Product != null)
                    {
                        this.Product.ProductAllAttributeCollection = GetProductAttributeCollectionByProductID();
                        // Add to http context so it can be shared with user controls
                        HttpContext.Current.Items.Add("Product", this.Product);
                    }
                }
            }
            else
            {
                throw new ApplicationException("Invalid Product Id");
            }

            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this.Product = (ZNodeProduct)HttpContext.Current.Items["Product"];

            if (!Page.IsPostBack)
            {
                this.AddToViewedList();
                ZNodeDisplayOrder displayOrder = new ZNodeDisplayOrder();
                displayOrder.SetDisplayOrder(this.Product);
            }
            this.BindProductData();
        }

        /// <summary>
        /// Represents the BindDefaultValues Method
        /// </summary>
        private void BindDefaultValues()
        {
            // This code must be placed here for SEO Url Redirect to work and fix postback problem with URL rewriting
            if (this.Page.Form != null)
            {
                this.Page.Form.Action = Request.RawUrl;
            }

            // Set button image
            uxAddToCart.Attributes.Add("onclick", "return window.parent.quickViewHelper.Detect()");
            FeaturedItemImage.ImageUrl = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/sale.png";
            //NewItemImage.ImageUrl = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/new.png";
        }

        /// <summary>
        /// Represents the BindProductData method
        /// </summary>
        private void BindProductData()
        {
            // set product properties       
            ProductTitle.Text = string.Format(ProductTitle.Text, this.Product.Name);
            ProductID.Text = this.Product.ProductNum;
            // Set Manufacturer Name
            //if (this.Product.ManufacturerName != string.Empty)
            //{
            //    BrandLabel.Visible = true;

            //    SetBrandLinkAttribute();
            //}

            CheckInventory(this.Product);
            if (!Page.IsPostBack)
            {
                this.BindStockData();
            }
        }

        /// <summary>
        /// Bind all the controls to the data
        /// </summary>
        private void BindStockData()
        {
            // Product highlights         
            uxHighlights.Product = this.Product;
            uxHighlights.Bind();

            uxProductPrice.Visible = this.Profile.ShowPrice && !this.Product.CallForPricing;
            pnlQty.Visible = this.Profile.ShowPrice;

            bool showCartButton = this.Profile.ShowAddToCart;

            // If out of stock and backorder disabled then hide the AddToCart button
            if (this.Product.ZNodeAttributeTypeCollection.Count > 0)
            {
                // If product has attributes,then display the AddToCart button
                showCartButton &= true;
            }
            else if (this.Product.AllowBackOrder && this.Product.QuantityOnHand <= 0)
            {
                // Allow back Order
                showCartButton &= true;

                // Set back order message
                lblstockmessage.Text = this.Product.BackOrderMsg;
            }
            else if (this.Product.AllowBackOrder && this.Product.QuantityOnHand > 0)
            {
                showCartButton &= true;

                // Set Item In-Stock message
                lblstockmessage.Text = this.Product.InStockMsg;
            }
            else if (this.Product.QuantityOnHand > 0 && this.Product.TrackInventoryInd == true && !this.Product.AllowBackOrder)
            {
                // Track Inventory
                showCartButton &= true;

                // Set In stock message
                lblstockmessage.Text = this.Product.InStockMsg;
            }
            else if (this.Product.AllowBackOrder == false && this.Product.TrackInventoryInd == false && this.Product.QuantityOnHand > 0)
            {
                // Don't track Inventory Items can always be added to the cart,TrackInventory is disabled
                showCartButton &= true;

                // Set In stock message
                lblstockmessage.Text = this.Product.InStockMsg;
            }
            else if (this.Product.AllowBackOrder == false && this.Product.TrackInventoryInd == false && this.Product.QuantityOnHand <= 0)
            {
                // Don't track Inventory. Items can always be added to the cart,TrackInventory is disabled
                showCartButton &= true;

                // Set OutOf Stock message
                lblstockmessage.Text = string.Empty;
            }
            else
            {
                // Set OutOf Stock message
                lblstockmessage.Text = this.Product.OutOfStockMsg;
                lblstockmessage.CssClass = "OutOfStockMsg";
                showCartButton &= false;
            }

            // Set Visible true for NewItem.
            if (this.Product.NewProductInd)
            {
                //NewItemImage.Visible = true;
                divProductPageNewItem.Visible = true;
            }

            //if (this.Product.InventoryDisplay == 1)
            //{
            //    uxBuyDirectFromVendor.Visible = true;
            //}

            // Set Visible true for FeaturedItem.
            if (this.Product.FeaturedInd)
            {
                FeaturedItemImage.Visible = true;
            }

            // Add to cart button
            if (this.Product.CallForPricing)
            {
                // If call for pricing is enabled,then hide AddToCart Button
                uxCallForPricing.Text = ZNodeCatalogManager.MessageConfig.GetMessage("ProductCallForPricing");
                showCartButton &= false;
                FeaturedItemImage.Visible = false;
                lblstockmessage.Text = string.Empty;
                lblstockmessage.CssClass = string.Empty;
                uxCallForPricing.Visible = !showCartButton;

                if (!string.IsNullOrEmpty(this.Product.CallMessage))
                {
                    uxCallForPricing.Text = this.Product.CallMessage;
                }
            }

            uxAddToCart.Visible = showCartButton;
        }

        /// <summary>
        /// Product Addons selected Index Changed Event raised by the Product Addon control
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        private void UxProductAddOns_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtQuantity.Text))
            {
                return;
            }
            this.UpdateProductStatus();
        }

        /// <summary>
        /// Product Attributes selected Index Changed Event raised by the Product Attributes control
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        private void UxProductAttributes_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strlog = "\nAttribute Event (Size/width) : Quick View. \nIP : {0} \nBrowser: {1}, \nVersion : {2}, \nType : {3}, \nProductNum : {4}, \nQty : {5}\n\n";
            strlog = string.Format(strlog, Request.UserHostAddress, Request.Browser.Browser, Request.Browser.Version, Request.UserAgent, this.Product.ProductNum, txtQuantity.Text);
            Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(strlog);

            if (string.IsNullOrEmpty(txtQuantity.Text))
            {
                return;
            }
            DropDownList ddl = (DropDownList)sender;
            this.Product.SelectedSKU = uxProductAttributes.SelectedSKU;

            if (this.Product.SelectedSKU.SKUID > 0)
            {
                ScriptManager.RegisterClientScriptBlock(UpdPnlOrderingOptions, sender.GetType(), "AttributeImage", "document.getElementById('CatalogItemImage').src='" + ResolveUrl(this.Product.MediumImageFilePath) + "';", true);
                UpdPnlOrderingOptions.Update();
            }
            this.UpdateStatusOnAttributeSelectedIndexChanged();
        }

        /// <summary>
        /// Event is raised when BundleProduct Selected Index Changed event is called
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        private void UxBundleProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdPnlOrderingOptions.Update();
            this.UpdateProductStatus();
        }

        private void uxProductAttributes_ColorSelectedIndexChanged(object sender, EventArgs e)
        {
            string strlog = "\n Color Change Event : Quick view. \nIP : {0} \nBrowser: {1}, \nVersion : {2}, \nType : {3}, \nProductNum : {4}, \nQty : {5}\n\n";
            strlog = string.Format(strlog, Request.UserHostAddress, Request.Browser.Browser, Request.Browser.Version, Request.UserAgent, this.Product.ProductNum, txtQuantity.Text);
            Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(strlog);

            if (!uxProductAttributes.IsLastAttributeSelectedIndexChanged)
            {
                this.DisableAddToCartButton(true);
            }
            else
            {
                this.UpdateStatusOnAttributeSelectedIndexChanged();
            }

        }

        /// <summary>
        /// Creates the navigation url
        /// </summary>
        private void AssignStoreLocatorNavigationURL()
        {
            string navigationUrl = _StoreLocatorPageLink;
            if (!string.IsNullOrEmpty(this.Product.SEOURL))
            {
                navigationUrl = navigationUrl + "?seourl=" + this.Product.SEOURL;
            }
            else
            {
                navigationUrl = navigationUrl + "?zpid=" + this.Product.ProductID;
            }
            hlnkStoreLocator.NavigateUrl = navigationUrl;
        }

        /// <summary>
        /// load custom setting on addto cart 
        /// </summary>
        private void LoadCustomAddtoCartSetting()
        {
            uxStatus.Text = (string)GetLocalResourceObject("SuccessMessage");
            uxStatus.Visible = true;
            uxStatus.CssClass = "InStockMsg";
            UpdateMiniCart();
            ScriptManager.RegisterClientScriptBlock(Page, GetType(), "CloseQuickWatch", "window.parent.quickViewHelper.CloseQuickWatch();", true);
        }

        /// <summary>
        /// Update Mini cart for flyout
        /// </summary>
        protected void UpdateMiniCart()
        {
            ZNodeShoppingCart ShoppingCart = (ZNodeShoppingCart)ZNodeCheckout.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
            string cartCount = "0", cartTotal = "$0.00";
            if (ShoppingCart != null)
            {
                cartCount = GetShoppingCartTotalItemCount(ShoppingCart.ShoppingCartItems);
                cartTotal = ShoppingCart.SubTotal.ToString("c");
                ScriptManager.RegisterClientScriptBlock(Page, GetType(), "Script", "window.parent.quickViewHelper.UpdateMiniCart('" + cartCount + "', '" + cartTotal + "');", true);
            }
        }

        /// <summary>
        /// Size of Product Image
        /// </summary>
        private void GetSizeOfImage()
        {
            string createLargeImage = this.Product.LargeImageFilePath;
            hdnfSmallImagename.Value = ZNodeConfigManager.SiteConfig.MaxCatalogItemMediumWidth.ToString();
            hdnfLargeImagename.Value = ZNodeConfigManager.SiteConfig.MaxCatalogItemLargeWidth.ToString();
        }

        /// <summary>
        /// Set Brand link control properties
        /// </summary>
        private void SetBrandLinkAttribute()
        {
            string imgTag = "<img src={0} alt={1} />";
            ZNodeImage znodeImage = new ZNodeImage();
            ManufacturerExtnService service = new ManufacturerExtnService();
            TList<ManufacturerExtn> lstmanufacaturerExtn = service.GetByManufacturerID(this.Product.ManufacturerID);
            if (lstmanufacaturerExtn != null && lstmanufacaturerExtn.Count > 0)
            {
                //BrandLink.NavigateUrl = !string.IsNullOrEmpty(lstmanufacaturerExtn[0].SEOURL) ? "~/" + "javascript:self.parent.location ='" + lstmanufacaturerExtn[0].SEOURL + "'" : "javascript:self.parent.location ='brand.aspx?mid=" + this.Product.ManufacturerID + "'";
                //BrandLink.Text = !string.IsNullOrEmpty(lstmanufacaturerExtn[0].Image) ? this.Product.ManufacturerName + string.Format(imgTag, znodeImage.GetImageBySize(ZNodeConfigManager.SiteConfig.MaxCatalogItemCrossSellWidth, lstmanufacaturerExtn[0].Image).Replace("~/", ""), this.Product.ManufacturerName) : this.Product.ManufacturerName;
            }
            else
            {
                //BrandLink.NavigateUrl = "javascript:self.parent.location ='brand.aspx?mid=" + this.Product.ManufacturerID + "'";
                //BrandLink.Text = this.Product.ManufacturerName;
            }
        }

        /// <summary>
        /// Set text of  uxAddToCart control
        /// </summary>
        private void ManageAddToCartText()
        {
            if (this.Product != null && this.Product.PreOrderFlag)
            {
                uxAddToCart.Text = this.GetLocalResourceObject("uxAddToCartPreOrderText").ToString();
            }
            else
            {
                uxAddToCart.Text = this.GetLocalResourceObject("uxAddToCartText").ToString();
            }
        }

        /// <summary>
        /// Bind Tier Price Collection
        /// </summary>
        /// <summary>
        /// Bind Tier Price Collection
        /// </summary>
        private void BindTierPriceCollection()
        {
            bool isProductHasCustomPromotion = false;
            int tierIndex = 0;
            StringBuilder htmlBuilder = new StringBuilder();
            StringBuilder priceRow = new StringBuilder();
            StringBuilder qtyRow = new StringBuilder();
            if (this.Product != null && this.Product.ZNodeTieredPriceCollection != null && this.Product.ZNodeTieredPriceCollection.Count > 0)
            {
                decimal price = 0;
                foreach (ZNode.Libraries.ECommerce.Entities.ZNodeProductTierEntity productTier in this.Product.ZNodeTieredPriceCollection)
                {
                    //Create Qty Row
                    qtyRow.Append("<td>");
                    if (tierIndex == 0)
                    {
                        qtyRow.Append(productTier.TierStart + " - " + productTier.TierEnd);
                    }
                    else
                    {
                        qtyRow.Append((Convert.ToInt32(productTier.TierStart.ToString())) + " - " + productTier.TierEnd.ToString());
                    }
                    qtyRow.Append("</td>");

                    //Create Price Row
                    string priceText = string.Empty;
                    if (tierIndex == 0 || isProductHasCustomPromotion)
                    {
                        ZNode.Libraries.ECommerce.Promotions.ZNodePromotionPricingOption pricing = new ZNode.Libraries.ECommerce.Promotions.ZNodePromotionPricingOption();
                        price = pricing.PromotionalPrice(this.Product, productTier.Price);
                        isProductHasCustomPromotion = this.Product.IsCustomPromotionApplied;
                    }
                    string htmText = this.Product.IsCustomPromotionApplied ? "<td><span class='RegularPrice'>" : "<td>";
                    priceRow.Append(htmText);
                    if (!this.Product.IsCustomPromotionApplied)
                    {
                        if (price == productTier.Price || price == 0)
                        {
                            priceText = string.Format(this.GetLocalResourceObject("TierRegulerPriceFormat").ToString(), productTier.Price.ToString("C"));
                        }
                        else
                        {
                            priceText = string.Format(this.GetLocalResourceObject("TierRegulerPriceFormat").ToString(), productTier.Price.ToString("C"));
                        }
                    }
                    else
                    {
                        priceText = productTier.Price.ToString("C") + string.Format(this.GetLocalResourceObject("TierSalesPriceFormat").ToString(), price.ToString("C")); ;
                    }
                    priceRow.Append(priceText);
                    priceRow.Append("</td>");
                    tierIndex++;

                }
                htmlBuilder.Append(string.Format(this.GetLocalResourceObject("TierPriceTableStructureFormat").ToString(), qtyRow.ToString(), priceRow.ToString()));
                //Create Proper HTML and Assign it to below variable.
                ltrProductTierPrice.Text = htmlBuilder.ToString();
            }
        }

        /// <summary>
        /// Get the cookies for the store locator
        /// </summary>
        /// <returns>Returns the HttpCookie</returns>
        private HttpCookie GetCookie()
        {
            HttpCookie storeCookie = null;
            int accID = ZNodeUserAccount.CurrentAccount() == null ? 0 : ZNodeUserAccount.CurrentAccount().AccountID;

            if (accID > 0)
            {
                string cookieName = Convert.ToString(accID) + "_StoreCookie";
                if (Request.Cookies[cookieName] != null)
                {
                    storeCookie = Request.Cookies[cookieName];
                }
            }
            else
            {
                if (Request.Cookies["StoreCookie"] != null)
                {
                    storeCookie = Request.Cookies["StoreCookie"];
                }
            }

            return storeCookie;
        }

        /// <summary>
        /// Assign the store wise availablity message
        /// </summary>
        private void AssignStoreWiseInventory()
        {
            ZNodeProduct prod = (ZNodeProduct)HttpContext.Current.Items["Product"];
            ProductService service = new ProductService();
            ZNode.Libraries.DataAccess.Entities.Product objProduct = service.GetByProductID(prod.ProductID);

            if (objProduct.DropShipInd != null && !Convert.ToBoolean(objProduct.DropShipInd))
            {
                HttpCookie storeCookie = GetCookie();
                if (storeCookie != null)
                {
                    int storeID = 0;
                    if (int.TryParse(storeCookie["StoreCookieID"].ToString(), out storeID))
                    {
                        ShowStoreWiseInventory(prod, storeID, storeCookie);
                    }
                }
                else
                {
                    lnkInStoreAvailability.Style["display"] = "none";
                    hlnkStoreLocator.Visible = true;
                }
            }
            service = null;
            objProduct = null;
        }

        /// <summary>
        /// Show the store wise inventory or show the check in store availability option
        /// </summary>
        /// <param name="product"></param>
        /// <param name="storeID"></param>
        /// <param name="storeCookie"></param>
        private void ShowStoreWiseInventory(ZNodeProduct product, int storeID, HttpCookie storeCookie)
        {
            string whereClause = "StoreId=" + storeID + " AND SKUID= " + product.SelectedSKU.SKUID;
            ZeonStoreWiseInventoryService service = new ZeonStoreWiseInventoryService();
            TList<ZeonStoreWiseInventory> selectedStoreList = service.Find(whereClause);

            if (selectedStoreList != null && selectedStoreList.Count > 0)
            {
                ZeonStoreWiseInventory selectedStore = selectedStoreList[0];
                if (selectedStore != null)
                {
                    int quantity = selectedStore.QuantityOnHand == null ? 0 : Convert.ToInt32(selectedStore.QuantityOnHand);
                    if (quantity <= 0)
                    {
                        SetCheckInStoreMessage();
                    }
                    else
                    {
                        SetStoreInventoryText(HttpContext.Current.Server.UrlDecode(storeCookie["StoreCookieName"].ToString()), HttpContext.Current.Server.UrlDecode(storeCookie["StoreCookiePhone"].ToString()));
                    }
                }
            }
        }

        /// <summary>
        /// Set the In Store Message
        /// </summary>
        /// <param name="textToSet"></param>
        private void SetStoreInventoryText(string textToSet, string txtCookiePhone)
        {
            lnkInStoreAvailability.Visible = false;
            lblStorewiseInventory.Text = string.Format(this.GetLocalResourceObject("InstoreMessageOfproduct").ToString(), textToSet, txtCookiePhone, txtCookiePhone);
            upnlStoreInventory.Update();
        }

        /// <summary>
        /// Set the Out of store with Check in availability
        /// </summary>
        private void SetCheckInStoreMessage()
        {
            lnkInStoreAvailability.Visible = true;
            lblStorewiseInventory.Visible = false;
        }

        ///// <summary>
        ///// Bind the video url. If present then show the control else hide the iframe control
        ///// </summary>
        //private void BindVideoURLAndThumbnailImage()
        //{
        //    ProductExtnService prodExtService = new ProductExtnService();
        //    TList<ProductExtn> prodList = prodExtService.GetByProductID(this.Product.ProductID);
        //    ifrmYoutubeVideo.Visible = false;
        //   imgYouTubeVideo.Visible = false;
        //    if (prodList != null && prodList.Count > 0)
        //    {
        //        ifrmYoutubeVideo.Visible = true;
        //        string videoUrl = prodList[0].VideoUrl;
        //        if (!string.IsNullOrEmpty(videoUrl) && !videoUrl.StartsWith(_URLStartString, StringComparison.CurrentCultureIgnoreCase))
        //        {
        //            videoUrl = HttpContext.Current.Request.IsSecureConnection ? _SecuredURLString + videoUrl : _NonSecuredURLString + videoUrl;
        //        }

        //        if (string.IsNullOrEmpty(videoUrl))
        //        {
        //            ifrmYoutubeVideo.Visible = false;
        //        }
        //        else
        //        {
        //            ShowHideVideoThumbnail(videoUrl);
        //        }
        //    }
        //}

        ///// <summary>
        ///// Show Hide Video Thumbnail
        ///// </summary>
        ///// <param name="videoUrl"></param>
        //private void ShowHideVideoThumbnail(string videoUrl)
        //{
        //    string[] splittedURL = videoUrl.Split('/');
        //    if (splittedURL != null && splittedURL.Length > 0)
        //    {
        //        string videoID = splittedURL[splittedURL.Length - 1];
        //        if (!string.IsNullOrEmpty(videoID) && videoID.Length.Equals(_YoutubeEmbdeTagLength))
        //        {
        //            imgYouTubeVideo.Visible = true;
        //        }
        //        else
        //        {
        //            imgYouTubeVideo.Visible = false;
        //        }
        //    }
        //    else
        //    {
        //        imgYouTubeVideo.Visible = false;
        //    }
        //    ifrmYoutubeVideo.Attributes["src"] = videoUrl;
        //}

        /// <summary>
        ///  Update Status On Attribute select event
        /// </summary>
        private void UpdateStatusOnAttributeSelectedIndexChanged()
        {
            // Update the panels
            UpdPnlOrderingOptions.Update();

            if (!uxProductAttributes.IsLastAttributeSelectedIndexChanged)
            {
                this.DisableAddToCartButton(true);
            }
            else
            {
                this.DisableAddToCartButton(false);
            }

            // Hide Product price
            uxProductPrice.Visible = this.Profile.ShowPrice && !this.Product.CallForPricing;
            uxAddToCart.Visible = this.Profile.ShowPrice && !this.Product.CallForPricing;
            uxProductAttributes.ResetAttributeErrorMessage(); //PRFT Custom Code


            // Check if product has attributes
            if (uxProductAttributes.IsLastAttributeSelectedIndexChanged && this.Product.ZNodeAttributeTypeCollection.Count > 0)
            {
                // If product has attributes then validate that all attributes have been selected by
                // the customer validate attributes first - This is to verify that all required attributes have a valid selection
                // if a valid selection is not found then an error message is displayed
                string attributeMessage = string.Empty;
                string attributeList = string.Empty;
                string selectedAttributesDescription = string.Empty;

                bool status = uxProductAttributes.ValidateAttributes(out attributeMessage, out attributeList, out selectedAttributesDescription);
                if (!status)
                {
                    if (!string.IsNullOrEmpty(attributeMessage))
                    {
                        uxStatus.Visible = true;
                        uxStatus.Text = attributeMessage;
                    }

                    // Hide AddToCart button
                    //uxAddToCart.Visible = false;

                    this.DisableAddToCartButton(true);
                }
                else
                {
                    uxAddToCart.Visible = true;

                    this.DisableAddToCartButton(false);

                    //Bind SKU Price on last attribute selected index changes
                    ZNodeSKU SKU = ZNodeSKU.CreateByProductAndAttributes(this.Product.ProductID, attributeList);
                    // Set Attributes Description to Description property
                    SKU.AttributesDescription = selectedAttributesDescription;

                    this.Product.SelectedSKU = SKU;
                    this.Product.CheckSKUProfile();
                    this.Product.IsPromotionApplied = false;
                    this.Product.ApplyPromotion();

                    // Check for 'Call for Pricing'
                    if (this.Product.CallForPricing)
                    {
                        lblstockmessage.Visible = false;
                        uxAddToCart.Visible = false;
                        uxCallForPricing.Visible = true;
                        uxStatus.Visible = false;

                        if (!string.IsNullOrEmpty(this.Product.CallMessage))
                        {
                            uxCallForPricing.Text = this.Product.CallMessage;
                        }
                    }
                    else
                    {
                        lblstockmessage.Visible = true;
                    }
                    upnlStockMessage.Update();
                    // Bind selected sku product price if one exists
                    uxProductPrice.Quantity = this.ShoppingCartQuantity;
                    uxProductPrice.Product = this.Product;
                    uxProductPrice.Bind();

                }
            }
        }

        /// <summary>
        /// Show the other available store
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkInStoreAvailability_Click(object sender, EventArgs e)
        {
            HttpCookie storeCookie = GetCookie();
            ZNodeProduct prod = (ZNodeProduct)HttpContext.Current.Items["Product"];
            if (prod != null)
            {
                int storeID = storeCookie != null ? int.Parse(storeCookie["StoreCookieID"].ToString()) : 0;
                DataTable dtStoreInvData = ZNodeStoreList.GetListOfStorewiseAvalabilityOfSku(storeID, prod.SelectedSKU.SKUID, ZNodeConfigManager.SiteConfig.PortalID);
                if (dtStoreInvData != null && dtStoreInvData.Rows.Count > 0)
                {
                    rptInStoreAvailability.DataSource = dtStoreInvData;
                    rptInStoreAvailability.DataBind();

                }
                upnlStoreInv.Update();
                SetCheckInStoreMessage();
                upnlStoreInventory.Update();
            }
            else
            {
                rptInStoreAvailability.Visible = false;
                lblStoreAvailErrorMsg.Visible = true;
            }
        }

        /// <summary>
        /// set values in ProductAttributeCollection
        /// </summary>
        private DataTable GetProductAttributeCollectionByProductID()
        {
            DataTable dtProductAttribute = null;
            if (this.Product.ProductID > 0)
            {
                ProductHelper prdHelper = new ProductHelper();
                dtProductAttribute = prdHelper.GetProductAttributesByID(this.Product.ProductID);
            }
            return dtProductAttribute;
        }

        /// <summary>
        /// get Shopping cart item total quantity count
        /// </summary>
        /// <returns></returns>
        private string GetShoppingCartTotalItemCount(ZNodeGenericCollection<ZNodeShoppingCartItem> shoppingCartItems)
        {
            int totalQuantity = 0;
            foreach (ZNodeShoppingCartItem item in shoppingCartItems)
            {
                totalQuantity += item.Quantity;
            }
            return totalQuantity.ToString();
        }

        /// <summary>
        /// Register Client Script On Page
        /// </summary>
        private void RegisterClientScriptClasses()
        {
            if (ScriptManager.GetCurrent(Page).IsInAsyncPostBack)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, GetType(), "AttributeImageZoom", "InitializeZoom();", true);
            }
        }

        /// <summary>
        /// Find If Current portal is Cheer and Pom
        /// </summary>
        private void HideControlByCurrentPortals()
        {
            bool isCustomPortal = ZCommonHelper.IsCustomMulitipleAttributePortal();
            inStoreAvailabilityListId.Visible = UpdPnlOrderingOptions.Visible = divStoreInventory.Visible = !isCustomPortal;
            divGroupAttr.Visible = isCustomPortal;

            if (this._Product.ProductSKUCount > 1)
            {
                uxProductSkuImages.Visible = true;
            }
            else
            {
                uxProductSkuImages.Visible = false;
            }

        }

        #endregion
    }
}