﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ZQuickWatch.ascx.cs" Inherits="WebApp.Controls.Default.QuickWatch.ZQuickWatch" %>
<%@ Register Src="~/Controls/Default/Product/SwatchImages.ascx" TagName="Swatches" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/CatalogImage.ascx" TagName="CatalogItemImage" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/ExternalLinks.ascx" TagName="ExternalLinks" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/Bundleproducts.ascx" TagName="BundleProduct" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Reviews/ProductAverageRating.ascx" TagName="ProductAverageRating" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/ProductPrice.ascx" TagName="ProductPrice" TagPrefix="ZNode" %>
<%@ Register TagName="AternateImages" TagPrefix="zeon" Src="~/Controls/Default/Product/ZProductAlternateImages.ascx" %>
<%@ Register Src="~/Controls/Default/Product/ZProductAttributes.ascx" TagName="ProductAttributes" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/ProductHighlights.ascx" TagName="ProductHighlights" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/ProductAddOns.ascx" TagName="ProductAddOns" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Default/Product/ZGroupProductAttributes.ascx" TagName="GroupProductAttributes" TagPrefix="Zeon" %>
<%@ Register Src="~/Controls/CheerandPom/Product/ZProductSkuImages.ascx" TagName="ZProductSkuImages" TagPrefix="Zeon" %>
<div id="ProductDetail">
    <div class="MainProductBlock clearfix">
        <div class="LeftColumn">
            <div class="Image">
                <%--<div id="BrandLabel" runat="server" visible="false" class="BrandImage">
                    <asp:Localize ID="Localize4" runat="server" meta:resourceKey="txtBRAND"></asp:Localize>:&nbsp;<asp:HyperLink
                        ID="BrandLink" runat="server" EnableViewState="false" meta:resourcekey="BrandLinkResource1"></asp:HyperLink>
                </div>--%>
                <div class="CataLogImage">
                    <asp:UpdatePanel ID="upCatalogImage" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <ZNode:CatalogItemImage ID="CatalogItemImage" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <%--<div class="ProductVideo" style="display: none;">
                    <span class="CloseButton" onclick="window.parent.quickViewHelper.CloseVideoArena();">
                        <asp:Literal ID="locClose" runat="server" meta:resourcekey="locCloseResource1"></asp:Literal></span>
                    <iframe id="ifrmYoutubeVideo" class="YouTubeFrame" runat="server"></iframe>
                </div>--%>
                <div class="ViewZoom">
                    <div class="EnlargeImg">
                        <asp:HiddenField ID="hdnfLargeImagename" runat="server" />
                        <asp:HiddenField ID="hdnfSmallImagename" runat="server" />
                        <div id="divLargeImage" runat="server" class="ViewLargeImage">
                            <a title="Enlarge Image" id="imgoverlay" onclick="window.parent.quickViewHelper.OpenImageZoomOverlay('<%= hdnfLargeImagename.Value%>','<%= hdnfSmallImagename.Value %>','<%= ResolveUrl(this.Product.MediumImageFilePath) %>');" class="Button">
                                <asp:Localize ID="locZoomImage" EnableViewState="false" runat="server" meta:resourcekey="btnViewZoomImage"></asp:Localize>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <asp:UpdatePanel ID="upnlAlternateImage" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="AlternateImageSection" onclick="window.parent.quickViewHelper.ShowCatalogImagePanel();">
                        <Zeon:AternateImages ID="uxAternateImages" runat="server" MaxDisplayColumns="4" IsQuickWatch="true" />
                    </div>
                    <%-- <span class="Prodict-video" style="display:none;"> 
                        <img id="imgYouTubeVideo" runat="server" src="/Themes/Default/Images/youtube-image.png" onclick="window.parent.quickViewHelper.ShowVideoArena();" /></span>--%>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="ProductTier">
                <asp:Literal ID="ltrProductTierPrice" runat="server" EnableViewState="false"></asp:Literal>
            </div>
        </div>
        <div class="RightColumn">
            <div>
                <h1 class="ProductTitle">
                    <asp:Literal ID="ProductTitle" Text="{0}" EnableViewState="false" runat="server" meta:resourcekey="ProductTitleResource1"></asp:Literal>
                </h1>
            </div>
            <div class="ProductPageNewItem" id="divProductPageNewItem" runat="server" visible="false">
                <%--<asp:Image ID="NewItemImage" runat="server" Visible="False" meta:resourcekey="NewItemImageResource1" EnableViewState="false" AlternateText="New" alt="New" />--%>
                <span class="rs-new"><span class="rs-new-text">New</span></span>
            </div>
            <div class="ProductNumbStock">
                <span class="ProductNumber">
                    <asp:Localize ID="Localize3" runat="server" meta:resourceKey="txtITEM"></asp:Localize>:&nbsp;#<asp:Label
                        ID="ProductID" runat="server" EnableViewState="false" meta:resourcekey="ProductIDResource1"></asp:Label>
                </span>
                <span>
                    <asp:UpdatePanel ID="upnlStockMessage" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="display: none;">
                                <asp:Label ID="lblstockmessage" runat="server" CssClass="ProductStock" meta:resourcekey="lblstockmessageResource1"></asp:Label>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </span>
            </div>
            <div class="StarRating">
                <ZNode:ProductAverageRating ID="uxReviewRating" runat="server" IsQuickWatch="true" />
                | <a id="Hyperlink" runat="server" title="Write a Review"><span>Write a Review</span>
                </a>

            </div>

            <div class="PriceContent">
                <ZNode:ProductPrice ID="uxProductPrice" runat="server" />
                <asp:Image ID="FeaturedItemImage" EnableViewState="false" runat="server" Visible="False" ImageAlign="AbsMiddle" AlternateText="Featured Item" ToolTip="Featured Item"
                    meta:resourcekey="FeaturedItemImageResource1" />
            </div>
            <div class="StarRight">
                <ZNode:ProductHighlights ID="uxHighlights" MaxDisplayColumns="10" runat="server" Visible="False" />
            </div>
            <div class="ProductAttributes">
                <asp:UpdatePanel ID="UpdPnlOrderingOptions" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="OrderingOptions">
                            <ZNode:ProductAttributes ID="uxProductAttributes" runat="server" IsQuickWatch="true" />
                            <ZNode:ProductAddOns ID="uxProductAddOns" runat="server" ShowCheckBoxesVertically="true"
                                ShowRadioButtonsVerically="true"></ZNode:ProductAddOns>
                            <asp:Panel ID="pnlQty" runat="server" Visible="False" meta:resourcekey="pnlQtyResource1" DefaultButton="uxAddToCart">
                                <table class="Quantity">
                                    <tr>
                                        <td class="QuantityOption">
                                            <asp:Localize ID="Localize1" runat="server" meta:resourceKey="txtQuantity"></asp:Localize>
                                            : </td>
                                        <td>
                                            <asp:TextBox ID="txtQuantity" runat="server" MaxLength="5" Text="1" ValidationGroup="vgProductQuickWatch" CssClass="qtytxt"></asp:TextBox>
                                            <ajaxToolKit:FilteredTextBoxExtender FilterType="Numbers" TargetControlID="txtQuantity" ID="fitTxtQty" runat="server">
                                            </ajaxToolKit:FilteredTextBoxExtender>
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="pnlAddToCart" runat="server" UpdateMode="Always">
                                                <ContentTemplate>
                                                    <div id="divAddToCart" runat="server">
                                                        <div class="AddToCartButton">
                                                            <asp:LinkButton CssClass="addtocartText" ID="uxAddToCart" runat="server" ValidationGroup="vgProductQuickWatch"
                                                                OnClick="UxAddToCart_Click" ToolTip="Add to Bag"><span>Add to Bag</span></asp:LinkButton>
                                                            <div class="CallForPrice">
                                                                <asp:Label ID="uxCallForPricing" EnableViewState="false" runat="server" Text="Call For Pricing" Visible="False"
                                                                    meta:resourcekey="uxCallForPricingResource1"></asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <div class="QuantityInHand">
                                <asp:Literal ID="ltrQuantityInHand" runat="server" ClientIDMode="Static"></asp:Literal>
                            </div>
                            <div class="SalePrice">
                                <asp:Literal EnableViewState="False" ID="AdditionalPrice" runat="server" meta:resourcekey="AdditionalPriceResource1"></asp:Literal>
                            </div>
                            <div class="StatusMsg">
                                <asp:RequiredFieldValidator ID="rfvQty" CssClass="Error" runat="server" ControlToValidate="txtQuantity" ValidationGroup="vgProductQuickWatch" ErrorMessage="<%$ Resources:CommonCaption,QuantityRequired %>" Display="Dynamic" SetFocusOnError="true" />
                                <asp:RangeValidator ID="rgvQuantity" runat="server" ControlToValidate="txtQuantity" MaximumValue="99999" MinimumValue="1" CssClass="Error"
                                    Display="Dynamic" SetFocusOnError="true" Type="Integer" ErrorMessage="<%$ Resources:CommonCaption,ProductQuantityRange %>" ValidationGroup="vgProductQuickWatch"></asp:RangeValidator>
                                <asp:Label ID="uxStatus" EnableViewState="False" runat="server" CssClass="Error"
                                    meta:resourcekey="uxStatusResource1"></asp:Label>
                            </div>
                            <div class="Error" id="cookiediv">
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <uc1:CustomMessage ID="ucPromoBannerRibbon" runat="server" EnableViewState="false" MessageKey="FestiveOrderShippingProductDetail" />
                <uc1:CustomMessage ID="uxFreeShippingQuick" runat="server" MessageKey="AlwaysFreeShippingMsgKey" EnableViewState="false" />
                <div id="divGroupAttr" runat="server">
                    <asp:UpdatePanel ID="upSkuImages" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <Zeon:ZProductSkuImages ID="uxProductSkuImages" runat="server" Visible="false" IsQuickWatch="true" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <Zeon:GroupProductAttributes ID="ucGroupProductAttribute" runat="server" />
                </div>
                <div class="StoreInventory" style="display: none;" id="divStoreInventory" runat="server">
                    <div class="ProductStoreLocator">
                        <asp:HyperLink ID="hlnkStoreLocator" runat="server" EnableViewState="false" meta:resourcekey="hlnkStoreLocatorResource1"></asp:HyperLink>
                    </div>
                    <asp:UpdatePanel ID="upnlStoreInventory" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <h4>
                                <asp:Label ID="lblStorewiseInventory" runat="server"></asp:Label>
                            </h4>
                            <div id="InStoreAvailability">
                                <%--<span id="infoIcon" runat="server" class="fa-info-circle"></span>--%>
                                <asp:LinkButton ID="lnkInStoreAvailability" runat="server" meta:resourceKey="InStoreAvailabilityResource1" OnClientClick="objProduct.OpenStoreAvailability();"
                                    OnClick="lnkInStoreAvailability_Click" CssClass="Button" EnableViewState="false"></asp:LinkButton>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="inStoreAvailabilityListId" class="ListInStoreAvailability" runat="server" style="display: none;" title="Store Availability">
                    <asp:UpdatePanel ID="upnlStoreInv" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Repeater ID="rptInStoreAvailability" runat="server" EnableViewState="false">
                                <HeaderTemplate>
                                    <ul class="ProductDataTable">
                                        <li class="ALeft">
                                            <ul>
                                                <li>
                                                    <asp:Literal ID="ltrStoreHeader" runat="server" EnableViewState="false" meta:resourceKey="ltrStoreHeaderResource1"></asp:Literal></li>
                                                <li>
                                                    <asp:Literal ID="ltrStoreContact" runat="server" EnableViewState="false" meta:resourceKey="ltrStoreContactResource1"></asp:Literal></li>
                                                <li>
                                                    <asp:Literal ID="ltrStoreAvailable" runat="server" EnableViewState="false" meta:resourceKey="ltrStoreAvailableResource1"></asp:Literal></li>
                                            </ul>
                                        </li>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li class="ALeft">
                                        <ul>
                                            <li>
                                                <asp:HiddenField ID="hdnStoreID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "StoreID")%>' />
                                                <asp:Label ID="lblStoreName" EnableViewState="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Name")%>'></asp:Label>
                                            </li>
                                            <li>
                                                <asp:Label ID="lblStorePhone" EnableViewState="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Phone")%>'></asp:Label>
                                            </li>
                                            <li>
                                                <asp:Label ID="lblAvailability" EnableViewState="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Availability")%>'></asp:Label>
                                            </li>
                                        </ul>
                                    </li>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ul>
                                </FooterTemplate>
                            </asp:Repeater>
                            <div class="Error">
                                <asp:Literal ID="lblStoreAvailErrorMsg" runat="server" meta:resourceKey="lblStoreAvailErrorMsgResource1"></asp:Literal>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
            </div>
            <uc1:CustomMessage ID="uxProductDetailSecurityInfo" runat="server" MessageKey="ProductDetailSecurityInfo" />
            <div id="BundleProduct">
                <ZNode:BundleProduct ID="uxBundleProduct" runat="server" />
            </div>
            <div class="Socail-Icon">
                <ZNode:ExternalLinks ID="ExternalLinks1" runat="server" />
            </div>

        </div>


    </div>
</div>
<div id="divEnlargedImage" runat="server" class="DilogModal" style="display: none;">
</div>

<%-- Zeon Custom HTML for Update progress bar --%>

<asp:UpdateProgress ID="updateProgressBar" DisplayAfter="0" runat="server"
    DynamicLayout="True">
    <ProgressTemplate>
        <div style="position: relative; top: 30%; text-align: center;" class="updateProgress">
            <img alt="loading" id="imgAddressloader" src="Themes/Default/Images/loading.gif" />&nbsp; Please wait...
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<ajaxToolKit:ModalPopupExtender ID="mpeprogressbar" runat="server" TargetControlID="updateProgressBar"
    BackgroundCssClass="modalBackground" PopupControlID="updateProgressBar">
</ajaxToolKit:ModalPopupExtender>
<script>
    //Script for Modal progress bar
    var ModalProgressAddress = '<%= mpeprogressbar.ClientID %>';
    Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginReq);
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endReq);
    function beginReq(sender, args) {
        // shows the Popup 
        $find(ModalProgressAddress).show();
    }
    function endReq(sender, args) {
        //  hide Popup 
        $find(ModalProgressAddress).hide();
    }
</script>
<%-- Zeon Custom HTML for Update progress bar --%>