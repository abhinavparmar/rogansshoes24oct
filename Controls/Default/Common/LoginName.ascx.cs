﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.ECommerce.UserAccount;

namespace WebApp
{
    /// <summary>
    /// Represents the LoginName user control class.
    /// </summary>
    public partial class Controls_Default_Common_LoginName : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SetUserName();
        }

        #region Zeon Custom Methods

        /// <summary>
        /// Set new user name
        /// </summary>
        private void SetUserName()
        {
            if (this.Page.User.Identity.IsAuthenticated)
            {
                if (!string.IsNullOrEmpty(this.Page.User.Identity.Name))
                {
                    ZNodeUserAccount userAccount = ZNodeUserAccount.CurrentAccount();
                    string formatedUserName = string.Empty;
                    if (userAccount != null && userAccount.BillingAddress != null && userAccount.BillingAddress.FirstName != null && userAccount.BillingAddress.LastName != null)
                    {
                        formatedUserName = GetFormatedUserName(string.Format(this.GetLocalResourceObject("lnLoginName").ToString(), userAccount.BillingAddress.FirstName + " " + userAccount.BillingAddress.LastName));
                    }
                    else
                    {
                        formatedUserName = GetFormatedUserName(string.Format(this.GetLocalResourceObject("lnLoginName").ToString(), this.Page.User.Identity.Name.Replace("_" + ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.PortalID.ToString(), "")));//(this.Page.User.Identity.Name);
                    }
                    lnLoginName.FormatString = formatedUserName;
                }
            }
        }

        /// <summary>
        /// Get the formated User Name
        /// </summary>
        /// <returns></returns>
        private string GetFormatedUserName(string userName)
        {
            lnLoginName.ToolTip = userName;
            int userNameMaxLimitVal = Convert.ToInt32(ConfigurationManager.AppSettings["UserNameCharecterLimits"].ToString());
            if (userName.Length > userNameMaxLimitVal)
            {
                userName = userName.Substring(0, userNameMaxLimitVal) + "...";
            }
            return userName;
        }

        #endregion
    }
}