<%@ Control Language="C#" AutoEventWireup="true" EnableViewState="false" Inherits="WebApp.Controls_Default_Common_HomeQuickSearch" Codebehind="HomeQuickSearch.ascx.cs" %>

<asp:Panel ID="pnlQuickSearch" runat="server" DefaultButton="btnSearch">
    <div id="HomeQuickSearch">    
      <ajaxToolKit:TextBoxWatermarkExtender ID="watermark" TargetControlID="txtKeyword" WatermarkCssClass="WaterMark" meta:resourceKey="txtKeyword" runat="server"></ajaxToolKit:TextBoxWatermarkExtender>
      <asp:TextBox ID="txtKeyword" Width="125" runat="server" CssClass="TextBox"></asp:TextBox>&nbsp;&nbsp;<asp:ImageButton EnableViewState="false" ID="btnSearch" AlternateText="Search" ToolTip="Search" runat="server" OnClick="BtnSearch_Click" CssClass="Button" />
    </div>    
</asp:Panel>

