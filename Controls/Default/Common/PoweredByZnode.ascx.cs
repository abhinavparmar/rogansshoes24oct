using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace WebApp
{
    /// <summary>
    /// Represents the PoweredByZnode user control class.
    /// </summary>
    public partial class Controls_Default_Common_PoweredByZnode : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Set ImagePath
            ImgZnode.Src = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/Images/poweredby_small.gif";
        }
    }
}