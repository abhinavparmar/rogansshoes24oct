using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Framework.Business;

/// <summary>
/// Displays Customer Service Phone Number
/// </summary>
namespace WebApp
{
    /// <summary>
    /// Represents the Customer Service Phone user control class.
    /// </summary>
    public partial class Controls_Default_Common_CustomerServicePhone : System.Web.UI.UserControl
    {
        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            uxCustomerServicePhone.Text = ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber;
            CustomerServicePhone.HRef = "tel:+" + ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber;
        }
        #endregion
    }
}