﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp
{
    /// <summary>
    /// Represents the Default Tab user control in marketplace.
    /// </summary>
    public partial class Controls_Default_Common_DefaultTab : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserStoreAccess.IsMultiStoreAdminEnabled())
            {
                // Load image based on locale
                TabMenu.Src = ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("DemoStoreTab.jpg");
            }
            else 
            { 
                TabMenu.Visible = false; 
            }
        }
    }
}