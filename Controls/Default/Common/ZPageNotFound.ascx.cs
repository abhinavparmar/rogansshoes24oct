﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace WebApp.Controls.Default.Common
{
    public partial class ZPageNotFound : System.Web.UI.UserControl
    {
        #region Enum

        public enum SEOURLEntityType
        {
            PRODUCT, //SEO URL Belongs to Product Table
            CATEGORY //SEO URL Belongs to Category Table
        }

        #endregion

        #region Page Event

        protected void Page_Load(object sender, EventArgs e)
        {
            OnPageLoad();
        }

        #endregion

        #region Send Mail

        /// <summary>
        /// Send mail on 404 error
        /// </summary>
        /// <param name="internalNonSEOUrlLink">internalNonSEOUrlLink for redirect </param>
        /// <param name="mailBodyContents">mailBodyContents mail body for sending mail</param>
        private void Send404EmailBySetting(string internalNonSEOUrlLink, string mailBodyContents)
        {
            try
            {
                string sendMailForAll404Error = ZNodeCatalogManager.MessageConfig.GetMessage("SendMailForAll404Error", ZNodeConfigManager.SiteConfig.PortalID, ZNodeConfigManager.SiteConfig.LocaleID);
                if (!string.IsNullOrEmpty(sendMailForAll404Error) && sendMailForAll404Error.Equals("True", StringComparison.OrdinalIgnoreCase) == true)
                {
                    //we have to send mail for all 404 errors
                    SendEmail(mailBodyContents);
                    if (!string.IsNullOrEmpty(internalNonSEOUrlLink))
                    {
                        //now redirect user to interal link to purchase
                        Response.Redirect(internalNonSEOUrlLink);
                    }
                }
                else
                {
                    //we have to send mail only when zpid/zcid exists for SEO Url
                    if (!string.IsNullOrEmpty(internalNonSEOUrlLink))
                    {
                        SendEmail(mailBodyContents);
                        //now redirect user to interal link to purchase
                        Response.Redirect(internalNonSEOUrlLink);
                    }
                }
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogMessage(ex.ToString());
            }
        }

        /// <summary>
        /// Send Email
        /// </summary>
        /// <param name="mailBodyContents">mailBodyContents is used for mail Content</param>
        private void SendEmail(string mailBodyContents)
        {
            try
            {
                string email = ZNodeCatalogManager.MessageConfig.GetMessage("MaintenancePageEmailID", ZNodeConfigManager.SiteConfig.PortalID, ZNodeConfigManager.SiteConfig.LocaleID);
                if (!string.IsNullOrEmpty(email))
                {
                    ZNodeEmail.SendEmail(email.Trim(), ZNodeConfigManager.SiteConfig.CustomerServiceEmail, "", ZNodeCatalogManager.MessageConfig.GetMessage("MaintenancePageTitleEmailSubject", ZNodeConfigManager.SiteConfig.PortalID, ZNodeConfigManager.SiteConfig.LocaleID), mailBodyContents, true);
                }
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogMessage(ex.ToString());
            }
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// On page load event methods
        /// </summary>
        private void OnPageLoad()
        {
            try
            {
                locCustomerServicePhone.Text = ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber;
                emailLink.Attributes.Add("href", "mailto:" + ZNodeConfigManager.SiteConfig.CustomerServiceEmail);

                string brokenLink = Request.Url.AbsoluteUri;

                if (brokenLink != null && brokenLink.Trim().EndsWith(".aspx", StringComparison.OrdinalIgnoreCase))
                {
                    string internalNonSEOUrlLink = GetBrokenLinkInternalNonSEOUrlLink();
                    string mailBodyContents = GetBodyContents(internalNonSEOUrlLink);
                    LogErrorOnFile(mailBodyContents);
                    Send404EmailBySetting(internalNonSEOUrlLink, mailBodyContents);
                }
                else
                {
                    string URL = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath;
                    ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNumExtn.PageNotFound_404, "404-Page Not Found Error", "Site URL:" + URL, "Missing/Broken Link:" + brokenLink, "Broken Item SEO Link:" + GetRequestedNonAspxUrlLink(brokenLink), "Page Not Found", GetRequestedNonAspxUrlLink(brokenLink), brokenLink);
                }
            }
            catch (ThreadAbortException ex1)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex1.ToString());
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
            }
        }

        /// <summary>
        /// Create Body Cotent for Mail
        /// </summary>
        /// <param name="internalNonSEOUrlLink"></param>
        /// <returns>Mail Body in String</returns>
        private string GetBodyContents(string internalNonSEOUrlLink)
        {
            StringBuilder sb = new StringBuilder(string.Empty);
            string URL = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
            string brokenLink = Request.Url.AbsoluteUri;
            string dateTime = DateTime.Now.ToString();


            //1
            sb.Append("<br/>");
            sb.Append("<strong>");
            sb.Append("Timestamp: ");
            sb.Append("</strong>");
            sb.Append(dateTime);
            sb.Append("<br/>");
            sb.Append("<br/>");

            //2
            sb.Append("<strong>");
            sb.Append("Site URL: ");
            sb.Append("</strong>");
            sb.Append(URL);
            sb.Append("<br/>");
            sb.Append("<br/>");

            //3
            sb.Append("<strong>");
            sb.Append("Missing/Broken Link: ");
            sb.Append("</strong>");
            sb.Append(brokenLink);
            sb.Append("<br/>");
            sb.Append("<br/>");

            //3.1
            sb.Append("<strong>");
            sb.Append("Broken Item SEO Link: ");
            sb.Append("</strong>");
            sb.Append(GetRequestedSEOUrlLink(brokenLink));
            sb.Append("<br/>");
            sb.Append("<br/>");

            //3.2
            sb.Append("<strong>");
            sb.Append("Broken Item Internal(Non SEO)Link: ");
            sb.Append("</strong>");
            sb.Append(internalNonSEOUrlLink);
            sb.Append("<br/>");
            sb.Append("<br/>");

            string clientIP = string.Empty;
            if (HttpContext.Current.Request.UserHostAddress != null)
            {
                clientIP = HttpContext.Current.Request.UserHostAddress;
            }
            sb.Append("<strong>");
            sb.Append("Client IP Address: ");
            sb.Append("</strong>");
            sb.Append(clientIP);
            sb.Append("<br/>");
            sb.Append("<br/>");

            //4 session count
            string sessionCount = string.Empty;
            if (Application["SessionCount"] != null)
            {
                sessionCount = Application["SessionCount"].ToString();
            }
            sb.Append("<strong>");
            sb.Append("Session Count: ");
            sb.Append("</strong>");
            sb.Append(sessionCount);
            sb.Append("<br/>");
            sb.Append("<br/>");

            Zeon_ErrorDiagnostics objError = new Zeon_ErrorDiagnostics();
            //5 CPU Usage   
            sb.Append("<strong>");
            sb.Append("Server CPU Usage");
            sb.Append("</strong>");
            sb.Append("<br/>");
            sb.Append(objError.GetCPUUsagePercentage());
            sb.Append("<br/>");
            sb.Append("<br/>");

            //6 RAM usage
            sb.Append("<strong>");
            sb.Append("Server Physical Memory Usage");
            sb.Append("</strong>");
            sb.Append("<br/>");
            sb.Append(objError.GetPhysicalMemoryUsage());
            sb.Append("<br/>");
            sb.Append("<br/>");

            //8 Web Site usage
            sb.Append("<strong>");
            sb.Append("WebSite Memory Usage");
            sb.Append("</strong>");
            sb.Append("<br/>");
            sb.Append(objError.GetWebSiteWorkerMemoryUsage());
            sb.Append("<br/>");
            sb.Append("<br/>");

            return sb.ToString();
        }

        /// <summary>
        /// Request for SEO URL
        /// </summary>
        /// <param name="brokenLinkUrl">brokenLinkUrl used to generate SEo url</param>
        /// <returns>SEO url in string</returns>
        private string GetRequestedSEOUrlLink(string brokenLinkUrl)
        {
            string requestedSEOUrl = string.Empty;
            string leftURL = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
            if (leftURL.Trim().EndsWith("/") == false)
            {
                leftURL = leftURL.Trim() + "/";
            }
            requestedSEOUrl = leftURL + GetSEOUrlNameFromBrokenLink(brokenLinkUrl) + ".aspx";
            return requestedSEOUrl;
        }

        /// <summary>
        /// Get SEO URL Name
        /// </summary>
        /// <param name="brokenLinkUrl">brokenLinkUrl used to retrive URL name</param>
        /// <returns>SEO URL name in String</returns>
        private string GetSEOUrlNameFromBrokenLink(string brokenLinkUrl)
        {
            string seoUrlName = string.Empty;
            int startIndex = 0;
            int endIndex = 0;

            startIndex = brokenLinkUrl.LastIndexOf("/");
            if (startIndex > 0)
            {
                endIndex = brokenLinkUrl.LastIndexOf(".aspx");
                if (endIndex > startIndex && endIndex > 0)
                {
                    int length = endIndex - startIndex;
                    seoUrlName = brokenLinkUrl.Substring(startIndex, length);
                    seoUrlName = seoUrlName.Trim('/');
                }
            }
            return seoUrlName;
        }

        /// <summary>
        /// Get Interal URL link
        /// </summary>
        /// <param name="currentSEOUrlName">currentSEOUrlName used for retriving Interal URL link</param>
        /// <returns>string</returns>
        private string GetInternalUrlLinkFromSEOUrlName(string currentSEOUrlName)
        {
            string internalUrl = string.Empty;
            DataSet dsSEOURLDetails = null;
            string leftURL = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;

            if (leftURL.Trim().EndsWith("/") == false)
            {
                leftURL = leftURL.Trim() + "/";
            }

            //SP call
            Zeon_ErrorDiagnostics objError = new Zeon_ErrorDiagnostics();
            dsSEOURLDetails = objError.GetSEOURLDetailsBySEOURL(currentSEOUrlName);
            if (dsSEOURLDetails != null && dsSEOURLDetails.Tables.Count > 0)
            {
                if (dsSEOURLDetails.Tables[0].Rows.Count > 0)
                {
                    string entityType = dsSEOURLDetails.Tables[0].Rows[0]["EntityType"].ToString().ToUpper();
                    int entityID = 0;
                    int.TryParse(dsSEOURLDetails.Tables[0].Rows[0]["EntityID"].ToString(), out entityID);

                    if (entityType.Equals(SEOURLEntityType.PRODUCT.ToString(), StringComparison.OrdinalIgnoreCase) == true)
                    {
                        internalUrl = leftURL + "product.aspx?zpid=" + entityID.ToString();
                    }
                    else if (entityType.Equals(SEOURLEntityType.CATEGORY.ToString(), StringComparison.OrdinalIgnoreCase) == true)
                    {
                        internalUrl = leftURL + "category.aspx?zcid=" + entityID.ToString();
                    }
                }
            }

            return internalUrl;
        }

        /// <summary>
        ///  Get Broken Link from Non SEO URL Link
        /// </summary>
        /// <returns>Stirng</returns>
        private string GetBrokenLinkInternalNonSEOUrlLink()
        {
            string internalUrlLink = string.Empty;

            string brokenLink = Request.Url.AbsoluteUri;
            string seoURLName = GetSEOUrlNameFromBrokenLink(brokenLink);
            internalUrlLink = GetInternalUrlLinkFromSEOUrlName(seoURLName);

            return internalUrlLink;

        }

        /// <summary>
        /// Log Error in Log file
        /// </summary>
        /// <param name="ErrorMessage">ErrorMessage for </param>
        private void LogErrorOnFile(string ErrorMessage)
        {
            string enableErrorLoggingOn404Page = string.Empty;

            try
            {
                enableErrorLoggingOn404Page = ZNodeCatalogManager.MessageConfig.GetMessage("EnableErrorLoggingOn404Page", ZNodeConfigManager.SiteConfig.PortalID, ZNodeConfigManager.SiteConfig.LocaleID);
                if (!string.IsNullOrEmpty(enableErrorLoggingOn404Page) && enableErrorLoggingOn404Page.Equals("True", StringComparison.OrdinalIgnoreCase) == true)
                {
                    ZNodeLogging.LogMessage("<br/>404Page Log: " + ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogMessage(ex.ToString());
            }
        }

        /// <summary>
        /// Get Origional NON ASPX Requested URL
        /// </summary>
        /// <param name="url">string</param>
        /// <returns>string</returns>
        private string GetRequestedNonAspxUrlLink(string url)
        {
            string requestedURLlink = string.Empty;
            string leftURL = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
            if (leftURL.Trim().EndsWith("/") == false)
            {
                leftURL = leftURL.Trim() + "/";
            }
            requestedURLlink = leftURL+url.Substring(url.IndexOf('=') + 1);
            return requestedURLlink;
        }
        #endregion
    }
}