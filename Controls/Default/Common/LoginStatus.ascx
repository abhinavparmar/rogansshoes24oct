<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Common_LoginStatus" Codebehind="LoginStatus.ascx.cs" %>

    <asp:LoginStatus ID="uxUserLoginStatus" runat="server" LoginText="Log In" OnLoggingOut="UxUserLoginStatus_LoggingOut" OnLoggedOut="UxUserLoginStatus_LoggedOut" CssClass="LogButton" title="User Login"></asp:LoginStatus>

