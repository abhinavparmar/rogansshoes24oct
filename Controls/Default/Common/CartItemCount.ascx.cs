using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.Framework.Business;

/// <summary>
/// Displays number of items in the shopping cart
/// </summary>
namespace WebApp
{
    /// <summary>
    /// Represents the CartItemCount user control class.
    /// </summary>
    public partial class Controls_Default_Common_CartItemCount : System.Web.UI.UserControl
    {
        #region Private Variables
        private string CartItemCount = string.Empty;
        #endregion

        #region Public Methods
        public void CalcCartItemCount()
        {
            ZNodeShoppingCart shoppingCart = (ZNodeShoppingCart)ZNodeCheckout.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);

            if (shoppingCart != null)
            {
                lblCartItemCount.Text = GetShoppingCartTotalItemCount(shoppingCart.ShoppingCartItems);//shoppingCart.ShoppingCartItems.Count.ToString();//Replace old Code with Zeon Code
                lblCartSubTotal.Text = shoppingCart.SubTotal.ToString("c");
                lblMobileCartItemCount.Text = GetShoppingCartTotalItemCount(shoppingCart.ShoppingCartItems);//shoppingCart.ShoppingCartItems.Count.ToString();//Replace old Code with Zeon Code

            }
            else
            {
                lblCartItemCount.Text = "0";
                lblCartSubTotal.Text = this.GetLocalResourceObject("EmptyCartTotal").ToString();
                lblMobileCartItemCount.Text = "0";
            }
        }
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get shopping cart
            //this.CalcCartItemCount();//old Code
            //SetCartItemCount();//Zeon Custom Code to Resolve OrderRecipt page count
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            SetCartItemCount();
            RegisterClientJavascriptClass();
        }

        #endregion

        #region Zeon Custom Methods

        /// <summary>
        /// set cart Item count
        /// </summary>
        private void SetCartItemCount()
        {
            string files = Page.MasterPageFile;
            int position = files.LastIndexOf("/");
            string mastername = string.Empty;
            if (position > 0)
            {
                mastername = files.Substring(position).Replace("/", "");
            }
            if (mastername.ToLower().Equals("orderreceipt.master") || mastername.ToLower().Equals("notification.master"))
            {
                lblCartItemCount.Text = "0";
                lblMobileCartItemCount.Text = "0";
                lblCartSubTotal.Text = this.GetLocalResourceObject("EmptyCartTotal").ToString();
            }
            else
            {
                this.CalcCartItemCount();
            }

        }

        /// <summary>
        /// Checkout button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Checkout_Click(object sender, EventArgs e)
        {
            ZNodeShoppingCart shoppingCart = (ZNodeShoppingCart)ZNodeCheckout.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
            string link = "~/editaddress.aspx";
            if (shoppingCart == null || (shoppingCart != null && shoppingCart.ShoppingCartItems.Count == 0))
            {
                link = "~/shoppingcart.aspx";
            }  
            else if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                link = "~/login.aspx?returnurl=editaddress.aspx";
            }
            // Redirect to checkout page
            Response.Redirect(link);
        }

        /// <summary>
        /// Registers the script
        /// </summary>
        private void RegisterClientJavascriptClass()
        {
            StringBuilder script = new StringBuilder();
            script.Append("var miniCartFlyOut = new MiniCartFlyOut({");
            script.Append("});");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "MiniCartFlyOutScript", script.ToString(), true);
        }

        /// <summary>
        /// get Shopping cart item total quantity count
        /// </summary>
        /// <returns></returns>
        private string GetShoppingCartTotalItemCount(ZNodeGenericCollection<ZNodeShoppingCartItem> shoppingCartItems)
        {
            int totalQuantity = 0;
            foreach (ZNodeShoppingCartItem item in shoppingCartItems)
            {
                totalQuantity += item.Quantity;
            }
            return totalQuantity.ToString();
        }
        #endregion
    }
}