using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Framework.Business;

/// <summary>
/// Displays Sales Department Phone Number
/// </summary>
namespace WebApp
{
    /// <summary>
    /// Represents the SalesDepartmentPhone user control class.
    /// </summary>
    public partial class Controls_Default_Common_SalesDepartmentPhone : System.Web.UI.UserControl
    {
        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            uxSalesDepartmentPhone.Text = ZNodeConfigManager.SiteConfig.SalesPhoneNumber;
        }
        #endregion
    }
}