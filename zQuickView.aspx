﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="zQuickView.aspx.cs" Inherits="WebApp.zQuickView" %>

<!DOCTYPE html>

<%@ Register TagName="zQuickWatch" TagPrefix="Zeon" Src="~/Controls/Default/QuickWatch/ZQuickWatch.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scriptManager" runat="server">
        </asp:ScriptManager>
        <div class="QuickView">
            <Zeon:zQuickWatch ID="uxzQuickWatch" runat="server" />
        </div>
        <script src="js/jquery-1.9.0.min.js"></script>
        <script src="js/jquery-ui-1.10.4.js" type="text/javascript"></script>
        <script src="js/ZAttributeHelper.js?v=<%= ConfigurationManager.AppSettings["jsversion"].ToString() %>" type="text/javascript"></script>
        <script>
            $j(document).ready(function () {
                InitializeZoom();
            });
            function InitializeZoom() {
                $j(".ItemImg img").attr("onclick", "SetAltImage(this);");
            }

            function SetAltImage(obj) {
                var imgurl = $j(obj).attr('src').replace('/55/', '/385/');
                $j("#CatalogItemImage").attr("src", imgurl);
            }
        </script>
    </form>
</body>
</html>
