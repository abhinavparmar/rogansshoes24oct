<%@ Page Language="C#" AutoEventWireup="True" Title="Error" Inherits="WebApp.ErrorPageBase" CodeBehind="ErrorPageBase.cs" %>

<asp:content id="Content2" contentplaceholderid="MainContent" runat="server">
 
        <div id="Container" class="PageNotFound ErrorPage">
            <div class="PageTitle">An error has occurred</div>
            <p>
                An error occurred because of which we were unable to complete your request. Please click on the browser's back button to go back to the
                website. We apologize for the inconvenience.
            </p> 
            <br /><br />
        </div>
    
</asp:content>
