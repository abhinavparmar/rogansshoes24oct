﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EuporiaCustomerImport.aspx.cs" Inherits="WebApp.EuporiaCustomerImport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div><span>Please Click Here To Start Importing Customer</span>
            <asp:Button ID="btnImportCustomer" runat="server" Text="Import Customer" OnClick="btnImportCustomer_Click" />
        </div>
        <asp:Label ID="lblImportStatus" runat="server" ></asp:Label>
    </form>
</body>
</html>
