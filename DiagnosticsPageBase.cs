using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Net.Mail;
using System.Security.AccessControl;
using System.Security.Authentication;
using System.Security.Permissions;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the Diagnostics Page class.
    /// </summary>
    public partial class DiagnosticsPageBase : System.Web.UI.Page
    {
        #region Protected Variables
        private StringBuilder build = new StringBuilder();
        private int errorCount = 0;
        #endregion

        #region General Events
      
        #endregion

        #region Public Methods
        /// <summary>
        /// Get the product Version
        /// </summary>
        /// <returns>Result of Product Version</returns>
        public string GetProductVersion()
        {
            string result = string.Empty;
            try
            {
                string path = ZNode.Libraries.Framework.Business.ZNodeConfigManager.EnvironmentConfig.ApplicationPath + "/Bin/ZNode.Libraries.Framework.Business.dll";

                // Create Instance for FileVersionInfo object
                FileVersionInfo info = FileVersionInfo.GetVersionInfo(Server.MapPath(path));
                if (info != null)
                {
                    result = "- v" + info.FileVersion;
                }
            }
            catch (Exception generalException)
            {
                lblMsg.Text = generalException.ToString();
            }

            return result;
        }

        /// <summary>
        /// Catch and Release
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        public void Page_Error(object sender, EventArgs e)
        {
            // Check the EnableDiagnostics value in the config to allow user to run the diagnostics tool
            if (System.Configuration.ConfigurationManager.AppSettings["EnableDiagnosticsPage"].ToString() == "1")
            {
                Server.ClearError();
            }
        }

        /// <summary>
        /// Click Event for Email Diagnostics
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Button_Click(object sender, EventArgs e)
        {
            try
            {
                System.Net.WebClient http = new System.Net.WebClient();
                string mergedText = string.Empty;
                string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
                string recepientEmail = "test@znode.com";
                string subject = string.Format("Diagnostics for Case {0}", CaseNumber.Text);

                // Download the diagnostics page and save it to a string
                byte[] result = http.DownloadData(Request.Url.AbsoluteUri);
                mergedText = Encoding.Default.GetString(result);

                // If the ZnodeLog.txt file exists send it along with the diagnostics page
                string filepath = ZNodeConfigManager.EnvironmentConfig.DataPath + "/Logs//ZNodeLog.txt";
                if (File.Exists(Server.MapPath(filepath)))
                {
                    string logfile = string.Empty;
                    FileInfo f = new FileInfo(Server.MapPath(filepath));
                    
                    // If we have a small ZNodeLog.txt file under 250kb send it in, otherwise just send diagnostics
                    if (f.Length < 250000)
                    {
                        StreamReader s = new StreamReader(Server.MapPath(filepath));
                        logfile = s.ReadToEnd();
                        s.Close();
                        ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(recepientEmail, senderEmail, senderEmail, subject, logfile, false);
                    }
                }

                ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(recepientEmail, senderEmail, senderEmail, subject, mergedText, true);
                PnlExceptionSummary.Visible = true;
                lblMsg.Text = "Diagnostics Email has been Sent";
                btnEmailDiagnostics.Visible = false;
                btnEmailDiagnostics.Enabled = false;
                CaseNumber.Visible = false;
                CaseNumber.Enabled = false;
                lblCase.Visible = false;
            }
            catch (Exception generalException)
            {
                lblMsg.Text = "Error " + (++this.errorCount) + ": " + generalException.ToString() + "<br><br>";
                PnlExceptionSummary.Visible = true;
            }
        }
        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Log details before any exception
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DiagnosticsPage, HttpContext.Current.Request.UserHostAddress);

            // Check the EnableDiagnostics value in the config to allow user to run the diagnostics tool
            if (System.Configuration.ConfigurationManager.AppSettings["EnableDiagnosticsPage"].ToString() == "0")
            {
                throw new ApplicationException("This page is not enabled to diagnose the store settings.");
            }
            else
            {
                Page.Trace.IsEnabled = true;

                // Check for Postback
                if (!Page.IsPostBack)
                {
                    this.CheckDataBaseConnection();

                    this.CheckPermissions("~/Data/Default/");

                    this.LicenseStatus();

                    this.CheckEmailAccount();

                    this.GetVersionDetails();
                    if (this.build.Length > 0)
                    {
                        lblMsg.Text = this.build.ToString();
                        PnlExceptionSummary.Visible = true;
                    }

                    // Log details
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DiagnosticsSent, HttpContext.Current.Request.UserHostAddress);
                }
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Check for License Status
        /// </summary>
        private void LicenseStatus()
        {
            try
            {
                // Create Instance for License Manager
                ZNodeLicenseManager licenseManager = new ZNodeLicenseManager();
                ZNodeLicenseType licenseStatus = licenseManager.Validate();

                if (licenseStatus == ZNodeLicenseType.Invalid)
                {
                    lblLicenseStatus.CssClass = "Error";
                }
                else
                {
                    lblLicenseStatus.CssClass = "Success";
                }

                lblLicenseStatus.Text = licenseManager.GetStatusDescription();
            }
            catch (ZNodeLicenseException licenseException)
            {
                this.build.Append("Error " + (++this.errorCount) + ": " + licenseException.Message);
                this.build.Append("<br><br>");
                lblLicenseStatus.CssClass = "Error";
                lblLicenseStatus.Text = "License Error";
            }
            catch (Exception otherException)
            {
                this.build.Append("Error " + (++this.errorCount) + ": " + otherException.Message);
                this.build.Append("<br><br>");

                lblLicenseStatus.CssClass = "Error";
                lblLicenseStatus.Text = "License Error";
            }
        }

        /// <summary>
        /// Check for Database connection String
        /// </summary>
        private void CheckDataBaseConnection()
        {
            try
            {
                // Create Instance of Connection and Command Object
                SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString);

                // Open Connection
                myConnection.Open();

                // Close Connection
                myConnection.Close();

                // Release resources
                myConnection.Dispose();

                lblDatabaseStatus.Text = " Database Connection OK";
                lblDatabaseStatus.CssClass = "Success";
            }
            catch (Exception sqlException)
            {
                this.build.Append("Error " + (++this.errorCount) + ": " + sqlException.ToString());
                this.build.Append("<br><br>");

                // Set Failed message text 
                lblDatabaseStatus.Text = " Database Connection failed";
                lblDatabaseStatus.CssClass = "Error";
            }
        }

        /// <summary>
        /// Display Version detail from database
        /// </summary>
        private void GetVersionDetails()
        {
            MultifrontService storefrontservice = new MultifrontService();

            TList<Multifront> baseversion = storefrontservice.GetAll();
            if (baseversion.Count > 0)
            {
                baseversion.Sort("ID Asc");
                BaseVersionLabel.Text = string.Format(" Base Version: v{0}.{1}.{2}", baseversion[0].MajorVersion.ToString(), baseversion[0].MinorVersion.ToString(), baseversion[0].Build.ToString());
            }

            ActivityLogService versions = new ActivityLogService();

            // Activity log type id 100 is for patches
            TList<ActivityLog> versionlist = versions.GetByActivityLogTypeID(100);
            versionlist.Sort("ActivityLogID Asc");

            if (versionlist.Count > 0)
            {
                VersionDetails.DataSource = versionlist;
                VersionDetails.DataBind();
            }
        }

        /// <summary>
        /// Check Public Fodler for Read + Write Permissions
        /// </summary>
        /// <param name="path">Represents the path </param>
        private void CheckPermissions(string path)
        {
            bool check = false;

            try
            {
                FileInfo file = new FileInfo(Server.MapPath(path) + "test.txt");
                FileStream filestream = file.Create();

                filestream.Close();
                file.Delete();
                check = true;
            }
            catch (Exception)
            {
                this.build.Append("Error " + (++this.errorCount) + ":" + "Read / Write action is denied for Users<br><br>");

                // Set to false if folder has no Read/Write Rights
                check = false;
            }

            if (check)
            {
                lblPublicPermissions.CssClass = "Success";
                lblPublicPermissions.Text = " Data folder - Read + Write OK";
            }
            else
            {
                lblPublicPermissions.Text = " Data folder does not have Read+Write+Modify permissions";
                lblPublicPermissions.CssClass = "Error";
            }
        }

        /// <summary>
        /// Check SMTP Account
        /// </summary>
        private void CheckEmailAccount()
        {
            try
            {
                ZNodeEmail.SendEmail("noreply@WebApp.com", "noreply@WebApp.com", string.Empty, "Diagnostic Test", "SMTP Account Test", false);

                lblSMTPAccountStatus.Text = " SMTP Settings OK";
                lblSMTPAccountStatus.CssClass = "Success";
                btnEmailDiagnostics.Visible = true;
                btnEmailDiagnostics.Enabled = true;
                CaseNumber.Visible = true;
                CaseNumber.Enabled = true;
                lblCase.Visible = true;
            }
            catch (System.Net.Mail.SmtpException smtpException)
            {
                btnEmailDiagnostics.Visible = false;
                btnEmailDiagnostics.Enabled = false;
                CaseNumber.Visible = false;
                CaseNumber.Enabled = false;
                lblCase.Visible = false;
                this.build.Append("Error " + (++this.errorCount) + ": " + smtpException.ToString() + "<br><br>");
                lblSMTPAccountStatus.Text = " Could not connect to a valid SMTP Service.";
                lblSMTPAccountStatus.CssClass = "Error";
            }
            catch (Exception generalException)
            {
                btnEmailDiagnostics.Visible = false;
                btnEmailDiagnostics.Enabled = false;
                CaseNumber.Visible = false;
                CaseNumber.Enabled = false;
                lblCase.Visible = false;
                this.build.Append("Error " + (++this.errorCount) + ": " + generalException.ToString() + "<br><br>");
                lblSMTPAccountStatus.Text = " Could not connect to a valid SMTP Service.";
                lblSMTPAccountStatus.CssClass = "Error";
            }
        }
        #endregion
    }
}