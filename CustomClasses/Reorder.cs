﻿using System;
using System.Collections.Generic;
using System.Web;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the Reorder class
    /// </summary>
    public class Reorder
    {
        /// <summary>
        /// Initializes a new instance of the Reorder class.
        /// </summary>
        public Reorder()
        {
        }

        /// <summary>
        /// Reorder Item Method
        /// </summary>
        /// <param name="orderlineItemId">Order Line Item Id</param>
        /// <param name="productNum">The value of Product Number</param>
        /// <param name="sku">The value of sku</param>
        /// <param name="quantity">The value of quantity</param>
        /// <returns>Returns a bool to reorder item or not</returns>
        public bool ReorderItem(int orderlineItemId, string productNum, string sku, int quantity)
        {
            int ProductID = 0;
            ZNodeProduct _product = null;

            ZNodeAddOnList _AddOnList = new ZNodeAddOnList();

            ZNodeSKU productSKU = new ZNodeSKU();

            if (sku.Length > 0)
            {
                productSKU = ZNodeSKU.CreateBySKU(sku);

                if (productSKU != null && productSKU.SKUID > 0)
                {
                    _product = ZNodeProduct.Create(productSKU.ProductID, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);
                    _product.SelectedSKU = productSKU;
                }
            }

            if (_product == null)
            {
                if (productNum.Length > 0)
                {
                    ProductService productService = new ProductService();
                    ProductQuery filters = new ProductQuery();
                    filters.AppendEquals(ProductColumn.ProductNum, productNum);
                    TList<Product> productList = productService.Find(filters.GetParameters());

                    if (productList != null)
                    {
                        if (productList.Count == 0)
                        {
                            // If SKUID or Invalid SKU is Zero then display Error Message                        
                            return false;
                        }
                    }
                    else
                    {
                        // If SKUID or Invalid SKU is Zero                    
                        return false;
                    }

                    ProductID = productList[0].ProductID;

                    _product = ZNodeProduct.Create(ProductID, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);
                }
            }

            if (_product == null || _product.Hide || _product.CallForPricing || !_product.IsActive)
            {
                return false;
            }

            CategoryService categoryService = new CategoryService();
            int total;
            TList<Category> categoryList = categoryService.GetByPortalID(ZNodeConfigManager.SiteConfig.PortalID, 0, 1, out total);
            bool IsDefaultPortal = categoryList.Count == 0;
            if (!IsDefaultPortal && !_product.Franchisable)
            {
                return false;
            }

            ZNode.Libraries.DataAccess.Service.OrderLineItemService ordlineitemService = new OrderLineItemService();
            TList<OrderLineItem> orderlineitems = ordlineitemService.GetByParentOrderLineItemID(orderlineItemId);

            System.Text.StringBuilder addOnValues = new System.Text.StringBuilder();

            foreach (OrderLineItem orderlineitem in orderlineitems)
            {
                if (!(string.Compare(orderlineitem.SKU, string.Empty, true) == 0))
                {
                    AddOnValueService addonvalueserv = new AddOnValueService();
                    TList<AddOnValue> selectedAddon = addonvalueserv.GetBySKU(orderlineitem.SKU);
                    addonvalueserv.DeepLoad(selectedAddon);

                    SKUInventoryService addonvalueInventoryServ = new SKUInventoryService();
                    SKUInventory inventory = addonvalueInventoryServ.GetBySKU(orderlineitem.SKU);

                    if (inventory == null)
                    {
                        return false;
                    }

                    foreach (AddOnValue addonvalue in selectedAddon)
                    {
                        if (inventory.QuantityOnHand <= 0 && addonvalue.AddOnIDSource.AllowBackOrder == false && addonvalue.AddOnIDSource.TrackInventoryInd)
                        {
                            return false;
                        }

                        if (addOnValues.Length > 0)
                        {
                            addOnValues.Append(",");
                        }

                        addOnValues.Append(addonvalue.AddOnValueID.ToString());
                    }
                }
            }

            if (addOnValues.Length > 0)
            {
                // Get a sku based on attributes selected
                _AddOnList = ZNodeAddOnList.CreateByProductAndAddOns(_product.ProductID, addOnValues.ToString());
            }

            if (_AddOnList != null)
            {
                ZNodeAddOnList SelectedAddOn = new ZNodeAddOnList();

                // Get a sku based on Add-ons selected
                SelectedAddOn = ZNodeAddOnList.CreateByProductAndAddOns(_product.ProductID, addOnValues.ToString());
                SelectedAddOn.SelectedAddOnValues = addOnValues.ToString();

                // Set Selected Add-on 
                _product.SelectedAddOnItems = SelectedAddOn;
            }

            if (productSKU.AttributesDescription != null)
            {
                //productSKU.AttributesDescription = productSKU.AttributesDescription + "<br />";//Old Code
                //Zeon Custom Formatting of Attribute Description
                string modifiedAttributeDesc = string.Empty;
                string tempAttr = productSKU.AttributesDescription.Replace("<br />", "* ");
                string[] attrArray = tempAttr.Split('*');
                foreach (string attribute in attrArray)
                {
                    string productAttrDec = "<span><strong>#attrname#:</strong> #attrValue#&nbsp;</span>";
                    string[] currentAttr = attribute.Split('-');
                    modifiedAttributeDesc = modifiedAttributeDesc + productAttrDec.Replace("#attrname#",currentAttr[0].Trim()).Replace("#attrValue#",currentAttr[1].Trim());
                }
                productSKU.AttributesDescription = modifiedAttributeDesc + "<br />";
            }

            // Create shopping cart item
            ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
            item.Product = new ZNodeProductBase(_product);
            item.Quantity = quantity;
            //Zeon Custom Code:Starts
            item.Notes = GetLineItemNotes(orderlineItemId);
            //Zeon Custom Code:Ends

            // Add product to cart
            ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

            // If shopping cart is null, create in session
            if (shoppingCart == null)
            {
                shoppingCart = new ZNodeShoppingCart();
                shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
            }

            // Add item to cart
            if (shoppingCart.AddToCart(item))
            {
                return true;
            }
            else
            {
                // If Product is out of Stock                 
                return false;
            }
        }

        #region Zeon Custom Code

        /// <summary>
        /// Get OrderLineItemString
        /// </summary>
        /// <param name="OrderLineItemID">string</param>
        /// <returns>string</returns>
        protected string GetLineItemNotes(int orderLineItemID)
        {
            string notesString = string.Empty;
            ZNode.Libraries.DataAccess.Service.OrderLineItemExtnService orderLineExtnSer = new ZNode.Libraries.DataAccess.Service.OrderLineItemExtnService();
            TList<OrderLineItemExtn> lineExtnList = orderLineExtnSer.GetByOrderLineItemID(orderLineItemID);
            if (lineExtnList != null && lineExtnList.Count > 0 && !string.IsNullOrEmpty(lineExtnList[0].Notes))
            {
                notesString = lineExtnList[0].Notes;
            }
            return notesString;
        }

        #endregion
    }
}