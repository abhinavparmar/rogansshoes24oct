﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;


namespace WebApp.CustomClasses
{
    public static class ZLoginCookieHelper
    {
        #region Remember Me Changes Header

        private enum LoginRememberMe
        {
            LoginRememberMe,
            LoginName,
            LoginPassword
        }

        #endregion

        #region Remember Me Changes

        /// <summary>
        /// Get Login Remember Me Cookie
        /// </summary>
        /// <returns></returns>
        public static string GetLoginRememberMeCookie()
        {
            string loginRememberMe = string.Empty;
            if (HttpContext.Current.Request.Cookies[LoginRememberMe.LoginRememberMe.ToString()] != null)
            {
                //Grab the cookie
                HttpCookie cookieRememberMe = HttpContext.Current.Request.Cookies[LoginRememberMe.LoginRememberMe.ToString()];
                if (cookieRememberMe != null)
                {
                    //loginRememberMe = HttpUtility.HtmlEncode(cookieRememberMe.Name);
                    loginRememberMe = cookieRememberMe.Name;
                }
            }
            return loginRememberMe;
        }

        /// <summary>
        /// Get Login Name From Cookie
        /// </summary>
        /// <returns></returns>
        public static string GetLoginNameFromCookie()
        {
            string loginName = string.Empty;
            if (HttpContext.Current.Request.Cookies[LoginRememberMe.LoginRememberMe.ToString()] != null)
            {
                //Grab the cookie
                HttpCookie cookieRememberMe = HttpContext.Current.Request.Cookies[LoginRememberMe.LoginRememberMe.ToString()];
                if (cookieRememberMe != null)
                {
                    if (HttpUtility.HtmlEncode(cookieRememberMe.Values[LoginRememberMe.LoginName.ToString()]) != null)
                    {
                        loginName = HttpUtility.HtmlEncode(cookieRememberMe.Values[LoginRememberMe.LoginName.ToString()]);
                    }
                }
            }
            return loginName;
        }

        /// <summary>
        /// Get Login Password From Cookie
        /// </summary>
        public static string GetLoginPasswordFromCookie()
        {
            ZNodeEncryption decrypt = new ZNodeEncryption();
            string loginPassword = string.Empty;
            if (HttpContext.Current.Request.Cookies[LoginRememberMe.LoginRememberMe.ToString()] != null)
            {
                //Grab the cookie
                HttpCookie cookieRememberMe = HttpContext.Current.Request.Cookies[LoginRememberMe.LoginRememberMe.ToString()];
                if (cookieRememberMe != null)
                {
                    string password = HttpUtility.HtmlEncode(cookieRememberMe.Values[LoginRememberMe.LoginPassword.ToString()]);
                    if (password != null)
                    {
                        password = decrypt.DecryptData(password);
                        loginPassword = password;
                    }
                }
            }
            return loginPassword;
        }

        /// <summary>
        /// Clear Login Remember Me Cookie
        /// </summary>
        public static void ClearLoginRememberMeCookie()
        {
            //Expire the cookie                
            HttpContext.Current.Response.Cookies[LoginRememberMe.LoginRememberMe.ToString()].Expires = DateTime.Now.AddDays(-20);
        }

        /// <summary>
        /// Save Login Remember Me Cookie
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        public static void SaveLoginRememberMeCookie(string username, string password)
        {
            ZNodeEncryption encrypt = new ZNodeEncryption();
            password = encrypt.EncryptData(password);
            //Check if the browser support cookies 
            if ((HttpContext.Current.Request.Browser.Cookies))
            {
                HttpCookie cookieLoginRememberMe = new HttpCookie(LoginRememberMe.LoginRememberMe.ToString());
                cookieLoginRememberMe.Values[LoginRememberMe.LoginName.ToString()] = username;
                cookieLoginRememberMe.Values[LoginRememberMe.LoginPassword.ToString()] = password;

                cookieLoginRememberMe.Expires = DateTime.Now.AddDays(Convert.ToDouble(ConfigurationManager.AppSettings["CookieExpiresValue"]));
                HttpContext.Current.Response.Cookies.Add(cookieLoginRememberMe);
            }
        }

        /// <summary>
        /// Save Login Remember Me Cookie
        /// </summary>
        /// <param name="username"></param>
        public static void SaveLoginRememberMeCookie(string username)
        {
            ZNodeEncryption encrypt = new ZNodeEncryption();
            //Check if the browser support cookies 
            if ((HttpContext.Current.Request.Browser.Cookies))
            {
                HttpCookie cookieLoginRememberMe = new HttpCookie(LoginRememberMe.LoginRememberMe.ToString());
                cookieLoginRememberMe.Values[LoginRememberMe.LoginName.ToString()] = username;
                cookieLoginRememberMe.Expires = DateTime.Now.AddDays(Convert.ToDouble(ConfigurationManager.AppSettings["CookieExpiresValue"]));
                HttpContext.Current.Response.Cookies.Add(cookieLoginRememberMe);
            }
        }

        #endregion

    }
}
