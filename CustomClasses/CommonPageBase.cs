﻿using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.ShoppingCart;
using System.Text.RegularExpressions;
using SquishIt.Framework;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for CommonPageBase
/// </summary>
namespace WebApp
{
    /// <summary>
    /// Represents a Common Page Base class
    /// </summary>
    public class CommonPageBase : ZNodePageBase
    {

        #region Member Variables

        private const string PostBackEventTarget = "__EVENTTARGET";
        private int _categoryId;
        private int _productId;
        private int _lastcategoryId;
        private bool IsApplyStyle = true;
        private ContentPage _ContentPage;

        public ContentPage ContentPage
        {
            get { return _ContentPage; }
            set { _ContentPage = value; }
        }

        private ZNodeCategory _Category;

        public ZNodeCategory Category
        {
            get { return _Category; }
            set { _Category = value; }
        }

        private ZNodeProduct _Product;

        /// <summary>
        /// Gets or sets the ZNodeProduct
        /// </summary>
        public ZNodeProduct Product
        {
            get { return _Product; }
            set { _Product = value; }
        }

        private string LanguageDropDownName = "$ddlLanguage";

        #endregion

        #region Public Methods
        /// <summary>
        /// Get the UI Culture
        /// </summary>
        /// <returns>Returns the current UI culture</returns>
        public static string GetCurrentUICulture()
        {
            string CurrentUICulture = string.Empty;

            if (HttpContext.Current.Session["NewCulture"] != null)
            {
                CurrentUICulture = HttpContext.Current.Session["NewCulture"].ToString();
                HttpContext.Current.Session.Remove("NewCulture");
            }
            else
            {
                // Set Culture From Cookie
                if (HttpContext.Current.Request.Cookies["CultureLanguage"] != null)
                {
                    CurrentUICulture = HttpContext.Current.Request.Cookies["CultureLanguage"].Value;
                }
            }

            Locale validLocale = ZNodeCatalogManager.GetLocaleByLocaleCode(CurrentUICulture);

            if (validLocale != null)
            {
                // Set the localeId
                ZNode.Libraries.ECommerce.SEO.ZNodeSEOUrl.LocaleID = validLocale.LocaleID;

                CurrentUICulture = validLocale.LocaleCode;
            }

            return CurrentUICulture;
        }

        /// <summary>
        /// Set UI culture value in cookie.
        /// </summary>
        /// <param name="cultureValue">Culture Value</param>
        public void SetUICulture(string cultureValue)
        {
            // Remove the existing Cookie
            Response.Cookies.Remove("CultureLanguage");

            // Set Culture in Cookie
            HttpCookie Cookie = new HttpCookie("CultureLanguage");
            Cookie.Value = cultureValue;
            Cookie.Expires = DateTime.Now.AddDays(Convert.ToDouble(ConfigurationManager.AppSettings["CookieExpiresValue"]));
            Response.Cookies.Add(Cookie);

            HttpContext.Current.Session["NewCulture"] = cultureValue;

            // Set Culture in Session
            Session["CurrentUICulture"] = cultureValue;

            // Set NULL to last viewed category when locale change.
            Session["BreadCrumzcid"] = null;

            // Reset the Session and Cache Value
            System.Web.HttpContext.Current.Session.Remove("CatalogConfig");

            // Set Culture in Thread
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(GetCurrentUICulture());

            // Set locale id in Cookie
            HttpCookie cookieLocaleId = new HttpCookie("LocaleID");
            cookieLocaleId.Value = ZNodeCatalogManager.LocaleId.ToString();
            cookieLocaleId.Expires = DateTime.Now.AddDays(Convert.ToDouble(ConfigurationManager.AppSettings["CookieExpiresValue"]));
            Response.Cookies.Add(cookieLocaleId);

            // Redirect to homepage.
            Response.Redirect("~/default.aspx");
        }
        #endregion

        #region Protected Methods and Events1
        /// <summary>
        /// Get Category Instance method
        /// </summary>
        /// <param name="categoryId">Represents the Category ID</param>
        protected void GetCategoryInstance(int categoryId)
        {
            // Default master page file path
            string masterPageFilePath = "~/themes/" + ZNodeCatalogManager.Theme + "/MasterPages/Category/category.master";

            // Check whether the category is enabled in category node. If not then redirect to home page
            CategoryNodeService categoryNodeService = new CategoryNodeService();
            string filterExpression = string.Format("CatalogID = {0} AND CategoryID = {1}", ZNodeCatalogManager.CatalogConfig.CatalogID, categoryId);
            TList<CategoryNode> categoryNodeList = categoryNodeService.Find(filterExpression);
            if (categoryNodeList == null || categoryNodeList.Count == 0)
            {
                Response.Redirect("~/default.aspx");
            }

            //Zeon Custom Code:start
            string cacheKey = string.Format("{0}_{1}_{2}_{3}", "CategoryListPage", categoryId, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);
            this.Category = HttpContext.Current.Cache[cacheKey] as ZNodeCategory;
            if (this.Category == null)
            {
                //Zeon Custom Code:end
                // Retrieve category data
                this.Category = ZNodeCategory.Create(categoryId, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);
                ZNodeCacheDependencyManager.Insert(cacheKey, this.Category, "ZNodeCategory", "ZNodeCategoryNode"); //zeon custom code
            }

            if (this.Category != null && this.Category.VisibleInd)
            {
                // Set Template
                if (this.Category.MasterPage != null && this.Category.MasterPage.Length > 0 && this.Category.Theme != null && this.Category.Theme.Length > 0)
                {
                    string masterFilePath = ZNodeConfigManager.EnvironmentConfig.ApplicationPath + "/Themes/" + this.Category.Theme + "/MasterPages/Category/" + this.Category.MasterPage + ".master";

                    if (System.IO.File.Exists(Server.MapPath(masterFilePath)))
                    {
                        // If template master page exists, then override default master page
                        masterPageFilePath = masterFilePath;
                    }
                }

                // Set CSS
                if (this.Category.Css != null && this.Category.Css.Length > 0 && this.Category.Theme != null && this.Category.Theme.Length > 0)
                {
                    this.IsApplyStyle = false;
                }
            }
            else
            {
                Response.Redirect("~/default.aspx");
            }

            // If department wise theme selected then save the theme to use in quick search and newsletter sign-up page.
            if (!string.IsNullOrEmpty(this.Category.Theme))
            {
                Session["CurrentCategoryTheme"] = this.Category.Theme;
            }

            // Set master page
            this.MasterPageFile = masterPageFilePath;

            // Add to httpContext
            HttpContext.Current.Items.Add("Category", this.Category);

            // Set SEO Properties 
            ZNodeSEO seo = new ZNodeSEO();

            // Set Default SEO values
            ZNodeMetaTags tags = new ZNodeMetaTags();
            seo.SEOTitle = tags.CategoryTitle(this.Category);
            seo.SEODescription = tags.CategoryDescription(this.Category);
            seo.SEOKeywords = tags.CategoryKeywords(this.Category);
        }

        /// <summary>
        /// Get Product Instance method
        /// </summary>
        /// <param name="productID">The value of Product ID</param>
        protected void GetProductInstance(int productID)
        {
            // Default master page file path
            string masterPageFilePath = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/MasterPages/Product/Product.master";

            if (Session["ProductObject"] != null)
            {
                // Retrieve product data
                this.Product = (ZNodeProduct)Session["ProductObject"];
                if (this.Product.ProductID != this._productId)
                {
                    this.Product = null;
                }
            }

            //Zeon Custom Code:Starts
            //ZNodeUserAccount usersAccount = ZNodeUserAccount.CurrentAccount();
            //string cacheKey = string.Format("{0}_{1}_{2}_{3}", "ProductPage", productID, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);

            //if (usersAccount != null && usersAccount.UserID != null)
            //{
            //    cacheKey = string.Format("{0}_{1}_{2}_{3}_{4}", "ProductPage", productID, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId,usersAccount.UserID);
            //}           

            //this.Product = HttpContext.Current.Cache[cacheKey] as ZNodeProduct;

            if (this.Product == null)
            {
                // Retrieve product data
                this.Product = ZNodeProduct.Create(this._productId, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);
                //Zeon Custom Code:Starts
                if (this.Product != null)
                {
                    this.Product.ProductAllAttributeCollection = GetProductAttributeCollectionByProductID();
                }
                //ZNodeCacheDependencyManager.Insert(cacheKey, this.Product, System.DateTime.UtcNow.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration, "ZNodeProduct", "ZNodeSKU", "ZNodeProductCategory", "ZNodePromotion");
                //Zeon Custom Code::Ends
            }

            if (Session["BreadCrumbCategoryId"] != null)
            {
                // Get categoryid from session
                this._lastcategoryId = int.Parse(Session["BreadCrumbCategoryId"].ToString());
            }

            if (this.Product != null && this.Product.IsActive)
            {
                foreach (ZnodeProductCategory productCategory in this.Product.ZNodeProductCategoryCollection)
                {
                    if (this._lastcategoryId == productCategory.CategoryID)
                    {
                        // Set Template
                        if (productCategory.MasterPage.Length > 0 && productCategory.MasterPage != null)
                        {
                            string masterFilePath = ZNodeConfigManager.EnvironmentConfig.ApplicationPath + "/Themes/" + productCategory.Theme + "/MasterPages/Product/" + productCategory.MasterPage + ".master";

                            if (System.IO.File.Exists(Server.MapPath(masterFilePath)))
                            {
                                // If template master page exists, then override default master page
                                masterPageFilePath = masterFilePath;
                            }
                        }

                        // Set Theme
                        if (productCategory.CSS != null && productCategory.CSS.Length > 0 && productCategory.Theme != null && productCategory.Theme.Length > 0)
                        {
                            this.IsApplyStyle = false;
                            Session["CurrentCategoryTheme"] = productCategory.Theme;
                        }
                    }
                }
            }
            else
            {
                Response.Redirect("~/default.aspx");
            }

            // Set master page
            this.MasterPageFile = masterPageFilePath;

            // Add to http context so it can be shared with user controls
            HttpContext.Current.Items.Add("Product", this.Product);



            // Set SEO Properties
            ZNodeSEO seo = new ZNodeSEO();

            // Set SEO default values to the product 
            ZNodeMetaTags tags = new ZNodeMetaTags();
            seo.SEOTitle = tags.ProductTitle(this.Product);
            seo.SEODescription = tags.ProductDescription(this.Product);
            seo.SEOKeywords = tags.ProductKeywords(this.Product);
        }

        /// <summary>
        /// Get Content page Instance method
        /// </summary>
        /// <param name="pageName">Represents a Page name</param>
        protected void GetContentPageInstance(string pageName)
        {
            string masterPageFilePath = string.Empty;

            if (pageName == "home")
            {
                // Default home.master page file path
                masterPageFilePath = "~/themes/" + ZNodeCatalogManager.Theme + "/MasterPages/home.master";

                // Get Home page data
                this.ContentPage = ZNodeContentManager.GetPageByName("home", ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.CatalogConfig.LocaleID);
            }
            else
            {
                // Default master page file path
                masterPageFilePath = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/MasterPages/content.master";

                // Get content page data
                this.ContentPage = ZNodeContentManager.GetPageByName(pageName, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.CatalogConfig.LocaleID);
            }

            if (this.ContentPage != null && this.ContentPage.ActiveInd)
            {
                // Set Template
                if (this.ContentPage.MasterPage != null && this.ContentPage.MasterPage.Length > 0 && this.ContentPage.Theme.Length > 0 && this.ContentPage.Theme != null)
                {
                    string masterFilePath = ZNodeConfigManager.EnvironmentConfig.ApplicationPath + "/Themes/" + this.ContentPage.Theme + "/MasterPages/" + this.ContentPage.MasterPage + ".master";

                    // Only do the template override operation if the master page file exists                
                    if (System.IO.File.Exists(Server.MapPath(masterFilePath)))
                    {
                        // If template master page exists, then override default master page
                        masterPageFilePath = masterFilePath;
                    }
                }

                // Set Css
                if (this.ContentPage.CSS != null && this.ContentPage.Theme != null && this.ContentPage.CSS.Length > 0 && this.ContentPage.Theme.Length > 0)
                {
                    this.IsApplyStyle = false;
                }

                // Add to context for control access
                HttpContext.Current.Items.Add("PageTitle", this.ContentPage.Title);

                // SEO stuff
                ZNodeSEO seo = new ZNodeSEO();

                // Set Default SEO Values
                ZNodeMetaTags tags = new ZNodeMetaTags();
                seo.SEOTitle = tags.ContentTitle(this.ContentPage);
                seo.SEODescription = tags.ContentDescription(this.ContentPage);
                seo.SEOKeywords = tags.ContentKeywords(this.ContentPage);
                seo.SEOAdditionalMeta = tags.ContentAdditionalMeta(this.ContentPage);

                // Set master page
                this.MasterPageFile = masterPageFilePath;
            }
            else
            {
                Response.Redirect("~/default.aspx");
            }

        }

        /// <summary>
        /// Get Content Page Instance
        /// </summary>
        /// <param name="contentPageId">Content page Id to load.</param>
        protected void GetContentPageInstance(int contentPageId)
        {
            string masterPageFilePath = string.Empty;

            // Default master page file path
            masterPageFilePath = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/MasterPages/content.master";

            // Get content page data
            this.ContentPage = ZNodeContentManager.GetPageById(contentPageId);

            if (this.ContentPage != null && this.ContentPage.ActiveInd)
            {
                // Set Template
                if (this.ContentPage.MasterPage != null && this.ContentPage.MasterPage.Length > 0 && this.ContentPage.Theme.Length > 0 && this.ContentPage.Theme != null)
                {
                    string masterFilePath = ZNodeConfigManager.EnvironmentConfig.ApplicationPath + "/Themes/" + this.ContentPage.Theme + "/MasterPages/" + this.ContentPage.MasterPage + ".master";

                    // Only do the template override operation if the master page file exists                
                    if (System.IO.File.Exists(Server.MapPath(masterFilePath)))
                    {
                        // If template master page exists, then override default master page
                        masterPageFilePath = masterFilePath;
                    }
                }

                // Set Css
                if (this.ContentPage.CSS != null && this.ContentPage.CSS.Length > 0 && this.ContentPage.Theme.Length > 0 && this.ContentPage.Theme != null)
                {
                    this.IsApplyStyle = false;
                }

                // Add to context for control access
                HttpContext.Current.Items.Add("PageTitle", this.ContentPage.Title);

                // SEO stuff
                ZNodeSEO seo = new ZNodeSEO();

                // Set Default SEO Values
                ZNodeMetaTags tags = new ZNodeMetaTags();
                seo.SEOTitle = tags.ContentTitle(this.ContentPage);
                seo.SEODescription = tags.ContentDescription(this.ContentPage);
                seo.SEOKeywords = tags.ContentKeywords(this.ContentPage);
                seo.SEOAdditionalMeta = tags.ContentAdditionalMeta(this.ContentPage);
            }
            else
            {
                this.GetContentPageInstance("home");
            }

            // Set master page
            this.MasterPageFile = masterPageFilePath;
        }

        #endregion

        #region InitializeCulture Events
        /// <summary>
        /// Initializes a UI Culture
        /// </summary>
        protected override void InitializeCulture()
        {
            string cultureValue = string.Empty;

            if (Request[PostBackEventTarget] != null && Request[PostBackEventTarget].Contains(this.LanguageDropDownName))
            {
                string controlID = Request[PostBackEventTarget];

                if (controlID.Contains(this.LanguageDropDownName))
                {
                    cultureValue = Request.Form[Request[PostBackEventTarget]].ToString();

                    // Set Culture
                    if (cultureValue != "-1")
                    {
                        this.SetUICulture(cultureValue);
                    }
                    else
                    {
                        this.SetUICulture("en");
                    }
                }
            }
            else
            {
                // Set Culture From Session
                if (HttpContext.Current.Session["CurrentUICulture"] != null)
                {
                    cultureValue = HttpContext.Current.Session["CurrentUICulture"].ToString();
                }
                else
                {
                    cultureValue = GetCurrentUICulture();
                }

                // Set Culture in Thread
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(cultureValue);
            }

            // Reset the session value
            HttpContext.Current.Session["CurrentUICulture"] = cultureValue;

            // Set locale id in Cookie
            HttpCookie cookieLocaleId = new HttpCookie("LocaleID");
            cookieLocaleId.Value = ZNodeCatalogManager.LocaleId.ToString();
            cookieLocaleId.Expires = DateTime.Now.AddDays(Convert.ToDouble(ConfigurationManager.AppSettings["CookieExpiresValue"]));
            Response.Cookies.Add(cookieLocaleId);

            base.InitializeCulture();
        }

        #endregion

        #region Render

        /// <summary>
        /// This method overrides the Render() method for the page and moves the ViewState
        /// from its default location at the top of the page to the bottom of the page. 
        /// </summary>
        /// <param name="writer">Html text Write instance</param>
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            base.Render(hw);
            string html = sw.ToString();

            hw.Close();
            sw.Close();

            int start = html.IndexOf(@"<input type=""hidden"" name=""__VIEWSTATE""");

            if (start > -1)
            {
                int end = html.IndexOf("/>", start) + 2;

                string viewstate = html.Substring(start, end - start);
                html = html.Remove(start, end - start);

                int formend = html.IndexOf("</form>");
                html = html.Insert(formend, viewstate);
            }
            //Znode Custom code: Start for performance optimization
            if (!Request.RawUrl.ToLower().Contains("orderreceipt"))
            {
                html = MoveGACode(html);
            }

            html = RemoveHTMLBlankSpaces(html);
            //Znode Custom code: End
            writer.Write(html);
        }

        #endregion

        #region Pre_Init
        /// <summary>
        /// Page Pre-Initialization Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected virtual void Page_PreInit(object sender, EventArgs e)
        {
            // Clear the current category session theme if some other category selected.
            Session["CurrentCategoryTheme"] = null;

            // Get page id from querystring  
            string _pageName = string.Empty;

            // Get Category id from querystring  
            if (Request.Params["zcid"] != null)
            {
                this._categoryId = int.Parse(Request.Params["zcid"]);

                this.GetCategoryInstance(this._categoryId);
            }
            else if (Request.Params["zpid"] != null)
            {
                // Get product id from querystring 
                this._productId = int.Parse(Request.Params["zpid"]);

                this.GetProductInstance(this._productId);
            }
            else if (Request.Params["page"] != null)
            {
                _pageName = Request.Params["page"];

                this.GetContentPageInstance(_pageName);
            }
            else if (Request.Params["zpgid"] != null)
            {
                int _pageId = Convert.ToInt32(Request.Params["zpgid"]);

                this.GetContentPageInstance(_pageId);
            }
            else
            {
                if (ConfigurationManager.AppSettings["SkipHome"] != null && ConfigurationManager.AppSettings["SkipHome"].ToString() == "1") //Perfi: Custom code
                {
                    if (this.Request.RawUrl == "/product.aspx" || this.Request.RawUrl == "/category.aspx" || this.Request.RawUrl == "/content.aspx")
                    {
                        Response.Redirect("~/default.aspx");
                    }
                }
                _pageName = "home";
                this.GetContentPageInstance(_pageName);
            }
        }

        /// <summary>
        /// Set the Header Section
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected virtual void Page_Load(object sender, EventArgs e)
        {
            //Perfi:Custom Code Start
            if (this.Page.Header == null)
            {
                Response.Redirect("~/default.aspx");
            }
            //Perfi:Custom Code End

            BindThemeCSSSquishItOrWithout();

            //Zeon Custom Code :End
            // Apply coupon from querystring
            if (Request.Params["coupon"] != null)
            {
                ZNodeShoppingCart _shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();
                string couponcode = Request.Params["coupon"].ToString();

                // Check the coupon
                ZNode.Libraries.DataAccess.Service.PromotionService promoService = new PromotionService();

                PromotionQuery promoQuery = new PromotionQuery();
                promoQuery.Append(PromotionColumn.CouponCode, couponcode);
                promoQuery.Append(PromotionColumn.EnableCouponUrl, "true");

                TList<Promotion> promotionList = promoService.Find(promoQuery);

                if (promotionList.Count > 0)
                {
                    if (_shoppingCart == null)
                    {
                        _shoppingCart = new ZNodeShoppingCart();
                        _shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
                    }

                    _shoppingCart.AddCouponCode(couponcode);
                }
            }
            //Bind Canonical Tags
            BindCanonicalTags();
            AddFaviconIconOnPage();
            // Add a reference for skype meta tag and tel tag to the head section
            HtmlMeta metaTag = new HtmlMeta();
            metaTag.Name = "SKYPE_TOOLBAR";
            metaTag.Content = "SKYPE_TOOLBAR_PARSER_COMPATIBLE";
            if (this.Page.Header != null) this.Page.Header.Controls.Add(metaTag);
            HtmlMeta metaTelTag = new HtmlMeta();
            metaTelTag.Name = "format-detection";
            metaTelTag.Content = "telephone=no";
            if (this.Page.Header != null) this.Page.Header.Controls.Add(metaTelTag);
            // Add a reference for skype meta tag to the head section 
            //Bind Robot Meta Tags
            BindRobotMetaTags();

        }

        #endregion

        #region Zeon Custom Methods

        private static readonly Regex RegexBetweenTags = new Regex(@">(?! )\s+", RegexOptions.Compiled);
        private static readonly Regex RegexLineBreaks = new Regex(@"([\n\s])+?(?<= {2,})<", RegexOptions.Compiled);

        /// <summary>
        /// Remove HTML blank spaces for perormance increase and less load on browser view source
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        private static string RemoveHTMLBlankSpaces(string html)
        {
            if ((HttpContext.Current.Handler is Page && HttpContext.Current.Handler.GetType().Name != "SyncSessionlessHandler") && HttpContext.Current.Request["HTTP_X_MICROSOFTAJAX"] == null)
            {
                html = RegexBetweenTags.Replace(html, ">");
                html = RegexLineBreaks.Replace(html, "<");
                html = html.Trim();
            }
            return html;
        }

        /// <summary>
        /// set values in ProductAttributeCollection
        /// </summary>
        private DataTable GetProductAttributeCollectionByProductID()
        {
            DataTable dtProductAttribute = null;
            if (this.Product.ProductID > 0)
            {
                ProductHelper prdHelper = new ProductHelper();
                dtProductAttribute = prdHelper.GetProductAttributesByID(this.Product.ProductID);
            }
            return dtProductAttribute;
        }

        /// <summary>
        /// Bind Theme CSS Fo rSquishIt Or Without using web.config flag
        /// </summary>
        protected void BindThemeCSSSquishItOrWithout()
        {
            bool isapplySquishIT = ConfigurationManager.AppSettings["ApplySquishIT"] != null ?
            bool.Parse(ConfigurationManager.AppSettings["ApplySquishIT"].ToString()) : false;

            string userAgent = Request.UserAgent; //entire UA string
            string browser = Request.Browser.Type; //Browser name and Major Version #
            int browserVersion = Request.Browser.MajorVersion;

            if (this.IsApplyStyle)
            {
                string Include = string.Empty;
                // If request comes from mobile then load the theme from ZNodePortal.MobileTheme
                //Zeon Custom Code :Start
                if (Session["cs"] != null && ZNodeConfigManager.SiteConfig.MobileTheme != null)
                {
                    if (isapplySquishIT)
                    {
                        Include = AddSquishITBundleCSSForMobileDevices();
                    }
                    else
                    {
                        AddMobileBootStrapCSSForMobileDevices();

                    }
                }
                else
                {
                    if (isapplySquishIT)
                    {
                        Include = AddSquishITBundleCSS();
                    }
                    else
                    {
                        AddBundleCSS();
                    }
                }
                if (isapplySquishIT)
                {
                    if (!string.IsNullOrEmpty(userAgent) && !userAgent.Contains("Trident/5.0"))
                    {
                        Include = Include + "<link href='" + GetFontURL() + "' rel='stylesheet' type='text/css'>";
                        Include = Include + "<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><meta name='viewport' content='width=device-width, initial-scale=1'><meta name='viewport' content='minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no' />";
                        // Add a reference for StyleSheet to the head section
                        //this.Page.Header.Controls.Add(Include);
                        this.Page.Header.InnerHtml = Include;
                    }
                }
                else
                {
                    IncludeCSS(this, "" + GetFontURL() + "");
                    this.Page.Header.Controls.Add(new LiteralControl("<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><meta name='viewport' content='width=device-width, initial-scale=1'><meta name='viewport' content='minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no' />"));
                }
            }
            else if (Request.Params["zcid"] != null)
            {
                AddCategoryCSS(isapplySquishIT);
            }
            else if (Request.Params["zpid"] != null && Session["BreadCrumbCategoryId"] != null)
            {
                AddProductCSS(isapplySquishIT);
            }
            else
            {
                AddContentPageCSS(isapplySquishIT);
            }
        }

        /// <summary>
        /// Moves the GA code and places it immediately after the body tag.
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        private string MoveGACode(string html)
        {
            string strBodyTag = "<body";
            string gaStartComment = ConfigurationManager.AppSettings["GAStartComment"];
            string gaEndComment = ConfigurationManager.AppSettings["GAEndComment"];
            string strBodyTagHtml = ConfigurationManager.AppSettings["BodyTagHtml"];

            int strBodyTagLength = strBodyTagHtml.Length;
            int gaEndCommentLength = gaEndComment.Length;

            int gaStarts = html.IndexOf(gaStartComment);
            if (gaStarts > -1)
            {
                int gaEnds = html.IndexOf(gaEndComment);
                string googleTagManager = html.Substring(gaStarts, (gaEnds - gaStarts) + gaEndCommentLength);
                html = html.Remove(gaStarts, (gaEnds - gaStarts) + gaEndCommentLength);
                int bodyTag = html.IndexOf(strBodyTag) + strBodyTagLength;
                html = html.Insert(bodyTag, googleTagManager);
            }
            return html;
        }

        #region Canonical Tag Section

        /// <summary>
        /// Bind Canonical Tag 
        /// </summary>
        private void BindCanonicalTags()
        {
            string canonicalURL = string.Empty;

            //ZEON:Add Cannonical Tags on Category Page
            if (Request.Params["zcid"] != null && this.Category != null)
            {
                canonicalURL = string.IsNullOrEmpty(this.Category.SEOURL) ? string.Empty : this.Category.SEOURL.ToLower().ToString();
            }
            //ZEON:Add Cannonical Tags on Category Page
            if (Request.Params["zpid"] != null && this.Product != null)
            {
                canonicalURL = string.IsNullOrEmpty(this.Product.SEOURL) ? string.Empty : this.Product.SEOURL.ToLower().ToString();
            }
            if (Request.Params["mid"] != null)
            {
                canonicalURL = GetBrandSEOURL();
            }
            if (Request.Params["page"] != null && this.ContentPage != null)
            {
                canonicalURL = !string.IsNullOrEmpty(this.ContentPage.SEOURL) ? this.ContentPage.SEOURL : this.ContentPage.Name;
            }
            if (!string.IsNullOrEmpty(canonicalURL))
            {
                /*Nivi Code Start:8/10/2018 
                 Just made http to https 
                 */
                string siteUrl = "https://" + this.Request.Url.Authority + "/";
                if (this.Request.Url.Authority.StartsWith("www."))
                {
                    siteUrl = "https://" + this.Request.Url.Authority + "/";
                }
                else
                {
                    siteUrl = "https://www." + this.Request.Url.Authority + "/";
                }
                /*Nivi Code End:8/10/2018            
                 */
                HtmlGenericControl IncludeConanical = new HtmlGenericControl("link");
                IncludeConanical.Attributes.Add("rel", "canonical");
                IncludeConanical.Attributes.Add("href", siteUrl + canonicalURL);

                // Add a reference for canonical to the head section
                if (this.Page.Header != null) this.Page.Header.Controls.Add(IncludeConanical);
            }
            //Bind Canonical Tag On Perticuler Pages
            if (Request.Url.AbsolutePath.ToLower().Contains("saledetails") || Request.Url.AbsolutePath.ToLower().Contains("contact") || Request.Url.AbsolutePath.ToLower().Contains("sitemap") || Request.Url.AbsolutePath.ToLower().Contains("shopbybrand")
                || Request.Url.AbsolutePath.ToLower().Contains("shoefinder") || Request.Url.AbsolutePath.ToLower().Contains("scheduleatruck") || Request.Url.AbsolutePath.ToLower().Contains("storelocator") || Request.Url.AbsolutePath.ToLower().Contains("unsubscribe"))
            {
                BindCanonicalTagOnNonSEOPages();
            }
        }

        /// <summary>
        /// Get Manufacturer Extn SEO URL Value 
        /// </summary>
        /// <returns>string</returns>
        private string GetBrandSEOURL()
        {
            string seoUrl = string.Empty;
            if (HttpContext.Current.Items["BrandExtn"] != null)
            {
                ZNodeManufacturer manufact = null;
                if (HttpContext.Current.Items["Brand"] != null) { manufact = (ZNodeManufacturer)HttpContext.Current.Items["Brand"]; }
                ManufacturerExtn manufactExtn = (ManufacturerExtn)HttpContext.Current.Items["BrandExtn"];
                seoUrl = !string.IsNullOrEmpty(manufactExtn.SEOURL) ? manufactExtn.SEOURL : string.Empty;
                if (string.IsNullOrEmpty(seoUrl) && manufact != null)
                {
                    seoUrl = manufact.Name;
                }
            }
            return seoUrl;
        }

        /// <summary>
        /// Bind Robot Meta Tags
        /// </summary>
        public void BindRobotMetaTags()
        {
            string tagContent = string.Empty;
            bool isIndexFollow = true;
            bool isCustomNoIndexedPage = IsCustomNoIndexedPage();
            if (Request.Params["zcid"] != null && this.Category != null)
            {
                isIndexFollow = this.Category.IsIndexFollow;
            }
            if ((Request.Params["zpid"] != null && this.Product != null) && !isCustomNoIndexedPage)
            {
                isIndexFollow = this.Product.IsIndexFollow;
                if (Request.Params["mode"] != null && Request.Params["mode"].ToString().Equals("review", StringComparison.CurrentCultureIgnoreCase))
                {
                    isIndexFollow = false;
                }
            }
            else if (isCustomNoIndexedPage)
            {
                isIndexFollow = false;
            }

            tagContent = !isIndexFollow ? "NOINDEX, FOLLOW" : "INDEX, FOLLOW";
            if (!string.IsNullOrWhiteSpace(tagContent))
            {
                // Add a reference for skype meta tag and tel tag to the head section
                HtmlMeta roboMetaTag = new HtmlMeta();
                roboMetaTag.Name = "ROBOTS";
                roboMetaTag.Content = tagContent;
                if (this.Page.Header != null) this.Page.Header.Controls.Add(roboMetaTag);
            }
        }

        #endregion

        /// <summary>
        /// Include the CSS on the page
        /// </summary>
        /// <param name="page"></param>
        /// <param name="cssfile"></param>
        public static void IncludeCSS(Page page, string cssfile)
        {
            HtmlGenericControl child = new HtmlGenericControl("link");
            child.Attributes.Add("rel", "stylesheet");
            child.Attributes.Add("href", cssfile);
            child.Attributes.Add("type", "text/css");
            page.Header.Controls.Add(child);
        }

        /// <summary>
        /// Include the MetaTag on the page
        /// </summary>
        /// <param name="page"></param>
        /// <param name="name"></param>
        /// <param name="content"></param>
        public static void IncludeMetaTag(Page page, string name, string content)
        {
            var keywords = new HtmlMeta { Name = name, Content = content };
            page.Header.Controls.Add(keywords);
        }

        /// <summary>
        /// Apply IE9 Css Patch
        /// </summary>
        /// <param name="include"></param>
        protected void ApplyIE9CssPatch(string include)
        {
            IncludeCSS(this, "bootstrap/css/bootstrap.min.css");
            IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery.selectbox.css");
            IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui-1.10.0.custom.min.css");
            IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui.css");
            IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/Css/ZMyPrintVersion.css");
            IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/Css/" + ZNodeCatalogManager.Theme + "_Client.css"); //PRFT Custom Code

            //cssFile = new Literal() { Text = @"<link href=""" + ZNodeCatalogManager.GetCssPathByLocale(ZNodeCatalogManager.Theme, ZNodeCatalogManager.CSS) + @""" type=""text/css"" rel=""stylesheet"" />" };
            //this.Page.Header.Controls.Add(cssFile);

            include = (Bundle.Css()
                .Add(ZNodeCatalogManager.GetCssPathByLocale(ZNodeCatalogManager.Theme, ZNodeCatalogManager.CSS))
                .Render("~/Squished/CSS/squished_" + ZNodeCatalogManager.Theme + "_#.css?v=" + ConfigurationManager.AppSettings["CSSVersion"].ToString()));

            // Add a reference for StyleSheet to the head section
            if (this.Page.Header != null)
            {
                include = include + "<link href='" + GetFontURL() + "' rel='stylesheet' type='text/css'>";
                include = include + "<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><meta name='viewport' content='width=device-width, initial-scale=1'><meta name='viewport' content='minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no' />";

                Literal cssFile = new Literal() { Text = include };
                this.Page.Header.Controls.Add(cssFile);
            }
        }

        /// <summary>
        /// Get Font URL
        /// </summary>
        /// <returns></returns>
        public string GetFontURL()
        {
            string fontURL = string.Empty;
            if (Request.IsSecureConnection)
            {
                fontURL = "https://fonts.googleapis.com/css?family=Oswald:400,300,700|Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,900,900italic,700italic" + "?v=" + ConfigurationManager.AppSettings["CSSVersion"].ToString();
            }
            else
            {
                fontURL = "http://fonts.googleapis.com/css?family=Oswald:400,300,700|Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,900,900italic,700italic" + "?v=" + ConfigurationManager.AppSettings["CSSVersion"].ToString(); ;
            }
            return fontURL;
        }

        /// <summary>
        /// Add Favicon Icon On page
        /// </summary>
        public void AddFaviconIconOnPage()
        {
            HtmlGenericControl child = new HtmlGenericControl("link");
            child.Attributes.Add("rel", "shortcut icon");
            child.Attributes.Add("href", "/Themes/" + ZNodeCatalogManager.Theme + "/Favicon/favicon.ico");
            child.Attributes.Add("type", "image/x-icon");
            this.Page.Header.Controls.Add(child);

            HtmlGenericControl childIcon = new HtmlGenericControl("link");
            childIcon.Attributes.Add("rel", "icon");
            child.Attributes.Add("href", "/Themes/" + ZNodeCatalogManager.Theme + "/Favicon/favicon.ico");
            childIcon.Attributes.Add("type", "image/ico");
            this.Page.Header.Controls.Add(childIcon);
        }

        /// <summary>
        /// Add Bundle CSS for Mobile Devices
        /// </summary>
        /// <returns>string</returns>
        private string AddSquishITBundleCSSForMobileDevices()
        {
            string userAgent = Request.UserAgent; //entire UA string
            string include = string.Empty;
            if (!string.IsNullOrEmpty(userAgent) && userAgent.Contains("Trident") && userAgent.Contains("Trident/5.0"))
            {
                ApplyIE9CssPatch(include);
            }
            else
            {
                include = Bundle.Css()
                    .Add("~/bootstrap/css/bootstrap.min.css")
                    .Add(ZNodeCatalogManager.GetCssPathByLocale(ZNodeConfigManager.SiteConfig.MobileTheme, ZNodeCatalogManager.CSS))
                    .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery.selectbox.css")
                    .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui-1.10.0.custom.min.css")
                    .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui.css")
                    .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/Css/ZMyPrintVersion.css")
                    .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/Css/" + ZNodeCatalogManager.Theme + "_Client.css") //PRFT Custom Code
                    .Render("~/Squished/CSS/squished_" + ZNodeCatalogManager.Theme + "_#.css?v=" + ConfigurationManager.AppSettings["CSSVersion"].ToString());
            }

            return include;
        }

        /// <summary>
        /// Add Bootstarp CSS for Mobile Devices
        /// </summary>
        private void AddMobileBootStrapCSSForMobileDevices()
        {
            IncludeCSS(this, "bootstrap/css/bootstrap.min.css");
            IncludeCSS(this, ZNodeCatalogManager.GetCssPathByLocale(ZNodeConfigManager.SiteConfig.MobileTheme, ZNodeCatalogManager.CSS).Replace("~/", string.Empty));
            IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery.selectbox.css");
            IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui-1.10.0.custom.min.css");
            IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui.css");
            IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/Css/ZMyPrintVersion.css");
            IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/Css/" + ZNodeCatalogManager.Theme + "_Client.css"); //PRFT Custom Code
        }

        /// <summary>
        /// Add SquishedIT bundle css
        /// </summary>
        /// <returns>string</returns>
        private string AddSquishITBundleCSS()
        {
            string userAgent = Request.UserAgent; //entire UA string
            string include = string.Empty;
            if (!string.IsNullOrEmpty(userAgent) && userAgent.Contains("Trident") && userAgent.Contains("Trident/5.0"))
            {
                ApplyIE9CssPatch(include);
            }
            else
            {
                include = Bundle.Css()
                    .Add("~/bootstrap/css/bootstrap.min.css")
                    .Add(ZNodeCatalogManager.GetCssPathByLocale(ZNodeCatalogManager.Theme, ZNodeCatalogManager.CSS))
                    .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery.selectbox.css")
                    .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui-1.10.0.custom.min.css")
                    .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui.css")
                    .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/Css/ZMyPrintVersion.css")
                    .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/Css/" + ZNodeCatalogManager.Theme + "_Client.css")//PRFT Custom Code
                    .Render("~/Squished/CSS/squished_" + ZNodeCatalogManager.Theme + "_#.css?v=" + ConfigurationManager.AppSettings["CSSVersion"].ToString());
            }
            return include;
        }

        /// <summary>
        /// Add Bundle CSS
        /// </summary>
        private void AddBundleCSS()
        {
            IncludeCSS(this, "bootstrap/css/bootstrap.min.css");
            IncludeCSS(this, ZNodeCatalogManager.GetCssPathByLocale(ZNodeCatalogManager.Theme, ZNodeCatalogManager.CSS).Replace("~/", string.Empty));
            IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery.selectbox.css");
            IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui-1.10.0.custom.min.css");
            IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui.css");
            IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/Css/ZMyPrintVersion.css");
            IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/Css/" + ZNodeCatalogManager.Theme + "_Client.css");
        }

        /// <summary>
        /// Add CSS on Category Page
        /// </summary>
        /// <param name="isapplySquishIT">bool</param>
        private void AddCategoryCSS(bool isapplySquishIT)
        {
            string userAgent = Request.UserAgent; //entire UA string
            string include = string.Empty;
            if (isapplySquishIT)
            {
                #region[Zeon Custom Code : For BundleCss]
                if (!string.IsNullOrEmpty(userAgent) && userAgent.Contains("Trident") && userAgent.Contains("Trident/5.0"))
                {
                    string Include = string.Empty;
                    ApplyIE9CssPatch(Include);
                }
                else
                {
                    string Include = Bundle.Css()
                            .Add("~/bootstrap/css/bootstrap.min.css")
                            .Add(ZNodeCatalogManager.GetCssPathByLocale(this.Category.Theme, this.Category.Css))
                            .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery.selectbox.css")
                            .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui-1.10.0.custom.min.css")
                            .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui.css")
                            .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/Css/ZMyPrintVersion.css")
                            .Render("~/Squished/CSS/squished_" + ZNodeCatalogManager.Theme + "_#.css?v=" + ConfigurationManager.AppSettings["CSSVersion"].ToString());
                    Include = Include + "<link href='" + GetFontURL() + "' rel='stylesheet' type='text/css'>";

                    Include = Include + "<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><meta name='viewport' content='width=device-width, initial-scale=1'><meta name='viewport' content='minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no' />";

                    this.Page.Header.InnerHtml = Include;
                }

                #endregion
            }
            else
            {

                IncludeCSS(this, "bootstrap/css/bootstrap.min.css");
                IncludeCSS(this, ZNodeCatalogManager.GetCssPathByLocale(ZNodeConfigManager.SiteConfig.MobileTheme, ZNodeCatalogManager.CSS).Replace("~/", string.Empty));
                IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery.selectbox.css");
                IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui-1.10.0.custom.min.css");
                IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui.css");
                IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/Css/ZMyPrintVersion.css");
                IncludeCSS(this, "" + GetFontURL() + "");
                this.Page.Header.Controls.Add(new LiteralControl("<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><meta name='viewport' content='width=device-width, initial-scale=1'><meta name='viewport' content='minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no' />"));
            }
        }

        /// <summary>
        /// Add CSS on Product Page
        /// </summary>
        /// <param name="isapplySquishIT">bool</param>
        private void AddProductCSS(bool isapplySquishIT)
        {
            string userAgent = Request.UserAgent; //entire UA string
            this._lastcategoryId = int.Parse(Session["BreadCrumbCategoryId"].ToString());

            foreach (ZnodeProductCategory _productCategory in this.Product.ZNodeProductCategoryCollection)
            {
                if (this._lastcategoryId == _productCategory.CategoryID)
                {
                    if (isapplySquishIT)
                    {
                        #region[Zeon Custom Code : For BundleCss]
                        if (!string.IsNullOrEmpty(userAgent) && userAgent.Contains("Trident") && userAgent.Contains("Trident/5.0"))
                        {
                            string Include = string.Empty;
                            ApplyIE9CssPatch(Include);
                        }
                        else
                        {
                            string Include = Bundle.Css()
                            .Add("~/bootstrap/css/bootstrap.min.css")
                            .Add(ZNodeCatalogManager.GetCssPathByLocale(_productCategory.Theme, _productCategory.CSS))
                            .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery.selectbox.css")
                            .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui-1.10.0.custom.min.css")
                            .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui.css")
                            .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/Css/ZMyPrintVersion.css")
                            .Render("~/Squished/CSS/squished_" + ZNodeCatalogManager.Theme + "_#.css?v=" + ConfigurationManager.AppSettings["CSSVersion"].ToString());

                            Include = Include + "<link href='" + GetFontURL() + "' rel='stylesheet' type='text/css'>";
                            Include = Include + "<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><meta name='viewport' content='width=device-width, initial-scale=1'><meta name='viewport' content='minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no' />";

                            this.Page.Header.InnerHtml = Include;
                        }
                        #endregion
                    }
                    else
                    {
                        IncludeCSS(this, "bootstrap/css/bootstrap.min.css");
                        IncludeCSS(this, ZNodeCatalogManager.GetCssPathByLocale(_productCategory.Theme, _productCategory.CSS).Replace("~/", string.Empty));
                        IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery.selectbox.css");
                        IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui-1.10.0.custom.min.css");
                        IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui.css");
                        IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/Css/ZMyPrintVersion.css");
                        IncludeCSS(this, "" + GetFontURL() + "");
                        this.Page.Header.Controls.Add(new LiteralControl("<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><meta name='viewport' content='width=device-width, initial-scale=1'><meta name='viewport' content='minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no' />"));
                    }
                }
            }
        }

        /// <summary>
        /// Add CSS on Content Page
        /// </summary>
        /// <param name="isapplySquishIT">bool</param>
        private void AddContentPageCSS(bool isapplySquishIT)
        {
            string userAgent = Request.UserAgent; //entire UA string
            if (isapplySquishIT)
            {
                #region[Zeon Custom Code : For BundleCss]
                if (!string.IsNullOrEmpty(userAgent) && userAgent.Contains("Trident") && userAgent.Contains("Trident/5.0"))
                {
                    string Include = string.Empty;
                    ApplyIE9CssPatch(Include);
                }
                else
                {
                    string Include = Bundle.Css()
                        .Add("~/bootstrap/css/bootstrap.min.css")
                        .Add(ZNodeCatalogManager.GetCssPathByLocale(this.ContentPage.Theme, this.ContentPage.CSS))
                        .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery.selectbox.css")
                        .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui-1.10.0.custom.min.css")
                        .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui.css")
                        .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/Css/ZMyPrintVersion.css")
                        .Render("~/Squished/CSS/squished_" + ZNodeCatalogManager.Theme + "_#.css?v=" + ConfigurationManager.AppSettings["CSSVersion"].ToString());

                    Include = Include + "<link href='" + GetFontURL() + "' rel='stylesheet' type='text/css'>";
                    Include = Include + "<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><meta name='viewport' content='width=device-width, initial-scale=1'><meta name='viewport' content='minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no' />";
                    this.Page.Header.InnerHtml = Include;
                }
                #endregion
            }
            else
            {
                IncludeCSS(this, "bootstrap/css/bootstrap.min.css");
                IncludeCSS(this, ZNodeCatalogManager.GetCssPathByLocale(this.ContentPage.Theme, this.ContentPage.CSS).Replace("~/", string.Empty));
                IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery.selectbox.css");
                IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui-1.10.0.custom.min.css");
                IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui.css");
                IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/Css/ZMyPrintVersion.css");
                IncludeCSS(this, "" + GetFontURL() + "");
                this.Page.Header.Controls.Add(new LiteralControl("<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><meta name='viewport' content='width=device-width, initial-scale=1'><meta name='viewport' content='minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no' />"));
            }
        }

        /// <summary>
        /// Bind canonical link in Non SEO Pages and Non Content Pages
        /// </summary>
        protected void BindCanonicalTagOnNonSEOPages()
        {
            string siteUrl = "https://" + this.Request.Url.Authority + "/";
            if (this.Request.Url.Authority.StartsWith("www."))
            {
                siteUrl = "https://" + this.Request.Url.Authority;
            }
            else
            {
                siteUrl = "https://www." + this.Request.Url.Authority;
            }
            HtmlGenericControl IncludeConanical = new HtmlGenericControl("link");
            IncludeConanical.Attributes.Add("rel", "canonical");
            IncludeConanical.Attributes.Add("href", siteUrl + this.Request.RawUrl);

            // Add a reference for canonical to the head section
            if (this.Page.Header != null) this.Page.Header.Controls.Add(IncludeConanical);
        }

        /// <summary>
        /// Validate if current page is among Noindexed Page
        /// </summary>
        /// <returns></returns>
        private bool IsCustomNoIndexedPage()
        {
            bool noIndexedPage = false;
            if (ConfigurationManager.AppSettings["CustomNoIndexPageList"] != null && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["CustomNoIndexPageList"]))
            {
                string[] strPageList = ConfigurationManager.AppSettings["CustomNoIndexPageList"].ToString().Split(',');
                if (strPageList.Length > 0)
                {
                    for (int index = 0; index < strPageList.Length; index++)
                    {
                        if (this.Request.FilePath.ToString().IndexOf(strPageList[index].ToString()) >= 0)
                        {
                            noIndexedPage = true;
                            break;
                        }
                    }
                }
            }
            return noIndexedPage;
        }
        #endregion
    }
}
