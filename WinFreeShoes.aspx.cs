﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    public partial class WinFreeShoes : CommonPageBase
    {
        protected override void Page_PreInit(object sender, EventArgs e)
        {
            this.MasterPageFile = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/MasterPages/ZWinFreeShoes.master";           
        }
    }
}